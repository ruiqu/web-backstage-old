export default [
  {
    path: '/user',
    component: '../layouts/UserLayout',
    routes: [
      {
        name: 'login',
        path: '/user/login',
        component: './user/login',
      },
      {
        name: 'ChangePassword',
        path: '/user/ChangePassword',
        component: './user/ChangePassword',
      },
      {
        name: 'PasswordStatuspage',
        path: '/user/PasswordStatuspage',
        component: './user/PasswordStatuspage',
      },
      {
        name: 'notice',
        path: '/user',
        component: './user/Notice',
      },
      {
        component: './404',
      },
    ],
  },
  {
    path: '/',
    component: '../layouts/SecurityLayout',
    routes: [
      {
        path: '/',
        component: '../layouts/BasicLayout',
        // authority: ['admin', 'user'],
        routes: [
          // dashboard
          { path: '/', redirect: '/Index' },
          // 首页
          {
            path: '/Index',
            name: 'Index',
            icon: 'bank',
            component: './Index',
          },
          // 店铺管理
          {
            path: '/shop',
            name: 'shop',
            icon: 'shop',
            authority: ['店铺管理'],
            routes: [
              {
                path: '/shop/info',
                name: 'info',
                component: './Shop/Info',
              },
              {
                path: '/shop/info/addInfo',
                name: 'addInfo',
                component: './Shop/AddInfo',
                hideInMenu: true,
              },
              {
                path: '/shop/info/Agreement',
                name: 'Agreement',
                component: './Shop/Agreement',
                hideInMenu: true,
              },
            ],
          },
          // 商品管理
          {
            path: '/goods',
            name: 'goods',
            icon: 'shopping',
            authority: ['商品管理'],
            routes: [
              // 所有商品
              {
                path: '/goods/list',
                name: 'list',
                component: './Goods/List',
                authority: ['商品列表'],
              },
              // {
              //   path: '/goods/Purchase',
              //   name: 'Purchase',
              //   component: './Goods/Purchase',
              //   authority: ['商品列表'],
              // },
              {
                path: '/goods/list/detail/:id?',
                name: 'detail',
                component: './Goods/Detail',
                hideInMenu: true,
              },
              {
                path: '/goods/Purchase/PurchaseDetail/:id?',
                name: 'PurchaseDetail',
                component: './Goods/PurchaseDetail',
                hideInMenu: true,
              },

              // 租赁规则模板
              // {
              //   path: '/goods/rentRuleList',
              //   name: 'rentRuleList',
              //   component: './Goods/RentRuleList',
              // },
              // // 租赁规则模板详情
              // {
              //   path: '/goods/rentRuleList/rentRuleDetail/:id?',
              //   name: 'rentRuleDetail',
              //   component: './Goods/RentRuleDetail',
              //   hideInMenu: true,
              // },
              // // 赔偿规则模板
              // {
              //   path: '/goods/compensateList',
              //   name: 'compensateList',
              //   component: './Goods/CompensateList',
              // },
              // {
              //   path: '/goods/compensateList/compensateDetail/:id?',
              //   name: 'compensateDetail',
              //   component: './Goods/CompensateDetail',
              //   hideInMenu: true,
              // },
              // 归还地址
              {
                path: '/goods/givebackList',
                name: 'givebackList',
                component: './Goods/GivebackList',
                authority: ['归还地址'],
              },
              // 增值服务
              {
                path: '/goods/addedServices',
                name: 'addedServices',
                component: './Shop/AddedServices',
                authority: ['增值服务'],
              },
              {
                path: '/goods/addedServices/detail/:id?',
                name: 'serviceDetail',
                component: './Shop/ServiceDetail',
                hideInMenu: true,
              },
            ],
          },
       
          // 订单
          {
            path: '/Order',
            name: 'Order',
            icon: 'audit',
            hideInMenu: false,
            authority: ['订单管理'],
            routes: [
              {
                path: '/Order/HomePage',
                component: './Order/HomePage',
                hideInMenu: false,
                name: 'HomePage',
                authority: ['订单列表'],
              },
              {
                path: '/Order/monthTj',
                component: './Order/monthTj',
                hideInMenu: false,
                name: 'monthTj',
                authority: ['订单统计'],
              },
              {
                path: '/Order/overOrder',
                component: './Order/overOrder',
                hideInMenu: false,
                name: 'overOrder',
                authority: ['租用中'],
              },
              {
                path: '/Order/HomePage/Details',
                component: './Order/Details',
                hideInMenu: true,
                name: 'Details',
              },
              {
                path: '/Order/HomePage/ReceiveOrder',
                component: './Order/HomePage/ReceiveOrder',
                hideInMenu: true,
                name: 'ReceiveOrder',
              },
              // {
              //   path: '/Order/Close',
              //   component: './Order/Close',
              //   hideInMenu: false,
              //   name: 'Close',
              // },

              {
                path: '/Order/BeOverdue',
                component: './Order/BeOverdue',
                hideInMenu: false,
                name: 'BeOverdue',
                authority: ['逾期订单'],
              },
              {
                path: '/Order/yqtj',
                component: './Order/yqtj',
                hideInMenu: false,
                name: 'yqtj',
                authority: ['逾期统计'],
              },
              {
                path: '/Order/OverdueNoRetrun',
                component: './Order/OverdueNoRetrun',
                hideInMenu: false,
                name: 'OverdueNoRetrun',
                authority: ['到期未归还订单'],
              },
              {
                path: '/Order/BeOverdue/Details',
                component: './Order/Details',
                hideInMenu: true,
                name: 'Details',
              },
              {
                path: '/Order/OverdueNoRetrun/Details',
                component: './Order/Details',
                hideInMenu: true,
                name: 'Details',
              },
              {
                path: '/Order/BuyOut',
                component: './Order/BuyOut',
                hideInMenu: false,
                name: 'BuyOut',
                authority: ['买断订单'],
              },
              {
                path: '/Order/BuyOut/Details',
                component: './Order/Details',
                hideInMenu: true,
                name: 'Details',
              },

              {
                path: '/Order/RentRenewal',
                component: './Order/RentRenewal',
                hideInMenu: false,
                name: 'RentRenewal',
                authority: ['续租订单'],
              },
              {
                path: '/Order/RentRenewal/Details',
                component: './Order/Details',
                hideInMenu: true,
                name: 'Details',
              },
              // {
              //   path: '/Order/Purchase',
              //   component: './Order/Purchase',
              //   hideInMenu: false,
              //   name: 'Purchase',
              //   authority: ['续租订单'],
              // },
              // {
              //   path: '/Order/Purchase/Details',
              //   component: './Order/Details',
              //   hideInMenu: true,
              //   name: 'Details',
              // },
              {
                path: '/Order/Details',
                component: './Order/Details',
                hideInMenu: true,
                name: 'Details',
              },
              // {
              //   path: '/Order/Details/:id',
              //   component: './Order/Details',
              //   hideInMenu: true,
              //   name: 'Details',
              // },
              {
                path: '/Order/BuyOut/BuyOutDetails',
                component: './Order/BuyOutDetails',
                hideInMenu: true,
                name: 'BuyOutDetails',
              },
              {
                path: '/Order/Purchase/PurchaseDetails',
                component: './Order/PurchaseDetails',
                hideInMenu: true,
                name: 'Details',
              },
              {
                component: './404',
              },
            ],
          },
          {
            path: '/zz',
            name: 'zz',
            icon: 'GlobalOutlined',
            authority: ['在租订单'],
            routes: [
              {
                path: '/zz/zyz',
                component: './Order/zyz',
                hideInMenu: false,
                name: 'zyz',
                authority: ['租用中'],
              },
              {
                path: '/zz/yuefu',
                component: './Order/yuefu',
                hideInMenu: false,
                name: 'yuefu',
                authority: ['月付账单'],
              },
              {
                path: '/zz/zhoufu',
                component: './Order/zhoufu',
                hideInMenu: false,
                name: 'zhoufu',
                authority: ['周付账单'],
              },
              {
                path: '/zz/rifu',
                component: './Order/rifu',
                hideInMenu: false,
                name: 'rifu',
                authority: ['日付账单'],
              },
              {
                path: '/zz/day10fu',
                component: './Order/day10fu',
                hideInMenu: false,
                name: 'day10fu',
                authority: ['10天账单'],
              },
              {
                path: '/zz/day5fu',
                component: './Order/day5fu',
                hideInMenu: false,
                name: 'day5fu',
                authority: ['10天账单'],
              },
            ],
          },
          {
            path: '/qd',
            name: 'qd',
            icon: 'ForkOutlined',
            authority: ['渠道'],
            routes: [
              {
                path: '/qd/qd',
                name: 'qd',
                component: './Finance/qd',
                authority: ['渠道管理'],
              },
              {
                path: '/qd/qdtj',
                name: 'qdtj',
                component: './Finance/qdtj',
                authority: ['渠道统计'],
              },
            ],
          },
          // 营销管理
       
          // 数据管理
          {
            path: '/dataManagement',
            name: 'dataManagement',
            icon: 'table',
            authority: ['数据管理'],
            routes: [
              {
                path: '/dataManagement/dataDownloadCenter',
                name: 'dataDownloadCenter',
                component: './DataManagement/ExportDownloadCenter',
                authority: ['导出数据下载'],
              }
            ],
          },
          {
            path: '/coupons',
            icon: 'money-collect',
            name: 'coupons',
            authority: ['营销管理'],
            // authority: ['admin'],
            routes: [
              {
                path: '/coupons/list',
                name: 'list',
                component: './Coupons/List',
                authority: ['优惠券列表'],
              },
              {
                path: '/coupons/list/add',
                name: 'add',
                component: './Coupons/AddList',
                hideInMenu: true,
              },
              {
                path: '/coupons/Package',
                name: 'Package',
                component: './Coupons/Package',
                authority: ['大礼包'],
              },
              {
                hideInMenu: true,
                path: '/coupons/list/ToView',
                name: 'ToView',
                component: './Coupons/ToView',
              },
              {
                hideInMenu: true,
                path: '/coupons/TheEditorList',
                name: 'TheEditorList',
                component: './Coupons/TheEditorList',
              },
              {
                path: '/coupons/config',
                name: 'config',
                component: './Coupons/Config',
                authority: ['店铺营销图配置'],
              },
            ],
          },

          // 财务管理
          {
            path: '/finance',
            name: 'finance',
            icon: 'shopping',
            authority: ['财务管理'],
            routes: [
              {
                path: '/finance/capitalAccount',
                name: 'money',
                component: './Finance/capitalAccount',
                authority: ['资金账户'],
              },
              
              {
                path: '/finance/settlement',
                name: 'settlement',
                component: './Finance/settlement',
                authority: ['财务结算'],
              },
              {
                path: '/finance/overview',
                name: 'overview',
                component: './Finance/overview',
                authority: ['结算明细查询'],
              },
              {
                path: '/finance/feeDetail',
                name: 'feeDetail',
                component: './Finance/feeDetail',
                authority: ['费用结算明细'],
              },
              {
                path: '/finance/settlement/detail',
                name: 'settlementdetail',
                component: './Finance/settlementDetail',
                hideInMenu: true,
              },
              {
                path: '/finance/normal',
                name: 'normal',
                component: './Finance/normal',

                hideInMenu: true,
              },
              {
                path: '/finance/normal/detail/:id',
                name: 'detail',
                component: './Finance/orderDetail',
                hideInMenu: true,

              },

              {
                path: '/finance/buyout',
                name: 'buyout',
                component: './Finance/buyout',

                hideInMenu: true,
              },
              {
                path: '/finance/buyout/detail/:id',
                name: 'detail',
                component: './Finance/buyoutOrderDetail',
                hideInMenu: true,
              },
              {
                path: '/finance/purchase',
                name: 'purchase',
                component: './Finance/purchase',

                hideInMenu: true,
              },
              {
                path: '/finance/purchase/purchaseDetail/:id',
                name: 'purchaseDetail',
                component: './Finance/purchaseDetail',
                hideInMenu: true,
              },
            ],
          },
      // 权限管理
      {
        path: '/permission',
        name: 'permission',
        icon: 'tool',
        authority: ['权限管理'],
        routes: [
          {
            path: '/permission/index',
            name: 'index',
            component: './Permission/index',
            authority: ['部门列表'],
          },
          {
            path: '/permission/index/config/:id',
            name: 'config',
            component: './Permission/PermissionConfig',
            hideInMenu: true,
          },
          {
            path: '/permission/member',
            name: 'member',
            component: './Member/index',
            authority: ['成员管理'],
          },
          {
            path: '/permission/member/config/:id',
            name: 'config',
            component: './Permission/PermissionConfig',
            hideInMenu: true,
          },
          // {
          //   path: '/permission/notice',
          //   name: 'notice',
          //   component: './Notice/index',
          //   authority: ['成员管理'],
          // },
        ],
      },

          // 服务中心
          {
            path: '/serviceCenter',
            name: 'serviceCenter',
            icon: 'question-circle',
            authority: ['服务中心'],
            routes: [
              {
                path: '/serviceCenter',
                name: 'serviceCenterProblem',
                component: './ServiceCenter/problemList',
                authority: ['常见问题'],
              },
              {
                path: '/serviceCenter/ts',
                component: './Order/ts',
                hideInMenu: false,
                name: 'ts',
                authority: ['投诉'],
              },
              {
                path: '/serviceCenter/problemDetail/:id',
                name: 'serviceCenterProblemDetail',
                component: './ServiceCenter/problemDetail',
                hideInMenu: true,
              }
            ]
          },

    
          {
            component: './404',
          },
        ],
      },
      {
        component: './404',
      },
    ],
  },
  {
    component: './404',
  },
];
