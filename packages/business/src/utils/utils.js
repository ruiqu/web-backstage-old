import { parse } from 'querystring';
import pathRegexp from 'path-to-regexp';
import moment from 'moment';
import { routerRedux } from 'dva/router';
import { uploadUrl } from '../config';
import { getToken } from '@/utils/localStorage';
import {
  Card, Tooltip, Button, Form, Input, Select, DatePicker, Spin, Row,
  Col, Divider, Modal, Drawer,
  InputNumber, Descriptions,
  message, Tag
} from 'antd';
/* eslint no-useless-escape:0 import/prefer-default-export:0 */
const reg = /(((^https?:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+(?::\d+)?|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)$/;
export const isUrl = path => reg.test(path);
export const isAntDesignPro = () => {
  if (ANT_DESIGN_PRO_ONLY_DO_NOT_USE_IN_YOUR_PRODUCTION === 'site') {
    return true;
  }

  return window.location.hostname === 'preview.pro.ant.design';
}; // 给官方演示站点用，用于关闭真实开发环境不需要使用的特性

export const isAntDesignProOrDev = () => {
  const { NODE_ENV } = process.env;

  if (NODE_ENV === 'development') {
    return true;
  }

  return isAntDesignPro();
};
export const getPageQuery = () => parse(window.location.href.split('?')[1]);
/**
 * props.route.routes
 * @param router [{}]
 * @param pathname string
 */

const gv = (d, k) => {
  if (d["var2_" + k] == "null") return '无'
  return d["var2_" + k]
}

//处理征信数据
export const handzx = (d) => {
  //处理成 几个块
  //1账龄
  let v账龄 = {
    column: [
      { title: '', dataIndex: 'name', },
      { title: '房贷', dataIndex: 'fd', },
      { title: '车贷', dataIndex: 'cd', },
      { title: '融资租赁业务', dataIndex: 'rzzl', },
      { title: '个人经营性贷款', dataIndex: 'grjyxdk', },
      { title: '其他个人消费贷款', dataIndex: 'qtgrxfdk', },
      { title: '非循环贷账户', dataIndex: 'fxhdzh', },
      { title: '循环贷账户和循环额度下分账户', dataIndex: 'fhdzhhxhed', },
      { title: '信用卡', dataIndex: 'xhk', },
    ],

    val: [
      //fd房贷，cd车贷，rzzl融资租赁业务
      {
        name: "最大帐龄", fd: gv(d, "1"), cd: gv(d, "2"),
        rzzl: gv(d, "3"), grjyxdk: gv(d, "4"), qtgrxfdk: gv(d, "5"), fxhdzh: gv(d, "6"),
        fhdzhhxhed: gv(d, "7"), xhk: gv(d, "8")
      },
      {
        name: "最小帐龄", fd: gv(d, "9"), cd: gv(d, "10"), rzzl: gv(d, "11"), grjyxdk: gv(d, "12"),
        qtgrxfdk: gv(d, "13"), fxhdzh: gv(d, "14"), fhdzhhxhed: gv(d, "15"), xhk: gv(d, "16")
      },
      {
        name: "账户总数", fd: gv(d, "17"), cd: gv(d, "18"), rzzl: gv(d, "19"), grjyxdk: gv(d, "20"),
        qtgrxfdk: gv(d, "21"), fxhdzh: gv(d, "22"), fhdzhhxhed: gv(d, "23"), xhk: gv(d, "24")
      },
    ]
  }
  d.v账龄 = v账龄
  d.v开户数 = {
    column: [
      { title: '', dataIndex: 'name', },
      { title: '房贷', dataIndex: 'fd', },
      { title: '车贷', dataIndex: 'cd', },
      { title: '融资租赁业务', dataIndex: 'rzzl', },
      { title: '个人经营性贷款', dataIndex: 'grjyxdk', },
      { title: '其他个人消费贷款', dataIndex: 'qtgrxfdk', },
      { title: '非循环贷账户', dataIndex: 'fxhdzh', },
      { title: '循环贷账户和循环额度下分账户', dataIndex: 'fhdzhhxhed', },
      { title: '信用卡', dataIndex: 'xhk', },
      { title: '总数', dataIndex: 'zs', },
    ],
    val: [
      {
        name: "近3个月新开账户数", fd: gv(d, "26"), cd: gv(d, "27"),
        rzzl: gv(d, "28"), grjyxdk: gv(d, "29"), qtgrxfdk: gv(d, "30"), fxhdzh: gv(d, "31"),
        fhdzhhxhed: gv(d, "32"), xhk: gv(d, "33"), zs: gv(d, "34")
      },
      {
        name: "近6个月新开账户数", fd: gv(d, "35"), cd: gv(d, "36"),
        rzzl: gv(d, "37"), grjyxdk: gv(d, "38"), qtgrxfdk: gv(d, "39"), fxhdzh: gv(d, "40"),
        fhdzhhxhed: gv(d, "41"), xhk: gv(d, "42"), zs: gv(d, "43")
      },
    ]
  }
  d.v其它数据 = [
    { title: '本月个人房贷应还款总金额', dataIndex: 'var2_44', render: e => e == 'null' ? "无" : e, },
    { title: '本月车贷应还款总金额', dataIndex: 'var2_45', render: e => e == 'null' ? "无" : e, },
    { title: '本月个人融资租赁业务应还款总金额', dataIndex: 'var2_46', render: e => e == 'null' ? "无" : e, },
    { title: '本月个人经营性贷款应还总金额', dataIndex: 'var2_47', render: e => e == 'null' ? "无" : e, },
    { title: '本月其他个人消费贷款应还总金额', dataIndex: 'var2_48', render: e => e == 'null' ? "无" : e, },
    { title: '本月非循环贷账户应还款账户总金额', dataIndex: 'var2_49', render: e => e == 'null' ? "无" : e, },
    { title: '本月循环贷和循环额度下分账户应还款账户总金额', dataIndex: 'var2_50', render: e => e == 'null' ? "无" : e, },
    { title: '本月信用卡应还款总金额', dataIndex: 'var2_51', render: e => e == 'null' ? "无" : e, },
    { title: '本月个人所有应还款总金额', dataIndex: 'var2_52', render: e => e == 'null' ? "无" : e, },
  ]
  d.v其它数据1 = [
    { title: '个人房贷负债余额', dataIndex: 'var2_53', render: e => e == 'null' ? "无" : e, },
    { title: '车贷账户负债余额', dataIndex: 'var2_54', render: e => e == 'null' ? "无" : e, },
    { title: '个人融资租赁业务账户负债余额', dataIndex: 'var2_55', render: e => e == 'null' ? "无" : e, },
    { title: '个人经营性贷款负债总余额', dataIndex: 'var2_56', render: e => e == 'null' ? "无" : e, },
    { title: '其他个人消费贷款负债总余额', dataIndex: 'var2_57', render: e => e == 'null' ? "无" : e, },
    { title: '非循环贷账户负债余额', dataIndex: 'var2_58', render: e => e == 'null' ? "无" : e, },
    { title: '循环贷和循环额度下分账户负债余额', dataIndex: 'var2_59', render: e => e == 'null' ? "无" : e, },
    { title: '信用卡负债余额', dataIndex: 'var2_60', render: e => e == 'null' ? "无" : e, },
    { title: '个人所有负债账户余额', dataIndex: 'var2_61', render: e => e == 'null' ? "无" : e, },
  ]
  d.v其它数据2 = [
    { title: '2年内连三累六账户数', dataIndex: 'var2_410', render: e => e == 'null' ? "无" : e, },
    { title: '2年内连四累八账户数', dataIndex: 'var2_411', render: e => e == 'null' ? "无" : e, },
    { title: '1年内累计逾期超过5次金额超过1.5万的账户数', dataIndex: 'var2_412', render: e => e == 'null' ? "无" : e, },
  ]
  d.v其它数据3 = [
    { title: '过去24-48个月连四累八账户数', dataIndex: 'var2_416', render: e => e == 'null' ? "无" : e, },
    { title: '过去24-48个月最大逾期期数(合计)', dataIndex: 'var2_414', render: e => e == 'null' ? "无" : e, },
    { title: '过去24-48个月逾期次数(合计)', dataIndex: 'var2_415', render: e => e == 'null' ? "无" : e, },
  ]

  let xl =
    d.v其它数据4 = [
      { title: '呆账账户数', dataIndex: 'baddebtNo', render: e => e == 'null' ? "无" : e, },
      { title: '冻结账户数', dataIndex: 'frozenAccountNo', render: e => e == 'null' ? "无" : e, },
      { title: '止付账户数', dataIndex: 'stopPaymentNo', render: e => e == 'null' ? "无" : e, },
      { title: '未结清贷款五级分类为次级、损失、可疑', dataIndex: 'loanClass', render: e => e == 'null' ? "无" : e, },
      { title: '学历', dataIndex: 'degree', render: e => e == 'null' ? "无" : e, },
      { title: '婚姻状况', dataIndex: 'marriage_status', render: e => e == 'null' ? "无" : e, },
    ]

  d.v最近月份 = {
    column: [
      { title: '', dataIndex: 'name', },
      { title: '房贷', dataIndex: 'fd', },
      { title: '车贷', dataIndex: 'cd', },
      { title: '融资租赁', dataIndex: 'rzzl', },
      { title: '个人经营性贷款', dataIndex: 'grjyxdk', },
      { title: '其他个人消费贷款', dataIndex: 'qtgrxfdk', },
      { title: '非循环贷账户', dataIndex: 'fxhdzh', },
      { title: '循环贷账户和循环额度下分账户', dataIndex: 'fhdzhhxhed', },
      { title: '信用卡', dataIndex: 'xhk', },
      { title: '准贷记卡', dataIndex: 'zdjk', },
      { title: '总数', dataIndex: 'zs', },
    ],
    val: [
      {
        name: "1个月逾期次数", fd: gv(d, "62"),
        cd: gv(d, "63"), rzzl: gv(d, "64"),
        grjyxdk: gv(d, "65"), qtgrxfdk: gv(d, "66"),
        fxhdzh: gv(d, "67"), fhdzhhxhed: gv(d, "68"),
        xhk: gv(d, "69"), zs: gv(d, "71"),
        zdjk: gv(d, "70")
      },
      {
        name: "3个月逾期次数", fd: gv(d, "72"),
        cd: gv(d, "73"), rzzl: gv(d, "74"),
        grjyxdk: gv(d, "75"), qtgrxfdk: gv(d, "76"),
        fxhdzh: gv(d, "77"), fhdzhhxhed: gv(d, "78"),
        xhk: gv(d, "79"),
        zs: gv(d, "80"),
      },
      {
        name: "6个月逾期次数", fd: gv(d, "81"),
        cd: gv(d, "82"), rzzl: gv(d, "83"),
        grjyxdk: gv(d, "84"), qtgrxfdk: gv(d, "85"),
        fxhdzh: gv(d, "86"), fhdzhhxhed: gv(d, "87"),
        xhk: gv(d, "88"),
        zs: gv(d, "89"),
      },
      {
        name: "12个月逾期次数", fd: gv(d, "90"),
        cd: gv(d, "91"), rzzl: gv(d, "92"),
        grjyxdk: gv(d, "93"), qtgrxfdk: gv(d, "94"),
        fxhdzh: gv(d, "95"), fhdzhhxhed: gv(d, "96"),
        xhk: gv(d, "97"),
        zs: gv(d, "98"),
      },
      {
        name: "24个月逾期次数", fd: gv(d, "99"),
        cd: gv(d, "100"), rzzl: gv(d, "101"),
        grjyxdk: gv(d, "102"), qtgrxfdk: gv(d, "103"),
        fxhdzh: gv(d, "104"), fhdzhhxhed: gv(d, "105"),
        xhk: gv(d, "106"),
        zs: gv(d, "107"),
      },
      {
        name: "60个月逾期次数", fd: gv(d, "108"),
        cd: gv(d, "109"), rzzl: gv(d, "110"),
        grjyxdk: gv(d, "111"), qtgrxfdk: gv(d, "112"),
        fxhdzh: gv(d, "113"), fhdzhhxhed: gv(d, "114"),
        xhk: gv(d, "115"),
        zs: gv(d, "116"),
      },
      {
        name: "1个月最大逾期期数", fd: gv(d, "117"),
        cd: gv(d, "118"), rzzl: gv(d, "119"),
        grjyxdk: gv(d, "120"), qtgrxfdk: gv(d, "121"),
        fxhdzh: gv(d, "122"), fhdzhhxhed: gv(d, "123"),
        xhk: gv(d, "124"), zs: gv(d, "125"),
      },
      {
        name: "3个月最大逾期期数", fd: gv(d, "126"),
        cd: gv(d, "127"), rzzl: gv(d, "128"),
        grjyxdk: gv(d, "129"), qtgrxfdk: gv(d, "130"),
        fxhdzh: gv(d, "131"), fhdzhhxhed: gv(d, "132"),
        xhk: gv(d, "133"), zs: gv(d, "134"),
      },
      {
        name: "6个月最大逾期期数", fd: gv(d, "135"),
        cd: gv(d, "136"), rzzl: gv(d, "137"),
        grjyxdk: gv(d, "138"), qtgrxfdk: gv(d, "139"),
        fxhdzh: gv(d, "140"), fhdzhhxhed: gv(d, "141"),
        xhk: gv(d, "142"), zs: gv(d, "143"),
      },
      {
        name: "12个月最大逾期期数", fd: gv(d, "144"),
        cd: gv(d, "145"), rzzl: gv(d, "146"),
        grjyxdk: gv(d, "147"), qtgrxfdk: gv(d, "148"),
        fxhdzh: gv(d, "149"), fhdzhhxhed: gv(d, "150"),
        xhk: gv(d, "151"), zs: gv(d, "152"),
      },
      {
        name: "24个月最大逾期期数", fd: gv(d, "153"),
        cd: gv(d, "154"), rzzl: gv(d, "155"),
        grjyxdk: gv(d, "156"), qtgrxfdk: gv(d, "157"),
        fxhdzh: gv(d, "158"), fhdzhhxhed: gv(d, "159"),
        xhk: gv(d, "160"), zs: gv(d, "161"),
      },
      {
        name: "60个月最大逾期期数", fd: gv(d, "162"),
        cd: gv(d, "163"), rzzl: gv(d, "164"),
        grjyxdk: gv(d, "165"), qtgrxfdk: gv(d, "166"),
        fxhdzh: gv(d, "167"), fhdzhhxhed: gv(d, "168"),
        xhk: gv(d, "169"), zs: gv(d, "170"),
      },
      {
        name: "过去3个月最大逾期金额", fd: gv(d, "171"),
        cd: gv(d, "172"), rzzl: gv(d, "173"),
        grjyxdk: gv(d, "174"), qtgrxfdk: gv(d, "175"),
        fxhdzh: gv(d, "176"), fhdzhhxhed: gv(d, "177"),
        xhk: gv(d, "178"), zs: gv(d, "179"),
      },
      {
        name: "过去6个月最大逾期金额", fd: gv(d, "180"),
        cd: gv(d, "181"), rzzl: gv(d, "182"),
        grjyxdk: gv(d, "183"), qtgrxfdk: gv(d, "184"),
        fxhdzh: gv(d, "185"), fhdzhhxhed: gv(d, "186"),
        xhk: gv(d, "187"), zs: gv(d, "188"),
      },
      {
        name: "过去12个月最大逾期金额", fd: gv(d, "189"),
        cd: gv(d, "190"), rzzl: gv(d, "191"),
        grjyxdk: gv(d, "192"), qtgrxfdk: gv(d, "193"),
        fxhdzh: gv(d, "194"), fhdzhhxhed: gv(d, "195"),
        xhk: gv(d, "196"), zs: gv(d, "197"),
      },
      {
        name: "过去24个月最大逾期金额", fd: gv(d, "199"),
        cd: gv(d, "199"), rzzl: gv(d, "200"),
        grjyxdk: gv(d, "201"), qtgrxfdk: gv(d, "202"),
        fxhdzh: gv(d, "203"), fhdzhhxhed: gv(d, "204"),
        xhk: gv(d, "205"), zs: gv(d, "206"),
      },
      {
        name: "1个月逾期账户数合计", fd: gv(d, "207"),
        cd: gv(d, "208"), rzzl: gv(d, "209"),
        grjyxdk: gv(d, "210"), qtgrxfdk: gv(d, "211"),
        fxhdzh: gv(d, "212"), fhdzhhxhed: gv(d, "213"),
        xhk: gv(d, "214"), zs: gv(d, "215"),
      },
      {
        name: "3个月逾期账户数合计", fd: gv(d, "216"),
        cd: gv(d, "217"), rzzl: gv(d, "218"),
        grjyxdk: gv(d, "219"), qtgrxfdk: gv(d, "220"),
        fxhdzh: gv(d, "221"), fhdzhhxhed: gv(d, "222"),
        xhk: gv(d, "223"), zs: gv(d, "224"),
      },
      {
        name: "6个月逾期账户数合计", fd: gv(d, "225"),
        cd: gv(d, "226"), rzzl: gv(d, "227"),
        grjyxdk: gv(d, "228"), qtgrxfdk: gv(d, "229"),
        fxhdzh: gv(d, "230"), fhdzhhxhed: gv(d, "231"),
        xhk: gv(d, "232"), zs: gv(d, "233"),
      },
      {
        name: "12个月逾期账户数合计", fd: gv(d, "234"),
        cd: gv(d, "235"), rzzl: gv(d, "236"),
        grjyxdk: gv(d, "237"), qtgrxfdk: gv(d, "238"),
        fxhdzh: gv(d, "239"), fhdzhhxhed: gv(d, "240"),
        xhk: gv(d, "241"), zs: gv(d, "242"),
      },
      {
        name: "24个月逾期账户数合计", fd: gv(d, "243"),
        cd: gv(d, "244"), rzzl: gv(d, "245"),
        grjyxdk: gv(d, "246"), qtgrxfdk: gv(d, "247"),
        fxhdzh: gv(d, "248"), fhdzhhxhed: gv(d, "249"),
        xhk: gv(d, "250"), zs: gv(d, "251"),
      },
      {
        name: "最近一次逾期M1及以上距今月数最大值", fd: gv(d, "252"),
        cd: gv(d, "253"), rzzl: gv(d, "254"),
        grjyxdk: gv(d, "255"), qtgrxfdk: gv(d, "256"),
        fxhdzh: gv(d, "258"), fhdzhhxhed: gv(d, "259"),
        xhk: gv(d, "257"),
      },
      {
        name: "最近一次逾期M2及以上距今月数最大值", fd: gv(d, "260"),
        cd: gv(d, "261"), rzzl: gv(d, "262"),
        grjyxdk: gv(d, "263"), qtgrxfdk: gv(d, "264"),
        fxhdzh: gv(d, "266"), fhdzhhxhed: gv(d, "267"),
        xhk: gv(d, "265"),
      },
      {
        name: "最近一次逾期M3及以上距今月数最大值", fd: gv(d, "268"),
        cd: gv(d, "269"), rzzl: gv(d, "270"),
        grjyxdk: gv(d, "271"), qtgrxfdk: gv(d, "272"),
        fxhdzh: gv(d, "274"), fhdzhhxhed: gv(d, "275"),
        xhk: gv(d, "273"),
      },
      {
        name: "在册时间>=3个月,从未逾期且帐户状态正常帐户数",
        fd: gv(d, "300"), cd: gv(d, "301"), rzzl: gv(d, "302"),
        grjyxdk: gv(d, "303"), qtgrxfdk: gv(d, "304"),
        fhdzhhxhed: gv(d, "305"), xhk: gv(d, "306"), zs: gv(d, "307"),
      },
      {
        name: "在册时间>=6个月,从未逾期且帐户状态正常帐户数",
        fd: gv(d, "308"), cd: gv(d, "309"), rzzl: gv(d, "310"),
        grjyxdk: gv(d, "311"), qtgrxfdk: gv(d, "312"),
        fhdzhhxhed: gv(d, "313"), xhk: gv(d, "314"), zs: gv(d, "315"),
      },
      {
        name: "在册时间>=12个月,从未逾期且帐户状态正常帐户数",
        fd: gv(d, "316"), cd: gv(d, "317"), rzzl: gv(d, "318"),
        grjyxdk: gv(d, "319"), qtgrxfdk: gv(d, "320"),
        fhdzhhxhed: gv(d, "321"), xhk: gv(d, "322"), zs: gv(d, "323"),
      },
      {
        name: "在册时间>=24个月,从未逾期且帐户状态正常帐户数",
        fd: gv(d, "324"), cd: gv(d, "325"), rzzl: gv(d, "326"),
        grjyxdk: gv(d, "327"), qtgrxfdk: gv(d, "328"),
        fhdzhhxhed: gv(d, "329"), xhk: gv(d, "330"), zs: gv(d, "331"),
      },
    ]
  }
  d.v查询情况 = {
    column: [
      { title: '时间', dataIndex: 'name', },
      { title: '贷款审批', dataIndex: 'dk', },
      { title: '信用卡审批', dataIndex: 'xyk', },
      { title: '融资审批', dataIndex: 'rz', },
      { title: '担保资格审查', dataIndex: 'dbzg', },
      { title: '总查询', dataIndex: 'zz', },
    ],
    val: [
      {
        name: "最近1个月查询次数", dk: gv(d, "361"), xyk: gv(d, "362"),
        rz: gv(d, "363"), dbzg: gv(d, "364"), zz: gv(d, "365"),
      },
      {
        name: "最近3个月查询次数", dk: gv(d, "366"), xyk: gv(d, "367"),
        rz: gv(d, "368"), dbzg: gv(d, "369"), zz: gv(d, "370"),
      },
      {
        name: "最近6个月查询次数", dk: gv(d, "371"), xyk: gv(d, "372"),
        rz: gv(d, "373"), dbzg: gv(d, "374"), zz: gv(d, "375"),
      },
      {
        name: "最近1个月查询机构数", dk: gv(d, "376"), xyk: gv(d, "377"),
        rz: gv(d, "378"), dbzg: gv(d, "379"),
      },
      {
        name: "最近3个月查询机构数", dk: gv(d, "380"), xyk: gv(d, "381"),
        rz: gv(d, "382"), dbzg: gv(d, "383"),
      },
      {
        name: "最近6个月查询机构数", dk: gv(d, "384"), xyk: gv(d, "385"),
        rz: gv(d, "386"), dbzg: gv(d, "387"),
      },
    ]
  }
  d.v差值情况 = {
    column: [
      { title: '时间', dataIndex: 'name', },
      { title: '贷款审批查询次数与近6个月放款账户数的差值', dataIndex: 'dk', },
      { title: '信用卡审批查询次数与近6个月信用卡开户账户数的差值', dataIndex: 'xyk', },
      { title: '融资审批查询次数与近6个月融资租赁业务类型放款账户数的差值', dataIndex: 'rz', },
      { title: '担保资格审批查询次数与近6个月相关还款责任人开户账户数的差值', dataIndex: 'dbzg', },
    ],
    val: [
      {
        name: "最近2-7个月", dk: gv(d, "388"), xyk: gv(d, "389"),
        rz: gv(d, "390"), dbzg: gv(d, "391"),
      },
    ]
  }
  d.v差值情况1 = {
    column: [
      { title: '时间', dataIndex: 'name', },
      { title: '贷款审批查询次数与近3个月放款账户数的差值', dataIndex: 'dk', },
      { title: '信用卡审批查询次数与近3个月信用卡开户账户数的差值', dataIndex: 'xyk', },
      { title: '融资审批查询次数与近3个月融资租赁业务类型放款账户数的差值', dataIndex: 'rz', },
      { title: '担保资格审批查询次数与近3个月相关还款责任人开户账户数的差值', dataIndex: 'dbzg', },
    ],
    val: [
      {
        name: "最近2-7个月", dk: gv(d, "392"), xyk: gv(d, "393"),
        rz: gv(d, "394"), dbzg: gv(d, "395"),
      },
    ]
  }
  d.v最近24个月 = {
    column: [
      { title: '时间', dataIndex: 'name', },
      { title: '展期账户数', dataIndex: 'zq', },
      { title: '担保人（第三方）代偿账户数', dataIndex: 'dbr', },
      { title: '以资抵债账户数', dataIndex: 'yzdz', },
      { title: '强制平仓，未结清账户数', dataIndex: 'qzw', },
      { title: '强制平仓，已结清账户数', dataIndex: 'qzy', },
      { title: '司法追偿账户数', dataIndex: 'sfzc', },
      { title: '其他账户数', dataIndex: 'qt', },
      { title: '债务减免账户数', dataIndex: 'zwjm', },
      { title: '资产剥离账户数', dataIndex: 'zcbl', },
      { title: '资产转让账户数', dataIndex: 'zczr', },
      { title: '信用卡个性化分期账户数', dataIndex: 'xyk', },
      { title: '银行主动延期账户数', dataIndex: 'yhk', },
      { title: '强制平仓账户数', dataIndex: 'qzz', },
    ],
    val: [
      {
        name: "最近24个月",
        zq: gv(d, "397"), dbr: gv(d, "398"),
        yzdz: gv(d, "399"), qzw: gv(d, "400"),
        qzy: gv(d, "401"), sfzc: gv(d, "402"),
        qt: gv(d, "403"), zwjm: gv(d, "404"),
        zcbl: gv(d, "405"), zczr: gv(d, "406"),
        xyk: gv(d, "407"), yhk: gv(d, "408"),
        qzz: gv(d, "409"),
      },
    ]
  }

  return d
}


export const getAuthorityFromRouter = (router = [], pathname) => {
  const authority = router.find(
    ({ routes, path = '/' }) =>
      (path && pathRegexp(path).exec(pathname)) ||
      (routes && getAuthorityFromRouter(routes, pathname)),
  );
  if (authority) return authority;
  return undefined;
};
export const getRouteAuthority = (path, routeData) => {
  let authorities;
  routeData.forEach(route => {
    // match prefix
    if (pathRegexp(`${route.path}/(.*)`).test(`${path}/`)) {
      if (route.authority) {
        authorities = route.authority;
      } // exact match

      if (route.path === path) {
        authorities = route.authority || authorities;
      } // get children authority recursively

      if (route.routes) {
        authorities = getRouteAuthority(path, route.routes) || authorities;
      }
    }
  });
  return authorities;
};
export function getTimeDistance(type) {
  const now = new Date();
  const oneDay = 1000 * 60 * 60 * 24;

  if (type === 'today') {
    now.setHours(0);
    now.setMinutes(0);
    now.setSeconds(0);
    return [moment(now), moment(now.getTime() + (oneDay - 1000))];
  }

  if (type === 'week') {
    let day = now.getDay();

    const beginTime = now.getTime();

    return [moment(beginTime - (6 * oneDay - 1000)), moment(beginTime)];
  }

  if (type === 'month') {
    let day = now.getDay();

    const beginTime = now.getTime();

    return [moment(beginTime - (30 * oneDay - 1000)), moment(beginTime)];
  }

  const year = now.getFullYear();
  return [moment(`${year}-01-01 00:00:00`), moment(`${year}-12-31 23:59:59`)];
}
export function onTableData(e) {
  if (!!e) {
    const customData =
      e === []
        ? []
        : e.map((item, sign) => {
          const newsItem = { ...item };
          const keys = sign + 1;
          newsItem.key = keys;
          return newsItem;
        });
    return customData;
  } else {
    return [];
  }
}
export function getParam(name) {
  const search = document.location.href;
  const pattern = new RegExp('[?&]' + name + '=([^&]+)', 'g');
  const matcher = pattern.exec(search);
  let items = null;
  if (matcher !== null) {
    try {
      items = decodeURIComponent(decodeURIComponent(matcher[1]));
    } catch (e) {
      try {
        items = decodeURIComponent(matcher[1]);
      } catch (e) {
        items = matcher[1];
      }
    }
  }
  return items;
}

export function goToRouter(dispatch, path) {
  dispatch(routerRedux.push(path));
}

export function downloadFile(href, filename = '') {
  if (href) {
    const download = document.createElement('a');
    download.download = filename;
    download.style.display = 'none';
    download.href = href;
    document.body.appendChild(download);
    download.click();
    document.body.removeChild(download);
  } else {
    throw Error('下载链接不正确');
  }
}
/**
 * 图片压缩
 * @param {object} file :图片文件信息
 * @param {string} width :宽
 * @param {string} height :高
 */

export const compression = file => {
  return new Promise(resolve => {
    const reader = new FileReader(); // 创建 FileReader
    reader.onload = ({ target: { result: src } }) => {
      const image = new Image(); // 创建 img 元素
      image.onload = async () => {
        const canvas = document.createElement('canvas'); // 创建 canvas 元素
        canvas.width = image.width;
        canvas.height = image.height;
        canvas.getContext('2d').drawImage(image, 0, 0, image.width, image.height); // 绘制 canvas
        const canvasURL = canvas.toDataURL('image/jpeg', 0.5);
        const buffer = atob(canvasURL.split(',')[1]);
        let length = buffer.length;
        const bufferArray = new Uint8Array(new ArrayBuffer(length));
        while (length--) {
          bufferArray[length] = buffer.charCodeAt(length);
        }
        const miniFile = new File([bufferArray], file.name, { type: 'image/jpeg' });
        resolve({
          file: miniFile,
          origin: file,
          beforeSrc: src,
          afterSrc: canvasURL,
          beforeKB: Number((file.size / 1024).toFixed(2)),
          afterKB: Number((miniFile.size / 1024).toFixed(2)),
        });
      };
      image.src = src;
    };
    reader.readAsDataURL(file);
  });
};

export const bfUploadFn = param => {
  // compression(param.file).then(res => {
  //   const serverURL = uploadUrl;
  //   const xhr = new XMLHttpRequest();
  //   const fd = new FormData();
  //   const successFn = () => {
  //     // 假设服务端直接返回文件上传后的地址
  //     // 上传成功后调用param.success并传入上传后的文件地址
  //     param.success({
  //       url: JSON.parse(xhr.responseText).data,
  //     });
  //   };

  //   const progressFn = event => {
  //     // 上传进度发生变化时调用param.progress
  //     param.progress((event.loaded / event.total) * 100);
  //   };

  //   const errorFn = () => {
  //     // 上传发生错误时调用param.error
  //     param.error({
  //       msg: 'unable to upload.',
  //     });
  //   };

  //   xhr.upload.addEventListener('progress', progressFn, false);
  //   xhr.addEventListener('load', successFn, false);
  //   xhr.addEventListener('error', errorFn, false);
  //   xhr.addEventListener('abort', errorFn, false);

  //   fd.append('file', res.file);
  //   xhr.open('POST', serverURL, true);
  //   xhr.setRequestHeader('token', getToken());
  //   xhr.send(fd);
  // });
  const serverURL = uploadUrl;
  const xhr = new XMLHttpRequest();
  const fd = new FormData();
  const successFn = () => {
    // 假设服务端直接返回文件上传后的地址
    // 上传成功后调用param.success并传入上传后的文件地址
    param.success({
      url: JSON.parse(xhr.responseText).data,
    });
  };

  const progressFn = event => {
    // 上传进度发生变化时调用param.progress
    param.progress((event.loaded / event.total) * 100);
  };

  const errorFn = () => {
    // 上传发生错误时调用param.error
    param.error({
      msg: 'unable to upload.',
    });
  };

  xhr.upload.addEventListener('progress', progressFn, false);
  xhr.addEventListener('load', successFn, false);
  xhr.addEventListener('error', errorFn, false);
  xhr.addEventListener('abort', errorFn, false);

  fd.append('file', param.file);
  xhr.open('POST', serverURL, true);
  xhr.setRequestHeader('token', getToken());
  xhr.send(fd);
};

/**
 * 从接口返回的数据中找出详细地址
 * @param {*} addressVO
 */
export const addressHandler = addressVO => {
  if (Object.prototype.toString.call(addressVO) !== '[object Object]') return '';
  const { provinceStr, cityStr, areaStr, street } = addressVO;

  if (!street) return `${provinceStr}${cityStr}${areaStr}`; // 街道信息不存在的话，那么街道信息的优先级放高
  let finalStr = street;
  if (!finalStr.includes(areaStr)) {
    finalStr = `${areaStr}${finalStr}`;
  }
  if (!finalStr.includes(cityStr)) {
    finalStr = `${cityStr}${finalStr}`;
  }
  if (!finalStr.includes(provinceStr)) {
    finalStr = `${provinceStr}${finalStr}`;
  }
  return finalStr;
};

// 做减法
export const makeSub = (arg1, arg2) => {
  let r1; // 第一个数字的小数点位数
  let r2; // 第二个数字的小数点位数
  let m;

  // 首先计算出两个数字的小数点位数
  try {
    r1 = arg1.toString().split('.')[1].length;
  } catch (e) {
    r1 = 0;
  }
  try {
    r2 = arg2.toString().split('.')[1].length;
  } catch (e) {
    r2 = 0;
  }

  const large = Math.max(r1, r2);
  m = Math.pow(10, large); // 根据最长的小数点为计算出所需要乘的倍数
  return ((arg1 * m - arg2 * m) / m).toFixed(large); // 变成整数后在进行相减操作，接着除以倍数，然后在保留指定小数位
};


/**
 * 渲染订单状态，如果订单已关闭的话，那么把关单原因也给展示出来
 * @param {*} record 
 */
export const renderOrderStatus = record => {
  if (!record || !record.status) return ""
  const { status } = record

  const orderStatusMap = {
    '01': '待支付',
    '02': '支付中',
    '03': '已支付申请关单',
    '11': '待审批',
    '04': '待发货',
    '05': '待确认收货',
    '06': '租用中',
    '07': '待结算',
    '08': '结算待支付',
    '09': '订单完成',
    '10': '交易关闭',
    '12': '待归还',
  };

  const orderCloseStatusMap = {
    '01': '未支付用户主动申请',
    '02': '支付失败',
    '03': '超时未支付',
    '04': '已支付用户主动申请',
    '05': '风控拒绝',
    '06': '商家关闭(客户要求)',
    '07': '商家风控关闭订单',
    '08': '商家超时发货',
    '09': '平台关闭订单',
    '11': '平台风控关闭订单',
  };

  if (status === "10") {
    return (
      <span>
        {orderStatusMap[status]}({orderCloseStatusMap[record.closeType]})
      </span>
    );
  }
  return <span>{orderStatusMap[status]}</span>
}
/**
 * 渲染订单状态，如果订单已关闭的话，那么把关单原因也给展示出来
 * @param {*} record 
 */
export const OrderStagesStatus = {
  '1': '待支付',
  '2': '已支付',
  '3': '逾期支付',
  '4': '逾期待支付',
  '5': '已取消',
  '6': '已结算',
  '7': '已退款',
  '8': '部分还款',
};
const qd = {
  "alipay": "支付宝小程序",
  "app": "app",
  "wechat": "微信"
}
export const getqd = (qd) => {
  if (!qd) return "支付宝小程序"
  return qd[qd]
}

export const ddzq = ["月付", "7天", "10天", "15天", "1天", "5天"]
export const dayMap = [1, 3, 7, 30, 84, 90, 120, 150, 180, 270, 365];

export const OrderStagesStatus1 = [
  { k: '1', v: '待支付', },
  { k: '2', v: '已支付', },
  { k: '3', v: '逾期支付', },
  { k: '4', v: '逾期待支付', },
  { k: '5', v: '已取消', },
  { k: '6', v: '已结算', },
  { k: '7', v: '已退款', },
  { k: '8', v: '部分还款', },
];
export const OrderStagesStatusColor = [
  { k: '1', v: 'magenta', },
  { k: '2', v: 'green', },
  { k: '3', v: 'blue', },
  { k: '4', v: 'magenta', },
  { k: '5', v: 'purple', },
  { k: '6', v: 'magenta', },
  { k: '7', v: 'magenta', },
  { k: '8', v: 'volcano', },
];

export const renderOrderStagesStatus = record => {
  if (!record || !record.status) return ""
  const { status } = record
  return <Tag >{OrderStagesStatus[status]}</Tag>
}

export const convertCurrency = money => {
  //汉字的数字
  let cnNums = new Array('零', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖');
  //基本单位
  let cnIntRadice = new Array('', '拾', '佰', '仟');
  //对应整数部分扩展单位
  let cnIntUnits = new Array('', '万', '亿', '兆');
  //对应小数部分单位
  let cnDecUnits = new Array('角', '分', '毫', '厘');
  //整数金额时后面跟的字符
  let cnInteger = '整';
  //整型完以后的单位
  let cnIntLast = '元';
  //最大处理的数字
  let maxNum = 999999999999999.9999;
  //金额整数部分
  let integerNum;
  //金额小数部分
  let decimalNum;
  //输出的中文金额字符串
  let chineseStr = '';
  //分离金额后用的数组，预定义
  let parts;
  if (money == '') {
    return '';
  }
  money = parseFloat(money);
  if (money >= maxNum) {
    //超出最大处理数字
    return '';
  }
  if (money == 0) {
    chineseStr = cnNums[0] + cnIntLast + cnInteger;
    return chineseStr;
  }
  //转换为字符串
  money = money.toString();
  if (money.indexOf('.') == -1) {
    integerNum = money;
    decimalNum = '';
  } else {
    parts = money.split('.');
    integerNum = parts[0];
    decimalNum = parts[1].substr(0, 4);
  }
  //获取整型部分转换
  if (parseInt(integerNum, 10) > 0) {
    let zeroCount = 0;
    let IntLen = integerNum.length;
    for (let i = 0; i < IntLen; i++) {
      let n = integerNum.substr(i, 1);
      let p = IntLen - i - 1;
      let q = p / 4;
      let m = p % 4;
      if (n == '0') {
        zeroCount++;
      } else {
        if (zeroCount > 0) {
          chineseStr += cnNums[0];
        }
        //归零
        zeroCount = 0;
        chineseStr += cnNums[parseInt(n)] + cnIntRadice[m];
      }
      if (m == 0 && zeroCount < 4) {
        chineseStr += cnIntUnits[q];
      }
    }
    chineseStr += cnIntLast;
  }
  //小数部分
  if (decimalNum != '') {
    let decLen = decimalNum.length;
    for (let i = 0; i < decLen; i++) {
      let n = decimalNum.substr(i, 1);
      if (n != '0') {
        chineseStr += cnNums[Number(n)] + cnDecUnits[i];
      }
    }
  }
  if (chineseStr == '') {
    chineseStr += cnNums[0] + cnIntLast + cnInteger;
  } else if (decimalNum == '') {
    chineseStr += cnInteger;
  }
  return chineseStr;
}

/**
 * 对antd timepicker所选择的时间做一个转换，接口所需的时间格式有些不同
 * @param {Object} originalData : 传参对象
 */
export function formatTime2ApiNeed(originalData) {
  if (Object.prototype.toString.call(originalData) !== "[object Object]") {
    console.error("入参必须为对象")
    return {}
  }

  const { times } = originalData
  if (times && times.length) { // 说明用户交互选中了时间
    const time1 = times[0] // 开始时间
    const time2 = times[1] // 结束时间
    time1 && (originalData.startTime = time1.format("YYYY-MM-DD 00:00:00"))
    time2 && (originalData.endTime = time2.format("YYYY-MM-DD 23:59:59"))
  }

  return originalData
}

/**
 * 根据设计稿所设计的尺寸进行响应式变化
 * @param {*} pxNum 
 * @returns 
 */
export const responsivePx = pxNum => {
  const designWidth = 1366
  const screenWidth = document.body.clientWidth
  const result = (screenWidth / designWidth) * pxNum
  return result
}

/**
 * 字符串的判空显示
 * @param {*} val 
 * @param {*} placeholder 
 * @returns 
 */
export const strUi = (val, placeholder = "-") => {
  if (val == undefined || val === "") return placeholder
  return val
}