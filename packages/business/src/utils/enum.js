export const orderStatusMap = {
  '01': '待支付',
  // '02': '支付中',
  // '03': '已支付申请关单',
  '11': '待审批',
  '04': '待发货',
  '05': '待确认收货',
  '06': '租用中',
  '07': '待结算',
  '08': '结算待支付',
  '09': '订单完成',
  '10': '交易关闭',
  '12': '待归还',
}

//审核状态
export const verfiyStatusMap = ["待审","通过","拒绝"]



export const orderCloseStatusMap = [
  {
    value: '01',
    content: '未支付用户主动申请',
  },
  {
    value: '02',
    content: '支付失败',
  },
  {
    value: '03',
    content: '超时支付',
  },
  {
    value: '04',
    content: '已支付用户主动申请',
  },
  {
    value: '05',
    content: '风控拒绝',
  },
  {
    value: '06',
    content: '商家关闭(客户要求)',
  },
  {
    value: '07',
    content: '商家风控关闭订单',
  },
  {
    value: '08',
    content: '商家超时发货',
  },
  {
    value: '09',
    content: '平台关闭订单',
  },
  {
    value: '11',
    content: '平台风控关闭订单',
  },
];


export const payStatus = {
  '1': '待支付',
  '2': '已支付',
  '3': '逾期已支付',
  '4': '逾期待支付',
  '5': '已取消',
  '6': '已结算',
  '7': '已退款,可用',
};


export const withShopStatus = {
  'UNPAID': '未支付',
  // 'DEALING': '支付中',
  'PAID': '已支付',
  'FAIL': '支付失败',
}

export const AuditStatus = {
  '00': '待审批',
  // 'DEALING': '支付中',
  '01': '审批通过',
  '02': '审批拒绝',
}

export const AuditReason = {
  '01': '法院涉案',
  '02': '纯白户',
  '03': '客户失联',
  '04': '不提供资料',
  '05': '多余订单',
  '06': '重新下单',
  '07': '客户不同意方案',
  '08': '终审拒绝',
  '09': '高炮过多',
   '10': '删除账单',
   '11': '在逾',
   '12': '黑名单',
   '13': '毙掉',
   '14': '年纪太小',
   '15': '年纪太大',
   '16': '还款能力弱',
   '17': '还款异常多',
}

export const PayMent={
  "ZFB":"支付宝",
  "VALET_PAYMENT":"代客支付",
  "MYL":"蚂蚁链扣款",
  "TTF":"统统付",
  "WX":"微信",
}

export const confirmSettlementType = {
  '01': '完好',
  '02': '损坏',
  '03': '丢失',
  '04': '违约',
};
export const confirmSettlementStatus = {
  '01': '待支付',
  '02': '待支付',
  '03': '已支付',
  '04': '待支付',
};

export const BuyOutEnum = [
  '不支持买断',
  '支持提前买断',
  '仅到期可买断'
]


export const defaultPlaceHolder = '-' // 统一的默认占位符，便于修改