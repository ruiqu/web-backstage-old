import React, { Component } from 'react';
import { Form, Icon, Input, Button, Checkbox, message } from 'antd';
import 'antd/dist/antd.css';
import styles from './index.less';
import Vcode from '@/components/Vcode';
import { router } from 'umi';
import { connect } from 'dva';
import UserService from '@/services/login'

@connect(() => ({}))
class LoginForm extends Component {
  state = {
    emailCode: '',
    vcode: '-1',
    code: '',
    isTip: true,
    isLogin: true
  };
  handleSubmit = e => {
    e.preventDefault();
    if (this.state.vcode === this.state.emailCode) {
      this.props.form.validateFields((err, values) => {
        if (!err) {
          const { dispatch } = this.props;
          dispatch({
            type: 'login/login',
            payload: {
              mobile: values.mobile,
              password: values.password,
              code: this.state.emailCode,
              userType: 'SHOP',
            },
          });
        }
      });
    }
  };
  
  handleSubmitRegister = e => {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        UserService.register({
          mobile: values.mobile,
          password: values.password,
          code: this.state.emailCode,
          userType: 'SHOP',
        }).then(res => {
          message.success('注册成功')
          this.goLogin()
        })
      }
    });
  };
  
  handleEmailInput(e) {
    this.setState(
      {
        emailCode: e.target.value,
      },
      () => {
        if (this.state.emailCode === this.state.vcode) {
          this.setState({
            isTip: true,
          });
        } else if (this.state.emailCode === '') {
          this.setState({
            isTip: true,
          });
        } else if (this.state.emailCode !== this.state.vcode) {
          this.setState({
            isTip: false,
          });
        }
      },
    );
  }
  
  onVcode(value) {
    if (value) {
      this.setState({
        vcode: value,
      });
    }
  }
  
  goRegister = () => {
    const { changeTitle, } = this.props
    changeTitle('注册')
    this.setState({
      isLogin: false
    });
  }
  
  goLogin = () => {
    const { changeTitle, } = this.props
    changeTitle('登录')
    this.setState({
      isLogin: true
    });
  }
  
  checkConfirm = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue('password')) {
      callback('两次输入的密码不匹配');
    } else {
      callback();
    }
  };
  
  renderRegister = () => {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form className="login-form">
        <div className={styles.loginInput}>
          <Form.Item>
            {getFieldDecorator('mobile', {
              rules: [
                { required: true, message: '请输入手机号' },
                {
                  pattern: /^1\d{10}$/,
                  message: '手机号格式错误！',
                },
                ],
            })(
              <Input
                prefix={<Icon type="mobile" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder="请输入手机号"
              />,
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('password', {
              rules: [
                { required: true, message: '请输入6 - 16 位密码，区分大小写' }
                ],
            })(
              <Input
                type="password"
                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder="请输入密码"
              />,
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('confirm', {
              rules: [
                { required: true, message: '请输入6 - 16 位密码，区分大小写' },
                { validator: this.checkConfirm,}
              ],
            })(
              <Input
                type="password"
                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder="请输入密码"
              />,
            )}
          </Form.Item>
          <div className="register-footer">
            <Button type="primary" className="register-footer-btn"
              onClick={this.handleSubmitRegister}
            >
              注册
            </Button>
            <a className="primary-color" onClick={this.goLogin}>使用已有帐户登录</a>
          </div>
        </div>
      </Form>
    )
  }
  
  renderLogin = () => {
    const { getFieldDecorator } = this.props.form;
    const { errorTips, isTip, isLogin } = this.state;
  
    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
        <div className={styles.loginInput}>
          <Form.Item>
            {getFieldDecorator('mobile', {
              rules: [{ required: true, message: '请输入手机号' }],
            })(
              <Input
                prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder="请输入手机号"
              />,
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('password', {
              rules: [{ required: true, message: '请输入密码' }],
            })(
              <Input
                type="password"
                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder="请输入密码"
              />,
            )}
          </Form.Item>
          <div className={styles.email}>
            <Form.Item hasFeedback={this.state.emailCode === this.state.vcode}>
              {getFieldDecorator('code', {
                rules: [{ required: true, message: '请输入验证码' }],
                onChange: e => this.handleEmailInput(e),
              })(
                <Input
                  style={{ width: '227px' }}
                  prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  placeholder="请输入验证码"
                  maxLength={4}
                />,
              )}
            </Form.Item>
            <Vcode
              onChange={value => this.onVcode(value)}
              value={this.state.code}
              width={128}
              height={40}
            />
            {isTip ? null : <p className={styles.tipText}>验证码输入不正确</p>}
          </div>
        </div>
    
        <Button
          type="link"
          style={{
            float: 'right',
            paddingRight: 70,
            cursor: 'pointer',
          }}
          onClick={() => router.push('/user/ChangePassword')}
        >
          修改密码
        </Button>
    
        <div className={styles.loginButton}>
          <Form.Item wrapperCol={{ span: 12, offset: 5 }}>
            <Button
              style={
                this.state.emailCode === this.state.vcode
                  ? null
                  : { backgroundColor: '#D3D3D3', border: 'none' }
              }
              className={styles.button}
              type="primary"
              htmlType="submit"
            >
              登录{' '}
            </Button>
            <a className="register" onClick={this.goRegister}>注册</a>
          </Form.Item>
        </div>
      </Form>
    )
  }
  
  
  render() {
    const { getFieldDecorator } = this.props.form;
    const { errorTips, isTip, isLogin } = this.state;
    return (
      <div>
        {isLogin ? this.renderLogin() : this.renderRegister()}
      </div>
    );
  }
}
const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(LoginForm);

export default WrappedNormalLoginForm;
