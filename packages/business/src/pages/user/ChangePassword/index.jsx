import React, { Component } from 'react';
import { router } from 'umi';
import styles from './index.less';
import Frame from '@/components/Frame';
import ResetForm from './components/ResetForm';
export default class ChangePassword extends Component {
  render() {
    return (
      <Frame
        frame={{
          width: 1014,
          height: 546,
          background: 'rgba(255,255,255,1)',
          borderRadius: 10,
        }}
      >
        <div className={styles.frames}>
          <p className={styles.ChangePassword}>修改密码</p>
          <ResetForm />
        </div>
      </Frame>
    );
  }
}
