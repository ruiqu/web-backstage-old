import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import {
  Card, Button, Form, Input, DatePicker, Spin, Select,
  Drawer, Tooltip, Modal, Divider, message, Descriptions,
} from 'antd';
const { TextArea } = Input;
import { getTimeDistance, renderOrderStatus, ddzq } from '@/utils/utils';
import MyPageTable1 from '@/components/MyPageTable1';
import { onTableData } from '@/utils/utils.js';
import CopyToClipboard from '@/components/CopyToClipboard';
import moment from 'moment';
const { RangePicker } = DatePicker;
import { exportCenterHandler } from '../util';
import MyStage from '@/components/MyStage';//探针
import orderService, { queryCreditReportByUid } from '@/services/order';  
import MyCs from '@/components/MyCs';//探针
import request from '../../../services/baseService';
import CustomCard from "@/components/CustomCard"

@connect(({ order, loading }) => ({
  ...order,
  loading: loading.effects['order/queryOpeOverDueOrdersByCondition'],
}))
@Form.create()
export default class BeOverdue extends Component {
  state = {
    current: 1,
    datas: {},
    stages: [],
    cslist: [],
    stagesShow1: false,//催收记录
    visible: false,
    orderVisible: null, channelList: [],
    stagesShow: false,
  };
  componentDidMount() {
    orderService.listBackstageUser1({sf:"cs"}).then(res => {
      if (res) {
        this.setState({
          cslist: res,
        })
      } 
    })

    this.props.dispatch({
      type: 'order/PlateformChannel',
    });
    this.onList(1, 30);
    request("/zyj-backstage-web/hzsx/business/channel/getChannelList", {
      pageNumber: 1,
      pageSize: 1000,
      islist: 1,
      unit: 0,
    }).then(res => {
      console.log("返回的信息是", res)
      res.records.unshift({ id: "000", name: "默认" })
      this.setState(
        {
          channelList: res.records,
        },
      );
    })
  }

  handleCancel = e => {
    this.setState({
      visible: false,
      orderVisible: false,
    });
  };

  onList = (pageNumber, pageSize, data = {}) => {
    const { dispatch } = this.props;
    /* request("/hzsx/business/order/queryOverDueOrdersByConditionNew",{
      pageSize,
      pageNumber,
      ...data,}).then(res=>{
        console.log("请求的数据:",res)
      }) */
    dispatch({
      type: 'order/queryOpeOverDueOrdersByCondition',
      payload: {
        pageSize,
        pageNumber,
        ...data,
      },
    });
  };
  seecs = (orderId) => {
    request(`/hzsx/ope/order/queryOrderHasten`, { orderId,source: "02",pageNumber:1,pageSize:100 }).then(res => {
      console.log("催收记录", res)
      this.setState(
        {
          stages: res.records,
          stagesShow1: true,
        },
      );
    })
  }
  onClose = () => {
    console.log("取消：")
    this.state.stagesShow = false
    this.setState({ stagesShow: false,stagesShow1: false })
  }
  showOrderModal = (type, orderId) => {
    console.log("按钮点击", type, orderId)
    this.setState({
      orderVisible: type,
      orderId
    });
  }
  renderAddMarkModal() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const { orderVisible, orderId } = this.state;
    const { getFieldDecorator } = this.props.form;
    const handleOk = e => {
      e.preventDefault();
      this.props.form.validateFields(['shopRemark'], (err, values) => {
        if (!err) {
          console.log("orderid:", orderId, values)
          values.orderId = orderId

          request(`/hzsx/business/order/addShopMark`, values).then(res => {
            console.log("rcs", res)
            this.setState({
              visible: false,
              orderVisible: false,
            });
            this.props.form.resetFields();
            //this.handleReset()
            message.success("添加完成", 5)
          })
        }
      });
    };

    return (
      <div>
        <Modal
          title="添加备注"
          visible={orderVisible === 'addmark'}
          onOk={handleOk}
          onCancel={this.handleCancel}
        >
          <Form>
            <Form.Item label="备注" {...formItemLayout}>
              {getFieldDecorator('shopRemark', {
                rules: [{ required: true, message: '请输入备注' }],
              })(<TextArea placeholder="请输入" />)}
            </Form.Item>
          </Form>
        </Modal>
      </div>
    );
  }
  renderCollectionRecordModal() {
    const { orderVisible, orderId } = this.state;
    const { getFieldDecorator } = this.props.form;

    const handleOk = e => {
      e.preventDefault();
      this.props.form.validateFields(['jg', 'xj'], (err, values) => {
        if (!err) {
          orderService.orderHasten({ orderId, notes: values.xj, result: values.jg }).then(res => {
            message.success("添加催收记录成功")
            this.handleCancel();
          });
        }
      });
    };
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    return (
      <Modal
        title="记录催收"
        visible={orderVisible === 'remarks'}
        onOk={handleOk}
        onCancel={this.handleCancel}
        destroyOnClose
      >
        <Form>
          <Form.Item label="结果" {...formItemLayout}>
            {getFieldDecorator('jg', {
              rules: [{ required: true, message: '请选择结果' }],
            })(
              <Select style={{ width: '100%' }} placeholder="请选择结果">
                <Option value="01">承诺还款</Option>
                <Option value="02">申请延期还款</Option>
                <Option value="03">拒绝还款</Option>
                <Option value="04">电话无人接听</Option>
                <Option value="05">电话拒接</Option>
                <Option value="06">电话关机</Option>
                <Option value="07">电话停机</Option>
                <Option value="08">客户失联</Option>
              </Select>,
            )}
          </Form.Item>
          <Form.Item label="小记" {...formItemLayout}>
            {getFieldDecorator('xj', {
              rules: [{ required: true, message: '请输入小记' }],
            })(<TextArea placeholder="请输入" />)}
          </Form.Item>
        </Form>
      </Modal>
    );
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      console.log('Received values of form: ', values);
      if (values.createDate && values.createDate.length < 1) {
        values.createTimeStart = '';
        values.createTimeEnd = '';
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList(1, 30, { ...values });
          },
        );
      } else if (values.createDate) {
        values.createTimeStart = moment(values.createDate[0]).format('YYYY-MM-DD HH:mm:ss');
        values.createTimeEnd = moment(values.createDate[1]).format('YYYY-MM-DD HH:mm:ss');
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList(1, 30, { ...values });
          },
        );
      } else {
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList(1, 30, { ...values });
          },
        );
      }
    });
  };
  //table 页数
  onPage = e => {
    this.setState(
      {
        current: e.current,
      },
      () => {
        this.onList(e.current, 30, this.state.datas);
      },
    );
  };
  // 重置
  handleReset = e => {
    this.props.form.resetFields();
    this.handleSubmit(e);
  };
  seeStage = (orderId) => {
    request(`/hzsx/business/order/queryOrderStagesDetail`, { orderId }).then(res => {
      console.log("sjrh：", res.orderByStagesDtoList)
      this.setState(
        {
          stages: res.orderByStagesDtoList,
          stagesShow: true,
        },
      );
    })
  }
  fp = () => {
    console.log("分配给催收的订单是", this.state.selectOrderItem)
    this.MyPageTable1.childFn();
  }
  confirm = () => { };
  onexport = () => {
    //要添加时间字段
    exportCenterHandler(this, 'exportOverdueOrder', false, false);

  };
  render() {
    const { conditionList, conditionTotal, PlateformList, loading, mydb } = this.props;
    console.log("aaaa:mydb", mydb)
    let orderByStagesDtoList = this.state.stages

    const gettext = (val, isred) => {
      if (val == "000") val = "默认"
      //<p className="green-status">{e.realName}</p>
      //if (isred == 1) return (<p className="red-status">{val}</p>)
      return (<p >{val}</p>)

    }
    const { cost, yj, payedRent, allRent, page, v到期金额, v逾期金额, } = mydb;
    let v逾期率 = ((v逾期金额 / v到期金额) * 100).toFixed(2) + "%"
    console.log("page", page)
    //差值cz(成本-已付押金-已付租金)
    //差值是 已付押金+已付租金-成本
    //应收（总租金-已付租金
    let cz = (yj + payedRent - cost).toFixed(2)
    let lr = (allRent - payedRent).toFixed(2)
    const paginationProps = {
      current: this.state.current,
      pageSize: 30,
      total: conditionTotal,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / 30)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };

    const columnsByStagesStute = {
      '1': '待支付',
      '2': '已支付',
      '3': '逾期已支付',
      '4': '逾期待支付',
      '5': '已取消',
      '6': '已结算',
      '7': '已退款,可用',
      '8': '部分还款',
    };

    const columns = [
      {
        title: '订单编号',
        dataIndex: 'orderId',
        render: orderId => {
          return <>
            <CopyToClipboard text={orderId} ></CopyToClipboard>
          </>
        },
        width: '120px',
      },
      {
        title: '渠道来源',
        width: 120,
        render: e => (gettext(e.channelName, e.isRed)),
      },
      {
        title: '催收',
        width: 120,
        dataIndex: 'cname',
      },
      {
        title: '姓名/手机号',
        width: 140,
        render: e => {
          return <>
            <div>
              {e && e.isVip && e.isVip == 1 ? (
                <p className="green-status">{e.realName}</p>
              ) : (
                <p >{e.realName}</p>
              )}
              <p>{e.telephone}</p>
            </div>
          </>;
        },
      },
      {
        title: '身份证',
        width: 180,
        dataIndex: 'sfz',
      },
      {
        title: '商品名称',
        //dataIndex: 'productName',
        width: 160,
        render: e => gettext(e.productName + e.skuTitle, e.isRed)
      },

      {
        title: '订单周期',
        //dataIndex: 'isWeek',
        width: 120,
        render: e => gettext(ddzq[e.isWeek], e.isRed)
      },
      {
        title: '已付期数',
        width: 110,
        render: e => {
          return (
            <>
              <Tooltip placement="top" title={e.currentPeriods}>
                <div
                  style={{
                    width: 60,
                  }}
                >
                  <p>
                    {e.payedPeriods}/{e.totalPeriods + 1}
                  </p>
                  <a onClick={() => {
                    this.seeStage(e.orderId)
                  }}>查看</a>
                </div>
              </Tooltip>
            </>
          );
        },
      },
      {
        title: '已付租金',
        width: 90,
        render: e => {
          return <>
            <div >
              <p style={{ display: 'flex', 'align-items': 'center', 'justify-content': 'center' }}>{e.payedRentAmount}</p>
              <p style={{ display: 'flex', 'align-items': 'center', 'justify-content': 'center' }}>{e.totalRentAmount}</p>
            </div>
          </>;
        },
      },
      {
        title: '逾期时间',
        width: 120,
        render: e => {
          if (e.v最多逾期天数) {
            return <>
              <div >
                <p >{(e.v最早逾期时间).substring(0, 10)}</p>
                <p >逾期{e.v最多逾期天数}天</p>
              </div>
            </>;
          } else {
            return "逾期已支付"
          }
        },
      },
      {
        title: '已付押金',
        dataIndex: 'yj',
        width: 120,
      },
      {
        title: '成本',
        dataIndex: 'costPrice',
        width: 120,
      },
      {
        title: '差值',
        dataIndex: 'cz',
        width: 120,
        render: (_, i) => {
          let cb = i.costPrice || 0  //成本
          let yj = i.yj || 0  //已付押金
          let zj = i.payedRentAmount || 0  //已付押金
          //差值cz(成本-已付押金-已付租金)
          let yfzj = i.payedRentAmount  //已付租金
          let cz = zj + yj - cb
          //console.log("差值",i)
          return cz.toFixed(2)
        },
      },
      {
        title: '应收',
        dataIndex: 'lr',
        width: 120,
        render: (_, i) => {
          let cb = i.costPrice || 0  //成本
          let zzj = i.totalRentAmount  //总金额
          let yfzj = i.payedRentAmount  //已付租金
          //利润（总租金-成本）
          //console.log("利润（总租金-成本）", i)
          return (zzj - yfzj).toFixed(2)
        },
      },
      {
        title: '审核',
        dataIndex: 'rname',
        width: 100,
      },
      {
        title: '马云',
        width: 80,
        render: e => {
          if (e.shopRemark) {
            return (<>
              <Tooltip placement="top" title={e.shopRemark}>
                <div
                  style={{
                    width: 60,
                  }}
                >
                  <a >查看</a>
                </div>
              </Tooltip>
            </>)
          }
        }
      },
      {
        title: '下单时间',
        dataIndex: 'placeOrderTime',
        width: 120,
      },
      /* {
        title: '订单状态',
        dataIndex: 'status',
        width: 120,
        render: (_, record) => renderOrderStatus(record),
      }, */

      {
        title: '操作',
        width: 120,
        fixed: 'right',
        align: 'center',
        render: (e, record) => {
          return (
            <div>
              <a
                className="primary-color"
                // onClick={() => router.push(`/Order/Details?id=${e.orderId}`)}
                href={`#/Order/HomePage/Details?id=${e.orderId}`}
                target="_blank"
              >
                处理
              </a>
              <Divider type="vertical" />
              <a
                className="primary-color"
                onClick={() => {
                  this.showOrderModal("addmark", e.orderId)
                }}
              >
                马云
              </a>
              <Divider type="vertical" />
              <a onClick={() => {
                this.seecs(e.orderId)
              }}>查看</a>
              <Divider type="vertical" />
              <a
                className="primary-color"
                onClick={() => {
                  this.showOrderModal("remarks", record.orderId)
                }}
              >
                记录催收
              </a>
              <Divider type="vertical" />
            </div>
          );
        },
      },
    ];
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const columnsByStages = [
      {
        title: '总期数',
        dataIndex: 'totalPeriods',
      },
      {
        title: '当前期数',
        dataIndex: 'currentPeriods',
      },
      {
        title: '租金',
        dataIndex: 'currentPeriodsRent',
      },
      {
        title: '状态',
        render: e => <>
          <span>
            {columnsByStagesStute[e.status]}
          </span>
        </>
        ,
      },
      {
        title: '支付时间',
        dataIndex: 'repaymentDate',
      },
      {
        title: '账单到期时间',
        dataIndex: 'statementDate',
      },
    ];

    return (
      <PageHeaderWrapper title={false}>
        <Drawer
          width={620}
          title='账单信息'
          placement="right"
          onClose={this.onClose}
          visible={this.state.stagesShow}>
          <MyStage orderByStagesDtoList={orderByStagesDtoList}></MyStage>
        </Drawer>
        <Drawer
          width={620}
          title='催收记录'
          placement="right"
          onClose={this.onClose}
          visible={this.state.stagesShow1}>
          <MyCs orderByStagesDtoList={orderByStagesDtoList}></MyCs>
        </Drawer>
        {this.renderAddMarkModal()}
        {this.renderCollectionRecordModal()}

        <Card bordered={false} className='ant-card antd-pro-pages-finance-capital-account-index-mb20'>
          <div style={{ fontSize: 20, marginBottom: 20 }}>
            <strong>统计信息</strong><img src='https://rich-rent.oss-cn-hangzhou.aliyuncs.com/icon/椭圆形 2.png'></img>
          </div>
          <Descriptions column={4}>
            <Descriptions.Item label="差额">{cz}</Descriptions.Item>
            <Descriptions.Item label="应收">{lr}</Descriptions.Item>
            <Descriptions.Item label="总租金">{allRent}</Descriptions.Item>
            <Descriptions.Item label="已付租金">{payedRent}</Descriptions.Item>
            <Descriptions.Item label="总押金">{yj}</Descriptions.Item>
            <Descriptions.Item label="逾期金额">{v逾期金额}</Descriptions.Item>
            <Descriptions.Item label="到期金额">{v到期金额}</Descriptions.Item>
            <Descriptions.Item label="逾期率">{v逾期率}</Descriptions.Item>
          </Descriptions>
        </Card>
        <Card bordered={false}>
          <Form layout="inline" onSubmit={this.handleSubmit}>
            <Form.Item label="商品名称">
              {getFieldDecorator('productName', {})(<Input allowClear placeholder="请输入商品名称" />)}
            </Form.Item>
            <Form.Item label="下单人姓名">
              {getFieldDecorator('userName', {})(<Input allowClear placeholder="请输入下单人姓名" />)}
            </Form.Item>
            <Form.Item label="下单人手机号">
              {getFieldDecorator('telephone', {})(<Input allowClear placeholder="请输入下单人手机号" />)}
            </Form.Item>
            <Form.Item label="订单编号">
              {getFieldDecorator('orderId', {})(<Input allowClear placeholder="请输入订单编号" />)}
            </Form.Item>
            <Form.Item label="订单周期">
              {getFieldDecorator(
                'unit',
                {},
              )(
                 <Select style={{ width: 180 }} allowClear placeholder="请选择">
                  {ddzq.map((value, i) => {
                    return (
                      <Option value={i} key={i}>
                        {value}
                      </Option>
                    );
                  })}
                </Select>,
              )}
            </Form.Item>
            <Form.Item label="渠道来源">
              {getFieldDecorator('channelId', {
                // initialValue: '',
              })(
                <Select
                  placeholder="渠道来源"
                  allowClear
                  style={{ width: 180 }}
                  onChange={this.onChangesChannel}
                >
                  {this.state.channelList.map(value => {
                    return (
                      <Option value={value.id} key={value.id}>
                        {value.name}
                      </Option>
                    );
                  })}
                </Select>,
              )}
            </Form.Item>
            <Form.Item label="催收筛选">
              {getFieldDecorator('cid', {
                // initialValue: '',
              })(
                <Select
                  placeholder="催收筛选"
                  allowClear
                  style={{ width: 180 }}
                  onChange={this.onChangesChannel}
                >
                  {this.state.cslist.map(value => {
                    return (
                      <Option value={value.id} key={value.id}>
                        {value.name}
                      </Option>
                    );
                  })}
                </Select>,
              )}
            </Form.Item>
            <Form.Item label="创建时间">{getFieldDecorator('createDate', {})(<RangePicker allowClear />)}</Form.Item>
            <div>
              <Form.Item>
                <Button type="primary" htmlType="submit">
                  查询
                </Button>
              </Form.Item>
              <Form.Item>
                <Button htmlType="button" onClick={this.handleReset}>
                  重置
                </Button>
              </Form.Item>
              <Form.Item>
                <Button onClick={this.onexport}>导出</Button>
              </Form.Item>
              <Form.Item>
                <Button htmlType="button" onClick={this.fp}>
                  分配催收
                </Button>
              </Form.Item>
            </div>
          </Form>
          <Spin spinning={loading}>
            {/* <MyPageTable1
              scroll={true}
              onPage={this.onPage}
              paginationProps={paginationProps}
              dataSource={onTableData(conditionList)}
              columns={columns}
            /> */}
            <MyPageTable1
              scroll={true}
              onPage={this.onPage}
              ref={node => this.MyPageTable1 = node}
              paginationProps={paginationProps}
              dataSource={onTableData(conditionList)}
              columns={columns}
            />
          </Spin>
        </Card>
      </PageHeaderWrapper>
    );
  }
}
