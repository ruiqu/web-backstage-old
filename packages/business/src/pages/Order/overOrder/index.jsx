import React, { Component,useRef  } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import {
  Card, Tooltip, Button, Form, Input, Select, DatePicker, Spin, Row,
  Col, Divider, Modal, Drawer,
  InputNumber, Descriptions,
  message,
} from 'antd';
import MyPageTable1 from '@/components/MyPageTable1';
import { onTableData } from '@/utils/utils.js';
import CustomCard from "@/components/CustomCard"
import MyStage from '@/components/MyStage';//探针
import MyCs from '@/components/MyCs';//探针
import CopyToClipboard from '@/components/CopyToClipboard';
import AntTable from '@/components/AntTable';
import orderService, { queryCreditReportByUid } from '@/services/order';
import { getTimeDistance, renderOrderStagesStatus, OrderStagesStatus, OrderStagesStatus1, ddzq } from '@/utils/utils';
import moment from 'moment';
const { Option } = Select;
const { TextArea } = Input;
const { RangePicker } = DatePicker;
import { routerRedux } from 'dva/router';
import { getCurrentUser } from '@/utils/localStorage';
import request from '../../../services/baseService';
import { orderCloseStatusMap, orderStatusMap } from '@/utils/enum';
import { exportCenterHandler } from '../util';
import { UserRating } from 'zwzshared';
@connect(({ order, loading }) => ({
  ...order,
  loading: loading.effects['order/queryOpeOrderByConditionList'],
}))
@Form.create()
export default class HomePage extends Component {
  state = {
    current: 1,
    visible: '',
    orderVisible: '',
    alldb: {},
    stages: [],
    channelList: [],
    selectOrderItem: [],
    stagesShow: false,
    stagesShow1: false,//催收记录
    isexp: 0,
    load: false,
    orderId: null,
    cday: null,
    cslist:[],
    mydata: {},
    createTimeStart: null,
    createTimeEnd: null,
    datas: {},
    record: {},
    mydb: [],
    expressList: [],
    yunTime: getTimeDistance('month'),
    gBtusua: '04', // 默认状态: 待发货
  };


  showOrderModal = (type, orderId) => {
    console.log("按钮点击", type, orderId)
    this.setState({
      orderVisible: type,
      orderId
    });
  }
  handleCancel = e => {
    this.setState({
      visible: false,
      orderVisible: false,
    });
  };

  gettime = e => {

  }


  componentDidMount() {
    const { status } = this.props.location.query;
    let d = new Date();
    let y = d.getMonth() + 1
    let ymd = d.getFullYear() + "-"
    if (y < 10) ymd += "0"
    ymd += y + "-"
    let dd = d.getDate()
    if (dd < 10) ymd += "0"
    ymd += dd
    let createTimeEnd = ymd + " 23:59:59"
    let createTimeStart = ymd + " 00:00:00"
    console.log("dd:", ymd, createTimeStart, createTimeEnd)
    this.state.createTimeStart = createTimeStart
    this.state.createTimeEnd = createTimeEnd

    orderService.listBackstageUser1({sf:"cs"}).then(res => {
      if (res) {
        this.setState({
          cslist: res,
        })
      } 
    })

    if (status) {
      this.setState(
        {
          gBtusua: status,
          load: true,
        },
        () => {
          this.props.form.setFieldsValue({
            status,
          });
          this.onList(1, 30, {
            status: this.state.gBtusua,
          });
          // this.getExpressList();
        },
      );
    } else {
      this.onList(1, 30);
      // this.getExpressList();
    }

    this.props.dispatch({
      type: 'order/PlateformChannel',
    });

    console.log("渠道获取列表")
    request("/zyj-backstage-web/hzsx/business/channel/getChannelList", {
      pageNumber: 1, islist: 1,
      pageSize: 1000
    }).then(res => {
      console.log("返回的信息是", res)
      res.records.unshift({ id: "000", name: "默认" })
      this.setState(
        {
          channelList: res.records,
        },
      );
    })
  }
  onList = (pageNumber, pageSize, data = {}) => {
    const { createTimeStart, createTimeEnd, isexp } = this.state
    console.log("data：：：", data, createTimeStart, createTimeEnd, this.state)
    data.createTimeStart = createTimeStart
    data.isexp = isexp
    data.createTimeEnd = createTimeEnd
    this.setState({
      load: true,
    })
    request("/zyj-backstage-web/hzsx/ope/order/queryOverOrderByStagesNew", {
      pageSize,
      pageNumber,
      ...data,
    }).then(res => {
      console.log("aaaa:", res)
      this.setState(
        {
          mydata: res.page,
          alldb: res,
          load: false,
        },
      );
    })
    const { dispatch } = this.props;
    /* dispatch({
      type: 'order/queryOpeOrderByConditionList',
      payload: {
        pageSize,
        pageNumber,
        iszy: 1,
        ...data,
      },
    }); */
  };
  //计算已经支付的金额
  jsyfu = e => {
    console.log("eeeee计算已付金额", e)
    //已还款
    let price = 0
    if (e.status == 2) price = e.currentPeriodsRent
    //部分还款
    if (e.status == 8) price = e.beforePrice
    return <>{price}</>
  }
  //导出到期账单
  expOrd = e => {
    e.preventDefault();
    this.props.form.validateFields(['userName', 'channelId'], (err, values) => {
      if (!err) {
        let db = {
          createStart: this.state.createTimeStart,
          createEnd: this.state.createTimeEnd,
          isover: "1",
          tp:"isover",
        }
        if (values.userName) db.userName = values.userName
        if (values.channelId) db.channelId = values.channelId
        request("/hzsx/export/rentOrder", db, "post").then(res => {
          console.log("订单导出")
          message.success("导出完成", 5)
        })
        console.log("eeee", db)
      }
    });

  }
  fp=()=>{
    console.log("分配给催收的订单是",this.state.selectOrderItem)
    this.MyPageTable1.childFn();
  }
  // 重置
  handleReset = e => {
    this.state.isexp = 0
    this.setState(
      {
        gBtusua: '',
      },
      () => {
        this.props.form.resetFields();
        this.props.form.setFieldsValue({
          status: undefined,
        });
        this.handleSubmit(e);
      },
    );
  };

  toSendSms = (type, orderId, currentPeriods) => {
    console.log("按钮点击进入短信发送", type, orderId)
    this.setState({
      load: true,
    })
    request(`/hzsx/business/order/toSendSms`, { mold: type, orderId, currentPeriods }).then(res => {
      console.log("rcs", res)
      this.setState({
        load: false,
      })
      message.success("发送完成", 5)
    }).catch(e => {
      this.setState({
        load: false,
      })
      console.log(e, "eeeee")
    })
  }



  renderCollectionRecordModal() {
    const { orderVisible, orderId } = this.state;
    const { getFieldDecorator } = this.props.form;

    const handleOk = e => {
      e.preventDefault();
      this.props.form.validateFields(['jg', 'xj'], (err, values) => {
        if (!err) {
          orderService.orderHasten({ orderId, notes: values.xj, result: values.jg }).then(res => {
            message.success("添加催收记录成功")
            this.handleCancel();
          });
        }
      });
    };
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    return (
      <Modal
        title="记录催收"
        visible={orderVisible === 'remarks'}
        onOk={handleOk}
        onCancel={this.handleCancel}
        destroyOnClose
      >
        <Form>
          <Form.Item label="结果" {...formItemLayout}>
            {getFieldDecorator('jg', {
              rules: [{ required: true, message: '请选择结果' }],
            })(
              <Select style={{ width: '100%' }} placeholder="请选择结果">
                <Option value="01">承诺还款</Option>
                <Option value="02">申请延期还款</Option>
                <Option value="03">拒绝还款</Option>
                <Option value="04">电话无人接听</Option>
                <Option value="05">电话拒接</Option>
                <Option value="06">电话关机</Option>
                <Option value="07">电话停机</Option>
                <Option value="08">客户失联</Option>
              </Select>,
            )}
          </Form.Item>
          <Form.Item label="小记" {...formItemLayout}>
            {getFieldDecorator('xj', {
              rules: [{ required: true, message: '请输入小记' }],
            })(<TextArea placeholder="请输入" />)}
          </Form.Item>
        </Form>
      </Modal>
    );
  }

  handleSubmit = e => {
    e && e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (values.createDate && values.createDate.length < 1) {
        values.createTimeStart = '';
        values.createTimeEnd = '';
        this.setState(
          {
            datas: { ...values },
            current: 1,
            isexp: 0,
          },
          () => {
            this.onList(1, 30, { ...values });
          },
        );
      } else if (values.createDate) {
        values.createTimeStart = moment(values.createDate[0]).format('YYYY-MM-DD');
        values.createTimeStart += " 00:00:00"
        values.createTimeEnd = moment(values.createDate[1]).format('YYYY-MM-DD');
        values.createTimeEnd += " 23:59:59"
        this.setState(
          {
            datas: { ...values },
            current: 1,
            isexp: 0,
          },
          () => {
            this.onList(1, 30, { ...values });
          },
        );
      } else {
        this.setState(
          {
            datas: { ...values },
            current: 1,
            isexp: 0,
          },
          () => {
            this.onList(1, 30, { ...values });
          },
        );
      }
    });
  };

  onexport = () => {
    exportCenterHandler(this, 'exportRentOrder', true, false);
  };

  handleCancel = e => {
    this.setState({
      visible: false,
      orderVisible: false,
    });
  };
  seeStage = (orderId) => {
    request(`/hzsx/business/order/queryOrderStagesDetail`, { orderId }).then(res => {
      console.log("sjrh：", res.orderByStagesDtoList)
      this.setState(
        {
          stages: res.orderByStagesDtoList,
          stagesShow: true,
        },
      );
    })
  }
  seecs = (orderId) => {
    request(`/hzsx/ope/order/queryOrderHasten`, { orderId,source: "02",pageNumber:1,pageSize:100 }).then(res => {
      console.log("催收记录", res)
      this.setState(
        {
          stages: res.records,
          stagesShow1: true,
        },
      );
    })
  }
  onClose = () => {
    console.log("取消：")
    this.state.stagesShow = false
    this.setState({ stagesShow: false,stagesShow1: false })
  }
  //table 页数
  onPage = e => {
    this.setState(
      {
        current: e.current,
      },
      () => {
        const params = {
          ...this.state.datas,
        }
        this.onList(e.current, 30, params);
      },
    );
  };

  onChanges = e => {
    this.setState({
      gBtusua: e,
    });
  };
  onChange = e => {
    let values = {};
    console.log("选择了时间", e)
    let createTimeStart = moment(e[0]).format('YYYY-MM-DD');
    createTimeStart += " 00:00:00"
    let createTimeEnd = moment(e[1]).format('YYYY-MM-DD');
    createTimeEnd += " 23:59:59"
    this.state.createTimeStart = createTimeStart
    this.state.createTimeEnd = createTimeEnd
  }

  // 认领订单
  lingqu = () => {
    // console.log("user",getCurrentUser());
    this.props.dispatch(
      routerRedux.push({
        pathname: `/Order/HomePage/ReceiveOrder`,
      })
    );
  }
  clickday = (key) => {
    let db = {}
    var now = new Date();
    var date = now.getDate();
    //now.setDate(date + 1);  //获取后一天
    now.setDate(date + key);  //获取前一天
    var y = now.getFullYear();
    var m = (now.getMonth() + 1).toString().padStart(2, "0");
    var d = now.getDate().toString().padStart(2, "0");
    var ymd = y + "-" + m + "-" + d;
    let createTimeEnd = ymd + " 23:59:59"
    let createTimeStart = ymd + " 00:00:00"
    this.state.createTimeStart = createTimeStart
    this.state.createTimeEnd = createTimeEnd
    this.state.cday = key

    console.log("点了时间", db)
    //this.onList(1, 30, db);
    //获取时间
  }
  render() {
    const { load, mydata, cday, alldb } = this.state;
    const { loading, form, mydb } = this.props;
    //console.log("page",page)
    let allTotal = mydata?.total
    let allList = mydata?.records
    //差值cz(成本-已付押金-已付租金)
    //差值是 已付押金+已付租金-成本
    //应收（总租金-已付租金

    //console.log("mydata", mydata)
    //console.log("allList", allList)
    const paginationProps = {
      current: this.state.current,
      pageSize: 30,
      total: allTotal,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / 30)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };

    const user = getCurrentUser();
    const columns = [
      {
        title: '订单编号',
        dataIndex: 'orderId',
        width: 130,
        render: (text, record, index) => {
          return (
            <>
              <span style={{ color: "red" }}>{record.children == null ? null : `(共${record.children.length + 1}单) `}</span>
              <span>{text}</span>
            </>
          )
        }
      },

      {
        title: '渠道来源',
        dataIndex: 'channelId',
        width: 120,
      },
      {
        title: '审核',
        dataIndex: 'rname',
        width: 100,
      },
      {
        title: '催收',
        dataIndex: 'cname',
        width: 100,
      },

      {
        title: '姓名/手机号',
        width: 140,
        render: e => {
          return <>
            <div>
              {e && e.isVip && e.isVip == 1 ? (
                <p className="green-status">{e.name}</p>
              ) : (
                <p >{e.name}</p>
              )}
              <p>{e.phone}</p>
            </div>
          </>;
        },
      },
      {
        title: '期数/总期数',
        width: 120,
        render: e => {
          return <>
            <div>
              <p>
                {e.currentPeriods}/{e.totalPeriods}
              </p>
              <a onClick={() => {
                this.seeStage(e.orderId)
              }}>查看</a>
            </div>
          </>;
        },
      },
      {
        title: '订单周期',
        dataIndex: 'isWeek',
        width: 120,
        render: (text, record, index) => {
          return <>{ddzq[text]}</>;
        },
      },
      {
        title: '当前租金',
        dataIndex: 'currentPeriodsRent',
        width: 90,
      },
      {
        title: '已付租金',
        width: 120,
        render: (e, record) => {
          //this.jsyfu(e)
          let price = 0
          if (e.status == 2) price = e.currentPeriodsRent
          if (e.status == 3) price = e.currentPeriodsRent
          if (e.status == 8) price = e.beforePrice //部分还款
          return <>{price}</>
        },
      },
      /* {
        title: '总租金',
        dataIndex: 'totalRent',
        width: 80,
      }, */
      {
        title: '已付/总租金',
        width: 140,
        render: e => {
          return <>
            <div>
            <p >{e.v已付租金}</p>
              <p>{e.totalRent}</p>
            </div>
          </>;
        },
      },
      {
        title: '账单状态',
        dataIndex: 'status',
        width: 120,
        render: (_, record) => renderOrderStagesStatus(record),
      },

      {
        title: '还款日',
        dataIndex: 'repaymentDate',
        width: 130,

      },
      {
        title: '账单日',
        dataIndex: 'statementDate',
        width: 130,

      },

      {
        title: '提前提醒',
        width: 120,
        render: (e, record) => {
          if (e.istq != 1) {
            return (
              <>
                <a
                  className="primary-color"
                  onClick={() => {
                    this.toSendSms("tq", e.orderId, e.currentPeriods)
                  }}
                >
                  提前提醒
                </a>
              </>
            )
          } else {
            return '已提醒'
          }
        },
      },
      {
        title: '到期提醒',
        width: 120,
        render: (e, record) => {
          if (e.isdq != 1)
            return (
              <>
                <a
                  className="primary-color"
                  onClick={() => {
                    this.toSendSms("dq", e.orderId, e.currentPeriods)
                  }}
                >
                  到期提醒
                </a>
              </>
            )
          return '已提醒'
        },
      },
      {
        title: '操作',
        width: 160,
        fixed: 'right',
        align: 'center',
        render: (e, record) => {
          return (
            <div>
              <a onClick={() => {
                this.seecs(e.orderId)
              }}>查看</a>
              <Divider type="vertical" />
              <a
                className="primary-color"
                // onClick={() => router.push(`/Order/Details?id=${e.orderId}`)}
                href={`#/Order/HomePage/Details?id=${e.orderId}`}
                target="_blank"
              >
                处理
              </a>
              <Divider type="vertical" />
              <a
                className="primary-color"
                onClick={() => {
                  this.showOrderModal("remarks", record.orderId)
                }}
              >
                记录催收
              </a>

            </div>
          );
        },
      },
    ];
    const columnsByStagesStute = {
      '1': '待支付',
      '2': '已支付',
      '3': '逾期已支付',
      '4': '逾期待支付',
      '5': '已取消',
      '6': '已结算',
      '7': '已退款,可用',
      '8': '部分还款',
    };
    const { getFieldDecorator } = form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const columnsByStages = [
      {
        title: '总期数',
        dataIndex: 'totalPeriods',
      },
      {
        title: '当前期数',
        dataIndex: 'currentPeriods',
      },
      {
        title: '租金',
        dataIndex: 'currentPeriodsRent',
      },
      {
        title: '状态',
        render: e => <>
          <span>
            {columnsByStagesStute[e.status]}
          </span>
        </>
        ,
      },
      {
        title: '支付时间',
        dataIndex: 'repaymentDate',
      },
      {
        title: '账单到期时间',
        dataIndex: 'statementDate',
      },
    ];
    let orderByStagesDtoList = this.state.stages
    let st = []
    return (
      <>
        {this.renderCollectionRecordModal()}
        <Drawer
          width={620}
          title='账单信息'
          placement="right"
          onClose={this.onClose}
          visible={this.state.stagesShow}>
          <MyStage
            orderByStagesDtoList={orderByStagesDtoList}></MyStage>
        </Drawer>
        <Drawer
          width={620}
          title='催收记录'
          placement="right"
          onClose={this.onClose}
          visible={this.state.stagesShow1}>
          <MyCs orderByStagesDtoList={orderByStagesDtoList}></MyCs>
        </Drawer>
        <PageHeaderWrapper title={false}>
          {/* <Card bordered={false} className='ant-card antd-pro-pages-finance-capital-account-index-mb20'>
            1111<img src='https://rich-rent.oss-cn-hangzhou.aliyuncs.com/icon/椭圆形 2.png'></img>
          </Card> */}
          <Card bordered={false} className='ant-card antd-pro-pages-finance-capital-account-index-mb20'>
            <div style={{ fontSize: 20, marginBottom: 20 }}>
              <strong>统计信息</strong><img src='https://rich-rent.oss-cn-hangzhou.aliyuncs.com/icon/椭圆形 2.png'></img>
            </div>
            <Descriptions column={5}>
              <Descriptions.Item label="应收人数">{alldb.hjysrs}</Descriptions.Item>
              <Descriptions.Item label="应收金额">{alldb.hjysje}</Descriptions.Item>
              <Descriptions.Item label="实收人数">{alldb.hjssrs}</Descriptions.Item>
              <Descriptions.Item label="实收金额">{alldb.hjssje}</Descriptions.Item>
              <Descriptions.Item label="回款率">{alldb.hjssrs ? (alldb.hjssje / alldb.hjysje * 100).toFixed(2) : ''}%</Descriptions.Item>
            </Descriptions>
          </Card>
          <Card bordered={false}>
            <Form layout="inline" onSubmit={this.handleSubmit}>
              {/* <Row>
              <Col span={8}>
                <Form.Item label="商品名称">
                  {getFieldDecorator(
                    'productName',
                    {},
                  )(<Input allowClear placeholder="请输入商品名称" />)}
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="下单人姓名">
                  {getFieldDecorator(
                    'userName',
                    {},
                  )(<Input allowClear placeholder="请输入下单人姓名" />)}
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="下单人手机号">
                  {getFieldDecorator(
                    'telephone',
                    {},
                  )(<Input allowClear placeholder="请输入下单人手机号" />)}
                </Form.Item>
              </Col>
            </Row> */}
              {/* <Row>
              <Col span={8}>
                <Form.Item label="收货人手机号">
                  {getFieldDecorator(
                    'addressUserPhone',
                    {},
                  )(<Input allowClear placeholder="请输入收货人手机号" />)}
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="下单人身份证号">
                  {getFieldDecorator(
                    'idCard',
                    {},
                  )(<Input allowClear placeholder="请输入下单人身份证号" />)}
                </Form.Item>
              </Col>
              <Col span={8}>
                {' '}
                <Form.Item label="订单编号">
                  {getFieldDecorator(
                    'orderId',
                    {},
                  )(<Input allowClear placeholder="请输入订单编号" />)}
                </Form.Item>
              </Col>
            </Row> */}

              <Form.Item label="创建时间">
                {getFieldDecorator('createDate', {})(<RangePicker onChange={this.onChange} />)}
                &nbsp;&nbsp;&nbsp; <Button type={cday === -1 ? 'primary' : 'default'} onClick={() => this.clickday(-1)}>  昨日</Button>
                <Button type={cday === 0 ? 'primary' : 'default'} onClick={() => this.clickday(0)}>  今日</Button>
                <Button type={cday === 1 ? 'primary' : 'default'} onClick={() => this.clickday(1)}>  明日</Button>
              </Form.Item>
              <Form.Item label="账单状态">
                {getFieldDecorator(
                  'status',
                  {},
                )(
                  <Select placeholder="账单状态" allowClear style={{ width: 180 }}>
                    {
                      Object.keys(OrderStagesStatus1).map((i) => {
                        //console.log("aaa",i)
                        let kk = OrderStagesStatus1[i]
                        return (
                          <Option value={kk.k} key={kk.k}>
                            {kk.v}
                          </Option>
                        );
                      })
                    }
                  </Select>,
                )}
              </Form.Item>
              <Form.Item label="订单周期">
                {getFieldDecorator(
                  'unit',
                  {},
                )(
                   <Select style={{ width: 180 }} allowClear placeholder="请选择">
                    {ddzq.map((value, i) => {
                      return (
                        <Option value={i} key={i}>
                          {value}
                        </Option>
                      );
                    })}
                  </Select>,
                )}
              </Form.Item>
              <Form.Item label="姓名">
                {getFieldDecorator(
                  'userName',
                  {},
                )(<Input allowClear placeholder="姓名" />)}
              </Form.Item>
              <Col span={8}>
                <Form.Item label="渠道来源">
                  {getFieldDecorator('channelId', {
                    // initialValue: '',
                  })(
                    <Select
                      placeholder="渠道来源"
                      allowClear
                      style={{ width: 180 }}
                      onChange={this.onChangesChannel}
                    >
                      {this.state.channelList.map(value => {
                        return (
                          <Option value={value.id} key={value.id}>
                            {value.name}
                          </Option>
                        );
                      })}
                    </Select>,
                  )}
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="催收筛选">
                  {getFieldDecorator('cid', {
                    // initialValue: '',
                  })(
                    <Select
                      placeholder="催收筛选"
                      allowClear
                      style={{ width: 180 }}
                      onChange={this.onChangesChannel}
                    >
                      {this.state.cslist.map(value => {
                        return (
                          <Option value={value.id} key={value.id}>
                            {value.name}
                          </Option>
                        );
                      })}
                    </Select>,
                  )}
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="第一期">
                  {getFieldDecorator('diyi', {
                  })(
                    <Select
                      placeholder="是否包含第一期"
                      allowClear
                      style={{ width: 180 }}
                    >
                      <Option value={1}>不含第一期</Option>
                    </Select>,
                  )}
                </Form.Item>
              </Col>
              {/*  <UserRating form={this.props.form} /> */}
              <div>
                <Form.Item>
                  <Button type="primary" htmlType="submit">
                    查询
                  </Button>
                </Form.Item>
                <Form.Item>
                  <Button htmlType="button" onClick={this.handleReset}>
                    重置
                  </Button>
                </Form.Item>
                <Form.Item>
                  <Button htmlType="button" onClick={this.expOrd}>
                    导出
                  </Button>
                </Form.Item>
                <Form.Item>
                  <Button htmlType="button" onClick={this.fp}>
                    分配催收
                  </Button>
                </Form.Item>

              </div>
            </Form>
            <Spin spinning={load}>
              <MyPageTable1
                scroll={true}
                onPage={this.onPage}
                ref={ node => this.MyPageTable1 = node }
                paginationProps={paginationProps}
                dataSource={onTableData(allList)}
                columns={columns}
              />
            </Spin>
          </Card>
        </PageHeaderWrapper>
      </>
    );
  }
}
