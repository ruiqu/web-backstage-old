import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import {
  Card, Tooltip, Button, Form, Input, Select, DatePicker, Spin, Row,
  Col, Divider, Table,
  InputNumber, Descriptions,
  message, Drawer,
} from 'antd';
import { getTimeDistance, renderOrderStatus, ddzq } from '@/utils/utils';
import request from '../../../services/baseService';
@connect(({ order, loading }) => ({
  ...order,
  loading: loading.effects['order/queryOpeOrderByConditionList'],
}))
@Form.create()
export default class HomePage extends Component {
  state = {
    current: 1,
    visible: '',
    unit: 2,
    orderVisible: '',
    load: false,
    stagesShow: false,
    orderId: null,
    channelList: [],
    stages: [],
    backuser: [],
    mydata: {},
    datas: {},
    record: {},
    mydb: [],
    expressList: [],
    yunTime: getTimeDistance('day'),
    gBtusua: '04', // 默认状态: 待发货
  };




  handleCancel = e => {
    this.setState({
      visible: false,
      orderVisible: false,
    });
  };

  getMon = (date) => {
    console.log("typeof", typeof date)
    let createTimeStart = this.getStartTime(date)
    let createTimeEnd = this.getEndTime(date)
    return { createTimeStart, createTimeEnd }
  }

  getStartTime = (date) => {
    date.setDate(1) // 将当前时间的日期设置成第一天
    let year = date.getFullYear()  // 得到当前年份 
    let month = date.getMonth() + 1 // 得到当前月份（0-11月份，+1是当前月份）
    month = month > 10 ? month : '0' + month // 补零
    let day = date.getDate() // 得到当前天数，实际是本月第一天，因为前面setDate(1) 设置过了
    // console.log(month) 03
    return year + '-' + month + '-' + day + " 00:00:00" // 这里传入的是字符串
  };

  getEndTime = (date) => {
    let year = date.getFullYear()
    let month = date.getMonth() + 1
    // 这里传入的是整数时间，返回的是下个月的第一天，因为月份是0-11
    let nextMonthFirthDay = new Date(year, month, 1) // 下个月的第一天
    console.log(nextMonthFirthDay)
    let oneDay = 1000 * 60 * 60 * 24 // 一天的时间毫秒数
    let endDay = new Date(nextMonthFirthDay - oneDay)
    let day = endDay.getDate() // 本月最后一天
    console.log(day)
    // 这里传入的是字符串格式的时间，返回的是传入字符串的时间
    return year + '-' + month + '-' + day + " 23:59:59"
  };

  componentDidMount() {
    this.onList(1, 30, this.getMon(new Date()));


  }
  handleSubmit = () => {
    this.props.form.validateFields((err, values) => {
      if (values.createDate) {
        console.log(values.createDate._d, "values")

        this.onList(1, 30, this.getMon(values.createDate._d));
      } else {

        this.onList(1, 30, this.getMon(new Date()));
      }
    })
    //this.onList()
  }
  onList = (pageNumber, pageSize, data = {}) => {
    //判断有没有时间
    console.log("data1111:", data)
    this.setState({ load: true, });

    request("/hzsx/business/order/queryOrderMonth", data).then(res => {
      let mydb = []
      for (let i in res.db) {
        const ele = res.db[i];
        ele.name = ddzq[i]
        mydb.push(ele)
      }
      console.log("aaa", mydb)
      this.setState({ mydb: mydb, load: false });
    })

  };


  render() {
    const { load, mydb } = this.state;
    let orderByStagesDtoList = this.state.stages
    const { form } = this.props;
    const { getFieldDecorator } = form;
    let columns = [
      { title: "类型", dataIndex: 'name', },
      { title: "到期订单", dataIndex: 'v到期订单', },
      { title: "到期金额", dataIndex: 'v到期金额', },
      { title: "差额", dataIndex: 'v差额', },
      { title: "已付租金", dataIndex: 'v已付租金', },
      { title: "应收金额", dataIndex: 'v应收金额', },
      { title: "总成本", dataIndex: 'v总成本', },
      { title: "总押金", dataIndex: 'v总押金', },
      { title: "总碎屏险金额", dataIndex: 'v总碎屏险金额', },
      { title: "总租金", dataIndex: 'v总租金', },
      { title: "逾期订单", dataIndex: 'v逾期订单', },
      { title: "逾期订单金额", dataIndex: 'v逾期订单金额', },
      { title: "逾期金额", dataIndex: 'v逾期金额', },
    ]
    return (
      <>
        <PageHeaderWrapper title={false}>


          <Card bordered={false}>
            <Form layout="inline" onSubmit={this.handleSubmit}>
              <Row>
                <Col span={16}>
                  <Form.Item label="选择月份">
                    {getFieldDecorator('createDate', {})(<DatePicker format='YYYY/MM'/>)}
                  </Form.Item>
                  <Form.Item>
                    <Button type="primary" htmlType="submit">
                      查询
                    </Button>
                  </Form.Item>
                </Col>
              </Row>
            </Form>
            <Spin spinning={load}>
              <Table
                columns={columns}
                dataSource={mydb}
                pagination={false}
                size="middle"
                tableLayout="fixed"
              />
            </Spin>
          </Card>
        </PageHeaderWrapper>
      </>
    );
  }
}
