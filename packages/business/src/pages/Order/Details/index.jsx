import React, { Component, Fragment } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import {
  Card,
  Button,
  Form,
  Input,
  Modal,
  Descriptions,
  Steps,
  Divider,
  Drawer,
  Timeline,
  Spin,
  Cascader,
  message,
  Tabs,
  Select,
  Radio, Icon,
  InputNumber,
  Popconfirm,
  DatePicker,
  Table,
  Upload,
  BackTop,
} from 'antd';
import MyPageTable from '@/components/MyPageTable';
import Xytx from '@/components/Xytx';//雷达
import Xytz from '@/components/Xytz';//探针
//import MyerDai from '@/components/MyerDai';
import AntTable from '@/components/AntTable';
import { onTableData, getParam, makeSub, renderOrderStatus } from '@/utils/utils.js';
import orderService, { queryCreditReportByUid } from '@/services/order';
import { getTimeDistance, renderOrderStagesStatus, handzx, OrderStagesStatus, OrderStagesStatus1 } from '@/utils/utils';
import yunxinCreditService from '@/services/yunxinCredit';
import CreditDetail from './creditDetail';
import CreditDetailOld from './creditDetailOld';
import CreditDetailThree from './creditDetailThree';
const { TextArea } = Input;
const FormItem = Form.Item;
const Option = Select.Option;
const { Step } = Steps;
const { TabPane } = Tabs;
const { confirm } = Modal;
import CustomCard from '@/components/CustomCard';
import {
  AuditReason,
  AuditStatus,
  confirmSettlementStatus,
  confirmSettlementType,
  orderStatusMap,
  BuyOutEnum,
  verfiyStatusMap,
  PayMent,
  defaultPlaceHolder
} from '@/utils/enum';
import { optionsdata } from './data.js';
import OrderService from '@/services/order';
import { router } from 'umi';
import busShopService from '@/services/busShop';
import { getToken } from "@/utils/localStorage"
import moment from 'moment';
import request from '../../../services/baseService';
import { renderFengxianfanqizha } from './unit/fengxianfanqizha'
import { fetchRiskErrHandler } from './fetchs/riskErrModule'
import {
  YouhuiMoneyWithTooltip, TLXProRiskReport, BaironProRiskReport, zhouqikoukuanReturnText,
  zhouqikoukuanReturnLabel,
  //DaiKeZhiFuBtn,
  //Daikou,
} from 'zwzshared'
import { number } from "prop-types";

const columnsByStagesStute = {
  '1': '待支付',
  '2': '已支付',
  '3': '逾期已支付',
  '4': '逾期待支付',
  '5': '已取消',
  '6': '已结算',
  '7': '已退款,可用',
  '8': '部分还款',
};

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};
//平台备注
const columns = [
  {
    title: '备注人姓名',
    dataIndex: 'userName',
  },
  {
    title: '备注时间',
    dataIndex: 'createTime',
  },
  {
    title: '备注内容',
    dataIndex: 'remark',
  },
];
//商家备注
const columnsBusiness = [
  {
    title: '备注人姓名',
    dataIndex: 'userName',
  },
  {
    title: '备注时间',
    dataIndex: 'createTime',
  },
  {
    title: '备注内容',
    dataIndex: 'remark',
  },
];
// 增值服务
const columnsAddService = [
  {
    title: '增值服务ID',
    dataIndex: 'shopAdditionalServicesId',
  },
  {
    title: '增值服务名称',
    dataIndex: 'shopAdditionalServicesName',
  },
  {
    title: '增值服务价格',
    dataIndex: 'price',
  },
];
//商品信息
const columnsInformation = [
  {
    title: '商品图片',
    dataIndex: 'imageUrl',
    render: imageUrl => {
      return (
        <img
          src={imageUrl}
          style={{
            width: 116,
            height: 62,
          }}
        />
      );
    },
  },
  {
    title: '商品名称',
    dataIndex: 'productName',
  },
  {
    title: '商品编号',
    dataIndex: 'productId',
    render: (text, record) => {
      return (
        <a className="primary-color" onClick={() => router.push(`/goods/list/detail/${record.id}`)}>
          {text}
        </a>
      );
    },
  },
  {
    title: '规格颜色',
    dataIndex: 'spec',
  },
  {
    title: '数量',
    dataIndex: 'num',
  },
  {
    title: '买断规则',
    dataIndex: 'buyOutSupportV1',
    render: buyOutSupport => {
      // buyOutSupport ? '可买断' : '不可买断'
      return <span>{BuyOutEnum[buyOutSupport] || '-'}</span>;
    },
  },
];

//账单信息
const columnsBill = [

  {
    title: '总租金',
    dataIndex: 'totalRent',
  },
  {
    title: '运费',
    dataIndex: 'freightPrice',
  },
  {
    title: '平台优惠',
    dataIndex: 'platformCouponReduction',
    render: (val, row) => {
      const youhuiList = row.userOrderCouponDtos || []
      const platformYouhuiList = youhuiList.filter(obj => {
        const str = obj.platform || ""
        return str.includes("OPE")
      }) // 筛选出只属于平台的优惠
      return <YouhuiMoneyWithTooltip money={val} youhuiList={platformYouhuiList} />
    }
  },
  {
    title: '店铺优惠',
    dataIndex: 'couponReduction',
    render: (val, row) => {
      const youhuiList = row.userOrderCouponDtos || []
      const shopYouhuiList = youhuiList.filter(obj => {
        const str = obj.platform || ""
        return str.includes("SHOP")
      }) // 筛选出只属于店铺的优惠
      return <YouhuiMoneyWithTooltip money={val} youhuiList={shopYouhuiList} />
    }
  },
];




//买断
const columnsBuyOutYes = [
  {
    title: '是否已买断',
    dataIndex: 'createTime',
    render: () => '是',
  },
  {
    title: '买断价格',
    dataIndex: 'buyOutAmount',
  },
];
const columnsBuyOutNo = [
  {
    title: '是否已买断',
    dataIndex: 'createTime',
    render: () => '否',
  },
  {
    title: '当前买断价格',
    dataIndex: 'currentBuyOutAmount',
  },
  {
    title: '到期买断价格',
    dataIndex: 'dueBuyOutAmount',
  },
];

//结算
const columnsSettlement = [
  {
    title: '宝贝状态',
    dataIndex: 'settlementType',
    render: (text, record) => {
      return confirmSettlementType[text];
    },
  },
  {
    title: '违约金',
    dataIndex: 'amount',
  },
  {
    title: '是否支付',
    dataIndex: 'settlementStatus',
    render: (text, record) => {
      return confirmSettlementStatus[text];
    },
  },
];

let collstus = {
  '01': '承诺还款',
  '02': '申请延期还款',
  '03': '拒绝还款',
  '04': '电话无人接听',
  '05': '电话拒接',
  '06': '电话关机',
  '07': '电话停机',
  '08': '客户失联',
};
//商家催收
const BusinessCollection = [
  {
    title: '记录人',
    dataIndex: 'userName',
  },
  {
    title: '记录时间',
    dataIndex: 'createTime',
  },
  {
    title: '结果',
    dataIndex: 'result',
    render: result => collstus[result],
  },
  {
    title: '小记',
    dataIndex: 'notes',
  },
];


// 账户情况
const columnsCreditAccountService = [
  {
    title: '',
    dataIndex: 'title',
  },
  {
    title: '非循环贷账户信息',
    dataIndex: 'fxhdzhxx',
  },
  {
    title: '循环额度分账户信息',
    dataIndex: 'xhedfzhxx',
  },
  {
    title: '循环贷账户信息',
    dataIndex: 'xhdzhxx',
  },
];




const paginationProps = {
  current: 1,
  pageSize: 1000,
  total: 1,
};



const KUAI_DI = 0; // 以快递形式发货
const ZI_TI = 1; // 以自提形式发货

@connect(({ order, loading, RentRenewalLoading }) => ({
  ...order,
  loading: loading.effects['order/queryOpeUserOrderDetail'],
  RentRenewalLoading: loading.effects['order/queryUserReletOrderDetail'],
}))
@Form.create()
export default class Details extends Component {
  state = {
    drawerVisible: false,
    drawerData: [],
    wdata: [],
    imgList: [],
    visible: false,
    showOrder: false,//展示订单列表
    visibles: false,
    visibles2: false,
    visiblesimg: false,
    isloading: false,
    img: '',
    settlement: null,
    xcurrent: 1,
    scurrent: 1,
    titles: '',
    subLists: [],
    creditInfo: null, // 天狼星旗舰版风控报告
    v布尔历史: null,
    v选择的日期: "",
    creditInfo1: null, // 二代风控报告
    baironRisk: null, // 百融风控报告
    baironRisk1: null, // 百融风控报告
    comprehensiveSuggest: '',
    deliverOptions: [],
    auditRecord: {},
    processDetail: [],

    orderVisible: '',
    orderId: '',
    radioValue: '',
    damageValue: '',

    queryOrderStagesDetail: {},

    activeTab: '1',
    cost: '',
    sign: false,

    HastenList: [],
    HastenTotal: 1,
    HastenCurrent: 1,

    opeHastenList: [],
    opeHastenTotal: 1,
    opeHastenCurrent: 1,
    visibles3: false,
    creditEmpty: false,
    showConfirmOrderReturnModal: false, // 是否显示确认归还的弹窗
    sendMethod: KUAI_DI, // 发货方式，默认快递
    userOrderCashesDtoBusiness: {},

    //征信弹窗
    showCreditDrawer: false,
    creditAccountInfo: null,

    showCreditBaseInfoDrawer: false,
    creditBaseInfo: null,
    //申请报送
    showConfirmApplyCreditModal: false,
    showConfirmfsModal: false,//复审按钮
    showConfirmfsVal: 0,//复审通过1，拒绝-1，默认0
    //放款报送
    showConfirmLoanCreditModal: false,
    //取消放款
    showConfirmCancelCreditModal: false,
    //结清
    showConfirmSettleCreditModal: false,
    //追偿报送
    showConfirmDunRepayCreditModal: false,
    //追偿结清
    showConfirmDunRepaySettleCreditModal: false,

    //押金信息
    depositData: {},
    //修改押金弹框
    depositModal: false,
    riskErrData: {}, // 风险反欺诈信息接口所返回的数据；可能会与原接口返回的内容存在不一致，经过了部分加工
    loadingRiskErrData: false, // 是否正在加载风险反欺诈信息数据中
    // loadingRiskReport: false, // 是否正在加载风控报告接口数据中
    showRiskReportCallBtn: false, // 是否显示查询风控报告按钮
    showRiskReportCallBtn1: false, // 二代风控报告
    showRiskReportBaironBtn: false, // 是否显示查询风控报告按钮
    showRiskReportBaironBtn1: false, // 是否显示查询风控报告按钮
    showCreditUploadInfoView: false,
    orderDetailApiRes: {},

    // 征信上报查询:
    createUploadInfo: null,

  };
  componentDidMount() {
    const { dispatch } = this.props;
    const orderId = getParam('id');
    this.setState({
      orderId,
    });
    this.onRentRenewalDetail();
    this.onPageBusiness({ current: 1 });
    this.onPage({ current: 1 });
    this.onHasten(1, 3, '02');
    this.onHasten(1, 3, '01');
    // config/getByCode?code=esign:fee
    busShopService.getByCode().then(res => {
      this.setState({
        cost: res,
      });
    });
    this.getExpressList();

    dispatch({
      type: 'order/queryOrderStagesDetail',
      payload: {
        orderId: getParam('id'),
      },
      callback: res => {
        if (res.responseType === 'SUCCESS') {
          this.setState({
            queryOrderStagesDetail: res.data,
          });
        }
      },
    });




    //押金信息
    OrderService.queryPayDepositLog({ orderId }).then(res => {
      if (res) {
        this.setState({
          depositData: res,
        });
      }
    });
  }



  fetchProcessDetail() {
    const orderId = getParam('id');
    orderService
      .queryOrderStatusTransfer({
        orderId,
      })
      .then(res => {
        this.setState({
          processDetail: res || [],
        });
      });
  }

  onRentRenewalDetail = () => {
    const { dispatch } = this.props;
    if (getParam('RentRenewal')) {
      dispatch({
        type: 'order/queryUserReletOrderDetail',
        payload: {
          orderId: getParam('id'),
        },
        callback: e => {
          const eData = e?.data?.userOrderCashesDto || {}
          this.setState({
            userOrderCashesDtoBusiness: eData,
            orderDetailApiRes: e.data,
          });
        },
      });
    } else {
      dispatch({
        type: 'order/queryOpeUserOrderDetail',
        payload: {
          orderId: getParam('id'),
        },
        callback: e => {
          console.log(e.data, "userOrderCashesDtoBusiness")
          this.setState({
            userOrderCashesDtoBusiness: e.data.userOrderCashesDto,
            orderDetailApiRes: e.data,
          });
        },
      });
    }
  };
  onQueryOrderRemark = (pageNumber, pageSize, source) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'order/queryOrderRemark',
      payload: {
        orderId: getParam('id'),
        pageNumber,
        pageSize,
        source,
      },
    });
  };
  //新建备注翻页
  onPage = (e = { current: 1 }) => {
    this.setState(
      {
        xcurrent: e.current,
      },
      () => {
        this.onQueryOrderRemark(e.current, 3, '01');
      },
    );
  };
  onHasten = (pageNumber, pageSize, source) => {
    orderService
      .queryOrderHasten({ pageNumber, pageSize, source, orderId: getParam('id') })
      .then(res => {
        if (source === '02') {
          this.setState({
            HastenList: res?.records,
            HastenTotal: res?.total,
          });
        } else {
          this.setState({
            opeHastenList: res?.records,
            opeHastenTotal: res?.total,
          });
        }
      });
  };
  onHastenBusiness = e => {
    this.setState(
      {
        HastenCurrent: e.current,
      },
      () => {
        this.onHasten(e.current, 3, '02');
      },
    );
  };
  onHastenOpe = e => {
    this.setState(
      {
        opeHastenCurrent: e.current,
      },
      () => {
        this.onHasten(e.current, 3, '01');
      },
    );
  };
  //商机备注翻页
  onPageBusiness = (e = { current: 1 }) => {
    this.setState(
      {
        scurrent: e.current,
      },
      () => {
        this.onQueryOrderRemark(e.current, 3, '02');
      },
    );
  };
  //物流信息
  onClose = () => {
    this.setState({
      drawerVisible: false,
    });
  };


  //查看物流
  onLogistics = (e, i) => {
    this.setState(
      {
        drawerVisible: true,
        drawerTitle: e,
        //wdata:res.list,
      },
      () => {
        this.onQueryExpressInfo(i);
      },
    );
    return;
    //wlList
    request("/zyj-api-web/hzsx/api/order/userGetExpressByOrderId?orderId=" + getParam('id'), {}, "get").then(res => {
      //this.props.wlList=res.list
      //console.log("eee",this.props.wlList)
      this.setState({
        drawerVisible: eData,
        orderDetailApiRes: e.data,
      });
      this.setState(
        {
          drawerVisible: true,
          drawerTitle: e,
          //wdata:res.list,
        },
        () => {
          this.onQueryExpressInfo(i);
        },
      );
    })
    this.setState(
      {
        drawerVisible: true,
        drawerTitle: e,
      },
      () => {
        this.onQueryExpressInfo(i);
      },
    );
  };
  handleOk = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { dispatch, userOrderInfoDto } = this.props;
        if (this.state.titles === '备注') {
          if (getParam('RentRenewal')) {
            dispatch({
              type: 'order/orderRemark',
              payload: {
                orderId: getParam('id'),
                remark: values.beizhu,
                orderType: userOrderInfoDto.type,
              },
              callback: res => {
                this.onQueryOrderRemark(1, 3, '02');
                this.onRentRenewalDetail();
                this.setState({
                  visible: false,
                });
              },
            });
          } else {
            dispatch({
              type: 'order/orderRemark',
              payload: {
                orderId: getParam('id'),
                remark: values.beizhu,
                orderType: userOrderInfoDto.type,
              },
              callback: res => {
                this.onQueryOrderRemark(1, 3, '01');
                this.onRentRenewalDetail();
                this.setState({
                  visible: false,
                });
              },
            });
          }
        } else {
          dispatch({
            type: 'order/opeOrderAddressModify',
            payload: {
              realName: values.realName,
              street: values.street,
              telephone: values.telephone,
              province: values && values.city && values.city[0],
              city: values && values.city && values.city[1],
              area: values && values.city && values.city[2],
              orderId: getParam('id'),
            },
            callback: res => {
              this.onRentRenewalDetail();
              this.setState({
                visible: false,
              });
            },
          });
        }
      }
    });
  };
  onQueryExpressInfo = i => {
    const { dispatch } = this.props;
    /* console.log(i, '======>/zyj-api-web/hzsx/api/order/userGetExpressByOrderId', getParam('id'));
    request("/zyj-api-web/hzsx/api/order/userGetExpressByOrderId?orderId="+getParam('id'),{},"get").then(res=>{
      console.log("eee",res)
    }) */
    dispatch({
      type: 'order/queryExpressInfo',
      payload: {
        expressNo: i.expressNo,
        receiverPhone: i.receiverPhone,
        shortName: i.shortName,
        orderId: getParam('id'),
      },
    });
  };

  getExpressList = () => {
    OrderService.selectExpressList().then(res => {
      this.setState({
        deliverOptions: res || [],
      });
    });
  };

  getAuditRecord = () => {
    OrderService.queryOrderAuditRecord({
      orderId: this.state.orderId,
    }).then(res => {
      this.setState({
        auditRecord: res || [],
      });
    });
  };

  handleCancel = e => {
    this.setState({
      visible: false,
      orderVisible: false,
    });
  };
  handleCancels = e => {
    this.setState({
      visibles: false,
      visibles2: false,
      visibles3: false,
      visiblesimg: false,
    });
  };

  tzmap = e => {
    if (e == null) return message.error("没有坐标信息")
    /* let transfer = document.createElement('input');
    document.body.appendChild(transfer);
    transfer.value = e.longitude + "," + e.latitude;  // 这里表示想要复制的内容
    transfer.focus();
    transfer.select();
    if (document.execCommand('copy')) { //判断是否支持document.execCommand('copy')       document.execCommand('copy');
    }
    transfer.blur();
    message.success('复制成功'); */
    console.log(e, "点击了跳转")
    let url = `https://ditu.amap.com/regeo?lng=${e.longitude}&lat=${e.latitude}&src=uriapi&innersrc=uriapi`
    window.open(url, "_blank");
  }

  onKaiImg = e => {
    this.setState({
      img: e,
      visiblesimg: true,
    });
  };
  showModal = e => {
    this.setState({
      visible: true,
      titles: e,
    });
  };

  settleDia = (type, orderId, userOrderCashId, record) => {
    const { userOrderCashesDto, userViolationRecords } = this.props;
    if (userViolationRecords && userViolationRecords[0]) {
      let radioValue = 'good';
      radioValue = userOrderCashesDto.damagePrice ? 'damage' : radioValue;
      radioValue = userOrderCashesDto.lostPrice ? 'lose' : radioValue;
      this.setState({
        checkValue: true,
        radioValue,
        damageValue: userOrderCashesDto.damagePrice || userOrderCashesDto.lostPrice,
      });
    }
    this.setState({
      orderId,
      userOrderCashId,
      record,
      orderVisible: 'settle',
    });
  };

  showOrderModal = (type, flag) => {
    if (type === 'deliver') {
      OrderService.checkOrderIsAuth({
        // ...fieldsValue,
        orderId: this.state.orderId,
      }).then(checkRes => {
        if (checkRes) {
          confirm({
            title: '发货失败',
            content: (
              <div>
                <p style={{ marginBottom: '0' }}>{checkRes}</p>
              </div>
            ),
          });
        } else {
          this.getExpressList();

          this.setState({
            orderVisible: type,
          });
        }
      });
    } else {
      if (type === 'express') {
        this.getExpressList();
      }
      if (type === 'settle') {
        this.settleDia();
      }
      if (type === 'audit') {
        this.setState({
          titles: flag ? '审批通过' : '审批拒绝',
        });
      }
      this.setState({
        orderVisible: type,
      });
    }
  };
  onXY = () => {
    location.href = this.props.contractUrl;
  };
  // 上传完之后的处理方法
  handleUploadImage = ({ file, fileList }) => {
    if (file.status === "done") {
      const images = fileList.map((v, index) => {
        if (v.response) {
          const src = v.response.data
          return { uid: index, src: src, url: src, isMain: v.isMain || null }
        }
        return v
      })
      const imgSrc = images && images[0] && images[0].url
      this.setState({ uploadImgUrl: imgSrc })
    }
    this.setState({ imgList: fileList })
  }
  // 加载天狼星报告数据
  viewCredit = (riskType = "01") => {
    if (this.state.isloading) return
    this.setState({ isloading: true });
    const uoiObj = this.props.userOrderInfoDto || {}
    const uid = uoiObj.uid
    const phone = uoiObj.telephone
    const idCardNo = uoiObj.idCard
    const userName = uoiObj.realName
    this.props.dispatch({
      type: 'order/getCredit',
      payload: {
        uid,
        orderId: getParam('id'),
        phone,
        riskType: riskType,
        idCardNo,
        userName,
      },
      callback: res => {

        this.setState({ isloading: false });
        if (res.responseType === 'SUCCESS') {
          const resultStr = (res.data && res.data.siriusRiskReport) || "" // 风控报告结果字符串
          const str2 = resultStr.replaceAll('\"', '"')
          const finalResult = JSON.parse(str2)
          if (riskType == "01") {
            this.setState({ creditInfo: finalResult, showRiskReportCallBtn: false })
          } else {
            this.setState({ creditInfo1: finalResult, showRiskReportCallBtn1: false })
          }
        }
        //获取完初步的布尔风控，再获取二代的
        if (riskType == "01") this.viewCredit("06")
      },
    });
  };





  f获取布尔历史风控 = (riskType = "01") => {
    if (this.state.isloading) return
    this.setState({ isloading: true });
    let db = {}
    const uoiObj = this.props.userOrderInfoDto || {}
    db.uid = uoiObj.uid
    db.phone = uoiObj.telephone
    db.riskType = riskType
    db.idCardNo = uoiObj.idCard
    db.userName = uoiObj.realName
    //发起请求，直接请求获取列表
    request("/hzsx/business/order/queryLsReportByUid", db).then(res => {
      //console.log("历史 的数据是",res)
      //this.f布尔历史数据处理(res)
      let v布尔历史 = {}
      for (let i = 0; i < res.length; i++) {
        const ele = res[i];
        let k = ele.createTime.substring(0, 10);
        if (!v布尔历史[k]) v布尔历史[k] = {}
        const str2 = ele.reportResult.replaceAll('\"', '"')
        const finalResult = JSON.parse(str2)
        v布尔历史[k][ele.reportType] = finalResult
        //判断数据有没有，有的话就添加，没有就新增 
      }
      console.log("的数据是db", v布尔历史)
      let arr = Object.keys(v布尔历史);
      if (arr.length == 1) {
        message.success("暂时没有历史数据")
        this.setState({ isloading: false });
      } else {
        this.setState({ isloading: false, v选择的日期: -1, v布尔历史 });
      }
      //判断一下，把第一个给去掉，然后显示另外 的俩个
    })
  }
  // 加载百融报告数据
  getbairon = () => {
    const uoiObj = this.props.userOrderInfoDto || {}
    const uid = uoiObj.uid
    const phone = uoiObj.telephone
    const idCardNo = uoiObj.idCard
    const userName = uoiObj.realName
    this.props.dispatch({
      type: 'order/getbairon',
      payload: {
        uid,
        orderId: getParam('id'),
        phone,
        idCardNo,
        userName,
      },
      callback: res => {
        if (res.responseType === 'SUCCESS') {
          const resultStr = (res.data && res.data.resultJson) || "" // 风控报告结果字符串
          //console.log("获取百度风控报告内容",resultStr)
          const str2 = resultStr.replaceAll('\"', '"')
          const finalResult = JSON.parse(str2)
          console.log("获取百融风控报告内容", finalResult)
          this.setState({ baironRisk: finalResult, showRiskReportBaironBtn: false })
        }
      },
    });
  };
  // 加载百融报告数据
  getbairon1 = () => {
    const uoiObj = this.props.userOrderInfoDto || {}
    const uid = uoiObj.uid
    const phone = uoiObj.telephone
    const idCardNo = uoiObj.idCard
    const userName = uoiObj.realName
    this.props.dispatch({
      type: 'order/getbairon',
      payload: {
        uid,
        type: 1,
        orderId: getParam('id'),
        phone,
        idCardNo,
        userName,
      },
      callback: res => {
        if (res.responseType === 'SUCCESS') {
          const resultStr = (res.data && res.data.resultJson) || "" // 风控报告结果字符串
          //console.log("获取百度风控报告内容",resultStr)
          const str2 = resultStr.replaceAll('\"', '"')
          const finalResult = JSON.parse(str2)
          console.log("获取百融风控报告内容", finalResult)
          this.setState({ baironRisk1: finalResult, showRiskReportBaironBtn1: false })
        }
      },
    });
  };

  tabChange = key => {
    console.log('切换了', key)
    const { userOrderInfoDto } = this.props;
    if (key === '3') {
      this.getAuditRecord();
    }
    if (key === '6') {
      this.fetchProcessDetail();
    }
    if (key === '2') {
      this.setState({ showRiskReportCallBtn: true })
    }
    if (key === '23') {
      this.setState({ showRiskReportCallBtn1: true })
    }
    if (key === '12') {
      this.setState({ showRiskReportBaironBtn: true })
    }
    if (key === '13') {
      this.setState({ showRiskReportBaironBtn1: true })
    }
    if (key === '333') {
      this.getCreditApplyInfo();
    }

    this.setState({
      activeTab: key,
    });
  };

  /**
   * 下载回执单的处理方法
   */
  downloadHuizhidanHandler = oId => {
    if (!oId) return // 订单ID必传
    if (this.loading) return // 点击中也不处理
    this.loading = true
    const url = `/hzsx/export/receiptConfirmationReceipt?orderId=${oId}`
    request(url, {}, 'get').then((res) => {
      console.log("res.data", res.downloadUrl)
      window.open(res.downloadUrl, '_blank');
    }).finally(() => {
      this.loading = false
    })
  }

  /**
   * 下载合同的处理方法
   */
  downloadHetongHandler = orderId => {
    if (!orderId) return // 订单ID必传
    if (this.loading) return // 点击中也不处理
    this.loading = true
    this.setState({ isloading: true });
    const url = `/hzsx/business/order/downOrderContract`
    request(url, { orderId }, 'get').then((res) => {
      console.log("返回的信息：,res", res)
      try {
        let obj = JSON.parse(res)
        for (let i = 0; i < obj.docs.length; i++) {
          const ele = obj.docs[i];
          window.open(ele.fileUrl, '_blank');
        }
      } catch (err) {
        window.open(res, '_blank');
      }
      //message.success('导出任务创建成功，请前往“数据管理-导出数据下载”完成下载。')
      this.loading = false
    }).finally(() => {
      this.setState({ isloading: false });
      this.loading = false
    })
  }



  renderOrderInfoTab() {
    const {
      userOrderInfoDto = {},
      orderAdditionalServicesDto = {},
      userCertification = {},
      orderAddressDto,
      opeRemarkDtoPage,
      businessRemarkDtoPage,
      productInfo,
      rentStart,
      rentDuration,
      unrentTime,
    } = this.props;
    console.log("userCertification    ", userCertification)
    const zengzhi = this.props.orderAdditionalServicesList || []; // 增值列表数据

    const orderId = getParam('id');
    return (
      <div>
        <Descriptions>
          <Descriptions.Item label="订单号" span={3}>
            {orderId}
          </Descriptions.Item>
          {this.props.contractUrl ? (
            <Descriptions.Item span={3} label="用户租赁协议">
              {/* <a href={this.props.contractUrl} target="_blank">《租赁协议》 </a> */}
              <a className="blackClickableA" onClick={() => this.downloadHetongHandler(orderId)}>协议下载</a>
            </Descriptions.Item>
          ) : null}
          <Descriptions.Item label="" span={3}>
            <a className="blackClickableA" onClick={() => this.downloadHuizhidanHandler(orderId)}>回执单下载</a>
          </Descriptions.Item>
        </Descriptions>

        {orderAddressDto ? (
          <>
            <Descriptions
              title={
                <>
                  <CustomCard title="收货人信息" />
                </>
              }
            >
              <Descriptions.Item label="收货人姓名">
                {orderAddressDto && orderAddressDto.realname}
              </Descriptions.Item>
              <Descriptions.Item label="收货人手机号">
                {orderAddressDto && orderAddressDto.telephone}
              </Descriptions.Item>
              <Descriptions.Item label="收货人地址">
                {orderAddressDto && orderAddressDto.provinceStr}
                {orderAddressDto && orderAddressDto.cityStr}
                {orderAddressDto && orderAddressDto.areaStr}
                {orderAddressDto && orderAddressDto.street}
              </Descriptions.Item>
              <Descriptions.Item label="用户备注">{userOrderInfoDto.remark}</Descriptions.Item>
            </Descriptions>


            <Descriptions
              title={
                <>
                  <CustomCard title="联系人信息" />
                </>
              } firstContactName
            >
              <Descriptions.Item label="紧急联系人姓名">
                {userCertification && userCertification.firstContactName}
              </Descriptions.Item>
              <Descriptions.Item label="关系">
                {userCertification && userCertification.firstContactRelation}
              </Descriptions.Item>
              <Descriptions.Item label="紧急联系人电话">
                {userCertification && userCertification.firstContactPhone}
              </Descriptions.Item>
              <Descriptions.Item label="紧急联系人姓名">
                {userCertification && userCertification.twoContactName}
              </Descriptions.Item>
              <Descriptions.Item label="关系">
                {userCertification && userCertification.twoContactRelation}
              </Descriptions.Item>
              <Descriptions.Item label="紧急联系人电话">
                {userCertification && userCertification.twoContactPhone}
              </Descriptions.Item>
            </Descriptions>
            <Divider />
          </>
        ) : null}

        <CustomCard title="商品信息" style={{ marginBottom: 20 }} />
        <AntTable
          columns={columnsInformation}
          dataSource={onTableData(productInfo)}
          paginationProps={paginationProps}
        />

        <Descriptions title={<CustomCard title="租用信息" />}>
          <Descriptions.Item label="租用天数">{rentDuration}</Descriptions.Item>
          <Descriptions.Item label="起租时间">{rentStart}</Descriptions.Item>
          <Descriptions.Item label="归还时间">{unrentTime}</Descriptions.Item>
        </Descriptions>
        <Divider />
        <Descriptions title={<CustomCard title="复审信息" />}>
          <Descriptions.Item label="复审状态">{userOrderInfoDto.preVerify ? verfiyStatusMap[userOrderInfoDto.preVerify] : '无'}</Descriptions.Item>
          <Descriptions.Item label="复审备注">{userOrderInfoDto.orderRemark}</Descriptions.Item>
        </Descriptions>
        <Divider />

        <CustomCard title="增值服务" style={{ marginBottom: 20 }} />
        <AntTable
          columns={columnsAddService}
          dataSource={zengzhi}
          // dataSource={onTableData([orderAdditionalServicesDto])}
          paginationProps={paginationProps}
        />

        <CustomCard title="商家备注" style={{ marginBottom: 20 }} />
        {/*<Button*/}
        {/*  type="primary"*/}
        {/*  style={{ margin: '20px 0' }}*/}
        {/*  onClick={() => this.showOrderModal('remark')}*/}
        {/*>*/}
        {/*  + 新建备注*/}
        {/*</Button>*/}
        <MyPageTable
          onPage={this.onPageBusiness}
          paginationProps={{
            current: this.state.scurrent,
            pageSize: 3,
            total: businessRemarkDtoPage.total,
          }}
          dataSource={onTableData(businessRemarkDtoPage.records)}
          columns={columnsBusiness}
        />
        <Divider />
        <CustomCard title="平台备注" />
        <MyPageTable
          onPage={this.onPage}
          paginationProps={{
            current: this.state.xcurrent,
            pageSize: 3,
            total: opeRemarkDtoPage.total,
          }}
          dataSource={onTableData(opeRemarkDtoPage.records)}
          columns={columns}
        />
      </div>
    );
  }

  // 在上传之前进行处理
  beforeUpload = file => {
    const isJPG = file.type === "image/jpeg" || file.type === "image/png" || file.type === "image/gif"
    if (!isJPG) {
      message.error("图片格式不正确")
    }
    const isLt2M = file.size / 1024 / 1024 < 2
    if (!isLt2M) {
      message.error("图片大于2MB")
    }
    return isJPG && isLt2M
  }
  // 渲染风控报告数据
  renderRiskTab() {
    let riskApiRes = this.state.creditInfo  //01
    let riskApiRes1 = this.state.creditInfo1  //06
    let { v布尔历史, v选择的日期 } = this.state
    if (v选择的日期 >= 0 && v布尔历史 && Object.keys(v布尔历史).length > 0) {//如果选择了日期，那么重置数据
      riskApiRes = v布尔历史[v选择的日期]["01"]
      riskApiRes1 = v布尔历史[v选择的日期]["06"]
    }
    let iss = !v选择的日期
    console.log("日期与历史数据是", v布尔历史, v选择的日期)
    //判断有没有点按钮，如果没有，那么就是默认的显示查询历史按钮
    if (!riskApiRes || !riskApiRes1) return null
    if (riskApiRes.resp_code === "SW0000" && riskApiRes1.resp_code === "SW0000") {
      console.log(iss, "riskapires1111", riskApiRes, riskApiRes1)
      const obj = riskApiRes.resp_data || {}
      const obj1 = riskApiRes1.resp_data || {}
      return <>
        {this.xran()}
        <TLXProRiskReport riskReport={obj} riskReport1={obj1} />
      </>
    } else {
      return <div style={{ marginTop: 15 }}>获取风险报告出现问题</div>
    }
  }
  xran = () => {
    let { v布尔历史, v选择的日期 } = this.state
    console.log("v布尔历史", v布尔历史)
    //如果没有，那么就显示
    if (!v选择的日期) {
      return <><Button onClick={() => this.f获取布尔历史风控("01")} type="primary">查询历史</Button></>
    } else {
      return (<>
        {Object.keys(v布尔历史).map(option => {
          return (
            <Button disabled={option == v选择的日期} onClick={() => {
              this.setState({ v选择的日期: option })
            }} type="primary">{option}</Button>
          );
        })}
      </>
      )
    }
  }


  // 渲染风控报告数据
  renderRiskbaironTab() {
    const riskApiRes = this.state.baironRisk
    if (!riskApiRes) return null
    if (riskApiRes.code === "00") {
      const obj = riskApiRes || {}
      return (
        <BaironProRiskReport riskReport={obj} />
      )
    } else {
      return <div style={{ marginTop: 15 }}>获取风险报告出现问题</div>
    }
  }
  // 渲染风控报告数据，三要素
  renderRiskbaironTab1() {
    const riskApiRes = this.state.baironRisk1
    if (!riskApiRes) return null
    if (riskApiRes.code === "00") {
      const obj = riskApiRes || {}
      return (
        <div className="booleandataTotalContainer">
          <div className="booleanDataWrap">
            <div>
              <Divider name="手机三要素" />
              <div className="riskDetection">
                <Descriptions column={4}>
                  <Descriptions.Item label="运营商">
                    {riskApiRes.TelCheck_s.operation == "1" ? '电信' : riskApiRes.TelCheck_s.operation == "2" ? '联通' : riskApiRes.TelCheck_s.operation == "3" ? '移动' : '其它'}
                  </Descriptions.Item>

                  <Descriptions.Item label="与身份信息是否一致">
                    {riskApiRes.TelCheck_s.result == "1" ? '一致' : riskApiRes.TelCheck_s.result == "2" ? '不一致' : '查无此号'}
                  </Descriptions.Item>

                  <Descriptions.Item label="在网时长">
                    {riskApiRes.TelPeriod.data.value == "1" ? '0-6' : riskApiRes.TelPeriod.data.value == "2" ? '6-12' : riskApiRes.TelPeriod.data.value == "3" ? '12-24' : '24+'}
                    个月
                  </Descriptions.Item>
                  <Descriptions.Item label="在网状态">
                    {riskApiRes.TelStatus.data.value == "1" ? '正常' : riskApiRes.TelPeriod.data.value == "2" ? '停机' : riskApiRes.TelPeriod.data.value == "3" ? '销号' : '异常'}
                  </Descriptions.Item>
                </Descriptions>
              </div>
            </div>
          </div>
        </div>
      )
    } else {
      return <div style={{ marginTop: 15 }}>获取风险报告出现问题</div>
    }
  }

  renderAuditTab() {
    const { auditRecord = {} } = this.state;

    return (
      <div>
        <Descriptions>
          <Descriptions.Item label="审批时间" span={3}>
            {auditRecord.approveTime}
          </Descriptions.Item>
          <Descriptions.Item label="审批人" span={3}>
            {auditRecord.approveUserName}
          </Descriptions.Item>
          <Descriptions.Item label="审批结果" span={3}>
            {AuditStatus[auditRecord.approveStatus]}
          </Descriptions.Item>
          {auditRecord.approveStatus === '02' && (
            <Descriptions.Item label="拒绝类型" span={3}>
              {AuditReason[auditRecord.refuseType]}
            </Descriptions.Item>
          )}
          <Descriptions.Item label="小记" span={3}>
            {auditRecord.remark}
          </Descriptions.Item>
        </Descriptions>
      </div>
    );
  }


  getCreditApplyInfo() {
    const orderId = getParam('id');
    console.log('看下订单ID', orderId)
    yunxinCreditService.queryCreditApplyInfo({
      'orderId': orderId,
    }).then(res => {
      this.setState({ createUploadInfo: res })
      this.setState({ showCreditUploadInfoView: true })
      console.log('渲染一下征信上报', res)
    });
  }

  /**
   * 渲染云信上报信息
   * @returns {JSX.Element}
   */
  renderCreditApplyInfoTab() {
    if (!this.state.showCreditUploadInfoView) {
      return;
    }

    let createUploadInfo = Object.assign({}, this.state.createUploadInfo);
    let dunRepayCreditListData = createUploadInfo.dunRepayCreditList;
    let repayCreditListData = createUploadInfo.stagesList;
    let userOrder = createUploadInfo.userOrder
    //默认是还款报送，否则为逾期还款报送
    const hkbs = (stage, type = 1) => {
      let param = {
        orderId: stage.orderId,
        stageId: stage.id,
      }
      let title = "还款报送--"
      console.log(1111, param)
      let url = `/hzsx/business/yunxin/repayCredit`
      if (type != 1) {
        title = "逾期还款报送--"
        url = `/hzsx/business/yunxin/overdueRepayCredit`
      }
      request(url, param, 'post').then(res => {

        message.warning(title + res)

        console.log("返回的结果是", res)
      })
    }
    console.log('来了', createUploadInfo)
    let time = '2023年02月20日17:39:15';
    let money = 1000;

    let repayCreditListColumns = [
      {
        title: '期数',
        dataIndex: 'currentPeriods',
      },
      {
        title: '租金',
        dataIndex: 'currentPeriodsRent',
      },
      {
        title: '状态',
        dataIndex: 'status',
        render: (text, record, index) => {
          return (
            <>
              <span>{text == "1" ? "待支付" : "支付成功"}</span>
            </>
          )
        }
      },
      {
        title: '支付方式',
        dataIndex: 'payment',
        render: (text, record, index) => {
          return (
            <>
              <span>{PayMent[text]}</span>
            </>
          )
        }
      },

      {
        title: '支付时间',
        dataIndex: 'repaymentDate',
      },
      {
        title: '账期到账时间',
        dataIndex: 'statementDate',
      },
      {
        title: '上报时间',
        dataIndex: 'repayCreditTime',
      },
      {
        title: '上报类型',
        dataIndex: 'repayCreditType',
        render: (text, record, index) => {
          return (
            <>

              <span>{record.currentPeriods != 0 && text ? text == "10" ? "还款报送" : "逾期还款报送" : ""}</span>
              {/* <span>{text == "10" ? "还款报送" : "逾期还款报送"}</span> */}
            </>
          )
        }
      },
      {
        title: '上报状态',
        dataIndex: 'repayCreditStatus',
        render: (text, record, index) => {
          return (
            <>
              <span>{record.currentPeriods != 0 && text ? text == "10" ? "成功" : "失败" : ""}</span>
            </>
          )
        }
      },
      {
        title: '操作',
        dataIndex: '',
        width: 220,
        render: (text, record, index) => {
          return (
            <>

              {record.currentPeriods != 0 ? <>   <Button onClick={() => {
                hkbs(record)
              }}>还款报送</Button>
                <Button onClick={() => {
                  hkbs(record, 2)
                }}>逾期还款</Button></> : <>  </>}

            </>
          )
        }
      },
    ];


    // let repayCreditListData = [
    //   {
    //     currentPeriods: 10,
    //     currentPeriodsRent: money,
    //     status: '已还款',
    //     repaymentDate: time,
    //     statementDate:time,
    //     reportTime: time,
    //     reportType: '什么意思?',
    //     reportStatus: '什么意思?'
    //   },
    //   {
    //     currentPeriods: 10,
    //     currentPeriodsRent: money,
    //     status: '已还款',
    //     repaymentDate: time,
    //     statementDate:time,
    //     reportTime: time,
    //     reportType: '什么意思?',
    //     reportStatus: '什么意思?'
    //   },
    //   {
    //     currentPeriods: 10,
    //     currentPeriodsRent: money,
    //     status: '已还款',
    //     repaymentDate: time,
    //     statementDate:time,
    //     reportTime: time,
    //     reportType: '什么意思?',
    //     reportStatus: '什么意思?'
    //   }
    // ];

    let dunRepayCreditListColumns = [
      {
        title: '上报时间',
        dataIndex: 'createTime',
      },
      {
        title: '上报类型',
        dataIndex: 'repayCreditType',
      },
      {
        title: '追偿金额',
        dataIndex: 'amount',
      },
      {
        title: '当前追偿后剩余金额',
        dataIndex: 'leftAmount',
      },
    ];

    // let dunRepayCreditListData = [
    //   {
    //     reportTime: time,
    //     reportType: '什么意思?',
    //     amount: money,
    //     surplusAmount: money
    //   },
    //   {
    //     reportTime: time,
    //     reportType: '什么意思?',
    //     amount: money,
    //     surplusAmount: money
    //   },
    //   {
    //     reportTime: time,
    //     reportType: '什么意思?',
    //     amount: money,
    //     surplusAmount: money
    //   }
    // ];
    return (
      <div>
        <Descriptions title={<CustomCard title="申请、放款报送" />}>
          <Descriptions.Item label="发货时间">{userOrder.deliveryTime}</Descriptions.Item>
          <Descriptions.Item label="申请报送时间">{userOrder.applyCreditTime}</Descriptions.Item>
          <Descriptions.Item label="放款报送时间">{userOrder.loanCreditTime}</Descriptions.Item>
        </Descriptions>
        <Divider />
        <Descriptions title={<CustomCard title="征信处理" />}>
        </Descriptions>
        <Button type="primary" onClick={this.returnApplyCreditModalHandler}>
          申请报送
        </Button>
        <Button type="primary" onClick={this.returnLoanCreditModalHandler}>
          放款报送
        </Button>
        <Button type="primary" onClick={this.returnCancelCreditModalHandler}>
          取消放款
        </Button>
        <Button type="primary" onClick={this.returnSettleCreditModalHandler}>
          结清
        </Button>
        <Button type="primary" onClick={this.returnDunRepayCreditModalHandler}>
          追偿报送
        </Button>
        <Button type="primary" onClick={this.returnDunRepaySettleCreditModalHandler}>
          追偿结清
        </Button>
        <Divider />
        <Descriptions title={<CustomCard title="还款报送" />}>
        </Descriptions>
        <AntTable
          columns={repayCreditListColumns}
          dataSource={repayCreditListData} />

        <Descriptions title={<CustomCard title="结清报送" />}>
          <Descriptions.Item label="上报时间">{userOrder.settleCreditTime}</Descriptions.Item>
          <Descriptions.Item label="催收总金额">¥: { }</Descriptions.Item>
        </Descriptions>
        <Divider />
        <Descriptions title={<CustomCard title="追偿,追偿结清报送" />}>
        </Descriptions>
        <AntTable
          columns={dunRepayCreditListColumns}
          dataSource={dunRepayCreditListData} />
        <Divider />

      </div>
    )
  }

  //押金管理
  renderDeposit() {
    const { depositData } = this.state;
    const yajinZhifuList = [] // 押金支付列表数据
    const { amount, creditAmount, paidAmount, waitPayAmount } = depositData || {}
    if (
      amount != undefined ||
      creditAmount != undefined ||
      paidAmount != undefined ||
      waitPayAmount != undefined
    ) { // 只要有任意一者存在有效值即进行展示
      const item = { key: 1, amount, creditAmount, paidAmount, waitPayAmount }
      yajinZhifuList.push(item)
    }
    let sm = {
      rank0: '提供信息不足，提供参数信息有误，或提供的支付宝账号不存在。',
      rank1: '表示用户拒付风险为低。',
      rank2: '表示用户拒付风险为中。',
      rank3: '表示用户拒付风险为高。',
    };
    let jj = {
      rank0: '等级0',
      rank1: '等级1',
      rank2: '等级2',
      rank3: '等级3',
    };
    const scoreTableCol11 = [
      {
        title: '风险评级',
        key: 'detail',
        render: text => <div>{this.props.nsfLevel}</div>,
      },
      {
        title: '风险描述',
        key: 'detail',
        render: text => <div>{jj[this.props.nsfLevel]}</div>,
      },
      {
        title: '评级备注',
        key: 'detail',
        render: text => <div>{sm[this.props.nsfLevel]}</div>,
      },
    ];

    let columns = [
      {
        title: '押金总额',
        dataIndex: 'amount',
      },
      {
        title: '已支付押金',
        dataIndex: 'paidAmount',
      },
      {
        title: '待支付押金',
        dataIndex: 'waitPayAmount',
      },
    ];
    let columnsLogs = [
      {
        title: '押金总额',
        dataIndex: 'afterAmount',
      },
      {
        title: '修改时间',
        dataIndex: 'createTime',
      },
      {
        title: '修改人',
        dataIndex: 'backstageUserName',
      },
    ];
    //芝麻额度冻结信息
    const columnsZm = [
      {
        title: '冻结额度',
        dataIndex: 'freezePrice',
      },
      {
        title: '信用减免',
        dataIndex: 'creditDeposit',
      },
      {
        title: '实际冻结',
        dataIndex: '', // todo
        render: (text, record) => {
          return makeSub(record.freezePrice, record.creditDeposit);
        },
      }

    ];
    return (
      <>
        <CustomCard title="芝麻额度冻结" style={{ marginBottom: 20 }} />
        <AntTable
          columns={columnsZm}
          dataSource={onTableData([this.state.userOrderCashesDtoBusiness])}
          paginationProps={paginationProps}
        />
        {
          this.props.nsfLevel && <>
            <CustomCard title="先享后付评级" style={{ marginBottom: 20 }} />
            <Table columns={scoreTableCol11} dataSource={onTableData([{}])} pagination={false} /></>
        }

        <CustomCard title="押金支付" style={{ marginBottom: 20 }} />
        <AntTable
          columns={columns}
          dataSource={yajinZhifuList}
          paginationProps={paginationProps}
        />
        <CustomCard title="修改记录" style={{ marginBottom: 20 }} />
        <AntTable
          columns={columnsLogs}
          dataSource={onTableData(depositData && depositData.logs)}
          paginationProps={paginationProps}
        />
      </>
    );
  }
  renderExpressTab() {
    const { receiptExpressInfo, giveBackExpressInfo } = this.props;
    return (
      <div>
        {giveBackExpressInfo ? (
          <>
            <Descriptions title={<CustomCard title="归还物流信息" />}>
              <Descriptions.Item label="发货物流公司">
                {giveBackExpressInfo.expressCompany}
              </Descriptions.Item>
              <Descriptions.Item label="发货物流单号">
                {giveBackExpressInfo.expressNo}
              </Descriptions.Item>
              <Descriptions.Item label="归还时间">
                {giveBackExpressInfo.deliveryTime}
              </Descriptions.Item>
            </Descriptions>
            <Button onClick={() => this.onLogistics('归还物流信息', giveBackExpressInfo)}>
              查看物流
            </Button>
            <Divider />
          </>
        ) : null}
        {receiptExpressInfo ? (
          <>
            <Descriptions title={<CustomCard title="发货物流信息" />}>
              <Descriptions.Item label="发货物流公司">
                {receiptExpressInfo.expressCompany}
              </Descriptions.Item>
              <Descriptions.Item label="发货物流单号">
                {receiptExpressInfo.expressNo}
              </Descriptions.Item>
              <Descriptions.Item label="发货时间">
                {receiptExpressInfo.deliveryTime}
              </Descriptions.Item>
            </Descriptions>
            <Button onClick={() => this.onLogistics('发货物流信息', receiptExpressInfo)}>
              查看物流
            </Button>
            <Divider />
          </>
        ) : null}
      </div>
    );
  }



  renderBillTab() {
    const {
      userOrderCashesDto = {},
      orderByStagesDtoList = [],
      orderStagesBillLogList = [],
      orderBuyOutDto = {},
      settlementInfoDto = {},
    } = this.state.queryOrderStagesDetail;
    const { signState,zxState } = this.props;
    const productInfoList = this.props.productInfo || []
    const productInfoObj = productInfoList[0] || {} // 接口返回的productInfo对象
    let mstatus = ["待生效", "待支付", "代扣成功", "代扣失败", "已退款", "已取消"]

    //分期
    const columnsByStages = [
      {
        title: '总期数',
        dataIndex: 'totalPeriods',
      },
      {
        title: '当前期数',
        dataIndex: 'currentPeriods',
      },
      {
        title: '租金',
        dataIndex: 'currentPeriodsRent',
      },
      {
        title: '已支付金额',
        render: e => {
          if (e.status == 2 || e.status == 3) {
            return e.currentPeriodsRent
          } else if (e.status == 8) return e.beforePrice
          return
        }
      },
      {
        title: '状态',
        render: e => <>
          <span>
            {columnsByStagesStute[e.status]}
          </span>
        </>
        ,
      },

      {
        title: '支付方式',
        dataIndex: 'payment',
        render: (text, record, index) => {
          return (
            <>
              <span>{PayMent[text]}</span>
            </>
          )
        }
      },
      {
        title: '支付时间',
        dataIndex: 'repaymentDate',
      },
      {
        title: '账单到期时间',
        dataIndex: 'statementDate',
      },
      {
        title: '蚂蚁状态',
        dataIndex: 'mstatus',
        render: (text, record, index) => {
          return (
            <>
              <span>{text ? mstatus[text] : ''}</span>
            </>
          )
        }
      },
      {
        title: '操作',
        width: 220,
        render: (_, record) => {
          return (
            <>
              <div>
                {/* <Button type="primary" size="small" style={{ fontSize: 10 }} onClick={() => {
                  //if(currentPeriods==0)return;
                  const reqParams = {
                    currentPeriods: record.currentPeriods,
                    deductionMethodType: "AUTH",
                    orderId: record.orderId,
                  } // 请求参数
                  const url = "/hzsx/ope/order/stageOrderWithhold"
                  const method = "post"
                  request(url, reqParams, method).then(() => {
                    message.info("处理完成", 5)
                  }).catch(e => {
                    console.log("eee", e)
                  })

                }}>发起代扣</Button> */}
                <Button type="primary" style={{ fontSize: 10 }} size="small" onClick={() => {
                  let bod = {
                    record,
                    visible: true,
                    orderVisible: "uptime"
                  }
                  console.log("修改时间,", bod)
                  this.setState(bod)
                }}>修改时间</Button>
              </div>
              <div style={{ marginTop: 10 }}>
                <Button type="danger" style={{ fontSize: 10 }} size="small" onClick={() => {
                  let bod = {
                    record,
                    visible: true,
                    orderVisible: "upprice"
                  }
                  console.log("修改金额,", bod)
                  this.setState(bod)
                }}>修改金额</Button>
                <Button type="danger" style={{ fontSize: 10 }} size="small" onClick={() => {
                  let bod = {
                    record,
                    visible: true,
                    orderVisible: "before"
                  }
                  console.log("修改时间,", bod)
                  this.setState(bod)
                }}>部分还款</Button>

              </div>
              <div style={{ marginTop: 10 }}>
                {signState && record.refundStatus != 1 && (record.mstatus == 2 || !record.mstatus) ? (<Button type="danger" style={{ fontSize: 10 }} size="small" onClick={() => {
                  console.log("执行蚂蚁退款", record)
                  //if(currentPeriods==0)return;
                  const reqParams = {
                    currentPeriods: record.currentPeriods,
                    deductionMethodType: "AUTH",
                    orderId: record.orderId,
                  } // 请求参数
                  const url = "/hzsx/ope/order/mylRefund"
                  const method = "post"
                  request(url, reqParams, method).then(() => {
                    message.info("处理完成", 5)
                  }).catch(e => {
                    console.log("eee", e)
                  })
                }}>蚂蚁退款</Button>) : <></>}
              </div>
            </>
          )
        }

      }
    ];
    const columnsByStagesLog = [
      {
        title: '当前期数',
        dataIndex: 'currentPeriods',
      },
      {
        title: '金额',
        dataIndex: 'beforePrice',
      },
      {
        title: '图片',
        render: e => <>
          <img
            style={{ width: 116, height: 62, marginRight: 20 }}
            onClick={() => this.onKaiImg(e.image)}
            src={e.image} />
        </>

      },
      {
        title: '支付时间',
        dataIndex: 'createTime',
      },
    ];

    return (
      <Card bordered={false}>
        {settlementInfoDto ? (
          <>
            <CustomCard title="结算信息" style={{ marginBottom: 20 }} />
            <AntTable
              columns={columnsSettlement}
              dataSource={onTableData([settlementInfoDto])}
              paginationProps={paginationProps}
            />
          </>
        ) : null}

        <CustomCard title="账单信息(金额单位：元)" style={{ marginBottom: 20 }} />

        <AntTable
          columns={columnsBill}
          dataSource={onTableData([this.state.userOrderCashesDtoBusiness])}
          paginationProps={paginationProps}
        />

        <CustomCard title="分期信息" style={{ marginBottom: 20 }} />
        <AntTable
          isLimitHeightTable={true}
          columns={columnsByStages}
          dataSource={onTableData(orderByStagesDtoList)}
          paginationProps={paginationProps}
        />
        <CustomCard title="部分还款记录" style={{ marginBottom: 20 }} />
        <AntTable
          isLimitHeightTable={true}
          columns={columnsByStagesLog}
          dataSource={onTableData(orderStagesBillLogList)}
          paginationProps={paginationProps}
        />
        {orderBuyOutDto.orderId ? (
          <>
            <CustomCard title="买断信息" style={{ marginBottom: 20 }} />
            <AntTable
              columns={orderBuyOutDto.payFlag ? columnsBuyOutYes : columnsBuyOutNo}
              dataSource={onTableData([orderBuyOutDto])}
              paginationProps={paginationProps}
            />
          </>
        ) : null}

        <div style={{ marginBottom: "20px" }}>
          <Descriptions
            title={
              <>
                <CustomCard title="交易快照(金额单位：元)" />
              </>
            }
          >
            <Descriptions.Item label="销售价">{productInfoObj.salePrice || defaultPlaceHolder}</Descriptions.Item>
          </Descriptions>
        </div>
      </Card>
    );
  }



  renderProcessTab() {
    const { processDetail = [] } = this.state;
    const { userOrderInfoDto = {} } = this.props;
    return (
      <Card
        className="remove-card-bottom-border"
        title={<CustomCard title="订单进度" />}
        bordered={false}
      >
        <Steps
          direction="vertical"
          progressDot={document.body.clientWidth >= 1025}
          current={processDetail.length}
        >
          {processDetail.map(item => {
            const desc = (
              <div style={{ width: 300 }}>
                <div>{item.operatorName}</div>
                <div>{item.createTime}</div>
              </div>
            );
            return <Step title={item.operate} key={item.operate} description={desc} />;
          })}
        </Steps>
      </Card>
    );
  }

  renderContentTabCard() {
    const { userOrderInfoDto } = this.props;
    const { activeTab } = this.state;
    const status = userOrderInfoDto.status;

    return (
      <Card bordered={false} style={{ marginTop: 20 }}>
        <Tabs activeKey={activeTab} onChange={this.tabChange} animated={false}>
          <TabPane tab="订单信息" key="1">
            {this.renderOrderInfoTab()}
          </TabPane>

          <TabPane tab="风控报告" key="2">
            <>
              {
                this.state.showRiskReportCallBtn && (
                  <Button onClick={() => this.viewCredit("01")} type="primary">点击查询</Button>
                )
              }
              {this.renderRiskTab()}
            </>
          </TabPane>

          {/*  {!['02', '01'].includes(status) ? (
            <TabPane tab="百融三要素(测试)" key="13">
              <>
                {
                  this.state.showRiskReportBaironBtn1 && (
                    <Button onClick={this.getbairon1} type="primary">点击查询</Button>
                  )
                }
                { this.renderRiskbaironTab1() }
              </>
            </TabPane>
          ) : null} */}

          {/* <TabPane tab="云信-上报征信" key="333">
            <>
              {
                this.state.showCreditUploadInfoView && (
                  this.renderCreditApplyInfoTab()
                )
              }
            </>
          </TabPane> */}
          <TabPane tab="雷达" key="332">
            <Xytx sfz={userOrderInfoDto} />
          </TabPane>
          <TabPane tab="探针" key="331">
            <Xytz sfz={userOrderInfoDto} />
          </TabPane>

          {/* {!['02', '01'].includes(status) ? (
            <TabPane tab="百融报告" key="12">
              <>
                {this.renderRiskbaironTab1()}
              </>
            </TabPane>
          ) : null} */}


          {!['01', '02', '11'].includes(status) ? (
            <TabPane tab="审批结论" key="3">
              {this.renderAuditTab()}
            </TabPane>
          ) : null}
          {!['01', '02', '04', '11'].includes(status) ? (
            <TabPane tab="物流信息" key="4">
              {this.renderExpressTab()}
            </TabPane>
          ) : null}
          <TabPane tab="押金管理" key="11">
            {this.renderDeposit()}
          </TabPane>
          <TabPane tab="账单信息" key="5">
            {this.renderBillTab()}
          </TabPane>
          {status == '06' ? (
            <TabPane tab="催收记录" key="10">
              {this.renderCollectionRecord()}
            </TabPane>
          ) : null}

          <TabPane tab="流程进度" key="6">
            {this.renderProcessTab()}
          </TabPane>
        </Tabs>
      </Card>
    );
  }
  renderCollectionRecord() {
    const { orderVisible } = this.state;

    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <CustomCard title="商家催收" style={{ marginBottom: 20 }} />
        <MyPageTable
          onPage={this.onHastenBusiness}
          columns={BusinessCollection}
          dataSource={onTableData(this.state.HastenList)}
          paginationProps={{
            current: this.state.HastenCurrent,
            pageSize: 3,
            total: this.state.HastenTotal,
          }}
        />
      </div>
    );
  }
  renderCollectionRecordModal() {
    const { orderVisible } = this.state;
    const { getFieldDecorator } = this.props.form;
    const orderId = getParam('id');

    const handleOk = e => {
      e.preventDefault();
      this.props.form.validateFields(['jg', 'xj'], (err, values) => {
        if (!err) {
          orderService.orderHasten({ orderId, notes: values.xj, result: values.jg }).then(res => {
            this.onHasten(1, 3, '02');
            this.handleCancel();
          });
        }
      });
    };

    return (
      <Modal
        title="记录催收"
        visible={orderVisible === 'remarks'}
        onOk={handleOk}
        onCancel={this.handleCancel}
        destroyOnClose
      >
        <Form>
          <Form.Item label="结果" {...formItemLayout}>
            {getFieldDecorator('jg', {
              rules: [{ required: true, message: '请选择结果' }],
            })(
              <Select style={{ width: '100%' }} placeholder="请选择结果">
                <Option value="01">承诺还款</Option>
                <Option value="02">申请延期还款</Option>
                <Option value="03">拒绝还款</Option>
                <Option value="04">电话无人接听</Option>
                <Option value="05">电话拒接</Option>
                <Option value="06">电话关机</Option>
                <Option value="07">电话停机</Option>
                <Option value="08">客户失联</Option>
              </Select>,
            )}
          </Form.Item>
          <Form.Item label="小记" {...formItemLayout}>
            {getFieldDecorator('xj', {
              rules: [{ required: true, message: '请输入小记' }],
            })(<TextArea placeholder="请输入" />)}
          </Form.Item>
        </Form>
      </Modal>
    );
  }
  renderRemarkModal() {
    const { orderVisible } = this.state;
    const { getFieldDecorator } = this.props.form;
    const orderId = getParam('id');
    const handleOk = e => {
      e.preventDefault();
      this.props.form.validateFields(['beizhu'], (err, values) => {
        if (!err) {
          const { dispatch } = this.props;
          dispatch({
            type: 'order/orderRemark',
            payload: {
              orderId,
              remark: values.beizhu,
              orderType: '01',
            },
            callback: res => {
              this.onPageBusiness();
              this.handleCancel();
              this.props.form.resetFields();
            },
          });
        }
      });
    };

    return (
      <div>
        <Modal
          title="备注"
          visible={orderVisible === 'remark'}
          onOk={handleOk}
          onCancel={this.handleCancel}
        >
          <Form>
            <Form.Item label="备注内容" {...formItemLayout}>
              {getFieldDecorator('beizhu', {
                rules: [{ required: true, message: '请输入备注' }],
              })(<TextArea placeholder="请输入" />)}
            </Form.Item>
          </Form>
        </Modal>
      </div>
    );
  }

  renderAddCostPriceModal() {
    const { orderVisible } = this.state;
    const { getFieldDecorator } = this.props.form;
    const orderId = getParam('id');
    const handleOk = e => {
      e.preventDefault();
      this.props.form.validateFields(['cost_price'], (err, values) => {
        if (!err) {
          console.log("orderid:", orderId, values)
          values.orderId = orderId
          values.costPrice = values.cost_price
          request(`/hzsx/business/order/addcostPrice`, values).then(res => {
            console.log("rcs", res)
            this.handleCancel();
            message.success("添加完成", 5)
          })
        }
      });
    };


  }

  renderuptimeModal() {
    const { orderVisible, record } = this.state;
    const { form } = this.props;
    const { getFieldDecorator } = this.props.form;
    const orderId = getParam('id');
    const handleOk = e => {
      e.preventDefault();
      this.props.form.validateFields(['zdr'], (err, values) => {
        if (!err) {
          let uptime = moment(values.zdr).format('YYYY-MM-DD hh:mm:ss')
          const orderId = record.orderId;
          const currentPeriods = record.currentPeriods;
          const reqParams = {
            currentPeriods,
            uptime,
            orderId,
          } // 请求参数
          console.log("修改账单时间", values, reqParams)
          const url = "/hzsx/ope/order/stageOrderUptime"
          request(url, reqParams, "post").then(() => {
            message.info("处理完成", 5)
            this.setState({
              visible: false,
              orderVisible: false,
            });
          }).catch(e => {
            console.log("eee", e)
          })
        }
      });
    };
    console.log(record, "record")
    return (
      <div>
        <Modal
          title={'修改账单时间'}
          visible={orderVisible === 'uptime'}
          onOk={handleOk}
          onCancel={this.handleCancel}
        >
          <Form>
            <Form.Item label="账单时间" {...formItemLayout}>
              {form.getFieldDecorator('zdr', {
                rules: [{ required: true, message: '请选择账单时间' }],
              })(
                <DatePicker
                  style={{ width: '100%' }}
                  format="YYYY-MM-DD HH:mm:ss"
                  placeholder="请选择账单时间"
                />,
              )}
            </Form.Item>
          </Form>
        </Modal>
      </div>
    );
  }
  renderuppriceModal() {
    const { orderVisible, record } = this.state;
    const { form } = this.props;
    const { getFieldDecorator } = this.props.form;
    const orderId = getParam('id');
    const handleOk = e => {
      e.preventDefault();
      this.props.form.validateFields(['price'], (err, values) => {
        if (!err) {
          let price = values.price
          const orderId = record.orderId;
          const currentPeriods = record.currentPeriods;
          const reqParams = {
            currentPeriods,
            price,
            orderId,
          } // 请求参数
          console.log("修改账单时间", values, reqParams)
          const url = "/hzsx/ope/order/stageOrderUpprice"
          request(url, reqParams, "post").then(() => {
            message.info("处理完成", 5)
            this.setState({
              visible: false,
              orderVisible: false,
            });
          }).catch(e => {
            console.log("eee", e)
          })
        }
      });
    };
    console.log(record, "record")
    return (
      <div>
        <Modal
          title={'修改账单金额'}
          visible={orderVisible === 'upprice'}
          onOk={handleOk}
          onCancel={this.handleCancel}
        >
          <Form>
            <Form.Item label="金额" {...formItemLayout}>
              {form.getFieldDecorator('price', {})(
                <InputNumber placeholder="账单的金额" style={{ width: '90%' }} />,
              )}
            </Form.Item>
          </Form>
        </Modal>
      </div>
    );
  }
  //部分代扣
  renderbeforeModal() {

    const { orderVisible, record, imgList } = this.state;
    const { form } = this.props;
    const { getFieldDecorator } = this.props.form;
    const orderId = getParam('id');
    const handleOk = e => {
      e.preventDefault();
      this.props.form.validateFields(['before'], (err, values) => {
        if (!err) {
          const orderId = record.orderId;
          const currentPeriods = record.currentPeriods;

          const reqParams = {
            currentPeriods,
            pic: imgList[0].response.data,
            beforePrice: values.before,
            orderId,
          } // 请求参数
          console.log("imgList", imgList)
          console.log("reqParams", reqParams)
          //return;
          //如果部分还款的金额大于应还金额，报一下错？，如果等于应还金额，直接后台修改成已支付？好像不需要这个
          console.log("部分扣款", values, reqParams)
          const url = "/hzsx/ope/order/stageOrderBefore"
          request(url, reqParams, "post").then(() => {
            message.info("处理完成", 5)
            this.setState({
              visible: false,
              orderVisible: false,
            });
          }).catch(e => {
            console.log("eee", e)
          })
        }
      });
    };
    console.log(record, "record")
    return (
      <div>
        <Modal
          title={'部分支付'}
          visible={orderVisible === 'before'}
          onOk={handleOk}
          onCancel={this.handleCancel}
        >
          <Form>
            <FormItem>
              {form.getFieldDecorator('before', {
                rules: [{ required: true, message: '线下一共支付的金额' }],
              })(<InputNumber placeholder="线下一共支付的金额" style={{ width: '90%' }} />)}
            </FormItem>
          </Form>

          <div className="rowsdsjk12">
            <span className="labelsds121s">凭证：</span>
            <Upload
              accept="image/*"
              action="/hzsx/busShop/doUpLoadwebp"
              listType="picture-card"
              headers={{
                token: getToken(),
              }}
              fileList={this.state.imgList}
              beforeUpload={this.beforeUpload}
              onChange={this.handleUploadImage}
            >
              {
                !this.state.imgList || !this.state.imgList.length && (
                  <div>
                    <Icon type="upload" />
                    <div className="ant-upload-text">上传照片</div>
                  </div>
                )
              }
            </Upload>
          </div>
        </Modal>
      </div>
    );
  }

  /**
   * 改变发货方式的时候触发
   * @param {*} event
   */
  changeSendMethod = event => {
    this.setState({ sendMethod: event.target.value });
  };

  renderDeliverModal() {
    const { form } = this.props;
    const { orderVisible, deliverOptions } = this.state;
    const { getFieldDecorator } = form;
    const orderId = getParam('id');

    const handleOk = e => {
      e.preventDefault();

      this.props.form.validateFields(['expressId', 'expressNo',], (err, fieldsValue) => {
        if (err) return;
        const resetCb = () => {
          this.onRentRenewalDetail();
          this.props.form.resetFields();
          this.setState({ sendMethod: KUAI_DI }); // 将发货方式重置为快递
          this.handleCancel();
        };
        if (this.state.sendMethod === ZI_TI) {
          // 自提走不一样的流程
          OrderService.expressZitiHandler(orderId).then(res => {
            resetCb();
          });
          return;
        }
        OrderService.orderDelivery({
          ...fieldsValue,
          orderId,
        }).then(res => {
          resetCb();
        });
      });
    };

    return (
      <Modal
        destroyOnClose
        title={orderVisible === 'express' ? '修改物流信息' : '快速发货'}
        visible={orderVisible === 'deliver' || orderVisible === 'express'}
        onOk={handleOk}
        width="450px"
        onCancel={this.handleCancel}
      >
        <Form labelCol={{ span: 5 }} labelAlign="left" wrapperCol={{ span: 19 }}>
          {this.props.contractUrl ? null : (
            <div style={{ color: '#1890ff', marginBottom: 10 }}>
              注意：该订单尚未签约存证，发货后无法签约，请确认！
            </div>
          )}
          {/** 修改物流信息的时候不显示发货方式 */}
          {orderVisible === 'deliver' && (
            <FormItem label="发货方式">
              <Radio.Group value={this.state.sendMethod} onChange={this.changeSendMethod}>
                <Radio value={KUAI_DI}>快递</Radio>
                <Radio value={ZI_TI}>自提</Radio>
              </Radio.Group>
            </FormItem>
          )}

          {this.state.sendMethod === 0 && (
            <>
              <FormItem>
                {form.getFieldDecorator('expressId', {
                  rules: [{ required: true, message: '请选择物流公司' }],
                })(
                  <Select style={{ width: '100%' }} placeholder="请选择物流公司">
                    {deliverOptions.map(option => {
                      return (
                        <Option key={option.id} value={option.id}>
                          {option.name}
                        </Option>
                      );
                    })}
                  </Select>,
                )}
              </FormItem>
              <FormItem>
                {form.getFieldDecorator('expressNo', {
                  rules: [{ required: true, message: '请输入物流单号' }],
                })(<Input placeholder="请输入物流单号" />)}
              </FormItem>

            </>
          )}
        </Form>
      </Modal>
    );
  }

  renderCloseModal() {
    const { form } = this.props;
    const { orderVisible, orderId } = this.state;
    const handleOk = e => {
      this.props.form.validateFields(['closeReason', "closeMold", "istk"], (err, fieldsValue) => {
        if (err) return;
        OrderService.businessClosePayedOrder({
          ...fieldsValue,
          closeType: '06',
          orderId,
        }).then(res => {
          this.onRentRenewalDetail();
          this.props.form.resetFields();
          this.handleCancel();
        });
      });
    };

    return (
      <Modal
        destroyOnClose
        title={'关闭订单'}
        visible={orderVisible === 'close'}
        onOk={handleOk}
        width="450px"
        onCancel={this.handleCancel}
      >
        <Form>
          <FormItem label="拒绝类型" {...formItemLayout}>
            {form.getFieldDecorator('closeMold', {
              rules: [{ required: true, message: '请选择拒绝类型' }],
            })(
              <Select style={{ width: '100%' }} placeholder="请选择">
                {Object.keys(AuditReason).map(option => {
                  return (
                    <Option key={option} value={option}>
                      {AuditReason[option]}
                    </Option>
                  );
                })}
              </Select>,
            )}
          </FormItem>
          <FormItem label="是否退款" {...formItemLayout}>
            {form.getFieldDecorator('istk', {
              initialValue: 0,
              rules: [{ required: true, message: '请选择拒绝类型' }],
            })(
              <Select style={{ width: '100%' }} placeholder="请选择">
                <Option key={0} value={0}>退款</Option>
                <Option key={1} value={1}>不退</Option>
              </Select>,
            )}
          </FormItem>

          <FormItem label="小记">
            {form.getFieldDecorator('closeReason', {
              rules: [{ required: true, message: '小记必填' }],
            })(<Input placeholder="请输入关单原因" width={260} />)}
          </FormItem>
        </Form>
      </Modal>
    );
  }

  renderSettleModal() {
    const { form, userOrderCashesDto = {} } = this.props;
    const { orderId, orderVisible, radioValue, damageValue } = this.state;

    const onRadioChange = e => {
      this.setState({
        radioValue: e.target.value,
      });
    };

    const handleDamageValue = val => {
      this.setState({
        damageValue: val,
      });
    };

    const handleOk = e => {
      this.props.form.validateFields(['penaltyAmount', 'cancelReason'], (err, fieldsValue) => {
        console.log("err:", err)
        if (err) return;
        const payload = {
          lossAmount: 0,
          damageAmount: 0,
          penaltyAmount: 0,
          settlementType: radioValue,
          ...fieldsValue,
          orderId,
        };
        //return console.log(payload,"payload")
        if (radioValue === '03') {
          payload.lossAmount = damageValue;
        } else if (radioValue === '02') {
          payload.damageAmount = damageValue;
        }
        OrderService.merchantsIssuedStatements(payload).then(res => {
          this.onRentRenewalDetail();
          this.props.form.resetFields();
          this.handleCancel();
        });
      });
    };

    return (
      <Modal
        title="是否确认结算？"
        zIndex={2000}
        visible={orderVisible === 'settle'}
        onOk={handleOk}
        onCancel={this.handleCancel}
      >
        <div>
          <div>
            <span style={{ marginRight: '20px' }}>宝贝状态</span>
            <Radio.Group onChange={onRadioChange} value={radioValue}>
              <Radio value="01">完好</Radio>
              <Radio value="02">损坏</Radio>
              <Radio value="03">丢失</Radio>
              <Radio value="04">其他</Radio>
            </Radio.Group>
            {radioValue === '02' && (
              <div style={{ display: 'inline-block', marginTop: '20px' }}>
                <span>损坏赔偿金：</span>
                <InputNumber
                  min={0}
                  defaultValue={0}
                  value={damageValue}
                  onChange={handleDamageValue}
                />
                <span>元</span>
              </div>
            )}
            {radioValue === '03' && (
              <div style={{ display: 'inline-block', marginTop: '20px' }}>
                <span>丢失赔偿金：</span>
                <InputNumber
                  min={0}
                  defaultValue={0}
                  value={damageValue}
                  onChange={handleDamageValue}
                />
                <span>元</span>
              </div>
            )}
            {radioValue === '04' && (
              <div style={{ marginTop: '20px' }}>
                <Form.Item labelCol={{ span: 4 }} label="违约金：">
                  {form.getFieldDecorator('penaltyAmount', {
                    initialValue: userOrderCashesDto.penaltyAmount,
                  })(<InputNumber min={0} />)}
                  <span> 元</span>
                </Form.Item>
                <Form.Item labelCol={{ span: 4 }} label="违约原因：">
                  {form.getFieldDecorator('cancelReason', {
                    initialValue: userOrderCashesDto.cancelReason,
                  })(<Input style={{ width: '60%' }} placeholder="请输入违约原因" />)}
                </Form.Item>
              </div>
            )}
          </div>
        </div>
      </Modal>
    );
  }

  renderAddressModal() {
    const {
      userOrderInfoDto = {},
      orderAddressDto,
      dispatch,
      productInfo,
      userOrderCashesDto,
      orderBuyOutDto,
      wlList,
    } = this.props;
    const { orderVisible, orderId } = this.state;
    const { getFieldDecorator } = this.props.form;
    const handleOk = e => {
      this.props.form.validateFields(['realName', 'street', 'city', 'telephone'], (err, values) => {
        if (err) return;
        dispatch({
          type: 'order/opeOrderAddressModify',
          payload: {
            realName: values.realName,
            street: values.street,
            telephone: values.telephone,
            province: values && values.city && values.city[0],
            city: values && values.city && values.city[1],
            area: values && values.city && values.city[2],
            orderId,
          },
          callback: res => {
            this.onRentRenewalDetail();
            this.props.form.resetFields();
            this.handleCancel();
          },
        });
        // OrderService.businessClosePayedOrder({
        //   ...fieldsValue,
        //   orderId,
        // }).then(res => {
        //   this.onRentRenewalDetail()
        //   this.props.form.resetFields();
        //   this.handleCancel();
        // });
      });
    };

    return (
      <Modal
        title="修改收货信息"
        visible={orderVisible === 'address'}
        onOk={handleOk}
        onCancel={this.handleCancel}
      >
        <Form>
          <Form.Item label="所在城市" {...formItemLayout}>
            {getFieldDecorator('city', {
              rules: [
                {
                  required: true,
                  message: '请输入备注',
                },
              ],
              initialValue: [
                orderAddressDto && orderAddressDto.province && orderAddressDto.province.toString(),
                orderAddressDto && orderAddressDto.city && orderAddressDto.city.toString(),
                orderAddressDto && orderAddressDto.area && orderAddressDto.area.toString(),
              ],
            })(
              <Cascader
                options={optionsdata}
                fieldNames={{ label: 'name', value: 'value', children: 'subList' }}
              />,
            )}
          </Form.Item>
          <Form.Item label="收货人姓名" {...formItemLayout}>
            {getFieldDecorator('realName', {
              rules: [{ required: true, message: '请输入备注' }],
              initialValue: orderAddressDto && orderAddressDto.realname,
            })(<Input placeholder="请输入" />)}
          </Form.Item>
          <Form.Item label="收货人手机号" {...formItemLayout}>
            {getFieldDecorator('telephone', {
              rules: [{ required: true, message: '请输入备注' }],
              initialValue: orderAddressDto && orderAddressDto.telephone,
            })(<Input placeholder="请输入" />)}
          </Form.Item>
          <Form.Item label="详细地址" {...formItemLayout}>
            {getFieldDecorator('street', {
              rules: [{ required: true, message: '请输入备注' }],
              initialValue: orderAddressDto && orderAddressDto.street,
            })(<TextArea placeholder="请输入" />)}
          </Form.Item>
        </Form>
      </Modal>
    );
  }

  renderAuditModal() {
    const { form } = this.props;
    const { orderVisible, orderId, titles } = this.state;
    const handleOk = e => {
      this.props.form.validateFields(['refuseType', 'auditRemark'], (err, fieldsValue) => {
        if (err) return;
        OrderService.telephoneAuditOrder({
          ...fieldsValue,
          orderId,
          remark: fieldsValue.auditRemark,
          orderAuditStatus: titles === '审批通过' ? '01' : '02',
        }).then(res => {
          this.onRentRenewalDetail();
          this.props.form.resetFields();
          this.handleCancel();
        });
      });
    };

    return (
      <Modal
        destroyOnClose
        title={titles}
        visible={orderVisible === 'audit'}
        onOk={handleOk}
        width="450px"
        onCancel={this.handleCancel}
      >
        <Form>
          {titles === '审批拒绝' ? (
            <FormItem label="拒绝类型" {...formItemLayout}>
              {form.getFieldDecorator('refuseType', {
                rules: [{ required: true, message: '请选择拒绝类型' }],
              })(
                <Select style={{ width: '100%' }} placeholder="请选择">
                  {Object.keys(AuditReason).map(option => {
                    return (
                      <Option key={option} value={option}>
                        {AuditReason[option]}
                      </Option>
                    );
                  })}
                </Select>,
              )}
            </FormItem>
          ) : null}
          <FormItem label="小记" {...formItemLayout}>
            {form.getFieldDecorator('auditRemark', {
              rules: [{ required: true, message: '请输入小记' }],
            })(<Input.TextArea placeholder="请输入小记" />)}
          </FormItem>
        </Form>
      </Modal>
    );
  }
  onConfirm = () => {
    const { orderId } = this.state;
    if (this.loading) return // 点击中也不处理
    this.setState({ isloading: true });
    this.loading = true
    busShopService.generateOrderContract({ orderId }).then(res => {
      this.setState({ isloading: false });
      if (res) {
        message.success('签约成功');
        this.onRentRenewalDetail();
        this.loading = false
      }
    });
  };

  seezx = () => {
    console.log("查看征信")
    this.setState({ isloading: true });
    const { userOrderInfoDto } = this.props
    let req = {
      "uid": userOrderInfoDto.uid,
      "orderId": userOrderInfoDto.orderId,
      "phone": userOrderInfoDto.telephone,
      "idCardNo": userOrderInfoDto.idCard,
      "userName": userOrderInfoDto.realName
    }

    request(`/hzsx/business/order/queryQdzx`, req).then(res => {
      this.setState({ isloading: false });
      console.log("ressss,", res)
    })
  };


  /**
   * 归还的处理
   */
  returnBackHandler = () => {
    this.setState({ showConfirmOrderReturnModal: true });
  };

  /**
   * 取消归还的回调方法
   */
  cancelReturnHandler = () => {
    this.setState({ showConfirmOrderReturnModal: false });
  };

  /**
   * 渲染归还的弹窗
   */
  renderConfirmOrderReturnModal = () => {
    const { form } = this.props;

    // 确认归还的回调
    const confirmReturnHandler = e => {
      e.preventDefault();
      this.props.form.validateFields(['returnTime', 'expressId', 'expressNo'], (err, values) => {
        if (!err) {
          if (this.confirmReturning) return;
          this.confirmReturning = true;
          const data = {
            expressId: values.expressId,
            expressNo: values.expressNo,
            orderId: this.state.orderId,
            returnTime: moment(values.returnTime).format('YYYY-MM-DD hh:mm:ss'),
          };
          orderService
            .confirmReturnOrder(data)
            .then(res => {
              this.onRentRenewalDetail(); // 加载商品详情数据
              this.cancelReturnHandler();
            })
            .finally(() => {
              this.confirmReturning = false;
            });
        }
      });
    };

    return (
      <Modal
        title="确认归还"
        visible={this.state.showConfirmOrderReturnModal}
        onOk={confirmReturnHandler}
        onCancel={this.cancelReturnHandler}
        destroyOnClose
      >
        <Form>
          <Form.Item label="归还时间" {...formItemLayout}>
            {form.getFieldDecorator('returnTime', {
              rules: [{ required: true, message: '请选择归还时间' }],
            })(
              <DatePicker
                style={{ width: '100%' }}
                format="YYYY-MM-DD HH:mm:ss"
                placeholder="请选择归还时间"
              />,
            )}
          </Form.Item>
          <Form.Item label="物流公司" {...formItemLayout}>
            {form.getFieldDecorator('expressId', {
              rules: [{ required: true, message: '请选择物流公司' }],
            })(
              <Select style={{ width: '100%' }} placeholder="请选择物流公司">
                {this.state.deliverOptions.map(option => {
                  return (
                    <Option key={option.id} value={option.id}>
                      {option.name}
                    </Option>
                  );
                })}
              </Select>,
            )}
          </Form.Item>
          <Form.Item label="物流单号" {...formItemLayout}>
            {form.getFieldDecorator('expressNo', {
              rules: [{ required: true, message: '请输入物流单号' }],
            })(<Input placeholder="请输入" />)}
          </Form.Item>
        </Form>
      </Modal>
    );
  };

  /**
   * 征信drawer
   */
  returnCreditDrawerHandler = () => {
    const uoiObj = this.props.userOrderInfoDto || {}
    const uid = uoiObj.uid;
    const phone = uoiObj.telephone
    const idCardNo = uoiObj.idCard
    const idCardFrontUrl = uoiObj.idCardFrontUrl
    const idCardBackUrl = uoiObj.idCardBackUrl;
    const userName = uoiObj.realName
    this.setState({ isloading: true });
    //需要一个总租金
    // const orderId = getParam('id');
    // console.log('看下参数信息',uid,phone,idCardNo,userName)
    yunxinCreditService.queryCreditReportByUid({
      uid,
      orderId: getParam('id'),
      phone,
      loanAmount: this.state.userOrderCashesDtoBusiness.totalRent,
      idCardNo,
      userName,
      idCardFrontUrl,
      idCardBackUrl
    }
    ).then(res => {
      let response = JSON.parse(res);
      // console.log('查询征信返回',response)
      if (!response.resp_code == 'SW0000') {
        message.error(response.resp_msg);
        return;
      }
      this.setState({
        creditAccountInfo: response.resp_data,
        isloading: false,
        showCreditDrawer: true
      });

    });
  };
  cancelCreditDrawerHandler = () => {
    this.setState({ showCreditDrawer: false });
  };



  renderOrderListDrawer() {
    let { showOrder } = this.state;
    const { userOrderInfoDto = {}, } = this.props;
    let columnOrder = [
      {
        title: '订单编号',
        dataIndex: 'orderId',
        width: 130,
        render: (text, record, index) => {
          return (
            <>
              <span style={{ color: "red" }}>{record.children == null ? null : `(共${record.children.length + 1}单) `}</span>
              <span>{text}</span>
            </>
          )
        }
      },

      {
        title: '创建时间',
        dataIndex: 'createTime',
        width: 120,
      },

      {
        title: '审核人',
        dataIndex: 'rname',
        width: 80,
      },
      {
        title: '订单状态',
        dataIndex: 'status',
        width: 120,
        render: (text, record, index) => orderStatusMap[text],
      },
      {
        title: '订单备注',
        dataIndex: 'orderRemark',
        width: 120,
      },
      {
        title: '操作',
        width: 80,
        fixed: 'right',
        align: 'center',
        render: (e, record) => {
          return (
            <div>
              <a
                className="primary-color"
                href={`#/Order/HomePage/Details?id=${e.orderId}`}
                target="_blank"
              >
                处理
              </a>
            </div>
          );
        },
      },
    ]
    console.log("u111serorderinfo11", userOrderInfoDto, showOrder)
    let onClose = () => {
      this.setState({ showOrder: false })
    }
    return (userOrderInfoDto ? <Drawer
      width={720}
      title='历史订单'
      placement="right"
      onClose={onClose}
      visible={showOrder}>
      <AntTable
        isLimitHeightTable={true}
        columns={columnOrder}
        dataSource={onTableData(userOrderInfoDto.list)}
        paginationProps={{
          current: 1,
          pageSize: 1000,
          total: 1,
        }}
      />
    </Drawer> : null)
  }

  renderCreditDrawer() {
    let creditAccountInfo = this.state.creditAccountInfo;
    console.log("征信返回的信息：", creditAccountInfo)
    if (!creditAccountInfo) {
      return;
    }
    let db = handzx(creditAccountInfo)

    return (
      <Drawer
        title="征信报告"
        width="60%"
        placement="right"
        closable={true}
        onClose={this.cancelCreditDrawerHandler}
        visible={this.state.showCreditDrawer}>

        <Descriptions title={"账户情况-账户总数" + db.var2_25}></Descriptions>
        <AntTable columns={db.v账龄.column}
          dataSource={db.v账龄.val} />

        <AntTable columns={db.v开户数.column}
          dataSource={db.v开户数.val} />
        <AntTable columns={db.v最近月份.column}
          dataSource={db.v最近月份.val} />
        <AntTable columns={db.v其它数据}
          dataSource={[db]} />
        <AntTable columns={db.v其它数据1}
          dataSource={[db]} />
        <AntTable columns={db.v其它数据2}
          dataSource={[db]} />
        <AntTable columns={db.v其它数据3}
          dataSource={[db]} />
        <AntTable columns={db.v查询情况.column}
          dataSource={db.v查询情况.val} />
        <AntTable columns={db.v查询机构数.column}
          dataSource={db.v查询机构数.val} />
        <AntTable columns={db.v差值情况.column}
          dataSource={db.v差值情况.val} />
        <AntTable columns={db.v差值情况1.column}
          dataSource={db.v差值情况1.val} />
        <AntTable columns={db.最近24个月.column}
          dataSource={db.最近24个月.val} />
      </Drawer>
    );
  };

  /**
   * 征信drawer
   */
  returnCreditBaseInfoDrawerHandler = () => {

    const uoiObj = this.props.userOrderInfoDto || {}
    const uid = uoiObj.uid;

    yunxinCreditService.getUserCreditBaseInfo({
      uid,
    }
    ).then(res => {
      // console.log('查询征信基础信息返回',res)
      let tempData = res;
      tempData.ocrStartValidDate = moment(tempData.ocrStartValidDate)
      this.setState({ creditBaseInfo: tempData });
      this.setState({ showCreditBaseInfoDrawer: true });
    });

  };
  queren = () => {


    request("/zyj-api-web/hzsx/api/order/userConfirmReceipt", { orderId: getParam('id') }).then(res => {
      console.log("手动确认收货", res)
      message.success('更新成功')
    })

  };
  setVip = (uid) => {
    request("/zyj-api-web/hzsx/userCertification/isVip?uid=" + uid, {}, "get").then(res => {
      message.success('更新成功')
      console.log("手动确认收货", res)
    })
    console.log("要修改的uid", uid)
  }
  cancelCreditBaseInfoDrawerHandler = () => {
    this.setState({ showCreditBaseInfoDrawer: false });
  };

  renderCreditBaseInfoDrawer() {
    console.log('renderCreditBaseInfoDrawer exe')
    if (!this.state.creditBaseInfo) {
      return;
    }

    let creditBaseInfo = this.state.creditBaseInfo

    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
      },
    };

    const handleCreditBaseInfoSubmit = e => {
      e.preventDefault();

      const fileds = ['email', 'homeAddress', 'resiStatus', 'resiPc', 'resiDist', 'registerDist', 'ocrStartValidDate', 'ocrAddress', 'ocrSex', 'ocrNation', 'ocrIssueOrg', 'firstContactName', 'firstContactPhone', 'firstContactRelation', 'annualInterestRate', 'coverCharge', 'coverChargeRatio', 'marriStatus', 'spoName', 'spoIdNum', 'spoTel', 'spsCmpyNm', 'eduLevel', 'acaDegree', 'empStatus', 'cpnName', 'cpnType', 'industry', 'cpnAddr', 'occupation', 'title', 'techTitle', 'workStartDate'];

      this.props.form.validateFields(fileds, (err, values) => {
        console.log(err, "aaaa", values)
        if (!err) {
          let postData = Object.assign({}, values);
          postData.ocrStartValidDate = moment(postData.ocrStartValidDate).format('YYYY-MM-DD');
          postData.id = creditBaseInfo.id;
          yunxinCreditService.updateUserCreditBaseInfo(postData).then(res => {
            message.success('更新成功')
            this.setState({ creditBaseInfo: null });
            this.setState({ showCreditBaseInfoDrawer: false });
          });
        } else {
          message.error('请补全所有必填信息');
        }
      });
    }
    return (
      <Drawer
        title="征信基础信息编辑"
        width="50%"
        placement="right"
        closable={true}
        onClose={this.cancelCreditBaseInfoDrawerHandler}
        visible={this.state.showCreditBaseInfoDrawer}>

        <Form onSubmit={handleCreditBaseInfoSubmit}>
          <Form.Item label="邮箱" {...formItemLayout}>
            {getFieldDecorator('email', {
              initialValue: creditBaseInfo.email
            })(<Input placeholder="请输入邮箱" />)}
          </Form.Item>
          <Form.Item label="家庭地址" {...formItemLayout}>
            {getFieldDecorator('homeAddress', {
              initialValue: creditBaseInfo.homeAddress
            })(<Input placeholder="请输入家庭地址" />)}
          </Form.Item>
          <Form.Item label="居住状况" {...formItemLayout}>
            {getFieldDecorator('resiStatus', {
              initialValue: '9'
            })(
              <Select>
                <Option value="1">自置</Option>
                <Option value="2">按揭</Option>
                <Option value="3">亲属楼宇</Option>
                <Option value="4">集体宿舍</Option>
                <Option value="5">租房</Option>
                <Option value="6">共有住宅</Option>
                <Option value="12">借住</Option>
                <Option value="9">未知</Option>
              </Select>
            )}
          </Form.Item>
          <Form.Item label="居住地邮编" {...formItemLayout}>
            {getFieldDecorator('resiPc', {
              initialValue: creditBaseInfo.resiPc
            })(<Input placeholder="请输入居住地邮编" />)}
          </Form.Item>
          <Form.Item label="居住地行政区划编号" {...formItemLayout}>
            {getFieldDecorator('resiDist', {
              initialValue: creditBaseInfo.resiDist
            })(<Input placeholder="请输入居住地行政区划（市县大区号）" />)}
          </Form.Item>
          <Form.Item label="户籍所在地行政区划编号" {...formItemLayout}>
            {getFieldDecorator('registerDist', {
              initialValue: creditBaseInfo.registerDist
            })(<Input placeholder="户籍所在地行政区划编号（市县大区号）" />)}
          </Form.Item>
          <Form.Item label="ocr结果中的身份证有效期开始日期" {...formItemLayout}>
            {getFieldDecorator('ocrStartValidDate', {
              initialValue: creditBaseInfo.ocrStartValidDate
            })(<DatePicker format={'YYYY-MM-DD'} allowClear={false} />
              // <Input placeholder="请输入ocr结果中的身份证有效期开始日期" />
            )}
          </Form.Item>
          <Form.Item label="OCR结果中的地址" {...formItemLayout}>
            {getFieldDecorator('ocrAddress', {
              initialValue: creditBaseInfo.homeAddress
            })(<Input placeholder="请输入OCR结果中的地址" />)}
          </Form.Item>
          <Form.Item label="OCR结果中的性别" {...formItemLayout}>
            {getFieldDecorator('ocrSex', {
              initialValue: creditBaseInfo.ocrSex || '1'
            })(
              <Select>
                <Option value="1">男性</Option>
                <Option value="2">女性</Option>
              </Select>
            )}
          </Form.Item>
          <Form.Item label="OCR结果中的民族" {...formItemLayout}>
            {getFieldDecorator('ocrNation', {
              initialValue: creditBaseInfo.ocrNation || '汉族'
            })(<Input placeholder="请输入OCR结果中的民族" />)}
          </Form.Item>
          <Form.Item label="OCR结果中的发证机关" {...formItemLayout}>
            {getFieldDecorator('ocrIssueOrg', {
              initialValue: creditBaseInfo.ocrIssueOrg
            })(<Input placeholder="请输入OCR结果中的发证机关" />)}
          </Form.Item>
          <Form.Item label="紧急联系人姓名" {...formItemLayout}>
            {getFieldDecorator('firstContactName', {
              initialValue: creditBaseInfo.firstContactName
            })(<Input placeholder="请输入紧急联系人姓名" />)}
          </Form.Item>
          <Form.Item label="紧急联系人电话" {...formItemLayout}>
            {getFieldDecorator('firstContactPhone', {
              initialValue: creditBaseInfo.firstContactPhone
            })(<Input placeholder="请输入紧急联系人电话" />)}
          </Form.Item>
          <Form.Item label="紧急联系人关系" {...formItemLayout}>
            {getFieldDecorator('firstContactRelation', {
              initialValue: creditBaseInfo.firstContactRelation
            })(
              <Select>
                <Option value="1">父母</Option>
                <Option value="2">子女</Option>
                <Option value="3">兄弟姐妹</Option>
                <Option value="4">朋友</Option>
                <Option value="5">配偶</Option>
                <Option value="6">亲戚</Option>
                <Option value="7">同学</Option>
                <Option value="8">同事</Option>
                <Option value="9">其他</Option>
              </Select>
            )}
          </Form.Item>

          <Form.Item label="婚姻状况" {...formItemLayout}>
            {getFieldDecorator('marriStatus', {
              initialValue: creditBaseInfo.marriStatus || '99'
            })(
              <Select>
                <Option value="10">未婚</Option>
                <Option value="20">已婚</Option>
                <Option value="30">丧偶</Option>
                <Option value="40">离婚</Option>
                <Option value="99">未知</Option>
              </Select>
            )}
          </Form.Item>

          <Form.Item label="配偶姓名" {...formItemLayout}>
            {getFieldDecorator('spoName', {
              initialValue: creditBaseInfo.spoName
            })(<Input placeholder="请输入配偶姓名" />)}
          </Form.Item>
          <Form.Item label="配偶证件号码" {...formItemLayout}>
            {getFieldDecorator('spoIdNum', {
              initialValue: creditBaseInfo.spoIdNum
            })(<Input placeholder="请输入配偶证件号码" />)}
          </Form.Item>
          <Form.Item label="配偶联系电话" {...formItemLayout}>
            {getFieldDecorator('spoTel', {
              initialValue: creditBaseInfo.spoTel
            })(<Input placeholder="请输入配偶联系电话" />)}
          </Form.Item>
          <Form.Item label="配偶工作单位" {...formItemLayout}>
            {getFieldDecorator('spsCmpyNm', {
              initialValue: creditBaseInfo.spsCmpyNm
            })(<Input placeholder="请输入配偶工作单位" />)}
          </Form.Item>
          <Form.Item label="学历" {...formItemLayout}>
            {getFieldDecorator('eduLevel', {
              initialValue: creditBaseInfo.eduLevel || '99'
            })(
              <Select>
                <Option value="10">研究生</Option>
                <Option value="20">本科</Option>
                <Option value="30">大专</Option>
                <Option value="40">中专、职高、技校</Option>
                <Option value="60">高中</Option>
                <Option value="91">初中及以下</Option>
                <Option value="99">未知</Option>
              </Select>
            )}
          </Form.Item>
          <Form.Item label="学位" {...formItemLayout}>
            {getFieldDecorator('acaDegree', {
              initialValue: creditBaseInfo.acaDegree || '9'
            })(
              <Select>
                <Option value="1">名誉博士</Option>
                <Option value="2">博士</Option>
                <Option value="3">硕士</Option>
                <Option value="4">学士</Option>
                <Option value="5">无</Option>
                <Option value="9">未知</Option>
              </Select>
            )}
          </Form.Item>
          <Form.Item label="就业状况" {...formItemLayout}>
            {getFieldDecorator('empStatus', {
              initialValue: creditBaseInfo.empStatus || '99'
            })(
              <Select>
                <Option value="11">国家公务员</Option>
                <Option value="13">专业技术人员</Option>
                <Option value="21">企业管理人员</Option>
                <Option value="27">农民</Option>
                <Option value="31">学生</Option>
                <Option value="37">现役军人</Option>
                <Option value="51">自由职业者</Option>
                <Option value="54">个体经营者</Option>
                <Option value="70">无业人员</Option>
                <Option value="80">退（离）休人员</Option>
                <Option value="90">其他</Option>
                <Option value="99">未知</Option>
              </Select>
            )}
          </Form.Item>
          {/* <Form.Item label="单位名称" {...formItemLayout}>
            {getFieldDecorator('cpnName', {
              initialValue: creditBaseInfo.cpnName
            })(
              <Fragment>
                <Input placeholder="请输入单位名称" />
                <div>
                  当“就业状况”为“国家公务员、专业技术人员、职员、企业管理人员、工人”，必填
                  * 填写信息主体就职单位的名称。其他情况合作方可以不提供
                </div>
              </Fragment>
            )}
          </Form.Item> */}
          <Form.Item label="单位名称" {...formItemLayout}>
            {getFieldDecorator('cpnName', {
              initialValue: creditBaseInfo.cpnName
            })(<Input placeholder="请输入单位名称" />)}
          </Form.Item>
          <Form.Item label="单位性质" {...formItemLayout}>
            {getFieldDecorator('cpnType', {
              initialValue: creditBaseInfo.cpnType || '99'
            })(
              <Select>
                <Option value="10">机关、事业单位</Option>
                <Option value="20">国有企业</Option>
                <Option value="30">外资企业</Option>
                <Option value="40">个体、私营企业</Option>
                <Option value="50">其他（包括三资企业、民营企业、民间团体等）</Option>
                <Option value="99">未知</Option>
              </Select>
            )}
          </Form.Item>

          <Form.Item label="单位性质" {...formItemLayout}>
            {getFieldDecorator('industry', {
              initialValue: creditBaseInfo.industry || '9'
            })(
              <Select>
                <Option value="A">农、林、牧、渔业</Option>
                <Option value="B">采矿业</Option>
                <Option value="C">制造业</Option>
                <Option value="D">电力、热力、燃气及水生产和供应业</Option>
                <Option value="E">建筑业</Option>
                <Option value="F">批发和零售业</Option>
                <Option value="G">交通运输、仓储和邮储业</Option>
                <Option value="H">住宿和餐饮业</Option>
                <Option value="I">信息传输、软件和信息技术服务</Option>
                <Option value="J">金融业</Option>
                <Option value="K">房地产业</Option>
                <Option value="L">租赁和商务服务业</Option>
                <Option value="M">科学研究和技术服务业</Option>
                <Option value="N">水利、环境和公共设施管理业</Option>
                <Option value="O">居民服务、修理和其他服务业</Option>
                <Option value="P">教育</Option>
                <Option value="Q">卫生和社会工作</Option>
                <Option value="R">文化、体育和娱乐业</Option>
                <Option value="S">公共管理、社会保障和社会组织</Option>
                <Option value="T">国际组织</Option>
                <Option value="9">未知</Option>
              </Select>
            )}
          </Form.Item>
          <Form.Item label="单位详细地址" {...formItemLayout}>
            {getFieldDecorator('cpnAddr', {
              initialValue: creditBaseInfo.cpnAddr || '无'
            })(<Input placeholder="请输入单位详细地址" />)}
          </Form.Item>
          <Form.Item label="职业" {...formItemLayout}>
            {getFieldDecorator('occupation', {
              initialValue: creditBaseInfo.occupation || 'Z'
            })(
              <Select>
                <Option value="0">国家机关、党群组织、企业、事业单位负责人</Option>
                <Option value="1">专业技术人员</Option>
                <Option value="3">办事人员和有关人员</Option>
                <Option value="4">商业、服务业人员</Option>
                <Option value="5">农、林、牧、渔、水利业生产人员</Option>
                <Option value="6">生产、运输设备操作人员及有关人员</Option>
                <Option value="X">军人</Option>
                <Option value="Y">不便分类的其他从业人员</Option>
                <Option value="Z">未知</Option>
              </Select>
            )}
          </Form.Item>

          <Form.Item label="职务" {...formItemLayout}>
            {getFieldDecorator('title', {
              initialValue: creditBaseInfo.title || '9'
            })(
              <Select>
                <Option value="1">高级领导（行政级别局级及以上领导或大公司高级管理人员）</Option>
                <Option value="2">中级领导（行政级别处级领导或大公司中级管理人员）</Option>
                <Option value="3">一般员工</Option>
                <Option value="9">未知</Option>
              </Select>
            )}
          </Form.Item>

          <Form.Item label="职称" {...formItemLayout}>
            {getFieldDecorator('techTitle', {
              initialValue: creditBaseInfo.techTitle || '9'
            })(
              <Select>
                <Option value="1">高级</Option>
                <Option value="2">中级</Option>
                <Option value="3">初级</Option>
                <Option value="0">无</Option>
                <Option value="9">未知</Option>
              </Select>
            )}
          </Form.Item>

          {/*  <Form.Item label="本单位工作起始年份" {...formItemLayout}>
            {getFieldDecorator('workStartDate', {
              initialValue: creditBaseInfo.workStartDate
            })(
              <Fragment>
                <InputNumber min={1950} max={2023} />
              
                <div>
                  当“就业状况”为“国家公务员、专业技术人员、职员、企业管理人员、工人、农民、学生、现役军人”时，必填
                </div>
              </Fragment>
            )}
          </Form.Item> */}
          <Form.Item label="本单位工作起始年份" {...formItemLayout}>
            {getFieldDecorator('workStartDate', {
              initialValue: creditBaseInfo.workStartDate
            })(<InputNumber min={1950} max={2023} placeholder="当“就业状况”为“国家公务员、专业技术人员、职员、企业管理人员、工人、农民、学生、现役军人”时，必填" />)}
          </Form.Item>

          <Fragment>
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <Button type="primary" htmlType="submit">
                保存
              </Button>
            </div>
          </Fragment>

        </Form>


        <Divider />

      </Drawer>
    );
  };
  //显示复审按钮
  fsModalHandler = () => {
    this.setState({ showConfirmfsModal: true });
  };
  /**
   * 申请报送 modal
   */
  returnApplyCreditModalHandler = () => {
    this.setState({ showConfirmApplyCreditModal: true });
  };

  cancelApplyCreditModalHandler = () => {
    this.setState({ showConfirmApplyCreditModal: false });
  };
  //复审的弹窗
  renderFsModal = () => {
    const { form } = this.props;

    const btnConfirmHandler = e => {
      e.preventDefault();
      this.props.form.validateFields(['fsType', 'remark'], (err, values) => {
        values.fsType *= 1
        values.orderId = getParam('id')
        request('/hzsx/business/order/orderFsRemark', values, "post").then(res => {
          console.log("eeee", res)
          message.success('操作成功')
          this.setState({ showConfirmfsModal: false });

        })
      });
      //此处url更换一下
      const url = "/hzsx/ope/order/stageOrderBefore"
      /* request(url, reqParams, "post").then(res=>{
        console.log("")
      }) */
    };
    //复审添加选项是否通过与input备注,fsType传到后端
    return (
      <Modal
        title="复审"
        visible={this.state.showConfirmfsModal}
        onOk={btnConfirmHandler}
        onCancel={() => { this.setState({ showConfirmfsModal: false }); }}
        destroyOnClose>
        <Form>
          <Form.Item label="复审状态" {...formItemLayout}>
            {form.getFieldDecorator('fsType', {
              rules: [{ required: true, message: '请选择分类' }],
              initialValue: "1"
            })(
              <Select>
                <Option value="1">通过</Option>
                <Option value="2">拒绝</Option>
              </Select>
            )}
          </Form.Item>
          <Form.Item label="复审备注" {...formItemLayout}>
            {form.getFieldDecorator('remark', {})(
              <Input placeholder="请输入复审备注" />
            )}
          </Form.Item>

        </Form>
      </Modal>
    );
  };
  renderApplyCreditModalModal = () => {
    const { form } = this.props;

    const btnConfirmHandler = e => {
      e.preventDefault();
      yunxinCreditService
        .applyCredit({
          orderId: getParam('id')
        })
        .then(res => {
          message.success('操作成功')
          this.setState({ showConfirmApplyCreditModal: false });
        });
    };

    return (
      <Modal
        title="申请报送"
        visible={this.state.showConfirmApplyCreditModal}
        onOk={btnConfirmHandler}
        onCancel={this.cancelApplyCreditModalHandler}
        destroyOnClose>
        <p>请确保征信基础信息填写完整,否则会报送失败.确认进行 [ 申请报送 ] 吗?</p>
      </Modal>
    );
  };


  /**
   * 放款报送 modal
   */
  returnLoanCreditModalHandler = () => {
    this.setState({ showConfirmLoanCreditModal: true });
  };

  cancelLoanCreditModalHandler = () => {
    this.setState({ showConfirmLoanCreditModal: false });
  };

  renderLoanCreditModalModal = () => {
    const { form } = this.props;

    const btnConfirmHandler = e => {
      e.preventDefault();
      yunxinCreditService
        .loanCredit({
          orderId: getParam('id')
        })
        .then(res => {
          message.success('操作成功')
          this.setState({ showConfirmLoanCreditModal: false });
        });
    };

    return (
      <Modal
        title="放款报送"
        visible={this.state.showConfirmLoanCreditModal}
        onOk={btnConfirmHandler}
        onCancel={this.cancelLoanCreditModalHandler}
        destroyOnClose>
        <p>确认进行 [ 放款报送 ] 吗?</p>
      </Modal>
    );
  };

  /**
   * 取消放款 modal
   */
  returnCancelCreditModalHandler = () => {
    this.setState({ showConfirmCancelCreditModal: true });
  };

  cancelCancelCreditModalHandler = () => {
    this.setState({ showConfirmCancelCreditModal: false });
  };

  renderCancelCreditModalModal = () => {
    const { form } = this.props;

    const btnConfirmHandler = e => {
      e.preventDefault();
      yunxinCreditService
        .cancelLoanCredit({
          orderId: getParam('id')
        })
        .then(res => {
          message.success('操作成功')
          this.setState({ showConfirmCancelCreditModal: false });
        });
    };

    return (
      <Modal
        title="取消放款"
        visible={this.state.showConfirmCancelCreditModal}
        onOk={btnConfirmHandler}
        onCancel={this.cancelCancelCreditModalHandler}
        destroyOnClose>
        <p>确认 [ 取消放款 ] 吗?</p>
      </Modal>
    );
  };


  /**
   * 结清 modal
   */
  returnSettleCreditModalHandler = () => {
    this.setState({ showConfirmSettleCreditModal: true });
  };

  cancelSettleCreditModalHandler = () => {
    this.setState({ showConfirmSettleCreditModal: false });
  };

  renderSettleCreditModalModal = () => {
    const { form } = this.props;

    const btnConfirmHandler = e => {
      e.preventDefault();
      yunxinCreditService
        .settleCredit({
          orderId: getParam('id')
        })
        .then(res => {
          message.success('操作成功')
          this.setState({ showConfirmSettleCreditModal: false });
        });
    };

    return (
      <Modal
        title="结清"
        visible={this.state.showConfirmSettleCreditModal}
        onOk={btnConfirmHandler}
        onCancel={this.cancelSettleCreditModalHandler}
        destroyOnClose>
        <p>确认 [ 结清 ] 吗?</p>
      </Modal>
    );
  };

  daikou = () => {
    console.log("body:", body)
  }
  /**
   * 追偿报送 modal
   */
  returnDunRepayCreditModalHandler = () => {
    this.setState({ showConfirmDunRepayCreditModal: true });
  };

  cancelDunRepayCreditModalHandler = () => {
    this.setState({ showConfirmDunRepayCreditModal: false });
  };

  renderDunRepayCreditModalModal = () => {
    const { form } = this.props;

    const btnConfirmHandler = e => {
      e.preventDefault();
      message.success('追偿报送')

      this.props.form.validateFields(['amount', 'surplusAmount', 'fivecate'], (err, values) => {
        if (!err) {
          console.log('看下数据', values)
          let postData = Object.assign({}, values);
          postData.orderId = getParam('id');
          yunxinCreditService
            .dunRepayCredit(postData)
            .then(res => {
              message.success('操作成功')
              this.setState({ showConfirmDunRepayCreditModal: false });
            });
        } else {
          message.error('请补全所有必填信息');
        }
      });
    };

    return (
      <Modal
        title="追偿报送"
        visible={this.state.showConfirmDunRepayCreditModal}
        onOk={btnConfirmHandler}
        width={750}
        onCancel={this.cancelDunRepayCreditModalHandler}
        destroyOnClose>
        <Form>
          <Form.Item label="追偿本金额（不含利息）" {...formItemLayout}>
            {form.getFieldDecorator('amount', {
              rules: [{ required: true, message: '请输入追偿本金额' }],
            })(
              <InputNumber min={0} />
            )}
          </Form.Item>

          <Form.Item label="当前追偿后剩余金额" {...formItemLayout}>
            {form.getFieldDecorator('surplusAmount', {
              rules: [{ required: true, message: '请输入剩余金额' }],
            })(
              <InputNumber min={0} />
            )}
          </Form.Item>

          <Form.Item label="五级分类" {...formItemLayout}>
            {form.getFieldDecorator('fivecate', {
              rules: [{ required: true, message: '请选择分类' }],
            })(
              <Select>
                <Option value="1">正常 （0-30天）</Option>
                <Option value="2">关注 （31-60天）</Option>
                <Option value="3">次级 （61-120天）</Option>
                <Option value="4">可疑 （121天-180天）</Option>
                <Option value="5">损失 （180天以上）</Option>
              </Select>
            )}
          </Form.Item>
        </Form>
      </Modal>
    );
  };


  /**
   * 追偿结清 modal
   */
  returnDunRepaySettleCreditModalHandler = () => {
    this.setState({ showConfirmDunRepaySettleCreditModal: true });
  };

  cancelDunRepaySettleCreditModalHandler = () => {
    this.setState({ showConfirmDunRepaySettleCreditModal: false });
  };

  renderDunRepaySettleCreditModal = () => {
    const { form } = this.props;

    const btnConfirmHandler = e => {
      e.preventDefault();
      yunxinCreditService
        .dunRepaySettleCredit({
          orderId: getParam('id')
        })
        .then(res => {
          message.success('操作成功')
          this.setState({ showConfirmDunRepaySettleCreditModal: false });
        });
    };

    return (
      <Modal
        title="追偿结清"
        visible={this.state.showConfirmDunRepaySettleCreditModal}
        onOk={btnConfirmHandler}
        onCancel={this.cancelDunRepaySettleCreditModalHandler}
        destroyOnClose>
        <p>确认 [ 追偿结清 ] 吗?</p>
      </Modal>
    );
  };


  //退押金
  // confirm = ()=>{
  //   const orderId = getParam('id');
  //   orderService
  //     .forceDepositRefund({
  //       orderId,
  //     })
  //     .then(res => {
  //   console.log(res,'退押金')
  //     });
  // }
  renderBaseInfo() {
    const { userOrderInfoDto = {}, contractUrl, orderLocationAddress, signState,zxState, userCertification } = this.props;
    let cert = userCertification
    console.log(cert, "userOrderInfoDto1111")
    const orderId = getParam('id');
    const { auditLabel, status } = userOrderInfoDto;
    return (
      <Card bordered={false} style={{ marginTop: 20 }}>
        <Descriptions title={<CustomCard title="下单人信息" />}>
          <Descriptions.Item label="姓名">
            {cert && cert.isVip && cert.isVip == 1 ? (
              <span className="green-status">{userOrderInfoDto.userName}</span>
            ) : (
              <span >{userOrderInfoDto.userName}</span>
            )}
          </Descriptions.Item>
          <Descriptions.Item label="手机号">{userOrderInfoDto.telephone}</Descriptions.Item>
          <Descriptions.Item label="身份证号">{userOrderInfoDto.idCard}</Descriptions.Item>
          <Descriptions.Item label="年龄">{userOrderInfoDto.age}</Descriptions.Item>
          <Descriptions.Item label="性别">{userOrderInfoDto.gender}</Descriptions.Item>
          <Descriptions.Item label="下单时间">{userOrderInfoDto.createTime}</Descriptions.Item>
          <Descriptions.Item label="在途/完结订单数">
            {userOrderInfoDto.userPayCount > 1 ? (
              <span className="red-status">{userOrderInfoDto.userPayCount}</span>
            ) : (
              userOrderInfoDto.userPayCount
            )}/{userOrderInfoDto.userFinishCount}
          </Descriptions.Item>
          <Descriptions.Item label="全部订单数">
            {userOrderInfoDto.orderCount}
            <Button size="small" onClick={() => {
              this.setState({ showOrder: true })
            }}>查看</Button>
          </Descriptions.Item>
          {/* <Descriptions.Item label="完结订单数">
            {userOrderInfoDto.userFinishCount}
          </Descriptions.Item> */}
          <Descriptions.Item label="订单状态">
            {renderOrderStatus(userOrderInfoDto)}
          </Descriptions.Item>
          <Descriptions.Item label="人脸认证" span={1}>
            {userOrderInfoDto.userFaceCertStatus ? (
              <span className="green-status">已通过</span>
            ) : (
              <span className="red-status">未通过</span>
            )}
          </Descriptions.Item>
          {/* <Descriptions.Item label="渠道来源">{userOrderInfoDto.channelName}</Descriptions.Item> */}
          <Descriptions.Item label="所在位置" onClick={() => this.tzmap(orderLocationAddress)}>
            {

              orderLocationAddress && orderLocationAddress.province
                ? `${orderLocationAddress.province}${orderLocationAddress.city}${orderLocationAddress.district}${orderLocationAddress.streetNumber}`
                : '暂无定位'
            }
            <button onClick={() => this.tzmap(orderLocationAddress)}>地图</button>
          </Descriptions.Item>
          <Descriptions.Item label="签约状态" span={1}>
            {signState ? (
              <span className="green-status">已签约</span>
            ) : (
              <span className="red-status">未签约</span>
            )}
          </Descriptions.Item>

          <Descriptions.Item label="身份证照片" span={2}>
            {userOrderInfoDto.idCardBackUrl ? (
              <>
                <img
                  src={userOrderInfoDto.idCardBackUrl}
                  style={{ width: 146, height: 77, marginRight: 20 }}
                  onClick={() => this.onKaiImg(userOrderInfoDto.idCardBackUrl)}
                />
                <img
                  onClick={() => this.onKaiImg(userOrderInfoDto.idCardFrontUrl)}
                  src={userOrderInfoDto.idCardFrontUrl}
                  style={{ width: 146, height: 77, marginRight: 20 }}
                />
                {userOrderInfoDto.photo ? (<><img
                  src={userOrderInfoDto.photo}
                  style={{ width: 146, height: 77, marginRight: 20 }}
                  onClick={() => this.onKaiImg(userOrderInfoDto.photo)}
                /></>) : null}
              </>
            ) : (
              <span className="red-status">未上传</span>
            )}
          </Descriptions.Item>
          {/*<Descriptions.Item label="风险分">*/}
          {/*      <span className="primary-color">*/}
          {/*        {userOrderInfoDto.score}分*/}
          {/*       */}
          {/*      </span>*/}
          {/*</Descriptions.Item>*/}
        </Descriptions>
        {status === '04' && contractUrl ? null : (
          <Popconfirm
            title={<div>费用：{this.state.cost}元</div>}
            style={{
              textAlign: 'center',
            }}
            icon={null}
            okText="确定"
            cancelText="取消"
            disabled={this.state.sign}
            onConfirm={this.onConfirm}
          >
            <Button type="primary">签约存证</Button>
          </Popconfirm>
        )}

        {zxState ? null : (
           <Button type="primary" onClick={()=>{this.seezx()}}>查看征信</Button>
        )}

        {status === '11' ? (
          <Fragment>
            {auditLabel !== '00' ? (
              <Fragment>
                <Button type="primary" onClick={() => this.showOrderModal('audit', true)}>
                  审批通过
                </Button>
                <Button type="primary" onClick={() => this.showOrderModal('audit', false)}>
                  审批拒绝
                </Button>
              </Fragment>
            ) : null}
            <Button type="primary" onClick={() => this.showOrderModal('address')}>
              修改收货信息
            </Button>
          </Fragment>
        ) : null}

        {['01', '02'].includes(status) ? (
          <Button type="primary" onClick={() => this.showOrderModal('address')}>
            修改收货信息
          </Button>
        ) : null}
        {['05', '11', "01", "04"].includes(status) ? (
          <Button type="primary" onClick={() => this.showOrderModal('close')}>
            关闭订单
          </Button>
        ) : null}
        {status === '04' ? (
          <Fragment>
            {/* {
  contractUrl?<Button type="primary" onClick={() => this.showOrderModal('deliver')}>
  发货
</Button>:<Button type="primary" onClick={() => this.showOrderModal('deliver')}>
  发货
</Button>
} */}


            <Button type="primary" onClick={() => this.showOrderModal('deliver')}>
              发货
            </Button>
            {/*  <Button type="primary" onClick={() => this.showOrderModal('close')}>
              关闭订单
            </Button> */}
            <Button type="primary" onClick={() => this.showOrderModal('address')}>
              修改收货信息
            </Button>
          </Fragment>
        ) : null}
        {status === '05' ? (
          <Fragment>
            <Button type="primary" onClick={() => this.showOrderModal('express')}>
              修改物流信息
            </Button>
          </Fragment>
        ) : null}
        {status === '07' ? (
          <Button
            type="primary"
            onClick={() =>
              this.settleDia('settle', orderId, userOrderInfoDto.orderCashId, userOrderInfoDto)
            }
          >
            结算
          </Button>
        ) : null}
        {getParam('settlement') ? (
          <Button type="primary" onClick={() => this.showOrderModal('remarks')}>
            记录催收
          </Button>
        ) : null}
        <Button type="primary" onClick={() => this.setState({ depositModal: true })}>
          修改押金
        </Button>
        <Button type="primary" onClick={() => this.showOrderModal('remark')}>
          备注
        </Button>
        <Button type="primary" onClick={() => {
          console.log("取消领取")
          request(`/hzsx/buckstageOrderClaim/closeAllocationClaim?orderId=` + getParam('id'), {}, "get").then(res => {
            console.log("取消领取", res)
            message.success("取消成功")
          })
        }}>
          取消领取
        </Button>


        {/* <Popconfirm title="确认退款？" onConfirm={this.confirm} okText="确认" cancelText="取消">
          <Button type="primary">退押金</Button>
        </Popconfirm> */}
        {this.props.userOrderInfoDto.status !== '06' ? null : (
          <Button type="primary" onClick={this.returnBackHandler}>
            确认归还
          </Button>
        )}
        {/* <Button type="primary" onClick={this.returnCreditDrawerHandler}>
          查看征信报告
        </Button>
        <Button type="primary" onClick={this.returnCreditBaseInfoDrawerHandler}>
          编辑征信信息
        </Button> */}
        <Button type="primary" onClick={this.queren}>
          手动确认收货
        </Button>
        {cert && cert.isVip != 1 ? (<Button type="primary" onClick={() => this.setVip(userOrderInfoDto.uid)}>
          VIP
        </Button>) : null}

        <Button type="primary" onClick={this.fsModalHandler}>
          复审
        </Button>

        {/*     <Button type="primary" onClick={this.returnApplyCreditModalHandler}>
          申请报送
        </Button>
        <Button type="primary" onClick={this.returnLoanCreditModalHandler}>
          放款报送
        </Button>
        <Button type="primary" onClick={this.returnCancelCreditModalHandler}>
          取消放款
        </Button>
        <Button type="primary" onClick={this.returnSettleCreditModalHandler}>
          结清
        </Button>
        <Button type="primary" onClick={this.returnDunRepayCreditModalHandler}>
          追偿报送
        </Button>
        <Button type="primary" onClick={this.returnDunRepaySettleCreditModalHandler}>
          追偿结清
        </Button> */}

        <Divider />

        {/*<Descriptions title={<CustomCard title="商家信息" />}>*/}
        {/*  <Descriptions.Item label="商家名称">*/}
        {/*    {shopInfoDto && shopInfoDto.shopName}*/}
        {/*  </Descriptions.Item>*/}
        {/*  <Descriptions.Item label="商家电话" span={2}>*/}
        {/*    {shopInfoDto && shopInfoDto.telephone}*/}
        {/*  </Descriptions.Item>*/}
        {/*</Descriptions>*/}
        {/*<Divider />*/}
      </Card>
    );
  }
  depositOverSure = e => {
    const orderId = getParam('id');
    e.preventDefault();
    this.props.form.validateFields(['afterAmount'], (err, values) => {
      if (!err) {
        OrderService.updatePayDepositAmount({ orderId, afterAmount: values.afterAmount }).then(
          res => {
            if (res) {
              message.success('修改成功');
              //押金信息
              OrderService.queryPayDepositLog({ orderId }).then(res => {
                if (res) {
                  this.setState({
                    depositData: res,
                    depositModal: false,
                  });
                }
              });
            }
          },
        );
      }
    });
  };
  render() {
    const { orderBuyOutDto, wlList } = this.props;

    const { drawerVisible, wdata } = this.state;
    console.log(wdata, "wdata")
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    return (
      <PageHeaderWrapper title={false}>
        <Spin
          spinning={getParam('RentRenewal') ? this.props.RentRenewalLoading : this.props.loading}
        >
          <Spin spinning={this.state.isloading}>

            {this.renderBaseInfo()}
            {this.renderContentTabCard()}
          </Spin>
        </Spin>
        <BackTop />
        {this.renderRemarkModal()}
        {this.renderAddCostPriceModal()}
        {this.renderuptimeModal()}
        {this.renderuppriceModal()}
        {this.renderbeforeModal()}
        {this.renderDeliverModal()}
        {this.renderCloseModal()}
        {this.renderSettleModal()}
        {this.renderAddressModal()}
        {this.renderAuditModal()}
        {this.renderCollectionRecordModal()}
        {this.renderConfirmOrderReturnModal()}
        {this.renderCreditDrawer()}
        {this.renderOrderListDrawer()}
        {this.renderCreditBaseInfoDrawer()}

        {this.renderApplyCreditModalModal()}
        {this.renderFsModal()}
        {this.renderLoanCreditModalModal()}
        {this.renderCancelCreditModalModal()}
        {this.renderSettleCreditModalModal()}
        {this.renderDunRepayCreditModalModal()}
        {this.renderDunRepaySettleCreditModal()}
        <Drawer
          width={420}
          title={wlList ? '发货物流信息' : "无信息"}
          placement="right"
          onClose={this.onClose}
          visible={drawerVisible}>
          {wlList ? <>
            <Timeline>
              {wlList.map((item, idx) => {
                let color = 'blue';
                if (idx === 0) {
                  color = 'green';
                }
                return (
                  <Timeline.Item
                    style={{ color: idx !== 0 ? '#aaa' : '#333' }}
                    key={idx}
                    color={color}
                  >
                    <p>{item.context}</p>
                    <p>{item.ftime}</p>
                  </Timeline.Item>
                );
              })}
            </Timeline>

          </> : null}
        </Drawer>
        <Modal
          title="修改押金"
          visible={this.state.depositModal}
          onOk={this.depositOverSure}
          onCancel={() => this.setState({ depositModal: false })}
          destroyOnClose
        >
          <Form>
            <Form.Item label="当前押金总额" {...formItemLayout}>
              {this.state.depositData && this.state.depositData.amount}
            </Form.Item>
            <Form.Item label="修改金额(元)" {...formItemLayout}>
              {getFieldDecorator('afterAmount', {
                rules: [
                  { required: true, message: '请输入押金金额' },
                  {
                    pattern: /^\d+$|^\d+[.]?\d+$/,
                    message: '请正确输入金额',
                  },
                ],
              })(<Input placeholder="请输入" />)}
            </Form.Item>
          </Form>
        </Modal>
        <Modal
          title="身份证图片"
          visible={this.state.visiblesimg}
          onCancel={this.handleCancels}
          destroyOnClose
          footer={null}
        >
          <img
            src={this.state.img}
            alt="alt"
            style={{ width: '100%', transform: `rotate(${this.state.rotate}deg)` }}
          />
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
              boxSizing: 'border-box',
              padding: '50px 150px 0 150px',
            }}
          >
            <img
              src={require('../../../../public/rotate-icon.png')}
              alt="alt"
              onClick={() => this.setState({ rotate: (this.state.rotate || 0) - 90 })}
              style={{ transform: 'rotateY(180deg)', cursor: 'pointer' }}
            />
            <img
              src={require('../../../../public/rotate-icon.png')}
              alt="alt"
              onClick={() => this.setState({ rotate: (this.state.rotate || 0) + 90 })}
              style={{ cursor: 'pointer' }}
            />
          </div>
        </Modal>
      </PageHeaderWrapper>
    );
  }
}
