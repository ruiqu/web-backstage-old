import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import {
  Card, Tooltip, Button, Form, Input, Select, DatePicker, Spin, Row,
  Col, Divider, Modal,
  InputNumber, Descriptions,
  message, Drawer,
} from 'antd';
import MyStage from '@/components/MyStage';//分期组件
import Tbxj from '@/components/Tbxj';//头部的统计
import AntTable from '@/components/AntTable';
import MyPageTable from '@/components/MyPageTable';
import { onTableData } from '@/utils/utils.js';
import CustomCard from "@/components/CustomCard"
import CopyToClipboard from '@/components/CopyToClipboard';
import { getTimeDistance, renderOrderStatus ,ddzq} from '@/utils/utils';
import moment from 'moment';
const { Option } = Select;

const { RangePicker } = DatePicker;
import { routerRedux } from 'dva/router';
import { getCurrentUser } from '@/utils/localStorage';
import request from '../../../services/baseService';
import { orderCloseStatusMap, orderStatusMap } from '@/utils/enum';
import { exportCenterHandler } from '../util';
import { UserRating } from 'zwzshared';
@connect(({ order, loading }) => ({
  ...order,
  loading: loading.effects['order/queryOpeOrderByConditionList'],
}))
@Form.create()
export default class HomePage extends Component {
  state = {
    current: 1,
    unit:0,
    visible: '',
    orderVisible: '',
    load: false,
    stagesShow: false,
    orderId: null,
    channelList: [],
    stages: [],
    backuser:[],
    mydata: {},
    datas: {},
    record: {},
    mydb: [],
    expressList: [],
    yunTime: getTimeDistance('month'),
    gBtusua: '04', // 默认状态: 待发货
  };

  renderAddCostPriceModal() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const { orderVisible, orderId } = this.state;
    const { getFieldDecorator } = this.props.form;
    const handleOk = e => {
      e.preventDefault();
      this.props.form.validateFields(['cost_price'], (err, values) => {
        if (!err) {
          console.log("orderid:", orderId, values)
          values.orderId = orderId
          values.costPrice = values.cost_price
          request(`/hzsx/business/order/addcostPrice`, values).then(res => {
            console.log("rcs", res)
            this.setState({
              visible: false,
              orderVisible: false,
            });
            this.props.form.resetFields();
            //this.handleReset();
            message.success("添加完成", 5)
          })
        }
      });
    };

    return (
      <div>
        <Modal
          title="添加成本"
          visible={orderVisible === 'addcost'}
          onOk={handleOk}
          onCancel={this.handleCancel}
        >
          <Form>
            <Form.Item label="成本金额" {...formItemLayout}>
              {getFieldDecorator('cost_price', {
                rules: [{ required: true, message: '请输入成本金额' }],
              })(<InputNumber placeholder="请输入" />)}
            </Form.Item>
          </Form>
        </Modal>
      </div>
    );
  }
  showOrderModal = (type, orderId) => {
    console.log("按钮点击", type, orderId)
    this.setState({
      orderVisible: type,
      orderId
    });
  }


  handleCancel = e => {
    this.setState({
      visible: false,
      orderVisible: false,
    });
  };

  renderAddYjPriceModal() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const { orderVisible, orderId } = this.state;
    const { getFieldDecorator } = this.props.form;
    const handleOk = e => {
      e.preventDefault();
      this.props.form.validateFields(['yj'], (err, values) => {
        if (!err) {
          console.log("orderid:", orderId, values)
          values.orderId = orderId

          request(`/hzsx/business/order/addyj`, values).then(res => {
            console.log("rcs", res)
            this.setState({
              visible: false,
              orderVisible: false,
            });
            this.props.form.resetFields();
            //this.handleReset()
            message.success("添加完成", 5)
          })
        }
      });
    };

    return (
      <div>
        <Modal
          title="添加押金"
          visible={orderVisible === 'addyj'}
          onOk={handleOk}
          onCancel={this.handleCancel}
        >
          <Form>
            <Form.Item label="押金金额" {...formItemLayout}>
              {getFieldDecorator('yj', {
                rules: [{ required: true, message: '请输入成本金额' }],
              })(<InputNumber placeholder="请输入" />)}
            </Form.Item>
          </Form>
        </Modal>
      </div>
    );
  }



  componentDidMount() {
    request("/hzsx/user/listBackstageUser", {}, "get").then(res => {
      console.log("用户信息：", res)
      this.setState(
        {
          backuser: res,
        },
      );
    })
    const { status } = this.props.location.query;
    if (status) {
      this.setState(
        {
          gBtusua: status,
          load: true,
        },
        () => {
          this.props.form.setFieldsValue({
            status,
          });
          this.onList(1, 30, {
            status: this.state.gBtusua,
          });
          // this.getExpressList();
        },
      );
    } else {
      this.onList(1, 30);
      // this.getExpressList();
    }

    this.props.dispatch({
      type: 'order/PlateformChannel',
    });
    request("/zyj-backstage-web/hzsx/business/channel/getChannelList", {
      pageNumber: 1,islist:1,
      pageSize: 3000,
      unit: 0,
    }).then(res => {
      console.log("返回的信息是", res)
      res.records.unshift({id:"000",name:"默认"})
      this.setState(
        {
          channelList: res.records,
        },
      );
    })
  }
  getStageYj=(sta,color=1)=>{
    if(!sta)return
    console.log(sta)
    
    if(['5', '1',"7","4"].includes(sta.status)){
      if(color==1)      return 0+"/"+sta.currentPeriodsRent
      else return 'red'
    }
    if(['2', '3',"6"].includes(sta.status))return sta.currentPeriodsRent+"/"+sta.currentPeriodsRent
    if(sta.status=="8"){
      if(color==1)  return sta.beforePrice+"/"+sta.currentPeriodsRent
      else return 'red'
    }
    //如果状态未支付，那么
  }
/**
 *       '1': '待支付',
      '2': '已支付',
      '3': '逾期已支付',
      '4': '逾期待支付',
      '5': '已取消',
      '6': '已结算',
      '7': '已退款,可用',
      '8': '部分还款',
 */
  getStageInfo=(sta)=>{
    if(!sta)return
    let style={display: 'flex','align-items': 'center',  'justify-content': 'center'}
    let str=sta.currentPeriodsRent+"/"+sta.currentPeriodsRent
    if(['5', '1',"7","4"].includes(sta.status)){
      str= 0+"/"+sta.currentPeriodsRent
    }
    if(sta.status=="8"){
      str =sta.beforePrice+"/"+sta.currentPeriodsRent
    }
    if(["8","4"].includes(sta.status)){
      style.color='red'
    }
    const columnsByStagesStute = {
      '1': '待支付',
      '2': '已支付',
      '3': '逾期已支付',
      '4': '逾期待支付',
      '5': '已取消',
      '6': '已结算',
      '7': '已退款,可用',
      '8': '部分还款',
    };
    return <>
            <div >
              <p style={style}>{str}</p>
              <p style={style}>{columnsByStagesStute[sta.status]}</p>
              <p style={style}> {sta.statementDate.slice(0,10)}</p>
            </div>
          </>;
  }
  //导出租用中的订单
  exporder=e=>{
    e && e.preventDefault();
    this.props.form.validateFields((err, values) => {
      console.log(values,"values")
      if (values.createDate && values.createDate.length < 1) {

        values.createTimeStart = "";
        values.createTimeEnd = "";
      } else if (values.createDate) {
        values.createTimeStart = moment(values.createDate[0]).format('YYYY-MM-DD HH:mm:ss');
        values.createTimeEnd = moment(values.createDate[1]).format('YYYY-MM-DD HH:mm:ss');
      } 
      if(values.status)values.status=[values.status]
      else values.status=["04","05","06","07","08","09"]
      values.unit=this.state.unit
      values.isday="1"
      values.tp="isday"
      console.log("db:",values)
      request("/hzsx/export/rentOrder", values).then(res => {
        console.log("返回的信息是", res)
        message.success('导出成功');
      })
    });
  }
  onList = (pageNumber, pageSize, data = {}) => {
    //判断有没有时间
    console.log("data1111:", data)
    this.setState({load: true,});
    request("/hzsx/business/order/queryOrderByRent", {
      pageSize,
      pageNumber,
      unit: this.state.unit,
      iszy: 1,
      ...data,
    }).then(res => {
      console.log("aaa",res)
      let page=res.page
      let records=page.records
      for (let i = 0; i < records.length; i++) {
        const ele = records[i];
        for (let o = 0; o < ele.stageList.length; o++) {
          const el1 = ele.stageList[o];
          let k="sta"+el1.currentPeriods
          records[i][k]=el1
        }
        
      }
      page.records=records
      let obj = {...res, ...page};
      console.log("aaaa:", res)
      this.setState(
        {
          mydata: obj,
          tjxx:res,
          load: false,
        },
      );
    })
    const { dispatch } = this.props;
    /* dispatch({
      type: 'order/queryOpeOrderByConditionList',
      payload: {
        pageSize,
        pageNumber,
        iszy: 1,
        ...data,
      },
    }); */
  };
    //查看账单
    seeStage = (orderId) => {
      request(`/hzsx/business/order/queryOrderStagesDetail`, { orderId }).then(res => {
        console.log("sjrh：", res.orderByStagesDtoList)
        this.setState(
          {
            stages: res.orderByStagesDtoList,
            stagesShow: true,
          },
        );
      })
    }

  // 重置
  handleReset = e => {
    this.setState(
      {
        gBtusua: '',
      },
      () => {
        this.props.form.resetFields();
        this.props.form.setFieldsValue({
          status: undefined,
        });
        this.handleSubmit(e);
      },
    );
  };
  handleSubmit = e => {
    e && e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (values.createDate && values.createDate.length < 1) {
        values.createTimeStart = '';
        values.createTimeEnd = '';
        this.setState(
          {
            datas: { ...values },
            current: 1,
          },
          () => {
            this.onList(1, 30, { ...values });
          },
        );
      } else if (values.createDate) {
        values.createTimeStart = moment(values.createDate[0]).format('YYYY-MM-DD HH:mm:ss');
        values.createTimeEnd = moment(values.createDate[1]).format('YYYY-MM-DD HH:mm:ss');
        this.setState(
          {
            datas: { ...values },
            current: 1,
          },
          () => {
            this.onList(1, 30, { ...values });
          },
        );
      } else {
        this.setState(
          {
            datas: { ...values },
            current: 1,
          },
          () => {
            this.onList(1, 30, { ...values });
          },
        );
      }
    });
  };

  onexport = () => {
    exportCenterHandler(this, 'exportRentOrder', true, false);
  };

  handleCancel = e => {
    this.setState({
      visible: false,
      orderVisible: false,
    });
  };
  //table 页数
  onPage = e => {
    this.setState(
      {
        current: e.current,
      },
      () => {
        const params = {
          ...this.state.datas,
        }
        this.onList(e.current, 30, params);
      },
    );
  };
  onChanges = e => {
    this.setState({
      gBtusua: e,
    });
  };

  // 认领订单
  lingqu = () => {
    // console.log("user",getCurrentUser());
    this.props.dispatch(
      routerRedux.push({
        pathname: `/Order/HomePage/ReceiveOrder`,
      })
    );
  }
  onClose = () => {
    console.log("取消：")
    this.state.stagesShow = false
    this.setState({stagesShow:false})
  }
  render() {
    const { load, mydata } = this.state;
    let orderByStagesDtoList = this.state.stages
    const { loading, form, mydb } = this.props;
    console.log("page", mydata)
    let allTotal = mydata?.total
    let allList = mydata?.records

    console.log("mydata", mydata)
    const paginationProps = {
      current: this.state.current,
      pageSize: 30,
      total: allTotal,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / 30)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };
    let db=[]
    let i=1
    while(i<=12){
      let key="sta"+i
      db.push({
        title: '第'+i+'期',
        width: 120,
        align: 'center',
        render: e => {
          return this.getStageInfo(e[key]);
        },
      },)
      i++;
    }
    const user = getCurrentUser();
    const columns = [
      {
        title: '订单编号',
        dataIndex: 'orderId',
        render: orderId => {
          return <>
            <CopyToClipboard text={orderId} >

            </CopyToClipboard>
          </>
        },
        width: '120px',
      },
      {
        title: '渠道来源',
        dataIndex: 'channelName',
        width: 120,
        render: text => text === "000" ? "默认" : text,
      },
      {
        title: '姓名/手机号',
        width: 140,
        render: e => {
          let style={}
          if(e.isRed==1){
            console.log("style",style,e)
            style.color='red'
          }
          return <>
            <div style={style}>
            {e&&e.isVip&&e.isVip==1 ? (
              <p className="green-status">{e.realName}</p>
              ) : (
              <p >{e.realName}</p>
            )}
              <p>{e.telephone}</p>
            </div>
          </>;
        },
      },
      {
        title: '商品名称',
        dataIndex: 'productName',
        width: 160,
        render: (text, record, index) => {
          return (
            <>
              {text}+{record.skuTitle}
            </>
          )
        }
      },

      {
        title: '已付期数',
        width: 110,
        render: e => {
          return (
            <>
              <Tooltip placement="top" title={e.currentPeriods}>
                <div
                  style={{
                    width: 60,
                  }}
                >
                  <p>
                  {e.payedPeriods}/{e.totalPeriods + 1}
                  </p>
                  <a onClick={() => {
                this.seeStage(e.orderId)
              }}>查看</a>
                </div>
              </Tooltip>
            </>
          );
        },
      },
      {
        title: '已付租金',
        width: 90,
        render: e => {
          return <>
            <div >
              <p style={{display: 'flex','align-items': 'center',  'justify-content': 'center'}}>{e.payedRentAmount}</p>
              <p style={{display: 'flex','align-items': 'center',  'justify-content': 'center'}}>{e.totalRentAmount}</p>
            </div>
          </>;
        },
      },
      ...db,

      {
        title: '已付押金',
        dataIndex: 'yj',
        width: 120,
      },
      {
        title: '差值',
        dataIndex: 'cz',
        width: 120,
        render: (_, i) => {
          let cb = i.costPrice || 0  //成本
          let yj = i.yj || 0  //已付押金
          let zj = i.payedRentAmount || 0  //已付押金
          //差值cz(成本-已付押金-已付租金)
          let yfzj = i.payedRentAmount  //已付租金
          let cz = zj + yj - cb
          //console.log("差值",i)
          return cz.toFixed(2)
        },
      },
     /*  {
        title: '成本',
        dataIndex: 'costPrice',
        width: 120,
      },
      {
        title: '应收',
        dataIndex: 'lr',
        width: 120,
        render: (_, i) => {
          let cb = i.costPrice || 0  //成本
          let zzj = i.totalRentAmount  //总金额
          let yfzj = i.payedRentAmount  //已付租金
          //利润（总租金-成本）
          //console.log("利润（总租金-成本）", i)
          return (zzj - yfzj).toFixed(2)
        },
      }, */
      {
        title: '审核',
        dataIndex: 'rname',
        width: 100,
      },

      {
        title: '下单时间',
        dataIndex: 'placeOrderTime',
        width: 120,
      },
      {
        title: '订单状态',
        dataIndex: 'status',
        width: 120,
        render: (_, record) => renderOrderStatus(record),
      },
    
      {
        title: '操作',
        width: 120,
        fixed: 'right',
        align: 'center',
        render: (e, record) => {
          return (
            <div>
              <a
                className="primary-color"
                // onClick={() => router.push(`/Order/Details?id=${e.orderId}`)}
                href={`#/Order/HomePage/Details?id=${e.orderId}`}
                target="_blank"
              >
                处理
              </a>
              
            </div>
          );
        },
      },
    ];
    const columnsByStagesStute = {
      '1': '待支付',
      '2': '已支付',
      '3': '逾期已支付',
      '4': '逾期待支付',
      '5': '已取消',
      '6': '已结算',
      '7': '已退款,可用',
      '8': '部分还款',
    };
    
    const columnsByStages = [
      {
        title: '总期数',
        dataIndex: 'totalPeriods',
      },
      {
        title: '当前期数',
        dataIndex: 'currentPeriods',
      },
      {
        title: '租金',
        dataIndex: 'currentPeriodsRent',
      },
      {
        title: '状态',
        render: e => <>
          <span>
            {columnsByStagesStute[e.status]}
          </span>
        </>
        ,
      },
      {
        title: '支付时间',
        dataIndex: 'repaymentDate',
      },
      {
        title: '账单到期时间',
        dataIndex: 'statementDate',
      },
    ];
    const { getFieldDecorator } = form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    return (
      <>
        <PageHeaderWrapper title={false}>
        <Drawer
          width={620}
          title='账单信息'
          placement="right"
          onClose={this.onClose}
          visible={this.state.stagesShow}>
          <MyStage
            orderByStagesDtoList={orderByStagesDtoList}></MyStage>
        </Drawer>
          {this.renderAddCostPriceModal()}
          {this.renderAddYjPriceModal()}
          <Tbxj mydb={mydata}></Tbxj>
          <Card bordered={false}>
            <Form layout="inline" onSubmit={this.handleSubmit}>
              <Row>
                <Col span={8}>
                  <Form.Item label="商品名称">
                    {getFieldDecorator(
                      'productName',
                      {},
                    )(<Input allowClear placeholder="请输入商品名称" />)}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item label="下单人姓名">
                    {getFieldDecorator(
                      'userName',
                      {},
                    )(<Input allowClear placeholder="请输入下单人姓名" />)}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item label="下单人手机号">
                    {getFieldDecorator(
                      'telephone',
                      {},
                    )(<Input allowClear placeholder="请输入下单人手机号" />)}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  {' '}
                  <Form.Item label="订单编号">
                    {getFieldDecorator(
                      'orderId',
                      {},
                    )(<Input allowClear placeholder="请输入订单编号" />)}
                  </Form.Item>
                </Col>

                <Col span={8}>
                  <Form.Item label="用户身份证">
                    {getFieldDecorator(
                      'idCard',
                      {},
                    )(<Input allowClear placeholder="请输入下单人身份证号" />)}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item label="收货人手机号">
                    {getFieldDecorator(
                      'addressUserPhone',
                      {},
                    )(<Input allowClear placeholder="请输入收货人手机号" />)}
                  </Form.Item>
                </Col>


                <Col span={8}>
                  <Form.Item label="订单状态">
                    {getFieldDecorator('status', {
                      // initialValue: '',
                    })(
                      <Select
                        placeholder="订单状态"
                        allowClear
                        style={{ width: 180 }}
                        onChange={this.onChanges}
                      >
                        {['01', '11', '04', '05', '06', '07', '08', '09', '10'].map(value => {
                          return (
                            <Option value={value.toString()} key={value.toString()}>
                              {orderStatusMap[value.toString()]}
                            </Option>
                          );
                        })}
                      </Select>,
                    )}
                  </Form.Item>
                </Col>
                {/* <Col span={8}>
                <Form.Item label="订单周期">
                  {getFieldDecorator(
                    'unit',
                    {},
                  )(
                     <Select style={{ width: 180 }} allowClear placeholder="请选择">
                  {ddzq.map((value, i) => {
                        return (
                          <Option value={i} key={i}>
                            {value}
                          </Option>
                        );
                      })}
                </Select>,
                  )}
                </Form.Item>
                </Col> */}
                <Col span={8}>
                  <Form.Item label="渠道来源">
                    {getFieldDecorator('channelId', {
                      // initialValue: '',
                    })(
                      <Select
                        placeholder="渠道来源"
                        allowClear
                        style={{ width: 180 }}
                        onChange={this.onChangesChannel}
                      >
                        {this.state.channelList.map(value => {
                          return (
                            <Option value={value.id} key={value.id}>
                              {value.name}
                            </Option>
                          );
                        })}
                      </Select>,
                    )}
                  </Form.Item>
                </Col>
                <Col span={8}>
                <Form.Item label="排序">
                  {getFieldDecorator('sort', {
                    // initialValue: '',
                  })(
                    <Select
                      placeholder="排序"
                      allowClear
                      style={{ width: 180 }}
                    >
                      <Option value='id_desc'>时间倒序</Option>
                    </Select>,
                  )}
                </Form.Item>
              </Col>

              <Col span={8}>
                <Form.Item label="审核名字">
                  {getFieldDecorator('rid', {
                  })(
                    <Select
                      placeholder="审核员"
                      allowClear
                      style={{ width: 180 }}
                      onChange={this.onChangesChannel}
                    >
                      {this.state.backuser.map(value => {
                        return (
                          <Option value={value.id} key={value.id}>
                            {value.name}
                          </Option>
                        );
                      })}
                    </Select>,
                  )}
                </Form.Item>
              </Col>
                <Col span={16}>
                  <Form.Item label="创建时间">
                    {getFieldDecorator('createDate', {})(<RangePicker />)}
                  </Form.Item>
                </Col>
              </Row>





              {/*  <UserRating form={this.props.form} /> */}
              <div>
                <Form.Item>
                  <Button type="primary" htmlType="submit">
                    查询
                  </Button>
                </Form.Item>
                <Form.Item>
                  <Button htmlType="button" onClick={this.handleReset}>
                    重置
                  </Button>
                </Form.Item>
                <Form.Item>
                  <Button  htmlType="button" onClick={this.exporder}>
                    导出
                  </Button>
                </Form.Item>

              </div>
            </Form>
            <Spin spinning={load}>
              <MyPageTable
                scroll={true}
                onPage={this.onPage}
                paginationProps={paginationProps}
                dataSource={onTableData(allList)}
                columns={columns}
              />
            </Spin>
          </Card>
        </PageHeaderWrapper>
      </>
    );
  }
}
