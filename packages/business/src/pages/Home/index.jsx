import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Card, Col, Row, Descriptions, DatePicker, Radio, Divider, Icon } from 'antd';
import styles from './index.less';
import CustomCard from '@/components/CustomCard';
// import { Line } from '@antv/g2plot';
import { Line, Area } from '@ant-design/charts';
import { getTimeDistance } from '@/utils/utils';
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const dateFormat = 'YYYY-MM-DD';
export default class Home extends Component {
  state = {
    rangeTimeData: getTimeDistance('week'),
    StatisticsData: getTimeDistance('week'),
  };
  componentDidMount() {}
  onTime = e => {
    this.setState({
      rangeTimeData: getTimeDistance(e.target.value),
    });
  };
  onTimes = e => {
    this.setState({
      StatisticsData: getTimeDistance(e.target.value),
    });
  };
  render() {
    const data = [
      {
        year: '1991',
        销售额: 3,
      },
      {
        year: '1992',
        销售额: 4,
      },
      {
        year: '1993',
        销售额: 3.5,
      },
      {
        year: '1994',
        销售额: 5,
      },
      {
        year: '1995',
        销售额: 4.9,
      },
      {
        year: '1996',
        销售额: 6,
      },
      {
        year: '1997',
        销售额: 7,
      },
      {
        year: '1998',
        销售额: 9,
      },
      {
        year: '1999',
        销售额: 13,
      },
    ];
    const config = {
      width: 200,

      title: {
        visible: true,
        text: '近一周订单统计',
      },

      padding: 'auto',
      forceFit: true,
      data,
      xField: 'year',
      yField: '销售额',
      yAxis: {
        label: {
          formatter: v => `${v}`.replace(/\d{1,3}(?=(\d{3})+$)/g, s => `${s},`),
        },
      },
      label: {
        visible: true,
        type: 'point',
      },
      point: {
        visible: true,
        size: 5,
        shape: 'diamond',
        style: {
          fill: 'white',
          stroke: '#2593fc',
          lineWidth: 2,
        },
      },
    };
    const configs = {
      width: 200,

      title: {
        visible: true,
        text: '近一周租金总额（¥）',
      },

      padding: 'auto',
      forceFit: true,
      data,
      xField: 'year',
      yField: '销售额',
      yAxis: {
        label: {
          formatter: v => `${v}`.replace(/\d{1,3}(?=(\d{3})+$)/g, s => `${s},`),
        },
      },
      label: {
        visible: true,
        type: 'point',
      },
      point: {
        visible: true,
        size: 5,
        shape: 'diamond',
        style: {
          fill: 'white',
          stroke: '#2593fc',
          lineWidth: 2,
        },
      },
    };
    const { rangeTimeData, StatisticsData } = this.state;
    return (
      <PageHeaderWrapper  title={false}>
        <Row gutter={16}>
          <Col span={6}>
            <Card
              title={
                <div className={styles.title}>
                  <img
                    className={styles.imgIk}
                    src="https://booleandata-crmmanagement-front.oss-cn-beijing.aliyuncs.com/operate/Group%2029%402x.png"
                  />
                  今日订单总数
                </div>
              }
              bordered={false}
            >
              ¥2000
            </Card>
          </Col>
          <Col span={6}>
            <Card
              title={
                <div className={styles.title}>
                  <img
                    className={styles.imgIk}
                    src="https://booleandata-crmmanagement-front.oss-cn-beijing.aliyuncs.com/operate/pay-circle%402x.png"
                  />
                  今日下单总租金
                </div>
              }
              bordered={false}
            >
              Card content
            </Card>
          </Col>
          <Col span={6}>
            <Card
              title={
                <div className={styles.title}>
                  <img
                    className={styles.imgIk}
                    src="https://booleandata-crmmanagement-front.oss-cn-beijing.aliyuncs.com/operate/pay-circle%402x%20%281%29.png"
                  />
                  昨日下单总租金
                </div>
              }
              bordered={false}
            >
              Card content
            </Card>
          </Col>
          <Col span={6}>
            <Card
              title={
                <div className={styles.title}>
                  <img
                    className={styles.imgIk}
                    src="https://booleandata-crmmanagement-front.oss-cn-beijing.aliyuncs.com/operate/pay-circle%402x%20%282%29.png"
                  />
                  近7日下单总租金
                </div>
              }
              bordered={false}
            >
              Card content
            </Card>
          </Col>
        </Row>
        <div
          className={styles.content}
          style={{
            padding: 0,
          }}
        >
          <Card title={<CustomCard title="待处理事物" />} bordered={false}>
            <Descriptions>
              <Descriptions.Item label="待付款订单">Zhou Maomao</Descriptions.Item>
              <Descriptions.Item label="租用中订单">Zhou Maomao</Descriptions.Item>
              <Descriptions.Item label="已取消订单">Zhou Maomao</Descriptions.Item>
              <Descriptions.Item label="待发货订单">Zhou Maomao</Descriptions.Item>
              <Descriptions.Item label="待结算订单">Zhou Maomao</Descriptions.Item>
              <Descriptions.Item label="已完成订单">Zhou Maomao</Descriptions.Item>
              <Descriptions.Item label="待收货订单">Zhou Maomao</Descriptions.Item>
              <Descriptions.Item label="已逾期订单">Zhou Maomao</Descriptions.Item>
            </Descriptions>
          </Card>
        </div>
        <Row
          gutter={16}
          style={{
            marginTop: 30,
          }}
        >
          <Col span={12}>
            <Card title={<CustomCard title="商品总览" />} bordered={false}>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                }}
              >
                <div
                  style={{
                    textAlign: 'center',
                  }}
                >
                  <div
                    style={{
                      fontSize: 12,
                      color: 'rgba(0,0,0,0.45)',
                    }}
                  >
                    已下架
                  </div>
                  <div
                    style={{
                      fontSize: 24,
                      fontWeight: 500,
                      color: 'rgba(0,0,0,0.65)',
                    }}
                  >
                    100
                  </div>
                </div>
                <div
                  style={{
                    textAlign: 'center',
                  }}
                >
                  <div
                    style={{
                      fontSize: 12,
                      color: 'rgba(0,0,0,0.45)',
                    }}
                  >
                    已上架
                  </div>
                  <div
                    style={{
                      fontSize: 24,
                      fontWeight: 500,
                      color: 'rgba(0,0,0,0.65)',
                    }}
                  >
                    400
                  </div>
                </div>
                <div
                  style={{
                    textAlign: 'center',
                  }}
                >
                  <div
                    style={{
                      fontSize: 12,
                      color: 'rgba(0,0,0,0.45)',
                    }}
                  >
                    全部商品
                  </div>
                  <div
                    style={{
                      fontSize: 24,
                      fontWeight: 500,
                      color: 'rgba(0,0,0,0.65)',
                    }}
                  >
                    500
                  </div>
                </div>
              </div>
            </Card>
          </Col>
          <Col span={12}>
            <Card title={<CustomCard title="用户总览" />} bordered={false}>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                }}
              >
                <div
                  style={{
                    textAlign: 'center',
                  }}
                >
                  <div
                    style={{
                      fontSize: 12,
                      color: 'rgba(0,0,0,0.45)',
                    }}
                  >
                    今日新增
                  </div>
                  <div
                    style={{
                      fontSize: 24,
                      fontWeight: 500,
                      color: 'rgba(0,0,0,0.65)',
                    }}
                  >
                    100
                  </div>
                </div>
                <div
                  style={{
                    textAlign: 'center',
                  }}
                >
                  <div
                    style={{
                      fontSize: 12,
                      color: 'rgba(0,0,0,0.45)',
                    }}
                  >
                    昨日新增
                  </div>
                  <div
                    style={{
                      fontSize: 24,
                      fontWeight: 500,
                      color: 'rgba(0,0,0,0.65)',
                    }}
                  >
                    400
                  </div>
                </div>
                <div
                  style={{
                    textAlign: 'center',
                  }}
                >
                  <div
                    style={{
                      fontSize: 12,
                      color: 'rgba(0,0,0,0.45)',
                    }}
                  >
                    本月新增
                  </div>
                  <div
                    style={{
                      fontSize: 24,
                      fontWeight: 500,
                      color: 'rgba(0,0,0,0.65)',
                    }}
                  >
                    500
                  </div>
                </div>
              </div>
            </Card>
          </Col>
        </Row>
        <Card
          title={
            <div className={styles.tiem}>
              <CustomCard title="租金统计" />{' '}
              <div>
                <Radio.Group
                  defaultValue="week"
                  buttonStyle="solid"
                  onChange={this.onTime}
                  style={{
                    marginRight: 20,
                  }}
                >
                  <Radio.Button value="week">本周</Radio.Button>
                  <Radio.Button value="month">本月</Radio.Button>
                </Radio.Group>
                <RangePicker allowClear={false} value={rangeTimeData} />
              </div>
            </div>
          }
          style={{
            margin: '30px 0',
          }}
        >
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
            }}
          >
            <div
              style={{
                paddingRight: 30,
                marginTop: 53,
              }}
            >
              <div
                style={{
                  display: 'flex',
                  marginBottom: 14,
                }}
              >
                <div
                  style={{
                    width: 200,
                    fontSize: 14,
                    fontWeight: 400,
                    color: 'rgba(0,0,0,0.85)',
                  }}
                >
                  本月订单总数
                </div>
                <span>1000</span>
              </div>
              <div
                style={{
                  display: 'flex',
                }}
              >
                <div style={{ width: 200, color: '#000000' }}>较上月下降</div>
                <span>
                  10% <Icon type="arrow-down" style={{ color: '#1890FF' }} />
                </span>
              </div>
              <Divider
                style={{
                  margin: '35px 0',
                }}
              />
              <div
                style={{
                  display: 'flex',
                  marginBottom: 14,
                }}
              >
                <div
                  style={{
                    width: 200,
                    fontSize: 14,
                    fontWeight: 400,
                    color: 'rgba(0,0,0,0.85)',
                  }}
                >
                  本周订单数量
                </div>
                <span>1000</span>
              </div>
              <div
                style={{
                  display: 'flex',
                }}
              >
                <div style={{ width: 200, color: '#000000' }}>较上周上升</div>
                <span>
                  10% <Icon type="arrow-up" style={{ color: '#FA541C' }} />
                </span>
              </div>
            </div>
            <Line
              {...config}
              style={{
                width: 800,
                height: 300,
              }}
            />
          </div>
        </Card>
        <Card
          title={
            <div className={styles.tiem}>
              <CustomCard title="订单统计" />{' '}
              <div>
                <Radio.Group
                  defaultValue="week"
                  buttonStyle="solid"
                  onChange={this.onTimes}
                  style={{
                    marginRight: 20,
                  }}
                >
                  <Radio.Button value="week">本周</Radio.Button>
                  <Radio.Button value="month">本月</Radio.Button>
                </Radio.Group>
                <RangePicker allowClear={false} value={StatisticsData} />
              </div>
            </div>
          }
          style={{
            margin: '30px 0',
          }}
        >
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
            }}
          >
            <div
              style={{
                paddingRight: 30,
                marginTop: 53,
              }}
            >
              <div
                style={{
                  display: 'flex',
                  marginBottom: 14,
                }}
              >
                <div
                  style={{
                    width: 200,
                    fontSize: 14,
                    fontWeight: 400,
                    color: 'rgba(0,0,0,0.85)',
                  }}
                >
                  本月租金总额
                </div>
                <span>¥10，000</span>
              </div>
              <div
                style={{
                  display: 'flex',
                }}
              >
                <div style={{ width: 200, color: '#000000' }}>较上月下降</div>
                <span>
                  10% <Icon type="arrow-down" style={{ color: '#1890FF' }} />
                </span>
              </div>
              <Divider
                style={{
                  margin: '35px 0',
                }}
              />
              <div
                style={{
                  display: 'flex',
                  marginBottom: 14,
                }}
              >
                <div
                  style={{
                    width: 200,
                    fontSize: 14,
                    fontWeight: 400,
                    color: 'rgba(0,0,0,0.85)',
                  }}
                >
                  本周租金总额
                </div>
                <span>¥10，000</span>
              </div>
              <div
                style={{
                  display: 'flex',
                }}
              >
                <div style={{ width: 200, color: '#000000' }}>较上周上升</div>
                <span>
                  10% <Icon type="arrow-up" style={{ color: '#FA541C' }} />
                </span>
              </div>
            </div>
            <Area
              {...configs}
              style={{
                width: 800,
                height: 300,
              }}
            />
          </div>
        </Card>
      </PageHeaderWrapper>
    );
  }
}
