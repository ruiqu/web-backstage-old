import React, { Component } from 'react';
import { Card, Badge, Row, Col, Descriptions } from 'antd';
import CustomCard from '@/components/CustomCard';
import styles from './index.less';
import { router } from 'umi';
import { connect } from 'dva';
import MoneyModule from './components/moneyModule'
import TongzhiModule from './components/tongzhiModule'
import DayModule from './components/dayModule'
import { getParam } from '@/utils/utils';

@connect(({ order }) => ({ ...order }))
export default class Home extends Component {
  state = {
    mydb: {},
    stateList: [
      {
        text: '待审批',
        num: 0,
        icon: '',
        icon2: '',
      },
      {
        text: '待发货',
        num: 0,
        icon: '',
        icon2: '',
      },
      {
        text: '待归还',
        num: 0,
        icon: '',
        icon2: '',
      },
      {
        text: '待结算',
        num: 0,
        icon: '',
        icon2: '',
      },
      {
        text: '已完成',
        num: 0,
        icon: '',
        icon2: '',
      },
    ],
    card2List: [
      {
        text: '逾期订单',
        img: './yuqi.png',
        url: '/Order/BeOverdue',
      },
      {
        text: '到期未归还订单',
        img: './daoqi.png',
        url: '/Order/OverdueNoRetrun',
      },
      {
        text: '买断订单',
        img: './maiduan.png',
        url: '/Order/BuyOut',
      },
      {
        text: '续租订单',
        img: './xuzu.png',
        url: '/Order/RentRenewal',
      },
      {
        text: '购买订单',
        img: './goumai.png',
        url: '/Order/Purchase',
      },
    ],
  };
  componentDidMount() {
    if (getParam('code')) {
      router.push(`/user`);
    } else {
      this.props.dispatch({
        type: 'order/businessOrderStatisticsUsing',
        callback: res => {
          console.log("请求到的数据，", res)
          this.setState({
            mydb: res,
            stateList: [
              {
                text: '待审核',
                num: res.waitingAuditOrderCount || 0,
                icon: '',
                icon2: '',
              },
              {
                text: '待发货',
                num: res.pendingOrderCount || 0,
                icon: '',
                icon2: '',
              },
              {
                text: '租用中',
                num: res.rentingOrderCount || 0,
                icon: '',
                icon2: '',
              },
             
              {
                text: '当日客户拒绝关单',
                //num: res.v当日客户拒绝||0,
                num: 10,
                icon: '',
                icon2: '',
              },
            ],
          });
        },
      });
    }
  }

  // 跳转
  iconRouter(url) {
    router.push(url);
  }

  router2(index, isRouter) {
    if (!isRouter) {
      return;
    }
    const statusList = ['11', '04', '06', '07', '09'];

    router.push({
      pathname: '/Order/HomePage',
      query: {
        status: statusList[index],
      },
    });
  }

  render() {
    const { stateList, card2List, mydb } = this.state;
    let rs={marginTop: 30,}//头部样式
    let zr={ marginLeft: 20,color:'#C9C9C9' }
    let bott={paddingTop:10}
    let ym='https://rich-rent.oss-cn-hangzhou.aliyuncs.com/icon/'
    console.log("mydb:",mydb)
    let img="https://booleandata-crmmanagement-front.oss-cn-beijing.aliyuncs.com/operate/pay-circle%402x%20%282%29.png"
    let newlist=[
      {name:"今日订单数",val:mydb.v当日总订单,rname:"昨日:",rval:mydb.v昨日总订单, img:"编组 4.png"},
      {name:"今日转化率",val:mydb.v当日转化率,rname:"昨日:",rval:mydb.v昨日转化率, img:"编组 3.png"},
      {name:"今日成交",val:mydb.v当日成交,rname:"昨日:",rval:mydb.v昨日成交, img:'编组 2.png'},
      {name:"今日成交金额",val:mydb.v当日成交金额,rname:"昨日:",rval:mydb.v昨日成交金额, img:"编组.png"},
    ]
    
    let 数据看板=[
      {name:"待发货",val:mydb.pendingOrderCount,img:"编组 16.png"},
      {name:"待确认收货",val:mydb.waitingConfirmOrderCount,img:"编组 15.png"},
      {name:"租用中",val:mydb.rentingOrderCount,img:"编组 14.png"},
      {name:"逾期订单",val:456,img:"编组 13.png"},
      {name:"已完成",val:mydb.finishOrderCount,img:"编组 12.png"},
      {name:"通过未成交",val:456,img:"编组 11.png"},
      {name:"被拒绝",val:456,img:"编组 10.png"},
    ]
    let 到期数据=[
      {name:"今日到期金额",val:mydb.v今日到期金额,img:"金额.png"},
      {name:"今日回款率",val:mydb.v今日回款率,img:"人员.png"},
      {name:"今日到期数量",val:mydb.v今日到期数量,img:"金额1.png"},
      {name:"今日已付数量",val:mydb.v今日已付数量,img:"人员1.png"},
      {name:"合计到期金额",val:mydb.v合计到期金额,img:"欠费金额@2x.png"},
      {name:"合计回款率",val:mydb.v合计回款率,img:"会员.png"},
      {name:"合计到期数量",val:mydb.v合计到期数量,img:"欠费金额@2x1.png"},
      {name:"合计已付数量",val:mydb.v合计已付数量,img:"金额2.png"},
    ]
    let 产品类型=[
      {name:"月付",val:mydb.v月付订单,back:"#F2F6FF"},
      {name:"周付",val:mydb.v周付订单,back:"#EFFBF7"},
      {name:"10日付",val:mydb.v10天付订单,back:"#FEF5F8"},
      {name:"日付",val:mydb.v日付订单,back:"#F4F5FF"},
    ]

    let sjcss={display: 'flex',
    'justify-content':'space-between',
    padding:"0 30px",
    'text-align':'center',
   }
    return (
      <div className="index">
      
        <Row gutter={16} style={rs}>
        {newlist.map((item, index) => (
          <Col span={6}>
          <Card >
            <div>
            <span>{item.name}</span><span style={{ marginLeft: 30 }}>{item.val}</span>
            </div>
            <div style={bott}>
            <img src={ym+item.img}/>
              <span style={zr}>{item.rname}  {item.rval}</span>
            </div>
          </Card>
        </Col>
          ))}
          
        </Row>
        {/* <Card bordered={false} style={{ marginTop: 20 }} title={<CustomCard title="订单流程" />}>
          <div className={styles.steps}>
            {stateList.map((item, index) => (
              <div key={`steps-${index}`} className={styles.steps_div}>
                <div>
                  {index === stateList.length - 1 ? (
                    <img
                      alt=""
                      onClick={() => this.router2(index, true)}
                      className={styles.icon_img}
                      src={`./step${index + 1}.png`}
                    />
                  ) : (
                    <Badge count={item.num} overflowCount={99}>
                      <img
                        alt=""
                        onClick={() => this.router2(index, item.num)}
                        className={styles.icon_img}
                        src={`./step${index + 1}${item.num === 0 ? '_2' : ''}.png`}
                      />
                    </Badge>
                  )}
                  <p className={styles.steps_title}>{item.text}</p>
                </div>
                {index === stateList.length - 1 ? '' : <div className={styles.line}></div>}
              </div>
            ))}
          </div>
        </Card> */}


        <Card bordered={false} style={{ marginTop: 20 }} title={<CustomCard title="数据看板" />}>
          <div style={sjcss}>
            {数据看板.map((item, index) => (
              <div style={{ 'background-color':'#F3F4F8',
              'padding':'20px','border-radius': '25px',width:'120px'}}>
                
                  <p ><img src={ym+item.img}/></p>
                  <p >{item.val}</p>
                  <p >{item.name}</p>
                </div>
            ))}
          </div>
        </Card>


        <Row gutter={16} style={rs}>
          <Col span={18}>
          <Card >
          <div style={{display: 'flex','background-repeat':'no-repeat',
    'justify-content':'space-between',
    padding:"0 30px",'flex-wrap' : 'wrap'}}>
            {到期数据.map((item, index) => (
              <div style={{ backgroundImage:`url(${ym + item.img})`,
              'padding':'20px','margin':'10px','background-size':'100% 100%',
              'width':'20%',}}>
                  <div>
                    <div>
                    {item.name}
                    </div>
                    <div>
                    {item.val}
                    </div>
                  </div>
                </div>
            ))}
          </div>
          </Card>
        </Col>
          <Col span={6}>
          <Card >
          <div style={{display: 'flex',
    'justify-content':'space-between',
    'padding':"0 30px",'flex-wrap' : 'wrap','flex-direction':'column'}}>
            {产品类型.map((item, index) => (
              <div style={{ 'background-color':item.back,
              'padding':'20px','margin':'10px',
              'border-radius': '25px',
              width:180}}>
                  <span style={{ width:100 }}> {item.name} :</span>
                  <span  style={{ marginLeft: 20,width:150 }}> {item.val} </span>
                </div>
            ))}
          </div>
          </Card>
        </Col>
          
        </Row>

        {/* <Card bordered={false} style={{ marginTop: 20 }}>
          <div className={styles.tongzhiContainer}>
            <TongzhiModule />

            <MoneyModule />
          </div>
        </Card> */}

       {/*  <Card bordered={false} style={{ marginTop: 20, marginBottom: 40 }} title={<CustomCard title="常用功能" />}>
          <div className={styles.card2}>
            {card2List.map((item, index) => (
              <div
                key={`card2list-${index}`}
                className={styles.card2_div}
                onClick={() => this.iconRouter(item.url)}
              >
                <img alt="" className={styles.icon_img2} src={item.img} />
                <p className={styles.steps_title2}>{item.text}</p>
              </div>
            ))}
          </div>
        </Card> */}
      </div>
    );
  }
}
