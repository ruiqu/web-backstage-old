/**
 * 当日数据
 */
import { PureComponent } from "react"
import request from "@/services/baseService"
import { responsivePx } from "../../../../utils/utils"
import styles from "./index.less"
import router from "umi/router"

class DayModule extends PureComponent {
  state = {
    restMoney: "", // 获取账户余额接口所返回的数据
  }

  componentDidMount() {
    this.fetchMoney()
  }

  fetchMoney = () => {
    request("/hzsx/shopFund/getShopFundBalance", {}, "get").then(resData => {
      this.setState({ restMoney: resData })
    })
  }

  // 查看明细的处理方法
  seeDetailHandler = () => {
    router.push("/finance/capitalAccount")
  }

  render() {
    const w = responsivePx(301)
    const h = w / 1.127
    const wtyle = { width: w, height: h }
    return (
      <div style={wtyle} className={styles.wrapper}>
        <div className={styles.top}>
          <img className={styles.logo} src="https://zuwuzuwebs.oss-cn-beijing.aliyuncs.com/bgCms/%E8%B4%A6%E6%88%B7%E4%BD%99%E9%A2%9D%402x%20%281%29.png" />
          <span>账户余额</span>
        </div>
        <div className={styles.center}>
          { this.state.restMoney }
          <span className={styles.s1}>元</span>
        </div>
        <div onClick={this.seeDetailHandler} className={styles.btn}>查看明细</div>
      </div>
    )
  }
}

export default DayModule