/**
 * 通知模块
 */
import React from "react"
import { Carousel } from "antd"
import { responsivePx } from "../../../../utils/utils"
import request from "@/services/baseService"
import router from "umi/router"
import styles from "./index.less"

class TongzhiModule extends React.Component {
  state = {
    noticeList: [], // 通知列表
  }

  componentDidMount() {
    this.fetchList()
  }

  // 加载通知列表数据
  fetchList = () => {
    request("/hzsx/noticeCenter/queryOpeNoticeDetailList").then(res => {
      this.setState({ noticeList: res })
    })
  }

  // 点击通知
  clickNoticeHandler = item => {
    const { jumpUrl } = item
    const url = `/serviceCenter/problemDetail/${jumpUrl}`
    router.push(url)
  }

  render() {
    const w = responsivePx(746)
    const h = w / 2.7940074906367043
    const whStyle = { width: w, height: h, marginRight: responsivePx(20) }
    return (
      <div style={whStyle} className={`${styles.wrapper} tongzhi-module`}>
        <Carousel autoplay>
          {
            this.state.noticeList.map((obj, idx) => (
              <img
                onClick={() => this.clickNoticeHandler(obj)}
                key={idx}
                // className={styles.img}
                src={obj.materialItemFileUrl}
              />
            ))
          }
        </Carousel>
      </div>
    )
  }
}

export default TongzhiModule