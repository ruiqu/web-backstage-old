import React, { Component } from 'react';
import {
  Card, Badge, Row, Col, Descriptions, Icon,Spin ,
  Switch, NavBar, Checkbox, Radio, Input, Tabs, TabBar, DatePicker, Divider, Button
} from 'antd';
import { Line, Area, Liquid } from '@ant-design/charts';
import { RingProgress, G2 } from '@ant-design/plots';

const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
import CustomCard from '@/components/CustomCard';
import styles1 from './index.less';
import styles from './assets/index.module.css';
import request from '@/services/baseService';
import { getTimeDistance } from '@/utils/utils';
import { router } from 'umi';
import { connect } from 'dva';
import MoneyModule from './components/moneyModule'
import TongzhiModule from './components/tongzhiModule'
import DayModule from './components/dayModule'
import { getParam } from '@/utils/utils';
@connect(({ order }) => ({ ...order }))
export default class Home extends Component {
  state = {
    mydb: {},
    statis: {},
    rangeTimeData: getTimeDistance('week'),
    StatisticsData: getTimeDistance('week'),
    dataDat: {},
    loading:false,
    StaData: [],
    moneyData: [],
    stateList: [
      {
        text: '待支付',
        num:  0,
        icon: '编组 6@2x.png',
        icon2: '',
      },
      {
        text: '待审核',
        num:  0,
        icon: '编组 7@2x.png',
        icon2: '',
      },

      {
        text: '待成交',
        //num: res.v当日客户拒绝||0,
        num: 0,
        icon: '编组 9@2x.png',
        icon2: '',
      },
      {
        text: '客户拒绝关单',
        num:  0,
        icon: '编组 5@2x.png',
        icon2: '',
      },
    ],
    card2List: [
      {
        text: '逾期订单',
        img: './yuqi.png',
        url: '/Order/BeOverdue',
      },
      {
        text: '到期未归还订单',
        img: './daoqi.png',
        url: '/Order/OverdueNoRetrun',
      },
      {
        text: '买断订单',
        img: './maiduan.png',
        url: '/Order/BuyOut',
      },
      {
        text: '续租订单',
        img: './xuzu.png',
        url: '/Order/RentRenewal',
      },
      {
        text: '购买订单',
        img: './goumai.png',
        url: '/Order/Purchase',
      },
    ],
  };
  componentDidMount() {
    var day1 = new Date();
    day1.setTime(day1.getTime() - 24 * 60 * 60 * 1000);
    var s1 = day1.getFullYear() + '-' + (day1.getMonth() + 1) + '-' + day1.getDate();
    console.log(s1, 's1s1s1s1');
    console.log(this.state.rangeTimeData, 'rangeTimeData');
    const { dispatch } = this.props;
    dispatch({
      type: 'login/home',
      payload: {},
      callback: res => {
        this.setState({
          dataDat: res.data,
          StaData: res.data.orderReportDtoMonthList,
          moneyData: res.data.orderReportDtoMonthList,
        });
      },
    });

    if (getParam('code')) {
      router.push(`/user`);
    } else {
      /* this.props.dispatch({
        type: 'order/businessOrderStatisticsUsing',
        callback: res => {
          console.log("请求到的数据，", res)
        },
      }); */
    }
    this.reqdb()
    
  }
  
  reqdb(sta=""){
    this.setState({
      loading:true
    })
    
    request(`/hzsx/business/order/businessOrderStatistics?gx=`+sta,{gx:sta}).then(res => {
      console.log("rcs111", res)
      this.setState({
        mydb: res,
        loading:false,
        stateList: [
          {
            text: '待支付',
            num: res.unPayOrderCount || 0,
            icon: '编组 6@2x.png',
            icon2: '',
            status:"01",
          },
          {
            text: '待审核',
            num: res.waitingAuditOrderCount || 0,
            icon: '编组 7@2x.png',
            icon2: '',
            status:"11",
          },
    
          {
            text: '待成交',
            //num: res.v当日客户拒绝||0,
            num: res.v待成交单量,
            icon: '编组 9@2x.png',
            icon2: '',
            status:"88",
          },
          {
            text: '客户拒绝关单',
            num: res.v今日客户拒绝关单 || 0,
            icon: '编组 5@2x.png',
            icon2: '',
            status:"99",
          },
        ],
      });
      /* this.setState({
        statis: res
      }) */
    }).catch(e=>{
      console.log("aq获取异常了",e)
    })
  }

  // 跳转
  iconRouter(url) {
    router.push(url);
  }

  router2(index,ti) {
    if(!index)return
    let query={}
    //return console.log("aaa",index,ti)
    if(ti){
      query.ti=1
      if(ti==2){
       return router.push({
          pathname: '/zz/zyz',
          query,
        });
      }
    }
    else query.status=index
    console.log("aaaa",query)
    router.push({
      pathname: '/Order/HomePage',
      query,
    });
  }
  updatedb(){
    console.log("更新数据")
    this.reqdb(1)
  }
  render() {
    const { stateList, card2List, mydb, statis, StatisticsData ,loading} = this.state;


    let dataDat = statis;
    let rs = { marginTop: 30, }//头部样式
    let zr = { marginLeft: 20, color: '#7D7D7D' }
    let bott = { paddingTop: 10 }
    let ym = 'https://rich-rent.oss-cn-hangzhou.aliyuncs.com/icon/'
    console.log("mydb:", mydb)
    let img = "https://booleandata-crmmanagement-front.oss-cn-beijing.aliyuncs.com/operate/pay-circle%402x%20%282%29.png"
    let newlist = [
      { name: "今日订单数", val: mydb.v当日总订单, rname: "昨日:", rval: mydb.v昨日总订单, img: "编组 4.png" ,url:1},
      { name: "今日转化率", val: mydb.v当日转化率, rname: "昨日:", rval: mydb.v昨日转化率, img: "编组 3.png" ,url:1},
      { name: "今日成交", val: mydb.v当日成交, rname: "昨日:", rval: mydb.v昨日成交, img: '编组 2.png',url:2 },
      { name: "今日成交金额", val: mydb.v当日成交金额, rname: "昨日:", rval: mydb.v昨日成交金额, img: "编组.png",url:1 },
    ]
    const { StaData, moneyData, zhongl, yhzhongl } = this.state;
    var keyMap = {
      successOrderCount: '订单数',
      successOrderRent: '租金',
    };
    if (StaData) {
      for (var i = 0; i < StaData.length; i++) {
        var obj = StaData[i];
        for (var key in obj) {
          var newKey = keyMap[key];
          if (newKey) {
            obj[newKey] = obj[key];
            delete obj[key];
          }
        }
      }
    }
    if (moneyData) {
      for (var i = 0; i < moneyData.length; i++) {
        var obj = moneyData[i];
        for (var key in obj) {
          var newKey = keyMap[key];
          if (newKey) {
            obj[newKey] = obj[key];
            delete obj[key];
          }
        }
      }
    }
    const configs = {
      width: 200,

      title: {
        visible: true,
        //text: '租金总额（¥）',
      },
      autoFit: true,
      padding: 'auto',
      forceFit: true,
      data: moneyData,
      xField: 'statisticsDate',
      yField: '租金',
      yAxis: {
        label: {
          formatter: v => `${v}`.replace(/\d{1,3}(?=(\d{3})+$)/g, s => `${s},`),
        },
      },
      label: {
        visible: true,
        type: 'point',
      },
      point: {
        visible: true,
        size: 5,
        shape: 'diamond',
        style: {
          fill: 'white',
          stroke: '#2593fc',
          lineWidth: 2,
        },
      },
    };
    let 数据看板 = [
      { name: "待发货", val: mydb.pendingOrderCount, img: "编组 16.png",status:"04" },
      { name: "待确认收货", val: mydb.waitingConfirmOrderCount, img: "编组 15.png",status:"05" },
      { name: "租用中", val: mydb.rentingOrderCount, img: "编组 14.png",status:"06" },
      { name: "逾期订单", val: mydb.v逾期订单数, img: "编组 13.png" ,status:""},
      { name: "已完成", val: mydb.finishOrderCount, img: "编组 12.png",status:"09" },
      { name: "通过未成交", val: mydb.v通过未成交, img: "编组 11.png",status:"87" },
      { name: "被拒绝", val: mydb.v被拒绝, img: "编组 10.png" ,status:"89"},
    ]
    let v今日回款率=((mydb.v今日实收金额/mydb.v今日到期金额 )*100).toFixed(2)+"%"
    let v合计回款率=((mydb.v合计实收金额/mydb.v合计到期金额 )*100).toFixed(2)+"%"
    let v金额逾期率=(mydb.v合计到期金额-mydb.v合计实收金额)/mydb.v合计到期金额
    let v订单逾期率=(mydb.v合计到期数量-mydb.v合计已付数量)/mydb.v合计到期数量
    console.log("v合计逾期率",v金额逾期率)
    let 到期数据 = [
      { name: "今日到期金额", val: mydb.v今日到期金额, img: "金额.png" },
      { name: "今日回款率", val: v今日回款率, img: "人员.png" },
      { name: "今日到期数量", val: mydb.v今日到期数量, img: "金额1.png" },
      { name: "今日已付数量", val: mydb.v今日已付数量, img: "人员1.png" },
      { name: "合计到期金额", val: mydb.v合计到期金额, img: "欠费金额@2x.png" },
      { name: "合计回款率", val: v合计回款率, img: "会员.png" },
      { name: "合计到期数量", val: mydb.v合计到期数量, img: "欠费金额@2x1.png" },
      { name: "合计已付数量", val: mydb.v合计已付数量, img: "金额2.png" },
    ]
    
    const 订单逾期率 = {
      width:150,
      height:150,
      autoFit: false,
    percent: v订单逾期率,
    color: ['#FF6600', '#1774F9'],
    };
    const 金额逾期率 = {
      width:150,
      height:150,
      autoFit: false,
    percent: v金额逾期率,
    color: ['#FF6600', '#1774F9'],
    };
    let 产品类型 = [
      { name: "月付", val: mydb.v月付订单,je:mydb.v月付金额, back: "#F2F6FF", fclo: '#1774F9',url:"/zz/yuefu" },
      { name: "周付", val: mydb.v周付订单,je:mydb.v周付金额, back: "#EFFBF7", fclo: '#13AB8F',url:"/zz/zhoufu" },
      { name: "10日付", val: mydb.v10天付订单,je:mydb.v10天付金额, back: "#FEF5F8", fclo: '#F43A86',url:"/zz/day10fu" },
      { name: "日付", val: mydb.v日付订单,je:mydb.v日付金额, back: "#F4F5FF", fclo: '#6170F2',url:"/zz/rifu" },
    ]

    let sjcss = {
      display: 'flex',
      'justify-content': 'space-between',
      padding: "0 30px",
      'text-align': 'center',
    }
    return (
      <div className="index">
 <Spin spinning={loading} delay={500}>
   


        <Row gutter={16} style={{ 'height': "400px" }}>
        <Col span={24} style={{ padding: 10, fontSize: 16, color: '#000000', 'font-weight': 'bold', display: 'flex',
      'justify-content': 'space-between', }}>
          <div>
            当日数据
            <img src={ym+'椭圆形 2.png'}></img>
          </div>
          <div>
            <Button onClick={()=>{this.reqdb(1)}}>刷新</Button>
          </div>
          </Col>
          {newlist.map((item, index) => (
            <Col span={6}>
              <Card bordered={false} style={{ 'border-radius': '29px' }}>
                <div >
                  <a onClick={()=>{this.router2(1,item.url)}}>

                  <div className="text-wrapper_1 flex-row justify-between" style={{ display: 'flex', 'justify-content': 'space-between' }}>
                    <span className="text_7" style={{ fontSize: 20, color: '#000' }} >{item.name} :</span>
                    <span className="text_8" style={{ fontSize: 20, color: '#152660' }}>{item.val}</span>
                  </div>
                  <div style={{ paddingTop: 10, display: 'flex', 'justify-content': 'space-between', 'align-items': 'flex-end' }}>
                    <img src={ym + item.img} />
                    <span style={zr}>{item.rname}  {item.rval}</span>
                  </div>
                  </a>
                </div>
              </Card>
            </Col>


          ))}

<Col span={24} >
            <Card bordered={false} style={{ marginTop: 20,'border-radius': '29px' }} >
              <div className={styles1.steps}>
                {stateList.map((item, index) => (
                  <div  className={styles1.steps_div} >
                    <div style={{width:'100px','text-align':'center'}}>
                    <Badge style={{backgroundColor:'#1774F9'}} count={item.num} overflowCount={99}>
                          <img
                          onClick={() => this.router2(item.status)}
                            className={styles1.icon_img}
                            src={ym+item.icon}
                          />
                        </Badge>
                      <p className={styles1.steps_title} style={{width:'100px','text-align':'center'}}>{item.text}</p>
                    </div>
                    {index === stateList.length - 1 ? '' : <div className={styles1.line}></div>}
                  </div>
                ))}
              </div>
            </Card>
          </Col>

          <Col span={24} style={{ padding: 10, fontSize: 16, color: '#000000', 'font-weight': 'bold' }}>
            数据看板
            <img src={ym+'椭圆形 2.png'}></img>
          </Col>
          <Col span={24} >
            <Card bordered={false} style={{ 'border-radius': '29px' }} >
              <div style={sjcss}>
                {数据看板.map((item, index) => (
                  <div style={{
                    'background-color': '#F3F4F8',
                    'padding': '20px', 'border-radius': '25px', width: '120px'
                    
                  }}>
                    <a onClick={() => this.router2(item.status)}>

                    <div ><img  src={ym + item.img} /></div>
                    <div  style={{ fontSize: '22px', color: '#1774F9' }}>{item.val}</div>
                    <div   style={{ color: '#000' }}>{item.name}</div>
                    </a>
                  </div>
                ))}
              </div>
            </Card>

          </Col>


          
          <Col span={18} style={{ marginTop: 10 }}><div style={{ padding: 10, fontSize: 16, color: '#000000', 'font-weight': 'bold' }}>
            到期数据<img src={ym+'椭圆形 2.png'}></img>
          </div></Col>
          <Col span={6} style={{ marginTop: 10 }}>
            <div className="box_9 flex-col" />
            <div style={{ padding: 10, fontSize: 16, color: '#000000', 'font-weight': 'bold' }} className='text_27'>
              产品类型
              <img src={ym+'椭圆形 2.png'}></img>
            </div>

          </Col>
          <Col span={18} >

            <Card style={{ 'border-radius': '29px', 'height': "500px" }}>
              <div style={{
                display: 'flex',
                'justify-content': 'space-evenly',
                padding: "0 30px",
                color: '#131313',
                'text-align': 'center',
              }}>
                <div >
                  <div><RingProgress {...订单逾期率} /></div>
                  <div>订单逾期率</div>
                </div>
                <div>
                <div><RingProgress {...金额逾期率} /></div>
                  <div>金额逾期率</div>
                </div>
              </div>
              <div style={{
                display: 'flex', 'background-repeat': 'no-repeat',
                'justify-content': 'space-between',
                marginTop: 40, color: '#131313',
                padding: "10 30px", 'flex-wrap': 'wrap'
              }}>
                {到期数据.map((item, index) => (
                  <div style={{
                    backgroundImage: `url(${ym + item.img})`,
                    'padding': '20px', 'margin': '10px', 'background-size': '100% 100%',
                    'width': '20%',
                  }}>
                    <div>
                      <div>
                        {item.name}
                      </div>
                      <div style={{ fontSize: '20px',marginTop:10 }}>
                        {item.val}
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </Card>
          </Col>
          <Col span={6}>


            <Card style={{ 'border-radius': '29px', 'height': "500px" }}>
              <div style={{
                display: 'flex',
                'justify-content': 'space-between', 'height': "450px",
                'flex-wrap': 'wrap', 'flex-direction': 'column'
              }}>
                {产品类型.map((item, index) => (
                  <div style={{
                    'background-color': item.back,
                    'padding': '25px',
                    //'margin': '18 18px',
                    'border-radius': '25px',
                    width: '100%',
                  }}>
                    <a  onClick={() => this.iconRouter(item.url)} style={{ color: '#272727', width: 100 }}> {item.name} :</a>
                    <a onClick={() => this.iconRouter(item.url)} style={{ color: item.fclo, fontSize: '18px', marginLeft: 10, width: 150 }}> {item.val} (¥{item.je})</a>
                  </div>
                ))}
              </div>
            </Card>
          </Col>
          <Col span={24} style={{ marginTop: 10, padding: 10, fontSize: 16, color: '#000000', 'font-weight': 'bold' }}>
            大盘数据<img src={ym+'椭圆形 2.png'}></img>
          </Col>
          <Col span={24}>
            <Card bordered={false} style={{ 'border-radius': '29px' }} >
              <Area
                {...configs}
                style={{

                }}
              />
            </Card>
          </Col>
        </Row>

        </Spin>
      </div>
    );
  }
}
