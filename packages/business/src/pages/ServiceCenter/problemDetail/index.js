/**
 * 常见问题的详情页面
 */
import React from "react"
import { PageHeaderWrapper } from "@ant-design/pro-layout"
import { Card } from "antd"
import request from "@/services/baseService"
import styles from "./index.less"

class ProblemDetail extends React.Component {
  state = {
    detailObj: {}, // 问题的详情数据
    loading: false,
  }

  componentDidMount() {
    this.fetchDetailDataHandler()
  }

  /**
   * 加载问题详情数据
   */
  fetchDetailDataHandler = () => {
    const { id } = this.props.match.params
    this.setState({ loading: true })
    const url = "/hzsx/noticeCenter/queryOpeNoticeItemDetail"
    const postData = { id }
    request(url, postData, "post").then(res => {
      this.setState({ detailObj: res })
    }).finally(() => {
      this.setState({ loading: false })
    })
  }

  render () {
    return (
      <PageHeaderWrapper  title={false}>
        <Card className={styles.mb20} bordered={false} loading={this.state.loading}>
          <div className={styles.title}>{this.state.detailObj?.name}</div>
          <div
            className={styles.richContainer}
            dangerouslySetInnerHTML={{ __html: this.state.detailObj?.detail }}
          ></div>
        </Card>
      </PageHeaderWrapper>
    )
  }
}

export default ProblemDetail