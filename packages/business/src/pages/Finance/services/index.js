import request from "@/services/baseService";

export default {
  audit:(data)=>{
    return request(`/hzsx/splitBillConfig/audit`, data)
  },
  page:(data)=>{
    return request(`/hzsx/splitBillConfig/page`, data)
  },
  getShopList:(data)=>{
    return request(`/hzsx/splitBillConfig/getShopList`, data, 'get')
  },
  detail:(data)=>{
    return request(`/hzsx/splitBillConfig/detail`, data, 'get')
  },
  add:(data)=>{
    return request(`/hzsx/splitBillConfig/add`, data)
  },
  update:(data)=>{
    return request(`/hzsx/splitBillConfig/update`, data)
  },
}
