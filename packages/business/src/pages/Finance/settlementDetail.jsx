import React, { Component } from 'react';
import {
  Card,
  Descriptions,
  Button,
  Tabs,
  Divider,
  Modal,
  Form,
  Input,
  message,
} from 'antd';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import CustomCard from '@/components/CustomCard';
import settlementService from './services/settlement';
import { getParam, convertCurrency } from '@/utils/utils';
import request from "@/services/baseService"
import moment from 'moment';
import statusMap from "@/config/status.config"
const { TabPane } = Tabs;

@Form.create()
export default class settlementDetail extends Component {
  state = {
    current: 1,
    total: 1,
    data: {},
    visible: false,
    title: '',
    payInfoData: {},
    pageLoading: false, // 页面是否处于加载过程中
  };

  componentDidMount() {
    this.onAccountPeriodDetail();
  }

  onAccountPeriodDetail = () => {
    this.setState({ pageLoading: true })
    let ajaxPromise
    const isFromZijin = getParam("fromCapitalAccount")
    const pid = getParam('id')

    if (isFromZijin === '1') { // 从资金账户页面跳转过来
      const url = `/hzsx/shopFund/brokerageDetail?id=${pid}`
      ajaxPromise = request(url, {}, "get")
    } else { // 普通跳转
      ajaxPromise = settlementService.queryAccountPeriodDetail({ id: pid })
    }
    ajaxPromise.then(res => {
      this.setState({
        data: res,
      });
    }).finally(() => {
      this.setState({ pageLoading: false })
    })
  };

  /** 返回实际ID */
  returnExportId = () => {
    const obj = this.state.data || {}
    return obj.id
  }

  onPage = e => {
    this.setState(
      {
        current: e.current,
      },
      () => {
        this.onRemark(e.current, 3);
      },
    );
  };
  handleOk = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { title } = this.state;
        if (title === '备注') {
          settlementService.addRemark({ accountPeriodId: this.returnExportId(), ...values }).then(res => {
            message.success('备注成功！');
            this.setState(
              {
                visible: false,
                current: 1,
              },
              () => {
                this.onRemark(1, 3);
              },
            );
          });
        } else {
          values.settleAmount = parseFloat(values.settleAmount).toFixed(2);
          settlementService
            .submitSettle({ accountPeriodId: this.returnExportId(), ...values })
            .then(res => {
              message.success('结算成功！');
              this.setState(
                {
                  visible: false,
                  current: 1,
                },
                () => {
                  this.onAccountPeriodDetail();
                },
              );
            });
        }
      }
    });
  };

  handleCancel = e => {
    this.setState({
      visible: false,
    });
  };

  render() {
    const { data, payInfoData } = this.state;
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 },
      },
    };

    return (
      <PageHeaderWrapper  title={false}>
        <Card loading={this.state.pageLoading} bordered={false}>
          <Descriptions
            title={
              <div style={{ display: 'flex' }}>
                <CustomCard title="基本信息" />{' '}
              </div>
            }
          >
            <Descriptions.Item label="结算日期">{moment(data.settleDate).format('YYYY-MM-DD')}</Descriptions.Item>
            <Descriptions.Item label="商家名称">{data.shopName}</Descriptions.Item>
            {/* <Descriptions.Item label="结算账户">{data.accountIdentity}</Descriptions.Item> */}
            <Descriptions.Item label="结算状态">{statusMap[data.status]}</Descriptions.Item>
            <Descriptions.Item label="结算总额">{data.totalSettleAmount}</Descriptions.Item>
            <Descriptions.Item label="佣金">{data.totalBrokerage}</Descriptions.Item>
            {data.status !== 'WAITING_SETTLEMENT' ? (
              <Descriptions.Item label="实际结算金额">{data.settleAmount}</Descriptions.Item>
            ) : null}
          </Descriptions>
        </Card>
        <Card loading={this.state.pageLoading} bordered={false} style={{ marginTop: 20 }}>
          <Tabs defaultActiveKey="1">
            <TabPane tab="结算情况" key="1">
              <Descriptions title={<CustomCard title="常规账单" />}>
                <Descriptions.Item label="结算金额">{data.rentAmount}</Descriptions.Item>
                <Descriptions.Item label="佣金">{data.rentBrokerage}</Descriptions.Item>
              </Descriptions>
              <Button
                type="primary"
              >
                <a href={`#/finance/normal?id=${this.returnExportId()}`} target="_blank">
                  查看明细
                </a>
              </Button>
              <Button
                onClick={() =>
                  settlementService
                    .accountPeriodRent({ accountPeriodId: this.returnExportId() })
                    .then(res => {
                      message.success('导出任务创建成功，请前往“数据管理-导出数据下载”完成下载。');
                    })
                }
              >
                导出数据
              </Button>
              <Divider />
              <Descriptions title={<CustomCard title="买断账单" />}>
                <Descriptions.Item label="结算金额">{data.buyoutAmount}</Descriptions.Item>
                <Descriptions.Item label="佣金">{data.buyoutBrokerage}</Descriptions.Item>
              </Descriptions>
              <Button
                type="primary"
              >
                <a href={`#/finance/buyout?id=${this.returnExportId()}`} target="_blank">
                  查看明细
                </a>
              </Button>
              <Button
                onClick={() =>
                  settlementService
                    .accountPeriodBuyOut({ accountPeriodId: this.returnExportId() })
                    .then(res => {
                      message.success('导出任务创建成功，请前往“数据管理-导出数据下载”完成下载。');
                    })
                }
              >
                导出数据
              </Button>
              <Divider />
              <Descriptions title={<CustomCard title="购买账单" />}>
                <Descriptions.Item label="结算金额">{data.purchaseAmount}</Descriptions.Item>
                <Descriptions.Item label="佣金">{data.purchaseBrokerage}</Descriptions.Item>
              </Descriptions>
              <Button
                type="primary"
              >
                <a href={`#/finance/purchase?id=${this.returnExportId()}`} target="_blank">
                  查看明细
                </a>
              </Button>
              <Button
                onClick={() =>
                  settlementService
                    .accountPeriodPurchase({ accountPeriodId: this.returnExportId() })
                    .then(res => {
                      message.success('导出任务创建成功，请前往“数据管理-导出数据下载”完成下载。');
                    })
                }
              >
                导出数据
              </Button>
              <Divider />
            </TabPane>
          </Tabs>
        </Card>
        <Modal
          title={this.state.title}
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          footer={this.state.title === '查看凭证' ? '' : undefined}
          width={this.state.title === '查看凭证' ? 1100 : 520}
          destroyOnClose
        >
          {this.state.title === '备注' ? (
            <Form {...formItemLayout}>
              <Form.Item label="备注">
                {getFieldDecorator('content', {
                  rules: [{ required: true, message: '请输入' }],
                })(<Input placeholder="请输入" />)}
              </Form.Item>
            </Form>
          ) : this.state.title === '查看凭证' ? (
            <Descriptions bordered>
              <Descriptions.Item label="付款账户">
                杭州首新网络科技有限公司[布尔数据]
                <br />
                <b>lana@shouxin168.com</b>
              </Descriptions.Item>
              <Descriptions.Item label="转账金额">
                {payInfoData.amount} 元 （{convertCurrency(payInfoData.amount)}）
              </Descriptions.Item>
              <Descriptions.Item label="服务费">
                {payInfoData.serviceFee} 元 （以实际收取为准）
              </Descriptions.Item>
              <Descriptions.Item label="收款账户">
                收款人姓名：{payInfoData.toAccountName}
                <br />
                收款支付宝账号：{payInfoData.toAccountIdentity}
              </Descriptions.Item>
              <Descriptions.Item label="备注"><span style={{width:100}}>{payInfoData.remark}</span></Descriptions.Item>
              <Descriptions.Item label="标题">{payInfoData.title}</Descriptions.Item>

              <Descriptions.Item label="付款总额" span={3}>
                {payInfoData.amount} 元 （{convertCurrency(payInfoData.amount)}）
              </Descriptions.Item>
            </Descriptions>
          ) : (
            <Form {...formItemLayout}>
              <Form.Item label="实际结算金额">
                {getFieldDecorator('settleAmount', {
                  rules: [{ required: true, message: '请输入' }],
                })(<Input placeholder="请输入" suffix="元" />)}
              </Form.Item>
              <Form.Item label="转账业务标题">
                {getFieldDecorator('settleTitle', {
                  rules: [{ required: true, message: '请输入' }],
                })(<Input placeholder="请输入" />)}
              </Form.Item>
              <Form.Item label="业务备注">
                {getFieldDecorator('settleRemark', {})(<Input placeholder="请输入" />)}
              </Form.Item>
            </Form>
          )}
        </Modal>
      </PageHeaderWrapper>
    );
  }
}
