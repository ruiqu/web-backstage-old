import React, { PureComponent, Fragment } from 'react';
// import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import {
  Card,
  Descriptions,
  Spin,
  message,
  Table,
  Row,
  Col,
  Modal,
  Button,
  Cascader,
  Input,
  Divider,
  Form,
  Select,
  Radio,
  Popconfirm,
} from 'antd';
const { TextArea } = Input;
const { Option } = Select;
import CustomCard from '@/components/CustomCard';
import { renderDescriptionsItems } from '@/components/LZDescriptions';
import SplitBillService from '@/pages/Finance/services/splitBill';
import status from '@/config/status.config';
import { onTableData } from '@/utils/utils';
import { orderStatusMap, BuyOutEnum } from '@/utils/enum';
import { router } from 'umi';
const getStatus = new Map([
  [1, '全新'],
  [2, '99新'],
  [3, '95新'],
  [4, '9成新'],
  [5, '8成新'],
  [6, '7成新'],
]);
const getAuditState = new Map([
  [0, '正在审核'],
  [1, '审核不通过'],
  [2, '审核通过'],
]);
const getPayStatus = new Map([
  [1, '待支付'],
  [2, '已支付'],
  [3, '逾期已支付'],
  [4, '逾期待支付'],
  [5, '已取消'],
  [6, '已结算'],
  [7, '已退款'],
]);
const refundStatusList = {
  0: '待用户退货',
  1: '待商家收货',
  2: '待商家退款',
  3: '退款成功',
  4: '退款拒绝关闭',
  5: '退款关闭',
  6: '待商家同意申请',
};
const userOrderListStatus = {
  2: '损坏',
  3: '丢失',
  4: '其它',
};

const goodsColumns = [
  {
    title: '商品图片',
    dataIndex: 'imageUrl',
    align: 'center',
    render: text => <img style={{ width: 85, hight: 85 }} src={text} />,
  },
  {
    title: '商品名称',
    dataIndex: 'productName',
    align: 'center',
    key: 'productName',
    width: '20%',
  },
  {
    title: '商品编号',
    dataIndex: 'productId',
    align: 'center',
    key: 'productId',
    width: '20%',
  },
  {
    title: '规格颜色',
    dataIndex: 'spec',
    align: 'center',
    width: '20%',
  },
  {
    title: '数量',
    dataIndex: 'num',
    align: 'center',
  },
  {
    title: '买断规则',
    dataIndex: 'buyOutSupportV1',
    align: 'center',
    render: text => BuyOutEnum[text] || '-',
    // render: text => (text === 1 ? '是' : '否'),
  },
];
const billColumns = [
  {
    title: '总押金',
    dataIndex: 'totalDeposit',
    key: 'totalDeposit',
    align: 'center',
  },
  {
    title: '押金减免',
    dataIndex: 'depositReduction',
    align: 'center',
    key: 'depositReduction',
  },
  // {
  //   title: '信用减免',
  //   dataIndex: 'credit',
  //   align: 'center',
  //   key: 'credit',
  // },
  {
    title: '实付押金',
    dataIndex: 'deposit',
    align: 'center',
    key: 'deposit',
  },
  {
    title: '总租金',
    dataIndex: 'totalRent',
    align: 'center',
    key: 'totalRent',
  },
  {
    title: '运费',
    dataIndex: 'freightPrice',
    align: 'center',
    key: 'freightPrice',
  },
  {
    title: '平台优惠',
    dataIndex: 'platformCouponReduction',
    align: 'center',
    key: 'platformCouponReduction',
  },
  {
    title: '店铺优惠',
    dataIndex: 'couponReduction',
    align: 'center',
    key: 'couponReduction',
  },
];

@connect()
@Form.create()
class GoodsDetail extends PureComponent {
  state = {
    loading: true,
    visible: false,
    imgSrc: [],
    imgLength: 0,
    current: 0,
    categoryVisible: false,
    categoryId: null,
    actionVisible: false,
    confirmLoading: false,
    tittle: '',
    status: 0,
    reason: null,
    isScale: null,
  };

  columns = [
    {
      title: '款式',
      align: 'center',
      key: 'style',
      width: '30%',
      render: (text, record) => {
        return record.values.map((item, index) => {
          if (index + 1 !== record.values.length) {
            return (
              <Fragment key={item.name}>
                <span>{item.name}/</span>
              </Fragment>
            );
          } else {
            return (
              <Fragment key={item.name}>
                <span>{item.name}</span>
              </Fragment>
            );
          }
        });
      },
    },
    {
      title: '市场价格',
      dataIndex: 'marketPrice',
      align: 'center',
      key: 'marketPrice',
    },
    {
      title: '销售价格',
      align: 'center',
      key: 'salePrice',
      render: record => {
        return (
          <div>
            {record.marketPrice * 1.24 > record.salePrice ? (
              <span>{record.salePrice}</span>
            ) : (
              <span style={{ color: 'red' }}>{record.salePrice}</span>
            )}
          </div>
        );
      },
    },
    {
      title: '租赁价格',
      dataIndex: 'cyclePrices',
      align: 'center',
      key: 'cyclePrices',
      render: text => (
        <div>
          {text.map(item => (
            <p key={item.id}>
              {item.days}天：{item.price}
            </p>
          ))}
        </div>
      ),
    },
  ];
  columnses = [
    {
      title: '款式',
      align: 'center',
      key: 'style',
      width: '30%',
      render: (text, record) => {
        return record.values.map((item, index) => {
          if (index + 1 !== record.values.length) {
            return (
              <Fragment key={item.name}>
                <span>{item.name}/</span>
              </Fragment>
            );
          } else {
            return (
              <Fragment key={item.name}>
                <span>{item.name}</span>
              </Fragment>
            );
          }
        });
      },
    },
    {
      title: '市场价格',
      dataIndex: 'marketPrice',
      align: 'center',
      key: 'marketPrice',
    },
    {
      title: '租赁价格',
      dataIndex: 'cyclePrices',
      align: 'center',
      key: 'cyclePrices',
      render: text => (
        <div>
          {text.map(item => (
            <p key={item.id}>
              {item.days}天：{item.price}
            </p>
          ))}
        </div>
      ),
    },
  ];

  addressColumns = [
    {
      title: '姓名',
      dataIndex: 'name',
      align: 'center',
      key: 'name',
    },
    {
      title: '手机',
      dataIndex: 'telephone',
      align: 'center',
      key: 'telephone',
    },
    {
      title: '省',
      dataIndex: 'provinceSrc',
      align: 'center',
      key: 'provinceSrc',
    },
    {
      title: '市',
      dataIndex: 'citySrc',
      align: 'center',
      key: 'citySrc',
    },
    {
      title: '区',
      dataIndex: 'areaSrc',
      align: 'center',
      key: 'areaSrc',
    },
    {
      title: '街道',
      dataIndex: 'street',
      align: 'center',
      key: 'street',
      width: '200px',
    },
  ];

  componentDidMount() {
    this.getDetail();
  }

  getDetail = () => {
    const {
      match: {
        params: { id },
      },
    } = this.props;
    this.setState({
      loading: true,
      id,
    });
    SplitBillService.buyOutDetail({ id })
      .then(res => {
        this.setState({
          detail: res || {},
        });
      })
      .finally(() => {
        this.setState({
          loading: false,
        });
      });
  };

  viewImg = (data, index) => {
    this.setState({
      visible: true,
      imgSrc: data,
      imgLength: data.length,
      current: index,
    });
  };
  // 图片modal的关闭
  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };
  // 下一张
  next = () => {
    let a = this.state.current;
    a += 1;
    this.setState({
      current: a,
    });
  };
  // 上一张
  prev = () => {
    let a = this.state.current;
    a -= 1;
    this.setState({
      current: a,
    });
  };

  daifa = record => {
    SplitBillService.paySplitBill({ orderId: record.buyOutOrderId }).then(res => {
      message.success('代发成功');
    });
  };

  render() {
    const { loading, imgSrc, visible, current, imgLength, detail = {} } = this.state;
    const {
      orderInfoDto = {},
      shopInfoDto = {},
      productInfo = {},
      userOrderCashesDto = {},
      originalOrderInfo = [],
      userOrderBuyOutDto = {},
    } = detail;
    const orderInfoFields = [
      { label: '下单人姓名', value: orderInfoDto.userName },
      { label: '下单人手机号', value: orderInfoDto.telephone },
      { label: '身份证号', value: orderInfoDto.idCard },
      { label: '下单时间', value: orderInfoDto.createTime },
      { label: '订单编号', value: orderInfoDto.orderId },
      // { label: '订单状态', value: orderStatusMap[orderInfoDto && orderInfoDto.status] },
      // { label: '完结订单数', value: orderInfoDto.userFinishCount },
      // { label: '在途订单数', value: orderInfoDto.userPayCount },
      // {
      //   label: '风险分',
      //   value: orderInfoDto.score,
      //   render: <span className="primary-color">{orderInfoDto && orderInfoDto.score}分</span>,
      // },
    ];

    const shopInfoField = [
      { label: '商家名称', value: shopInfoDto.shopName },
      { label: '商家电话', value: shopInfoDto.telephone },
    ];

    const oldOrderColumns = [
      {
        title: '订单编号',
        dataIndex: 'orderId',
        align: 'center',
        key: 'orderId',
        render: orderId => {
          return <a onClick={() => router.push(`/Order/Details?id=${orderId}`)}>{orderId}</a>;
        },
      },
      {
        title: '商品名称',
        dataIndex: 'productName',
        align: 'center',
        key: 'productName',
      },
      {
        title: '总租金',
        dataIndex: 'totalRent',
        align: 'center',
        key: 'totalRent',
      },
      {
        title: '已付租金',
        dataIndex: 'payedRent',
        align: 'center',
      },

      {
        title: '订单状态',
        dataIndex: 'status',
        align: 'center',
        key: 'status',
        render: text => orderStatusMap[text],
      },
    ];
    const buyOrderColumns = [
      {
        title: '创建时间',
        dataIndex: 'createTime',
        width: 120,
        align: 'center',
      },
      {
        title: '订单编号',
        dataIndex: 'orderId',
        width: 120,
        align: 'center',
        key: 'orderId',
      },
      {
        title: '商品名称',
        dataIndex: 'productName',
        align: 'center',
        key: 'productName',
      },
      {
        title: '销售价',
        dataIndex: 'realSalePrice',
        align: 'center',
        key: 'realSalePrice',
      },
      {
        title: '已付租金',
        dataIndex: 'paidRent',
        align: 'center',
        key: 'paidRent',
      },
      {
        title: '买断尾款',
        dataIndex: 'endFund',
        align: 'center',
        key: 'endFund',
      },
      { title: '商家结算金额', dataIndex: 'toShopAmount', align: 'center' },
      { title: '平台佣金', dataIndex: 'toOpeAmount', align: 'center' },
      {
        title: '与商家结算状态',
        dataIndex: 'splitBillStatus',
        align: 'center',
        render: text => status[text],
      },
      { title: '结算人', dataIndex: 'userName', align: 'center' },
      { title: '结算时间', dataIndex: 'splitBillTime', align: 'center' },
    ];
    return (
      <PageHeaderWrapper  title={false}>
        <Spin spinning={false}>
          <Card bordered={false} style={{ marginTop: 20 }}>
            {renderDescriptionsItems(orderInfoFields, <CustomCard title="订单信息" />)}
            <Divider dashed />
            {renderDescriptionsItems(shopInfoField, <CustomCard title="商家信息" />)}
            <CustomCard title="商品信息" />
            <Table
              style={{ marginTop: 20 }}
              columns={goodsColumns}
              dataSource={onTableData([productInfo])}
              rowKey={record => record.id}
              pagination={false}
            />
            <CustomCard title="账单信息" style={{ marginTop: 30 }} />
            <Table
              style={{ marginTop: 10 }}
              columns={billColumns}
              dataSource={onTableData([userOrderCashesDto])}
              rowKey={record => record.id}
              pagination={false}
            />
            <CustomCard title="买断信息" style={{ marginTop: 30 }} />
            <Table
              style={{ marginTop: 10 }}
              scroll={{ x: true }}
              columns={buyOrderColumns}
              dataSource={onTableData([userOrderBuyOutDto])}
              rowKey={record => record.id}
              pagination={false}
            />
            <CustomCard title="原订单信息" style={{ marginTop: 30 }} />
            <Table
              style={{ marginTop: 10 }}
              columns={oldOrderColumns}
              dataSource={onTableData([originalOrderInfo])}
              rowKey={record => record.id}
              pagination={false}
            />
          </Card>
        </Spin>

        <Modal
          visible={visible}
          footer={
            <div style={{ textAlign: 'center' }}>
              <Button disabled={current <= 0 ? true : false} onClick={this.prev}>
                上一张
              </Button>
              <Button
                disabled={current >= imgLength - 1 ? true : false}
                type="primary"
                onClick={this.next}
              >
                下一张
              </Button>
            </div>
          }
          onCancel={this.handleCancel}
          width={600}
        >
          <img
            src={imgSrc.length > 0 ? imgSrc[current].src : null}
            alt=""
            style={{ width: '100%', paddingTop: 15 }}
          />
        </Modal>
      </PageHeaderWrapper>
    );
  }
}

export default GoodsDetail;
