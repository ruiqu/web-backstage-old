import React, { Component } from 'react';
import {
  Card, Badge, Row, Col, Descriptions, Icon, Spin, Table, Form,Select ,
  Switch, NavBar, Checkbox, Radio, Input, Tabs, TabBar, DatePicker, Divider, Button
} from 'antd';
const FormItem = Form.Item;
import { Line, Area, Liquid } from '@ant-design/charts';
import { RingProgress, Column } from '@ant-design/plots';
import MyPageTable from '@/components/MyPageTable';
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
import request from '@/services/baseService';
import { getTimeDistance } from '@/utils/utils';
import { router } from 'umi';
import { connect } from 'dva';
import moment from 'moment';
import { getParam } from '@/utils/utils';
@Form.create()
@connect(({ order }) => ({ ...order }))
export default class Home extends Component {
  state = {
    mydb: [],
    statis: {},
    rangeTimeData: getTimeDistance('week'),
    StatisticsData: getTimeDistance('week'),
    createTimeStart: null,
    createTimeEnd: null,
    day: 7,
    channel:[],
    channel1:[],
    channel2:[],
    
    v转化统计: [],
    v成交排行: [],
    v订单排行: [],
    dayNum: 7,
    day7db: {},
    dataDat: {},
    loading: false,
    StaData: [],
    moneyData: [],

  };
  componentDidMount() {
    var day1 = new Date();
    day1.setTime(day1.getTime() - 24 * 60 * 60 * 1000);
    var s1 = day1.getFullYear() + '-' + (day1.getMonth() + 1) + '-' + day1.getDate();
    console.log(s1, 's1s1s1s1');
    console.log(this.state.rangeTimeData, 'rangeTimeData');
    const { dispatch } = this.props;



    if (getParam('code')) {
      router.push(`/user`);
    } else {

    }
    this.reqdb(0)

  }



f获取几天前时间(day){
  var now = new Date();
  var date = now.getDate();
  now.setDate(date - day);  
  var y = now.getFullYear();
  var m = (now.getMonth() + 1).toString().padStart(2, "0");
  var d = now.getDate().toString().padStart(2, "0");
  var ymd = y + "-" + m + "-" + d;
  return ymd
}

  reqdb(dayNum = 1) {
    this.setState({
      loading: true,
      dayNum
    })


    let db = {}

    db.createTimeEnd = this.f获取几天前时间(1) + " 23:59:59"
    db.createTimeStart = this.f获取几天前时间(7) + " 00:00:00"
    if(dayNum==0){
      db.createTimeEnd = this.f获取几天前时间(0) + " 23:59:59"
      db.createTimeStart = this.f获取几天前时间(0) + " 00:00:00"
    }
    //昨天
    if(dayNum==-1){
      db.createTimeEnd = this.f获取几天前时间(1) + " 23:59:59"
      db.createTimeStart = this.f获取几天前时间(1) + " 00:00:00"
    }
    //前天
    if(dayNum==-2){
      db.createTimeEnd = this.f获取几天前时间(2) + " 23:59:59"
      db.createTimeStart = this.f获取几天前时间(2) + " 00:00:00"
    }
    console.log("aaa时间为:",db)
    this.newreqdb(db)
    /* request(`/zyj-backstage-web/hzsx/business/channel/getChannelDb`, { dayNum }).then(res => {
      console.log("rcs111", res)
      this.handdb(res.channels)
    }).catch(e => {
      this.setState({ loading: false, });
      console.log("aq获取异常了", e)
    }) */
  }
  newreqdb( values) {
    this.setState({
      loading: true,
    })
    values.istime=1
    request(`/zyj-backstage-web/hzsx/business/channel/getChannelDb`, values).then(res => {
      console.log("rcs111", res)
      this.handdb(res.channels)
      this.setState({
        mydb:res.channels1,
        channel2:res.channels,
      })
    }).catch(e => {
      this.setState({ loading: false, });
      console.log("aq获取异常了", e)
    })
  }



  // 跳转
  iconRouter(url) {
    router.push(url);
  }

  router2(index, ti) {
    if (!index) return
    let query = {}
    //return console.log("aaa",index,ti)
    if (ti) {
      query.ti = 1
      if (ti == 2) {
        return router.push({
          pathname: '/zz/zyz',
          query,
        });
      }
    }
    else query.status = index
    console.log("aaaa", query)
    router.push({
      pathname: '/Order/HomePage',
      query,
    });
  }
  //处理成表格数据
  handdb(mydb) {
    //先获取最近七天
    let day7cj = {}  //最近七天成交
    let day7 = {}  //成近七天总订单
    let v总图表 = []
    let v七天成交排行 = []
    let v日期数据 = []
    let v每日成交 = {}
    let v每日下单 = {}
    for (let i = 0; i < mydb.length; i++) {
      const ele = mydb[i];
      mydb[i].v转化率 = ele.v成交单量 ? (ele.v成交单量 / ele.v订单量 * 100).toFixed(2) : 0
      for (let key in ele.v渠道成交订单) {
        if (!v每日成交[key]) v每日成交[key] = []
        v每日成交[key].push({
          name: key,
          day: ele.name,
          val: ele.v渠道成交订单[key],
        })
        if (!day7cj[key]) day7cj[key] = ele.v渠道成交订单[key]
        else day7cj[key] += ele.v渠道成交订单[key]
      }
      for (let key in ele.v渠道订单) {
        if (!v每日下单[key]) v每日下单[key] = []
        v每日下单[key].push({
          name: key,
          day: ele.name,
          val: ele.v渠道订单[key],
        })
        //先判断日期有没有，如果没 ，那么添加进去
        if (!day7[key]) day7[key] = ele.v渠道订单[key]
        else day7[key] += ele.v渠道订单[key]
      }

    }
    //获取当天日期排行最前的几个是属于哪个
    for (let key in day7) {
      let dbb = {
        name: "订单量",
        day: key,
        val: day7[key],
      }
      v总图表.push(dbb)
    }
    for (let key in day7cj) {
      let dbb = {
        name: "成交量",
        day: key,
        val: day7cj[key],
      }
      v总图表.push(dbb)
    }
    console.log("ele.v渠道订单", v总图表)
    //console.log("ele.每日的成交与下单", this.pickGreatest(v每日成交数据,5),this.pickGreatest(v每日下单数据,5))
    let 每日下单表格数据 = []
    let 每日成交表格数据 = []
    for (let key in v每日成交) {
      let db = this.pickGreatest(v每日成交[key], 5)
      for (let key1 in db) {
        每日成交表格数据.push({
          name: db[key1].day,
          day: key,
          val: db[key1].val,
        })
      }
    }
    for (let key in v每日下单) {
      //v每日下单[key] = this.pickGreatest(v每日下单[key], 5)
      let db = this.pickGreatest(v每日下单[key], 5)
      for (let key1 in db) {
        每日下单表格数据.push({
          name: db[key1].day,
          day: key,
          val: db[key1].val,
        })
      }
    }
    //再来两个循环，分别把数据加进对象


    console.log("ele.每日的成交与下单", 每日下单表格数据, 每日成交表格数据)
    this.setState({
      mydb,
      v成交排行: 每日成交表格数据,
      v订单排行: 每日下单表格数据,
      v转化统计: v总图表,
      loading: false,
    });
  }

  pickGreatest(arr, num) {
    const sorter = (a, b) => b.val - a.val;
    const descendingCopy = arr.slice().sort(sorter);
    return descendingCopy.splice(0, num);
  }
  handleSubmit = e => {
    e && e.preventDefault();
    this.props.form.validateFields((err, values) => {
      console.log("点击了提交", values)
      if (values.createDate) {
        values.createTimeStart = moment(values.createDate[0]).format('YYYY-MM-DD') + " 00:00:00";
        values.createTimeEnd = moment(values.createDate[1]).format('YYYY-MM-DD') + " 23:59:59";
        console.log("点击了提交", values)
      } 
      this.newreqdb(values)
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const { stateList, card2List, mydb, statis, StatisticsData,channel2,
      loading, dayNum, v转化统计, v成交排行, v订单排行 } = this.state;
    let ym = 'https://rich-rent.oss-cn-hangzhou.aliyuncs.com/icon/'
    let configs = {}
    let total = mydb.length;

    const data = [
      {
        name: 'London',
        月份: 'Jan.',
        月均降雨量: 18.9,
      },
      {
        name: 'London',
        月份: 'Feb.',
        月均降雨量: 28.8,
      },
      {
        name: 'London',
        月份: 'Mar.',
        月均降雨量: 39.3,
      },
      {
        name: 'London',
        月份: 'Apr.',
        月均降雨量: 81.4,
      },
      {
        name: 'London',
        月份: 'May',
        月均降雨量: 47,
      },
      {
        name: 'London',
        月份: 'Jun.',
        月均降雨量: 20.3,
      },
      {
        name: 'London',
        月份: 'Jul.',
        月均降雨量: 24,
      },
      {
        name: 'London',
        月份: 'Aug.',
        月均降雨量: 35.6,
      },
      {
        name: 'Berlin',
        月份: 'Jan.',
        月均降雨量: 12.4,
      },
      {
        name: 'Berlin',
        月份: 'Feb.',
        月均降雨量: 23.2,
      },
      {
        name: 'Berlin',
        月份: 'Mar.',
        月均降雨量: 34.5,
      },
      {
        name: 'Berlin',
        月份: 'Apr.',
        月均降雨量: 99.7,
      },
      {
        name: 'Berlin',
        月份: 'May',
        月均降雨量: 52.6,
      },
      {
        name: 'Berlin',
        月份: 'Jun.',
        月均降雨量: 35.5,
      },
      {
        name: 'Berlin',
        月份: 'Jul.',
        月均降雨量: 37.4,
      },
      {
        name: 'Berlin',
        月份: 'Aug.',
        月均降雨量: 42.4,
      },
    ];

    const v订单排行设置 = {
      data: v订单排行,
      isGroup: true,
      xField: 'day',
      yField: 'val',
      seriesField: 'name',

      /** 设置颜色 */
      //color: ['#1ca9e6', '#f88c24'],

      /** 设置间距 */
      // marginRatio: 0.1,
      label: {
        // 可手动配置 label 数据标签位置
        position: 'middle',
        // 'top', 'middle', 'bottom'
        // 可配置附加的布局方法
        layout: [
          // 柱形图数据标签位置自动调整
          {
            type: 'interval-adjust-position',
          }, // 数据标签防遮挡
          {
            type: 'interval-hide-overlap',
          }, // 数据标签文颜色自动调整
          {
            type: 'adjust-color',
          },
        ],
      },
    };
    const v成交排行设置 = {
      data: v成交排行,
      isGroup: true,
      xField: 'day',
      yField: 'val',
      seriesField: 'name',

      /** 设置颜色 */
      //color: ['#1ca9e6', '#f88c24'],

      /** 设置间距 */
      // marginRatio: 0.1,
      label: {
        // 可手动配置 label 数据标签位置
        position: 'middle',
        // 'top', 'middle', 'bottom'
        // 可配置附加的布局方法
        layout: [
          // 柱形图数据标签位置自动调整
          {
            type: 'interval-adjust-position',
          }, // 数据标签防遮挡
          {
            type: 'interval-hide-overlap',
          }, // 数据标签文颜色自动调整
          {
            type: 'adjust-color',
          },
        ],
      },
    };
    const config = {
      data: v转化统计,
      isGroup: true,
      xField: 'day',
      yField: 'val',
      seriesField: 'name',

      /** 设置颜色 */
      //color: ['#1ca9e6', '#f88c24'],

      /** 设置间距 */
      // marginRatio: 0.1,
      label: {
        // 可手动配置 label 数据标签位置
        position: 'middle',
        // 'top', 'middle', 'bottom'
        // 可配置附加的布局方法
        layout: [
          // 柱形图数据标签位置自动调整
          {
            type: 'interval-adjust-position',
          }, // 数据标签防遮挡
          {
            type: 'interval-hide-overlap',
          }, // 数据标签文颜色自动调整
          {
            type: 'adjust-color',
          },
        ],
      },
    };

    const columns = [

      {
        title: '渠道名称',
        dataIndex: 'name',
        width: 100,
      },
      {
        title: '订单数量',
        dataIndex: 'v订单量',
        width: 120,
        defaultSortOrder: 'descend',
        sorter: (a, b) => a.v订单量 - b.v订单量,
      },
      {
        title: '下单人数',
        dataIndex: 'v下单人数',
        width: 120,
        sorter: (a, b) => a.v下单人数 - b.v下单人数,
      },
      {
        title: '待支付',
        dataIndex: 'v待支付',
        width: 80,
      },
      {
        title: '待审批',
        dataIndex: 'v待审批',
        width: 80,
      },
      {
        title: '待成交',
        dataIndex: 'v待成交',
        width: 80,
      },
      {
        title: '客户拒绝关单',
        dataIndex: 'v客户拒绝关单',
        width: 140,
      },
      {
        title: '转化率',
        //dataIndex: 'v转化率',
        width: 100,
        render: e => {
          if(!e.v成交量)return 0
          return ((e.v成交量 / e.v订单量) * 100).toFixed(2) + "%"
        },
      },
      {
        title: '逾期量',
        dataIndex: 'v逾期量',
        width: 100,
      },
      {
        title: '逾期率',
        //dataIndex: 'v转化率',
        width: 100,
        render: e => {
          if(!e.v成交量)return 0
          return ((e.v逾期量 / e.v成交量) * 100).toFixed(2) + "%"
        },
      },
      {
        title: '成交单数',
        dataIndex: 'v成交量',
        width: 120,
        sorter: (a, b) => a.v成交量 - b.v成交量,
      },
      {
        title: '成交金额',
        dataIndex: 'v成交金额',
        width: 100,
      },


    ];

    return (
      <div className="index">
        <Spin spinning={loading} delay={500}>
          <Row gutter={16} style={{ 'height': "400px" }}>
            <Col span={24} style={{ marginTop: 10, padding: 10, fontSize: 16, color: '#000000', 'font-weight': 'bold' }}>
              渠道数据<img src={ym + '椭圆形 2.png'}></img>
            </Col>
            <Col span={24}>
              <Card bordered={false}>
                <Form layout="inline" onSubmit={this.handleSubmit}>
                  <Row>
                    <Col span={8}>
                      <Form.Item label="渠道来源">
                        {getFieldDecorator('cid', {
                          // initialValue: '',
                        })(
                          <Select
                            placeholder="渠道来源"
                            allowClear
                            style={{ width: 180 }}
                          >
                            {this.state.channel2.map(value => {
                              return (
                                <Option value={value.id+''} key={value.id}>
                                  {value.name}
                                </Option>
                              );
                            })}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col span={8}>
                      <Form.Item label="">
                        {getFieldDecorator('createDate', {})(<RangePicker />)}
                      </Form.Item>
                    </Col>
                    <Col span={24}>

                      <Button disabled={dayNum == -1} type="primary" onClick={() => this.reqdb(-1)}>
                        昨天
                      </Button>
                      <Button disabled={dayNum == 0} type="primary" onClick={() => this.reqdb(0)}>
                        今天
                      </Button>
                      <Button disabled={dayNum == -2} type="primary" onClick={() => this.reqdb(-2)}>
                        前天
                      </Button>
                      <Button disabled={dayNum == 7} type="primary" onClick={() => this.reqdb(7)}>
                        7天
                      </Button>
                      <Button disabled={dayNum == 15} type="primary" onClick={() => this.reqdb(15)}>
                        15天
                      </Button>
                      <Button disabled={dayNum == 30} type="primary" onClick={() => this.reqdb(30)}>
                        30天
                      </Button>
                      <Button type="primary" htmlType="submit">
                        查询
                      </Button>
                    </Col>
                  </Row>
                </Form>
                <div>
                </div>

                {/* <MyPageTable
                  paginationProps={paginationProps}
                  dataSource={mydb}
                  scroll
                  bordered
                  columns={columns}
                  //onPage={this.onChange}
                /> */}
                <div>
                  <Table
                    scroll={{ x: true, y: 450 }}
                    columns={columns}
                    dataSource={mydb}
                    pagination={false}
                  //onChange={this.onChange}
                  />
                </div>
              </Card>

            </Col>

            <Col span={24} style={{ marginTop: 10, padding: 10, fontSize: 16, color: '#000000', 'font-weight': 'bold' }}>
              大盘数据<img src={ym + '椭圆形 2.png'}></img>
            </Col>
            <Col span={24}>
              <Card bordered={false} style={{ 'border-radius': '29px' }} >
                <Column {...config} />
              </Card>
            </Col>
            <Col span={24} style={{ marginTop: 10, padding: 10, fontSize: 16, color: '#000000', 'font-weight': 'bold' }}>
              订单排行<img src={ym + '椭圆形 2.png'}></img>
            </Col>
            <Col span={24}>
              <Card bordered={false} style={{ 'border-radius': '29px' }} >
                <Column {...v订单排行设置} />
              </Card>
            </Col>
            <Col span={24} style={{ marginTop: 10, padding: 10, fontSize: 16, color: '#000000', 'font-weight': 'bold' }}>
              成交排行<img src={ym + '椭圆形 2.png'}></img>
            </Col>
            <Col span={24}>
              <Card bordered={false} style={{ 'border-radius': '29px' }} >
                <Column {...v成交排行设置} />
              </Card>
            </Col>
          </Row>

        </Spin>
      </div>
    );
  }
}
