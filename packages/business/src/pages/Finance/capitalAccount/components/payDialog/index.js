/**
 * 重置的表单
 */
import React from "react"
import { Form, Button, Modal, InputNumber, message } from "antd"
import PropTypes from "prop-types"
import request from "@/services/baseService"
import styles from "./index.less"

@Form.create()
class ChongzhiForm extends React.PureComponent {
  state = {
    hasRedirectToAlipay: false, // 是否已经跳转到支付宝的支付页面
  }

  componentDidMount() {
    const visibilityChangeEvent = this.returnEventListenerName() // 不同浏览器的事件名
    document.addEventListener(visibilityChangeEvent, this.visibilityChangeHandler) // 监听页面的隐藏以及显示
  }

  componentWillUnmount() {
    this.cancelLoopFetch()
    const visibilityChangeEvent = this.returnEventListenerName() // 不同浏览器的事件名
    document.removeEventListener(visibilityChangeEvent, this.visibilityChangeHandler)
  }

  returnEventListenerName = () => {
    const eName = this.returnEventName()
    return eName.replace(/hidden/i, "visibilitychange") 
  }

  // 返回事件名，不同浏览器的事件名不同
  returnEventName = () => {
    let hiddenProperty = "hidden" in document ? "hidden" :    
      "webkitHidden" in document ? "webkitHidden" :    
      "mozHidden" in document ? "mozHidden" : null // 不同浏览器事件名有不同
    return hiddenProperty
  }

  // 监听页面的显示
  visibilityChangeHandler = () => {
    const eveName = this.returnEventName()
    if (!document[eveName]) { // 页面被激活
      if (this.state.hasRedirectToAlipay) { // 有跳转到支付宝的支付页面，那么此时重新拉取一下最新的数据
        this.checkHasPay()
        this.cancelLoopFetch()
        this.loopFetch()
      }
    } else { // 页面隐藏
      this.cancelLoopFetch()
    }
  }

  // 清轮询
  cancelLoopFetch = () => {
    this.tid && clearInterval(this.tid)
  }

  // 进行轮询
  loopFetch = () => {
    this.tid = setInterval(() => {
      this.checkHasPay()
    }, 2000)
  }

  // 判断是否支付成功
  checkHasPay = () => {
    this.fetchNewestMoney().then(newMoney => {
      if (newMoney != this.props.money) { // 说明充值成功
        this.cancelLoopFetch()
        message.success("充值成功")
        this.setState({ hasRedirectToAlipay: false }) // 成功拉取了数据的话，那么进行重置
        this.props.refreshPageHandler() // 拉取最新的数据
        this.props.hideModalHandler() // 隐藏模态
      }
    })
  }

  // 加载最新的余额
  fetchNewestMoney = () => {
    return request("/hzsx/shopFund/getShopFundBalance", {}, "get").then(res => {
      return res // 最新的余额
    })
  }

  // 手动关闭弹窗
  closeModal = () => {
    this.props.form.resetFields() // 重置输入的金额
    if (this.state.hasRedirectToAlipay) { // 手动关闭的话，也检查一下是否支付成功
      this.checkHasPay()
      this.props.refreshPageHandler()
    } 
    this.props.hideModalHandler()
  }

  // 生成二维码中或者支付中禁止交互
  checkBtnAndInputShouldDisabled = () => { // 说明跳转到了支付宝页面
    return this.state.hasRedirectToAlipay
  }

  // 生成二维码
  gotoPay = () => {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const amount = values.money
        const url = `/hzsx/shopFund/recharge?amount=${amount}`
        request(url, {}, "get").then(res => {
          const payUrl = res
          this.setState({ hasRedirectToAlipay: true })
          this.loopFetch() // 轮询是否付款
          location.href = payUrl
          // const a = document.createElement("a")
          // a.href = payUrl
          // a.target = "_blank"
          // document.body.appendChild(a)
          // a.click()
          // document.body.removeChild(a)
        })
      }
    })
  }

  render() {
    const { getFieldDecorator } = this.props.form

    return (
      <Modal
        title="充值"
        visible={this.props.showPayModal}
        onCancel={this.closeModal}
        footer={null}
        maskClosable={false}
      >
        <Form ref="chongzhiForm" layout="inline">
          <Form.Item label="金额（元）：">
            {
              getFieldDecorator(
                "money",
                {
                  initialValue: "",
                  rules: [
                    {
                      validator: (_, value, callback) => {
                        if (value == null || value == "") {
                          callback("充值金额不可为空")
                        } else if (value <= 0) {
                          callback("0元不可充值")
                        } else if (value >= 50000) {
                          callback("单笔充值需小于5万元")
                        } else {
                          callback()
                        }
                      }
                    }
                  ]
                },
              )(
                <InputNumber
                  min={0}
                  max={50000}
                  className={styles.w160}
                  type="number"
                  placeholder="请输入充值金额"
                  allowClear
                  disabled={this.checkBtnAndInputShouldDisabled()}
                />
              )
            }
          </Form.Item>
          <Button
            className={styles.mt4}
            type="primary"
            loading={this.checkBtnAndInputShouldDisabled()}
            onClick={this.gotoPay}
          >
            去支付
          </Button>
        </Form>
      </Modal>
    )
  }
}

ChongzhiForm.propTypes = {
  money: PropTypes.number, // 余额
  showPayModal: PropTypes.bool, // 是否显示弹窗
  hideModalHandler: PropTypes.func, // 关闭弹窗的方法
  refreshPageHandler: PropTypes.func, // 更新账户明细的回调方法
}

export default ChongzhiForm