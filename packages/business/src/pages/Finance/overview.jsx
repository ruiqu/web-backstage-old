/**
 * 结算明细查询页面
 */
import React from "react"
import { PageHeaderWrapper } from "@ant-design/pro-layout"
import { Card, Tabs, Form, Select, Input, DatePicker, Button, Row, message } from "antd"
import { connect } from "dva"
import { defaultAppVersion, defaultPageSize, tab1, tab2, tab3 } from "../../models/fianceOverview"
import styles from "./overview.less"
import request from "@/services/baseService"
import NormalTable from "./tables/normalTable"
import BuyoutTable from "./tables/buyoutTable"
import PurchaseTable from "./tables/purchaseTable"

const { TabPane } = Tabs
const { RangePicker } = DatePicker

// tab栏数据
const tabs = [
  { key: tab1, tab: "常规订单", },
  { key: tab2, tab: "买断订单", },
  // { key: tab3, tab: "购买订单", }
]


@connect(({ financeOverview }) => ({
  ...financeOverview, // 把/src/model/finaceOverview文件中的所有state数据都暴露在this.props中
}))
@Form.create()
class Overview extends React.Component {
  state = {
    loading: false, // 是否正在加载表格数据中
    listApiRes: {}, // 订单列表接口返回的数据
  }

  componentDidMount() {
    const postData = this.generatePostData()
    this.fetchDataHandler(postData)
  }

  returnActiveTab = () => this.timelyTab || this.props.activeTab || tab1 // 默认是第一个tab，优先级，先本地，在redux，在默认

  /**
  * 合成请求参数
  * @param {object} customizeObj : 第三方对象
  * @param {boolean} isChangeTab : 是否是又切换tab动作引起的
  */
  generatePostData = (customizeObj, isChangeTab)  => {
    if (customizeObj) return customizeObj // 如果传入了自定义参数的话，那么以这个为准，优先级最高
  
    const obj = this.props.form.getFieldsValue() // 当前表单所输入的值
    const cache = this.returnQueryObj() // 当前tab所对应的缓存值

    if (isChangeTab) { // 说明是切换tab进来的，那么此时是缓存里面的值优先级更高
      return { ...obj, ...cache }
    } else { // 非切换tab，表单里面的值优先级更高
      return { ...cache, ...obj }
    }
  }

  // 忽略无效数据
  ignoreEmptyData = data => {
    const postObj = {}
    Object.keys(data).forEach(key => {
      const val = data[key];
      (val != undefined && val !== "") && (postObj[key] = val)
    })
    return postObj
  }

  /**
  * 加载列表数据的请求方法
  * @param {*} postObj
  */
  fetchDataHandler = postObj => {
    postObj.appVersion = "ZWZ"
    if (!postObj.appVersion) { // 缺少合作方式
      message.warn("请选择合作方式")
      return
    }

    if (this.state.loading) return // 当前处于加载中状态

    // 对开始时间和结束时间做一个转换
    const { times } = postObj
    if (times && times.length) {
      const time1 = times[0] // 开始时间
      const time2 = times[1] // 结束时间
      time1 && (postObj.startTime = time1.format("YYYY-MM-DD 00:00:00"))
      time2 && (postObj.endTime = time2.format("YYYY-MM-DD 23:59:59"))
    } else { // 不存在
      delete postObj.endTime
      delete postObj.startTime
    }
    const focusTab = this.returnActiveTab()

    this.props.dispatch({ type: "financeOverview/mutListApiParams", payload: { val: { ...postObj }, key: focusTab } }) // 保存到redux
    delete postObj["times"]

    const url = `/hzsx/accountPeriod/${focusTab}`
    
    this.setState({ loading: true })
    request(url, this.ignoreEmptyData(postObj), "post").then(resData => {
      this.setState({ listApiRes: resData })
    }).finally(() => {
      this.setState({ loading: false })
    })
  }

  /**
  * 点击查询时触发
  * @param {*} event 
  * @param {*} obj : 自定义参数
  * @param {*} tab 
  * @returns 
  */
  onSearch = event => { // 点击搜索的时候把值存储到redux
    event.preventDefault()
    const obj = this.generatePostData()
    obj.pageNumber = 1 // 从第一页开始搜索
    this.fetchDataHandler(obj)
  }

  // 切换菜单栏的时候触发
  changeTabHandler = tab => {
    this.props.form.resetFields() // 这个是必要的，尽管redux覆盖了值
    this.timelyTab = tab // 实时的tab变化值
    this.props.dispatch({ type: "financeOverview/mutActiveTab", payload: tab })
    const postData = this.generatePostData(null, true)
    this.fetchDataHandler(postData)
  }

  // 重置表单输入数据
  resetForm = () => {
    this.props.form.resetFields()
    const resetPostData = {
      appVersion: defaultAppVersion,
      orderId: "",
      times: [],
      pageNumber: 1,
      pageSize: defaultPageSize,
    }
    const postObj = this.generatePostData(resetPostData)
    this.fetchDataHandler(postObj)
  }

  // 返回筛选数据
  returnQueryObj = () => {
    const tab = this.returnActiveTab()
    return this.props.listApiParams?.[tab] || {}
  }

  // 改变分页器的页码时触发
  changePagenumberHandler = currentPageNumber => {
    const payload = {
      tab: this.returnActiveTab(),
      key: "pageNumber",
      val: currentPageNumber,
    }
    this.props.dispatch({ type: "financeOverview/muPartOfListApiParams", payload })
    const postObj = this.generatePostData()
    postObj.pageNumber = currentPageNumber
    this.fetchDataHandler(postObj)
  }

  changePageSizeHandler = size => {
    const payload = {
      tab: this.returnActiveTab(),
      key: "pageSize",
      val: size,
    }
    this.props.dispatch({ type: "financeOverview/muPartOfListApiParams", payload })
    const postObj = this.generatePostData()
    postObj.pageNumber = 1
    postObj.pageSize = size
    this.fetchDataHandler(postObj)
  }

  // 渲染搜索区域
  renderSearchArea = () => {
    const { getFieldDecorator } = this.props.form
    const cacheForm = this.returnQueryObj()

    return (
      <Form className={styles.mb15} layout="inline" onSubmit={this.onSearch}>
        <Row className={styles.mb20}>
          {/* <Form.Item label="合作方式">
            {
              getFieldDecorator("appVersion", { initialValue: cacheForm.appVersion })(
                <Select className={styles.selectWrapper}>
                  {
                    options.map(obj => (
                      <Select.Option key={obj.value} value={obj.value}>{ obj.cn }</Select.Option>
                    ))
                  }
                </Select>
              )
            }
          </Form.Item> */}
          <Form.Item label="订单编号">
            {
              getFieldDecorator("orderId", { initialValue: cacheForm.orderId })(
                <Input placeholder="请输入订单编号" className={styles.inputWrapper} allowClear/>
              )
            }
          </Form.Item>
          <Form.Item label="账单生成时间">
            {
              getFieldDecorator("times", { initialValue: cacheForm.times })(
                <RangePicker />
              )
            }
          </Form.Item>
        </Row>
        <Button type="primary" htmlType="submit">
          查询
        </Button>
        <Button onClick={this.resetForm}>
          重置
        </Button>
      </Form>
    )
  }

  // 渲染表格区域
  renderTableArea = () => {
    const tab = this.returnActiveTab()

    let ReactEle
    if (tab === tab1) ReactEle = NormalTable // 常规订单
    if (tab === tab2) ReactEle = BuyoutTable // 买断订单
    if (tab === tab3) ReactEle = PurchaseTable // 购买订单

    const { records, total } = this.state.listApiRes || {}
    const { pageNumber, pageSize } = this.returnQueryObj() || {}

    return (
      <ReactEle
        tableData={records}
        pageSize={pageSize}
        loading={this.state.loading}
        total={total}
        current={pageNumber}
        onChangeHandler={this.changePagenumberHandler}
        onShowSizeChangeHandler={this.changePageSizeHandler}
      />
    )
  }

  render() {
    return (
      <PageHeaderWrapper  title={false}>
        <Card bordered={false} style={{ marginTop: 20 }}>
          <Tabs animated={false} onChange={this.changeTabHandler} activeKey={this.returnActiveTab()}>
            {
              tabs.map(obj => (
                <TabPane tab={obj.tab} key={obj.key}>
                  {/** 搜索区域 */}
                  { this.renderSearchArea() }
                  
                  {/** 表格区域 */}
                  { this.renderTableArea() }
                </TabPane>
              ))
            }
          </Tabs>
        </Card>
      </PageHeaderWrapper>
    )
  }
}

export default Overview