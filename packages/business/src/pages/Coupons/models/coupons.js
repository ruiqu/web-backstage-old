import * as servicesApi from '@/services/coupons';
import { router } from 'umi';
import { message } from 'antd';
export default {
  namespace: 'coupons',
  state: {
    queryInfo: {
      pageNum: 1,
      pageSize: 10,
    },
    total: 0,
    list: [],
    productList: [],
    BusinessPrdouct: {},
    listData: [],
    listData2: [],
    detailDat: {},
    BindList: [],
    Bindtotal: 1,
  },
  effects: {
    *getCouponListByPage({ payload }, { call, put }) {
      const res = yield call(servicesApi.getCouponListByPage, payload);
      if (res) {
        yield put({
          type: 'saveList',
          payload: { data: res.data, queryInfo: payload },
        });
      }
    },
    *saveCoupon({ payload, callback }, { call }) {
      const info = { ...payload };
      if (payload.type === 0) {
        info.start = payload.startEnd[0].format('YYYY-MM-DD 00:00:00');
        info.end = payload.startEnd[1].format('YYYY-MM-DD 00:00:00');
      }
      if (payload.radioPurchase === '0') {
        info.minPurchase = 0;
      }
      let work = servicesApi.saveCouponByShopId;
      if (payload.id) {
        work = servicesApi.updateShopCoupon;
      }
      const res = yield call(work, info);
      if (res) {
        callback();
      }
    },
    *delCouponById({ payload, callback }, { call }) {
      const res = yield call(servicesApi.delCouponById, payload);
      if (res) {
        callback();
      }
    },
    *setBusinessPrdouct({ payload, callback }, { call }) {
      const res = yield call(servicesApi.setBusinessPrdouct, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
        yield put({
          type: 'business',
          payload: res,
        });
      }
    },
    *getUpload({ payload, callback }, { call }) {
      const res = yield call(servicesApi.getUpload, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    *couponAdd({ payload, callback }, { call }) {
      const res = yield call(servicesApi.couponAdd, payload);
      if (res) {
        message.success('新增成功');
        router.push('/coupons/list');
      }
    },
    *modify({ payload, callback }, { call }) {
      const res = yield call(servicesApi.modify, payload);
      if (res) {
        message.success('修改成功');
        router.push('/coupons/list');
      }
    },
    
    *list({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.queryPage, payload);
      if (res) {
        yield put({
          type: 'listData',
          payload: res,
        });
      }
    },
    *couponTemplatequeryPage({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.couponTemplatequeryPage, payload);
      if (res) {
        yield put({
          type: 'listData',
          payload: res.data,
        });
      }
    },
    *getAssignAbleTemplate({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.getAssignAbleTemplate, payload);
      if (res) {
        yield put({
          type: 'listData2',
          payload: res.data,
        });
      }
    },
    *deleteList({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.deleteList, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    *couponPackagemodify({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.couponPackagemodify, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    *detailData({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.detailDataList, payload);
      if (res) {
        yield put({
          type: 'detailDatas',
          payload: res.data,
        });
      }
    },
    *getUpdatePageData({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.getUpdatePageData, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    *detailBindList({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.detailBindList, payload);
      if (res) {
        yield put({
          type: 'detailBindLists',
          payload: res,
        });
      }
    },
    *upDate({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.upDate, payload);
      if (res) {
        message.success('修改成功');
        router.push('/coupons/list');
      }
    },
    *couponPackage({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.add, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    *couponPackageList({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.queryPage, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    *couponPackageDelete({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.couponPackageDelete, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    *getPageData({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.getUpdatePageDatas, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    *getupdate({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.add, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    *getGoods({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.getGoods, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    *getSelectAlls({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.getSelectAlls, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
  },
  reducers: {
    saveList(state, { payload }) {
      return {
        ...state,
        list: payload.data.records,
        total: payload.data.total,
        queryInfo: {
          ...payload.queryInfo,
        },
      };
    },
    
    saveProductList(state, { payload }) {
      return { ...state, productList: payload };
    },
    listData(state, { payload }) {
      console.log('payload:', payload);
      return {
        ...state,
        listData: payload.records,
        total: payload.total,
      };
    },
    listData2(state, { payload }) {
      console.log('payload:', payload);
      return {
        ...state,
        listData2: payload.records,
        total: payload.total,
      };
    },
    detailDatas(state, { payload }) {
      return {
        ...state,
        detailDat: payload.data,
      };
    },
    detailBindLists(state, { payload }) {
      return {
        ...state,
        BindList: payload.data.records,
        Bindtotal: payload.data.total,
      };
    },
  },
};
