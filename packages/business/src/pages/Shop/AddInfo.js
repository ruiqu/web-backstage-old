import React, { Fragment } from 'react';
import { connect } from 'dva';
import {
  Tabs,
  Form,
  Spin,
  Card,
  Input,
  Upload,
  Icon,
  message,
  Row,
  Col,
  Button,
  DatePicker,
  Modal,
  Divider,
  Radio,
  InputNumber
} from 'antd';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { uploadUrl } from '@/config';
import { cloneDeep, isEqual } from 'lodash';
import router from 'umi/router';
import Prompt from 'umi/prompt';
import CustomCard from '@/components/CustomCard';
import { LZFormItem } from '@/components/LZForm';
import moment from 'moment';
import busShopService from '@/services/busShop';
import { getToken } from '@/utils/localStorage';
import word from '../../assets/word.png';
import RouterWillLeave from '@/components/RouterWillLeave';

const { RangePicker } = DatePicker;
const FormItem = Form.Item;

// 默认的占位数据
const defaultInfo = {
  shop: {
    name: '',
    logo: '',
    serviceTel: '',
    recvMsgTel: '',
    createTime: undefined,
    updateTime: null,
    deleteTime: null,
    background: 'https://zwzrent-oss.laiyongmall.com/5989e1c0a22041ce97af4bfb456ec351.png',
    description: '',
    status: 3,
    isPhoneExamination: 1,
    enterpriseName: null,
    realname: null,
    userEmail: '',
    userName: '',
    userTel: '',
    zfbCode: '',
    zfbName: '',
    shopContractLink: '',
  },
  enterpriseInfoDto: {
    shopEnterpriseInfos: {
      name: '',
      registrationCapital: '',
      businessLicenseNo: '',
      licenseSrc: null,
      licenseStart: '2019-10-03 00:00:00',
      licenseEnd: '2099-01-01 00:00:00',
      licenseProvince: '',
      licenseProvinceStr: '',
      licenseCity: '',
      licenseCityStr: '',
      businessScope: '',
      realname: '',
      telephone: null,
      contactName: '',
      contactTelephone: '',
      contactQq: null,
      contactEmail: '',
      status: 0,
      identity: '',
      licenseStreet: '',
      shopName: null,
      idcardDirect: null,
      idcardBack: null,
    },
    shopEnterpriseCertificates: [
      {
        image: '',
        type: 0,
      },
      {
        image: '',
        type: 3,
      },
      {
        image: '',
        type: 4,
      },
    ],
  },
};

@connect()
@Form.create()
class EditShopInfo extends React.PureComponent {
  state = {
    loading: false,
    uploading: false,
    detail: {},
    originValues: {},
    shopInfo: {},
    enterpriseInfo: {},
    shopEnterpriseCertificates: [],
    shopContractLink: '',
    licenseSrcFileList: [],
    fileName: '',
    isSubmit: false,
  };

  componentWillMount = () => {
    const {
      match: {
        params: { id },
      },
    } = this.props;
    if (!id) {
      this.selectBusShopInfo(id);
    }
  };

  selectBusShopInfo = id => {
    this.setState({
      loading: true,
    });
    busShopService
      .selectBusShopInfo()
      .then(res => {
        if (!res.shop) {
          res = defaultInfo;
        }
        this.setState({
          detail: res || {},
          shopInfo: res.shop || {},
          shopEnterpriseCertificates: res.enterpriseInfoDto.shopEnterpriseCertificates || [],
          enterpriseInfo: res.enterpriseInfoDto.shopEnterpriseInfos || {},
          isPhoneExamination: res.shop.isPhoneExamination,
          shopContractLink: res.shop.shopContractLink,
        });
      })
      .finally(() => {
        const values = this.props.form.getFieldsValue();
        this.setState({
          loading: false,
          originValues: values,
        });
      });
  };

  // 限制用户上传大小与格式
  beforeUpload = file => {
    const isJPG =
      file.type === 'image/jpeg' || file.type === 'image/png' || file.type === 'image/gif';
    if (!isJPG) {
      message.error('图片格式不正确');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('图片大于2MB');
    }
    return isJPG && isLt2M;
  };

  handleUploadChange = (info, type) => {
    const { shopInfo, shopEnterpriseCertificates } = this.state;
    const { fileList, file } = info;
    if (file.status === 'uploading') {
      this.setState({ uploading: true });
    }

    if (info.file.status === 'done') {
      const hasImgs = shopEnterpriseCertificates && shopEnterpriseCertificates.length > 1;
      const data = info.file.response.data;
      const map = {
        idcardBack: 4,
        idcardDirect: 3,
        licenseSrc: 0,
      };
      if (type === 'shopContractLink') {
        this.setState({
          shopContractLink: data,
          hetong: info.file,
        });
      } else if (type === 'logo') {
        this.setState({
          shopInfo: {
            ...shopInfo,
            logo: data,
          },
        });
      } else if (Object.keys(map).includes(type)) {
        const index = hasImgs && shopEnterpriseCertificates.findIndex(f => f.type === map[type]);
        if (index > -1) {
          const _shopEnterpriseCertificates = cloneDeep(shopEnterpriseCertificates);
          _shopEnterpriseCertificates[index].image = data;
          this.setState({
            shopEnterpriseCertificates: _shopEnterpriseCertificates,
          });
        }
      }
      this.setState({
        uploading: false,
      });
    }
  };

  handleSubmit = () => {
    const { shopEnterpriseCertificates, shopContractLink, shopInfo } = this.state;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { shop, shopEnterpriseInfos } = values;
        shop.createTime = moment(shop.createTime).format('YYYY-MM-DD hh:mm:ss');
        shop.shopContractLink = shopContractLink;
        shop.logo = shopInfo.logo;
        shopEnterpriseInfos.licenseStart = moment(shopEnterpriseInfos.license[0]).format(
          'YYYY-MM-DD 00:00:00',
        );
        shopEnterpriseInfos.licenseEnd = moment(shopEnterpriseInfos.license[1]).format(
          'YYYY-MM-DD 00:00:00',
        );
        delete shopEnterpriseInfos.license;
        values.enterpriseInfoDto = {
          shopEnterpriseInfos: shopEnterpriseInfos,
          shopEnterpriseCertificates,
        };
        busShopService.updateShopAndEnterpriseInfo(values).then(res => {
          message.success('更新成功');
          this.setState({
            isSubmit: true,
          });
          router.push('/shop/info');
        });
      } else {
        message.error('请补全所有必填信息');
      }
    });
  };

  handleCancel = () => {
    router.push('/shop/info');
  };

  renderShopInfo = () => {
    const { form } = this.props;
    const { shopInfo, shopContractLink } = this.state;
    const { getFieldDecorator } = form;
    const { uploading, hetong } = this.state;
    return (
      <div>
        <Card bordered={false} style={{ marginTop: 20 }}>
          <CustomCard title="店铺信息" />
          <Form layout="vertical">
            <Row>
              <Col span={6}>
                <LZFormItem
                  field="shop.name"
                  label="店铺名称"
                  getFieldDecorator={getFieldDecorator}
                  requiredMessage="请输入店铺名称"
                  initialValue={shopInfo.name}
                >
                  <Input placeholder="请输入" />
                </LZFormItem>
              </Col>
              <Col span={6} offset={2}>
                <LZFormItem
                  field="shop.serviceTel"
                  label="店铺客服电话"
                  getFieldDecorator={getFieldDecorator}
                  requiredMessage="请输入店铺客服电话"
                  initialValue={shopInfo.serviceTel}
                >
                  <Input placeholder="请输入" />
                </LZFormItem>
              </Col>
              <Col span={6} offset={2}>
                <LZFormItem
                  field="shop.userEmail"
                  label="店铺联系邮箱"
                  getFieldDecorator={getFieldDecorator}
                  requiredMessage="请输入店铺联系邮箱"
                  initialValue={shopInfo.userEmail}
                >
                  <Input placeholder="请输入" />
                </LZFormItem>
              </Col>
            </Row>
            <Row>
              <Col span={6}>
                <LZFormItem
                  field="shop.userName"
                  label="店铺联系人姓名"
                  getFieldDecorator={getFieldDecorator}
                  requiredMessage="请输入店铺联系人姓名"
                  initialValue={shopInfo.userName}
                >
                  <Input placeholder="请输入" />
                </LZFormItem>
              </Col>
              <Col span={6} offset={2}>
                <LZFormItem
                  field="shop.userTel"
                  label="店铺联系人电话"
                  getFieldDecorator={getFieldDecorator}
                  requiredMessage="请输入店铺联系人电话"
                  initialValue={shopInfo.userTel}
                >
                  <Input placeholder="请输入" />
                </LZFormItem>
              </Col>
              <Col span={6} offset={2}>
                <LZFormItem
                  field="shop.createTime"
                  label="店铺创建时间"
                  getFieldDecorator={getFieldDecorator}
                  requiredMessage="请输入店铺创建时间"
                  initialValue={moment(shopInfo.createTime)}
                >
                  <DatePicker
                    className="w294"
                    format="YYYY-MM-DD HH:mm:ss"
                    placeholder={moment().format('YYYY-MM-DD')}
                    // showTime={{ defaultValue: moment('00:00:00', 'HH:mm:ss') }}
                  />
                </LZFormItem>
              </Col>
            </Row>
            <Row>
              <Col span={6}>
                <LZFormItem
                  field="shop.zfbCode"
                  label="支付宝账号"
                  getFieldDecorator={getFieldDecorator}
                  requiredMessage="请输入支付宝账号"
                  initialValue={shopInfo.zfbCode}
                >
                  <Input placeholder="请输入" />
                </LZFormItem>
              </Col>
              <Col span={6} offset={2}>
                <LZFormItem
                  field="shop.zfbName"
                  label="支付宝姓名"
                  getFieldDecorator={getFieldDecorator}
                  requiredMessage="请输入支付宝姓名"
                  initialValue={shopInfo.zfbName}
                >
                  <Input placeholder="请输入" />
                </LZFormItem>
              </Col>
              <Col span={6} offset={2}>
                <Form.Item label="店铺合同" required>
                  <div>
                    {shopInfo.shopContractLink ? (
                      <a href={shopInfo.shopContractLink} target="_blank">《租赁商家入驻合作协议》</a>
                    ) : (
                      <a onClick={() => router.push('/shop/info/Agreement')}>点击签约</a>
                    )}
                  </div>
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={6}>
                <LZFormItem
                  field="shop.logo"
                  label="店铺logo"
                  getFieldDecorator={getFieldDecorator}
                  requiredMessage="请上传店铺logo"
                  rules={[
                    {
                      validator: (rule, value, callback) => {
                        if (!value && !shopInfo.logo) {
                          callback('图片不能为空');
                        }
                        callback();
                      },
                    },
                  ]}
                  initialValue={shopInfo.logo}
                >
                  <Upload
                    accept="image/*"
                    listType="picture-card"
                    className="avatar-uploader"
                    headers={{
                      token: getToken(),
                    }}
                    showUploadList={false}
                    beforeUpload={this.beforeUpload}
                    onChange={info => this.handleUploadChange(info, 'logo')}
                    action={uploadUrl}
                  >
                    {shopInfo.logo ? (
                      <img src={shopInfo.logo} className="table-cell-img" />
                    ) : (
                      <Fragment>
                        <Icon type={uploading ? 'loading' : 'upload'} />
                        <div className="ant-upload-text">上传照片</div>
                      </Fragment>
                    )}
                  </Upload>
                </LZFormItem>
              </Col>
              <Col span={6} offset={2}>
                <LZFormItem
                  field="shop.description"
                  label={
                    <span>
                      店铺描述<span className="grey">(选填)</span>
                    </span>
                  }
                  required={false}
                  getFieldDecorator={getFieldDecorator}
                  initialValue={shopInfo.description}
                >
                  <Input.TextArea rows={4} placeholder="请输入描述" />
                </LZFormItem>
              </Col>
              <Col span={6} offset={2}>
                <LZFormItem
                  field="shop.shopAddress"
                  label={<span>店铺地址<span className="grey">(用于租赁协议，填写完整地址)</span></span>}
                  // help="用于租赁协议，请填写完整地址"
                  requiredMessage="请输入店铺地址，将用于租赁协议"
                  getFieldDecorator={getFieldDecorator}
                  initialValue={shopInfo.shopAddress}
                >
                  <Input placeholder="请输入" />
                </LZFormItem>
              </Col>
            </Row>
          </Form>
        </Card>
      </div>
    );
  };

  renderCompanyInfo = () => {
    const { form } = this.props;
    const { enterpriseInfo = {}, shopEnterpriseCertificates } = this.state;
    const { getFieldDecorator } = form;
    let { uploading, licenseSrcFileList } = this.state;
    const hasImgs = shopEnterpriseCertificates && shopEnterpriseCertificates.length > 1;
    const image0 = (hasImgs && shopEnterpriseCertificates.find(f => f.type === 0).image) || ''; // 营业执照
    const image3 = (hasImgs && shopEnterpriseCertificates.find(f => f.type === 3).image) || ''; // 身份证正面
    const image4 = (hasImgs && shopEnterpriseCertificates.find(f => f.type === 4).image) || ''; // 身份证背面
    licenseSrcFileList = [{ uid: 1, url: image0 }];
    const address =
      enterpriseInfo.licenseProvinceStr +
      enterpriseInfo.licenseCityStr +
      enterpriseInfo.licenseStreet;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
    };
    return (
      <div>
        <Card bordered={false} style={{ marginTop: 20 }}>
          <CustomCard title="企业信息" />
          <Form layout="vertical">
            <Row>
              <Col span={6}>
                <LZFormItem
                  field="shopEnterpriseInfos.name"
                  label="企业名称"
                  getFieldDecorator={getFieldDecorator}
                  requiredMessage="请输入企业名称"
                  initialValue={enterpriseInfo.name}
                >
                  <Input placeholder="请输入" />
                </LZFormItem>
              </Col>
              <Col span={6} offset={2}>
                <LZFormItem
                  field="shopEnterpriseInfos.registrationCapital"
                  label="企业注册资金"
                  getFieldDecorator={getFieldDecorator}
                  requiredMessage="请输入企业注册资金"
                  initialValue={enterpriseInfo.registrationCapital}
                >
                  <InputNumber placeholder="请输入" style={{width:"100%"}}/>
                </LZFormItem>
              </Col>
              <Col span={6} offset={2}>
                <LZFormItem
                  field="shopEnterpriseInfos.businessLicenseNo"
                  label="营业执照号"
                  getFieldDecorator={getFieldDecorator}
                  requiredMessage="请输入营业执照号"
                  initialValue={enterpriseInfo.businessLicenseNo}
                >
                  <Input placeholder="请输入" />
                </LZFormItem>
              </Col>
            </Row>
            <Row>
              <Col span={6}>
                <LZFormItem
                  field="shopEnterpriseInfos.licenseStreet"
                  label="营业执照地址"
                  getFieldDecorator={getFieldDecorator}
                  requiredMessage="请输入营业执照地址"
                  initialValue={enterpriseInfo.licenseStreet}
                >
                  <Input placeholder="请输入" />
                </LZFormItem>
              </Col>
              <Col span={6} offset={2}>
                <LZFormItem
                  field="shopEnterpriseInfos.businessScope"
                  label="营业执照经营范围"
                  getFieldDecorator={getFieldDecorator}
                  requiredMessage="请输入营业执照经营范围"
                  initialValue={enterpriseInfo.businessScope}
                >
                  <Input placeholder="请输入" />
                </LZFormItem>
              </Col>
              <Col span={6} offset={2}>
                <LZFormItem
                  field="shopEnterpriseInfos.license"
                  label="营业执照有效时间"
                  getFieldDecorator={getFieldDecorator}
                  requiredMessage="请输入营业执照有效时间"
                  initialValue={[
                    moment(enterpriseInfo.licenseStart),
                    moment(enterpriseInfo.licenseEnd),
                  ]}
                >
                  <RangePicker format="YYYY/MM/DD" placeholder={['开始时间', '结束时间']} />
                </LZFormItem>
              </Col>
            </Row>
            <Row>
              <Col span={6}>
                <FormItem
                  field="shopEnterpriseCertificates.licenseSrc"
                  label="营业执照照片"
                  required
                  requiredMessage="请上传营业执照照片"
                  getFieldDecorator={getFieldDecorator}
                >
                  {getFieldDecorator('licenseSrc', {
                    rules: [
                      {
                        validator: (rule, value, callback) => {
                          if (!value && !image0) {
                            callback('图片不能为空');
                          }
                          callback();
                        },
                      },
                    ],
                  })(
                    <Upload
                      accept="image/*"
                      listType="picture-card"
                      className="avatar-uploader"
                      headers={{
                        token: getToken(),
                      }}
                      showUploadList={false}
                      beforeUpload={this.beforeUpload}
                      onChange={info => this.handleUploadChange(info, 'licenseSrc')}
                      action={uploadUrl}
                    >
                      {image0 ? (
                        <img src={image0 || undefined} className="table-cell-img" />
                      ) : (
                        <Fragment>
                          <Icon type={uploading ? 'loading' : 'upload'} />
                          <div className="ant-upload-text">上传照片</div>
                        </Fragment>
                      )}
                    </Upload>,
                  )}
                </FormItem>
              </Col>
            </Row>
            <Divider />
            <Row>
              <Col span={6}>
                <LZFormItem
                  field="shopEnterpriseInfos.realname"
                  label="法人姓名"
                  getFieldDecorator={getFieldDecorator}
                  requiredMessage="请输入法人姓名"
                  initialValue={enterpriseInfo.realname}
                >
                  <Input placeholder="请输入" />
                </LZFormItem>
              </Col>
              <Col span={6} offset={2}>
                <LZFormItem
                  field="shopEnterpriseInfos.identity"
                  label="法人身份证号"
                  getFieldDecorator={getFieldDecorator}
                  requiredMessage="请输入法人身份证号"
                  initialValue={enterpriseInfo.identity}
                >
                  <Input placeholder="请输入" />
                </LZFormItem>
              </Col>
            </Row>
            <FormItem label="法人身份证信息">
              <Row className="df">
                <Col span={4}>
                  <FormItem
                    field="shopEnterpriseInfos.idcardBack"
                    getFieldDecorator={getFieldDecorator}
                    initialValue={enterpriseInfo.idcardBack}
                  >
                    {getFieldDecorator('shopEnterpriseInfos.idcardBack', {
                      rules: [
                        {
                          validator: (rule, value, callback) => {
                            if (!value && !image4) {
                              callback('图片不能为空');
                            }
                            callback();
                          },
                        },
                      ],
                    })(
                      <div>
                        <Upload
                          accept="image/*"
                          listType="picture-card"
                          className="avatar-uploader"
                          headers={{
                            token: getToken(),
                          }}
                          showUploadList={false}
                          beforeUpload={this.beforeUpload}
                          onChange={info => this.handleUploadChange(info, 'idcardBack')}
                          action={uploadUrl}
                        >
                          {image4 ? (
                            <img src={image4 || undefined} className="table-cell-img" />
                          ) : (
                            <Fragment>
                              <div className="ant-upload-text">上传照片</div>
                            </Fragment>
                          )}
                        </Upload>
                        <span className="id-card-upload-desc">身份证头像面</span>
                      </div>,
                    )}
                  </FormItem>
                </Col>
                <Col span={4}>
                  <FormItem
                    key="ss"
                    field="shopEnterpriseInfos.idcardDirect"
                    getFieldDecorator={getFieldDecorator}
                    initialValue={enterpriseInfo.idcardDirect}
                  >
                    {getFieldDecorator('shopEnterpriseInfos.idcardDirect', {
                      rules: [
                        {
                          validator: (rule, value, callback) => {
                            if (!value && !image3) {
                              callback('图片不能为空');
                            }
                            callback();
                          },
                        },
                      ],
                    })(
                      <div>
                        <Upload
                          accept="image/*"
                          listType="picture-card"
                          className="avatar-uploader"
                          headers={{
                            token: getToken(),
                          }}
                          showUploadList={false}
                          beforeUpload={this.beforeUpload}
                          onChange={info => this.handleUploadChange(info, 'idcardDirect')}
                          action={uploadUrl}
                        >
                          {image3 ? (
                            <img src={image3 || undefined} className="table-cell-img" />
                          ) : (
                            <Fragment>
                              <div className="ant-upload-text">上传照片</div>
                            </Fragment>
                          )}
                        </Upload>
                        <span className="id-card-upload-desc">身份证国徽面</span>
                      </div>,
                    )}
                  </FormItem>
                </Col>
              </Row>
            </FormItem>

            {/*<FormItem label="法人身份证号">*/}
            {/*  <Input value={forIdCard(enterpriseInfo.identity)} disabled />*/}
            {/*</FormItem>*/}
          </Form>
        </Card>
      </div>
    );
  };

  renderAudit = () => {
    const { form } = this.props;
    const { shopInfo } = this.state;
    const { getFieldDecorator } = form;
    return (
      <Card bordered={false} style={{ marginTop: 20 }}>
        <CustomCard
          title="审核确认"
          isExplain
          explain="仅允许商家或平台其中一方做订单电话审核，勿双重审核打扰用户，请谨慎选择"
        />
        <Form layout="inline">
          <LZFormItem
            label="是否需要平台进行电审"
            field="shop.isPhoneExamination"
            getFieldDecorator={getFieldDecorator}
            initialValue={shopInfo.isPhoneExamination}
          >
            <Radio.Group>
              <Radio value={1}>是</Radio>
              <Radio value={0}>否</Radio>
            </Radio.Group>
          </LZFormItem>
        </Form>
      </Card>
    );
  };

  renderRouterWillLeave() {
    const { originValues, isSubmit } = this.state;
    const values = this.props.form.getFieldsValue();
    let isPrompt = !isEqual(originValues, values);

    return <RouterWillLeave isPrompt={isPrompt} isSubmit={isSubmit} />;
  }

  render() {
    const { loading } = this.state;
    return (
      <PageHeaderWrapper title={false}>
        {this.renderRouterWillLeave()}
        <Spin spinning={loading}>
          {this.renderShopInfo()}
          {this.renderCompanyInfo()}
          {this.renderAudit()}
          <div className="mt-20 mb-20">
            <Button onClick={this.handleCancel}>取消</Button>
            <Button type="primary" htmlType="submit" onClick={this.handleSubmit}>
              保存并提交
            </Button>
          </div>
        </Spin>
      </PageHeaderWrapper>
    );
  }
}
export default EditShopInfo;
