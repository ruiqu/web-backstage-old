import request from '@/utils/request';

export const selectShopAdditionalServicesList = body =>
  request('/hzsx/business/shop/selectShopAdditionalServicesList', {
    method: 'GET',
    body,
  });

export const insertShopAdditionServices = body =>
  request('/hzsx/business/shop/insertShopAdditionServices', {
    method: 'POST',
    body,
  });

export const deletShopAdditionService = body =>
  request('/hzsx/business/shop/deletShopAdditionService', {
    method: 'GET',
    body,
  });

export const selectShopAdditionalServicesProudctList = body =>
  request('/hzsx/business/shop/selectShopAdditionalServicesProudctList', {
    method: 'GET',
    body,
  });

export const insertShopAdditionServicesProduct = body =>
  request('/hzsx/business/shop/insertShopAdditionServicesProduct', {
    method: 'POST',
    body,
  });

export const deletShopAdditionServicesProduct = body =>
  request('/hzsx/business/shop/deletShopAdditionServicesProduct', {
    method: 'GET',
    body,
  });

export const checkedAdditionalServicesProductList = body =>
  request('/hzsx/business/shop/checkedAdditionalServicesProductList', {
    method: 'GET',
    body,
  });
