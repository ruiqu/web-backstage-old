// import { routerRedux } from 'dva/router';
import { message } from 'antd';
import * as servicesApi from '../services';

export default {
  spacename: 'goodsList',
  state: {
    queryInfo: {
      auditState: null, // 0为正在审核中 1为审核不通过 2为审核通过
      categoryIds: undefined, // 分类id
      creatTime: [null, null], // 录入时间
      productId: null,
      productName: null,
      type: null, // 0回收站中的商品 1已上架售卖的商品 2放在仓库的商品
      pageNumber: 1,
      pageSize: 10,
    },
    total: 0,
    list: [],
    categories: [],
  },
  effects: {
    *selectBusinessPrdouct({ payload }, { call, put }) {
      const newInfo = { ...payload };
      newInfo.creatTime = [...payload.creatTime];
      if (!newInfo.creatTime[0] || !newInfo.creatTime[1]) {
        newInfo.creatTime = null;
      } else {
        newInfo.creatTime[0] = newInfo.creatTime[0].format('YYYY-MM-DD 00:00:00');
        newInfo.creatTime[1] = newInfo.creatTime[1].format('YYYY-MM-DD 23:59:59');
      }
      if (payload.categoryIds) {
        newInfo.categoryIds = payload.categoryIds.join(',');
      }
      const res = yield call(servicesApi.selectBusinessPrdouct, newInfo);
      if (res) {
        yield put({
          type: 'saveList',
          payload: { data: res.data, queryInfo: payload },
        });
      }
    },

    *findCategories(_, { call, put }) {
      const res = yield call(servicesApi.findCategories);
      if (res) {
        yield put({
          type: 'saveCategories',
          payload: res.data,
        });
      }
    },

    *busUpdateProductByRecycle({ payload, callback }, { call }) {
      const res = yield call(servicesApi.busUpdateProductByRecycle, payload);
      if (res && callback) {
        message.success(res.data);
        callback();
        // yield put({
        //   type: 'deleteRecord',
        //   payload,
        // });
      }
    },

    *busUpdateProductByRecycleDel({ payload, callback }, { call }) {
      const res = yield call(servicesApi.busUpdateProductByRecycleDel, payload);
      if (res && callback) {
        message.success(res.data);
        callback();
        // yield put({
        //   type: 'deleteRecord',
        //   payload,
        // });
      }
    },

    *busUpdateProductByRecycleRecover({ payload, callback }, { call }) {
      const res = yield call(servicesApi.busUpdateProductByRecycleRecover, payload);
      if (res && callback) {
        message.success(res.data);
        callback();
      }
    },

    *busUpdateProductByUpOrDown({ payload, callback }, { call }) {
      const work =
        payload.type === 1 ? servicesApi.busUpdateProductByDown : servicesApi.busUpdateProductByUp;
      const res = yield call(work, { id: payload.id });
      if (res && callback) {
        message.success(res.data);
        callback();
      }
    },
  },

  reducers: {
    saveList(state, { payload }) {
      return {
        ...state,
        list: payload.data.records,
        total: payload.data.total,
        queryInfo: {
          ...payload.queryInfo,
        },
      };
    },

    saveCategories(state, { payload }) {
      return {
        ...state,
        categories: payload,
      };
    },

    deleteRecord(state, { payload }) {
      const newList = [...state.list];
      const index = newList.findIndex(info => info.id === payload.id);
      if (index > -1) {
        newList.splice(index, 1);
      }
      return { ...state, list: newList };
    },
  },
};
