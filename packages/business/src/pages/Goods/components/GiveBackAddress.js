import React, { PureComponent } from 'react';
import { Table } from 'antd';

class GiveBackAddress extends PureComponent {
  render() {
    const { addressList, value, onChange } = this.props;
    const columns = [
      {
        title: '手机号码',
        dataIndex: 'telephone',
      },
      {
        title: '收货地址',
        dataIndex: 'street',
        render: (text, record) => `${record.provinceStr}${record.cityStr}${record.areaStr}${text}`,
      },
      {
        title: '收货人',
        dataIndex: 'name',
      },
      {
        title: '添加时间',
        dataIndex: 'createTime',
      },
    ];
    const rowSelection = {
      selectedRowKeys: value,
      onChange: selectedRowKeys => {
        onChange(selectedRowKeys);
      },
    };
    return (
      <Table
        rowKey="id"
        columns={columns}
        dataSource={addressList}
        rowSelection={rowSelection}
        pagination={false}
      />
    );
  }
}

export default GiveBackAddress;
