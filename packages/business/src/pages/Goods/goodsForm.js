import {
  Button,
  Card,
  Cascader,
  Form,
  Icon,
  Input,
  Modal,
  message,
  Radio,
  Select,
  Tooltip,
  Upload,
} from 'antd';
import OtherSpec from '@/pages/Goods/components/OtherSpec';
import React, { useEffect, useState, useRef } from 'react';
import NewProductSku from '@/pages/Goods/components/NewProductSku';
import { uploadUrl } from '@/config';
import BraftEditor from 'braft-editor';
import { bfUploadFn } from '@/utils/utils';
import GiveBackAddress from '@/pages/Goods/components/GiveBackAddress';
import Parameters from './components/Parameters';
import { LZFormItem } from '@/components/LZForm';
import { getToken } from '@/utils/localStorage';
import addressList from '@/assets/provinceAndCityAndArea.json';
import {isEqual} from "lodash";
import Prompt from "umi/prompt";
import router from "umi/router";
import RouterWillLeave from "@/components/RouterWillLeave";

const FormItem = Form.Item;
const RadioGroup = Radio.Group;

const largeFormItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 3 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 20 },
  },
};
const defaultDetail = {
  goodsDetail: { categoryId: '' },
  categories: [],
  compenRule: [],
  rentRule: [],
  backAddressList: [],
};

const GoodsForm = Form.create()(props => {
  let {
    form,
    categories,
    goodsDetail,
    changeSpecs,
    handleSubmit,
    onProductBuyout,
    cancel,
    addServiceTableData = [],
    giveBackList = [],
    deleteSpecs,
    changeSku,
    deleteSku,
  } = props;
  const { getFieldDecorator } = form;
  const {
    categoryId,
    name,
    salesVolume,
    oldNewDegree,
    labels,
    shopAdditionals,
    buyOutSupport,
    isZuwan,
    isOne,
    isbx,
    isWeek,
    returnRule,
    detail,
    images,
    province,
    city,
    freightType,
    returnfreightType,
    addIds,
    productSkuses = [],
    specs = [],
  } = goodsDetail;
  const [fileList, setFileList] = useState([]);
  const [parameters, setParameters] = useState([]);
  const [previewImage, setPreviewImage] = useState('');
  const [submitLoading, setSubmitLoading] = useState(false);
  const [isBuyout, setIsBuyout] = useState(buyOutSupport);
  const [isSubmit, setIsSubmit] = useState(false);
  const [originPayload, setOriginPayload] = useState({});
  categories.map(v=>{
    v.children && v.children.map(child=>{
      if (child.children && !child.children.length) {
        delete child.children
      }
    })
  })

  let _specs = {},
    colorSpecs = {};
  specs.forEach(v => {
    if (v.opeSpecId === 1) {
      colorSpecs = v;
    } else {
      _specs = v;
    }
  });
  const items = Array.from({ length: 100 }, a => useRef(null));

  useEffect(() => {
    setIsBuyout(goodsDetail.buyOutSupport);
  }, [goodsDetail.buyOutSupport]);
  
  useEffect(() => {
    if (detail) {
      props.form.setFieldsValue({
        detail: BraftEditor.createEditorState(detail),
      });
    }
    if (images && images.length) {
      setFileList(
        images.map(v => ({ uid: v.src, url: v.src, src: v.src, isMain: v.isMain || null })),
      );
    }
    setParameters(goodsDetail.parameters);
  }, [detail, images, goodsDetail]);
  
  useEffect(() => {
    // setIsBuyout(goodsDetail.buyOutSupport);
    const values = form.getFieldsValue()
    const payload = getPayload(values)
    setOriginPayload(payload)
  }, [goodsDetail, detail, images, buyOutSupport, parameters]);

 

  const findInitCategory = id => {
    const cates = [];
    const findCategoryParentId = category => {
      let result;
      const find = e => {
        if (e.value === category) result = e;
        if (e.children) {
          for (let i = 0; i < e.children.length; i += 1) {
            find(e.children[i]);
          }
        }
      };
      categories.forEach(e => {
        find(e);
      });
      return result;
    };
    let element = findCategoryParentId(id);
    if (element) {
      cates.push(element);
    }
    while (element && element.parentId !== 0) {
      element = findCategoryParentId(element.parentId);
      cates.push(element);
    }
    if (cates) {
      return cates
        .map(e => {
          return e.value;
        })
        .reverse();
    }
    return [];
  };

  // 限制用户上传大小与格式
  const beforeUpload = file => {
    const isJPG =
      file.type === 'image/jpeg' || file.type === 'image/png' || file.type === 'image/gif';
    if (!isJPG) {
      message.error('图片格式不正确');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('图片大于2MB');
    }
    return isJPG && isLt2M;
  };

  const checkMainImages = (_, value, callback) => {
    if (!value && fileList.length > 0) {
      callback('请上传主图');
      return;
    }
    callback();
  };
  const checkprice = (_, value, callback) => {
    if (!productSkuses.length) {
      callback('请填写租金');
      return;
    }
    callback();
  };

  const checkDetail = (_, value, callback) => {
    if (value.toHTML() === '<p></p>') {
      callback('请输入商品详情');
      return;
    }
    callback();
  };

  const validateAddress = (_rule, value, callback) => {
    if (!value || !value[0] || (!value[1] && province && city)) {
      callback('请选择发货地');
      return;
    }
    callback();
  };

  const changeBuyout = evt => {
    const value = evt.target.value;
    setIsBuyout(value);
  };

  const handleChangeSpecs = (value, opeSpecId) => {
    if (type === 'buyOutSupport') {
      onProductBuyout(value);
    }
    changeSpecs(value, opeSpecId);
  };

  const handleUploadImage = ({ file, fileList }) => {
    if (file.status === 'done') {
      const images = fileList.map((v, index) => {
        if (v.response) {
          const src = v.response.data;
          return { uid: index, src: src, url: src, isMain: v.isMain || null };
        }
        return v;
      });
      setFileList(images);
    }
    setFileList(fileList);
  };

  const handleChangeParam = val => {
    setParameters(val);
  };
  
  const getPayload = (fieldsValue) =>{
    const payload = fieldsValue;
    if (payload.detail) {
      payload.detail = payload.detail.toHTML();
    }
    payload.images = fileList.map((v, index) => {
      if (v.response) {
        const src = v.response.data;
        return { uid: index, src: src, url: src, isMain: v.isMain };
      }
      return v;
    });
    let addressObj = {};
    if (fieldsValue.address) {
      addressObj = {
        province: Number(fieldsValue.address[0]),
        city: Number(fieldsValue.address[1]),
      }
    }
    return {
      ...payload,
      ...addressObj,
      isBuyout: (fieldsValue.isBuyout || fieldsValue.isBuyout === 0) ? 0 : 1,
      parameters
    }
  }

  const handleSubmitForm = e => {
    e.preventDefault();
    form.validateFieldsAndScroll((err, fieldsValue) => {

      if (!err) {
        let hasError = false;
        items.map(item => {
          if (item.current) {
            item.current.validateFieldsAndScroll((err, fieldsValue) => {
              if (err) {
                hasError = true;
              }
            });
          }
        });
        if (hasError) {
          const errors = document.querySelector('.error-sale');
          if (errors) {
            message.error('存在' + errors.textContent + '的输入框');
            errors.scrollIntoView({ block: 'center' });
          }
        }
        if (!hasError) {
          //return console.log("fieldsValue,",fieldsValue)
          setSubmitLoading(true)
          setIsSubmit(true)
          const payload = getPayload(fieldsValue)
          handleSubmit(payload).finally(res => {
            setSubmitLoading(false)
          });
        }
      }
    });
  };

  let address = [];
  if (province && city) {
    address = [String(province), String(city)];
  }
  
  const handlePreview = ({ thumbUrl, url }) => {
    const imgUrl = thumbUrl ? thumbUrl : url
    setPreviewImage(imgUrl);
  };
  
  const handleCancel = () => {
   
    // cancel(isPrompt, isSubmit)
  }
  
  const renderRouterWillLeave=()=>{
    // const { isSubmit } = this.state
    const values = props.form.getFieldsValue()
    const payload = getPayload(values)
    let isPrompt = !isEqual(payload, originPayload)
    return (
      <RouterWillLeave isPrompt={isPrompt} isSubmit={isSubmit}  />
    )
  }
  
  return (
    <div className="goods-main">
      <Form {...largeFormItemLayout}>
        
        {
          renderRouterWillLeave()
        }
    
        <Card bordered={false}>
         {/*  <div>
          `商家须认真设置销售价格，租户在租期中，租完归还：到期买断价格=销售价格-累计支付的租金 提前买断价=[（官方价-已付租金+提前支付的租金+增值服务费）+官方价*月利率*使用期数]*1.06-提前支付的租金 当租期大于等于360天时，销售价需小于等于官方售价*1.24；当租期小于等于80天时，销售价需小于等于官方售价*1.12；租完即送： 当租期大于等于360天时，总租金需小于等于官方售价*1.24；当租期小于等于180天时，总租金需小于等于官方售价*1.12。
以上规则供参考，实际请商家严格按照最新的支付宝/政策法律法规规定执行！`
          </div> */}
          <div style={{"color":"red","padding": "0 10px",margin: "110 10px"}} >
          商家须认真设置销售价格，租户在租期中，租完归还：到期买断价格=销售价格-累计支付的租金 提前买断价=[（官方价-已付租金+提前支付的租金+增值服务费）+官方价*月利率*使用期数]*1.06-提前支付的租金 当租期&gt;=360天时，销售价需&lt;=官方售价*1.24；当租期&lt;=180天时，销售价需&lt;=官方售价*1.12；租完即送： 当租期&gt;=360天时，总租金需&lt;=官方售价*1.24；当租期&lt;=180天时，总租金需&lt;=官方售价*1.12。<br />以上规则供参考，实际请商家严格按照最新的支付宝/政策法律法规规定执行！
          </div>
          
          <LZFormItem
            label="商品类目"
            field="categoryIds"
            requiredMessage="请选择商品类目"
            initialValue={findInitCategory(categoryId)}
            getFieldDecorator={getFieldDecorator}
          >
            <Cascader placeholder="请选择类目" options={categories || []} />
          </LZFormItem>
          <LZFormItem
            label="商品名称"
            field="name"
            requiredMessage="请输入商品名称"
            getFieldDecorator={getFieldDecorator}
            initialValue={name}
          >
            <Input placeholder="请输入商品名称" />
          </LZFormItem>
          <LZFormItem
            label="销量"
            field="salesVolume"
            requiredMessage="请输入销量"
            getFieldDecorator={getFieldDecorator}
            initialValue={salesVolume}
          >
            <Input placeholder="请输入销量" />
          </LZFormItem>
          <LZFormItem
            label="产品新旧"
            field="oldNewDegree"
            requiredMessage="请输入产品新旧"
            getFieldDecorator={getFieldDecorator}
            initialValue={oldNewDegree}
          >
            <Radio.Group>
               {/* <Radio value={1}>先用后付</Radio>
              <Radio value={2}>大牌直降</Radio>
              <Radio value={3}>精选好货</Radio>
              <Radio value={4}>9.9包邮</Radio>
              <Radio value={5}>品质认证</Radio>
              <Radio value={6}>限时秒杀</Radio> */}
              <Radio value={1}>全新</Radio>
              <Radio value={2}>99新</Radio>
              <Radio value={3}>95新</Radio>
              <Radio value={4}>9成新</Radio>
              <Radio value={5}>8成新</Radio>
              <Radio value={6}>7成新</Radio>
            </Radio.Group>
          </LZFormItem>
          <LZFormItem
            label={
              <label className="">
                租赁标签
                <Tooltip title="租赁标签可多选">
                  <Icon type="info-circle" />
                </Tooltip>
              </label>
            }
            field="labels"
            getFieldDecorator={getFieldDecorator}
            initialValue={labels || undefined}
          >
            <Select mode="tags" placeholder="请输入自定义租赁标签" />
          </LZFormItem>
          <LZFormItem
            label="增值服务列表"
            field="shopAdditionals"
            getFieldDecorator={getFieldDecorator}
            initialValue={shopAdditionals || undefined}
          >
            <Select placeholder="请选择" mode="multiple">
              {addServiceTableData.map(value => {
                return (
                  <Select.Option value={value.id} label={value.name} key={value.id}>
                    {value.name}
                  </Select.Option>
                );
              })}
            </Select>
          </LZFormItem>
          <LZFormItem
            label="买断规则"
            field="buyOutSupport"
            requiredMessage="请选择类型"
            getFieldDecorator={getFieldDecorator}
            initialValue={buyOutSupport}
          >
            <Radio.Group onChange={changeBuyout}>
              <Radio value={1}>支持提前买断</Radio>
              <Radio value={2}>仅到期可买断</Radio>
              <Radio value={0}>不支持买断</Radio>
            </Radio.Group>
          </LZFormItem>
          <LZFormItem
            label="是否租完即送"
            field="isZuwan"
            requiredMessage="请选择租完规则"
            getFieldDecorator={getFieldDecorator}
            initialValue={isZuwan}
          >
            <Radio.Group>
              <Radio value={1}>否</Radio>
              <Radio value={2}>是</Radio>
            </Radio.Group>
          </LZFormItem>
          <LZFormItem
            label="是否首月一元"
            field="isOne"
            requiredMessage="是否首月一元"
            getFieldDecorator={getFieldDecorator}
            initialValue={isOne}
          >
            <Radio.Group>
              <Radio value={0}>否</Radio>
              <Radio value={1}>是</Radio>
            </Radio.Group>
          </LZFormItem>
          <LZFormItem
            label="是否必选"
            field="isbx"
            requiredMessage="是否必选"
            getFieldDecorator={getFieldDecorator}
            initialValue={isbx}
          >
            <Radio.Group>
              <Radio value={0}>否</Radio>
              <Radio value={1}>是</Radio>
            </Radio.Group>
          </LZFormItem>
          <LZFormItem
            label="付款周期"
            field="isWeek"
            requiredMessage="请选择是否周付"
            getFieldDecorator={getFieldDecorator}
            initialValue={isWeek}
          >
            <Radio.Group>
              <Radio value={0}>月</Radio>
              <Radio value={1}>7天</Radio>
              <Radio value={2}>10天</Radio>
              <Radio value={3}>15天</Radio>
              <Radio value={4}>1天</Radio>
              <Radio value={5}>5天</Radio>
            </Radio.Group>
          </LZFormItem>
          <LZFormItem
            label="归还规则"
            field="returnRule"
            requiredMessage="请选择归还规则"
            getFieldDecorator={getFieldDecorator}
            initialValue={returnRule}
          >
            <Radio.Group>
              <Radio value={1}>支持提前归还</Radio>
              <Radio value={2}>仅到期可归还</Radio>
            </Radio.Group>
          </LZFormItem>

          <FormItem {...largeFormItemLayout} label="颜色分类">
            {getFieldDecorator('colorSpecs', {
              initialValue: colorSpecs || [],
            })(<OtherSpec opeSpecId={1} key="color" onChange={handleChangeSpecs} />)}
          </FormItem>
          <FormItem {...largeFormItemLayout} label="规格">
            {getFieldDecorator('specs', {
              initialValue: _specs || [],
            })(<OtherSpec opeSpecId={2} key="spec" onChange={handleChangeSpecs} />)}
          </FormItem>
          <FormItem label="租金" required>
            {getFieldDecorator('price', {
              rules: [{ validator: checkprice }],
            })(
              <div>
                {productSkuses.map((p, index) => {
                  return (
                    <NewProductSku
                      getFieldDecorator={getFieldDecorator}
                      oldNewDegree={oldNewDegree}
                      ref={items[index]}
                      key={p.skuId}
                      isBuyout={isBuyout}
                      changeSku={changeSku}
                      deleteSpecs={deleteSpecs}
                      deleteSku={deleteSku}
                      specs={_specs}
                      colorSpecs={colorSpecs}
                      index={index}
                      productSkuses={p}
                    />
                  );
                })}
              </div>,
            )}
          </FormItem>
          <FormItem {...largeFormItemLayout} required label="商品图片">
            {getFieldDecorator('images', {
              initialValue: goodsDetail.images,
              rules: [{ validator: checkMainImages }],
            })(
              <div className="upload-desc">
                <Upload
                  accept="image/*"
                  action={uploadUrl}
                  listType="picture-card"
                  headers={{
                    token: getToken(),
                  }}
                  fileList={fileList}
                  onPreview={handlePreview} //预览
                  beforeUpload={beforeUpload}
                  onChange={handleUploadImage}
                >
                  <div>
                    <Icon type="upload" />
                    <div className="ant-upload-text">上传照片</div>
                  </div>
                </Upload>
                <div className="size-desc w294">
                  <span>● 尺寸为700x700px及以上正方形</span>
                  <br />
                  <span>● 不能出现商家logo、水印、电话、微信等联系方式</span>
                  <br />
                  <span>● 其中第一张图要求为纯白色底图</span>
                </div>
              </div>,
            )}
          </FormItem>
          <FormItem {...largeFormItemLayout} required label="商品详情">
            {getFieldDecorator('detail', {
              initialValue: BraftEditor.createEditorState(goodsDetail.detail),
              rules: [{ validator: checkDetail }],
            })(
              <BraftEditor
                style={{ border: '1px solid #d1d1d1', borderRadius: 5, backgroundColor: '#fff' }}
                contentStyle={{ height: 500, boxShadow: 'inset 0 1px 3px rgba(0,0,0,.1)' }}
                excludeControls={['emoji', 'clear', 'blockquote', 'code']}
                media={{ uploadFn: bfUploadFn }}
              />,
            )}
          </FormItem>
          <FormItem
            label="商品参数"
            field="labels"
            getFieldDecorator={getFieldDecorator}
            initialValue={labels || undefined}
          >
            <div>
              <Parameters parameters={parameters} onChange={handleChangeParam} />
            </div>
          </FormItem>
      
          <Form.Item label="发货地" required>
            {getFieldDecorator('address', {
              rules: [
                {
                  validator: validateAddress,
                },
              ],
              initialValue: address,
            })(<Cascader options={addressList} placeholder="请选择" />)}
          </Form.Item>
          <LZFormItem
            label="发货快递"
            field="freightType"
            requiredMessage="请选择发货快递方式"
            getFieldDecorator={getFieldDecorator}
            initialValue={freightType}
          >
            <Radio.Group>
              <Radio value={'FREE'}>商家包邮</Radio>
              <Radio value={'PAY'}>发货到付</Radio>
              <Radio value={'SELF'}>自提</Radio>
            </Radio.Group>
          </LZFormItem>
          <FormItem {...largeFormItemLayout} required label="是否上架">
            {getFieldDecorator('type', {
              initialValue: goodsDetail.type,
              rules: [{ type: 'number', required: true, message: '请选择是否上架' }],
            })(
              <RadioGroup>
                <Radio value={1}>上架</Radio>
                <Radio value={2}>放入仓库</Radio>
              </RadioGroup>,
            )}
          </FormItem>
        </Card>

        <LZFormItem
          label="归还快递"
          field="returnfreightType"
          requiredMessage="请选择归还快递方式"
          getFieldDecorator={getFieldDecorator}
          initialValue={returnfreightType}
        >
          <Radio.Group>
            <Radio value="PAY">商家承担</Radio>
            <Radio value="FREE">用户支付</Radio>
          </Radio.Group>
        </LZFormItem>
    
        <FormItem {...largeFormItemLayout} required label="归还地址">
          <Button onClick={()=>props.getGiveBackList()}>刷新地址列表</Button>
          {getFieldDecorator('addIds', {
            initialValue: addIds,
            rules: [{ type: 'array', required: true, message: '请选择归还地址' }],
          })(<GiveBackAddress addressList={giveBackList} />)}
        </FormItem>
    
        <Form.Item wrapperCol={{ offset: 3 }}>
          <Button onClick={cancel} className="w70">
            取消
          </Button>
          <Button type="primary" loading={submitLoading} onClick={handleSubmitForm} className="w-112">
            确定
          </Button>
        </Form.Item>
    
        <Modal
          visible={!!previewImage}
          footer={null}
          onCancel={() => setPreviewImage('')}
          width={600}
        >
          <img src={previewImage} alt="" style={{ width: '100%', paddingTop: 15 }} />
        </Modal>
      </Form>
    </div>
  );
});

export default GoodsForm;
