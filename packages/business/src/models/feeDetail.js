export const defaultPageSize = 10

export const tab1 = "zhima"

export const tab2 = "eqb"

export const status_yijiesuan = "SETTLED"

export const status_weijiesuan = "WAITING_SETTLEMENT"


export const defaultQuery = {
  times: [],
  status: status_yijiesuan,
  pageNumber: 1,
  pageSize: defaultPageSize,
}

export default {
  namespace: "feeDetailModel",

  state: {
    listApiParams: {
      [tab1]: defaultQuery,
      [tab2]: defaultQuery,
    }, // 分页接口的传参数据

    activeTab: tab1, // 处于焦点的tab
  },

  reducers: {
    /**
     * 修改某个tab的query数据
     * @param {*} state 
     * @param {*} param1 
     */
    muFeeDetailListApiParams(state, { payload: { tab, key, val }}) {
      return {
        ...state,
        listApiParams: {
          ...state.listApiParams,
          [tab]: {
            ...state.listApiParams[tab],
            [key]: val,
          }
        }
      }
    },

    /**
     * 修改某个tab的所有query数据
     */
    muFeeDetailListApiParamsComplete(state, { payload: { tab, val } }) {
      return {
        ...state,
        listApiParams: {
          ...state.listApiParams,
          [tab]: val,
        }
      }
    },

    /**
     * 修改tab
     * @param {*} state 
     * @param {*} param1 
     */
    mutFeeDetailActiveTab(state, { payload: activeTab }) {
      return {
        ...state,
        activeTab,
      }
    }
  }
}