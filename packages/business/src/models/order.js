import { router } from 'umi';
import * as shopApi from '@/services/order';
import { message } from 'antd';

export default {
  namespace: 'order',
  state: {
    allList: [], //全部列表
    allTotal: 1,
    closeList: [], //关闭列表
    closeTotal: 1,
    conditionList: [], //逾期列表列表
    conditionTotal: 1,
    mydb:{},
    BuyOutList: [], //买断订单列表
    BuyOutTotal: 1,
    ReletList: [], //查询续租订单
    ReletTotal: 1,
    AuditList: [], //电审
    AuditTotal: 1,
    PlateformList: [], //获取上架渠道集合
    userOrderInfoDto: {}, //订单信息
    shopInfoDto: {}, //商家信息
    orderAddressDto: {}, //收货人信息
    receiptExpressInfo: {}, //发货物流信息
    giveBackExpressInfo: {}, //归还物流信息
    productInfo: [],
    opeRemarkDtoPage: {}, //平台备注
    // OrderRemarkDataTotal: 1,
    businessRemarkDtoPage: {}, //商家备注
    // businessRemarkDtoTotal: 1,
    userOrderCashesDto: [], //账单信息
    orderByStagesDtoList: [], //分期信息
    orderBuyOutDto: [], //买断信息
    wlList: [], //物流信息
    originalOrderDto: [], //原定单详情
    userOrderBuyOutDto: [], //买断信息
    userOrderPurchaseList: [],
    userOrderPurchaseTotal: 1,
  },
  effects: {
    *queryOpeOrderByConditionList({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.queryOpeOrderByCondition, payload);

      yield put({
        type: 'allListData',
        payload: response,
      });
    },
    *queryPendingOrderClosureList({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.queryPendingOrderClosureList, payload);

      yield put({
        type: 'closeListData',
        payload: response,
      });
    },
    *closeUserOrderAndRefundPrice({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.closeUserOrderAndRefundPrice, payload);
      if (callback && typeof callback === 'function') {
        callback(response);
      }
    },
    *queryOpeOverDueOrdersByCondition({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.queryOpeOverDueOrdersByCondition, payload);

      yield put({
        type: 'conditionListData',
        payload: response,
      });
    },
    *queryWaitingGiveBackOrder({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.queryWaitingGiveBackOrder, payload);

      yield put({
        type: 'waitingGiveBackListData',
        payload: response,
      });
    },
    *queryOpeBuyOutOrdersByCondition({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.queryOpeBuyOutOrdersByCondition, payload);

      yield put({
        type: 'BuyOutListData',
        payload: response,
      });
    },
    *businessOrderStatisticsUsing({ payload, callback }, { put, call }) {
      const response = yield call(shopApi.businessOrderStatisticsUsing, payload);
      if (response.responseType === 'SUCCESS') {
        callback && callback(response.data);
      }
    },
    *expressInfo({ payload, callback }, { put, call }) {
      const response = yield call(shopApi.expressInfo, payload);
      if (response.responseType === 'SUCCESS') {
        callback && callback(response.data);
      }
    },
    
    *queryTelephoneAuditOrder({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.queryTelephoneAuditOrder, payload);

      yield put({
        type: 'AuditListData',
        payload: response,
      });
    },
    *queryReletOrderByCondition({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.queryReletOrderByCondition, payload);

      yield put({
        type: 'ReletListData',
        payload: response,
      });
    },
    *PlateformChannel({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.PlateformChannel, payload);
      yield put({
        type: 'PlateformData',
        payload: response,
      });
      if (callback && typeof callback === 'function') {
        callback(response);
      }
    },
    *orderRemark({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.orderRemark, payload);

      message.warning('备注成功～');
      if (callback && typeof callback === 'function') {
        callback(response);
      }
    },

    *purchaseOrder({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.purchaseOrder, payload);
      if (callback && typeof callback === 'function') {
        callback(response);
      }
    },
    *queryUserWeixinPage({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.queryUserWeixinPage, payload);
      if (callback && typeof callback === 'function') {
        callback(response);
      }
    },
    *queryOrderStagesDetail({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.queryOrderStagesDetail, payload);
      if (callback && typeof callback === 'function') {
        callback(response);
      }
    },

    *modifyUserWeixin({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.modifyUserWeixin, payload);
      if (callback && typeof callback === 'function') {
        callback(response);
      }
    },
    *deleteUserWeixin({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.deleteUserWeixin, payload);
      if (callback && typeof callback === 'function') {
        callback(response);
      }
    },
    *saveWeixinUserInfo({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.saveWeixinUserInfo, payload);
      if (callback && typeof callback === 'function') {
        callback(response);
      }
    },
    *userOrderPurchase({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.userOrderPurchase, payload);

      yield put({
        type: 'userOrderPurchaseData',
        payload: response,
      });
    },
    *telephoneAuditOrder({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.telephoneAuditOrder, payload);
      message.warning('备注成功～');
      if (callback && typeof callback === 'function') {
        callback(response);
      }
    },

    *queryOpeUserOrderDetail({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.queryOpeUserOrderDetail, payload);
      yield put({
        type: 'OpeUserData',
        payload: response,
      });
      if (callback && typeof callback === 'function') {
        callback(response);
      }
    },
    *queryOpeBuyOutOrderDetail({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.queryOpeBuyOutOrderDetail, payload);
      yield put({
        type: 'OpeUserData',
        payload: response,
      });
      if (callback && typeof callback === 'function') {
        callback(response);
      }
    },

    //续租
    *queryUserReletOrderDetail({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.queryUserReletOrderDetail, payload);
      yield put({
        type: 'OpeUserData',
        payload: response,
      });
      if (callback && typeof callback === 'function') {
        callback(response);
      }
    },
    *queryOrderRemark({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.queryOrderRemark, payload);
      console.log(payload.source);
      yield put({
        type: 'OrderRemarkData',
        payload: {
          response,
          source: payload.source,
        },
      });
    },
    *queryExpressInfo({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.queryExpressInfo, payload);
      yield put({
        type: 'queryInfoList',
        payload: response,
      });
    },
    *contractReport({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.contractReport, payload);
      window.open(response.data);
    },
    *selectDistrict({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.selectDistrict, payload);
      if (callback && typeof callback === 'function') {
        callback(response);
      }
    },
    *opeOrderAddressModify({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.opeOrderAddressModify, payload);
      if (callback && typeof callback === 'function') {
        callback(response);
      }
    },
    *exportOpeAllUserOrders({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.exportOpeAllUserOrders, payload);
      location.href = response.data;
      message.success('导出成功～');
    },
    /**
     * 导出租赁订单
     * @param {*} param0
     * @param {*} param1 
     */
    *exportRentOrder({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.default.ajaxForExportRentOrders, payload);
      callback && callback(response);
    },
    // 导出买断订单-适用于新版的导出中心
    *exportBuyOutOrder({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.default.ajaxForExportBuyOutOrders, payload);
      callback && callback(response);
    },
    // 导出未归还订单-适用于新版的导出中心
    *exportNotReturnOrder({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.default.ajaxForExportNotReturnOrders, payload);
      callback && callback(response);
    },
    // 导出逾期订单-适用于新版的导出中心
    *exportOverdueOrder({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.default.ajaxForExportOverdueOrders, payload);
      callback && callback(response);
    },
    // 导出中心-导出购买订单
    *exportPurchaseOrder({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.default.ajaxForExportPurchaseOrders, payload);
      callback && callback(response);
    },
    *userOrdersPurchase({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.userOrdersPurchase, payload);

      if (callback && typeof callback === 'function') {
        callback(response);
      }
    },
    *exportOpeBuyOutUserOrders({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.exportOpeBuyOutUserOrders, payload);
      location.href = response.data;
      message.success('导出成功～');
    },
    *exportOpeOverDueUserOrders({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.exportOpeOverDueUserOrders, payload);
      location.href = response.data;
      message.success('导出成功～');
    },
    *exportWaitingGiveBack({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.exportWaitingGiveBack, payload);
      location.href = response.data;
      message.success('导出成功～');
    },
    *exportReletOrders({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.exportReletOrders, payload);
      location.href = response.data;
      message.success('导出成功～');
    },
    *exportTelephoneAuditOrders({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.exportTelephoneAuditOrders, payload);
      location.href = response.data;
      message.success('导出成功～');
    },
    *exportCloseRefundOrders({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.exportCloseRefundOrders, payload);
      location.href = response.data;
      message.success('导出成功～');
    },
    // 获取信用详情
    *getCredit({ payload, callback }, { put, call }) {
      let res = yield call(shopApi.getSiriusReportRecord, payload);
      if (callback) callback(res);
    },
    // 获取百融
    *getbairon({ payload, callback }, { put, call }) {
      let res = yield call(shopApi.getbaironReportRecord, payload);
      if (callback) callback(res);
    },
    *purchaseOrderDelivery({ payload, callback }, { put, call }) {
      let res = yield call(shopApi.purchaseOrderDelivery, payload);
      if (callback) callback(res);
    },
    *purchaseOrderExport({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.purchaseOrderExport, payload);
      location.href = response.data;
      message.success('导出成功～');
    },
  },
  reducers: {
    allListData(state, { payload }) {
      return {
        ...state,
        allList: payload.data.records,
        allTotal: payload.data.total,
      };
    },
    closeListData(state, { payload }) {
      return {
        ...state,
        closeList: payload.data.records,
        closeTotal: payload.data.total,
      };
    },
    userOrderPurchaseData(state, { payload }) {
      return {
        ...state,
        userOrderPurchaseList: payload.data.records,
        userOrderPurchaseTotal: payload.data.total,
      };
    },
    conditionListData(state, { payload }) {
      return {
        ...state,
        conditionList: payload.data.page.records,
        conditionTotal: payload.data.page.total,
        mydb:payload.data,
      };
    },
    waitingGiveBackListData(state, { payload }) {
      return {
        ...state,
        waitingGiveBackList: payload.data.records,
        waitingGiveTotal: payload.data.total,
      };
    },
    AuditListData(state, { payload }) {
      return {
        ...state,
        AuditList: payload.data.records,
        AuditTotal: payload.data.total,
      };
    },
    BuyOutListData(state, { payload }) {
      return {
        ...state,
        BuyOutList: payload.data.records,
        BuyOutTotal: payload.data.total,
      };
    },
    ReletListData(state, { payload }) {
      return {
        ...state,
        ReletList: payload.data.records,
        ReletTotal: payload.data.total,
      };
    },
    PlateformData(state, { payload }) {
      return {
        ...state,
        PlateformList: payload.data,
      };
    },
    OpeUserData(state, { payload }) {
      console.log('payload:', payload);
      return {
        ...state,
        ...payload.data,
        productInfo: [payload.data.productInfo],
        // OrderRemarkDataList: payload.data.opeRemarkDtoPage.records,
        // OrderRemarkDataTotal: payload.data.opeRemarkDtoPage.total,
        // businessRemarkDtoList: payload.data.businessRemarkDtoPage.records,
        // businessRemarkDtoTotal: payload.data.businessRemarkDtoPage.total,
        userOrderCashesDto: [payload.data.userOrderCashesDto],
        orderByStagesDtoList: payload.data.orderByStagesDtoList,
        orderBuyOutDto: [payload.data.orderBuyOutDto],
        originalOrderDto: [payload.data.originalOrderDto],
        userOrderBuyOutDto: [payload.data.userOrderBuyOutDto],
      };
    },
    OrderRemarkData(state, { payload }) {
      const { source, response } = payload;
      if (source === '01') {
        return {
          ...state,
          opeRemarkDtoPage: response.data,
        };
      }
      return {
        ...state,
        businessRemarkDtoPage: response.data,
      };
    },
    queryInfoList(state, { payload }) {
      console.log("payload",payload)
      return {
        ...state,
        wlList: payload.data.list,
      };
    },
  },
};
