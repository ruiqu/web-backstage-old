module.exports = {
  navTheme: 'light',
  primaryColor: '#E83F40',
  layout: 'sidemenu',
  contentWidth: 'Fluid',
  fixedHeader: false,
  autoHideHeader: false,
  fixSiderbar: false,
  menu: {
    disableLocal: false,
  },
  title: '商家中心',
  pwa: true,
  iconfontUrl: '',
  collapse: true,
};
