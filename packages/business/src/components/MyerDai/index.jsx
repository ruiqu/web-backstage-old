import React, { Component } from 'react';

import "./index.scss"
import { Divider,Descriptions } from 'antd';
export default class erDai extends Component {
  //获取子组件页数

  render() {
    const { riskReport } = this.props;
    console.log("获取到的数据,c",riskReport)
    return (
      <div className="booleandataTotalContainer">
      <div className="booleanDataWrap">
        <div>

          <div className="highestGrade">
            <span style={{ fontWeight: "bold" }}>是否命中租赁黑名单：</span>
            <span>
              {rentalInformation.rent_history_black === 0
                ? "未命中"
                : "命中"}
            </span>
          </div>
          <Divider name="关联风险检测" />
          <div className="riskDetection">
            <Descriptions column={1}>
              <Descriptions.Item label="3个月身份证关联手机号数(个)">
                {riskDetection.m3_idcard_to_phone_time||1}
              </Descriptions.Item>
              <Descriptions.Item label="3个月手机号关联身份证数(个)">
                {riskDetection.m3_phone_to_idcard_time||1}
              </Descriptions.Item>
            </Descriptions>
          </div>

          <Divider name="法院风险信息" />
          <Courtrisklist dataValidation={uiObj} />
          <div className="footer">
            <p>报告使用说明:</p>
            <div className="text">
              1、本报告著作权属于布尔，未经我司正式文件许可，不得复制、摘录和转载。
            </div>
            <div className="text">
              2、本报告仅供使用者参考，布尔不承担据此报告产生的任何法律责任。
            </div>
          </div>
        </div>
      </div>
    </div>
    );
  }
}
