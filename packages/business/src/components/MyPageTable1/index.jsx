import React, { Component } from 'react';
import styles from './index.less';
import { Table, Spin, Modal, message, } from 'antd';
import request from '@/services/baseService';
import orderService from "@/services/order";
export default class MyTable1 extends Component {
  state = {
    selectOrderItem: [],
    userList: [],
    selectUserItem: {},
    userLoading: false,
    userVisible: false,
  }

  childFn = () => {
    console.log("我是子组件中的方法", this.state.selectOrderItem)
    if (this.state.selectOrderItem && this.state.selectOrderItem.length == 0) {
      message.warning('请先选择订单');
    } else {
      this.setState({
        userLoading: true
      }, () => {
        // 用户
        orderService.listBackstageUser1({sf:"cs"}).then(res => {
          if (res) {
            console.log("userList", res)
            this.setState({
              userList: res,
              userLoading: false,
              userVisible: true
            })
          } else {
            message.warning('无有效用户');
          }
        })
      })
    }
  }
  //获取子组件页数
  onChange = e => {
    e && this.props.onPage(e);
  };




  render() {
    let handleOk = () => {
      const { selectOrderItem, selectUserItem } = this.state
      console.log("点击了分配同意", selectOrderItem, selectUserItem)
      this.setState({
        userVisible: true
      }, () => {
        request("/hzsx/buckstageOrderClaim/fpcs",{
          orderId: selectOrderItem,
          buckstageUserId: selectUserItem.buckstageUserId,
          name: selectUserItem.name
        }).then(res => {
          // console.log("认领", res)
          message.success("认领成功")
          this.onChange();
          this.setState({
            userVisible: false
          })
        })
        /* orderService.allocationClaim({
          orderId: selectOrderItem,
          buckstageUserId: selectUserItem.buckstageUserId,
          name: selectUserItem.name
        }).then(res => {
          // console.log("认领", res)
          message.success("认领成功")
          this.initData();
          this.setState({
            userVisible: false
          })
        }) */
      })
    }
    let handleCancel = () => {
      console.log("取消分配同意")
      this.setState({
        userVisible: false
      })
    }

    let columnsUser = [
      {
        title: '成员账号',
        dataIndex: 'mobile',
      },
      {
        title: '姓名',
        dataIndex: 'name',
      },
      {
        title: '邮箱地址',
        dataIndex: 'email',
      },
      {
        title: '是否启用',
        dataIndex: 'status',
        render: text => text === 'VALID' ? '启用' : '未启用'
      },
    ]
    let { userList, userLoading, userVisible } = this.state
    let { columns, dataSource, paginationProps, scroll } = this.props;
    if (!paginationProps) paginationProps = {}
    paginationProps.pageSizeOptions = ["10", "20", "30", "50"]
    paginationProps.showSizeChanger = true
    return (
      <div>
        <Spin spinning={userLoading}>
          <Modal title="选择用户" visible={userVisible}
            onOk={handleOk}
            onCancel={handleCancel}
            width={1000}
          >
            <Table
              columns={columnsUser}
              dataSource={userList}
              pagination={false}
              rowKey={record => record.id}
              rowSelection={{
                type: 'radio',
                onChange: (selectedRowKeys, selectedRows) => {
                  console.log(selectedRowKeys)
                  console.log(selectedRows)
                  this.setState({
                    selectUserItem: {
                      buckstageUserId: selectedRows[0].id,
                      name: selectedRows[0].name,
                    }
                  })
                }
              }}
              scroll={{ y: 400 }}
            />
          </Modal>
        </Spin>
        <Table
          scroll={{ x: scroll }}
          columns={columns}
          dataSource={dataSource}
          pagination={paginationProps}
          onChange={this.onChange}
          rowKey={record => record.orderId}
          rowSelection={{
            type: 'checkbox',
            onChange: (selectedRowKeys, selectedRows) => {
              console.log(selectedRowKeys)
              console.log(selectedRows)
              //onselect
              //this.props.onselect(selectedRowKeys);
              this.setState({
                selectOrderItem: selectedRowKeys
              })
            }
          }}
        />
      </div>
    );
  }
}
