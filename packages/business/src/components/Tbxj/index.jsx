import React, { Component } from 'react';
import { Card, Descriptions, Drawer, Button, Tag, Table } from 'antd';
import AntTable from '@/components/AntTable';

//头部统计
export default class Tbxj extends Component {
  state = {
    stagesShow: false,
  }
  ckmq = () => {
    console.log("查看每期")
    this.setState({ stagesShow: true, });
  }
  onClose = () => {
    this.setState({ stagesShow: false, });
  }
  render() {

    const { cost, yj, payedRent, allRent, v到期金额, v逾期金额, v每期数据,
      v到期订单数, v总成本, v逾期订单数, v应收金额, v逾期订单金额, v总碎屏险金额 } = this.props.mydb;
    let cz = (yj + payedRent - cost + v总碎屏险金额).toFixed(2)
    let v逾期率 = v到期金额 ? ((v逾期金额 / v到期金额) * 100).toFixed(2) + "%" : 0

    let v订单逾期率 = v到期订单数 ? ((v逾期订单数 / v到期订单数) * 100).toFixed(2) + "%" : 0
    let column = [
      { title: '期数', dataIndex: 'name', },
      { title: '到期订单', dataIndex: 'v到期订单', },
      { title: '逾期订单', dataIndex: 'v逾期订单', },
      { title: '订单逾期率', dataIndex: 'v订单逾期率', },
      { title: '到期金额', dataIndex: 'v到期金额', },
      { title: '逾期金额', dataIndex: 'v逾期金额', },
      { title: '金额逾期率', dataIndex: 'v金额逾期率', },
    ]
    let db = []
    if (v每期数据) {
      for (let i in v每期数据) {
        //console.log(i, v每期数据[i])  //i是index，arr[i]是item
        let e = v每期数据[i]
        v每期数据[i].name = "第" + i + "期"
        v每期数据[i].v金额逾期率 = e.v到期金额 ? (e.v逾期金额 / e.v到期金额 * 100).toFixed(2) + "%" : 0
        v每期数据[i].v订单逾期率 = e.v到期订单 ? (e.v逾期订单 / e.v到期订单 * 100).toFixed(2) + "%" : 0
        db.push(v每期数据[i])
      }
      for (let i = 0; i < v每期数据.length; i++) {
      }
      console.log(v每期数据, typeof db, db.length)
    }

    return (
      <>
        <Drawer
          width={820}
          title='账单信息'
          placement="right"
          onClose={this.onClose}
          visible={this.state.stagesShow}>
         {/*  <AntTable columns={column}
            dataSource={db} /> */}
          <Table
            columns={column}
            dataSource={db}
            pagination={false}
            size="middle"
            tableLayout="fixed"
          />
        </Drawer>
        <Card bordered={false} className='ant-card antd-pro-pages-finance-capital-account-index-mb20'>
          <div style={{ fontSize: 20, marginBottom: 20 }}>
            <strong>统计信息</strong><img src='https://rich-rent.oss-cn-hangzhou.aliyuncs.com/icon/椭圆形 2.png'></img>
          </div>
          <Descriptions column={4}>
            <Descriptions.Item label="总租金">{allRent}</Descriptions.Item>
            <Descriptions.Item label="总成本">{v总成本}</Descriptions.Item>
            <Descriptions.Item label="已付租金">{payedRent}</Descriptions.Item>
            <Descriptions.Item label="总押金">{yj}</Descriptions.Item>
            <Descriptions.Item label="差额">{cz}</Descriptions.Item>
            <Descriptions.Item label="应收">{v应收金额 || 0}</Descriptions.Item>
            <Descriptions.Item label="到期金额">{v到期金额}</Descriptions.Item>
            <Descriptions.Item label="逾期金额">{v逾期金额}({v逾期订单金额})</Descriptions.Item>
            <Descriptions.Item label="逾期率">{v逾期率}</Descriptions.Item>
            <Descriptions.Item label="到期订单数">{v到期订单数}</Descriptions.Item>
            <Descriptions.Item label="逾期订单数">{v逾期订单数}</Descriptions.Item>
            <Descriptions.Item label="订单逾期率">{v订单逾期率}</Descriptions.Item>
            <Descriptions.Item label="总碎屏险金额">{v总碎屏险金额}</Descriptions.Item>
            <Descriptions.Item label="每期信息"> <Tag htmlType="button" onClick={this.ckmq}>
              查看每期
            </Tag></Descriptions.Item>
          </Descriptions>
        </Card>
      </>
    );
  }
}
