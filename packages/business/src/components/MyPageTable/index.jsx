import React, { Component } from 'react';
import styles from './index.less';
import { Table } from 'antd';
export default class MyTable extends Component {
  //获取子组件页数
  onChange = e => {
    e && this.props.onPage(e);
  };
  render() {
    let { columns, dataSource, paginationProps, scroll } = this.props;
    if(!paginationProps)paginationProps={}
    paginationProps.pageSizeOptions= ["10", "20", "30", "50"]
    paginationProps.showSizeChanger= true
    return (
      <div>
        <Table
          scroll={{x: scroll}}
          columns={columns}
          dataSource={dataSource}
          pagination={paginationProps}
          onChange={this.onChange}
        />
      </div>
    );
  }
}
