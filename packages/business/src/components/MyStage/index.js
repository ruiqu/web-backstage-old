import React, { Component, Fragment } from 'react';
import { Form, DatePicker, Input, Select, Button, Table,Drawer } from 'antd';
import request from '@/services/baseService';
import { onTableData, getParam, makeSub, renderOrderStatus } from '@/utils/utils.js';
import AntTable from '@/components/AntTable';
@Form.create()

export default class MyStage extends Component {
  //查询
  state = {
    isshow: false,
    db: {},
    attachment: {},
    sfz: {},
  }


  render() {
    const { orderByStagesDtoList } = this.props

    const columnsByStagesStute = {
      '1': '待支付',
      '2': '已支付',
      '3': '逾期已支付',
      '4': '逾期待支付',
      '5': '已取消',
      '6': '已结算',
      '7': '已退款,可用',
      '8': '部分还款',
    };
    
    const columnsByStages = [
      {
        title: '总期数',
        dataIndex: 'totalPeriods',
      },
      {
        title: '当前期数',
        dataIndex: 'currentPeriods',
      },
      {
        title: '租金',
        dataIndex: 'currentPeriodsRent',
      },
      {
        title: '已支付金额',
        render: e => {
          if(e.status==2||e.status==3){
            return e.currentPeriodsRent
          }else if(e.status==8)return e.beforePrice
          return 
        }
      },
      {
        title: '状态',
        render: e => <>
          <span>
            {columnsByStagesStute[e.status]}
          </span>
        </>
        ,
      },
      {
        title: '支付时间',
        dataIndex: 'repaymentDate',
      },
      {
        title: '账单到期时间',
        dataIndex: 'statementDate',
      },
    ];


    return (
      <>
       <AntTable
            isLimitHeightTable={true}
            columns={columnsByStages}
            dataSource={onTableData(orderByStagesDtoList)}
            paginationProps={{
              current: 1,
              pageSize: 1000,
              total: 1,
            }}
          />

      </>
    );
  }
}
