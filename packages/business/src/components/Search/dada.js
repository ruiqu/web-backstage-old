import React, {Component, Fragment} from 'react';
import moment from 'moment';
import { Form, DatePicker, Input, Select, Button } from 'antd';
import SearchItemEnum from "@/components/Search/SearchItemEnum";

const FormItem = Form.Item;
const { RangePicker } = DatePicker;
const { Option } = Select;
@Form.create()

export default class Search extends Component {
  //查询
  handleFilter = () => {
    this.props.form.validateFields((err, value) => {
      this.props.handleFilter(value);
    });
  };
  // 重置
  handleReset = async () => {
    this.props.form.resetFields();
    this.props.handleFilter({ type: '重置' });
  };
  // 导出
  exportData = () => {
    this.props.form.validateFields((err, value) => {
      this.props.exportData(value);
    });
  }
  
  renderLZSearch = (searchConfigs)=>{
    const {getFieldDecorator,} = this.props.form
    return (
      <Fragment>
        {
          searchConfigs.map(searchConfig=>{
            return SearchItemEnum[searchConfig](getFieldDecorator)
          })
        }
      </Fragment>
    )
  }
  
  selectForms = (source)=>{
    let searchConfigs = []
    switch (source) {
      case "佣金设置":
        searchConfigs = ['shopName','applicant', 'type', 'status']
        break;
      case "佣金审批":
        searchConfigs = ['shopName', 'type', 'status']
        break;
      case "常规订单":
        searchConfigs = ['productName','withBusinessSettleStatus', 'orderer', 'ordererPhone','orderId',
          'createTimeStart', 'orderStatus']
        break;
      case "买断订单":
        searchConfigs = ['productName','withBusinessSettleStatus', 'orderer', 'ordererPhone','orderId',
          'shopName', 'createTimeStart', 'orderStatus']
        break;
      case "部落配置":
        searchConfigs = ['tribeTitle', 'status']
        break;
      default:
        break;
    }
    return this.renderLZSearch(searchConfigs);
  }
  
  renderFooter = () => {
    const { needExport, needReset} = this.props
    return (
      <FormItem>
        <Button type="primary" type="primary" onClick={this.handleFilter}>
          查询
        </Button>
        {
          needReset?(
            <Button style={{ marginLeft: 8 }} onClick={this.handleReset}>
              重置
            </Button>
          ): null
        }
        {
          needExport ? (
            <Button style={{ marginLeft: 8 }} onClick={this.exportData} >
              导出
            </Button>
          ) : null
        }
      </FormItem>
    )
  }
  
  render() {
    const {source} = this.props
    const { getFieldDecorator, validateFields } = this.props.form;
    return (
      <Form layout="inline">
        {
          this.selectForms(source)
        }
        {/*{*/}
        {/*  this.props.newSoure && this.props.newSoure == '逾期' ? null : (*/}
        {/*    <FormItem>*/}
        {/*      {getFieldDecorator('status', {*/}
        {/*        initialValue: '全部',*/}
        {/*      })(*/}
        {/*        <Select style={{ width: 180 }}>*/}
        {/*          <Option value="全部">全部</Option>*/}
        {/*          {this.props.arrStatus.map(item => {*/}
        {/*            return (*/}
        {/*              <Option key={item} value={item.split('/')[0]}>*/}
        {/*                {item.split('/')[1]}*/}
        {/*              </Option>*/}
        {/*            );*/}
        {/*          })}*/}
        {/*        </Select>*/}
        {/*      )}*/}
        {/*    </FormItem>*/}
        {/*  )*/}
        {/*}*/}
        {/* 店铺审核，企业资质审核 */}
        {
          this.props.source && this.props.source === '企业资质' ? (
            <FormItem>
              {getFieldDecorator(
                'shopId',
                {}
              )(<Input placeholder="请输入店铺id" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        <FormItem
          style={{
            display:
              this.props.source && this.props.type && this.props.source === '企业资质'
                ? 'inline-block'
                : 'none',
          }}
        >
          {getFieldDecorator(
            'name',
            {}
          )(<Input placeholder="请输入公司名称" style={{ width: 200 }} allowClear />)}
        </FormItem>
        {
          this.props.source && this.props.source === '企业资质' ? (
            <FormItem>
              {getFieldDecorator(
                'shopName',
                {}
              )(<Input placeholder="请输入店铺名称" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        {/* 订单查询 */}
        {
          this.props.source && this.props.source === '订单' ? (
            <FormItem>
              {getFieldDecorator(
                'shopName',
                {}
              )(<Input placeholder="请输入店铺名称" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        {
          this.props.source && this.props.source === '订单' ? (
            <FormItem>
              {getFieldDecorator(
                'shopId',
                {}
              )(<Input placeholder="请输入店铺id" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        {
          this.props.source && this.props.source === '订单' ? (
            <FormItem
              // style={{
              //   display: this.props.source && this.props.source === '订单' ? 'inline-block' : 'none',
              // }}
            >
              {getFieldDecorator(
                'productName',
                {}
              )(<Input placeholder="请输入商品名称" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        {
          this.props.source && this.props.source === '订单' ? (
            <FormItem
              // style={{
              //   display: this.props.source && this.props.source === '订单' ? 'inline-block' : 'none',
              // }}
            >
              {getFieldDecorator(
                'productId',
                {}
              )(<Input placeholder="请输入productId" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        {
          this.props.source && this.props.source === '订单' ? (
            <FormItem
              // style={{
              //   display: this.props.source && this.props.source === '订单' ? 'inline-block' : 'none',
              // }}
            >
              {getFieldDecorator(
                'userName',
                {}
              )(<Input placeholder="请输入下单人姓名" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        {
          this.props.source && this.props.source === '订单' ? (
            <FormItem
              // style={{
              //   display: this.props.source && this.props.source === '订单' ? 'inline-block' : 'none',
              // }}
            >
              {getFieldDecorator(
                'telephone',
                {}
              )(<Input placeholder="请输入下单人电话" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        {
          this.props.source && this.props.source === '订单' ? (
            <FormItem
              // style={{
              //   display: this.props.source && this.props.source === '订单' ? 'inline-block' : 'none',
              // }}
            >
              {getFieldDecorator(
                'addressUserPhone',
                {}
              )(<Input placeholder="请输入收货人手机号码" style={{ width: 200 }} allowClear />)}
            </FormItem>

          ) : null
        }
        {
          this.props.source && this.props.source === '订单' ? (
            <FormItem
              // style={{
              //   display: this.props.source && this.props.source === '订单' ? 'inline-block' : 'none',
              // }}
            >
              {getFieldDecorator(
                'orderId',
                {}
              )(<Input placeholder="请输入订单ID" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        {
          this.props.source && this.props.oldOrders && this.props.source === '订单' ? (
            <FormItem
              // style={{
              //   display:
              //     this.props.source && this.props.oldOrders && this.props.source === '订单'
              //       ? 'inline-block'
              //       : 'none',
              // }}
            >
              {getFieldDecorator(
                'originalOrderId',
                {}
              )(<Input placeholder="请输入原订单ID" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        
        
        
        <FormItem
          style={{
            display: this.props.source && this.props.source === '订单' ? 'inline-block' : 'none',
          }}
        >
          {getFieldDecorator(
            'time',
            {}
          )(
            <DatePicker
              format="YYYY-MM-DD HH:mm:ss"
              placeholder="请选择起租时间"
              showTime={{ defaultValue: moment('00:00:00', 'HH:mm:ss') }}
            />
          )}
        </FormItem>
        <FormItem
          style={{
            display: this.props.source && this.props.source === '订单' ? 'inline-block' : 'none',
          }}
        >
          {getFieldDecorator(
            'createTimeStart',
            {}
          )(
            <RangePicker
              ranges={{
                Today: [moment(), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
              }}
              showTime
              format="YYYY/MM/DD HH:mm:ss"
              placeholder={['开始下单日期', '结束下单日期']}
              // onChange={onChange}
            />
          )}
        </FormItem>
        {
          this.props.source && this.props.source === '分期订单' ? (
            <FormItem>
              {getFieldDecorator(
                'orderId',
                {}
              )(<Input placeholder="请输入订单ID" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        {
          this.props.source && this.props.source === '分期订单' ? (
            <FormItem
              style={{
                display:
                  this.props.source && this.props.source === '分期订单' ? 'inline-block' : 'none',
              }}
            >
              {getFieldDecorator(
                'productId',
                {}
              )(<Input placeholder="请输入productId" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        {
          this.props.source && this.props.source === '分期订单' ? (
            <FormItem
              style={{
                display:
                  this.props.source && this.props.source === '分期订单' ? 'inline-block' : 'none',
              }}
            >
              {getFieldDecorator(
                'telephone',
                {}
              )(<Input placeholder="请输入下单人电话" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        {
          this.props.source && this.props.close && this.props.source === '分期订单' ? (
            <FormItem>
              {getFieldDecorator(
                'addressUserName',
                {}
              )(<Input placeholder="请输入收货人姓名" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        {
          this.props.source && this.props.close && this.props.source === '分期订单' ? (
            <FormItem>
              {getFieldDecorator(
                'userName',
                {}
              )(<Input placeholder="请输入下单人姓名" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        <FormItem
          style={{
            display:
              this.props.source && this.props.source === '分期订单' ? 'inline-block' : 'none',
          }}
        >
          {getFieldDecorator(
            'statementDate',
            {}
          )(
            <RangePicker
              ranges={{
                Today: [moment(), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
              }}
              showTime
              format="YYYY/MM/DD HH:mm:ss"
              placeholder={['开始下单日期', '结束下单日期']}
              // onChange={onChange}
            />
          )}
        </FormItem>
        {/* 订单退款 */}
        {
          this.props.source && this.props.source === '订单退款' ? (
            <FormItem>
              {getFieldDecorator(
                'shopName',
                {}
              )(<Input placeholder="请输入店铺名称" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        {
          this.props.source && this.props.source === '订单退款' ? (
            <FormItem>
              {getFieldDecorator(
                'shopId',
                {}
              )(<Input placeholder="请输入店铺ID" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        {
          this.props.source && this.props.source === '订单退款' ? (
            <FormItem>
              {getFieldDecorator(
                'refundNumber',
                {
                  initialValue: this.props.refundNumber ? this.props.refundNumber : null,
                }
              )(<Input placeholder="请输入退款编号" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        {
          this.props.source && this.props.source === '订单退款' ? (
            <FormItem>
              {getFieldDecorator(
                'orderId',
                {}
              )(<Input placeholder="请输入原订单号" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        {
          this.props.source && this.props.source === '订单退款' ? (
            <FormItem>
              {getFieldDecorator(
                'userName',
                {}
              )(<Input placeholder="请输入下单人姓名" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        {
          this.props.source && this.props.source === '订单退款' ? (
            <FormItem>
              {getFieldDecorator(
                'telephone',
                {}
              )(<Input placeholder="请输入下单人手机号" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        {
          this.props.source && this.props.source === '订单退款' ? (
            <FormItem>
              {getFieldDecorator(
                'addressUserName',
                {}
              )(<Input placeholder="请输入收货人姓名" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        {
          this.props.source && this.props.source === '订单退款' ? (
            <FormItem>
              {getFieldDecorator(
                'addressUserPhone',
                {}
              )(<Input placeholder="请输入收货人手机号" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }

        {/* 商品审核 */}
        <FormItem
          style={{
            display:
              this.props.source && this.props.source === '商品审核' ? 'inline-block' : 'none',
          }}
        >
          {getFieldDecorator(
            'type',
            {}
          )(
            <Select placeholder="是否上架" style={{ width: 200 }} allowClear={true}>
              <Option value="1">已上架</Option>
              <Option value="2">已下架</Option>
              <Option value="0">回收站</Option>
            </Select>
          )}
        </FormItem>
        <FormItem
          style={{
            display:
              this.props.source && this.props.source === '商品审核' ? 'inline-block' : 'none',
          }}
        >
          {getFieldDecorator(
            'goodStatus',
            {}
          )(
            <Select placeholder="是否有效" style={{ width: 200 }} allowClear={true}>
              <Option value="1">有效</Option>
              <Option value="0">失效</Option>
            </Select>
          )}
        </FormItem>
        {
          this.props.source && this.props.source === '商品审核' ? (
            <FormItem
              // style={{
              //   display:
              //     this.props.source && this.props.source === '商品审核' ? 'inline-block' : 'none',
              // }}
            >
              {getFieldDecorator(
                'productName',
                {}
              )(<Input placeholder="请输入产品名称" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        {
          this.props.source && this.props.source === '商品审核' ? (
            <FormItem
              // style={{
              //   display:
              //     this.props.source && this.props.source === '商品审核' ? 'inline-block' : 'none',
              // }}
            >
              {getFieldDecorator(
                'productId',
                {}
              )(<Input placeholder="请输入productId" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        {
          this.props.source && this.props.source === '商品审核' ? (
            <FormItem
              // style={{
              //   display:
              //     this.props.source && this.props.source === '商品审核' ? 'inline-block' : 'none',
              // }}
            >
              {getFieldDecorator(
                'shopId',
                {}
              )(<Input placeholder="请输入店铺ID" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        {
          this.props.source && this.props.source === '商品审核' ? (
            <FormItem
              style={{
                display:
                  this.props.source && this.props.source === '商品审核' ? 'inline-block' : 'none',
              }}
            >
              {getFieldDecorator(
                'shopName',
                {}
              )(<Input placeholder="请输入店铺名称" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        <FormItem
          style={{
            display:
              this.props.source && this.props.source === '商品审核' ? 'inline-block' : 'none',
          }}
        >
          {getFieldDecorator(
            'creatTime',
            {}
          )(
            <RangePicker
              ranges={{
                Today: [moment(), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
              }}
              showTime
              format="YYYY/MM/DD HH:mm:ss"
              placeholder={['创建开始日期', '创建结束日期']}
              // onChange={onChange}
            />
          )}
        </FormItem>

        {/* 押金订单 */}
        {
          this.props.source && this.props.source === '押金订单' ? (
            <FormItem>
              {getFieldDecorator(
                'outTradeNo',
                {}
              )(<Input placeholder="请输入商户订单号" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        {
          this.props.source && this.props.source === '押金订单' ? (
            <FormItem>
              {getFieldDecorator(
                'tradeNo',
                {}
              )(<Input placeholder="请输入支付宝订单号" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        {
          this.props.source && this.props.source === '押金订单' ? (
            <FormItem>
              {getFieldDecorator(
                'telephone',
                {}
              )(<Input placeholder="请输入用户手机号" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }
        {
          this.props.source && this.props.source === '押金订单' ? (
            <FormItem>
              {getFieldDecorator(
                'userName',
                {}
              )(<Input placeholder="请输入用户姓名" style={{ width: 200 }} allowClear />)}
            </FormItem>
          ) : null
        }

        {/* 店铺审核和企业资质审核 */}
        <FormItem
          style={{
            display:
              this.props.source && this.props.source === '企业资质' ? 'inline-block' : 'none',
          }}
        >
          {getFieldDecorator(
            'createDate',
            {}
          )(
            <RangePicker
              ranges={{
                Today: [moment(), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
              }}
              showTime
              format="YYYY/MM/DD HH:mm:ss"
              // onChange={onChange}
            />
          )}
        </FormItem>
        {
          this.props.channelList ? (
            <FormItem>
              {getFieldDecorator('channelId', {
                initialValue: '',
              })(
                <Select style={{ width: 180 }}>
                  <Option value="">全部渠道来源</Option>
                  {this.props.channelList.map(item => {
                    return (
                      <Option key={item.channelId} value={item.channelId}>
                        {item.channelName}
                      </Option>
                    );
                  })}
                </Select>
              )}
            </FormItem>
          ) : null
        }
        {
          this.renderFooter()
        }
      </Form>
    );
  }
}
