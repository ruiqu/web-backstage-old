import React, { Component, Fragment } from 'react';
import { Form, DatePicker, Input, Select, Button, message, Table } from 'antd';
import request from '@/services/baseService';
import { onTableData, getParam, makeSub, renderOrderStatus } from '@/utils/utils.js';
@Form.create()
//全景雷达
export default class Xytx extends Component {
  //查询
  state = {
    riskType: "12",
    isshow: false,
    isload: false,
    v历史: null,
    v选择的日期: null,
    db: {},
    data: {},
    sfz: {},
  }


  viewCredit() {
    const { sfz } = this.props
    let req = {
      "uid": sfz.uid,
      "orderId": sfz.orderId,
      "phone": sfz.telephone,
      "riskType": this.state.riskType,
      "idCardNo": sfz.idCard,
      "userName": sfz.realName
    }

    request(`/hzsx/business/order/queryXyTzReportByUid`, req).then(res => {
      let db = JSON.parse(res.siriusRiskReport)
      console.log("ressss,", db)
      this.setState({ db, isshow: true })
    })

  }

  f获取历史 = () => {
    const { sfz } = this.props
    if (this.state.isload) return
    this.setState({ isload: true });
    let db = {}
    db.uid = sfz.uid
    db.phone = sfz.telephone
    db.idCardNo = sfz.idCard
    db.riskType = this.state.riskType
    db.userName = sfz.realName
    //发起请求，直接请求获取列表
    request("/hzsx/business/order/queryLsReportByUid", db).then(res => {
      console.log("历史的数据是", res)
      //this.f布尔历史数据处理(res)
      let v历史 = {}
      for (let i = 0; i < res.length; i++) {
        const ele = res[i];
        let k = ele.createTime.substring(0, 10);
        if (!v历史[k]) v历史[k] = {}
        const str2 = ele.reportResult.replaceAll('\"', '"')
        v历史[k] = JSON.parse(str2)
        //判断数据有没有，有的话就添加，没有就新增 
      }
      Object.keys(v历史).map((item, i) => {
        console.log("历史是", item, i)
      })
      console.log("的数据是db", v历史)
      let arr = Object.keys(v历史);
      if (arr.length == 1) {
        message.success("暂时没有历史数据")
        this.setState({ isload: false });
      } else {
        this.setState({ isload: false, v选择的日期: -1, v历史 });
      }
      //判断一下，把第一个给去掉，然后显示另外 的俩个
    })
  }

  xran = () => {
    let { v历史, v选择的日期 } = this.state
    console.log("v历史", v历史)
    //如果没有，那么就显示
    if (!v选择的日期) {
      return <><Button onClick={() => this.f获取历史()} type="primary">查询历史</Button></>
    } else {
      return (<>
        {Object.keys(v历史).map(option => {
          return (
            <Button disabled={option==v选择的日期} onClick={() => {
              this.setState({ v选择的日期: option })
            }} type="primary">{option}</Button>
          );
        })}
      </>
      )
    }
  }

  render() {
    const { db, isshow, sfz, data, v选择的日期, v历史 } = this.state
    let v数据 = db
    console.log("判断是否选择了日期", v数据, v历史)
    console.log("判断是否选择了日期", v数据)
    if(v历史)console.log("taya ,",Object.keys(v历史).length > 0,Object.keys(v历史).length )
    if (v选择的日期 && v历史 && Object.keys(v历史).length > 0) {//如果选择了日期，那么重置数据
      v数据 = v历史[v选择的日期]
      console.log("选择了数据", v数据, v选择的日期)
    }
    let v报告详情1 = [
      { title: '申请准入分', dataIndex: 'A22160001', width: 150 },
      { title: '申请准入置信度', dataIndex: 'A22160002', width: 150 },
      { title: '申请命中机构数', dataIndex: 'A22160003', width: 150 },
      { title: '申请命中消金类机构数', dataIndex: 'A22160004', width: 150 },
      { title: '申请命中网络贷款类机构数', dataIndex: 'A22160005', width: 150 },
    ]
    let v报告详情2 = [
      { title: '机构总查询次数', dataIndex: 'A22160006', width: 150 },
      { title: '最近一次查询时间', dataIndex: 'A22160007', width: 150 },
      { title: '近1个月机构总查询笔数', dataIndex: 'A22160008', width: 150 },
      { title: '近3个月机构总查询笔数', dataIndex: 'A22160009', width: 150 },
      { title: '近6个月机构总查询笔数', dataIndex: 'A22160010', width: 150 },
    ]
    let v行为雷达报告 = [
      { title: '贷款行为分', dataIndex: 'B22170001', width: 150 },
      { title: '近1个月贷款笔数', dataIndex: 'B22170002', width: 150 },
      { title: '近3个月贷款笔数', dataIndex: 'B22170003', width: 150 },
      { title: '近6个月贷款笔数', dataIndex: 'B22170004', width: 150 },
      { title: '近12个月贷款笔数', dataIndex: 'B22170005', width: 150 },
      { title: '近24个月贷款笔数', dataIndex: 'B22170006', width: 150 },
    ]
    let v行为雷达报告1 = [
      { title: '近1个月贷款总金额', dataIndex: 'B22170007', width: 150 },
      { title: '近3个月贷款总金额', dataIndex: 'B22170008', width: 150 },
      { title: '近6个月贷款总金额', dataIndex: 'B22170009', width: 150 },
      { title: '近12个月贷款总金额', dataIndex: 'B22170010', width: 150 },
      { title: '近24个月贷款总金额', dataIndex: 'B22170011', width: 150 },
      { title: '近12个月贷款金额在1k及以下的笔数', dataIndex: 'B22170012', width: 150 },
    ]
    let v行为雷达报告2 = [
      { title: '近12个月贷款金额在1k-3k的笔数', dataIndex: 'B22170013', width: 150 },
      { title: '近12个月贷款金额在3k-10k的笔数', dataIndex: 'B22170014', width: 150 },
      { title: '近12个月贷款金额在1w以上的笔数', dataIndex: 'B22170015', width: 150 },
      { title: '近1个月贷款机构数', dataIndex: 'B22170016', width: 150 },
      { title: '近3个月贷款机构数', dataIndex: 'B22170017', width: 150 },
      { title: '近6个月贷款机构数', dataIndex: 'B22170018', width: 150 },
    ]
    let v行为雷达报告3 = [
      { title: '近12个月贷款机构数', dataIndex: 'B22170019', width: 150 },
      { title: '近24个月贷款机构数', dataIndex: 'B22170020', width: 150 },
      { title: '近12个月消金类贷款机构数', dataIndex: 'B22170021', width: 150 },
      { title: '近24个月消金类贷款机构数', dataIndex: 'B22170022', width: 150 },
      { title: '近12个月网贷类贷款机构数', dataIndex: 'B22170023', width: 150 },
      { title: '近24个月网贷类贷款机构数', dataIndex: 'B22170024', width: 150 },
    ]
    let v行为雷达报告4 = [
      { title: '近6个月M0+逾期贷款笔数', dataIndex: 'B22170025', width: 150 },
      { title: '近12个月M0+逾期贷款笔数', dataIndex: 'B22170026', width: 150 },
      { title: '近24个月M0+逾期贷款笔数', dataIndex: 'B22170027', width: 150 },
      { title: '近6个月M1+逾期贷款笔数', dataIndex: 'B22170028', width: 150 },
      { title: '近12个月M1+逾期贷款笔数', dataIndex: 'B22170029', width: 150 },
      { title: '近24个月M1+逾期贷款笔数', dataIndex: 'B22170030', width: 150 },
    ]
    let v行为雷达报告5 = [
      { title: '近6个月累计逾期金额', dataIndex: 'B22170031', width: 150 },
      { title: '近12个月累计逾期金额', dataIndex: 'B22170032', width: 150 },
      { title: '近24个月累计逾期金额', dataIndex: 'B22170033', width: 150 },
      { title: '正常还款订单数占贷款总订单数比例', dataIndex: 'B22170034', width: 150 },
      { title: '近1个月失败扣款笔数', dataIndex: 'B22170025', width: 150 },
      { title: '近3个月失败扣款笔数', dataIndex: 'B22170036', width: 150 },
    ]
    let v行为雷达报告6 = [
      { title: '近6个月失败扣款笔数', dataIndex: 'B22170037', width: 150 },
      { title: '近12个月失败扣款笔数', dataIndex: 'B22170038', width: 150 },
      { title: '近24个月失败扣款笔数', dataIndex: 'B22170039', width: 150 },
      { title: '近1个月履约贷款总金额', dataIndex: 'B22170040', width: 150 },
      { title: '近3个月履约贷款总金额', dataIndex: 'B22170041', width: 150 },
      { title: '近6个月履约贷款总金额', dataIndex: 'B22170042', width: 150 },
    ]
    let v行为雷达报告7 = [
      { title: '近12个月履约贷款总金额', dataIndex: 'B22170043', width: 150 },
      { title: '近24个月履约贷款总金额', dataIndex: 'B22170044', width: 150 },
      { title: '近1个月履约贷款次数', dataIndex: 'B22170045', width: 150 },
      { title: '近3个月履约贷款次数', dataIndex: 'B22170046', width: 150 },
      { title: '近6个月履约贷款次数', dataIndex: 'B22170047', width: 150 },
      { title: '近12个月履约贷款次数', dataIndex: 'B22170048', width: 150 },
    ]
    let v行为雷达报告8 = [
      { title: '近24个月履约贷款次数', dataIndex: 'B22170049', width: 150 },
      { title: '最近一次履约距今天数', dataIndex: 'B22170050', width: 150 },
      { title: '贷款行为置信度', dataIndex: 'B22170051', width: 150 },
      { title: '贷款已结清订单数', dataIndex: 'B22170052', width: 150 },
      { title: '信用贷款时长', dataIndex: 'B22170053', width: 150 },
      { title: '最近一次贷款放款时间', dataIndex: 'B22170054', width: 150 },
    ]

    let v信用详情 = [
      { title: '网贷授信额度', dataIndex: 'C22180001', width: 150 },
      { title: '网贷额度置信度', dataIndex: 'C22180002', width: 150 },
      { title: '网络贷款类机构数', dataIndex: 'C22180003', width: 150 },
      { title: '网络贷款类产品数', dataIndex: 'C22180004', width: 150 },
      { title: '网络贷款机构最大授信额度', dataIndex: 'C22180005', width: 150 },
      { title: '网络贷款机构平均授信额度', dataIndex: 'C22180006', width: 150 },
    ]
    let v信用详情1 = [
      { title: '消金贷款类机构数', dataIndex: 'C22180007', width: 150 },
      { title: '消金贷款类产品数', dataIndex: 'C22180008', width: 150 },
      { title: '消金贷款类机构最大授信额度', dataIndex: 'C22180009', width: 150 },
      { title: '消金贷款类机构平均授信额度', dataIndex: 'C22180010', width: 150 },
      { title: '消金建议授信额度', dataIndex: 'C22180011', width: 150 },
      { title: '消金额度置信度', dataIndex: 'C22180012', width: 150 },
    ]
    const f获取参数 = (v数据) => {
      let detail = []
      if (v数据) detail.push(v数据)
      return detail
    }
    return (
      <>
        {
          !isshow && (
            <Button onClick={() => this.viewCredit()} type="primary">点击查询</Button>
          )
        }

        {
          isshow && (

            <>
              {this.xran()}
              <p style={{ fontSize: 20 }}>
                申请雷达报告详情:
              </p>
              <Table
                bordered
                columns={v报告详情1}
                dataSource={f获取参数(v数据.data.apply_report_detail)}
                pagination={false}
              />
              <Table
                bordered
                columns={v报告详情2}
                dataSource={f获取参数(v数据.data.apply_report_detail)}
                pagination={false}
              />
              <p></p>
              <p style={{ fontSize: 20 }}>
                行为雷达报告详情:
              </p>
              <Table
                bordered
                columns={v行为雷达报告}
                dataSource={f获取参数(v数据.data.behavior_report_detail)}
                pagination={false}
              />
              <Table
                bordered
                columns={v行为雷达报告1}
                dataSource={f获取参数(v数据.data.behavior_report_detail)}
                pagination={false}
              />
              <Table
                bordered
                columns={v行为雷达报告2}
                dataSource={f获取参数(v数据.data.behavior_report_detail)}
                pagination={false}
              />
              <Table
                bordered
                columns={v行为雷达报告1}
                dataSource={f获取参数(v数据.data.behavior_report_detail)}
                pagination={false}
              />
              <Table
                bordered
                columns={v行为雷达报告3}
                dataSource={f获取参数(v数据.data.behavior_report_detail)}
                pagination={false}
              />
              <Table
                bordered
                columns={v行为雷达报告4}
                dataSource={f获取参数(v数据.data.behavior_report_detail)}
                pagination={false}
              />
              <Table
                bordered
                columns={v行为雷达报告5}
                dataSource={f获取参数(v数据.data.behavior_report_detail)}
                pagination={false}
              />
              <Table
                bordered
                columns={v行为雷达报告6}
                dataSource={f获取参数(v数据.data.behavior_report_detail)}
                pagination={false}
              />
              <Table
                bordered
                columns={v行为雷达报告7}
                dataSource={f获取参数(v数据.data.behavior_report_detail)}
                pagination={false}
              />
              <Table
                bordered
                columns={v行为雷达报告8}
                dataSource={f获取参数(v数据.data.behavior_report_detail)}
                pagination={false}
              />

              <p style={{ fontSize: 20 }}>
                信用详情:
              </p>
              <Table
                bordered
                columns={v信用详情}
                dataSource={f获取参数(v数据.data.current_report_detail)}
                pagination={false}
              />
              <Table
                bordered
                columns={v信用详情1}
                dataSource={f获取参数(v数据.data.current_report_detail)}
                pagination={false}
              />
            </>
          )
        }

      </>
    );
  }
}
