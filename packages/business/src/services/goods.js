import request from '@/services/baseService';

export default {
  findCategories: data => {
    return request(`/hzsx/examineProduct/findCategories`, data, 'get');
  },
  selectBusinessPrdouct: data => {
    return request(`/hzsx/product/selectBusinessPrdouct`, data);
  },
  selectExamineProductAuditLog: data => {
    return request(`/hzsx/product/selectExamineProductAuditLog`, data);
  },
  selectBusinessPrdouctCounts: data => {
    return request(`/hzsx/product/selectBusinessPrdouctCounts`, data);
  },
  busCopyProduct: data => {
    return request(`/hzsx/product/busCopyProduct`, data, 'get');
  },
  busCopyProductBuyProduct: data => {
    return request(`/hzsx/productDirect/busCopyProductBuyProduct`, data, 'get');
  },
  busInsertProduct: data => {
    return request(`/hzsx/product/busInsertProduct`, data);
  },
  busUpdateDirectBuyProduct: data => {
    return request(`/hzsx/productDirect/busUpdateDirectBuyProduct`, data);
  },
  busInsertDirectBuyProduct: data => {
    return request(`/hzsx/productDirect/busInsertDirectBuyProduct`, data);
  },

  busUpdateProductByRecycleDel: data => {
    return request(`/hzsx/product/busUpdateProductByRecycleDel`, data, 'get');
  },
  busDeleteProductBuyProductById: data => {
    return request(`/hzsx/productDirect/busDeleteProductBuyProductById`, data, 'get');
  },

  selectProductEdit: data => {
    return request(`/hzsx/product/selectProductEdit`, data, 'get');
  },
  updateBusProduct: data => {
    return request(`/hzsx/product/updateBusProduct`, data);
  },
  selectDistrict: data => {
    return request(`/hzsx/district/selectDistrict`, data, 'get');
  },
  exportBusinessPrdouct: data => {
    return request(`/hzsx/product/exportBusinessPrdouct`, data);
  },
  exportBusinessDirectBuyPrdouct: data => {
    return request(`/hzsx/productDirect/exportBusinessDirectBuyPrdouct`, data);
  },
  selectBusinessDirectBuyPrdouct: data => {
    return request(`/hzsx/productDirect/selectBusinessDirectBuyPrdouct`, data);
  },
  selectProductBuyProductEdit: data => {
    return request(`/hzsx/productDirect/selectProductBuyProductEdit`, data, 'get');
  },
};
