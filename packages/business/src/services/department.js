import request from "@/services/baseService";

export default {
  queryAll:(data)=>{
    return request(`/hzsx/department/queryAll`, data, 'get')
  },
  add:(data)=>{
    return request(`/hzsx/department/add`, data)
  },
  authPage:(data)=>{
    return request(`/hzsx/department/authPage`, data, 'get')
  },
  delete:(data)=>{
    return request(`/hzsx/department/delete`, data, 'get')
  },
  queryPage:(data)=>{
    return request(`/hzsx/department/queryPage`, data)
  },
  update:(data)=>{
    return request(`/hzsx/department/update`, data)
  },
  updateAuth:(data)=>{
    return request(`/hzsx/department/updateAuth`, data)
  },
  userUpdateAuth:(data)=>{
    return request(`/hzsx/user/updateAuth`, data)
  },
  queryOne:(data)=>{
    return request(`/hzsx/department/queryOne`, data, 'get')
  },
  userAuthPage:(data)=>{
    return request(`/hzsx/user/authPage`, data, 'get')
  },
  queryBackstageUserDetail:(data)=>{
    return request(`/hzsx/user/queryBackstageUserDetail`, data)
  },
}
