import request from '@/utils/request';

export async function query() {
  return request('/hzsx/api/users');
}
export async function queryCurrent() {
  return request('/hzsx/api/currentUser');
}
export async function queryNotices() {
  return request('/hzsx/api/notices');
}
