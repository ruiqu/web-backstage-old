import request from '@/utils/request';
function getToken() {
  const token = localStorage.getItem('token');
  return token;
}
// //商品审核
// export const getGoodsList = body => {
//   return request('ope/product/selectExaminePoroductList', {
//     method: 'POST',
//     body,
//   });
// };

//运营模板消息分页
export async function queryOpeOrderByCondition(params) {
  return request(`/hzsx/examineProduct/selectExaminePoroductList`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
export async function selectExamineProductEdit(params) {
  return request(`/hzsx/examineProduct/selectExamineProductEdit?productId=${params.productId}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}
export async function findCategories(params) {
  return request(`/hzsx/examineProduct/findCategories`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}
export async function selectExamineProductDetail(params) {
  return request(`/hzsx/examineProduct/selectExamineProductDetail?productId=${params.productId}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}

export async function updateExamineProduct(params) {
  return request(`/hzsx/examineProduct/updateExamineProduct`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}

//商品编辑
export const getGoodsEdit = body =>
  request(`/hzsx/ope/product/selectExamineProductEdit?productId=${body.productId}`, { methods: 'GET' });
//商品编辑类目
export const editCategory = body => request(`/hzsx/ope/product/findCategories`, { method: 'GET' });

//商品详情
export const getGoodsInfo = body =>
  request(`/hzsx/ope/product/selectExamineProductDetail?productId=${body}`, {
    method: 'GET',
  });

//编辑类目
export const getSelectAll = body => request('/hzsx/ope/category/selectAll', { method: 'GET' });

//改变类目
export const handleChangSelect = body =>
  request(
    `/hzsx/ope/product/editFrontCategory?categoryId=${body.categoryId}&productId=${body.productId}`,
    { method: 'GET' },
  );

//改变审核状态
export const changStatus = body =>
  request(
    `/hzsx/ope/product/examineProductConfirm?id=${body.id}&auditState=${body.auditState}&reason=${body.reason}`,
    { method: 'GET' },
  );

//编辑商品
export const updateGoods = body =>
  request(`/hzsx/ope/product/updateExamineProduct`, {
    method: 'POST',
    body,
  });
