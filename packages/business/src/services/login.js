import request from '@/utils/request';
function getToken() {
  const token = localStorage.getItem('token');
  return token;
}
export async function Login(params) {
  return request('/hzsx/user/login', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
export async function getFakeCaptcha(mobile) {
  return request(`/hzsx/api/login/captcha?mobile=${mobile}`);
}

export async function homeData(params) {
  return request('/hzsx/ope/order/opeOrderStatistics', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
export async function queryOrderReport(params) {
  return request('/hzsx/ope/order/queryOrderReport', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}

import _request from "@/services/baseService";

export default {
  register:(data)=>{
    return _request(`/hzsx/user/register`, data)
  },
  restPassword:(data)=>{
    return _request(`/hzsx/user/restPassword`, data)
  },
  sendValidateCode:(data)=>{
    return _request(`/hzsx/user/sendValidateCode`, data, 'get')
  },
 
  
  
}
