import Mock from 'mockjs'
const getShopList = (req, res) => {
  const { pageNumber, pageSize} = req.body
  res.json(Mock.mock({
    "data": {
      "records|20": [
        {
          "shopId": '@id',
          'shopName': '@cname',
          'enterpriseName': '@ctitle(30)',
          'type|1': [0, 1, 2],
          'status|1': [0, 1, 2],
          'crateTime': '@datetime'
        }
      ],
      current: 1,
      pages:3,
      size: 1000,
      total: 21,
    },
    msg: '操作成功'
  }));
};

export default {
  'post /hzsx/shop/getShopList': getShopList,
};
