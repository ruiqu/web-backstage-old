import Mock from 'mockjs'

const getCommission = (req, res) => {
  const { pageNumber, pageSize} = req.body
  res.json(Mock.mock({
    "data": {
      "records|10": [
        {
          "shopId": '@id',
          'shopName': '@cname',
          'enterpriseName': '@ctitle(30)',
          'type|1': [0, 1, 2],
          'status|1': [0, 1, 2],
          'createTime': '@datetime'
        }
      ],
      current: pageNumber,
      pages:3,
      size: pageSize,
      total: 21,
    },
    msg: '操作成功'
  }));
};
const addCommission = (req, res) => {
  const data = req.body
  res.json(Mock.mock({
    "data": {
      data
    },
    msg: '操作成功'
  }));
};
const getCommissionDetail = (req, res) => {
  const data = req.body
  res.json(Mock.mock({"data":{"records":{
     'status|1': [0, 1, 2],'createTime': '@datetime',"shopName":"120000201904261894","enterpriseName":"323","shopId":"dasdasdas",
        'approveTime': '@datetime',
        'approvePerson': '@cname',
        'approveSuggestion': '@cparagraph(1, 3)',
        "type":[
          {type: 1, "zhouqi":"32","traderRate":"32","platformRate":"32","aliPayName":"3232"},
          {type: 2, "zhouqi":"22","traderRate":"12","platformRate":"12","aliPayName":"13232"},
          ]}
        },"msg":"操作成功"}));
};

export default {
  'post /hzsx/commission/getCommissionListByPage': getCommission,
  'post /hzsx/commission/addCommission': addCommission,
  'post /hzsx/commission/getCommissionDetail': getCommissionDetail,
};
