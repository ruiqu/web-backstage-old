import Mock from 'mockjs'

const selectReceptionCategoryList = (req, res) => {
  const { categoryId, pageSize} = req.params
  console.log(categoryId);
  const data0 = {"code":1,"msg":"操作成功","data":[{"id":7,"createTime":"2019-03-18 01:11:53","updateTime":"2020-06-28 22:15:31","deleteTime":null,"name":"手机优选","icon":"","parentId":0,"sortRule":2},{"id":8,"createTime":"2019-03-18 01:17:01","updateTime":"2019-03-18 01:17:01","deleteTime":null,"name":"电脑平板","icon":null,"parentId":0,"sortRule":2},{"id":9,"createTime":"2019-03-18 01:21:11","updateTime":"2020-02-18 22:55:04","deleteTime":null,"name":"游戏电玩","icon":null,"parentId":0,"sortRule":3},{"id":10,"createTime":"2019-03-18 01:21:35","updateTime":"2019-03-18 01:21:35","deleteTime":null,"name":"数码相机","icon":null,"parentId":0,"sortRule":4},{"id":11,"createTime":"2019-03-18 01:21:35","updateTime":"2019-03-18 01:21:35","deleteTime":null,"name":"腕表配饰","icon":"","parentId":0,"sortRule":4},{"id":12,"createTime":"2019-03-18 01:22:29","updateTime":"2019-03-18 01:22:29","deleteTime":null,"name":"生活电器","icon":null,"parentId":0,"sortRule":6},{"id":112,"createTime":"2019-05-15 03:24:29","updateTime":"2019-05-15 03:24:29","deleteTime":null,"name":"健康护理","icon":"http://oss.huizustore.com/98c411655a0641d2bc2cc1634bf1df53.png","parentId":0,"sortRule":7},{"id":131,"createTime":"2019-06-12 00:17:57","updateTime":"2019-06-12 00:17:57","deleteTime":null,"name":"母婴玩具","icon":"http://jbkj-res.oss-cn-hangzhou.aliyuncs.com/98c411655a0641d2bc2cc1634bf1df53.png","parentId":0,"sortRule":8},{"id":137,"createTime":"2019-06-14 21:14:37","updateTime":"2019-06-14 21:14:37","deleteTime":null,"name":"办公设备","icon":"http://jbkj-res.oss-cn-hangzhou.aliyuncs.com/98c411655a0641d2bc2cc1634bf1df53.png","parentId":0,"sortRule":9},{"id":142,"createTime":"2019-06-17 21:03:54","updateTime":"2019-06-17 21:03:54","deleteTime":null,"name":"服饰箱包","icon":"http://jbkj-res.oss-cn-hangzhou.aliyuncs.com/98c411655a0641d2bc2cc1634bf1df53.png","parentId":0,"sortRule":10},{"id":151,"createTime":"2019-07-02 21:49:24","updateTime":"2019-07-02 21:49:24","deleteTime":null,"name":"出行娱乐","icon":"http://jbkj-res.oss-cn-hangzhou.aliyuncs.com/98c411655a0641d2bc2cc1634bf1df53.png","parentId":0,"sortRule":11},{"id":1201,"createTime":"2020-03-21 22:34:02","updateTime":"2020-03-21 22:34:02","deleteTime":null,"name":"心选好物","icon":null,"parentId":0,"sortRule":12}]}
  const data7 = {"code":1,"msg":"操作成功","data":[{"id":123,"createTime":"2019-06-02 22:19:48","updateTime":"2020-03-21 21:24:43","deleteTime":null,"name":"华为","icon":"http://oss.huizustore.com/4bd4d69f4bac4b57b975c109f70eaf28.png","parentId":7,"sortRule":2},{"id":124,"createTime":"2019-06-02 22:20:06","updateTime":"2020-03-21 21:24:47","deleteTime":null,"name":"小米","icon":"http://oss.huizustore.com/3d0fadf092364dc3a3f8af046f7a5086.png","parentId":7,"sortRule":3},{"id":125,"createTime":"2019-06-02 22:20:26","updateTime":"2020-03-21 21:24:51","deleteTime":null,"name":"VIVO","icon":"http://oss.huizustore.com/b6ab9a31aef54c46b1ef4331a7548292.png","parentId":7,"sortRule":4},{"id":126,"createTime":"2019-06-02 22:20:44","updateTime":"2020-03-21 21:24:55","deleteTime":null,"name":"OPPO","icon":"http://oss.huizustore.com/dc01537712e24879a7ffd5b079f5c554.png","parentId":7,"sortRule":5},{"id":127,"createTime":"2019-06-02 22:21:10","updateTime":"2020-03-21 21:25:00","deleteTime":null,"name":"美图","icon":"http://oss.huizustore.com/4256a8ed2cf94922a686866e52cd8e9b.png","parentId":7,"sortRule":6},{"id":128,"createTime":"2019-06-02 22:21:27","updateTime":"2020-03-21 21:25:03","deleteTime":null,"name":"魅族","icon":"http://oss.huizustore.com/fdc3992626304acfa93864394cd7b00c.png","parentId":7,"sortRule":7},{"id":130,"createTime":"2019-06-02 22:36:16","updateTime":"2020-03-21 21:25:07","deleteTime":null,"name":"卡西欧拍照","icon":"http://oss.huizustore.com/24a9e53e72ca412fb9d6ae2e6fc11ce0.png","parentId":7,"sortRule":8},{"id":656,"createTime":"2019-09-23 23:53:51","updateTime":"2019-09-23 23:53:51","deleteTime":null,"name":"三星","icon":"http://oss.huizustore.com/7fc1a654a8e54aea9a02c2b738e5b882.png","parentId":7,"sortRule":8},{"id":150,"createTime":"2019-06-29 00:44:48","updateTime":"2019-06-29 00:44:48","deleteTime":null,"name":"一加","icon":"http://oss.huizustore.com/44315ce20026466585d00e0ce05e8d5c.png","parentId":7,"sortRule":9},{"id":140,"createTime":"2019-06-17 19:26:54","updateTime":"2020-03-21 21:25:16","deleteTime":null,"name":"充电配件","icon":"http://oss.huizustore.com/c66c46c13614467ba9999bc622c20cf4.png","parentId":7,"sortRule":10},{"id":13,"createTime":"2019-03-18 01:29:00","updateTime":"2020-06-22 22:33:57","deleteTime":null,"name":"苹果","icon":"http://oss.huizustore.com/98c411655a0641d2bc2cc1634bf1df53.png","parentId":7,"sortRule":14}]}
  const data71 = {"code":1,"msg":"操作成功","data":[{"id":123,"createTime":"2019-06-02 22:19:48","updateTime":"2020-03-21 21:24:43","deleteTime":null,"name":"华为","icon":"http://oss.huizustore.com/4bd4d69f4bac4b57b975c109f70eaf28.png","parentId":7,"sortRule":2},]}
  let data = categoryId === 0 ? data0 : data71
  res.json(Mock.mock(data));
};
const addCommission = (req, res) => {
  const data = req.body
  res.json(Mock.mock({
    "data": {
      data
    },
    msg: '操作成功'
  }));
};
const getCommissionDetail = (req, res) => {
  const data = req.body
  res.json(Mock.mock({"data":{"records":{
        'status|1': [0, 1, 2],'createTime': '@datetime',"shopName":"120000201904261894","enterpriseName":"323","shopId":"dasdasdas",
        'approveTime': '@datetime',
        'approvePerson': '@cname',
        'approveSuggestion': '@cparagraph(1, 3)',
        "type":[
          {type: 1, "zhouqi":"32","traderRate":"32","platformRate":"32","aliPayName":"3232"},
          {type: 2, "zhouqi":"22","traderRate":"12","platformRate":"12","aliPayName":"13232"},
        ]}
    },"msg":"操作成功"}));
};

export default {
  'get /hzsx/category/selectReceptionCategoryList': selectReceptionCategoryList,
  'post /hzsx/commission/addCommission': addCommission,
  'post /hzsx/commission/getCommissionDetail': getCommissionDetail,
};
