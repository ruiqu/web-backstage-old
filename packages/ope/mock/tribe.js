import Mock from 'mockjs'
const queryTribePageUsingPOST = (req, res) => {
  const { pageNumber, pageSize} = req.body
  res.json(Mock.mock({
    "data": {
      "current": 1,
      "orders": [
        {
          "column": ""
        }
      ],
      "pages": 0,
      "records|10": [
        {
          "authorIcon": "",
          "authorName": "",
          "authorUid": "",
          "channerIds": [],
          "channerNames": [],
          "commentCount": 0,
          "content": "123",
          "createTime": "@datetime",
          "deleteTime": "@datetime",
          "id": "@id",
          "images": [
            {
              "isMain": 0,
              "src": ""
            }
          ],
          "laudCount": 0,
          "lauded": true,
          "pageNumber": 0,
          "pageSize": 0,
          "picUrl": "",
          "pics": [],
          "status|1": ['0', '1'],
          "summary": "@title",
          "title": "@title",
          "top": 0,
          "videoStatus": 0,
          "viewCount": 0
        }
      ],
      "size": 20,
      "total": 100
    },
    msg: '操作成功'
  }));
};

export default {
  'post /hzsx/tribe-controller/queryTribePageUsingPOST': queryTribePageUsingPOST,
  'post /hzsx/tribe/deleteTribe': (req, res)=>{
    res.json(Mock.mock({
      "data": {},
      msg: '操作成功'
    }));
  },
};
