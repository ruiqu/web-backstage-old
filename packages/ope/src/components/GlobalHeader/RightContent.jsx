import React, { PureComponent } from 'react';
import { FormattedMessage } from 'umi/locale';
import { Tag, Icon } from 'antd';
import { connect } from 'dva';
import moment from 'moment';
import groupBy from 'lodash/groupBy';
import styles from './index.less';

@connect()
export default class GlobalHeaderRight extends PureComponent {
  handleMenuClick = ({ key }) => {
    const { dispatch } = this.props;
    if (key === 'userCenter') {
      router.push('/account/center');
      return;
    }
    if (key === 'triggerError') {
      router.push('/exception/trigger');
      return;
    }
    if (key === 'userinfo') {
      router.push('/account/settings/base');
      return;
    }
    if (key === 'logout') {
      dispatch({
        type: 'login/logout',
      });
    }
  };
  render() {
    const { onMenuClick, theme } = this.props;
    let className = styles.right;
    if (theme === 'dark') {
      className = `${styles.right}  ${styles.dark}`;
    }
    return (
      <div className={className}>
        <div style={{ cursor: 'pointer', marginRight: '10px' }}>
          <span>登录账号：{localStorage.getItem('mobile')}</span>
          <span onClick={() => this.handleMenuClick({ key: 'logout' })}>
            <Icon type="logout" style={{ paddingRight: '5px', paddingLeft: '20px' }} />
            <FormattedMessage id="menu.account.logout" defaultMessage="logout" />
          </span>
        </div>
      </div>
    );
  }
}
