import React from 'react';
import styles from './index.less';
export default ({
  title,
  hasWarning,
  children,
  style,
  explain,
  isExplain,
  money,
  titleWrapper,
}) => {
  return (
    <div className={styles.wrapper} style={{ ...style }}>
      {!!title && (
        <div className={styles.titleWrapper} style={{ ...titleWrapper }}>
          <span className={styles.title}>
            {title}
            {/* {
                isExplain ? <span className={styles.explain} style={{ ...style }}>{explain}{money}元</span> : null
              } */}
          </span>
          {/* {!!hasWarning && <div className={styles.warning}>您的账户剩余金额不足，请及时续充，以免影响贵公司业务</div>} */}
        </div>
      )}
      {children}
    </div>
  );
};
