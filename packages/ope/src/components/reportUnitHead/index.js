/**
 * 报告的标题头信息
 */
import styles from "./index.less"

const ReportUnitHead = ({ title }) => {
  return (
    <div className={styles.riskUnitHeadWrapper}>
      <span className={styles.l1}>///</span>
      <span>{ title }</span>
      <span className={styles.l1}>///</span>
    </div>
  )
}

export default ReportUnitHead
