import React from 'react';
import {Descriptions} from "antd";

// 批量渲染描述组件
export const renderDescriptionsItems = (fields, title) =>{
  return (
    <Descriptions title={title}>
      {
        fields && fields.map(field=>{
          return (
            <Descriptions.Item key={field.label} label={field.label} className={field.className}>
              {
                field.render ? field.render : field.value
              }
            </Descriptions.Item>
          )
        })
      }
    </Descriptions>
  )
}

