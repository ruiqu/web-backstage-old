import React, { Component } from 'react';
import { Table } from 'antd';
import { connect } from 'dva';
import styles from './index.less';

@connect(() => ({}))
export default class AntTable extends Component {
  render() {
    const tableClsName = this.props.isLimitHeightTable ? styles.limitHeightTable : "" // 如果需要限高的话，那么返回其样式名

    return (
      <div className={tableClsName} style={{ marginBottom: '28px' }}>
        <Table
          columns={this.props.columns}
          dataSource={this.props.dataSource}
          pagination={false}
          size="middle"
          tableLayout="fixed"
          scroll={{ y: 430, x: this.props.scrollx }}
        />
      </div>
    );
  }
}
