import React, { Component } from 'react';
import styles from './index.less';
export default class index extends Component {
  render() {
    const { total, land, frame, children } = this.props;
    return (
      <div className={styles.total} style={{ ...total }}>
        <div className={styles.land} style={{ ...land }}>
          <div className={styles.frame} style={{ ...frame }}>
            {children}
          </div>
        </div>
      </div>
    );
  }
}
