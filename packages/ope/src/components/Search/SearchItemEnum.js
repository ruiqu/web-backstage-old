import {LZFormItem} from "@/components/LZForm";
import {Input, Select, DatePicker} from "antd";
import moment from "moment";
import React from "react";
import {goodsAuditStatus, withShopStatus} from "@/utils/enum";

const Option = Select.Option;

const { RangePicker } = DatePicker;

// 买断账单状态列表
const buyOutStatusData = {
  '01': '取消',
  // '02': '关闭',
  '03': '完成',
  '04': '待支付',
  // '05': '支付中',
};

const arrStatus =  [
  // 用户取消订单
  'WAITING_BUSINESS_DELIVERY/待商家发货',
  'WAITING_GIVE_BACK/租用中',
  'WAITING_SETTLEMENT/待结算',
  'BUSINESS_CLOSE_ORDER/商家关闭订单',
  'PLATFORM_CLOSE_ORDER/平台关闭订单',
  'SHOP_RISK_REVIEW_CLOSE_ORDER/商家风控关单',
  'USER_APPLICATION/用户申请关单',
  'WAITING_USER_RECEIVE_CONFIRM/待用户确认收货',
  'RC_REJECT/风控关闭订单',
  'USER_CANCELED_CLOSED/用户取消订单',
  'ALREADY_FREEZE/已冻结',
  'WAITING_PAYMENT/待支付',
  'APPLY_CLOSE_ORDER/订单关闭',
  'USER_OVERTIME_PAYMENT_CLOSED/用户超时支付关闭订单',
  'USER_DELETE_ORDER/用户删除订单',
  'BUSINESS_OVERTIME_CLOSE_ORDER/商家超时发货关闭订单',
  'WAITING_BUSINESS_CONFIRM_RETURN_REFUND/待商家确认退货退款',
  'BUSINESS_REFUSED_RETURN_REFUND/商家拒绝退货退款',
  'WAITING_USER_RETURN/待退货',
  'WAITING_REFUND/待退款',
  'WAITING_BUSINESS_RECEIVE_CONFIRM/待商家确认收货',
  'TO_BE_RETURNED/待归还',
  'WAITING_CONFIRM_SETTLEMENT/待确认结算端(C端确认)',
  'WAITING_SETTLEMENT_PAYMENT/出结算单后的待支付',
  'SETTLEMENT_WITHOUT_PAYMENT_OVERTIME_AUTOCONFIRM/自动确认结算',
  'SETTLEMENT_WITH_PAYMENT_OVERTIME/结算支付超时',
  'GIVING_BACK/归还中',
  'ORDER_FINISH/订单完成',
  'ORDER_VERDUE/逾期',
  'SETTLEMENT_RETURN_CONFIRM_PAY/结算单违约金支付逾期',
  'USER_PAY_FAIL_CLOSE/用户支付失败关闭订单',
  'WAITING_EVALUATION/待评价',
  'ALREADY_EVALUATION/已评价',
  'PENALTY_WAITING_SETTLEMENT/违约金待结算中',
  'JH_CREATED/已生成借还订单，其角色类似“待支付”',
  'WAITING_BUCKLE_SETTLEMENT/代扣结算中',
  'AITING_USER_RECEIVE_CONFIRM/已冻结',
]

const renderFormItemInput = ({field, getFieldDecorator, placeholder, label}) => {
  return (
    <LZFormItem field={field} label={label} key={field} getFieldDecorator={getFieldDecorator}>
      <Input placeholder={placeholder} style={{ width: 200 }} allowClear />
    </LZFormItem>
  )
}

const disabledDate = current => {
  return moment(current).format('E') != 5;
};
export default {
  companyName: (getFieldDecorator)=>{
    return renderFormItemInput({getFieldDecorator, field: 'name',
      placeholder: '请输入企业资质名称', label: '企业资质名称'})
  },
  shopName: (getFieldDecorator)=>{
    return renderFormItemInput({getFieldDecorator, field: 'shopName',
      placeholder: '请输入店铺名称', label: '店铺名称'})
  },
  shopId: (getFieldDecorator)=>{
    return renderFormItemInput({getFieldDecorator, field: 'shopId',
      placeholder: '请输入店铺编号', label: '店铺编号'})
  },
  type: (getFieldDecorator)=>{
    return (
      <LZFormItem field="typeInfo" label={'分佣类型'} key="typeInfo" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="分佣类型" style={{ width: 200 }} allowClear>
          <Option value="BUY_OUT">买断</Option>
          <Option value="RENT_MONTH">月租金分账</Option>
 
        </Select>
      </LZFormItem>
    )
  },
  isPhoneExamination: (getFieldDecorator)=>{
    return (
      <LZFormItem field="isPhoneExamination" label={'是否需要平台电审'} key="isPhoneExamination" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="是否需要平台电审" style={{ width: 180 }} allowClear>
          <Option value="">全部</Option>
          <Option value="1">是</Option>
          <Option value="0">否</Option>
        </Select>
      </LZFormItem>
    )
  },
  status: (getFieldDecorator)=>{
    return (
      <LZFormItem field="status" label={'分佣状态'} key="status" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="分佣状态" style={{ width: 200 }} allowClear>
          <Option value="PASS">审核通过</Option>
          <Option value="REJECT">审核拒绝</Option>
          <Option value="PENDING">待审核</Option>
        </Select>
      </LZFormItem>
    )
  },
  goodsAuditStatus: (getFieldDecorator)=>{
    return (
      <LZFormItem field="auditState" label="审批状态" key="auditState"
                  initialValue={'全部'}
                  getFieldDecorator={getFieldDecorator}>
        <Select placeholder="审批状态" style={{ width: 200 }} allowClear>
          <Option value="全部" key="全部">全部</Option>
          {
            Object.keys(goodsAuditStatus).map(v=>{
              return (
                <Option value={v} key={v}>{goodsAuditStatus[v]}</Option>
              )
            })
          }
        </Select>
      </LZFormItem>
    )
  },
  applicant: (getFieldDecorator)=>{
    return renderFormItemInput({getFieldDecorator, field: 'applicant',
      placeholder: '请输入申请人', label: '申请人'})
  },
  /** 使用在佣金管理-佣金设置中的申请人 */
  // yongjingApplyName: (getFieldDecorator)=>{
  //   return renderFormItemInput({getFieldDecorator, field: 'name',
  //     placeholder: '请输入申请人', label: '申请人'})
  // },
  productName: (getFieldDecorator)=>{
    return renderFormItemInput({getFieldDecorator, field: 'productName',
      placeholder: '请输入商品名称', label: '商品名称'})
  },
  
  shopName: (getFieldDecorator)=>{
    return renderFormItemInput({getFieldDecorator, field: 'shopName',
      placeholder: '请输入商家名称', label: '商家名称'})
  },
  productId: (getFieldDecorator)=>{
    return renderFormItemInput({getFieldDecorator, field: 'productId',
      placeholder: '请输入商品编号', label: '商品编号'})
  },
  activityName: (getFieldDecorator)=>{
    return renderFormItemInput({getFieldDecorator, field: 'activityName',
      placeholder: '请输入主标题', label: '主标题'})
  },
  categoryIds: (getFieldDecorator, options)=>{
    return (
      <LZFormItem field="categoryIds" key="categoryIds" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="商品类目" style={{ width: 200 }} allowClear>
          {
            options.map(option=>{
              return <Option value={option.value} key={option.label}>{option.label}</Option>
            })
          }
        </Select>
      </LZFormItem>
    )
  },
  goodsAuditUpStatus: (getFieldDecorator)=>{
    return (
      <LZFormItem field="type" key="type" label="上架状态" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="上架状态" style={{ width: 200 }} allowClear>
          <Option value="">全部</Option>
          <Option value="1">已上架</Option>
          <Option value="2">已下架</Option>
          <Option value="0">回收站</Option>
        </Select>
      </LZFormItem>
    )
  },
  shangjiaStatus: (getFieldDecorator)=>{
    return (
      <LZFormItem field="type" key="type" label="上架状态" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="上架状态" style={{ width: 200 }} allowClear>
          {/*<Option value="0">回收站中的商品</Option>*/}
          <Option value="1">已上架</Option>
          <Option value="2">已下架</Option>
        </Select>
      </LZFormItem>
    )
  },
  auditStatus: (getFieldDecorator)=>{
    return (
      <LZFormItem field="auditState" key="auditState" label="审核状态" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="审核状态" style={{ width: 200 }} allowClear>
          <Option value={0}>审核中</Option>
          <Option value={1}>审核不通过</Option>
          <Option value={2}>审核通过</Option>
        </Select>
      </LZFormItem>
    )
  },
  shopAuditStatus: (getFieldDecorator)=>{
    return (
      <LZFormItem field="status" key="status" label="审核状态" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="审核状态" style={{ width: 200 }}>
          <Option value="">全部</Option>
          <Option value="3">待审核</Option>
          <Option value="5">审核通过</Option>
          <Option value="4">审核拒绝</Option>
        </Select>
      </LZFormItem>
    )
  },
  withBusinessSettleStatus: (getFieldDecorator)=>{
    return (
      <LZFormItem field="status" label={'结算状态'} key="status" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="结算状态" style={{ width: 200 }} allowClear>
          {/* {
            Object.keys(withShopStatus).map(v=>{
              return (
                <Option value={v} key={v}>{withShopStatus[v]}</Option>
              )
            })
          } */}
           <Option value={'WAITING_SETTLEMENT'} key={'WAITING_SETTLEMENT'}>待结算</Option>
           <Option value={'TO_AUDIT'} key={'TO_AUDIT'}>待审核</Option>
           <Option value={'WAITING_PAYMENT'} key={'WAITING_PAYMENT'}>待支付</Option>
           <Option value={'IN_PAYMENT'} key={'IN_PAYMENT'}>支付中</Option>
           <Option value={'PAID'} key={'PAID'}>已支付</Option>
        </Select>
      </LZFormItem>
    )
  },
  orderer: (getFieldDecorator)=>{
    return renderFormItemInput({getFieldDecorator, field: 'userName',
      placeholder: '请输入下单人姓名', label: '下单人姓名'})
  },
  ordererPhone: (getFieldDecorator)=>{
    return renderFormItemInput({getFieldDecorator, field: 'telephone',
      placeholder: '请输入下单人手机号', label: '下单人手机号'})
  },
  orderId: (getFieldDecorator)=>{
    return renderFormItemInput({getFieldDecorator, field: 'orderId',
      placeholder: '请输入订单编号', label: '订单编号'})
  },
  
  // 部落
  tribeTitle: (getFieldDecorator)=>{
    return renderFormItemInput({getFieldDecorator, field: 'title',
      placeholder: '请输入部落标题', label: '部落标题'})
  },
  tribeStatus: (getFieldDecorator)=>{
    return (
      <LZFormItem field="status" label="状态" key="status" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="状态" style={{ width: 200 }} allowClear>
          <Option value="INVALID">无效</Option>
          <Option value="VALID">有效</Option>
        </Select>
      </LZFormItem>
    )
  },
  
  
  createTimeStart: (getFieldDecorator)=>{
    return (
      <LZFormItem field="createDate" label={'下单时间'} key="createDate" getFieldDecorator={getFieldDecorator}>
        <RangePicker
          showTime
          format="YYYY/MM/DD HH:mm:ss"
          placeholder={['下单开始时间', '结束时间']}
        />
      </LZFormItem>
    )
  },
  billTimeStart: (getFieldDecorator)=>{
    return (
      <LZFormItem field="bill" label={'账单生成时间'} key="bill" getFieldDecorator={getFieldDecorator}>
        <RangePicker
          // showTime
          format="YYYY-MM-DD"
          placeholder={['开始时间', '结束时间']}
          disabledDate={disabledDate}
        />
      </LZFormItem>
    )
  },
  settleDate:(getFieldDecorator)=>{
    // 之前的逻辑把初始值设置为了当前时间：initialValue={moment().subtract(moment().format('E') - 5+7, 'days')}
    // 但是这个初始值结合佣金结算页面进行使用时会出现问题
    return (
      <LZFormItem initialValue={null} field="settleDate" label={'结算日期'} key="settleDate" getFieldDecorator={getFieldDecorator}>
        <DatePicker
          format="YYYY-MM-DD"
          disabledDate={disabledDate}
          placeholder="结算日期"
        />
      </LZFormItem>
    )
  },
  shopCreateDate: (getFieldDecorator)=>{
    return (
      <LZFormItem field="createDate" label={'提交时间'} key="createDate" getFieldDecorator={getFieldDecorator}>
        <RangePicker/>
      </LZFormItem>
    )
  },
  creatTime: (getFieldDecorator)=>{
    return (
      <LZFormItem field="creatTime" label={'提交时间'} key="creatTime" getFieldDecorator={getFieldDecorator}>
        <RangePicker
          ranges={{
            现在: [moment(), moment()],
            本月: [moment().startOf('month'), moment().endOf('month')],
          }}
          showTime
          format="YYYY/MM/DD HH:mm:ss"
          placeholder={['提交开始日期', '提交结束日期']}
          // onChange={onChange}
        />
      </LZFormItem>
    )
  },
  orderStatus: (getFieldDecorator)=>{
    return (
      <LZFormItem field="orderStatus" label={'订单状态'} key="orderStatus" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="订单状态" style={{ width: 200 }} allowClear>
          <Option value="全部">全部</Option>
          <Option value="01">待支付</Option>
          {/* <Option value="03">已支付申请关单</Option> */}
          <Option value="11">待审核</Option>
          <Option value="04">待发货</Option>
          <Option value="05">待确认收货</Option>
          <Option value="06">租用中</Option>
          <Option value="07">待结算</Option>
          <Option value="08">结算待支付</Option>
          <Option value="09">订单完成</Option>
          <Option value="10">交易关闭</Option>
          <Option value="12">待归还</Option>
        </Select>
      </LZFormItem>
    )
  },
  buyOutOrderStatus: (getFieldDecorator)=>{
    return (
      <LZFormItem field="orderStatus" label={'订单状态'} key="orderStatus" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="订单状态" style={{ width: 200 }} allowClear>
          {/* <Option value="" key="all">全部</Option> */}
          {
            Object.keys(buyOutStatusData).sort().map(value => {
              return <Option value={value} key={value}>{buyOutStatusData[value]}</Option>
            })
          }
        </Select>
      </LZFormItem>
    )
  },
  purchaseOrderStatus: (getFieldDecorator)=>{
    return (
      <LZFormItem field="orderStatus" label={'订单状态'} key="status" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="订单状态" style={{ width: 200 }} allowClear>
          {/* <Option value="" key="all">全部</Option> */}
          <Option value="WAITING_FOR_PAY">待支付</Option>
          <Option value="CANCEL">已取消</Option>
          <Option value="WAITING_FOR_DELIVERY">待发货</Option>
          <Option value="WAITING_RECEIVE">待收货</Option>
          <Option value="FINISH">已完成</Option>
        </Select>
      </LZFormItem>
    )
  },
  memberName: (getFieldDecorator)=>{
    return renderFormItemInput({getFieldDecorator, field: 'memberName',
      placeholder: '请输入姓名', label: '姓名'})
  },
  departments: (getFieldDecorator, options)=>{
    return (
      <LZFormItem field="departments" label={'所属部门'}
                  key="departments" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="所属部门" style={{ width: 200 }} allowClear>
          {
            options.map(option=>{
              return <Option value={option.value} key={option.label}>{option.label}</Option>
            })
          }
        </Select>
      </LZFormItem>
    )
  },
  packageName: (getFieldDecorator)=>{
    return renderFormItemInput({getFieldDecorator, field: 'name', placeholder: '请输入大礼包名称'})
  },
  packageStatus: (getFieldDecorator)=>{
    return (
      <LZFormItem field="status" key="status" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="大礼包状态" style={{ width: 200 }} allowClear>
          <Option value="0">无效</Option>
          <Option value="1">有效</Option>
        </Select>
      </LZFormItem>
    )
  },
  appVersion: (getFieldDecorator) => {
    return (
      <LZFormItem field="appVersion" label="合作方式" key="appVersion" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="请选择合作方式" style={{ width: 200 }} allowClear>
          <Option value="ZWZ">***名字***</Option>
          <Option value="LITE">简版小程序</Option>
          <Option value="TOUTIAO">抖音小程序</Option>
        </Select>
      </LZFormItem>
    )
  }
}

