import React, { Component, Fragment } from 'react';
import moment from 'moment';
import { Form, DatePicker, Input, Select, Button } from 'antd';
import SearchItemEnum from '@/components/Search/SearchItemEnum';

const FormItem = Form.Item;
const { RangePicker } = DatePicker;
const { Option } = Select;
@Form.create()
export default class Search extends Component {
  //查询
  handleFilter = () => {
    this.props.form.validateFields((err, value) => {
      this.props.handleFilter(value);
    });
  };
  // 重置
  handleReset = async () => {
    this.props.form.resetFields();
    this.props.form.setFieldsValue({
      auditState: undefined,
      type: undefined,
      isDelete: undefined,
      settleDate: undefined,
      // settleDate: moment().subtract(moment().format('E') - 5 + 7, 'days'),
    });
    this.props.handleFilter({ type: '重置' });
  };
  // 导出
  exportData = () => {
    this.props.form.validateFields((err, value) => {
      this.props.exportData(value);
    });
  };

  // 给搜索组件的选择框传递选项数据
  renderLZSearch = searchConfigs => {
    const { getFieldDecorator } = this.props.form;
    let allOptions = this.props.options || [];

    return (
      <Fragment>
        {searchConfigs.map(searchConfig => {
          let _options = [];
          if (allOptions) {
            const item = allOptions.find(f => f.field === searchConfig) || [];
            if (item) {
              _options = item.options;
            }
          }
          return SearchItemEnum[searchConfig](getFieldDecorator, _options);
        })}
      </Fragment>
    );
  };

  // 判断不同的来源展示不同的搜索组件
  selectForms = source => {
    let searchConfigs = [];
    switch (source) {
      case '店铺审核':
        searchConfigs = [
          'companyName',
          'isPhoneExamination',
          'shopId',
          'shopName',
          'shopCreateDate',
          'shopAuditStatus',
        ];
      case '店铺审核':
        searchConfigs = [
          'shopName',
          'isPhoneExamination',
          'shopId',
          'companyName',
          'shopCreateDate',
          'shopAuditStatus',
        ];
        break;
      case '商品审核':
        searchConfigs = [
          'goodsAuditStatus',
          'goodsAuditUpStatus',
          'productName',
          'productId',
          'shopId',
          'shopName',
          'creatTime',
        ];
        break;
      case '佣金设置':
        searchConfigs = ['shopName', 'type', 'status'];
        break;
      case '佣金审批':
        searchConfigs = ['shopName', 'type', 'status'];
        break;
      case '财务结算':
        searchConfigs = ['settleDate', 'shopName', 'withBusinessSettleStatus'];
        break;
      case '常规订单':
        searchConfigs = [
          'shopName',
          'withBusinessSettleStatus',
          'orderer',
          'ordererPhone',
          'orderId',
          'billTimeStart',
          // 'orderStatus',
        ];
        break;

      case '买断订单':
        searchConfigs = [
          'shopName',
          'withBusinessSettleStatus',
          'orderer',
          'ordererPhone',
          'orderId',
          'billTimeStart',
          // 'buyOutOrderStatus',
        ];
        break;
      case '购买订单':
        searchConfigs = [
          'shopName',
          'withBusinessSettleStatus',
          'orderer',
          'ordererPhone',
          'orderId',
          'billTimeStart',
          // 'purchaseOrderStatus',
        ];
        break;
      case '部落配置':
        searchConfigs = ['tribeTitle', 'tribeStatus'];
        break;
      case '成员管理':
        searchConfigs = ['memberName', 'departments'];
        break;
      case '商品列表':
        searchConfigs = ['productName', 'categoryIds', 'shangjiaStatus', 'auditStatus'];
        break;
      case '大礼包':
        searchConfigs = ['packageName', 'packageStatus'];
        break;
      case '活动配置':
        searchConfigs = ['activityName'];
        break;

      default:
        break;
    }
    return this.renderLZSearch(searchConfigs);
  };

  renderFooter = () => {
    const { needExport, needReset = true } = this.props;
    return (
      <div>
        <FormItem style={{ textAlign: 'center' }}>
          <Button type="primary" type="primary" onClick={this.handleFilter}>
            查询
          </Button>
          {needReset ? (
            <Button style={{ marginLeft: 8 }} onClick={this.handleReset}>
              重置
            </Button>
          ) : null}
          {needExport ? (
            <Button style={{ marginLeft: 8 }} onClick={this.exportData}>
              导出
            </Button>
          ) : null}
        </FormItem>
      </div>
    );
  };

  render() {
    const { source } = this.props;
    const { getFieldDecorator } = this.props.form;
    return (
      <Form layout="inline">
        {this.selectForms(source)}
        {this.props.channelList ? (
          <FormItem>
            {getFieldDecorator('channelId', {
              initialValue: '',
            })(
              <Select style={{ width: 180 }}>
                {this.props.channelList.map(item => {
                  return (
                    <Option key={item.channelId} value={item.channelId}>
                      {item.channelName}
                    </Option>
                  );
                })}
              </Select>,
            )}
          </FormItem>
        ) : null}
        {this.renderFooter()}
      </Form>
    );
  }
}
