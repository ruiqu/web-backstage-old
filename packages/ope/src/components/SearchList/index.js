import React, { Component } from 'react';
import { Form, DatePicker, Input, Select, Button, Pagination } from 'antd';
import MyPageTable from '@/components/MyPageTable';
import { onTableData } from '@/utils/utils.js';
const { RangePicker } = DatePicker;
const { Option } = Select;
/**
 * 传入props参数
 * @param {Object} handleFilter : 搜索翻页函数
 * @param {Array} searchHander : 搜索参数
 * @param {Array} columns : 表格列的配置描述
 * @param {Boolean} loading : 列表页面是否加载中
 * @param {Number} total : 列表条数
 * @param {Number} pageSize : 列表每页多少条 默认：pageSize = 20
 * @param {Boolean} pagination : 是否有翻页
 *
 */
/**
 * searchHander
 * @param {String} label : 名字
 * @param {String} type : 类型 input | select | rangePicker
 * @param {String} key: 搜索接口名字
 * @param {Array:[{value:value,name:name}]} optionList: select列表
 * @param {*} initialValue: 默认值
 */
@Form.create()
export default class SearchList extends Component {
  state = {
    valueName: 'value',
    labelName: 'name',
    current: 1,
  };
  componentDidMount() {
    const { pageSize = 20 } = this.props;
    this.props.handleFilter && this.props.handleFilter(1, pageSize);
  }
  /**
   * 查询列表
   */
  handleSubmit = e => {
    e.preventDefault();
    let { current } = this.state;
    const { pageSize = 20 } = this.props;
    this.props.form.validateFields((err, value) => {
      this.setState(
        {
          data: value,
        },
        () => {
          this.props.handleFilter && this.props.handleFilter(current, pageSize, value);
        },
      );
    });
  };

  renderInput = (item, val) => {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form.Item label={item.label} key={val}>
        {getFieldDecorator(item.key, { initialValue: item.initialValue || undefined })(
          <Input allowClear placeholder="请输入" style={{ width: 140 }} />,
        )}
      </Form.Item>
    );
  };
  renderSelect = (item, val) => {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form.Item label={item.label} key={val}>
        {getFieldDecorator(item.key, { initialValue: item.initialValue || undefined })(
          <Select allowClear placeholder="请选择" style={{ width: 140 }}>
            {item.optionList &&
              item.optionList.map((item, val) => (
                <Option value={item.value} key={val}>
                  {item.name}
                </Option>
              ))}
          </Select>,
        )}
      </Form.Item>
    );
  };
  renderRangePicker = (item, val) => {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form.Item key={val} label={item.label}>
        {getFieldDecorator(item.key, { initialValue: item.initialValue || undefined })(
          <RangePicker />,
        )}
      </Form.Item>
    );
  };

  /**
   * 搜索便利列表
   * @param {Array} list
   */
  selectForms = (list = []) => {
    return list.map((item, val) => {
      switch (item.type) {
        case 'input':
          return this.renderInput(item, val);
        case 'select':
          return this.renderSelect(item, val);
        case 'rangePicker':
          return this.renderRangePicker(item, val);
        default:
          break;
      }
    });
  };
  /**
   * 翻页函数
   * @param {Number} e.current 页数
   */
  onPage = e => {
    let _current = 1;
    if (e.current) {
      _current = e.current;
    } else {
      _current = e;
    }
    const { data } = this.state;
    const { pageSize = 20 } = this.props;
    this.setState(
      {
        current: _current,
      },
      () => {
        this.props.handleFilter(_current, pageSize, data);
      },
    );
  };
  render() {
    const {
      columns = [],
      loading = false,
      total = 1,
      pageSize = 20,
      list = [],
      pagination,
      children,
      paginationType,
    } = this.props;
    const { current } = this.state;
    const paginationProps = {
      current: current,
      pageSize: pageSize,
      total: total,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{total}条</span>
        </span>
      ),
    };
    return (
      <>
        <Form layout="inline" style={{ marginBottom: 20 }}>
          {this.selectForms(this.props.searchHander)}
          <Form.Item>
            <Button type="primary" onClick={this.handleSubmit}>
              查询
            </Button>
          </Form.Item>
        </Form>
        {children}
        <MyPageTable
          scroll
          onPage={this.onPage}
          paginationProps={pagination ? paginationProps : false}
          dataSource={onTableData(list)}
          columns={columns}
          loading={loading}
        />
        {paginationType && (
          <div style={{ display: 'flex', justifyContent: 'flex-end', margin: '20px 0' }}>
            <Pagination
              defaultCurrent={current}
              showTotal={total => `共${Math.ceil(total / 10)}页  共${total}条`}
              pageSize={pageSize}
              total={total}
              onChange={this.onPage}
            />
          </div>
        )}
      </>
    );
  }
}
