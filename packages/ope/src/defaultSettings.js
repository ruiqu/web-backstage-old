module.exports = {
  navTheme: 'light',
  primaryColor: '#5476F0',
  layout: 'sidemenu',
  contentWidth: 'Fluid',
  fixedHeader: false,
  autoHideHeader: false,
  fixSiderbar: false,
  menu: {
    disableLocal: false,
  },
  title: '运营中心',
  pwa: true,
  iconfontUrl: '',
  collapse: true,
};
