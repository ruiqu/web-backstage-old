export const defaultPageSize = 10

export const defaultQuery = {
  pageNumber: 1,
  pageSize: defaultPageSize,
}

export default {
  namespace: "quesConfigModel",

  state: {
    listApiParams: {}, // 每个tab所对应的请求参数数据
    activeTabId: null, // 处于焦点的菜单id
  },

  reducers: {
    /**
     * 修改处于焦点的菜单栏
     * @param {*} state 
     * @param {*} param1 
     */
    quesConfigMuActiveTab(state, { payload: activeTabId }) {
      return {
        ...state,
        activeTabId,
      }
    },
  }
}