import * as servicesApi from '@/services/coupons';
import { router } from 'umi';
import { message } from 'antd';
export default {
  namespace: 'coupons',
  state: {
    queryInfo: {
      pageNum: 1,
      pageSize: 10,
    },
    total: 0,
    list: [],
    productList: [],
    BusinessPrdouct: {},
    listData: [],
    listData2: [],
    detailDat: {},
    BindList: [],
    Bindtotal: 1,
  },
  effects: {
    *getCouponListByPage({ payload }, { call, put }) {
      const res = yield call(servicesApi.getCouponListByPage, payload);
      if (res) {
        yield put({
          type: 'saveList',
          payload: { data: res.data, queryInfo: payload },
        });
      }
    },
    *saveCoupon({ payload, callback }, { call }) {
      const info = { ...payload };
      if (payload.type === 0) {
        info.start = payload.startEnd[0].format('YYYY-MM-DD 00:00:00');
        info.end = payload.startEnd[1].format('YYYY-MM-DD 00:00:00');
      }
      if (payload.radioPurchase === '0') {
        info.minPurchase = 0;
      }
      let work = servicesApi.saveCouponByShopId;
      if (payload.id) {
        work = servicesApi.updateShopCoupon;
      }
      const res = yield call(work, info);
      if (res) {
        callback();
      }
    },
    *delCouponById({ payload, callback }, { call }) {
      const res = yield call(servicesApi.delCouponById, payload);
      if (res) {
        callback();
      }
    },
    *setBusinessPrdouct({ payload, callback }, { call }) {
      const res = yield call(servicesApi.setBusinessPrdouct, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
        yield put({
          type: 'business',
          payload: res,
        });
      }
    },
    *getUpload({ payload, callback }, { call }) {
      const res = yield call(servicesApi.getUpload, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    *couponAdd({ payload, callback }, { call }) {
      const res = yield call(servicesApi.couponAdd, payload);
      if (res) {
        message.success('新增成功');
        router.push('/Marketing/zuwuzuPackage/Coupon/list');
      }
    },
    *couponLiteAdd({ payload, callback }, { call }) {
      const res = yield call(servicesApi.couponLiteAdd, payload);
      if (res) {
        message.success('新增成功');
        router.push('/Marketing/LitePackage/Litelist');
      }
    },
    
    *modify({ payload, callback }, { call }) {
      const res = yield call(servicesApi.modify, payload);
      if (res) {
        message.success('编辑成功');
        router.push('/Marketing/zuwuzuPackage/Coupon/list');
      }
    },
    *liteModify({ payload, callback }, { call }) {
      const res = yield call(servicesApi.liteModify, payload);
      if (res) {
        message.success('编辑成功');
        router.push('/Marketing/LitePackage/Litelist');
      }
    },

    *list({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.queryPage, payload);
      if (res) {
        yield put({
          type: 'listData',
          payload: res,
        });
      }
    },
    *couponTemplatequeryPage({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.couponTemplatequeryPage, payload);
      if (res) {
        yield put({
          type: 'listData',
          payload: res.data,
        });
      }
    },
    *liteCouponTemplatequeryPage({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.liteCouponTemplatequeryPage, payload);
      if (res) {
        yield put({
          type: 'listData',
          payload: res.data,
        });
      }
    },
    
    *getAssignAbleTemplate({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.getAssignAbleTemplate, payload);
      if (res) {
        yield put({
          type: 'listData2',
          payload: res.data,
        });
      }
    },
    *getliteCouponTemplate({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.getliteCouponTemplate, payload);
      if (res) {
        yield put({
          type: 'listData2',
          payload: res.data,
        });
      }
    },
    *deleteList({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.deleteList, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    *liteDeleteList({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.liteDeleteList, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    
    *couponPackagemodify({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.couponPackagemodify, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    *liteCouponPackagemodify({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.liteCouponPackagemodify, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    *detailData({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.detailDataList, payload);
      if (res) {
        yield put({
          type: 'detailDatas',
          payload: res.data,
        });
      }
    },
    *getUpdatePageData({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.getUpdatePageData, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    *getLiteUpdatePageData({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.getLiteUpdatePageData, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    *detailBindList({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.detailBindList, payload);
      if (res) {
        yield put({
          type: 'detailBindLists',
          payload: res,
        });
      }
    },
    *liteDetailBindList({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.liteDetailBindList, payload);
      if (res) {
        yield put({
          type: 'detailBindLists',
          payload: res,
        });
      }
    },
    
    *exportEntityNum({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.exportEntityNum, payload);
      if (res) {
        window.open(res.data);
      }
    },
    *liteExportEntityNum({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.liteExportEntityNum, payload);
      if (res) {
        window.open(res.data);
      }
    },
    
    *upDate({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.upDate, payload);
      if (res) {
        message.success('编辑成功');
        router.push('/coupons/list');
      }
    },
    *couponPackage({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.add, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    *couponPackageLite({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.liteAdd, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    *couponPackageList({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.queryPage, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    *couponPackageListLite({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.liteQueryPage, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },

    //获取需要配置的小程序
    *listLitePlatformChannel({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.listLitePlatformChannel, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res.data);
        }
      }
    },
    *couponPackageDelete({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.couponPackageDelete, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    *liteCouponPackageDelete({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.liteCouponPackageDelete, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    
    *getPageData({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.getUpdatePageDatas, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    *getLietPageData({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.getLietUpdatePageDatas, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    
    *getupdate({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.add, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    *getGoods({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.getGoods, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    *addBindUrl({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.addBindUrl, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    *liteAddBindUrl({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.liteAddBindUrl, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
    *getSelectAlls({ payload, callback }, { call, put }) {
      const res = yield call(servicesApi.getSelectAlls, payload);
      if (res) {
        if (callback && typeof callback === 'function') {
          callback(res);
        }
      }
    },
  },
  reducers: {
    saveList(state, { payload }) {
      return {
        ...state,
        list: payload.data.records,
        total: payload.data.total,
        queryInfo: {
          ...payload.queryInfo,
        },
      };
    },

    saveProductList(state, { payload }) {
      return { ...state, productList: payload };
    },
    listData(state, { payload }) {
      console.log('payload:', payload);
      return {
        ...state,
        listData: payload.records,
        total: payload.total,
      };
    },
    listData2(state, { payload }) {
      console.log('payload:', payload);
      return {
        ...state,
        listData2: payload.records,
        total: payload.total,
      };
    },
    detailDatas(state, { payload }) {
      return {
        ...state,
        detailDat: payload.data,
      };
    },
    detailBindLists(state, { payload }) {
      return {
        ...state,
        BindList: payload.data.records,
        Bindtotal: payload.data.total,
      };
    },
  },
};
