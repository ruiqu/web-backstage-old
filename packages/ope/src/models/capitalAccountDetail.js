export const defaultPageSize = 10

export const defaultQuery = {
  pageNumber: 1,
  pageSize: defaultPageSize,
}

export default {
  namespace: "capitalAccountDetailModel",

  state: {
    queryObj: defaultQuery,
  },

  reducers: {
    /**
     * 修改查询数据
     * @param {*} state 
     * @param {*} param1 
     * @returns 
     */
    muQueryObjByKv(state, { payload: { val, key } }) {
      return {
        ...state,
        queryObj: {
          ...state.queryObj,
          [key]: val,
        }
      }
    },

    /**
     * 修改全部查询数据时触发
     * @param {*} state 
     * @param {*} param1 
     * @returns 
     */
     muQueryObjCompletely(state, { payload: obj }) {
      return {
        ...state,
        queryObj: {
          ...state.queryObj,
          ...obj
        }
      }
    },
  }
}