import * as servicesApi from '@/services/service';

export default {
  spacename: 'index',
  state: {
    list: [],
    goodsInfo: {},
    selectData: [],
    editSelectData: [],
    editInfo: {},
  },
  effects: {
    //获取商品审核列表
    *getGoods({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.queryOpeOrderByCondition, payload);

      yield put({
        type: 'saveData',
        data: res.data,
      });

      if (callback) callback(res);
    },
    *selectExaminePoroductDirectBuyList({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.selectExaminePoroductDirectBuyList, payload);

      yield put({
        type: 'saveData',
        data: res.data,
      });

      if (callback) callback(res);
    },
    // // 获取商品编辑信息
    *editGoods({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.selectExamineProductEdit, payload);

      yield put({
        type: 'saveEditInfo',
        data: res.data,
      });

      if (callback) callback(res);
    },
    *selectExamineProductDirectBuyEdit({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.selectExamineProductDirectBuyEdit, payload);

      yield put({
        type: 'saveEditInfo',
        data: res.data,
      });

      if (callback) callback(res);
    },
    *examineProductConfirm({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.examineProductConfirm, payload);

      if (callback) callback(res);
    },
    *examineProductDirectBuyConfirm({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.examineProductDirectBuyConfirm, payload);

      if (callback) callback(res);
    },
    // //获取编辑drawer里面的类目数据
    *editGoodsCategory({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.findCategories);
      yield put({
        type: 'saveEditSelect',
        data: res.data,
      });
      if (callback) callback(res);
    },

    // // 获取商品详细信息
    *getGoodsInfo({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.selectExamineProductDetail, payload);

      yield put({
        type: 'saveInfo',
        data: res.data,
      });

      if (callback) callback(res);
    },
    *selectExamineProductDirectDetail({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.selectExamineProductDirectDetail, payload);

      yield put({
        type: 'saveInfo',
        data: res.data,
      });

      if (callback) callback(res);
    },
    // // 获取商品类目编辑数据
    // *getSelect({ payload, callback }, { put, call }) {
    //   let res = yield call(servicesApi.getSelectAll, payload);
    //   if (res.data.msg === '操作成功') {
    //     yield put({
    //       type: 'saveSelect',
    //       data: res.data.data,
    //     });
    //   }
    //   if (callback) callback(res);
    // },
    // //改变商品类别
    // *changSelect({ payload, callback }, { put, call }) {
    //   let res = yield call(servicesApi.handleChangSelect, payload);
    //   if (callback) callback(res);
    // },
    // //审核状态通过或拒绝
    // *changStatus({ payload, callback }, { put, call }) {
    //   let res = yield call(servicesApi.changStatus, payload);
    //   if (callback) callback(res);
    // },
    // //编辑商品
    *updateGoods({ payload, callback }, { call, back }) {
      let res = yield call(servicesApi.updateExamineProduct, payload);
      callback(res);
    },
    *updateExamineProductDirectBuy({ payload, callback }, { call, back }) {
      let res = yield call(servicesApi.updateExamineProductDirectBuy, payload);
      callback(res);
    },
  },
  reducers: {
    saveData(state, payload) {
      let _state = JSON.parse(JSON.stringify(state));
      _state.list = payload.data.records;
      return _state;
    },

    saveInfo(state, payload) {
      let _state = JSON.parse(JSON.stringify(state));
      _state.goodsInfo = payload.data;
      return _state;
    },
    saveSelect(state, payload) {
      let _state = JSON.parse(JSON.stringify(state));
      _state.selectData = payload.data;
      return _state;
    },
    saveEditInfo(state, payload) {
      console.log('payload:', payload);
      let _state = JSON.parse(JSON.stringify(state));
      _state.editInfo = payload.data;
      _state.editInfo.productImages.filter(item => {
        return (item.uid = item.id), (item.url = item.src);
      });
      _state.editInfo.uploadImg = _state.editInfo.productImages;
      return _state;
    },
    saveEditSelect(state, payload) {
      let _state = JSON.parse(JSON.stringify(state));
      _state.editSelectData = payload.data;
      return _state;
    },
    saveUploadImg(state, payload) {
      let _state = JSON.parse(JSON.stringify(state));
      _state.editInfo.uploadImg = payload.payload;
      return _state;
    },
  },
};
