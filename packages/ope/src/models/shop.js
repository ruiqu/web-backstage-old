import { router } from 'umi';
import * as shopApi from '@/services/shop';
import { message } from 'antd';

export default {
  namespace: 'shop',
  state: {
    shopList: [],
    shopTotal: 1,
    InfoData: {},
    shopEnterpriseInfos: {},
    shopEnterpriseCertificates: [],
  },
  effects: {
    *list({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.shopList, payload);
      yield put({
        type: 'listData',
        payload: response,
      });
    },
    *exportShop({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.exportShop, payload);
      location.href = response.data;
      message.warning('导出成功～');
    },
    *selectBusShopInfo({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.selectBusShopInfo, payload);
      yield put({
        type: 'shopInfoData',
        payload: response,
      });
    },
    *toShopExamineConform({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.toShopExamineConform, payload);
      message.warning('提交成功～');
      router.push('/StoreAudit/list');
    },
  },
  reducers: {
    listData(state, { payload }) {
      return {
        ...state,
        shopList: payload.data.records,
        shopTotal: payload.data.total,
      };
    },
    shopInfoData(state, { payload }) {
      console.log('payload:', payload);
      return {
        ...state,
        InfoData: payload.data.shop,
        shopEnterpriseInfos: payload.data.enterpriseInfoDto.shopEnterpriseInfos,
        shopEnterpriseCertificates: payload.data.enterpriseInfoDto.shopEnterpriseCertificates,
      };
    },
  },
};
