import { router } from 'umi';
import * as shopApi from '@/services/batch';
import { message } from 'antd';

export default {
  namespace: 'batch',
  state: {
    List: [],
    Total: 1,
    EditData: {},
  },
  effects: {
    *queryTribeCommentPage({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.queryTribeCommentPage, payload);
      yield put({
        type: 'listData',
        payload: response,
      });
      if (callback) callback(response);
    },
    *queryProductEvaluationPage({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.queryProductEvaluationPage, payload);
      yield put({
        type: 'listData',
        payload: response,
      });
    },
    *queryAppletsCommentPage({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.queryAppletsCommentPage, payload);
      if (callback) callback(response);
    },
    *queryOrderComplaintsPage({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.queryOrderComplaintsPage, payload);
      if (callback) callback(response);
    },

    *queryOrderComplaintsDetail({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.queryOrderComplaintsDetail, payload);
      if (callback) callback(response);
    },
    *modifyOrderComplaints({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.modifyOrderComplaints, payload);
      if (callback) callback(response);
    },
    *getOrderComplaintsTypes({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.getOrderComplaintsTypes, payload);
      if (callback) callback(response);
    },
    *modifyAppletsComment({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.modifyAppletsComment, payload);
      if (callback) callback(response);
    },
    *selectTribeCommentEdit({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.selectTribeCommentEdit, payload);
      yield put({
        type: 'Edit',
        payload: response,
      });
    },
    *selectExamineProductEdit({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.selectExamineProductEdit, payload);
      yield put({
        type: 'Edit',
        payload: response,
      });
    },
    *confirmTribeCommment({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.confirmTribeCommment, payload);
      if (callback) callback(response);
    },
    *confirmProductEvaluation({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.confirmProductEvaluation, payload);
      if (callback) callback(response);
    },
    *batchEffectUserComment({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.batchEffectUserComment, payload);
      if (callback) callback(response);
    },
    *batchEffectProductEvaluation({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.batchEffectProductEvaluation, payload);
      if (callback) callback(response);
    },
    *exportUserComment({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.exportUserComment, payload);
      location.href = response.data;
      // if (callback) callback(response);
    },
    *exportProductEvaluation({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.exportProductEvaluation, payload);
      location.href = response.data;
      // if (callback) callback(response);
    },
    *importUserComment({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.importUserComment, payload);
      if (callback) callback(response);
    },
    *importProductEvaluation({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.importProductEvaluation, payload);
      if (callback) callback(response);
    },
    *deleteUserComment({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.deleteUserComment, payload);
      if (callback) callback(response);
      message.success('删除成功～');
    },
    *deleteProductEvaluation({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.deleteProductEvaluation, payload);
      if (callback) callback(response);
      message.success('删除成功～');
    },
    *exportUserCommentMessage({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.exportUserCommentMessage, payload);
      if (callback) callback(response);
    },
    *exportProductEvaluationMessage({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.exportProductEvaluationMessage, payload);
      if (callback) callback(response);
    },
  },
  reducers: {
    listData(state, { payload }) {
      return {
        ...state,
        List: payload.data.records,
        Total: payload.data.total,
      };
    },
    Edit(state, { payload }) {
      return {
        ...state,
        EditData: payload.data,
      };
    },
    // shopInfoData(state, { payload }) {
    //   console.log('payload:', payload);
    //   return {
    //     ...state,
    //     InfoData: payload.data.shop,
    //     shopEnterpriseInfos: payload.data.enterpriseInfoDto.shopEnterpriseInfos,
    //     shopEnterpriseCertificates: payload.data.enterpriseInfoDto.shopEnterpriseCertificates,
    //   };
    // },
  },
};
