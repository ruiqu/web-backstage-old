import { stringify } from 'querystring';
import { router } from 'umi';
import {
  Login,
  getFakeCaptcha,
  homeData,
  queryOrderReport,
  selectProductCounts,
  sendValidateCode,
  restPassword,
  getUserStatistics,
} from '@/services/login';
import { setAuthority } from '@/utils/authority';
import { getPageQuery } from '@/utils/utils';
import { reloadAuthorized } from '@/utils/Authorized';
import { setToken, setShopId, setShopAdminId, setCurrentUser } from '../utils/localStorage';

const Model = {
  namespace: 'login',
  state: {
    status: undefined,
  },
  effects: {
    *login({ payload }, { call, put }) {
      const response = yield call(Login, payload);
      // localStorage
      localStorage.setItem('token', response.data.token);
      localStorage.setItem('mobile', payload.mobile);
      setAuthority(response.data.menus);
      router.push('/');
    },
    *getUserStatisticsData({ payload, callback }, { call, put }) {
      const response = yield call(getUserStatistics, payload);
      if (callback && typeof callback === 'function') {
        callback(response);
      }
    },
    *home({ payload, callback }, { call, put }) {
      const response = yield call(homeData, payload);
      if (callback && typeof callback === 'function') {
        callback(response);
      }
    },
    *sendValidateCodeData({ payload, callback }, { call, put }) {
      const response = yield call(sendValidateCode, payload);
      if (callback && typeof callback === 'function') {
        callback(response);
      }
    },
    *restPasswordData({ payload, callback }, { call, put }) {
      const response = yield call(restPassword, payload);
      if (callback && typeof callback === 'function') {
        callback(response);
      }
    },
    *selectProduct({ payload, callback }, { call, put }) {
      const response = yield call(selectProductCounts, payload);
      if (callback && typeof callback === 'function') {
        callback(response);
      }
    },
    *queryOrder({ payload, callback }, { call, put }) {
      const response = yield call(queryOrderReport, payload);
      if (callback && typeof callback === 'function') {
        callback(response);
      }
    },

    *getCaptcha({ payload }, { call }) {
      yield call(getFakeCaptcha, payload);
    },

    logout() {
      const { redirect } = getPageQuery(); // Note: There may be security issues, please note

      if (window.location.pathname !== '/user/login' && !redirect) {
        router.replace({
          pathname: '/user/login',
          search: stringify({
            redirect: window.location.href,
          }),
        });
      }
    },
  },
  reducers: {
    changeLoginStatus(state, { payload }) {
      console.log(payload);
      setToken(payload.token);
      setAuthority(payload.token);
      setCurrentUser(payload.user);
      return { ...state, status: payload.status, type: payload.type };
    },
  },
};
export default Model;
