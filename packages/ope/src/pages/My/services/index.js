import request from "@/services/baseService";

export default {
  queryOpeMyServicePage:(data)=>{
    return request(`/hzsx/myConfig/queryOpeMyServicePage`, data)
  },
  modifyOpeMyService:(data)=>{
    return request(`/hzsx/myConfig/modifyOpeMyService`, data)
  },
  deleteOpeMyService:(data)=>{
    return request(`/hzsx/myConfig/deleteOpeMyService`, data)
  },
  addOpeMyService:(data)=>{
    return request(`/hzsx/myConfig/addOpeMyService`, data)
  },
  queryOpeMyOrderPage:(data)=>{
    return request(`/hzsx/myConfig/queryOpeMyOrderPage`, data)
  },
  modifyOpeMyOrder:(data)=>{
    return request(`/hzsx/myConfig/modifyOpeMyOrder`, data)
  },
  deleteMyOrder:(data)=>{
    return request(`/hzsx/myConfig/deleteMyOrder`, data)
  },
  addOpeMyOrder:(data)=>{
    return request(`/hzsx/myConfig/addOpeMyOrder`, data)
  },
  queryOpeMyToolPage:(data)=>{
    return request(`/hzsx/myConfig/queryOpeMyToolPage`, data)
  },
  modifyOpeMyTool:(data)=>{
    return request(`/hzsx/myConfig/modifyOpeMyTool`, data)
  },
  deleteMyTool:(data)=>{
    return request(`/hzsx/myConfig/deleteMyTool`, data, 'get')
  },
  addOpeMyTool:(data)=>{
    return request(`/hzsx/myConfig/addOpeMyTool`, data)
  },
 
}
