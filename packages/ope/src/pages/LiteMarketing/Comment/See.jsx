import React, { PureComponent } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import { Card, Descriptions, Spin, message, Row, Col, Modal, Button, Input, Form } from 'antd';
const { TextArea } = Input;
import { router } from 'umi';
import CustomCard from '@/components/CustomCard';
import { getParam } from '@/utils/utils';
@connect(({ batch }) => ({
  ...batch,
}))
@Form.create()
class GoodsDetail extends PureComponent {
  state = {
    loading: true,
    visible: false,
    imgSrc: [],
    imgLength: 0,
    current: 0,
    categoryVisible: false,
    categoryId: null,
    actionVisible: false,
    confirmLoading: false,
    tittle: '',
    status: 0,
    reason: null,
    isScale: [],
    data: {},
  };

  componentDidMount() {
    this.props.dispatch({
      type: 'batch/queryOrderComplaintsDetail',
      payload: { id: getParam('id') },
      callback: res => {
        this.setState({
          data: res.data,
        });
      },
    });
  }

  viewImg = (data, index) => {
    this.setState({
      visible: true,
      imgSrc: data,
      imgLength: data.length,
      current: index,
    });
  };
  // 图片modal的关闭
  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };
  // 下一张
  next = () => {
    let a = this.state.current;
    a += 1;
    this.setState({
      current: a,
    });
  };
  // 上一张
  prev = () => {
    let a = this.state.current;
    a -= 1;
    this.setState({
      current: a,
    });
  };
  submit = () => {
    this.props.form.validateFields((err, value) => {
      if (!err) {
        this.props.dispatch({
          type: 'batch/modifyOrderComplaints',
          payload: { result: value.result, id: getParam('id') },
          callback: res => {
            if (res.data) {
              message.success('提交成功～');
              router.push('/Marketing/Comment?See=See');
            }
          },
        });
      }
    });
  };
  render() {
    const { imgSrc, visible, current, imgLength, data } = this.state;

    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 1 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
    };

    return (
      <PageHeaderWrapper>
        <Spin spinning={false}>
          <Card bordered={false} style={{ marginTop: 20 }}>
            <Descriptions title={<CustomCard title="投诉详情" />}>
              <Descriptions.Item label="投诉人">{data && data.name}</Descriptions.Item>
              <Descriptions.Item label="订单号">
                <a href={`/ope/#/Order/Details?id=${data && data.orderId}`} target="_blank">
                  {data && data.orderId}
                </a>
              </Descriptions.Item>
              <Descriptions.Item label="联系电话">{data && data.telphone}</Descriptions.Item>
              <Descriptions.Item label="涉诉类型">{data && data.typeName}</Descriptions.Item>
              <Descriptions.Item label="涉诉商户">{data && data.shopName}</Descriptions.Item>
              <Descriptions.Item label="投诉时间">{data && data.createTime}</Descriptions.Item>
              <Descriptions.Item label="投诉内容" span={3}>
                {data && data.content}
              </Descriptions.Item>
              <Descriptions.Item label="客户上传证据" span={3}>
                <Row type="flex">
                  {data &&
                    data.images &&
                    data.images.map((item, index) => {
                      return (
                        <Col style={{ marginLeft: 10 }} key={item.id}>
                          <img
                            onClick={() => this.viewImg(data.images, index)}
                            style={{ width: 85, height: 85 }}
                            src={item}
                            alt=""
                          />
                        </Col>
                      );
                    })}
                </Row>
              </Descriptions.Item>
            </Descriptions>
          </Card>
          <Card bordered={false} style={{ marginTop: 20, marginBottom: 30 }}>
            <CustomCard title="投诉处理" style={{ marginTop: 30 }} />
            <Form {...formItemLayout} style={{ marginTop: 20 }}>
              <Form.Item label="备注">
                {getFieldDecorator('result', {
                  initialValue: (data && data.result) || undefined,
                })(<TextArea placeholder="请输入处理详情" style={{ height: 100 }} />)}
              </Form.Item>
              <Form.Item wrapperCol={{ span: 24 }}>
                <Button type="primary" onClick={this.submit}>
                  提交
                </Button>
              </Form.Item>
            </Form>
          </Card>
        </Spin>

        <Modal
          visible={visible}
          footer={
            <div style={{ textAlign: 'center' }}>
              <Button disabled={current <= 0 ? true : false} onClick={this.prev}>
                上一张
              </Button>
              <Button
                disabled={current >= imgLength - 1 ? true : false}
                type="primary"
                onClick={this.next}
              >
                下一张
              </Button>
            </div>
          }
          onCancel={this.handleCancel}
          width={600}
        >
          <img
            src={imgSrc.length > 0 ? imgSrc[current] : null}
            alt=""
            style={{ width: '100%', paddingTop: 15 }}
          />
        </Modal>
      </PageHeaderWrapper>
    );
  }
}

export default GoodsDetail;
