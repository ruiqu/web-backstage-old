import React, { useState, useContext, useEffect, useCallback, useMemo } from 'react';
import { connect } from 'dva';
import { Spin, Card, Table, Descriptions, Form, Input, Icon, Checkbox, Button, Select } from 'antd';
import { PageHeaderWrapper } from '@ant-design/pro-layout';

import { getParam } from '@/utils/utils';
import axios from 'axios';
function getToken() {
  const token = localStorage.getItem('token');
  return token;
}
// import { baseUrl } from '@/config/index';
const { Option } = Select;


const pageSize = 10;

let ToView = props => {
  const [detailDat, setdetailDat] = useState({});
  const [current, setcurrent] = useState(1);
  useEffect(() => {
    axios(`/hzsx/liteCouponTemplate/getDetail?id=${getParam('id')}`, {
      method: 'get',
      headers: { token: getToken() },
    }).then(res => {
      setdetailDat(res.data.data);
    });
  }, []);
  useEffect(() => {
    props.dispatch({
      type: 'coupons/liteDetailBindList',
      payload: {
        pageNumber: 1,
        pageSize,
        templateId: parseInt(getParam('id')),
      },
    });
  }, []);

  const handleSubmit = e => {
    e.preventDefault();
    setcurrent(1) // 初始化第一页
    props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        const fetchParams = {
          pageNumber: 1,
          pageSize,
          templateId: parseInt(getParam('id')),
        };
        values.status && (fetchParams.status = values.status);
        values.orderId && (fetchParams.orderId = values.orderId);
        props.dispatch({
          type: 'coupons/detailBindList',
          payload: fetchParams,
        });
      }
    });
  };
  // 下载csv
  const downloadCSV = () => {
    props.dispatch({
      type: 'coupons/liteExportEntityNum',
      payload: {
        id: parseInt(getParam('id')),
      },
    });
  };

  const onChange = e => {
    const fetchData = {
      pageNumber: e.current,
      pageSize,
      templateId: parseInt(getParam('id')),
    }; // 提交数据
    const usingStatusObj = props.form.getFieldsValue(['status']) || {}; // 使用状态
    const orderNumberObj = props.form.getFieldsValue(['orderId']) || {}; // 订单编号
    usingStatusObj.status && (fetchData.status = usingStatusObj.status);
    orderNumberObj.orderId && (fetchData.orderId = orderNumberObj.orderId);
    props.dispatch({
      type: 'coupons/detailBindList',
      payload: fetchData,
    });
    setcurrent(e.current);
  };

  // 重置的时候触发
  const resetHandler = () => {
    props.form.setFieldsValue({
      status: undefined,
      orderId: '',
    }); // 初始化搜索数据
    setcurrent(1); // 初始化为第一页

    props.dispatch({
      type: 'coupons/detailBindList',
      payload: {
        templateId: parseInt(getParam('id')),
        pageSize,
      }
    });
  };

  const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = props.form;
  const columns = [
    {
      title: '优惠券',
      dataIndex: 'code',
      key: 'code',
    },
    {
      title: '手机号',
      dataIndex: 'phone',
      key: 'phone',
    },
    {
      title: '领取方式',
      dataIndex: 'receiveType',
      key: 'receiveType',
      render: receiveType => {
        return <div>{receiveType === 'ASSIGN' ? '系统派发' : '用户主动领取'}</div>;
      },
    },
    {
      title: '领取时间',
      dataIndex: 'receiveTime',
      key: 'receiveTime',
      render: receiveTime => {
        return <div>{receiveTime ? receiveTime : 'N/A'}</div>;
      },
    },
    {
      title: '领取状态',
      dataIndex: 'status',
      key: 'status',
      render: status => {
        return <div>{status === 'UNUSED' ? '未使用' : '已使用'}</div>;
      },
    },
    {
      title: '使用时间',
      dataIndex: 'useTime',
      key: 'useTime',
      render: useTime => {
        return <div>{useTime ? useTime : 'N/A'}</div>;
      },
    },
    {
      title: '订单编号',
      dataIndex: 'orderId',
      key: 'orderId',
      render: orderId => {
        return <div>{orderId ? orderId : 'N/A'}</div>;
      },
    },
  ];

  return (
    <PageHeaderWrapper title="查看优惠券">
      <Spin spinning={false}>
        <Card>
          <Descriptions bordered layout="vertical" column={6}>
            <Descriptions.Item label="标题">{detailDat.title}</Descriptions.Item>
            <Descriptions.Item label="优惠券类型">
              {detailDat.scene === 'RENT' ? '租赁' : '买断'}
            </Descriptions.Item>
            <Descriptions.Item label="优惠券适用范围">
              {detailDat.rangeType === 'ALL' ? '全场通用' : null}
              {detailDat.rangeType === 'PRODUCT' ? '商品' : '类目'}
            </Descriptions.Item>
            <Descriptions.Item label="使用条件">
              {detailDat.minAmount === 0 ? '不限制' : detailDat.minAmount}
            </Descriptions.Item>
            <Descriptions.Item label="面额">{detailDat.discountAmount}</Descriptions.Item>
            <Descriptions.Item label="优惠券状态">
              {detailDat.status === 'RUN_OUT' ? '已经领取完' : null}
              {detailDat.status === 'VALID' ? '有效' : null}
              {detailDat.status === 'INVALID' ? '无效' : null}
            </Descriptions.Item>
            <Descriptions.Item label="时间设置">
              {detailDat.delayDayNum ? (
                `自领取${detailDat.delayDayNum}天内使用`
              ) : (
                <div>
                  <div>开始：{detailDat.startTime}</div>
                  <div>结束：{detailDat.endTime}</div>
                </div>
              )}
            </Descriptions.Item>
            <Descriptions.Item label="发放总量">{detailDat.num}</Descriptions.Item>
            <Descriptions.Item label="已领取">{detailDat.bindNum}</Descriptions.Item>
            <Descriptions.Item label="待领取">{detailDat.leftNum}</Descriptions.Item>
            <Descriptions.Item label="已使用">{detailDat.usedNum}</Descriptions.Item>
            <Descriptions.Item label="未使用">{detailDat.unusedNum}</Descriptions.Item>
          </Descriptions>
          {
            detailDat.type === 'ALIPAY' && detailDat.status === 'VALID'
              ? (
                <Button
                  type="primary"
                  style={{ marginTop: 20, marginLeft: '50%', transform: 'translateX(-50%)' }}
                  onClick={ downloadCSV }
                >
                  下载券码券csv文件
                </Button>
              )
              : ''
          }
        </Card>
        <Card style={{ marginTop: 20 }}>
          <Descriptions title="数据列表" />
          <Form className="login-form" layout="inline" onSubmit={handleSubmit}>
            <Form.Item label="使用状态">
              {getFieldDecorator(
                'status',
                {},
              )(
                <Select placeholder="请选择" style={{ width: 180 }}>
                  <Option value="">全部</Option>
                  <Option value="USED">已使用</Option>
                  <Option value="UNUSED">未使用</Option>
                </Select>,
              )}
            </Form.Item>
            <Form.Item label="订单编号">
              {getFieldDecorator('orderId', {})(<Input placeholder="请输入订单编号" />)}
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit" className="login-form-button">
                查询
              </Button>
            </Form.Item>
            <Form.Item>
              <Button onClick={resetHandler} type="default" className="login-form-button">
                重置
              </Button>
            </Form.Item>
          </Form>
          <Table
            rowKey="key"
            columns={columns}
            dataSource={props.BindList}
            pagination={{
              current: current,
              pageSize,
              total: props.Bindtotal,
            }}
            style={{
              marginTop: 20,
            }}
            onChange={onChange}
          />
        </Card>
      </Spin>
    </PageHeaderWrapper>
  );
};
const WrappedHorizontalLoginForm = Form.create()(ToView);
export default connect(({ coupons, loading }) => ({
  ...coupons,
  loading: loading.models.coupons,
}))(WrappedHorizontalLoginForm);
