import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import {
  Card,
  message,
  Tooltip,
  Button,
  Form,
  Input,
  Select,
  DatePicker,
  Radio,
  Modal,
  Descriptions,
  Divider,
  Upload,
  Icon,
  Tabs,
  Spin,
  Popconfirm,
} from 'antd';
import axios from 'axios';
import MyPageTable from '@/components/MyPageTable';
import { onTableData } from '@/utils/utils.js';
import { uploadUrl } from '@/config/index';
import IpaIndex from './ipaIndex.jsx';
function getToken() {
  const token = localStorage.getItem('token');
  return token;
}
import moment from 'moment';
const { Option } = Select;
const { RangePicker } = DatePicker;
const { TextArea } = Input;
const { TabPane } = Tabs;
import { router } from 'umi';
function callback(key) {
  console.log(key);
}
@connect(({ batch, loading }) => ({
  ...batch,
  queryTribeCommentPage: loading.effects['batch/queryTribeCommentPage'],
  queryProductEvaluationPage: loading.effects['batch/queryProductEvaluationPage'],
}))
@Form.create()
export default class ipaIndex extends Component {
  state = {
    current: 1,
    visible: false,
    title: null,
    fileLists: [],
    TabsData: '1',
    datas: {},
    channelData: [],
    dataId: {},
    Lists: [],
    linding: false,
    initValue: 0,
    channel: undefined,
  };
  componentDidMount() {
    this.onList(1, 10, { status: 0 });

    this.props.dispatch({
      type: 'order/PlateformChannel',
      payload: {},
      callback: res => {
        this.setState({
          channelData: res.data || [],
        });
      },
    });
  }
  onList = (pageNumber, pageSize, data = {}) => {
    const { dispatch } = this.props;
    this.setState({
      linding: true,
    });
    dispatch({
      type: 'batch/queryAppletsCommentPage',
      payload: {
        pageSize,
        pageNumber,
        ...data,
      },
      callback: res => {
        this.setState({
          Lists: res.data.records,
          total: res.data.total,
          linding: false,
        });
      },
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      }
    });
  };
  showModal = id => {
    this.setState({
      visible: true,
      id,
    });
  };
  onPage = e => {
    const { datas } = this.state;
    this.setState(
      {
        current: e.current,
      },
      () => {
        this.onList(e.current, 10, { ...datas });
      },
    );
  };
  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  handleOk = e => {
    e.preventDefault();
    const { id } = this.state;
    const { dispatch } = this.props;
    this.props.form.validateFields((err, values) => {
      dispatch({
        type: 'batch/modifyAppletsComment',
        payload: {
          id,
          result: values.beizhu,
        },
        callback: res => {
          console.log(res.data, 'rsadsa');
          if (res.data) {
            message.warning('编辑成功～');
            this.onList(this.state.current, 10, { ...this.state.datas });
            this.setState({
              visible: false,
            });
          }
        },
      });
    });
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 },
      },
    };
    const { Lists, total } = this.state;
    const paginationProps = {
      current: this.state.current,
      pageSize: 10,
      total: total,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / 10)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };

    const columns = [
      {
        title: '评论人',
        dataIndex: 'name',
        width: '10%',
      },
      {
        title: '手机号码',
        dataIndex: 'telphone',
        width: '10%',
      },
      {
        title: '评价',
        dataIndex: 'content',
        width: '30%',
      },
      {
        title: '渠道来源',
        dataIndex: 'channelName',
        width: '20%',
      },
      {
        title: '评分',
        dataIndex: 'starCountService',
        width: '5%',
      },
      {
        title: '评价时间',
        dataIndex: 'createTime',
        width: '10%',
      },
      {
        title: '平台处理',
        dataIndex: 'result',
        width: '5%',
        render: result => {
          return (
            <>
              {result ? (
                <Tooltip placement="top" title={result}>
                  <Button type="link">
                    已处理
                    <Icon type="message" />
                  </Button>
                </Tooltip>
              ) : (
                <Button type="link">未处理</Button>
              )}
            </>
          );
        },
      },

      {
        title: '操作',
        width: '5%',
        render: e => {
          return (
            <div>
              <span
                style={{ cursor: 'pointer', color: '#3F66F5', marginRight: 7 }}
                onClick={() => this.showModal(e.id)}
              >
                处理
              </span>
            </div>
          );
        },
      },
    ];
    return (
      <div>
        <Form layout="inline" onSubmit={this.handleSubmit}>
          <Form.Item label="渠道来源">
            {getFieldDecorator('channel', { initialValue: this.state.channel })(
              <Select placeholder="请选择渠道来源" style={{ width: 180 }} allowClear>
                  {this.state.channelData.map((item, val) => {
                    return (
                      <Option value={item.channelId} key={val}>
                        {item.channelName}
                      </Option>
                    );
                  })}
              </Select>,
            )}
          </Form.Item>
          <Form.Item label="手机号">
            {getFieldDecorator('telphone', {})(<Input placeholder="请输入手机号" allowClear />)}
          </Form.Item>
          <Form.Item label="评论状态">
            {getFieldDecorator('status', { initialValue: this.state.initValue })(
              <Select placeholder="评论状态" style={{ width: 180 }} allowClear>
                <Option value={0}>未处理</Option>
                <Option value={1}>已处理</Option>
              </Select>,
            )}
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              搜索
            </Button>
          </Form.Item>
          <Form.Item>
            <Button
              onClick={() => {
                this.onList(1, 10);
                this.props.form.resetFields();
                this.setState({
                  initValue: undefined,
                  channel: undefined,
                });
              }}
            >
              重置
            </Button>
          </Form.Item>
        </Form>
        <Spin spinning={this.state.linding}>
          <MyPageTable
            onPage={this.onPage}
            paginationProps={paginationProps}
            dataSource={onTableData(Lists)}
            columns={columns}
          />
        </Spin>
        <Modal
          title="编辑处理"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          destroyOnClose
        >
          <Form {...formItemLayout}>
            <Form.Item label="平台处理">
              {getFieldDecorator('beizhu', {})(<TextArea placeholder="请输入平台处理意见" />)}
            </Form.Item>
          </Form>
        </Modal>
      </div>
    );
  }
}
