import React, { Component } from 'react';
import { connect } from 'dva';
import { message, Tooltip, Button, Form, Input, Select, Modal, Icon, Spin } from 'antd';
import MyPageTable from '@/components/MyPageTable';
import { onTableData } from '@/utils/utils.js';
import { router } from 'umi';
import CopyToClipboard from '@/components/CopyToClipboard';
const { Option } = Select;
const { TextArea } = Input;
@connect(({ batch }) => ({
  ...batch,
}))
@Form.create()
export default class GuestComplaint extends Component {
  state = {
    current: 1,
    visible: false,
    title: null,
    fileLists: [],
    TabsData: '1',
    datas: {},
    channelData: [],
    dataId: {},
    Lists: [],
    linding: false,
    data: [],
    initValue: 0,
  };
  componentDidMount() {
    this.onList(1, 10, { status: 0 });
    this.props.dispatch({
      type: 'batch/getOrderComplaintsTypes',
      callback: res => {
        this.setState({
          data: res.data,
        });
      },
    });
  }
  onList = (pageNumber, pageSize, data = {}) => {
    const { dispatch } = this.props;
    this.setState({
      linding: true,
    });
    dispatch({
      type: 'batch/queryOrderComplaintsPage',
      payload: {
        pageSize,
        pageNumber,
        ...data,
      },
      callback: res => {
        this.setState({
          Lists: res.data.records,
          total: res.data.total,
          linding: false,
        });
      },
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      }
    });
  };

  onPage = e => {
    const { datas } = this.state;
    this.setState(
      {
        current: e.current,
      },
      () => {
        this.onList(e.current, 10, { ...datas });
      },
    );
  };
  handleCancel = e => {
    this.setState({
      visible: false,
    });
  };
  handleOk = e => {
    e.preventDefault();
    const { id } = this.state;
    const { dispatch } = this.props;
    this.props.form.validateFields((err, values) => {
      dispatch({
        type: 'batch/modifyAppletsComment',
        payload: {
          id,
          result: values.beizhu,
        },
        callback: res => {
          console.log(res.data, 'rsadsa');
          if (res.data) {
            message.warning('编辑成功～');
            this.onList(this.state.current, 10, { ...this.state.datas });
            this.setState({
              visible: false,
            });
          }
        },
      });
    });
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 },
      },
    };
    const { Lists, total } = this.state;
    const paginationProps = {
      current: this.state.current,
      pageSize: 10,
      total: total,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / 10)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };

    const columns = [
      {
        title: '投诉人',
        dataIndex: 'name',
        width: '10%',
      },
      {
        title: '订单号',
        dataIndex: 'orderId',
        width: '10%',
        render: orderId => <CopyToClipboard text={orderId} />,
      },
      {
        title: '涉诉商户',
        dataIndex: 'shopName',
        width: '10%',
      },
      {
        title: '涉诉类型',
        dataIndex: 'typeName',
        width: '10%',
      },
      {
        title: '手机号码',
        dataIndex: 'telphone',
        width: '10%',
      },

      {
        title: '投诉时间',
        dataIndex: 'createTime',
        width: '10%',
      },
      {
        title: '处理状态',
        dataIndex: 'result',
        width: '10%',
        render: result => {
          return (
            <>
              {result ? (
                <Tooltip placement="top" title={result}>
                  <Button type="link">
                    已处理
                    <Icon type="message" />
                  </Button>
                </Tooltip>
              ) : (
                <Button type="link">未处理</Button>
              )}
            </>
          );
        },
      },

      {
        title: '操作',
        width: '10%',
        render: e => {
          return (
            <div>
              {/* <span
                style={{ cursor: 'pointer', color: '#3F66F5', marginRight: 7 }}
                onClick={() => router.push(`/Marketing/Comment/See?id=${e.id}`)}
              >
                处理
              </span> */}
              <a
                className="primary-color"
                // onClick={() => router.push(`/Order/Details?id=${e.orderId}`)}
                href={`#/Marketing/Comment/See?id=${e.id}`}
                target="_blank"
              >
                处理
              </a>
            </div>
          );
        },
      },
    ];
    return (
      <div>
        <Form layout="inline" onSubmit={this.handleSubmit}>
          <Form.Item label="投诉人">
            {getFieldDecorator('name', {})(<Input placeholder="请输入投诉人" allowClear />)}
          </Form.Item>
          <Form.Item label="订单号">
            {getFieldDecorator('orderId', {})(<Input placeholder="请输入订单号" allowClear />)}
          </Form.Item>
          <Form.Item label="涉诉商户">
            {getFieldDecorator('shopName', {})(<Input placeholder="请输入涉诉商户" allowClear />)}
          </Form.Item>
          <Form.Item label="涉诉类型">
            {getFieldDecorator(
              'typeId',
              {},
            )(
              <Select placeholder="涉诉类型" style={{ width: 180 }} allowClear>
                {this.state.data &&
                  this.state.data.map((item, val) => {
                    return (
                      <Option value={item.id} key={val}>
                        {item.name}
                      </Option>
                    );
                  })}
              </Select>,
            )}
          </Form.Item>
          <Form.Item label="联系电话">
            {getFieldDecorator('telphone', {})(<Input placeholder="请输入联系电话" allowClear />)}
          </Form.Item>
          <Form.Item label="处理状态">
            {getFieldDecorator('status', {
              initialValue: this.state.initValue,
            })(
              <Select placeholder="处理状态" style={{ width: 180 }} allowClear>
                <Option value={0}>未处理</Option>
                <Option value={1}>已处理</Option>
              </Select>,
            )}
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              搜索
            </Button>
          </Form.Item>
          <Form.Item>
            <Button
              onClick={() => {
                this.onList(1, 10);
                this.props.form.resetFields();
                this.setState({
                  initValue: undefined,
                });
              }}
            >
              重置
            </Button>
          </Form.Item>
        </Form>
        <Spin spinning={this.state.linding}>
          <MyPageTable
            onPage={this.onPage}
            paginationProps={paginationProps}
            dataSource={onTableData(Lists)}
            columns={columns}
          />
        </Spin>
        <Modal
          title="编辑处理"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          destroyOnClose
        >
          <Form {...formItemLayout}>
            <Form.Item label="平台处理">
              {getFieldDecorator('beizhu', {})(<TextArea placeholder="请输入平台处理意见" />)}
            </Form.Item>
          </Form>
        </Modal>
      </div>
    );
  }
}
