import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Card, Col, Row, Descriptions, DatePicker, Radio, Divider, Icon, Button } from 'antd';
import styles from './index.less';
import CustomCard from '@/components/CustomCard';
// import { Line } from '@antv/g2plot';
import { Line, Area } from '@ant-design/charts';
import { getTimeDistance } from '@/utils/utils';
import { connect } from 'dva';
import { router } from 'umi';
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const dateFormat = 'YYYY-MM-DD';
@connect(({ login }) => ({ ...login }))
export default class Home extends Component {
  state = {
    rangeTimeData: getTimeDistance('week'),
    StatisticsData: getTimeDistance('week'),
    dataDat: {},
    StaData: [],
    moneyData: [],
    zhongl: {},
    yhzhongl: {},
  };
  componentWillUnmount = () => {
    this.setState = (state,callback)=>{
      return;
    };
}
  componentDidMount() {
    var day1 = new Date();
    day1.setTime(day1.getTime() - 24 * 60 * 60 * 1000);
    var s1 = day1.getFullYear() + '-' + (day1.getMonth() + 1) + '-' + day1.getDate();
    console.log(s1, 's1s1s1s1');
    console.log(this.state.rangeTimeData, 'rangeTimeData');
    const { dispatch } = this.props;
    dispatch({
      type: 'login/home',
      payload: {},
      callback: res => {
        this.setState({
          dataDat: res.data,
          StaData: res.data.orderReportDtoMonthList,
          moneyData: res.data.orderReportDtoMonthList,
        });
      },
    });
    dispatch({
      type: 'login/selectProduct',
      payload: {},
      callback: res => {
        this.setState({
          zhongl: res.data,
        });
      },
    });
    dispatch({
      type: 'login/getUserStatisticsData',
      payload: {},
      callback: res => {
        this.setState({
          yhzhongl: res.data,
        });
      },
    });
  }
  onTime = e => {
    this.setState(
      {
        rangeTimeData: getTimeDistance(e.target.value),
      },
      () => {
        const { dispatch } = this.props;
        dispatch({
          type: 'login/queryOrder',
          payload: {
            orderStatisticsStartDate: getTimeDistance(e.target.value)[0],
            orderStatisticsEndDate: getTimeDistance(e.target.value)[1],
          },
          callback: res => {
            this.setState({
              StaData: res.data,
            });
          },
        });
      },
    );
  };
  onSits = e => {
    this.setState(
      {
        rangeTimeData: e,
      },
      () => {
        const { dispatch } = this.props;
        dispatch({
          type: 'login/queryOrder',
          payload: {
            orderStatisticsStartDate: e[0],
            orderStatisticsEndDate: e[1],
          },
          callback: res => {
            this.setState({
              StaData: res.data,
            });
          },
        });
      },
    );
  };
  onTimes = e => {
    this.setState(
      {
        StatisticsData: getTimeDistance(e.target.value),
      },
      () => {
        const { dispatch } = this.props;
        dispatch({
          type: 'login/queryOrder',
          payload: {
            orderStatisticsStartDate: getTimeDistance(e.target.value)[0],
            orderStatisticsEndDate: getTimeDistance(e.target.value)[1],
          },
          callback: res => {
            this.setState({
              moneyData: res.data,
            });
          },
        });
      },
    );
  };
  onMoney = e => {
    this.setState(
      {
        StatisticsData: e,
      },
      () => {
        const { dispatch } = this.props;
        dispatch({
          type: 'login/queryOrder',
          payload: {
            orderStatisticsStartDate: e[0],
            orderStatisticsEndDate: e[1],
          },
          callback: res => {
            this.setState({
              moneyData: res.data,
            });
          },
        });
      },
    );
  };
  render() {
    const { StaData, moneyData, zhongl, yhzhongl } = this.state;
    var keyMap = {
      successOrderCount: '订单数',
      successOrderRent: '租金',
    };
    if (StaData) {
      for (var i = 0; i < StaData.length; i++) {
        var obj = StaData[i];
        for (var key in obj) {
          var newKey = keyMap[key];
          if (newKey) {
            obj[newKey] = obj[key];
            delete obj[key];
          }
        }
      }
    }
    if (moneyData) {
      for (var i = 0; i < moneyData.length; i++) {
        var obj = moneyData[i];
        for (var key in obj) {
          var newKey = keyMap[key];
          if (newKey) {
            obj[newKey] = obj[key];
            delete obj[key];
          }
        }
      }
    }

    const config = {
      width: 200,

      title: {
        visible: true,
        text: '订单统计',
      },

      padding: 'auto',
      forceFit: true,
      data: this.state.StaData,
      xField: 'statisticsDate',
      yField: '订单数',
      yAxis: {
        label: {
          formatter: v => `${v}`.replace(/\d{1,3}(?=(\d{3})+$)/g, s => `${s},`),
        },
      },
      label: {
        visible: true,
        type: 'point',
      },
      point: {
        visible: true,
        size: 5,
        shape: 'diamond',
        style: {
          fill: 'white',
          stroke: '#2593fc',
          lineWidth: 2,
        },
      },
    };
    const configs = {
      width: 200,

      title: {
        visible: true,
        text: '租金总额（¥）',
      },

      padding: 'auto',
      forceFit: true,
      data: moneyData,
      xField: 'statisticsDate',
      yField: '租金',
      yAxis: {
        label: {
          formatter: v => `${v}`.replace(/\d{1,3}(?=(\d{3})+$)/g, s => `${s},`),
        },
      },
      label: {
        visible: true,
        type: 'point',
      },
      point: {
        visible: true,
        size: 5,
        shape: 'diamond',
        style: {
          fill: 'white',
          stroke: '#2593fc',
          lineWidth: 2,
        },
      },
    };
    const { rangeTimeData, StatisticsData, dataDat } = this.state;
    return (
      <PageHeaderWrapper>
        <Row gutter={16}>
          <Col span={6}>
            <Card
              title={
                <div className={styles.title}>
                  <img
                    className={styles.imgIk}
                    src="https://booleandata-crmmanagement-front.oss-cn-beijing.aliyuncs.com/operate/Group%2029%402x.png"
                  />
                  今日订单总数
                </div>
              }
              bordered={false}
            >
              {dataDat && dataDat.todayTotalOrderCount}
            </Card>
          </Col>
          <Col span={6}>
            <Card
              title={
                <div className={styles.title}>
                  <img
                    className={styles.imgIk}
                    src="https://booleandata-crmmanagement-front.oss-cn-beijing.aliyuncs.com/operate/pay-circle%402x.png"
                  />
                  今日下单总租金
                </div>
              }
              bordered={false}
            >
              ¥{dataDat && dataDat.todayTotalOrderRentAmount}
            </Card>
          </Col>
          <Col span={6}>
            <Card
              title={
                <div className={styles.title}>
                  <img
                    className={styles.imgIk}
                    src="https://booleandata-crmmanagement-front.oss-cn-beijing.aliyuncs.com/operate/pay-circle%402x%20%281%29.png"
                  />
                  昨日下单总租金
                </div>
              }
              bordered={false}
            >
              ¥{dataDat && dataDat.yesterdayTotalRentAmount}
            </Card>
          </Col>
          <Col span={6}>
            <Card
              title={
                <div className={styles.title}>
                  <img
                    className={styles.imgIk}
                    src="https://booleandata-crmmanagement-front.oss-cn-beijing.aliyuncs.com/operate/pay-circle%402x%20%282%29.png"
                  />
                  近7日下单总租金
                </div>
              }
              bordered={false}
            >
              ¥{dataDat && dataDat.weekOrderRent}
            </Card>
          </Col>
        </Row>
        <div
          className={styles.content}
          style={{
            padding: 0,
          }}
        >
          <Card title={<CustomCard title="待处理事务" />} bordered={false}>
            <Descriptions>
              <Descriptions.Item label="待支付订单">
                <a onClick={() => router.push('/Order/HomePage?status=01')}>
                  {dataDat.unPayOrderCount}
                </a>
              </Descriptions.Item>
              <Descriptions.Item label="租用中订单">
                <a onClick={() => router.push('/Order/HomePage?status=06')}>
                  {dataDat.rentingOrderCount}
                </a>
              </Descriptions.Item>
              <Descriptions.Item label="已取消订单">
                <a onClick={() => router.push('/Order/HomePage?status=10')}>
                  {dataDat.closeOrderCount}
                </a>
              </Descriptions.Item>
              <Descriptions.Item label="待审批订单">
                <a onClick={() => router.push('/Order/HomePage?status=11')}>{dataDat.telephoneAuditOrderCount}</a>
              </Descriptions.Item>
              <Descriptions.Item label="待发货订单">
                <a onClick={() => router.push('/Order/HomePage?status=04')}>
                  {dataDat.pendingOrderCount}
                </a>
              </Descriptions.Item>
              <Descriptions.Item label="待结算订单">
                <a onClick={() => router.push('/Order/HomePage?status=07')}>
                  {dataDat.waitingSettlementOrderCount}
                </a>
              </Descriptions.Item>
              <Descriptions.Item label="待收货订单">
                <a onClick={() => router.push('/Order/HomePage?status=05')}>
                  {dataDat.waitingConfirmOrderCount}
                </a>
              </Descriptions.Item>
              <Descriptions.Item label="已逾期订单">
                <a onClick={() => router.push('/Order/BeOverdue')}>{dataDat.overDueOrderCount}</a>
              </Descriptions.Item>
              <Descriptions.Item label="已完成订单">
                <a onClick={() => router.push('/Order/HomePage?status=09')}>
                  {dataDat.finishOrderCount}
                </a>
              </Descriptions.Item>
            </Descriptions>
          </Card>
        </div>
        <Row
          gutter={16}
          style={{
            marginTop: 30,
          }}
        >
          <Col span={12}>
            <Card title={<CustomCard title="商品总览" />} bordered={false}>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                }}
              >
                <div
                  style={{
                    textAlign: 'center',
                  }}
                >
                  <div
                    style={{
                      fontSize: 12,
                      color: 'rgba(0,0,0,0.45)',
                    }}
                  >
                    未上架
                  </div>
                  <div
                    style={{
                      fontSize: 24,
                      fontWeight: 500,
                      color: 'rgba(0,0,0,0.65)',
                    }}
                  >
                    {zhongl && zhongl.notPutOnCounts}
                  </div>
                </div>
                <div
                  style={{
                    textAlign: 'center',
                  }}
                >
                  <div
                    style={{
                      fontSize: 12,
                      color: 'rgba(0,0,0,0.45)',
                    }}
                  >
                    已上架
                  </div>
                  <div
                    style={{
                      fontSize: 24,
                      fontWeight: 500,
                      color: 'rgba(0,0,0,0.65)',
                    }}
                  >
                    {zhongl && zhongl.putOnCounts}
                  </div>
                </div>
                <div
                  style={{
                    textAlign: 'center',
                  }}
                >
                  <div
                    style={{
                      fontSize: 12,
                      color: 'rgba(0,0,0,0.45)',
                    }}
                  >
                    全部商品
                  </div>
                  <div
                    style={{
                      fontSize: 24,
                      fontWeight: 500,
                      color: 'rgba(0,0,0,0.65)',
                    }}
                  >
                    {zhongl && zhongl.allCounts}
                  </div>
                </div>
              </div>
            </Card>
          </Col>
          <Col span={12}>
            <Card title={<CustomCard title="用户总览" />} bordered={false}>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                }}
              >
                <div
                  style={{
                    textAlign: 'center',
                  }}
                >
                  <div
                    style={{
                      fontSize: 12,
                      color: 'rgba(0,0,0,0.45)',
                    }}
                  >
                    今日新增
                  </div>
                  <div
                    style={{
                      fontSize: 24,
                      fontWeight: 500,
                      color: 'rgba(0,0,0,0.65)',
                    }}
                  >
                    {yhzhongl && yhzhongl.todayIncrease}
                  </div>
                </div>
                <div
                  style={{
                    textAlign: 'center',
                  }}
                >
                  <div
                    style={{
                      fontSize: 12,
                      color: 'rgba(0,0,0,0.45)',
                    }}
                  >
                    昨日新增
                  </div>
                  <div
                    style={{
                      fontSize: 24,
                      fontWeight: 500,
                      color: 'rgba(0,0,0,0.65)',
                    }}
                  >
                    {yhzhongl && yhzhongl.yesterdayIncrease}
                  </div>
                </div>
                <div
                  style={{
                    textAlign: 'center',
                  }}
                >
                  <div
                    style={{
                      fontSize: 12,
                      color: 'rgba(0,0,0,0.45)',
                    }}
                  >
                    本月新增
                  </div>
                  <div
                    style={{
                      fontSize: 24,
                      fontWeight: 500,
                      color: 'rgba(0,0,0,0.65)',
                    }}
                  >
                    {yhzhongl && yhzhongl.monthIncrease}
                  </div>
                </div>
              </div>
            </Card>
          </Col>
        </Row>
        <Card
          title={
            <div className={styles.tiem}>
              <CustomCard title="订单统计" />
              <div>
                <Radio.Group
                  defaultValue="week"
                  buttonStyle="solid"
                  onChange={this.onTime}
                  style={{
                    marginRight: 20,
                  }}
                >
                  <Radio.Button value="week">本周</Radio.Button>
                  <Radio.Button value="month">本月</Radio.Button>
                </Radio.Group>
                <RangePicker allowClear={false} value={rangeTimeData} onChange={this.onSits} />
              </div>
            </div>
          }
          style={{
            margin: '30px 0',
          }}
        >
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
            }}
          >
            <div
              style={{
                paddingRight: 30,
                marginTop: 53,
              }}
            >
              <div
                style={{
                  display: 'flex',
                  marginBottom: 14,
                }}
              >
                <div
                  style={{
                    width: 200,
                    fontSize: 14,
                    fontWeight: 400,
                    color: 'rgba(0,0,0,0.85)',
                  }}
                >
                  本月订单总数
                </div>
                <span>{dataDat && dataDat.monthOrderCount}</span>
              </div>
              <div
                style={{
                  display: 'flex',
                }}
              >
                {dataDat && dataDat.monthOrderCountRate < 0 ? (
                  <>
                    <div style={{ width: 200, color: '#000000' }}>较上月下</div>
                    <span>
                      {dataDat && dataDat.monthOrderCountRate}%{' '}
                      <Icon type="arrow-down" style={{ color: '#1890FF' }} />
                    </span>
                  </>
                ) : (
                  <>
                    <div style={{ width: 200, color: '#000000' }}>较上月上升</div>
                    <span>
                      {dataDat && dataDat.monthOrderCountRate}%
                      <Icon type="arrow-up" style={{ color: '#FA541C' }} />
                    </span>
                  </>
                )}
              </div>
              <Divider
                style={{
                  margin: '35px 0',
                }}
              />
              <div
                style={{
                  display: 'flex',
                  marginBottom: 14,
                }}
              >
                <div
                  style={{
                    width: 200,
                    fontSize: 14,
                    fontWeight: 400,
                    color: 'rgba(0,0,0,0.85)',
                  }}
                >
                  本周订单数量
                </div>
                <span>{dataDat && dataDat.weekOrderCount}</span>
              </div>
              <div
                style={{
                  display: 'flex',
                }}
              >
                {dataDat && dataDat.weekOrderCountRate < 0 ? (
                  <>
                    <div style={{ width: 200, color: '#000000' }}>较上周下降</div>
                    <span>
                      {dataDat && dataDat.weekOrderCountRate}%{' '}
                      <Icon type="arrow-down" style={{ color: '#1890FF' }} />
                    </span>
                  </>
                ) : (
                  <>
                    <div style={{ width: 200, color: '#000000' }}>较上周上升</div>
                    <span>
                      {dataDat && dataDat.weekOrderCountRate}%
                      <Icon type="arrow-up" style={{ color: '#FA541C' }} />
                    </span>
                  </>
                )}
              </div>
            </div>
            <Line
              {...config}
              style={{
                width: 800,
                height: 300,
              }}
            />
          </div>
        </Card>
        <Card
          title={
            <div className={styles.tiem}>
              <CustomCard title="租金统计" />{' '}
              <div>
                <Radio.Group
                  defaultValue="week"
                  buttonStyle="solid"
                  onChange={this.onTimes}
                  style={{
                    marginRight: 20,
                  }}
                >
                  <Radio.Button value="week">本周</Radio.Button>
                  <Radio.Button value="month">本月</Radio.Button>
                </Radio.Group>
                <RangePicker allowClear={false} value={StatisticsData} onChange={this.onMoney} />
              </div>
            </div>
          }
          style={{
            margin: '30px 0',
          }}
        >
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
            }}
          >
            <div
              style={{
                paddingRight: 30,
                marginTop: 53,
              }}
            >
              <div
                style={{
                  display: 'flex',
                  marginBottom: 14,
                }}
              >
                <div
                  style={{
                    width: 200,
                    fontSize: 14,
                    fontWeight: 400,
                    color: 'rgba(0,0,0,0.85)',
                  }}
                >
                  本月租金总额
                </div>
                <span>¥{dataDat && dataDat.monthOrderRent}</span>
              </div>
              <div
                style={{
                  display: 'flex',
                }}
              >
                {dataDat && dataDat.monthOrderRentRate < 0 ? (
                  <>
                    <div style={{ width: 200, color: '#000000' }}>较上月下降</div>
                    <span>
                      {dataDat && dataDat.monthOrderRentRate}%{' '}
                      <Icon type="arrow-down" style={{ color: '#1890FF' }} />
                    </span>
                  </>
                ) : (
                  <>
                    <div style={{ width: 200, color: '#000000' }}>较上月上升</div>
                    <span>
                      {dataDat && dataDat.monthOrderRentRate}%
                      <Icon type="arrow-up" style={{ color: '#FA541C' }} />
                    </span>
                  </>
                )}
              </div>
              <Divider
                style={{
                  margin: '35px 0',
                }}
              />
              <div
                style={{
                  display: 'flex',
                  marginBottom: 14,
                }}
              >
                <div
                  style={{
                    width: 200,
                    fontSize: 14,
                    fontWeight: 400,
                    color: 'rgba(0,0,0,0.85)',
                  }}
                >
                  本周租金总额
                </div>
                <span>¥{dataDat && dataDat.weekOrderRent}</span>
              </div>
              <div
                style={{
                  display: 'flex',
                }}
              >
                {dataDat && dataDat.weekOrderRentRate < 0 ? (
                  <>
                    <div style={{ width: 200, color: '#000000' }}>较上周下降</div>
                    <span>
                      {dataDat && dataDat.weekOrderRentRate}%{' '}
                      <Icon type="arrow-down" style={{ color: '#1890FF' }} />
                    </span>
                  </>
                ) : (
                  <>
                    <div style={{ width: 200, color: '#000000' }}>较上周上升</div>
                    <span>
                      {dataDat && dataDat.weekOrderRentRate}%
                      <Icon type="arrow-up" style={{ color: '#FA541C' }} />
                    </span>
                  </>
                )}
              </div>
            </div>
            <Area
              {...configs}
              style={{
                width: 800,
                height: 300,
              }}
            />
          </div>
        </Card>
      </PageHeaderWrapper>
    );
  }
}
