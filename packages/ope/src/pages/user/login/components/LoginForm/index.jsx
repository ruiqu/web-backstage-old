import React, { Component } from 'react';
import { Form, Icon, Input, Button, Checkbox, message } from 'antd';
import 'antd/dist/antd.css';
import styles from './index.less';
import Vcode from '@/components/Vcode';
import { router } from 'umi';
import { connect } from 'dva';
import UserService from '@/services/login';
import phoneIcon from '@/assets/icon/phone.png';
import passwordIcon from '@/assets/icon/password.png';
import codeIcon from '@/assets/icon/code.png';
import emailIcon from '@/assets/icon/email.png';
import request from '@/services/baseService';
import request2 from '@/services/baseService2';
import { encrypt } from '@/utils/crypto';

@connect(() => ({}))
class LoginForm extends Component {
  state = {
    emailCode: '',
    vcode: '-1',
    code: '',
    isTip: true,
    isLogin: true,
    isSms: 0,
    verificationImg: '',
    isSend: false,
    smsCodeData: {},
    timer: null,
    number: 60,
    phoneNumber: '',
  };
  handleSubmit = e => {
    e.preventDefault();
    if (this.state.vcode === this.state.emailCode) {
      this.props.form.validateFields((err, values) => {
        if (!err) {
          const { dispatch } = this.props;
          let smsCode = values.smsCode;
          dispatch({
            type: 'login/login',
            payload: {
              mobile: values.mobile,
              password: values.password,
              code: this.state.emailCode,
              userType: 'OPE',
              isSmsVerifyEncryp: '',
              isSmsVerifyEncrypt: encrypt(this.state.isSms, 'MoUHzS9y3o2Cif3E'),
              smsCode: smsCode,
            },
          });
        }
      });
    }
  };

  handleSubmitRegister = e => {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        UserService.register({
          mobile: values.mobile,
          password: values.password,
          code: this.state.emailCode,
          userType: 'SHOP',
        }).then(res => {
          message.success('注册成功');
          this.goLogin();
        });
      }
    });
  };

  handleEmailInput(e) {
    this.setState(
      {
        emailCode: e.target.value,
      },
      () => {
        if (this.state.emailCode === this.state.vcode) {
          this.setState({
            isTip: true,
          });
        } else if (this.state.emailCode === '') {
          this.setState({
            isTip: true,
          });
        } else if (this.state.emailCode !== this.state.vcode) {
          this.setState({
            isTip: false,
          });
        }
      },
    );
  }

  onVcode(value) {
    if (value) {
      this.setState({
        vcode: value,
      });
    }
  }

  phoneChange = e => {
    const { value } = e.target;
    this.setState({
      phoneNumber: value,
    });
  };

  phoneOnBlur = e => {
    this.props.form.validateFields(['mobile'], (err, values) => {
      if (!err) {
        console.log(123, encrypt(values.mobile, 'MoUHzS9y3o2Cif3E'),)
        request2(
          '/zyj-api-web/hzsx/aliPay/captcha/isSmsVerify',
          { phoneEncrypt: encrypt(values.mobile, 'MoUHzS9y3o2Cif3E'),type:'OPE' },
          'get',
        ).then(res => {
            this.setState({
            isSms: res,
          });
        })
      }
    });
  };

  smsSend = () => {
    this.props.form.validateFields(['mobile'], (err, values) => {
      this.setState({
        isSend: true,
      });
      this.state.timer = setInterval(() => {
        const time = this.state.number--;
        this.setState({
          time: time,
        });
        if (this.state.number < 1) {
          clearInterval(this.state.timer);
          this.setState({
            number: '60',
          });
          return this.setState({
            isSend: false,
          });
        }
      }, 1000);

      request2(
        '/zyj-api-web/hzsx/aliPay/user/sendSmsCodePaaS',
        { isCheckCaptcha: false, mobile: values.mobile, overtime: 600 },
        'get',
      ).then(res => {
        this.setState({
          smsCodeData: res || {},
        });
        message.success('发送验证成功');
      });
    });
  };

  goRegister = () => {
    const { changeTitle } = this.props;
    changeTitle('注册');
    this.setState({
      isLogin: false,
    });
  };

  goLogin = () => {
    const { changeTitle } = this.props;
    changeTitle('登录');
    this.setState({
      isLogin: true,
    });
  };

  checkConfirm = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue('password')) {
      callback('两次输入的密码不匹配');
    } else {
      callback();
    }
  };

  renderRegister = () => {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form className="login-form">
        <div className={styles.loginInput}>
          <Form.Item>
            {getFieldDecorator('mobile', {
              rules: [
                { required: true, message: '请输入手机号' },
                {
                  pattern: /^1\d{10}$/,
                  message: '手机号格式错误！',
                },
              ],
            })(
              <Input
                prefix={<Icon type="mobile" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder="请输入手机号"
              />,
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('password', {
              rules: [
                {
                  required: true,
                  message: '密码应由8-16位数字、大小写字母、符号组成',
                  pattern: /^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?\d)(?=.*?[!#@*&^$%.])[a-zA-Z\d!#@_*&^$%.]{8,16}$/,
                },
              ],
            })(
              <Input
                type="password"
                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder="请输入密码22"
              />,
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('confirm', {
              rules: [
                { required: true, message: '请输入6 - 16 位密码，区分大小写' },
                { validator: this.checkConfirm },
              ],
            })(
              <Input
                type="password"
                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder="请输入密码"
              />,
            )}
          </Form.Item>
          <div className="register-footer">
            <Button
              type="primary"
              className="register-footer-btn"
              onClick={this.handleSubmitRegister}
            >
              注册
            </Button>
            <a className="primary-color" onClick={this.goLogin}>
              使用已有帐户登录
            </a>
          </div>
        </div>
      </Form>
    );
  };

  renderLogin = () => {
    const { getFieldDecorator } = this.props.form;
    const { errorTips, isTip, isLogin } = this.state;

    return (
      <div style={{ width: 602, display: 'flex', justifyContent: 'center' }}>
        <Form
          onSubmit={this.handleSubmit}
          className="login-form"
          style={{ textAlign: 'center', width: 380 }}
        >
          <div
            style={{
              fontSize: '32px',
              color: '#333333',
              fontWeight: 800,
              textAlign: 'left',
              marginBottom: 5,
            }}
          >
            欢迎登录
          </div>
          <div className={styles.loginInput}>
            <Form.Item className={styles.itemStyle}>
              {getFieldDecorator('mobile', {
                rules: [{ required: true, message: '请输入手机号' }],
              })(
                <Input
                  className={styles.inputStyle}
                  onBlur={this.phoneOnBlur}
                  onChange={this.phoneChange}
                  prefix={
                    <img
                      src={phoneIcon}
                      alt=""
                      style={{ width: '12px', height: '17px', marginTop: '2px' }}
                    />
                  }
                  placeholder="请输入手机号"
                />,
              )}
            </Form.Item>

            {this.state.isSms === 1 ? (
              <Form.Item className={styles.itemStyle}>
                {getFieldDecorator('smsCode', {
                  rules: [{ required: true, message: '请输入短信验证码' }],
                })(
                  <Input
                    className={styles.inputStyle}
                    autocomplete="off"
                    prefix={
                      <img
                        src={emailIcon}
                        alt=""
                        style={{ width: '18px', height: '15px', marginTop: '8px' }}
                      />
                    }
                    placeholder="请输入短信验证码"
                    suffix={
                      !this.state.isSend ? (
                        <div
                          style={{
                            marginTop: '4px',
                            borderRadius: '4px',
                            width: '130px',
                            marginRight: '-8px',
                            height: '36px',
                            color: 'white',
                            textAlign: 'center',
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center',
                            cursor: 'pointer',
                            background: '#1442E3',
                          }}
                          onClick={() => this.smsSend()}
                        >
                          获取手机验证码
                        </div>
                      ) : (
                        <Button
                          style={{
                            width: '118px',
                            marginLeft: '11px',
                            height: '40px',
                            padding: '0',
                          }}
                          disabled={true}
                        >
                          {this.state.number}s
                        </Button>
                      )
                    }
                  />,
                )}
              </Form.Item>
            ) : (
              ''
            )}
            <div style={{ position: 'relative', textAlign: 'left' }}>
              <Form.Item className={styles.itemStyle}>
                {getFieldDecorator('code', {
                  rules: [{ required: true, message: '请输入验证码' }],
                  onChange: e => this.handleEmailInput(e),
                })(
                  <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <Input
                      className={styles.inputStyle}
                      prefix={
                        <img
                          src={codeIcon}
                          alt=""
                          style={{ width: '15px', height: '17px', marginTop: '2px' }}
                        />
                      }
                      autoComplete="off"
                      placeholder="请输入验证码"
                      maxLength={4}
                      suffix={
                        <Vcode
                          onChange={value => this.onVcode(value)}
                          value={this.state.code}
                          width={130}
                          height={38}
                          style={{
                            marginTop: '3px',
                            marginRight: '-8px',
                          }}
                        />
                      }
                    />
                  </div>,
                )}
              </Form.Item>

              {isTip ? null : (
                <p
                  style={{
                    position: 'absolute',
                    top: '-23px',
                    left: '8%',
                    color: 'rgb(245, 34, 45)',
                  }}
                >
                  验证码输入不正确
                </p>
              )}
            </div>
            <Form.Item>
              {getFieldDecorator('password', {
                rules: [{ required: true, message: '请输入密码' }],
              })(
                <Input
                  type="password"
                  className={styles.inputStyle}
                  prefix={
                    <img
                      src={passwordIcon}
                      alt=""
                      style={{ width: '14px', height: '17px', marginTop: '4px' }}
                    />
                  }
                  placeholder="请输入密码"
                />,
              )}
            </Form.Item>
          </div>
          <div
            style={{
              color: '#333333',
              cursor: 'pointer',
              width: '368px',
              fontSize: '14px',
              textAlign: 'right',
              margin: '10px auto 0',
            }}
            onClick={() => router.push('/user/ChangePassword')}
          >
            修改密码
          </div>

          <div className={styles.loginButton} style={{ position: 'relative' }}>
            <Form.Item wrapperCol={{ span: 24, offset: 5 }}>
              <Button
                className={styles.button}
                type="primary"
                htmlType="submit"
                disabled={this.state.vcode !== this.state.emailCode}
                style={
                  this.state.emailCode === this.state.vcode
                    ? { marginTop: '30px' }
                    : { backgroundColor: '#D3D3D3', border: 'none', marginTop: '30px' }
                }
              >
                登录{' '}
              </Button>
            </Form.Item>
          </div>
        </Form>
      </div>
    );
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { errorTips, isTip, isLogin } = this.state;
    return <div>{isLogin ? this.renderLogin() : this.renderRegister()}</div>;
  }
}
const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(LoginForm);

export default WrappedNormalLoginForm;
