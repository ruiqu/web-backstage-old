import React, { Component } from 'react';
import { connect } from 'dva';
import styles from './style.less';
import LoginForm from './components/LoginForm';
import Frame from '@/components/Frame';
import { router } from 'umi';
import { setAuthority } from '@/utils/authority';
import logoActive from '@/assets/image/logo-active.png';
import logoBot from '@/assets/image/logo-bot.png';
class Login extends Component {
  componentDidMount() {
    setAuthority(['admin']);
  }

  changeTitle = val => {
    this.setState({
      title: val,
    });
  };

  render() {
    return (
      <div className={styles.layouts}>
        <div className={styles.frameright}>
          <div>
            <img src={logoActive} alt="" srcset="" />
          </div>
          <LoginForm  changeTitle={this.changeTitle} />
          <div style={{position:'absolute',bottom:-38,right:50}}>
            <img height={81} width={60} src={logoBot} alt="" srcset="" />
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
