import React, { Component } from 'react';
import { Form, Input, Button, Row, Col, message, Icon } from 'antd';
import { router } from 'umi';
import { connect } from 'dva';
import styles from './index.less';
import phoneIcon from '@/assets/icon/phone.png';
import passwordIcon from '@/assets/icon/password.png';
import codeIcon from '@/assets/icon/code.png';
import emailIcon from '@/assets/icon/email.png';
const accountIcon = (
  <div style={{ display: 'flex', alignItems: 'center' }}>
    <img
      style={{ display: 'inlineBlock', width: '26px', height: '26px' }}
      src="https://booleandata-open.oss-cn-shanghai.aliyuncs.com/login/userIcon.png"
      alt="userIcon"
    />
    <Icon
      theme="outlined"
      style={{ fontSize: '25px', color: '#D8DADC' }}
      className={styles.icon}
      type="minus"
    />
  </div>
);

@connect(() => ({}))
class ResetForm extends Component {
  state = {
    isBotton: true,
    number: '60',
    dataMobile: {},
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log(values, 'values');
        const { dataMobile } = this.state;
        const { dispatch } = this.props;
        dispatch({
          type: 'login/restPasswordData',
          payload: {
            ...values,
            codeKey: dataMobile.codeKey,
            codeTime: dataMobile.codeTime,
            userType: 'OPE',
          },
          callback: res => {
            message.success('修改成功～');
            router.push('/user/login');
          },
        });
      }
    });
  };

  onEmailCode = () => {
    const {
      form: { getFieldValue },
    } = this.props;
    const regu = /^1\d{10}$/;
    const { dispatch } = this.props;
    dispatch({
      type: 'login/sendValidateCodeData',
      payload: {
        mobile: getFieldValue('mobile'),
        overtime:600,
        isCheckCaptcha:false
      },
      callback: res => {
        this.setState({
          dataMobile: res.data,
        });
        message.success('验证码发送成功～');
      },
    });
    if (regu.test(getFieldValue('mobile'))) {
      this.setState({
        isBotton: false,
      });
      this.state.timer = setInterval(() => {
        const time = this.state.number--;
        this.setState({
          time: time,
        });
        if (this.state.number < 1) {
          clearInterval(this.state.timer);
          this.setState({
            number: '60',
          });
          return this.setState({
            isBotton: true,
          });
        }
      }, 1000);
    } else {
      this.onError('请正确填写手机号');
    }
  };
  onError = res => {
    message.error(res);
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    const { isVerificationInput, verification } = this.state;
    return (
      <div
        style={{ width: 602, display: 'flex', justifyContent: 'center' }}
        className={styles.resetFrom}
      >
        <Form
          style={{ textAlign: 'center', width: 380 }}
          onSubmit={this.handleSubmit}
          className="login-form"
        >
          <div
            style={{
              fontSize: '32px',
              color: '#333333',
              fontWeight: 800,
              textAlign: 'left',
              marginBottom: 5,
            }}
          >
            修改密码
          </div>
          <div className={styles.loginInput}>
            <Form.Item>
              {getFieldDecorator('mobile', {
                rules: [
                  { required: true, message: '请输入手机号' },
                  {
                    pattern: /^1\d{10}$/,
                    message: '手机号格式错误！',
                  },
                ],
              })(
                <Input
                  className={styles.inputStyle}
                  prefix={
                    <img
                      src={phoneIcon}
                      alt=""
                      style={{ width: '12px', height: '17px', marginTop: '2px' }}
                    />
                  }
                  onBlur={this.handleConfirmBlur}
                  placeholder="请输入手机号"
                />,
              )}
            </Form.Item>
            {isVerificationInput ? (
              // <div className={styles.verification}>
              <Form.Item>
                {getFieldDecorator('captcha', {
                  rules: [{ required: true, message: '请输入图形验证码' }],
                })(
                  <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <Input
                      className={styles.inputStyle}
                      prefix={
                        <img src={codeIcon} alt="" style={{ width: '15px', height: '18px' }} />
                      }
                      placeholder="请输入图形验证码"
                      style={{
                        width: 250,
                      }}
                      suffix={
                        <Button
                          style={{
                            width: '108px',
                            marginLeft: '11px',
                            height: '40px',
                            padding: '0',
                            top: '2px',
                            background: 'transparent',
                            border: '0px',
                            borderRadius: '6px',
                          }}
                          onClick={this.handleConfirmBlur}
                        ></Button>
                      }
                    />
                    <img
                      width={108}
                      height={40}
                      src={verification?.imageBase64}
                      style={{ borderRadius: '6px' }}
                      onClick={() => this.handleConfirmBlur()}
                    />
                  </div>,
                )}
              </Form.Item>
            ) : (
              // </div>
              ''
            )}

            <Form.Item>
              {getFieldDecorator('code', {
                rules: [{ required: true, message: '请输入手机验证码' }],
              })(
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                  <Input
                    className={styles.inputStyle}
                    style={{
                      width: 250,
                    }}
                    prefix={
                      <img
                        src={emailIcon}
                        alt=""
                        style={{ width: '16px', height: '12px', marginTop: '2px' }}
                      />
                    }
                    placeholder="请输入手机验证码"
                  />
                  {this.state.isBotton ? (
                    <div
                      style={{
                        width: '130px',
                        marginLeft: '11px',
                        height: '40px',
                        color: 'white',
                        textAlign: 'center',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        cursor: 'pointer',
                        background:'#1442E3',
                        borderRadius:4
                      }}
                      onClick={this.onEmailCode}
                    >
                      获取手机验证码
                    </div>
                  ) : (
                    <Button
                      style={{ width: '118px', marginLeft: '11px', height: '40px', padding: '0' }}
                      disabled={true}
                    >
                      {this.state.number}s
                    </Button>
                  )}
                </div>,
              )}
            </Form.Item>

            <Form.Item>
              {getFieldDecorator('newPassword', {
                rules: [
                  {
                    required: true,
                    message: '密码应由8-16位数字、大小写字母、符号组成',
                    pattern: /^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?\d)(?=.*?[!#@*&^$%.])[a-zA-Z\d!#@_*&^$%.]{8,16}$/,
                  },
                ],
              })(
                <Input
                  className={styles.inputStyle}
                  type="password"
                  prefix={
                    <img
                      src={passwordIcon}
                      alt=""
                      style={{ width: '15px', height: '17px', marginTop: '2px' }}
                    />
                  }
                  placeholder="请输入新密码"
                />,
              )}
            </Form.Item>
          </div>
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              marginTop: 60,
            }}
          >
            <Button
              className={styles.button}
              style={{ backgroundColor: '#1442E3', width: '50%' }}
              type="primary"
              htmlType="submit"
            >
              {' '}
              确认{' '}
            </Button>
            <Button
              className={styles.button}
              style={{
                color: '#1442e3',
                border: '1px solid #1442e3',
                width: '50%',
              }}
              type="link"
              onClick={() => router.push('/user/login')}
            >
              使用已有帐户登录
            </Button>
          </div>
        </Form>
      </div>
    );
  }
}
const WrappedNormalResetForm = Form.create({ name: 'normal_login' })(ResetForm);
export default WrappedNormalResetForm;
