import React, { PureComponent } from 'react';
import {Card, Input, Form, Modal, DatePicker, Checkbox, message} from 'antd';
import { connect } from 'dva';
import {LZFormItem} from "@/components/LZForm";
import moment from "moment";

const { RangePicker } = DatePicker;

@connect()
@Form.create()
class ReportTableConfigModal extends PureComponent {
  state = {
    form: {
      channel: '',
      action: '',
      position: '',
      productId: '',
      shopName: '',
      time: null,
      channelId: [],
    },
    help: '',
    validateStatus: ''
  };

  componentDidMount() {
  }
  
  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.currentCondition !== this.props.currentCondition) {
      const nextCurrentCondition = nextProps.currentCondition
      if (nextCurrentCondition) {
        this.setState({
          form: nextCurrentCondition,
        });
      }
    }
    if (nextProps.isLock !== this.props.isLock) {
      this.setState({
        form: {},
      });
    }
  }
  
  submit = () => {
    this.props.form.validateFields((err, values) => {
      const checkScene = this.checkScene()
      if (!err && checkScene) {
        const { scene, ...rest } = values;
        const payload = { ...this.state.form, ...rest };
        payload.begin = moment(values.time[0]).format('YYYY-MM-DD HH:mm:ss');
        payload.end = moment(values.time[1]).format('YYYY-MM-DD HH:mm:ss');
        this.props.fetchData(payload).then(res => {
          this.handleCancel()
        });
      }
    });
  }
  
  handleCancel = () => {
    this.setState({
      form: {
        channel: '',
        action: '',
        position: '',
        productId: '',
        shopName: '',
        time: null,
        channelId: [],
      }
    })
    this.props.onCancel()
    
  }
  
  onChannelChange = (conditions) => {
    this.setState({
      form: {
        ...this.state.form,
        channelId: conditions
      }
    });
  }
  
  handleSceneChange = (evt, field) => {
    this.setState({
      form: {
        ...this.state.form,
        [field]: evt.target.value
      }
    }, () => {
      this.checkScene()
    });
  }
  
  checkScene = () =>{
    const { action, channel, position } = this.state.form;
    const arr = [ action, channel, position ]
    if (arr.every(v1=> v1) || arr.every(v2=> !v2)) {
      this.setState({
        help: '',
        validateStatus: ''
      });
      return true
    }
      this.setState({
        help: '营销场景需要三个字段全部为空或全部不为空',
        validateStatus: 'error'
      });
      return false
  }

  render() {
    const { checkedCondition } = this.props
    const { help, validateStatus } = this.state
    const { action, position, channel, time, productId, shopName, channelId } = this.state.form;
    const { visible } = this.props;
    const { getFieldDecorator } = this.props.form
    
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    
    const options = [
      {label: '***名字***', value: '000'},
    ];
    
    return (
      <Modal
        visible={visible}
        title="输入条件"
        destroyOnClose
        onCancel={this.handleCancel}
        onOk={this.submit}
        width={600}
      >
        <Card bordered={false} className="chart-table-condition">
          <Form {...formItemLayout}>
            <LZFormItem field="time" label="时间区间" getFieldDecorator={getFieldDecorator}
                        requiredMessage="时间区间不能为空" initialValue={time}>
              <RangePicker
                className="w-400"
                showTime
                format="YYYY/MM/DD HH:mm:ss"
                placeholder={['开始时间', '结束时间']}
              />
            </LZFormItem>
            <LZFormItem field="scene" label="营销场景" getFieldDecorator={getFieldDecorator}
                        validateStatus={validateStatus} help={help}
                        lzIf={checkedCondition.includes('营销场景')} required>
              <div className="df scene">
                <Input placeholder="action" value={action}
                       onChange={val=>this.handleSceneChange(val, 'action')} />
                <Input placeholder="position" value={position}
                       onChange={val=>this.handleSceneChange(val, 'position')}/>
                <Input placeholder="channel_id" value={channel}
                       onChange={val=>this.handleSceneChange(val, 'channel')}/>
              </div>
            </LZFormItem>
            <LZFormItem
              field="shopName"
              label="商户名称"
              lzIf={checkedCondition.includes('商户名称')}
              getFieldDecorator={getFieldDecorator}
              requiredMessage="商户名称不能为空"
              initialValue={shopName}
            >
              <Input placeholder="请输入" />
            </LZFormItem>
            <LZFormItem
              lzIf={checkedCondition.includes('商品名称')}
              field="productId"
              label="商品ID"
              getFieldDecorator={getFieldDecorator}
              requiredMessage="商品ID不能为空"
              initialValue={productId}
            >
              <Input placeholder="请输入" />
            </LZFormItem>
            <LZFormItem
              field="channelId"
              label="渠道来源"
              lzIf={checkedCondition.includes('渠道来源')}
              getFieldDecorator={getFieldDecorator}
              requiredMessage="渠道来源不能为空"
              initialValue={channelId}
            >
              <Checkbox.Group options={options} onChange={this.onChannelChange}/>
            </LZFormItem>

          </Form>
        </Card>
      </Modal>
    );
  }
}

export default ReportTableConfigModal;
