import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Card, Checkbox, Form, Switch } from 'antd';
import HkReportTable from './hkReportTable';

const options = [
  {label: '时间区间', value: '时间区间', disabled: true},
  {label: '营销场景', value: '营销场景'},
  {label: '商户名称', value: '商户名称'},
  {label: '商品ID', value: '商品名称'},
  {label: '渠道来源', value: '渠道来源'},
];
@Form.create()
export default class ReportTable extends Component {
  state = {
    isLock: false,
    checkedCondition: ['时间区间'],
  };

  componentDidMount() {}

  onConditionChange = conditions => {
    this.setState({
      checkedCondition: conditions,
    });
  };

  onSwitchChange = val => {
    this.setState({
      isLock: val,
    });
  };

  render() {
    const { checkedCondition, isLock } = this.state;
    return (
      <PageHeaderWrapper title={false}>
        <Card bordered={false}>
          <div className="hk-chart-header">
            <div className="hk-chart-title">获客明细表</div>
            <div className="hk-chart-subtitle">单位: 笔</div>
            <div className="hk-chart-filter">
              <Checkbox.Group
                options={options}
                defaultValue={checkedCondition}
                disabled={isLock}
                onChange={this.onConditionChange}
              />
              <Switch
                className="filter-switch"
                checkedChildren="解锁"
                unCheckedChildren="锁定"
                defaultChecked={isLock}
                onChange={this.onSwitchChange}
              />
            </div>
          </div>
          <div className="hk-chart-content-table">
            <HkReportTable checkedCondition={checkedCondition} isLock={isLock} />
          </div>
        </Card>
      </PageHeaderWrapper>
    );
  }
}
