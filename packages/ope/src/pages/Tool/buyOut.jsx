import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import {
  Form,
  Icon,
  Input,
  Button,
  DatePicker,
  Card,
  Row,
  Col,
  Descriptions,
  Divider,
  Select,
  Spin,
  Typography,
  
} from 'antd';
import AntTable from '@/components/AntTable';
import { onTableData } from '@/utils/utils.js';
import CustomCard from '@/components/CustomCard';
import buyOutServices from './services';
import moment from 'moment';
const { Option } = Select;
const { Text,Title } = Typography;
@Form.create()
export default class buyOut extends Component {
  state = {
    list: [],
    data: {},
    loading: false,
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState({ loading: true }, () => {
          values.yiFuQiCi = values.yiFuQiCi.toString();
          (values.dangQianRiQi = moment(values.dangQianRiQi).format('YYYY-MM-DD HH:mm:ss')),
            (values.qiZuRiQi = moment(values.qiZuRiQi).format('YYYY-MM-DD HH:mm:ss')),
            buyOutServices.cal({ ...values }).then(res => {
              if (res) {
                this.setState({
                  list: res.orderByStagesDtoList,
                  data: res,
                  loading: false,
                });
              }
            });
        });
      }
    });
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };

    let statusType = {
      1: '待支付',
      2: '已支付',
      3: '逾期已支付',
      4: '逾期待支付',
      5: '已取消',
      6: '已结算',
      7: '已退款',
    };
    const columns = [
      {
        title: '总期数',
        dataIndex: 'totalPeriods',
      },
      {
        title: '当前租期',
        dataIndex: 'currentPeriods',
      },
      {
        title: '租金',
        dataIndex: 'currentPeriodsRent',
      },
      {
        title: '状态',
        dataIndex: 'status',
        render: e => statusType[e],
      },
      {
        title: '支付时间',
        dataIndex: 'repaymentDate',
      },
      {
        title: '账单到账时间',
        dataIndex: 'statementDate',
      },
    ];
    const children = [];
    for (let i = 1; i < 13; i++) {
      children.push(
        <Option key={i} value={i}>
          {i}
        </Option>,
      );
    }
    const { list, data, loading } = this.state;
    return (
      <PageHeaderWrapper>
        <Spin spinning={loading}>
        
        <Card bordered={false}>
        <Text code style={{fontSize:18}}><span style={{fontWeight:700}}>买断公式：</span> [（官方价-已付租金+提前支付的租金+增值服务费）+官方价*月利率*使用期数]*1.06-提前支付的租金</Text>
        
        
          <Form onSubmit={this.handleSubmit} {...formItemLayout} style={{marginTop:30}}>
            <Row>
              <Col span={8}>
                <Form.Item label="官方价">
                  {getFieldDecorator('guanFangJia', {
                    rules: [{ required: true, message: '请输入' }],
                  })(<Input placeholder="请输入" style={{ width: 200 }} />)}
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="总租期（天数）">
                  {getFieldDecorator('zongZuQi', {
                    rules: [{ required: true, message: '请输入' }],
                  })(<Input placeholder="请输入" style={{ width: 200 }} />)}
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="已付租金">
                  {getFieldDecorator('yiFuQiCi', {
                    rules: [{ required: true, message: '请选择' }],
                  })(
                    <Select placeholder="请选择" mode="multiple" style={{ width: 200 }}>
                      {children}
                    </Select>,
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={8}>
                <Form.Item label="销售价格">
                  {getFieldDecorator('xiaoShouJia', {
                    rules: [{ required: true, message: '请输入' }],
                  })(<Input placeholder="请输入" style={{ width: 200 }} />)}
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="起租日期">
                  {getFieldDecorator('qiZuRiQi', {
                    rules: [{ required: true, message: '请选择' }],
                  })(<DatePicker style={{ width: 200 }} />)}
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="增值服务费">
                  {getFieldDecorator('zengZhiFuWu', {
        
                  })(<Input placeholder="请输入" style={{ width: 200 }} />)}
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={8}>
                <Form.Item label="日租金">
                  {getFieldDecorator('riZuJin', {
                    rules: [{ required: true, message: '请输入' }],
                  })(<Input style={{ width: 200 }} placeholder="请输入" />)}
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="当前日期">
                  {getFieldDecorator('dangQianRiQi', {
                    rules: [{ required: true, message: '请选择' }],
                  })(<DatePicker style={{ width: 200 }} />)}
                </Form.Item>
              </Col>
            </Row>
            <div style={{ textAlign: 'center' }}>
              <Button type="primary" htmlType="submit">
                计算
              </Button>
              <Button onClick={() => this.props.form.resetFields()}>清空</Button>
            </div>
          </Form>
          <CustomCard title="其他租赁条件" style={{ marginTop: 10 }} />
          <Descriptions style={{ marginTop: 10 }}>
            <Descriptions.Item label="应付租金">{data.prePayedAmount}</Descriptions.Item>
            <Descriptions.Item label="提前支付的租金">{data.advancePayedAmount}</Descriptions.Item>
            <Descriptions.Item label="月利率">{data.rate}</Descriptions.Item>
          </Descriptions>
          <CustomCard title="交付计划" style={{ margin: '10px 0' }} />
          <AntTable dataSource={onTableData(list)} columns={columns} />
          <CustomCard title="买断价格" style={{ margin: '10px 0' }} />
          <Descriptions style={{ marginTop: 10 }}>
            <Descriptions.Item label="当前买断价格" span={3}>
              {data.currentBuyOutAmount}
            </Descriptions.Item>
            <Descriptions.Item label="到期买断价格" span={3}>
              {data.dueBuyOutAmount}
            </Descriptions.Item>
          </Descriptions>
        </Card>
        </Spin>
      </PageHeaderWrapper>
    );
  }
}
