import servicesApi from '../services'

export default {
  spacename: 'commission',
  state: {
    commission: [],
    shops: [],
    commissionDetail: {}
  },
  effects: {
    //获取tab
    *getCommissionList({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.getCommissionList, payload)
      if (res.msg === '操作成功') {
        yield put({
          type: 'saveCommissionList',
          payload: res.data
        })
      }
      callback(res)
    },
    *getShopList({ payload }, { put, call }) {
      let res = yield call(servicesApi.getShopList, payload)
      if (res.success) {
        yield put({
          type: 'saveShopList',
          payload: res.data
        })
      }
    },
    *addCommission({ payload }, { put, call }) {
      yield call(servicesApi.addCommission, payload)
    },
    *getCommissionDetail({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.getCommissionDetail, payload)
      if (res.success) {
        yield put({
          type: 'saveCommission',
          payload: res.data
        })
      }
    },
    //添加tab
    *addTab({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.addTab, payload)
      callback(res)
    },
    //删除tab
    *deleteTab({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.deleteTab, payload)
      callback(res)
    },
    //编辑tab
    *updateTab({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.updateTab, payload)
      callback(res)
    },
    //删除tab下面的产品
    *deleteProduct({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.deleteProduct, payload)
      callback(res)
    },
    //更新tab下的产品
    *updateProduct({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.updateProduct, payload)
      callback(res)
    },
    //获取tab下面的产品
    *getProduct({ payload, callback }, { call, put }) {
      let res = yield call(servicesApi.getProduct, payload)
      if (res.data.msg === '操作成功') {
        yield put({
          type: 'saveProduct',
          payload: res.data.data
        })
      }
      callback(res)
    },
  },
  reducers: {
    saveCommissionList(state, {payload}) {
      return {...state, commission: payload.records}
    },
    saveShopList(state, {payload}) {
      return {...state, shops: payload.records}
    },
    saveCommission(state, {payload}) {
      console.log(payload);
      return {...state, commissionDetail: payload.records}
    },
  }
}
