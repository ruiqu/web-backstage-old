/**
 * 资金账户详情页面
 */
import React from "react"
import { connect } from "dva"
import { PageHeaderWrapper } from "@ant-design/pro-layout"
import { Card, Table, message } from "antd"
import PinzhengDialog from "./components/pinzhengDialog"
import request from "@/services/baseService"
import router from "umi/router"
import { tab1, tab2 } from "@/models/feeDetail"


const pinzhengTypes = ["WITHDRAW", "RECHARGE"]

@connect(({ capitalAccountDetailModel }) => ({
  ...capitalAccountDetailModel,
}))
class CapitalAccount extends React.Component {
  state = {
    tableLoading: false, // 列表数据加载中
    tableData: {},
    showPinzhengModal: false, // 是否显示凭证弹窗
    flowId: "", // 凭证的流水号
  }

  shopId = "" // 商家ID

  componentDidMount() {
    this.shopId = this.props.match.params.id
    if (!this.shopId) { // 链接中不存在店铺ID
      message.warn("缺少店铺编号")
      return
    }
    this.fetchListHandler()
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    let beforeQueryStr = JSON.stringify(this.props.queryObj)
    let afterQueryStr = JSON.stringify(nextProps.queryObj)
    if (beforeQueryStr != afterQueryStr) {
      this.fetchListHandler(nextProps.queryObj)
    }
  }

  // 加载资金明细列表数据
  fetchListHandler = postObj => {
    this.setState({ tableLoading: true })
    const postData = postObj || { ...this.props.queryObj }
    postData.shopId = this.shopId
    return request("/hzsx/shopFund/pageShopFundFlow", postData).then(res => {
      this.setState({ tableData: res })
    }).finally(() => {
      this.setState({ tableLoading: false })
    })
  }

  // Table组件的配置数据
  tableColumns = [
    { title: "时间", dataIndex: "createTime" },
    {
      title: "类型",
      dataIndex: "operate",
      render: val => {
        const uimap = {
          WITHDRAW: "提现",
          RECHARGE: "充值",
          BROKERAGE_ZWZ: "佣金结算",
          ASSESSMENT: "芝麻租押分离接口费用",
          E_SIGN: "e签宝费用",
          BROKERAGE_LITE: "简版小程序结算",
          BROKERAGE_TOUTIAO: "抖音小程序佣金结算"
        }
        const cn = uimap[val] || "-"
        return cn
      }
    },
    {
      title: "变更金额",
      dataIndex: "changeAmount",
    },
    { title: "变更人", dataIndex: "operator" },
    { title: "余额", dataIndex: "afterAmount" },
    {
      title: "操作",
      render: (_, row) => {
        const tv = row.operate // 类型
        let cn
        if (pinzhengTypes.includes(tv)) cn = "凭证"
        else cn = "明细"
        return <a onClick={() => this.actionHandler(row)} className="blackClickableA">{ cn }</a>
      }
    }
  ]

  actionHandler = rowData => {
    const ty = rowData.operate // 操作类型
    if (pinzhengTypes.includes(ty)) { // 查看凭证弹窗的
      this.setState({ showPinzhengModal: true, flowId: rowData.id })
    } else { // 其它操作
      const jiesuanType = ["BROKERAGE_ZWZ", "BROKERAGE_LITE", "BROKERAGE_TOUTIAO"] // 结算类型
      if (jiesuanType.includes(ty)) { // 跳转到佣金结算明细详情页面
        const url = `/finance/settlement/detail?fromCapitalAccount=1&id=${rowData.id}`
        router.push(url)
      } else { // 跳转到费用结算明细详情页面
        const vMap = {
          ASSESSMENT: tab1,
          E_SIGN: tab2,
        }
        const v = vMap[ty] // 菜单ID
        this.props.dispatch({ type: "feeDetailModel/mutFeeDetailActiveTab", payload: v }) // 修改redux中所存储的焦点tab
        this.props.dispatch({ type: "feeDetailModel/muFeeDetailListApiParamsComplete", payload: { tab: v, val: { pageNumber: 1, pageSize: 10 } }}) // 修改页码
        const url = `/finance/feeDetail?id=${rowData.id}`
        router.push(url)
      }
    }
  }

  showTotal = () => {
    return `共有${this.state.tableData?.total}条`
  }

  // 改变页码的时候触发
  changePageHandler = val => {
    const payload = { key: "pageNumber", val }
    this.props.dispatch({ type: "capitalAccountDetailModel/muQueryObjByKv", payload })
  }

  // 改变每页条数时触发
  showSizeChangeHandler = (_, size) => {
    const payload = { pageSize: size, pageNumber: 1 }
    this.props.dispatch({ type: "capitalAccountDetailModel/muQueryObjCompletely", payload })
  }

  render() {
    const queryObj = this.props.queryObj || {}
    const { pageNumber, pageSize } = queryObj
    const tableDataSource = this.state.tableData?.records || []

    return (
      <PageHeaderWrapper>
        <Card bordered={false}>
          <Table
            loading={this.state.tableLoading}
            columns={this.tableColumns}
            dataSource={tableDataSource}
            rowKey={record => record.id}
            pagination={{
              current: pageNumber,
              pageSize: pageSize,
              total: this.state.tableData?.total,
              onChange: this.changePageHandler,
              onShowSizeChange: this.showSizeChangeHandler,
              showTotal: this.showTotal,
              showQuickJumper: true,
              pageSizeOptions: ["5", "10", "20"],
              showSizeChanger: true,
            }}
          />
        </Card>

        <PinzhengDialog
          showPinzheng={this.state.showPinzhengModal}
          flowNo={this.state.flowId}
          hideModalHandler={() => { this.setState({ showPinzhengModal: false, flowId: "" })}}
        />
      </PageHeaderWrapper>
    )
  }
}

export default CapitalAccount