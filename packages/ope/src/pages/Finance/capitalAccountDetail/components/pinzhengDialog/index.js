/**
 * 凭证弹窗，展示凭证的一些信息
 */
import React from "react"
import { Modal, Descriptions, Card, message } from "antd"
import request from "@/services/baseService"
import { convertCurrency, strUi } from "../../../../../../src/utils/utils"

/**
* 
* @param {*} props { showPinzheng: 是否显示弹窗；hideModalHandler: 关闭弹窗的方法 }
* @returns 
*/
class PinzhengDialog extends React.Component {
  state = {
    loading: false, // 是否处于加载凭证数据过程中
    data: {}, // 凭证详情数据
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.flowNo != this.props.flowNo && nextProps.showPinzheng) {
      this.fetchData(nextProps.flowNo)
    }
  }

  /**
  * 加载凭证数据
  * @param {} fNum 
  */
  fetchData = fNum => {
    if (!fNum) {
      message.warn("缺少流水号")
      return
    }
    this.setState({ loading: true })
    const url = `/hzsx/shopFund/prof?id=${fNum}`
    request(url, {}, "get").then(res => {
      this.setState({ data: res })
    }).finally(() => {
      this.setState({ loading: false })
    })
  }

  render() {
    const uiData = this.state.data || {} // 凭证详情数据
    return (
      <Modal
        title="查看凭证"
        visible={this.props.showPinzheng}
        onCancel={() => { this.props.hideModalHandler() }}
        footer={null}
        width={1200}
      >
        <Card bordered={false} loading={this.state.loading}>
        <Descriptions bordered>
          <Descriptions.Item label="付款账户">
            {/* { strUi(uiData.fromAccountName) } */}
            {/* <br /> */}
            <b>{ strUi(uiData.fromAccountIdentity) }</b>
          </Descriptions.Item>
          <Descriptions.Item label="转账金额">
            {uiData.amount} 元 （{convertCurrency(uiData.amount)}）
          </Descriptions.Item>
          <Descriptions.Item label="服务费">
            {uiData.serviceFee} 元 （以实际收取为准）
          </Descriptions.Item>
          <Descriptions.Item label="收款账户">
            收款人姓名：{ strUi(uiData.toAccountName) }
            <br />
            收款支付宝账号：{ strUi(uiData.toAccountIdentity) }
          </Descriptions.Item>
          <Descriptions.Item label="备注"><span style={{width:100}}>{ strUi(uiData.remark) }</span></Descriptions.Item>
          <Descriptions.Item label="标题">{ strUi(uiData.title) }</Descriptions.Item>
          <Descriptions.Item label="付款总额">
            {uiData.amount} 元 （{convertCurrency(uiData.amount)}）
          </Descriptions.Item>
          <Descriptions.Item label="流水订单号">
            { strUi(uiData.orderNo) }
          </Descriptions.Item>
        </Descriptions>
        </Card>
      </Modal>
    )
  }
}

export default PinzhengDialog