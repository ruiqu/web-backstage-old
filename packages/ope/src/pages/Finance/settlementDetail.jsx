import React, { Component } from 'react';
import {
  Card,
  Descriptions,
  Button,
  Tabs,
  Divider,
  Steps,
  Modal,
  Form,
  Input,
  message,
  Popconfirm,
} from 'antd';
import moment from 'moment';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import CustomCard from '@/components/CustomCard';
import MyPageTable from '@/components/MyPageTable';
import { onTableData } from '@/utils/utils.js';
import settlementService from './services/settlement';
import { getParam } from '@/utils/utils';
const { TabPane } = Tabs;
const { Step } = Steps;
import statusMap from '@/config/status.config'
import request from '@/services/baseService'

@Form.create()
export default class settlementDetail extends Component {
  state = {
    current: 1,
    total: 1,
    processDetail: [],
    data: {},
    visible: false,
    listRemark: [],
    totalRemark: 1,
    title: '',
    payInfoData: {},
    loading: false,
  };

  componentDidMount() {
    this.setState({ loading: true })
    this.onAccountPeriodDetail().then(() => {
      return this.onRemark(1, 3);
    }).finally(() => {
      this.setState({ loading: false })
    })
  }

  onAccountPeriodDetail = () => {
    let ajaxPromise
    const isFromZijin = getParam("fromCapitalAccount")
    const pid = getParam('id')
    if (isFromZijin) { // 资金账户页面跳转过来
      const url = `/hzsx/shopFund/brokerageDetail?id=${pid}`
      ajaxPromise = request(url, {}, "get")
    } else { // 正常跳转行为
      ajaxPromise = settlementService.queryAccountPeriodDetail({ id: pid })
    }
    return ajaxPromise.then(res => {
      this.setState({
        data: res,
      });
    });
  };

  /** 返回实际ID */
  returnExportId = () => {
    const obj = this.state.data || {}
    return obj.id
  }

  // 加载备注数据
  onRemark = (pageNumber, pageSize) => {
    return settlementService
      .listRemark({ accountPeriodId: this.returnExportId(), pageNumber, pageSize })
      .then(res => {
        this.setState({
          listRemark: res.records,
          totalRemark: res.total,
        });
      });
  };

  onPage = e => {
    this.setState(
      {
        current: e.current,
      },
      () => {
        this.onRemark(e.current, 3);
      },
    );
  };
  handleOk = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { title } = this.state;
        if (title === '备注') {
          settlementService.addRemark({ accountPeriodId: this.returnExportId(), ...values }).then(res => {
            message.success('备注成功！');
            this.setState(
              {
                visible: false,
                current: 1,
              },
              () => {
                this.onRemark(1, 3);
              },
            );
          });
        } else {
          values.settleAmount = parseFloat(values.settleAmount).toFixed(2);
          settlementService
            .submitSettle({ accountPeriodId: this.returnExportId(), ...values })
            .then(res => {
              message.success('结算成功！');
              this.setState(
                {
                  visible: false,
                  current: 1,
                },
                () => {
                  this.onAccountPeriodDetail();
                },
              );
            });
        }
      }
    });
  };

  handleCancel = e => {
    this.setState({
      visible: false,
    });
  };
  showModal = e => {
    this.setState({
      visible: true,
      title: e,
    });
  };
  confirm = e => {
    settlementService.submitAudit({ accountPeriodId: this.returnExportId(), auditResult: e }).then(res => {
      message.success('成功！');
      this.onAccountPeriodDetail();
    });
  };
  confirmPayment = () => {
    settlementService.submitPay({ accountPeriodId: this.returnExportId() }).then(res => {
      message.success('支付成功！');
      this.onAccountPeriodDetail();
    });
  };
  convertCurrency(money) {
    //汉字的数字
    let cnNums = new Array('零', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖');
    //基本单位
    let cnIntRadice = new Array('', '拾', '佰', '仟');
    //对应整数部分扩展单位
    let cnIntUnits = new Array('', '万', '亿', '兆');
    //对应小数部分单位
    let cnDecUnits = new Array('角', '分', '毫', '厘');
    //整数金额时后面跟的字符
    let cnInteger = '整';
    //整型完以后的单位
    let cnIntLast = '元';
    //最大处理的数字
    let maxNum = 999999999999999.9999;
    //金额整数部分
    let integerNum;
    //金额小数部分
    let decimalNum;
    //输出的中文金额字符串
    let chineseStr = '';
    //分离金额后用的数组，预定义
    let parts;
    if (money == '') {
      return '';
    }
    money = parseFloat(money);
    if (money >= maxNum) {
      //超出最大处理数字
      return '';
    }
    if (money == 0) {
      chineseStr = cnNums[0] + cnIntLast + cnInteger;
      return chineseStr;
    }
    //转换为字符串
    money = money.toString();
    if (money.indexOf('.') == -1) {
      integerNum = money;
      decimalNum = '';
    } else {
      parts = money.split('.');
      integerNum = parts[0];
      decimalNum = parts[1].substr(0, 4);
    }
    //获取整型部分转换
    if (parseInt(integerNum, 10) > 0) {
      let zeroCount = 0;
      let IntLen = integerNum.length;
      for (let i = 0; i < IntLen; i++) {
        let n = integerNum.substr(i, 1);
        let p = IntLen - i - 1;
        let q = p / 4;
        let m = p % 4;
        if (n == '0') {
          zeroCount++;
        } else {
          if (zeroCount > 0) {
            chineseStr += cnNums[0];
          }
          //归零
          zeroCount = 0;
          chineseStr += cnNums[parseInt(n)] + cnIntRadice[m];
        }
        if (m == 0 && zeroCount < 4) {
          chineseStr += cnIntUnits[q];
        }
      }
      chineseStr += cnIntLast;
    }
    //小数部分
    if (decimalNum != '') {
      let decLen = decimalNum.length;
      for (let i = 0; i < decLen; i++) {
        let n = decimalNum.substr(i, 1);
        if (n != '0') {
          chineseStr += cnNums[Number(n)] + cnDecUnits[i];
        }
      }
    }
    if (chineseStr == '') {
      chineseStr += cnNums[0] + cnIntLast + cnInteger;
    } else if (decimalNum == '') {
      chineseStr += cnInteger;
    }
    return chineseStr;
  }

  render() {
    const { processDetail, data, listRemark, totalRemark, payInfoData } = this.state;
    const { getFieldDecorator } = this.props.form;
    const paginationProps = {
      current: this.state.current,
      pageSize: 3,
      total: totalRemark,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / 10)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };

    const progressesStatus = {
      GENERATE: '生成结算单',
      SUBMIT_SETTLE: '提交结算',
      AUDIT_PASS: '审核通过',
      AUDIT_DENY: '审核拒绝',
      SUBMIT_PAY: '提交支付',
      PAY_FAIL: '支付失败',
      PAY_SUCCESS: '支付成功',
    };
    const columns = [
      {
        title: '备注人姓名',
        dataIndex: 'backstageUserName',
        width: '33%',
        align: 'center',
      },
      {
        title: '备注时间',
        dataIndex: 'createTime',
        width: '33%',
        align: 'center',
      },
      {
        title: '备注内容',
        width: '33%',
        align: 'center',
        dataIndex: 'content',
      },
    ];
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 },
      },
    };
    const formItemLayoutBz = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 },
      },
    };
    return (
      <PageHeaderWrapper>
        <Card bordered={false} loading={this.state.loading}>
          <Descriptions title={<CustomCard title="基本信息" />}>
            <Descriptions.Item label="结算日期">
              {moment(data.settleDate).format('YYYY-MM-DD')}
            </Descriptions.Item>
            <Descriptions.Item label="商家名称">{data.shopName}</Descriptions.Item>
            <Descriptions.Item label="结算状态">{statusMap[data.status]}</Descriptions.Item>
            <Descriptions.Item label="结算总额">{data.totalSettleAmount}</Descriptions.Item>
            <Descriptions.Item label="佣金">{data.totalBrokerage}</Descriptions.Item>
            {data.status !== 'WAITING_SETTLEMENT' ? (
              <Descriptions.Item label="实际结算金额">{data.settleAmount}</Descriptions.Item>
            ) : null}
          </Descriptions>
          <div style={{ textAlign: 'center' }}>
            {data.status === 'WAITING_SETTLEMENT' ? (
              <Button type="primary" onClick={() => this.showModal('提交结算')}>
                提交结算
              </Button>
            ) : null}

            {data.status === 'WAITING_PAYMENT' ? (
              <Popconfirm
                title="是否提交支付?"
                onConfirm={this.confirmPayment}
                okText="确定"
                cancelText="取消"
              >
                <Button type="primary">提交支付</Button>
              </Popconfirm>
            ) : null}
            {data.status === 'TO_AUDIT' ? (
              <Popconfirm
                title="是否提交审核?"
                onConfirm={() => this.confirm(true)}
                okText="确定"
                cancelText="取消"
              >
                <Button type="primary">审核通过</Button>
              </Popconfirm>
            ) : null}
            {data.status === 'TO_AUDIT' ? (
              <Popconfirm
                title="是否提交审核?"
                onConfirm={() => this.confirm(false)}
                okText="确定"
                cancelText="取消"
              >
                <Button type="primary">审核拒绝</Button>
              </Popconfirm>
            ) : null}
            <Button onClick={() => this.showModal('备注')}>备注</Button>
          </div>
        </Card>
        <Card bordered={false} loading={this.state.loading} style={{ marginTop: 20 }}>
          <Tabs defaultActiveKey="1">
            <TabPane tab="结算情况" key="1">
              <Descriptions title={<CustomCard title="常规账单" />}>
                <Descriptions.Item label="结算金额">{data.rentAmount}</Descriptions.Item>
                <Descriptions.Item label="佣金">{data.rentBrokerage}</Descriptions.Item>
              </Descriptions>
              <Button
                type="primary"
              >
                <a href={`#/finance/normal?id=${this.returnExportId()}`} target="_blank">
                  查看明细
                </a>
              </Button>
              <Button
                onClick={() =>
                  settlementService
                    .accountPeriodRent({ accountPeriodId: this.returnExportId() })
                    .then(res => {
                      message.success('导出任务创建成功，请前往“数据管理-导出数据下载”完成下载。');
                    })
                }
              >
                导出数据
              </Button>
              <Divider />
              <Descriptions title={<CustomCard title="买断账单" />}>
                <Descriptions.Item label="结算金额">{data.buyoutAmount}</Descriptions.Item>
                <Descriptions.Item label="佣金">{data.buyoutBrokerage}</Descriptions.Item>
              </Descriptions>
              <Button
                type="primary"
              >
                <a href={`#/finance/buyout?id=${this.returnExportId()}`} target="_blank">
                  查看明细
                </a>
              </Button>
              <Button
                onClick={() =>
                  settlementService
                    .accountPeriodBuyOut({ accountPeriodId: this.returnExportId() })
                    .then(res => {
                      message.success('导出任务创建成功，请前往“数据管理-导出数据下载”完成下载。');
                    })
                }
              >
                导出数据
              </Button>
              <Divider />
              <Descriptions title={<CustomCard title="购买账单" />}>
                <Descriptions.Item label="结算金额">{data.purchaseAmount}</Descriptions.Item>
                <Descriptions.Item label="佣金">{data.purchaseBrokerage}</Descriptions.Item>
              </Descriptions>
              <Button
                type="primary"
              >
                <a href={`#/finance/purchase?id=${this.returnExportId()}`} target="_blank">
                  查看明细
                </a>
              </Button>
              <Button
                onClick={() =>
                  settlementService
                    .accountPeriodPurchase({ accountPeriodId: this.returnExportId() })
                    .then(res => {
                      message.success('导出任务创建成功，请前往“数据管理-导出数据下载”完成下载。');
                    })
                }
              >
                导出数据
              </Button>
              <Divider />
              <CustomCard title="备注" />
              <MyPageTable
                dataSource={onTableData(listRemark)}
                columns={columns}
                onPage={this.onPage}
                paginationProps={paginationProps}
              />
            </TabPane>
            <TabPane tab="结算流程" key="2">
              <Steps
                direction="vertical"
                progressDot={document.body.clientWidth >= 1025}
                current={data && data.progresses && data.progresses.length}
              >
                {data &&
                  data.progresses &&
                  data.progresses.map(item => {
                    const desc = (
                      <div style={{ width: 300 }}>
                        <div>{item.operator}</div>
                        <div>{progressesStatus[item.status]}</div>
                        <div>{item.createTime}</div>
                      </div>
                    );
                    return <Step title={item.operate} key={item.operate} description={desc} />;
                  })}
              </Steps>
            </TabPane>
          </Tabs>
        </Card>
        <Modal
          title={this.state.title}
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          footer={this.state.title === '查看凭证' ? '' : undefined}
          width={this.state.title === '查看凭证' ? 1100 : 520}
          destroyOnClose
        >
          {this.state.title === '备注' ? (
            <Form {...formItemLayoutBz}>
              <Form.Item label="备注">
                {getFieldDecorator('content', {
                  rules: [{ required: true, message: '请输入' }],
                })(<Input placeholder="请输入" />)}
              </Form.Item>
            </Form>
          ) : this.state.title === '查看凭证' ? (
            <Descriptions bordered>
              <Descriptions.Item label="付款账户">
                杭州布布信息技术有限公司
                <br />
                <b>zuwu@shouxin168.com</b>
              </Descriptions.Item>
              <Descriptions.Item label="转账金额">
                {payInfoData.amount} 元 （{this.convertCurrency(payInfoData.amount)}）
              </Descriptions.Item>
              <Descriptions.Item label="服务费">
                {payInfoData.serviceFee} 元 （以实际收取为准）
              </Descriptions.Item>
              <Descriptions.Item label="收款账户">
                收款人姓名：{payInfoData.toAccountName}
                <br />
                收款支付宝账号：{payInfoData.toAccountIdentity}
              </Descriptions.Item>
              <Descriptions.Item label="备注"><span style={{width:100}}>{payInfoData.remark}</span></Descriptions.Item>
              <Descriptions.Item label="标题">{payInfoData.title}</Descriptions.Item>

              <Descriptions.Item label="付款总额" span={3}>
                {payInfoData.amount} 元 （{this.convertCurrency(payInfoData.amount)}）
              </Descriptions.Item>
            </Descriptions>
          ) : (
            <Form {...formItemLayout}>
              <Form.Item label="实际结算金额">
                {getFieldDecorator('settleAmount', {
                  rules: [{ required: true, message: '请输入' }],
                  initialValue: data && data.totalSettleAmount,
                })(<Input placeholder="请输入" suffix="元" />)}
              </Form.Item>
              <Form.Item label="转账业务标题">
                {getFieldDecorator('settleTitle', {
                  rules: [{ required: true, message: '请输入' }],
                })(<Input placeholder="请输入" />)}
              </Form.Item>
              <Form.Item label="业务备注">
                {getFieldDecorator('settleRemark', {})(<Input placeholder="请输入" />)}
              </Form.Item>
            </Form>
          )}
        </Modal>
      </PageHeaderWrapper>
    );
  }
}
