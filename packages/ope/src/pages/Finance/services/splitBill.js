import request from '@/services/baseService';

export default {
  buyOutPage: data => {
    return request(`/hzsx/accountPeriod/buyOut`, data);
  },
  // rentPage: data => {
  //   return request(`/hzsx/splitBill/rentPage`, data);
  // },
  rentPage: data => {
    return request(`/hzsx/accountPeriod/rent`, data);
  },
  
  rentDetail: data => {
    return request(`/hzsx/accountPeriod/rentDetail`, data, 'get');
  },
  buyOutDetail: data => {
    return request(`/hzsx/accountPeriod/buyOutDetail`, data, 'get');
  },
  paySplitBill: data => {
    return request(`/hzsx/splitBill/paySplitBill`, data, 'get');
  },
  rentExport: data => {
    return request(`/hzsx/splitBill/rentExport`, data);
  },

  purchaseExport: data => {
    return request(`/hzsx/splitBill/purchaseExport`, data);
  },
  buyOutExport: data => {
    return request(`/hzsx/splitBill/buyOutExport`, data);
  },
  purchasePage: data => {
    return request(`/hzsx/accountPeriod/purchase`, data);
  },
  purchaseDetail: data => {
    return request(`/hzsx/accountPeriod/purchaseDetail`, data, 'get');
  },
  paySplitBills: data => {
    return request(`/hzsx/splitBill/paySplitBill`, data, 'get');
  },
};
