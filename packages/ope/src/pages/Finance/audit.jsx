import React, { PureComponent } from 'react';

import { connect } from 'dva';
import Search from '../../components/Search';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import {downloadFile, goToRouter} from "@/utils/utils";
import AuditService from './services'
import CopyToClipboard from '@/components/CopyToClipboard';

import {
  Table,
  Card,
  Form,
  Tabs
} from 'antd';

const getStatus = new Map([
  ['REJECT', { label: '审核拒绝', color: 'red'}],
  ['PENDING', {label: '待审核', color: 'yellow'}],
  ['PASS', {label: '审核通过', color: ''}],
  ['UNSET', {label: '未设置', color: ''}],
]);
const typeMap = {
  'BUY_OUT': '买断',
  'RENT_MONTH': '月租金分账',
}
const tabs = [
  { key: "zwz", ui: "支付宝小程序" },
  // { key: "lite", ui: "简版小程序" },
  // { key: "douyin", ui: "抖音小程序" },
]
const { TabPane } = Tabs

@connect(({ commission }) => ({
  ...commission,
}))
@Form.create()
class Audit extends PureComponent {
  state = {
    tableData: [],
    loading: true,
    current: 1,
    pageNumber: 1,
    pageSize: 10,
    type: null,
    shopId: null,
    shopName: null,
    status: null,
    visible: false,
    spinning: false,
    filter: {},
    activeTab: "zwz",
  };
  
  columns = [
    {
      title: '店铺编号',
      dataIndex: 'shopId',
      align: 'center',
      render: text => <CopyToClipboard text={text} />
    },
    {
      title: '店铺名称',
      dataIndex: 'shopName',
      align: 'center',
    },
    {
      title: '企业资质名称',
      dataIndex: 'shopFirmInfo',
      align: 'center',
      ellipsis: true
    },
    {
      title: '分佣类型',
      dataIndex: 'typeInfo',
      align: 'center',
      render: (text, record) => {
        const textArr = text.split(',')
        const result = textArr.map(v=>{
          return typeMap[v]
        })
        return result.join(',')
      }
    },
    {
      title: '分佣状态',
      dataIndex: 'status',
      align: 'center',
      render: text => {
        const item = getStatus.get(text)
        return (
          <span className={item.color}>
            {item.label}
          </span>
        )
      },
    },
    {
      title: '申请人',
      dataIndex: 'addUser',
      align: 'left',
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      align: 'left',
      key: 'crateTime',
      width: '200'
    },
    {
      title: '操作',
      align: 'center',
      key: 'action',
      render: (text, record) => {
        return (
          <div className="table-action">
            <a onClick={()=> this.preview(record.id)}>查看</a>
          </div>
        );
      },
    },
  ];
  
  componentDidMount() {
    this.initData();
  }
  
  initData = () => {
    let payload = {
      pageNumber: this.state.pageNumber,
      pageSize: this.state.pageSize,
      ...this.state.filter
    };
    this.setState(
      {
        loading: true,
      },
      () => {
        let fetchListFunction
        const activeTabKey = this.state.activeTab
        if (activeTabKey === "zwz") {
          fetchListFunction = AuditService.page
        } else if (activeTabKey === "lite") {
          fetchListFunction = AuditService.litePage
        } else if (activeTabKey === "douyin") {
          fetchListFunction = AuditService.douyinPage
        }
        fetchListFunction(payload).then(res => {
          this.setState({
            tableData: res.records,
            total: res.total,
            loading: false,
            current: res.current,
          });
        })
      },
    );
  };
  
  handleFilter = (data = {}) => {
    if (data.type && data.type === '分页') {
      this.initData();
    } else {
      this.setState(
        {
          pageNumber: 1,
          pageSize: 10,
          filter: data
        },
        () => {
          this.initData();
        },
      );
    }
  };
  
  handleExportData = () => {
    downloadFile()
  }
  
  //分页，下一页
  onChange = pageNumber => {
    this.setState(
      {
        pageNumber: pageNumber,
      },
      () => {
        this.handleFilter({ type: '分页' });
      },
    );
  };
  
  showTotal = () => {
    return `共有${this.state.total}条`;
  };
  
  //切换每页数量
  onShowSizeChange = (current, pageSize) => {
    this.setState({ pageSize: pageSize }, () => {
      this.handleFilter({ type: '分页' });
    });
  };
  
  edit = id => {
    this.setState(
      {
        visible: true,
        spinning: true,
      },
      () => {
        goToRouter(this.props.dispatch, '/commission/edit/'+ id)
      },
    );
  };
  
  preview = id => {
    this.setState(
      {
        visible: true,
        spinning: true,
      },
      () => {
        let goPage
        const currentTabName = this.state.activeTab
        if (currentTabName === "zwz") {
          goPage = `/finance/audit/${id}?approve=1`
        } else if (currentTabName === "lite") { // 简版小程序，简版小程序和其余两者有些不同，不用携带额外的approve参数
          goPage = `/commission/liteList/commissionDetail/${id}`
        } else if (currentTabName === "douyin") { // 抖音小程序类型
          goPage = `/commission/douyin/commissionDetail/${id}?approve=1`
        }
        goToRouter(this.props.dispatch, goPage)
      },
    );
  };

  // 修改tab时触发
  changeTabHandler = atab => {
    this.setState({ activeTab: atab, filter: {} }, this.initData)
  }
  
  render() {
    const {loading, current, total, tableData} = this.state;
    return (
      <PageHeaderWrapper title={false}>
        <Card bordered={false} style={{ marginTop: 20 }}>
          <Tabs
            activeKey={this.state.activeTab}
            onChange={this.changeTabHandler}
            animated={false}
            destroyInactiveTabPane={true}
          >
            {
              tabs.map(tabObj => (
                <TabPane tab={tabObj.ui} key={tabObj.key}>
                  <>
                    <Search
                      needReset source={'佣金设置'}
                      exportData={this.handleExportData}
                      handleFilter={this.handleFilter}
                    />
                    <Table
                      columns={this.columns}
                      tableLayout="fixed"
                      loading={loading}
                      dataSource={tableData}
                      rowKey={record => record.id}
                      pagination={{
                        current: current,
                        total: total,
                        onChange: this.onChange,
                        showTotal: this.showTotal,
                        showQuickJumper: true,
                        pageSizeOptions: ['5', '10', '20'],
                        showSizeChanger: true,
                        onShowSizeChange: this.onShowSizeChange,
                      }}
                    />
                  </>
                </TabPane>
              ))
            }
          </Tabs>
        </Card>
      </PageHeaderWrapper>
    );
  }
}

export default Audit;
