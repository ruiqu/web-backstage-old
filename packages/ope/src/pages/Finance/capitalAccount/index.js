import React from "react"
import { connect } from "dva"
import { PageHeaderWrapper } from "@ant-design/pro-layout"
import { Card, Form, Input, Button, Row, Table } from "antd"
import styles from "./index.less"
import { onTableData } from "@/utils/utils"
import request from "@/services/baseService"
import router from "umi/router"

@connect(({ capitalAccountModel }) => ({
  ...capitalAccountModel,
}))
@Form.create()
class CapitalAccount extends React.Component {
  state = {
    listApiResult: {},
    loadingList: false,
  }

  componentDidMount() {
    this.dataFetchHandler()
  }

  componentWillReceiveProps(nextProps) {
    const currentQueryObj = this.props.queryObj
    const nextQueryObj = nextProps.queryObj
    const s1 = JSON.stringify(currentQueryObj)
    const s2 = JSON.stringify(nextQueryObj)
    if (s1 != s2) { // 查询数据
      this.dataFetchHandler(nextQueryObj)
    }
  }

  // 返回存储在redux中的查询query数据
  returnCacheQuery = () => {
    return this.props.queryObj
  }

  // 查询数据
  dataFetchHandler = qObj => {
    this.setState({ loadingList: true })
    const postData = qObj || this.returnCacheQuery()
    if (!postData.shopName) delete postData["shopName"]
    return request("/hzsx/shopFund/pageShopFundBalance", postData, "post").then(res => {
      this.setState({ listApiResult: res })
    }).finally(() => {
      this.setState({ loadingList: false })
    })
  }

  tableColumns = [
    { title: "店铺编号", dataIndex: "shopId" },
    { title: "店铺名称", dataIndex: "shopName" },
    { title: "资金余额（元）", dataIndex: "afterAmount" },
    {
      title: "操作",
      render: (_, row) => {
        const goDetailHandler = () => {
          const url = `/finance/capitalAccountDetail/${row.shopId}`
          router.push(url)
        }

        return <a onClick={goDetailHandler}>查看明细</a>
      }
    }
  ]

  // 改变页码时触发
  changePageHandler = pageNum => {
    this.props.dispatch({ type: "capitalAccountModel/muQueryObjByKv", payload: { key: "pageNumber", val: pageNum }})
  }

  // 改变每页条数时触发
  changePageSizeHandler = (_, newSize) => {
    const newObj = {
      ...this.returnCacheQuery(),
      pageSize: newSize,
      pageNumber: 1,
    }
    this.props.dispatch({ type: "capitalAccountModel/muQueryObjCompletely", payload: newObj })
  }

  // 点击搜索时触发
  searchHandler = () => {
    this.props.form.validateFields((err, value) => {
      if (!err) {
        this.props.dispatch({ type: "capitalAccountModel/muQueryObjByKv", payload: { key: "shopName", val: value.shopName }})
      }
    })
  }

  // 重置时触发
  resetForm = () => {
    this.props.form.resetFields()
    const newObj = {
      pageSize: 10,
      pageNumber: 1,
      shopName: "",
    }
    this.props.dispatch({ type: "capitalAccountModel/muQueryObjCompletely", payload: newObj })
  }

  render() {
    const { getFieldDecorator } = this.props.form
    const cacheForm = this.returnCacheQuery()

    const tableDatasource = onTableData(this.state.listApiResult?.records || [])
    const total = this.state.listApiResult?.total || 1

    const showTotal = () => `共有${total}条数据`

    return (
      <PageHeaderWrapper>
        <Card bordered={false}>
          {/** 顶部搜索区域 */}
          <Form layout="inline">
            <Row className={styles.mb15}>
              <Form.Item label="店铺名称">
                {
                  getFieldDecorator(
                    "shopName",
                    {
                      initialValue: cacheForm.shopName,
                      // rules: [
                      //   { required: true, message: "请输入店铺名称" },
                      // ],
                    },
                  )(
                    <Input placeholder="请输入店铺名称" className={styles.inputWrapper} allowClear />
                  )
                }
              </Form.Item>
            </Row>
            <Row className={styles.mb20}>
              <Button type="primary" onClick={this.searchHandler}>
                查询
              </Button>
              <Button onClick={this.resetForm}>
                重置
              </Button>
            </Row>
          </Form>

          <Table
            columns={this.tableColumns}
            loading={this.state.loadingList}
            dataSource={tableDatasource}
            rowKey={record => record.key}
            pagination={{
              current: cacheForm.pageNumber,
              pageSize: cacheForm.pageSize,
              total,
              onChange: this.changePageHandler,
              showTotal: showTotal,
              showQuickJumper: true,
              pageSizeOptions: ['5', '10', '20'],
              showSizeChanger: true,
              onShowSizeChange: this.changePageSizeHandler,
            }}
          />
        </Card>
      </PageHeaderWrapper>
    )
  }
}

export default CapitalAccount