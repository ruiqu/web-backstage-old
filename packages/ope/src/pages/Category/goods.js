// 添加前台类目商品用的
import React, { PureComponent, Fragment } from 'react';
import { Card, Button, Input, Form, Table, Row, Col, message, Cascader, Popconfirm } from 'antd';
import { connect } from 'dva';
import categoryService from './service';

@connect(({ category }) => ({
  ...category,
}))
class Goods extends PureComponent {
  state = {
    pageSize: 3,
    pageNumber: 1,
    keyWord: '',
    name: '',
    itemId: '',
    total: 0,
    current: 1,
    selectedRowKeys: [],
    opeCategoryId: null, //添加商品用到的
    goods: [],
    goodsList: [],
    select: [],
    secondary: [],
  };
  columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
      align: 'center',
    },
    {
      title: '名称',
      dataIndex: 'name',
      key: 'name',
      align: 'center',
    },
    {
      title: '图片',
      dataIndex: 'image',
      key: 'image',
      align: 'center',
      render: text => <img src={text} className="table-cell-img" alt="" />,
    },
  ];
  listColumns = [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
      align: 'center',
    },
    {
      title: '名称',
      dataIndex: 'name',
      key: 'name',
      align: 'center',
      render: (text, record) => <Input defaultValue={record.name} style={{ width: 200 }} onBlur={(e)=>this.onBlur(e,record.id)}/>,
    },
    {
      title: '操作',
      key: 'action',
      align: 'center',
      width: 120,
      render: (text, record) => (
        <Popconfirm title="确定从该类目删除吗？" onConfirm={() => this.delete(record.id)}>
          <a>删除</a>
        </Popconfirm>
      ),
    },
  ];
  onBlur = (e,id) =>{
    categoryService.editFirstCategory({id,name:e.target.value,}).then(res => {
      this.getGoods();
    });
  }
  //删除
  delete = id => {
    categoryService
      .deleteCategory({
        id,
      })
      .then(res => {
        this.getGoods();
      });
  };
  componentDidMount() {
    this.getGoods();
  }
  //获取类别
  getSelectAll = () => {
    categoryService.getSelectAll().then(res => {
      this.setState({
        select: res.records,
      });
    });
  };
  //获取商品
  getGoods = () => {
    const { pageSize, pageNumber, keyWord } = this.state;
    // categoryService
    //   .getGoods({
    //     pageSize,
    //     pageNumber,
    //     keyWord,
    //   })
    //   .then(res => {
    //     message.success('获取商品信息成功');
    //     this.setState({
    //       total: res.total,
    //       current: res.current,
    //       goods: res.records,
    //     });
    //   });
    categoryService
      .queryOpeCategoryPage({
        pageNumber,
        pageSize,
        parentId: this.props.categoryId,
        id: this.state.itemId,
        name: this.state.name,
      })
      .then(res => {
        this.setState({
          loading: false,
          secondary: res.records,
          total: res.total,
          current: res.current,
        });
      });
  };
  //获取二级类目下的商品
  getGoodsList = () => {
    const { pageSize, pageNumber, name, itemId } = this.state;
    categoryService
      .getGoodsList({
        categoryId: this.props.categoryId,
        name,
        itemId,
        pageSize,
        pageNumber,
      })
      .then(res => {
        message.success('获取商品列表成功');
        this.setState({
          total: res.total,
          current: res.current,
          goodsList: res.records,
        });
      });
  };
  //分页，下一页
  onChange = pageNumber => {
    // alert(pageNumber)
    this.setState(
      {
        pageNumber: pageNumber,
      },
      () => {
        this.getGoods();
      },
    );
  };
  showTotal = () => {
    return `共有${this.state.total}条`;
  };
  //切换每页数量
  onShowSizeChange = (current, pageSize) => {
    this.setState(
      {
        pageSize: pageSize,
        pageNumber: 1,
      },
      () => {
        this.getGoods();
      },
    );
  };
  onSelectChange = (selectedRowKeys, selectedRows) => {
    this.setState({ selectedRowKeys });
  };
  cascaderOnChange = (value, selectedOptions) => {
    this.setState({
      opeCategoryId: value[1],
    });
  };
  //添加选定商品
  addSelectGoods = () => {
    if (!this.state.opeCategoryId) {
      message.error('请选择添加的类目');
      return false;
    }
    this.props.dispatch({
      type: 'category/addGoods',
      payload: {
        itemIds: this.state.selectedRowKeys,
        opeCategoryId: this.state.opeCategoryId,
      },
      callback: res => {
        if (res.data.msg === '操作成功') {
          this.props.onCancel();
        } else {
          message.error(res.data.msg);
        }
      },
    });
  };
  //查询的
  handleCondition = e => {
    this.setState({
      keyWord: e.target.value,
    });
  };
  //查询
  search = () => {
    this.getGoods();
  };
  // 重置
  reset = () => {
    this.setState(
      {
        pageSize: 3,
        pageNumber: 1,
        keyWord: '',
        name: '',
        itemId: '',
      },
      () => {
        this.getGoods();
      },
    );
  };
  render() {
    const {
      total,
      current,
      selectedRowKeys,
      keyWord,
      name,
      itemId,
      goods,
      goodsList,
      select,
    } = this.state;
    const { score } = this.props;
    let newSelect =
      select &&
      select.map(item => {
        if (item.children.length === 0) {
          item.disabled = true;
        }
        return item;
      });

    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    return (
      <Card bordered={false}>
        <Row
          type="flex"
          align="middle"
          gutter={8}
          style={{ display: score === '添加类目商品' ? 'blocl' : 'none' }}
        >
          <Col>类目选择：</Col>
          <Col>
            <Cascader options={newSelect} onChange={this.cascaderOnChange} />
          </Col>
          <Col>
            <Button
              type="primary"
              onClick={this.addSelectGoods}
              disabled={selectedRowKeys.length > 0 ? false : true}
            >
              添加选定商品
            </Button>
          </Col>
        </Row>
        <Row type="flex" align="middle" gutter={8} style={{ marginTop: 10 }}>
          <Col>查询：</Col>
          {score === '添加类目商品' ? (
            <Col>
              <Input
                onChange={this.handleCondition}
                placeholder="请输入店铺查询内容"
                value={keyWord}
              />
            </Col>
          ) : null}
          {score === '查看商品列表' ? (
            <Fragment>
              <Col>
                <Input
                  onChange={e => this.setState({ itemId: e.target.value })}
                  placeholder="ID"
                  value={itemId}
                />
              </Col>
              <Col>
                <Input
                  onChange={e => this.setState({ name: e.target.value })}
                  placeholder="名称"
                  value={name}
                />
              </Col>
            </Fragment>
          ) : null}
          <Col>
            <Button type="primary" onClick={this.search}>
              查询
            </Button>
          </Col>
          <Col>
            <Button onClick={this.reset}>重置</Button>
          </Col>
        </Row>
        <Table
          columns={this.listColumns}
          // rowSelection={score === '添加类目商品' ? rowSelection : null}
          dataSource={this.state.secondary}
          style={{ marginTop: 20 }}
          rowKey={record => record.id}
          pagination={{
            current: current,
            total: total,
            defaultPageSize: 3,
            onChange: this.onChange,
            showTotal: this.showTotal,
            showQuickJumper: true,
            pageSizeOptions: ['3', '5', '10'],
            showSizeChanger: true,
            onShowSizeChange: this.onShowSizeChange,
          }}
        ></Table>
      </Card>
    );
  }
}

export default Goods;
