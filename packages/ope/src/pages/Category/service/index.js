import request from "@/services/baseService";

export default {
  getCategory:(data)=>{
    return request(`/hzsx/examineProduct/findCategories`, data, 'get')
  },
  selectParentCategoryList:(data)=>{
    return request(`/hzsx/category/selectParentCategoryList`, data, 'get')
  },
  getSecondaryCategory:(data)=>{
    return request(`/hzsx/category/selectReceptionCategoryList`, data, 'get')
  },
  queryOpeCategoryPage:(data)=>{
    return request(`/hzsx/category/queryOpeCategoryPage`, data)
  },
  addFirstCategory:(data)=>{
    return request(`/hzsx/category/addOperateCategory`, data)
  },
  deleteCategory:(data)=>{
    return request(`/hzsx/category/removeOperateCategory`, data, 'get')
  },
  editFirstCategory:(data)=>{
    return request(`/hzsx/category/updateOperateCategory`, data)
  },
  getSelectAll:(data)=>{
    return request(`/hzsx/category/selectAll`, data, 'get')
  },
  getGoods:(data)=>{
    return request(`/hzsx/category/checkedProduct`, data)
  },
  addGoods:(data)=>{
    return request(`/hzsx/category/manyInsertCategoryProduct`, data)
  },
  getGoodsList:(data)=>{
    return request(`/hzsx/category/selectOperateCategoryProduct`, data)
  },
  deleteCategoryGoods:(data)=>{
    return request(`/hzsx/category/deleteProduct`, data, 'get')
  },
}

