import * as servicesApi from '../service/index'
export default {
  spacename: 'category',
  state: {
    category: [],
    secondary: [],
    synchronizeSecondary: [],
    select: [],
    goods: [],
    goodsList: []
  },
  effects: {
    // 获取一级类别
    *getCategory({ payload, callback }, { select, put, call }) {
      let res = yield call(servicesApi.getCategory, payload)

      if (res.msg === '操作成功') {
        yield put({
          type: 'saveCategory',
          data: res.data
        })
      }
      callback(res)
    },
    //获取二级类目
    *getSecondaryCategory({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.getSecondaryCategory, payload)
      if (res.msg === '操作成功') {
        yield put({
          type: 'saveSecondary',
          data: res.data,
        })
      }
      callback(res)
    },
    *getSynchronizeSecondaryCategory({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.getSecondaryCategory, payload)
      if (res.msg === '操作成功') {
        yield put({
          type: 'saveSynchronizeSecondary',
          data: res.data,
          value: payload.name
        })
      }
      callback(res)
    },

    //添加一级类目
    *addOperateCategory({ payload, callback }, { call }) {
      let res = yield call(servicesApi.addFirstCategory, payload)
      callback(res)
    },

    //删除一级类目
    *deleteCategory({ payload, callback }, { call }) {
      let res = yield call(servicesApi.deleteCategory, payload)
      callback(res)
    },

    //修改
    *editFirstCategory({ payload, callback }, { call }) {
      let res = yield call(servicesApi.editFirstCategory, payload)
      callback(res)
    },
    *editSecondCategory({ payload, callback }, { call }) {
      let res = yield call(servicesApi.editFirstCategory, payload)
      callback(res)
    },


    //goods组件的
    //添加前台类目商品的当前渠道的类别
    *getSelectAll({ payload, callback }, { call, put }) {
      let res = yield call(servicesApi.getSelectAll, payload)
      if (res.msg === '操作成功') {
        yield put({
          type: 'saveSelect',
          data: res.data
        })
      }
      callback(res)
    },
    //获取可添加的商品
    *getGoods({ payload, callback }, { call, put }) {
      let res = yield call(servicesApi.getGoods, payload)
      if (res.msg === '操作成功') {
        yield put({
          type: 'saveGoods',
          data: res.data
        })
      }
      callback(res)
    },
    //往指定类目添加商品
    *addGoods({ payload, callback }, { call, put }) {
      let res = yield call(servicesApi.addGoods, payload)
      callback(res)
    },
    //获取二级类目下的商品
    *getGoodsList({ payload, callback }, { call, put }) {
      let res = yield call(servicesApi.getGoodsList, payload)
      if (res.msg === '操作成功') {
        yield put({
          type: 'saveGoodsList',
          data: res.data
        })
      }
      callback(res)
    },
    //删除指定类目下的商品
    *deleteCategoryGoods({ payload, callback }, { call, put }) {
      let res = yield call(servicesApi.deleteCategoryGoods, payload)
      callback(res)
    },

  },
  reducers: {
    saveCategory(state, payload) {
      let _state = JSON.parse(JSON.stringify(state))
      _state.category = payload.data
      return _state
    },
    saveSecondary(state, payload) {
      let _state = JSON.parse(JSON.stringify(state))
      _state.secondary = payload.data
      return _state
    },
    saveSynchronizeSecondary(state, payload) {
      let _state = JSON.parse(JSON.stringify(state))
      let a = [{
        name: payload.value,
        data: payload.data
      }]
      let synchronizeSecondary = _state.synchronizeSecondary
      _state.synchronizeSecondary = synchronizeSecondary.concat(a)
      return _state
    },
    saveSelect(state, payload) {
      let _state = JSON.parse(JSON.stringify(state))
      _state.select = payload.data
      return _state
    },
    saveGoods(state, payload) {
      let _state = JSON.parse(JSON.stringify(state))
      _state.goods = payload.data.records
      return _state
    },
    saveGoodsList(state, payload) {
      let _state = JSON.parse(JSON.stringify(state))
      _state.goodsList = payload.data.records
      return _state
    },
    changeSynchronizeSecondary(state, payload) {
      let _state = JSON.parse(JSON.stringify(state))
      _state.synchronizeSecondary = _state.synchronizeSecondary.filter(item => item.name !== payload.payload.name)
      return _state
    },
  },
};
