import request from '@/services/baseService';

export default {
  querySpecialIndexPage: data => {
    return request(`/hzsx/special/querySpecialIndexPage`, data);
  },
  ableProductV1: data => {
    return request(`/hzsx/index/ableProductV1`, data, 'get');
  },
  saveSpikeSpecialPage: data => {
    return request(`/hzsx/special/saveSpikeSpecialPage`, data);
  },
  saveTopicSpecialPage: data => {
    return request(`/hzsx/special/saveTopicSpecialPage`, data);
  },
  saveDirectSpecialPage: data => {
    return request(`/hzsx/special/saveDirectSpecialPage`, data);
  },

  getSpikeSpecialPageDetail: data => {
    return request(`/hzsx/special/getSpikeSpecialPageDetail`, data, 'get');
  },
  getProductSkusDetail: data => {
    return request(`/hzsx/special/getProductSkusDetail`, data, 'get');
  },
  ableProductDirect: data => {
    return request(`/hzsx/examineProduct/ableProductDirect`, data, 'get');
  },

  modifySpecialIndex: data => {
    return request(`/hzsx/special/modifySpecialIndex`, data);
  },
  addSpecialIndex: data => {
    return request(`/hzsx/special/addSpecialIndex`, data);
  },
  getTopicSpecialPageDetail: data => {
    return request(`/hzsx/special/getTopicSpecialPageDetail`, data, 'get');
  },
  getDirectSpecialPageDetail: data => {
    return request(`/hzsx/special/getDirectSpecialPageDetail`, data, 'get');
  },
  deleteSpecialIndex: data => {
    return request(`/hzsx/special/deleteSpecialIndex`, data, 'get');
  },
};
