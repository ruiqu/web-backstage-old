import * as servicesApi from '../services'

export default {
  spacename: 'tab',
  state: {
    tab: [],
    tabTransfer: [],
    product: [],
    allProductGoods: []
  },
  effects: {
    //获取tab
    *getTab({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.getTab)
      if (res.msg === '操作成功') {
        yield put({
          type: 'saveTab',
          payload: res.data
        })
      }
      callback(res)
    },
    //获取tab,Transfer组件用到的
    *getTabTransfer({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.getTab, payload)
      if (res.msg === '操作成功') {
        yield put({
          type: 'saveTabTransfer',
          payload: res.data
        })
      }
      callback(res)
    },
    //添加tab
    *addTab({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.addTab, payload)
      callback(res)
    },
    //删除tab
    *deleteTab({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.deleteTab, payload)
      callback(res)
    },
    //编辑tab
    *updateTab({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.updateTab, payload)
      callback(res)
    },
    //删除tab下面的产品
    *deleteProduct({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.deleteProduct, payload)
      callback(res)
    },
    //更新tab下的产品
    *updateProduct({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.updateProduct, payload)
      callback(res)
    },
    //获取tab下面的产品
    *getProduct({ payload, callback }, { call, put }) {
      let res = yield call(servicesApi.getProduct, payload)
      if (res.msg === '操作成功') {
        yield put({
          type: 'saveProduct',
          payload: res.data
        })
      }
      callback(res)
    },
    //获取所有可增加的tab产品，product组件用的
    *getProductGoods({ payload, callback }, { call, put }) {
      let res = yield call(servicesApi.getProductGoods, payload)
      if (res.msg === '操作成功') {
        yield put({
          type: 'saveAllProductGoods',
          payload: res.data
        })
      }
      callback(res)
    },
    //product组件的添加
    *addProduct({ payload, callback }, { call, put }) {
      let res = yield call(servicesApi.addProduct, payload)
      callback(res)
    },
    //获取上架信息
    *getChannel({ payload, callback }, { call, put }) {
      const response = yield call(servicesApi.getChannel, payload);
      if (response.data.code == 1) {
        callback(response.data.data);
      }
    }

  },
  reducers: {
    saveTab(state, payload) {
      let _state = JSON.parse(JSON.stringify(state))
      _state.tab = payload.payload.data.records
      return _state
    },
    saveTabTransfer(state, payload) {
      let _state = JSON.parse(JSON.stringify(state))
      _state.tabTransfer = payload.payload.data.records
      return _state
    },
    saveProduct(state, payload) {
      let _state = JSON.parse(JSON.stringify(state))
      _state.product = payload.payload.records
      return _state
    },
    saveAllProductGoods(state, payload) {
      let _state = JSON.parse(JSON.stringify(state))
      _state.allProductGoods = payload.payload.records
      return _state
    }
  }
}
