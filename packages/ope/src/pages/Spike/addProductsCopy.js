// 添加前台类目商品用的
import React, { PureComponent, Fragment } from 'react';
import { Card, Button, Input, Form, Table, Row, Col, Select, Modal, message } from 'antd';
import { connect } from 'dva';
import { cloneDeep } from 'lodash';
import SpikeService from './services';
import CopyToClipboard from '@/components/CopyToClipboard';

const { Option } = Select;

@connect()
class Products extends PureComponent {
  state = {
    pageSize: 10,
    pageNumber: 1,
    content: '',
    total: 0,
    current: 1,
    selectedRows: [],
    productData: [],
  };

  columns = [
    {
      title: '商品ID',
      dataIndex: 'productId',
      align: 'center',
      width: 100,
      render: text => <CopyToClipboard text={text} />,
    },
    {
      title: '商品名称',
      dataIndex: 'productName',
      align: 'center',
      width: 100,
    },
    {
      title: 'SKUID',
      dataIndex: 'skuId',
      align: 'center',
      width: 100,
    },
    {
      title: '规格',
      dataIndex: 'skuSpe',
      align: 'center',
      width: 100,
    },
    {
      title: '租期',
      dataIndex: 'days',
      align: 'center',
      width: 80,
      render: (text, record, rowIndex) => {
        const value = record.days || (record.cyclePrices && record.cyclePrices[0].days);
        return (
          <div>
            <Select
              value={value}
              className="w-80"
              onChange={value => this.changeInputValue(value, rowIndex, 'days')}
            >
              {(record.cyclePrices || []).map(option => {
                return (
                  <Select.Option key={option.days} value={option.days}>
                    {option.days}天
                  </Select.Option>
                );
              })}
            </Select>
          </div>
        );
      },
    },
    {
      title: '商品价格',
      dataIndex: 'salePrice',
      align: 'center',
      width: 100,
      render: (text, record) => {
        const index = record.cyclePrices.findIndex(f => f.days === record.days);
        if (index > -1) {
          return `${record.cyclePrices[index].price}元/天`;
        }
        return `${record.cyclePrices[0].price}元/天`;
      },
    },
    {
      title: '限时价格',
      dataIndex: 'price',
      align: 'center',
      render: (text, record, rowIndex) => {
        return (
          <div>
            <Input
              placeholder="元/天"
              onChange={value => this.changeInputValue(value, rowIndex, 'price')}
            />
          </div>
        );
      },
    },
    {
      title: '库存',
      dataIndex: 'inventory',
      align: 'center',
      render: (text, record, rowIndex) => {
        return (
          <div>
            <Input
              placeholder="数量"
              onChange={value => this.changeInputValue(value, rowIndex, 'inventory')}
            />
          </div>
        );
      },
    },
    {
      title: '展示标签',
      dataIndex: 'lable',
      align: 'center',
      render: (text, record, rowIndex) => {
        return (
          <div>
            <Input
              placeholder="请输入"
              onChange={value => this.changeInputValue(value, rowIndex, 'lable')}
            />
          </div>
        );
      },
    },
  ];

  scrollColumns = [
    {
      title: '商品ID',
      dataIndex: 'productId',
      align: 'center',
      render: text => <CopyToClipboard text={text} />,
    },
    {
      title: '商品名称',
      dataIndex: 'productName',
      align: 'center',
    },
    {
      title: '商品价格',
      dataIndex: 'price',
      align: 'center',
      width: 100,
    },
  ];

  labelColumns = [
    {
      title: '商品ID',
      dataIndex: 'productId',
      align: 'center',
      render: text => <CopyToClipboard text={text} />,
    },
    {
      title: '商品名称',
      dataIndex: 'productName',
      align: 'center',
    },
    {
      title: '商品价格',
      dataIndex: 'price',
      align: 'center',
      width: 100,
    },
    {
      title: '选择所属标签',
      dataIndex: 'label',
      align: 'center',
      render: (text, record, rowIndex) => {
        const tabs = this.props.labels;
        return (
          <div>
            <Select
              value={record.label}
              className="w-80"
              onChange={value => this.changeInputValue(value, rowIndex, 'label')}
            >
              {(tabs || []).map(option => {
                return (
                  <Select.Option key={option} value={option}>
                    {option}
                  </Select.Option>
                );
              })}
            </Select>
          </div>
        );
      },
    },
  ];

  componentDidMount() {
    this.getProductSkusDetail();
  }

  getProductSkusDetail = () => {
    const { pageNumber, pageSize, content } = this.state;
    const { type } = this.props;

    SpikeService.ableProductDirect({
      pageNumber,
      pageSize,
      content,
      keyWord: content || null,
    }).then(res => {
      this.setState({
        productData: res.records || [],
        total: res.total,
        current: res.current,
      });
    });
  };

  changeInputValue = (value, rowIndex, field) => {
    const { selectedRows, productData } = this.state;
    const _productData = cloneDeep(productData);
    const rowData = _productData[rowIndex];
    let _value = value;
    if (value.target) {
      _value = value.target.value;
    }
    rowData[field] = _value;
    // const selectIndex = selectedRows.findIndex(f => f.productId === rowData.productId);
    const selectIndex = selectedRows.findIndex(f => f.skuId === rowData.skuId);
    if (selectIndex > -1) {
      selectedRows[selectIndex] = rowData;
      this.setState({
        selectedRows,
      });
    }
    this.setState({
      productData: _productData,
    });
  };

  //分页，下一页
  onChange = pageNumber => {
    this.setState(
      {
        pageNumber: pageNumber,
      },
      () => {
        this.getProductSkusDetail();
      },
    );
  };
  showTotal = () => {
    return `共有${this.state.total}条`;
  };

  //切换每页数量
  onShowSizeChange = (current, pageSize) => {
    this.setState(
      {
        pageSize: pageSize,
        pageNumber: 1,
      },
      () => {
        this.getProductGoods();
      },
    );
  };
  submit = () => {
    const { selectedRows } = this.state;
    const { selectProducts, type } = this.props;
    let hasError = false;
    if (type === 'labelProducts') {
      hasError = selectedRows.some(row => !row.label);
      if (hasError) {
        message.warning('请选择已选项的所属标签');
      }
    }
    if (!hasError) {
      selectProducts(selectedRows);
    }
  };
  onSelectChange = (selectedRowKeys, selectedRows) => {
    console.log(selectedRows);
    this.setState({ selectedRows });
  };

  //查询的
  handleKeyWords = e => {
    this.setState({
      content: e.target.value,
    });
  };
  //查询
  search = () => {
    this.setState(
      {
        pageNumber: 1,
      },
      () => {
        this.getProductSkusDetail();
      },
    );
  };

  render() {
    const { total, current, selectedRows, content, productData } = this.state;
    const { visible, scrollProductCancel, selectProducts, type } = this.props;
    const rowSelection = {
      selectedRows,
      onChange: this.onSelectChange,
    };
    const columnMap = {
      spike: this.columns,
      labelProducts: this.labelColumns,
      products: this.scrollColumns,
      productss: this.scrollColumns,
    };
    const onCancel = () => {
      scrollProductCancel();
      this.setState({
        selectedRows: [],
      });
    };
    return (
      <Modal
        visible={visible}
        title="选择商品"
        destroyOnClose
        onCancel={onCancel}
        // onCancel={scrollProductCancel}
        onOk={this.submit}
        width={1062}
      >
        <Card bordered={false}>
          <Row type="flex" align="middle" gutter={8} style={{ marginTop: 10 }}>
            <Col>
              <Input
                style={{ width: 260 }}
                onChange={this.handleKeyWords}
                placeholder="请输入"
                value={content}
              />
            </Col>
            <Col>
              <Button type="primary" onClick={this.search}>
                查询
              </Button>
            </Col>
          </Row>
          <Table
            columns={columnMap[type]}
            rowSelection={rowSelection}
            dataSource={productData}
            style={{ marginTop: 20 }}
            rowKey={record => record.productId}
            pagination={{
              current: current,
              total: total,
              defaultPageSize: 10,
              onChange: this.onChange,
              showTotal: this.showTotal,
              showQuickJumper: true,
              pageSizeOptions: ['5', '10', '20'],
              showSizeChanger: true,
              onShowSizeChange: this.onShowSizeChange,
            }}
          ></Table>
        </Card>
      </Modal>
    );
  }
}

export default Products;
