import request from "@/services/baseService";

export default {
  getTab:(data)=>{
    return request(`/hzsx/index/getIndexTabListByPage?&pageNumber=1&pageSize=10`, data, 'get')
  },
  addTab:(data)=>{
    return request(`/hzsx/index/saveIndexTab`, data)
  },
  deleteTab:(data)=>{
    return request(`/hzsx/index/delIndexTab`, data, 'get')
  },
  updateTab:(data)=>{
    return request(`/hzsx/index/updateIndexTabById`, data)
  },
  deleteProduct:(data)=>{
    return request(`/hzsx/index/delIndexPrdouct`, data, 'get')
  },
  getProduct:(data)=>{
    return request(`/hzsx/index/getIndexProductListByPage`, {...data, tabId: data.id}, 'get')
  },
  updateProduct:(data)=>{
    return request(`/hzsx/index/getIndexProductListByPage`, {...data, tabId: data.id}, 'get')
  },
  getProductGoods:(data)=>{
    return request(`/hzsx/index/ableProduct`, data, 'get')
  },
  addProduct:(data)=>{
    return request(`/hzsx/index/saveIndexPrdouct`, data)
  },
  updateIndexPrdouctSort:(data)=>{
    return request(`/hzsx/index/updateIndexPrdouctSort`, data)
  },

}
