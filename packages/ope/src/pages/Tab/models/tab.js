import servicesApi from '../services'

export default {
  spacename: 'tab',
  state: {
    tab: [],
  },
  effects: {
    //获取tab
    *getTab({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.getTab)
      yield put({
        type: 'saveTab',
        payload: res.records
      })
      callback(res)
    },
  },
  reducers: {
    saveTab(state, {payload}) {
      return {...state, tab: payload}
    },
  }
}
