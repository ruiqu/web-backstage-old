// 添加前台类目商品用的
import React, { PureComponent, Fragment } from 'react'
import {
  Card,
  Button,
  Input,
  Form,
  Table,
  Row,
  Col,
  message,
  Cascader,
  Popconfirm
} from 'antd'
import { connect } from 'dva'
import tabService from './services'

@connect(({tab}) => ({
  ...tab
}))

class Products extends PureComponent {
  state = {
    pageSize: 10,
    pageNumber: 1,
    keyWords: '',
    total: 0,
    current: 1,
    selectedRows: [],
    allProductGoods: []
  }

  columns = [
    {
      title: '商品编号',
      dataIndex: 'itemId',
      key: 'itemId',
      align: 'center',
      width: 160
    },
    {
      title: '商品名称',
      dataIndex: 'productName',
      key: 'productName',
      align: 'center'
    },
    {
      title: '店铺名称',
      dataIndex: 'shopName',
      key: 'shopName',
      align: 'center',
      width: 120
    },
  ]

  componentDidMount () {
    this.getProductGoods()
  }

  getProductGoods = () => {
    tabService.getProductGoods({
      pageNumber: this.state.pageNumber,
      pageSize: this.state.pageSize,
      productId: this.state.keyWords,
    }).then(res => {
      this.setState({
        total: res.total,
        current: res.current,
        allProductGoods: res.records
      })
    })
  }

  // 分页，下一页
  onChange = pageNumber => {
    // alert(pageNumber)
    this.setState({
      pageNumber,
    },() => {
      this.getProductGoods()
    })
  }

  showTotal = () => {
    return `共有${this.state.total}条`;
  }

  // 切换每页数量
  onShowSizeChange = (current, pageSize) => {
    this.setState({
      pageSize,
      pageNumber: 1,
    },() => {
      this.getProductGoods()
    })
  }

  onSelectChange = (selectedRowKeys, selectedRows) => {
    this.setState({ selectedRows })
  }

  // 查询的
  handleKeyWords = (e) => {
    this.setState({
      keyWords: e.target.value
    })
  }

  // 查询
  search = () => {
    this.getProductGoods()
  }

  // 重置
  reset = () => {
    this.setState({
      pageSize: 10,
      pageNumber: 1,
      keyWords: '',
    },() => {
      this.getProductGoods()
    })
  }

  // 新增
  add = () => {
    const newPayload = this.state.selectedRows.filter((item,index) => {
      // item.channel = this.props.channel
      item.tabId = Number(this.props.tabActiveKey)
      item.name = item.productName
      item.indexSort = index
      delete item.productName
      delete item.status
      return item
    })
    tabService.addProduct(newPayload).then(res => {
      message.success('添加成功')
      this.props.tabProductCancel()
      this.props.onTabChange(this.props.tabActiveKey)
    })
  }

  render () {
    const { total, current, selectedRows, keyWords, allProductGoods } = this.state
    const rowSelection = {
      selectedRows,
      onChange: this.onSelectChange,
    }
    return (
      <div>
        <Row type='flex' align='middle' gutter={8} style={{marginTop:10}}>
          <Col>查询：</Col>
            <Col>
              <Input style={{width:260}} onChange={this.handleKeyWords}
                     placeholder='请输入' value={keyWords} />
            </Col>
          <Col>
            <Button type='primary' onClick={this.search}>查询</Button>
          </Col>
          <Col>
            <Button onClick={this.reset}>重置</Button>
          </Col>
          <Col>
            <Button type='primary' disabled={selectedRows.length <= 0 } onClick={this.add}>新增</Button>
          </Col>
        </Row>
        <Table
          columns={this.columns}
          rowSelection={rowSelection}
          dataSource={allProductGoods}
          style={{marginTop:20}}
          rowKey={record => record.itemId}
          pagination={{
            current,
            total,
            defaultPageSize: 10,
            onChange: this.onChange,
            showTotal: this.showTotal,
            showQuickJumper: true,
            pageSizeOptions: ['5', '10', '20'],
            showSizeChanger: true,
            onShowSizeChange: this.onShowSizeChange,
          }}
         />
      </div>
    )
  }
}

export default Products
