import request from '@/services/baseService';

export default {
  //分页
  page: data => {
    return request(`/hzsx/productDistribute/page`, data, 'post');
  },
  //导入
  importAdd: data => {
    return request(`/hzsx/productDistribute/importAdd`, data, 'post');
  },
  //添加
  addCategory: data => {
    return request(`/hzsx/productDistribute/add`, data, 'post');
  },
  //根据虚拟ID获取配置
  getByVirtualProductId: data => {
    return request(`/hzsx/productDistribute/getByVirtualProductId`, data, 'get');
  },
  //更新
  update: data => {
    return request(`/hzsx/productDistribute/update`, data, 'post');
  },
  //删除
  deleteCategory: data => {
    return request(`/hzsx/productDistribute/delete`, data, 'get');
  },
  //添加支付宝商品ID
  updateRemark: data => {
    return request(`/hzsx/productDistribute/updateRemark`, data, 'post');
  },
};
