import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import {
  Upload,
  Button,
  Card,
  Modal,
  Form,
  Input,
  message,
  Divider,
  Popconfirm,
  Table,
} from 'antd';
import SearchList from '@/components/SearchList';
import channelService from './service';
const { Dragger } = Upload;
const { TextArea } = Input;
@Form.create()
export default class channel extends Component {
  state = {
    current: 1,
    fileData: [],
    total: 1,
    words: [],
    itemNumber: undefined,
    visibleTitle: '',
    virtualProductId: '',
    zfbValue: undefined,
  };
  handleFilter = (pageNumber, pageSize, data = {}) => {
    this.setState(
      {
        loading: true,
      },
      () => {
        channelService
          .page({
            pageNumber,
            pageSize,
            ...data,
          })
          .then(res => {
            if (res) {
              let data = res.records || [];
              const tableData = [];
              data.map(item => {
                if (item.infoList && item.infoList.length) {
                  item.infoList.map((item2, index2) => {
                    for (let key in item2) {
                      item2[`list_${key}`] = item2[key];
                      delete item2[key];
                    }

                    tableData.push({
                      rowSpan: index2 === 0 ? item.infoList.length : 0,
                      ...item,
                      ...item2,
                    });
                  });
                } else {
                  tableData.push({
                    rowSpan: 1,
                    ...item,
                  });
                }
              });
              this.setState({
                list: tableData,
                total: res.total,
                loading: false,
                current: res.current,
              });
            }
          });
      },
    );
  };
  showModal = () => {
    this.setState({
      visible: true,
      visibleTitle: '新增',
    });
  };

  handleOk = e => {
    e.preventDefault();
    const { words, visibleTitle, virtualProductId, current } = this.state;
   let words_ = words.find(item => Boolean(item.weights) == false)
   if(words_){
     message.warning("权重不能为空")
     return
   }
    if (words.length) {
      if (visibleTitle === '新增') {
        channelService.addCategory(words).then(res => {
          if (res) {
            message.success('新建成功');
            this.handleFilter(1, 10);
            this.handleCancel();
          }
        });
      } else {
        words.map(item => (item.virtualProductId = virtualProductId));
        channelService.update(words).then(res => {
          if (res) {
            message.success('修改成功');
            this.handleFilter(current, 10);
            this.handleCancel();
          }
        });
      }
    } else {
      message.success('请添加');
    }
  };
  delete = virtualProductId => {
    const { current } = this.state;
    channelService.deleteCategory({ virtualProductId }).then(res => {
      if (res) {
        message.success('删除成功');
        this.handleFilter(current, 10);
      }
    });
  };
  handleCancel = e => {
    this.setState({
      visible: false,
      words: [],
      itemNumber: undefined,
    });
  };
  uploadCancel = e => {
    this.setState({
      uploadVisible: false,
    });
  };

  onUpload = e => {
    if (e.fileList.length !== 0) {
      const file = e.fileList[e.fileList.length - 1].originFileObj;
      let formdata = new FormData();
      formdata.append('file', file);
      this.setState({
        formdata,
        fileData: [e.fileList[e.fileList.length - 1]],
      });
    } else {
      this.setState({
        fileData: [],
      });
    }
  };
  uploadOk = e => {
    e.preventDefault();
    const { formdata } = this.state;
    channelService.importAdd(formdata).then(res => {
        message.success('导入成功');
        this.handleFilter(1, 10);
        this.setState({ uploadVisible: false });
    });
  };
  save = (e, index, type) => {
    const words = [...this.state.words];
    words[index][type] = e.currentTarget.value;
    this.setState({
      words,
    });
  };
  addTempKeyword = () => {
    const { itemNumber } = this.state;
    const words = [...this.state.words];
    if (!itemNumber) {
      message.success('请输入商品');
    } else {
      words.push({
        realProductId: itemNumber,
        weights: '',
      });
      this.setState({
        words,
      });
    }
  };
  deleteTempKeyword = index => {
    const words = [...this.state.words];
    words.splice(index, 1);
    this.setState({
      words,
    });
  };
  onNumber = e => {
    this.setState({ itemNumber: e.target.value });
  };
  edit = e => {
    channelService.getByVirtualProductId({ virtualProductId: e.virtualProductId }).then(res => {
      if (res) {
        this.setState({
          words: res,
          visible: true,
          visibleTitle: '修改',
          virtualProductId: e.virtualProductId,
        });
      }
    });
  };
  zfbOk = () => {
    const { zfbValue, virtualProductId } = this.state;
    if (zfbValue) {
      channelService.updateRemark({ virtualProductId, remark: zfbValue }).then(res => {
        this.setState({ zfbVisible: false });
        message.success("修改成功")
      });
    } else {
      message.warning('请输入');
    }
  };
  upZfb = e => {
    channelService.getByVirtualProductId({ virtualProductId: e.virtualProductId }).then(res => {
      if (res) {
        this.setState({
          zfbValue: res[0]?.remark,
          zfbVisible: true,
          virtualProductId: e.virtualProductId,
        });
      }
    });
  };
  onZfb = e => {
    this.setState({ zfbValue: e.target.value });
  };
  render() {
    const columns = [
      {
        title: '虚拟商品id',
        render: (text, e) => {
          return {
            children: e.virtualProductId,
            props: {
              rowSpan: e.rowSpan,
            },
          };
        },
      },
      {
        title: '关联商品编号',
        render: (text, e) => {
          return {
            children: e.list_realProductId,
            props: {
              rowSpan: 1,
            },
          };
        },
      },
      {
        title: '商品名称',
        width: 300,
        render: (text, e) => {
          return {
            children: e.list_productName,
            props: {
              rowSpan: 1,
            },
          };
        },
      },
      {
        title: '店铺名称',
        render: (text, e) => {
          return {
            children: e.list_shopName,
            props: {
              rowSpan: 1,
            },
          };
        },
      },

      {
        title: '分配权重',
        render: (text, e) => {
          return {
            children: e.list_weights,
            props: {
              rowSpan: 1,
            },
          };
        },
      },
      {
        title: '操作',
        width: 240,
        render: e => {
          return {
            children: (
              <>
                <a onClick={() => this.edit(e)}>修改</a>
                <Divider type="vertical" />
                <Popconfirm title="确定删除吗？" onConfirm={() => this.delete(e.virtualProductId)}>
                  <a>删除</a>
                </Popconfirm>
                <Divider type="vertical" />
                <a onClick={() => this.upZfb(e)}>支付宝商品id</a>
              </>
            ),
            props: {
              rowSpan: e.rowSpan,
            },
          };
        },
      },
    ];

    let searchHander = [
      {
        label: '虚拟商品id',
        type: 'input',
        key: 'virtualProductId',
      },
      {
        label: '关联商品编号',
        type: 'input',
        key: 'realProductId',
      },
      {
        label: '支付宝商品id',
        type: 'input',
        key: 'remark',
      },
    ];
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 },
      },
    };
    const editColumns = [
      {
        title: '商品编号',
        dataIndex: 'realProductId',
        width: '40%',
      },
      {
        title: '权重',
        dataIndex: 'weights',
        width: '40%',
        render: (text, record, index) => (
          <Input
            defaultValue={text}
            onPressEnter={e => this.save(e, index, 'weights')}
            onBlur={e => this.save(e, index, 'weights')}
            // onChange={e => this.save(e, index, 'weights')}
          />
        ),
      },
      {
        title: '操作',
        dataIndex: 'operation',
        align: 'center',
        width: '20%',
        render: (text, record, index) => <a onClick={() => this.deleteTempKeyword(index)}>删除</a>,
      },
    ];
    let {
      loading,
      list = [],
      total,
      visible,
      uploadVisible,
      words,
      itemNumber,
      visibleTitle,
      virtualProductId,
      zfbVisible,
      zfbValue,
    } = this.state;
    const { getFieldDecorator } = this.props.form;
    return (
      <PageHeaderWrapper title={false}>
        <Card bordered={false}>
          <SearchList
            handleFilter={this.handleFilter}
            columns={columns}
            searchHander={searchHander}
            loading={loading}
            total={total}
            list={list}
            pagination={false}
            pageSize={10}
            paginationType={true}
          >
            <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
              <Button
                type="primary"
                onClick={this.showModal}
                style={{ width: 111, marginBottom: 10 }}
              >
                新增
              </Button>
              <Button
                type="primary"
                onClick={() =>
                  (location.href =
                    'https://booleandata-zuwuzu.oss-cn-beijing.aliyuncs.com/channel/zwpdmb.xlsx')
                }
                style={{ width: 111, marginBottom: 10 }}
              >
                下载模版
              </Button>
              <Button
                type="primary"
                onClick={() => this.setState({ uploadVisible: true })}
                style={{ width: 111, marginBottom: 10 }}
              >
                导入
              </Button>
            </div>
          </SearchList>
        </Card>
        <Modal
          title={visibleTitle}
          visible={visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          destroyOnClose
        >
          {visibleTitle === '修改' && (
            <div style={{ marginBottom: 20, fontWeight: 700 }}>虚拟商品id： {virtualProductId}</div>
          )}
          <div style={{ marginBottom: 20 }}>
            <Input
              placeholder="请输入商品编号"
              style={{ width: 300 }}
              onChange={e => this.onNumber(e)}
              value={itemNumber}
            />
            <Button type="primary" style={{ marginLeft: 20 }} onClick={this.addTempKeyword}>
              添加
            </Button>
            <Button onClick={() => this.setState({ words: [] })}>清除</Button>
          </div>
          <Table
            bordered
            size="small"
            dataSource={words}
            rowKey={record => record.realProductId + Math.random()}
            columns={editColumns}
            pagination={false}
          />
        </Modal>
        <Modal
          title="导入"
          visible={uploadVisible}
          onOk={this.uploadOk}
          onCancel={this.uploadCancel}
        >
          <Dragger onChange={this.onUpload} fileList={this.state.fileData}>
            <div>
              <div>
                <img
                  src="https://booleandata-crmmanagement-front.oss-cn-beijing.aliyuncs.com/publicPool/downloadblock.png"
                  alt="upload"
                />
              </div>
              <div style={{ paddingTop: 20 }}>点击此处或者将文件拖拽到这里后点击上传</div>
            </div>
          </Dragger>
        </Modal>
        <Modal
          title="支付宝商品id"
          visible={zfbVisible}
          onOk={this.zfbOk}
          onCancel={() => this.setState({ zfbVisible: false })}
        >
          <TextArea
            placeholder="请输入"
            onChange={this.onZfb}
            autoSize={{ minRows: 3, maxRows: 5 }}
            value={zfbValue}
          />
        </Modal>
      </PageHeaderWrapper>
    );
  }
}
