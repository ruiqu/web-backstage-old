import request from "@/services/baseService";

export default {
  audit:(data)=>{
    return request(`/hzsx/splitBillConfig/audit`, data)
  },
  douyinAudit:(data)=> {
    return request(`/hzsx/splitBillConfigToutiao/audit`, data)
  }, // 抖音小程序的审批

  page:(data)=>{
    return request(`/hzsx/splitBillConfig/page`, data)
  },
  litePage:(data)=>{
    return request(`/hzsx/splitBillConfigLite/page`, data)
  },
  douyinPage:(data)=>{
    return request(`/hzsx/splitBillConfigToutiao/page`, data)
  }, // 抖音小程序的佣金列表

  getShopList:(data)=>{
    return request(`/hzsx/splitBillConfig/getShopList`, data, 'get')
  },
  getLiteShopList:(data)=>{
    return request(`/hzsx/splitBillConfigLite/getShopList`, data, 'get')
  },
  getDouyinShopList:(data)=>{
    return request(`/hzsx/splitBillConfigToutiao/getShopList`, data, 'get')
  },
  
  detail:(data)=>{
    return request(`/hzsx/splitBillConfig/detail`, data, 'get')
  },
  liteDetail:(data)=>{
    return request(`/hzsx/splitBillConfigLite/detail`, data, 'get')
  },
  douyinDetail:(data)=>{
    return request(`/hzsx/splitBillConfigToutiao/detail`, data, 'get')
  },

  add:(data)=>{
    return request(`/hzsx/splitBillConfig/add`, data)
  },
  liteAdd:(data)=>{
    return request(`/hzsx/splitBillConfigLite/add`, data)
  },
  douyinAdd:(data)=>{
    return request(`/hzsx/splitBillConfigToutiao/add`, data)
  },
  
  update:(data)=>{
    return request(`/hzsx/splitBillConfig/update`, data)
  },
  liteUpdate:(data)=>{
    return request(`/hzsx/splitBillConfigLite/update`, data)
  },
  douyinUpdate:(data)=>{
    return request(`/hzsx/splitBillConfigToutiao/update`, data)
  },

  configPage:(data)=>{
    return request(`/hzsx/config/page`, data)
  },
  configUpdate:(data)=>{
    return request(`/hzsx/config/update`, data)
  },
  
}
