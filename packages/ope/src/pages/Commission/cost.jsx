import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import MyPageTable from '@/components/MyPageTable';
import { onTableData } from '@/utils/utils.js';
import commissionService from './services';
import { Modal, Form, Input, message } from 'antd';
@Form.create({})
export default class cost extends Component {
  state = {
    current: 1,
    total: 1,
    list: [],
    value: '',
    id: null,
    visible: false,
  };
  componentDidMount() {
    this.onList(1, 10);
  }
  onList = (pageNumber, pageSize) => {
    commissionService.configPage({ pageNumber, pageSize }).then(res => {
      this.setState({
        list: res.records,
        total: res.total,
      });
    });
  };
  onPage = e => {
    this.setState(
      {
        current: e.current,
      },
      () => {
        this.onList(e.current, 10);
      },
    );
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { id, value } = this.state;
        commissionService.configUpdate({ id, value:values.note }).then(res => {
          if(res){
              message.success("修改成功")
              this.onList(this.state.current, 10);
              this.handleOk()
          }
        });
      }
    });
  };
  onXg = e => {
    this.setState(
      {
        value: e.value,
        id: e.id,
      },
      () => {
        this.showModal();
      },
    );
  };
  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = e => {
    this.setState({
      visible: false,
    });
  };

  handleCancel = e => {
    this.setState({
      visible: false,
    });
  };
  render() {
    const columns = [
      {
        title: '服务',
        dataIndex: 'name',
        width: '33%',
        align: 'center',
      },
      {
        title: '费用（元）',
        dataIndex: 'value',
        width: '33%',
        align: 'center',
      },
      // {
      //   title: '操作',
      //   width: '33%',
      //   align: 'center',
      //   render: e => <a onClick={() => this.onXg(e)}>修改</a>,
      // },
    ];
    const paginationProps = {
      current: this.state.current,
      pageSize: 10,
      total: this.state.total,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / 10)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };
    const { getFieldDecorator } = this.props.form;
    return (
      <PageHeaderWrapper title={false}>
        <MyPageTable
          dataSource={onTableData(this.state.list)}
          columns={columns}
          onPage={this.onPage}
          paginationProps={paginationProps}
        />
        <Modal
          title="修改"
          visible={this.state.visible}
          onOk={this.handleSubmit}
          onCancel={this.handleCancel}
        >
          <Form labelCol={{ span: 5 }} wrapperCol={{ span: 12 }}>
            <Form.Item label="费用">
              {getFieldDecorator('note', {
                rules: [{ required: true, message: '请输入费用!' }],
                initialValue: this.state.value,
              })(<Input />)}
            </Form.Item>
          </Form>
        </Modal>
      </PageHeaderWrapper>
    );
  }
}
