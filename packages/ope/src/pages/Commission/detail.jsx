import React, { PureComponent } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import {
  Card,
  Spin,
  Input,
  Divider,
  Form, Button, message,
} from 'antd';
import {goToRouter} from "@/utils/utils";
import { renderDescriptionsItems } from '@/components/LZDescriptions'
import {router} from "umi";
import Services from './services/index'


const typeMap = {
  'BUY_OUT': '买断',
  'RENT_MONTH': '月租金分账',

}
const getStatus = new Map([
  ['REJECT', { label: '审核拒绝', color: 'red'}],
  ['PENDING', {label: '待审核', color: 'yellow'}],
  ['PASS', {label: '审核通过', color: ''}],
  ['UNSET', {label: '未设置', color: ''}],
  [undefined, {label: ''}]
]);

@connect(({ commission }) => ({
  ...commission,
}))
@Form.create()
class CommissionDetail extends PureComponent {
  state = {
    audit: false,
    auditRemark: '',
    commissionDetail: {}
  };
  
  componentDidMount() {
    const currentPath = this.props.location.pathname
    const douyinScenePath = '/commission/douyin/commissionDetail/'
    this.usedInDouyin = currentPath.includes(douyinScenePath) // 是否使用在抖音场景
    const {
      match: {
        params: { id },
      },
    } = this.props;
    // 从财务管理使用场景进来的话，那么会携带上这个approve=1
    const {approve} = this.props.location.query
    if (approve === '1') {
      this.setState({
        audit: true
      })
    }
    if (id) {
      const fetchFunction = this.usedInDouyin ? Services.douyinDetail : Services.detail
      fetchFunction({
        id
      }).then(res => {
        this.setState({
          commissionDetail: res,
          auditRemark: res.account.auditRemark,
          id
        });
      })
    }
  }
  
  // 监听修改的处理方法
  handleEdit = () =>{
    const {dispatch,match: {
      params: { id },
    },} = this.props
    let goPage
    if (this.usedInDouyin) { // 使用在抖音的场景中
      goPage = `/commission/douyin/edit/${id}`
    } else {
      goPage = `/commission/list/edit/${id}`
    }
    goToRouter(dispatch, goPage)
  }
  
  handleRate = (value) => {
    return (value * 100).toFixed(2)
  }
  
  getTypeItems = (item)=>{
    const shop = this.handleRate(item.scale)
    const platform = this.handleRate(1 - item.scale)
    return [
      {label: '分拥类型', value: typeMap[item.type]},
      {label: '结算周期', value: `按${item.delayNum}个自然日`},
      {label: '分拥比例', value: `商家占比${shop}%；平台占比${platform}%`},
    ]
  }
  
  handleChange = (evt) => {
    this.setState({
      auditRemark: evt.target.value
    });
  }
  
  handleAudit = (isPass) => {
    const { auditRemark, id } = this.state
    const fetchCb = this.usedInDouyin ? Services.douyinAudit : Services.audit
    fetchCb({
      auditRemark,
      pass: isPass,
      id
    }).then(res => {
      message.success(isPass?'审核通过': '审核拒绝')
      router.push('/finance/audit')
    })
  }
  
  renderFooter = () => {
    const { auditRemark, audit, commissionDetail } = this.state
    const { account={}, rules=[] } = commissionDetail;
    if (account.status === 'PASS') {
      return null
    }
    if (audit) {
      return (
        <div className="footer">
          <div className="df">
            <span className="w70">审核意见:</span>
            <Input.TextArea value={auditRemark} placeholder="请输入说明"
                            className="w294 br-4" rows={4}
                            onChange={this.handleChange}/>
          </div>
          <div className="audit-footer-btns ml-70">
            <Button type="primary" htmlType="submit" onClick={()=>this.handleAudit(true)}>
              审核通过
            </Button>
            <Button onClick={()=>this.handleAudit(false)}>
              审核拒绝
            </Button>
          </div>
        </div>
      )
    }
    return (
      <div>
        <Button type="primary" onClick={this.handleEdit}>修改分佣设置</Button>
      </div>
    )
  }
  
  
  render() {
    const { commissionDetail = {} } = this.state;
    const { account={}, rules=[] } = commissionDetail;
    
    const requiredField = [
      {label: '企业资质名称', value: account.shopFirmInfo},
      {label: '店铺名称', value: account.shopName},
      {label: '分拥状态', value: getStatus.get(account.status).label},
      {label: '创建时间', value: account.createTime},
      {label: '店铺ID', value: account.shopId},
      {label: '平台支付宝账号', value: `${account.name}/${account.identity}`},
    ]
    const typeItems =()=> {
      return rules.reduce((pre, cur)=>{
        return pre.concat(this.getTypeItems(cur))
      }, [])
    }
    const auditField = [
      {label: '审核时间', value: account.auditTime},
      {label: '审核人', value: account.auditUser},
      {label: '审核意见', value: account.auditRemark},
    ]
    return (
      <PageHeaderWrapper title={'佣金详情'}>
        <Spin spinning={false}>
          <Card bordered={false} style={{ marginTop: 20 }}>
            {
              renderDescriptionsItems(requiredField)
            }
            <Divider />
            {
              renderDescriptionsItems(typeItems())
            }
            <Divider />
            {
              this.state.audit ? null: renderDescriptionsItems(auditField)
            }
            {
              this.renderFooter()
            }
          </Card>
        </Spin>
      </PageHeaderWrapper>
    );
  }
}

export default CommissionDetail;
