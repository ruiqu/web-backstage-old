import React, { PureComponent } from 'react';

import { connect } from 'dva';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import {downloadFile, goToRouter} from "@/utils/utils";
import {
  Table,
  Card,
  message,
  Button,
  Form,
  Divider,
} from 'antd';
import commissionService from './services'

import Search from '../../components/Search';

const getStatus = new Map([
  ['REJECT', { label: '审核拒绝', color: 'red'}],
  ['PENDING', {label: '待审核', color: 'yellow'}],
  ['PASS', {label: '审核通过', color: ''}],
  ['UNSET', {label: '未设置', color: ''}],
]);
const typeMap = {
  'BUY_OUT': '买断',
  'RENT_MONTH': '月租金分账',

}

@connect(({ commission }) => ({
  ...commission,
}))
@Form.create()
class CommissionIndex extends PureComponent {
  state = {
    tableData: [],
    loading: true,
    current: 1,
    pageNumber: 1,
    pageSize: 10,
    type: null,
    typeInfo: '', // 分佣类型数据
    name: '', // 商家负责人名
    shopName: null,
    status: null,
  };

  columns = [
    {
      title: '店铺编号',
      dataIndex: 'shopId',
      align: 'center',
    },
    {
      title: '店铺名称',
      dataIndex: 'shopName',
      align: 'center',
    },
    {
      title: '企业资质名称',
      dataIndex: 'shopFirmInfo',
      align: 'center',
      ellipsis: true
    },
    {
      title: '分佣类型',
      dataIndex: 'typeInfo',
      align: 'center',
      render: (text, record) => {
        const textArr = text.split(',')
        const result = textArr.map(v=>{
          return typeMap[v]
        })
        return result.join(',')
      }
    },
    {
      title: '分佣状态',
      dataIndex: 'status',
      align: 'center',
      render: text => {
        const item = getStatus.get(text)
        return (
          <span className={item.color}>
            {item.label}
          </span>
        )
      },
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      align: 'left',
      key: 'crateTime',
      width: '200'
    },
    {
      title: '操作',
      align: 'center',
      key: 'action',
      render: (text, record) => {
        return (
          <div className="table-action">
            <a onClick={() => this.edit(record.id)}>修改</a>
            <Divider type="vertical" />
            <a onClick={()=> this.preview(record.id)}>查看</a>
          </div>
        );
      },
    },
  ];

  componentDidMount() {
    this.initData();
  }

  initData = () => {
    const { pageNumber, pageSize, shopName, status, type, typeInfo, name } = this.state
    const payload = { pageNumber, pageSize, shopName, status, type };
    typeInfo && (payload.typeInfo = typeInfo); // 分佣类型有效
    name && (payload.name = name);

    this.setState(
      {
        loading: true,
      },
      () => {
        commissionService.litePage(payload).then(res => {
          message.success('获取信息成功');
          this.setState({
            tableData: res.records,
            total: res.total,
            loading: false,
            current: res.current,
          });
        })
      },
    );
  };

  handleFilter = (data = {}) => {
    if (data.type && data.type === '分页') {
      this.initData();
    } else {
      this.setState(
        {
          pageNumber: 1,
          pageSize: 10,
          type: data.type,
          typeInfo: data.typeInfo,
          name: data.name,
          shopName: data.shopName,
          status: data.status,
        },
        () => {
          this.initData();
        },
      );
    }
  };
  
  handleExportData = () => {
    downloadFile()
  }

  // 分页，下一页
  onChange = pageNumber => {
    this.setState(
      {
        pageNumber,
      },
      () => {
        this.handleFilter({ type: '分页' });
      },
    );
  };
  
  showTotal = () => {
    return `共有${this.state.total}条`;
  };
  
  // 切换每页数量
  onShowSizeChange = (current, pageSize) => {
    this.setState({ pageSize }, () => {
      this.handleFilter({ type: '分页' });
    });
  };

  edit = id => {
    goToRouter(this.props.dispatch, `/commission/liteList/edit/${ id}`)
  };
  
  preview = id => {
    goToRouter(this.props.dispatch, `/commission/liteList/commissionDetail/${ id}`)
  };
  
  handleAddCommission = ()=>{
    const { dispatch } = this.props;
    const path = '/commission/liteList/liteAdd'
    goToRouter(dispatch, path)
  }
  

  render() {
    const {loading, current, total, tableData} = this.state;
    return (
      <PageHeaderWrapper title={false}>
        <Card bordered={false} style={{ marginTop: 20 }}>
          <Search source="佣金设置"
                  exportData={this.handleExportData}
                  handleFilter={this.handleFilter} />
          <Button type="primary" className="w-112 mt-16 mb-18"
                  onClick={this.handleAddCommission}>新增佣金</Button>
          <Table
            columns={this.columns}
            tableLayout="fixed"
            loading={loading}
            dataSource={tableData}
            rowKey={record => record.id}
            pagination={{
              current,
              total,
              onChange: this.onChange,
              showTotal: this.showTotal,
              showQuickJumper: true,
              pageSizeOptions: ['5', '10', '20'],
              showSizeChanger: true,
              onShowSizeChange: this.onShowSizeChange,
            }}
          />
        </Card>
      </PageHeaderWrapper>
    );
  }
}

export default CommissionIndex;
