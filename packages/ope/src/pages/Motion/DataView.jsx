import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import { Card, message, Button, Form, Select, DatePicker, Spin, Col, Row, Divider } from 'antd';
import AntTable from '@/components/AntTable';
import { onTableData } from '@/utils/utils.js';
import moment from 'moment';
const { RangePicker } = DatePicker;
import { getTimeDistance } from '@/utils/utils';
import MotionService from './services';
import styles from './index.less';
@connect(({ order }) => ({
  ...order,
}))
@Form.create()
export default class DataView extends Component {
  state = {
    yesterDayData: getTimeDistance('yesterday'),
    visible: false,
    orderId: null,
    datas: {},
    gBtusua: '',
    Viewloading: false,
    list: [],
    data: {},
  };
  componentDidMount() {
    this.onList({
      begin: moment(getTimeDistance('yesterday')[0]).format('YYYY-MM-DD HH:mm:ss'),
      end: moment(getTimeDistance('yesterday')[1]).format('YYYY-MM-DD HH:mm:ss'),
    });
  }
  onSits = e => {
    this.setState({
      yesterDayData: e,
    });
  };

  onExport = () => {
    const { yesterDayData } = this.state;
    MotionService.homeStepStatisticsExport({
      begin: moment(yesterDayData[0]).format('YYYY-MM-DD HH:mm:ss'),
      end: moment(yesterDayData[1]).format('YYYY-MM-DD HH:mm:ss'),
    }).then(res => {
      message.success('导出成功～');
      location.href = res;
    });
  };
  onList = (data = {}) => {
    this.setState({
      Viewloading: true,
    });
    MotionService.homeStepStatistics({ ...data }).then(res => {
      this.setState({
        Viewloading: false,
        list: res.productList || [],
        data: res,
      });
    });
  };
  
  handleReset = e => {
    this.props.form.resetFields();
    this.props.form.setFieldsValue({
      createDate: getTimeDistance('yesterday'),
    });
    this.onList(1, 10);
  };
  
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (values.createDate && values.createDate.length < 1) {
        values.begin = '';
        values.end = '';
        delete values.createDate;
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList({ ...values });
          },
        );
      } else if (values.createDate) {
        values.begin = moment(values.createDate[0]).format('YYYY-MM-DD HH:mm:ss');
        values.end = moment(values.createDate[1]).format('YYYY-MM-DD HH:mm:ss');
        delete values.createDate;
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList({ ...values });
          },
        );
      } else {
        delete values.createDate;
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList({ ...values });
          },
        );
      }
    });
  };

  render() {
    const { Viewloading, list, yesterDayData, data } = this.state;

    const columns = [
      {
        title: '商品名称',
        dataIndex: 'productName',
        width: '33%',
        align: 'center',
      },
      {
        title: '兑换订单数（点击兑换）',
        dataIndex: 'clickCount',
        width: '33%',
        align: 'center',
      },
      {
        title: '兑换订单数（成功兑换）',
        dataIndex: 'successCount',
        width: '33%',
        align: 'center',
      },
    ];
    const { getFieldDecorator } = this.props.form;
    return (
      <PageHeaderWrapper>
        <Card bordered={false}>
          <Form layout="inline" onSubmit={this.handleSubmit}>
            <Form.Item label="创建时间">
              {getFieldDecorator('createDate', { initialValue: yesterDayData })(
                <RangePicker
                  placeholder={['下单开始时间', '下单结束时间']}
                  onChange={this.onSits}
                  allowClear={false}
                />,
              )}
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
            </Form.Item>
            <Form.Item>
              <Button htmlType="button" onClick={this.handleReset}>
                重置
              </Button>
            </Form.Item>
            <Form.Item>
              <Button onClick={this.onExport}>导出</Button>
            </Form.Item>
          </Form>
        </Card>
        <div style={{ marginTop: 24 }}>
          <Row gutter={16}>
            <Col span={8}>
              <Card bordered={false}>
                <div className={styles.title}>
                  <div style={{ textAlign: 'center' }}>
                    <div className={styles.title_title}>主页面访问</div>
                    <div style={{ marginTop: 30 }}>
                      <span style={{ marginRight: 30 }}>
                        pv<span style={{ color: '#5476F0' }}>{data && data.indexPV}</span>
                      </span>
                      <span>
                        uv<span style={{ color: '#5476F0' }}>{data && data.indexUV}</span>
                      </span>
                    </div>
                  </div>
                  <Divider type="vertical" style={{ height: '70px' }} />
                  <div style={{ textAlign: 'center' }}>
                    <div className={styles.title_title}>授权运动步数</div>
                    <div style={{ marginTop: 30 }}>
                      <span style={{ marginRight: 30 }}>
                        pv<span style={{ color: '#5476F0' }}>{data && data.authStepPV}</span>
                      </span>
                      <span>
                        uv<span style={{ color: '#5476F0' }}>{data && data.authStepUV}</span>
                      </span>
                    </div>
                  </div>
                </div>
              </Card>
            </Col>
            <Col span={8}>
              <Card bordered={false}>
                <div className={styles.title}>
                  <div style={{ textAlign: 'center' }}>
                    <div className={styles.title_title}>任务页面访问</div>
                    <div style={{ marginTop: 30 }}>
                      <span style={{ marginRight: 30 }}>
                        pv<span style={{ color: '#5476F0' }}>{data && data.taskIndexPV}</span>
                      </span>
                      <span>
                        uv<span style={{ color: '#5476F0' }}>{data && data.taskIndexUV}</span>
                      </span>
                    </div>
                  </div>
                  <Divider type="vertical" style={{ height: '70px' }} />
                  <div style={{ textAlign: 'center' }}>
                    <div className={styles.title_title}>任务完成</div>
                    <div style={{ marginTop: 30 }}>
                      <span style={{ marginRight: 30 }}>
                        pv<span style={{ color: '#5476F0' }}>{data && data.taskFinishPV}</span>
                      </span>
                      <span>
                        uv<span style={{ color: '#5476F0' }}>{data && data.taskFinishUV}</span>
                      </span>
                    </div>
                  </div>
                </div>
              </Card>
            </Col>
            <Col span={8}>
              <Card bordered={false}>
                <div className={styles.title}>
                  <div style={{ textAlign: 'center' }}>
                    <div className={styles.title_title}>抽奖页面访问</div>
                    <div style={{ marginTop: 30 }}>
                      <span style={{ marginRight: 30 }}>
                        pv<span style={{ color: '#5476F0' }}>{data && data.lotteryPagePV}</span>
                      </span>
                      <span>
                        uv<span style={{ color: '#5476F0' }}>{data && data.lotteryPageUV}</span>
                      </span>
                    </div>
                  </div>
                  <Divider type="vertical" style={{ height: '70px' }} />
                  <div style={{ textAlign: 'center' }}>
                    <div className={styles.title_title}>抽奖按钮点击</div>
                    <div style={{ marginTop: 30 }}>
                      <span style={{ marginRight: 30 }}>
                        pv<span style={{ color: '#5476F0' }}>{data && data.lotteryPV}</span>
                      </span>
                      <span>
                        uv<span style={{ color: '#5476F0' }}>{data && data.lotteryUV}</span>
                      </span>
                    </div>
                  </div>
                </div>
              </Card>
            </Col>
          </Row>
        </div>
        <Card bordered={false} style={{ marginTop: 24 }}>
          <Spin spinning={Viewloading}>
            <AntTable dataSource={onTableData(list)} columns={columns} />
          </Spin>
        </Card>
      </PageHeaderWrapper>
    );
  }
}
