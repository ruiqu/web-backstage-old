let data = {
  PUT_ON_SHELF: '已上架',
  PUT_ON_DEPOT: '已下架',
  DELETE: '已删除',
};
export default data;
