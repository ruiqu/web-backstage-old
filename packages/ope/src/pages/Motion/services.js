import request from '@/services/baseService';

export default {
  // {运动步数商品}
  //新增兑换商品
  productExchangeAdd: data => {
    return request(`/hzsx/productExchange/add`, data);
  },
  //分页查询
  productExchangeQueryPage: data => {
    return request(`/hzsx/productExchange/queryPage`, data);
  },
  //查询一条新增商品
  productExchangeQueryDetail: data => {
    return request(`/hzsx/productExchange/queryDetail`, data, 'get');
  },
  //更新商品
  productExchangeUpdate: data => {
    return request(`/hzsx/productExchange/update`, data);
  },
  //删除商品
  productExchangeDelete: data => {
    return request(`/hzsx/productExchange/delete`, data, 'get');
  },
  //切换商品上架下架状态
  productExchangeUpdateStatus: data => {
    return request(`/hzsx/productExchange/updateStatus`, data, 'get');
  },
  // {点券订单查询}
  //查询物流公司
  selectExpressList: data => {
    return request(`/hzsx/platformExpress/selectExpressList`, data, 'get');
  },
  //订单列表
  exchangeOrderPage: data => {
    return request(`/hzsx/exchangeOrder/page`, data);
  },
  //订单详情
  exchangeOrderDetail: data => {
    return request(`/hzsx/exchangeOrder/detail`, data, 'get');
  },
  //发货
  exchangeOrderDelivery: data => {
    return request(`/hzsx/exchangeOrder/delivery`, data);
  },
  //拒绝兑换
  exchangeOrderRefuse: data => {
    return request(`/hzsx/exchangeOrder/refuse`, data);
  },
  //获取订单拒绝原因
  exchangeOrderRefuseReason: data => {
    return request(`/hzsx/exchangeOrder/refuseReason`, data, 'get');
  },
  //{数据看板}
  //获取商品详细信息
  homeStepStatistics: data => {
    return request(`/hzsx/home/stepStatistics`, data);
  },
  //导出
  homeStepStatisticsExport: data => {
    return request(`/hzsx/home/stepStatisticsExport`, data);
  },
};
