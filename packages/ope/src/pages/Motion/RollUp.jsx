import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import {
  Card,
  message,
  Button,
  Form,
  Input,
  Select,
  DatePicker,
  Spin,
  Divider,
  Popconfirm,
} from 'antd';
import MyPageTable from '@/components/MyPageTable';
import { onTableData } from '@/utils/utils.js';
import moment from 'moment';
const { Option } = Select;
const { RangePicker } = DatePicker;
import { router } from 'umi';
import MotionService from './services';
import Status from './status';
import SearchItemEnum from "@/components/Search/SearchItemEnum";
@connect(({ order }) => ({
  ...order,
}))
@Form.create()
export default class RollUp extends Component {
  state = {
    current: 1,
    visible: false,
    orderId: null,
    datas: {},
    gBtusua: '',
    RollUploading: false,
    list: [],
    total: 1,
  };
  componentDidMount() {
    this.onList(1, 10);
    this.props.dispatch({
      type: 'order/PlateformChannel',
    });
  }
  onList = (pageNumber, pageSize, data = {}) => {
    this.setState({
      RollUploading: true,
    });
    MotionService.productExchangeQueryPage({ pageNumber, pageSize, ...data }).then(res => {
      this.setState({
        RollUploading: false,
        list: res.records || [],
        total: res.total,
      });
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (values.createDate && values.createDate.length < 1) {
        values.createTimeBegin = '';
        values.createTimeEnd = '';
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      } else if (values.createDate) {
        values.createTimeBegin = moment(values.createDate[0]).format('YYYY-MM-DD HH:mm:ss');
        values.createTimeEnd = moment(values.createDate[1]).format('YYYY-MM-DD HH:mm:ss');
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      } else {
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      }
    });
  };

  //table 页数
  onPage = e => {
    this.setState(
      {
        current: e.current,
      },
      () => {
        this.onList(e.current, 10, this.state.datas);
      },
    );
  };
  onDelt = e => {
    MotionService.productExchangeDelete({ id: e.id }).then(res => {
      message.success('删除成功～');
      this.onList(1, 10);
      this.setState({
        current: 1,
      });
    });
  };
  onDelts = e => {
    MotionService.productExchangeUpdateStatus({ id: e.id }).then(res => {
      this.onList(1, 10);
      this.setState({
        current: 1,
      });
    });
  };
  onReset = () => {
    this.props.form.resetFields();
    this.onList(1, 10);
    this.setState({
      current: 1,
    });
  };

  render() {
    const { RollUploading, list, total } = this.state;
    const paginationProps = {
      current: this.state.current,
      pageSize: 10,
      total: total,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / 10)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };

    const columns = [
      {
        title: '主图',
        dataIndex: 'mainPic',
        width: '15%',
        render: mainPic => <img src={mainPic} style={{ width: 47, height: 47 }} alt={mainPic} />,
      },
      {
        title: '名称',
        dataIndex: 'productName',
        width: '15%',
      },
      {
        title: '库存',
        dataIndex: 'totalStock',
        width: '10%',
      },
      {
        title: '点券',
        dataIndex: 'exchangePoint',
        width: '10%',
      },

      {
        title: '录入时间',
        dataIndex: 'createTime',
        width: '20%',
      },
      {
        title: '状态',
        dataIndex: 'status',
        width: 200,
        render: status => Status[status],
      },
      {
        title: '操作',
        width: '10%',
        align: 'center',
        render: e => {
          return (
            <div>
              {e.status === 'DELETE' ? (
                <a
                  href={`#/Motion/RollUp/RollUpDetails?id=${e.id}&status=DELETE`}
                  target="_blank"
                  style={{ color: '#3F66F5' }}
                >
                  查看
                </a>
              ) : (
                <>
                  <span
                    style={{ cursor: 'pointer', color: '#3F66F5' }}
                    onClick={() => router.push(`/Motion/RollUp/RollUpDetails?id=${e.id}`)}
                  >
                    修改
                  </span>
                  <Divider type="vertical" />
                  <Popconfirm
                    title={e.status === 'PUT_ON_SHELF' ? '确定下架吗？' : '确定上架吗？'}
                    onConfirm={() => this.onDelts(e)}
                  >
                    <span style={{ cursor: 'pointer', color: '#3F66F5' }}>
                      {e.status === 'PUT_ON_SHELF' ? '下架' : '上架'}
                    </span>
                  </Popconfirm>
                  <div>
                    <Popconfirm title="确定删除吗？" onConfirm={() => this.onDelt(e)}>
                      <span style={{ cursor: 'pointer', color: '#3F66F5' }}>删除</span>
                    </Popconfirm>
                  </div>
                </>
              )}
            </div>
          );
        },
      },
    ];
    const { getFieldDecorator } = this.props.form;
    return (
      <PageHeaderWrapper>
        <Card bordered={false}>
          <Form layout="inline" onSubmit={this.handleSubmit}>
            <Form.Item label={'状态'}>
              {getFieldDecorator(
                'status',
                {},
              )(
                <Select placeholder="状态" style={{ width: 180 }}>
                  <Option value="">全部</Option>
                  <Option value="PUT_ON_SHELF">已上架</Option>
                  <Option value="PUT_ON_DEPOT">已下架</Option>
                  <Option value="DELETE">已删除</Option>
                </Select>,
              )}
            </Form.Item>
            { SearchItemEnum['productName'](getFieldDecorator) }
            <Form.Item label={'创建时间'}>
              {getFieldDecorator(
                'createDate',
                {},
              )(<RangePicker placeholder={['开始时间', '结束时间']} />)}
            </Form.Item>
            <div>
              <Form.Item>
                <Button type="primary" htmlType="submit">
                  查询
                </Button>
              </Form.Item>
              <Form.Item>
                <Button onClick={this.onReset}>重置</Button>
              </Form.Item>
            </div>
          </Form>
          <Button
            type="primary"
            onClick={() => {
              router.push('/Motion/RollUp/add');
            }}
            style={{
              marginTop: 42,
              marginBottom: 16,
            }}
          >
            添加商品
          </Button>
          <Spin spinning={RollUploading}>
            <MyPageTable
              onPage={this.onPage}
              paginationProps={paginationProps}
              dataSource={onTableData(list)}
              columns={columns}
            />
          </Spin>
        </Card>
      </PageHeaderWrapper>
    );
  }
}
