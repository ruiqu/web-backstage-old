import React, { Component, Fragment } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Card, Descriptions, Divider } from 'antd';
import { connect } from 'dva';
import CustomCard from '@/components/CustomCard';
import MotionService from './services';
import { getParam } from '@/utils/utils.js';
@connect(({}) => ({}))
export default class OrderDetails extends Component {
  state = {
    data: {},
  };
  componentDidMount() {
    MotionService.exchangeOrderDetail({ orderId: getParam('orderId') }).then(res => {
      this.setState({
        data: res,
      });
    });
  }
  render() {
    let { data } = this.state;
    const Status = {
      DEALING: '兑换中',
      WAITING_RECEIVE: '已发货',
      REFUSED: '已拒绝',
      FINISHED: '已完成',
    };
    return (
      <PageHeaderWrapper>
        <Card bordered={false}>
          <Descriptions title={<CustomCard title="收货人信息" />}>
            <Descriptions.Item label="收货人姓名">
              {data && data.addressDto && data.addressDto.realname}
            </Descriptions.Item>
            <Descriptions.Item label="收货人手机号">
              {data && data.addressDto && data.addressDto.telephone}
            </Descriptions.Item>
            <Descriptions.Item label="收货人地址">
              {data && data.addressDto && data.addressDto.provinceStr}
              {data && data.addressDto && data.addressDto.cityStr}
              {data && data.addressDto && data.addressDto.areaStr}
              {data && data.addressDto && data.addressDto.street}
            </Descriptions.Item>
            <Descriptions.Item label="用户备注">{data && data.remark}</Descriptions.Item>
          </Descriptions>
          <Divider />
          <Descriptions title={<CustomCard title="订单信息" />}>
            <Descriptions.Item label="下单人姓名">{data && data.userName}</Descriptions.Item>
            <Descriptions.Item label="下单人手机号">{data && data.telephone}</Descriptions.Item>
            <Descriptions.Item label="兑换订单号">{data && data.orderId}</Descriptions.Item>
            <Descriptions.Item label="商品名称">{data && data.productName}</Descriptions.Item>
            <Descriptions.Item label="销售原价">{data && data.salePrice}</Descriptions.Item>
            <Descriptions.Item label="点券">{data && data.exchangePoint}</Descriptions.Item>
            <Descriptions.Item label="兑换状态">{data && Status[data.status]}</Descriptions.Item>
            <Descriptions.Item label="兑换时间">{data && data.createTime}</Descriptions.Item>
            <Descriptions.Item label="物流公司">
              {data && data.expressInfo && data.expressInfo.expressCompany}
            </Descriptions.Item>
            <Descriptions.Item label="物流单号">
              {data && data.expressInfo && data.expressInfo.expressNo}
            </Descriptions.Item>
            <Descriptions.Item label="商品主图">
              <img src={data && data.mainPic} alt={data && data.mainPic} style={{ width: 64 }} />
            </Descriptions.Item>
            <Descriptions.Item label="商品副图">
              {data &&
                data.subPicUrls &&
                data.subPicUrls.map((item, val) => {
                  return <img src={item} alt={item} style={{ width: 64 }} key={val} />;
                })}
            </Descriptions.Item>
          </Descriptions>
        </Card>
      </PageHeaderWrapper>
    );
  }
}
