import React, { PureComponent, Fragment } from 'react';
// import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import {
  Card,
  Descriptions,
  Spin,
  message,
  Table,
  Row,
  Col,
  Modal,
  Button,
  Cascader,
  Input,
  Divider,
  Form,
  Select,
  Radio,
} from 'antd';
const { TextArea } = Input;
const { Option } = Select;
import { router } from 'umi';
import CustomCard from '@/components/CustomCard';
const getStatus = new Map([
  [1, '全新'],
  [2, '99新'],
  [3, '95新'],
  [4, '9成新'],
  [5, '8成新'],
  [6, '7成新'],
]);
const getAuditState = new Map([
  [0, '正在审核'],
  [1, '审核不通过'],
  [2, '审核通过'],
]);

@connect(({ goodsIndex }) => ({
  ...goodsIndex,
}))
@Form.create()
class GoodsDetail extends PureComponent {
  state = {
    loading: true,
    visible: false,
    imgSrc: [],
    imgLength: 0,
    current: 0,
    categoryVisible: false,
    categoryId: null,
    actionVisible: false,
    confirmLoading: false,
    tittle: '',
    status: 0,
    reason: null,
    isScale: [],
  };

  columns = [
    {
      title: '规格/颜色',
      align: 'center',
      key: 'style',
      width: '30%',
      dataIndex: 'speAndColer',
    },
    {
      title: '官方售价',
      dataIndex: 'marketPrice',
      align: 'center',
      key: 'marketPrice',
    },
    {
      title: '购买价格',
      dataIndex: 'salePrice',
      align: 'center',
      key: 'salePrice',
    },
    {
      title: '库存数量',
      dataIndex: 'inventory',
      align: 'center',
      key: 'inventory',
    },
    {
      title: '是否支持分期',
      align: 'center',
      render: e => (e.isSupportStage === 1 ? '是' : '否'),
    },
  ];
  columnses = [
    {
      title: '款式',
      align: 'center',
      key: 'style',
      width: '30%',
      render: (text, record) => {
        return record.values.map((item, index) => {
          if (index + 1 !== record.values.length) {
            return (
              <Fragment key={item.name}>
                <span>{item.name}/</span>
              </Fragment>
            );
          } else {
            return (
              <Fragment key={item.name}>
                <span>{item.name}</span>
              </Fragment>
            );
          }
        });
      },
    },
    {
      title: '市场价格',
      dataIndex: 'marketPrice',
      align: 'center',
      key: 'marketPrice',
    },
    {
      title: '租赁价格',
      dataIndex: 'cyclePrices',
      align: 'center',
      key: 'cyclePrices',
      // render: text => (
      //   <div>
      //     {text.map(item => (
      //       <p key={item.id}>
      //         {item.days}天：{item.price}
      //       </p>
      //     ))}
      //   </div>
      // ),
    },
  ];

  addressColumns = [
    {
      title: '姓名',
      dataIndex: 'name',
      align: 'center',
      key: 'name',
    },
    {
      title: '手机',
      dataIndex: 'telephone',
      align: 'center',
      key: 'telephone',
    },
    {
      title: '收货地址',
      align: 'center',
      render: e => {
        return (
          <>
            {e && e.provinceStr}
            {e && e.cityStr}
            {e && e.areaStr}
            {e && e.street}
          </>
        );
      },
    },
    {
      title: '添加时间',
      dataIndex: 'createTime',
      align: 'center',
      width: '200px',
    },
  ];

  componentDidMount() {
    this.getGoodsInfo();
  }

  getGoodsInfo = () => {
    this.setState(
      {
        actionId: this.props.location.pathname.split('=')[2],
      },
      () => {},
    );
    let id = this.props.location.pathname.split('=')[1].split('/')[0];

    this.props.dispatch({
      type: 'goodsIndex/selectExamineProductDirectDetail',
      payload: { productId: id },
      callback: res => {
        console.log(res.data, 'reasas');
        if (res.responseType === 'SUCCESS') {
          message.success('获取信息成功');
          this.setState({
            isScale: res.data.pricingInformations,
            loading: false,
          });
        } else {
          message.error(res.data.msg);
        }
      },
    });
  };
  viewImg = (data, index) => {
    this.setState({
      visible: true,
      imgSrc: data,
      imgLength: data.length,
      current: index,
    });
  };
  // 图片modal的关闭
  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };
  // 下一张
  next = () => {
    let a = this.state.current;
    a += 1;
    this.setState({
      current: a,
    });
  };
  // 上一张
  prev = () => {
    let a = this.state.current;
    a -= 1;
    this.setState({
      current: a,
    });
  };
  submit = () => {
    this.props.form.validateFields((err, value) => {
      if (!err) {
        this.props.dispatch({
          type: 'goodsIndex/examineProductDirectBuyConfirm',
          payload: { ...value, id: this.state.actionId },
          callback: res => {
            if (res.responseType === 'SUCCESS') {
              router.push('/goods/Purchase');
              message.success('审核成功！');
            }
          },
        });
      }
    });
  };
  render() {
    const {
      loading,
      imgSrc,
      visible,
      current,
      imgLength,
      categoryVisible,
      actionVisible,
      tittle,
      confirmLoading,
      isScale,
    } = this.state;
    const {
      goodsInfo,
      selectData,
      goodsInfo: { shop },
    } = this.props;
    console.log('shop:', shop);
    console.log('goodsInfo:', goodsInfo);
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 2 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
    };
    let auditStates = {
      0: '正在审核中',
      1: '审核不通过',
      2: '审核通过',
    };
    let oldNewDegrees = {
      1: '全新',
      2: '99新',
      3: '95新',
      4: '9成新',
      7: '98新',
      8: '85成新',
    };
    return (
      <PageHeaderWrapper title={'购买商品详情'}>
        <Spin spinning={false}>
          <Card bordered={false} style={{ marginTop: 20 }}>
            <Descriptions title={<CustomCard title="基本信息" />}>
              <Descriptions.Item label="店铺编号">{shop && shop.id}</Descriptions.Item>
              <Descriptions.Item label="店铺名称">{shop && shop.name}</Descriptions.Item>
              <Descriptions.Item label="商品类目">
                {goodsInfo && goodsInfo.categoryStr && goodsInfo.categoryStr.name[0]}/
                {goodsInfo && goodsInfo.categoryStr && goodsInfo.categoryStr.name[1]}/
                {goodsInfo && goodsInfo.categoryStr && goodsInfo.categoryStr.name[2]}
              </Descriptions.Item>
              <Descriptions.Item label="商品编号">{shop && shop.mainCategoryId}</Descriptions.Item>
              <Descriptions.Item label="商品名称">{goodsInfo && goodsInfo.name}</Descriptions.Item>
              <Descriptions.Item label="新旧">
                {oldNewDegrees[goodsInfo && goodsInfo.oldNewDegree]}
              </Descriptions.Item>
              <Descriptions.Item label="审核状态">
                {auditStates[goodsInfo && goodsInfo.auditState]}
              </Descriptions.Item>
              <Descriptions.Item label="上架状态">
                {goodsInfo && goodsInfo.type === 1
                  ? '上架'
                  : goodsInfo.type === 2
                  ? '未上架'
                  : null}
              </Descriptions.Item>
              <Descriptions.Item label="发货地">{goodsInfo && goodsInfo.city}</Descriptions.Item>
              <Descriptions.Item label="发货快递">
                {goodsInfo && goodsInfo.freightType}
              </Descriptions.Item>
              <Descriptions.Item label="创建时间">
                {goodsInfo && goodsInfo.createTime}
              </Descriptions.Item>
              <Descriptions.Item label="增值服务">
                {goodsInfo &&
                  goodsInfo.additionalServices &&
                  goodsInfo.additionalServices.map((item, val) => {
                    return (
                      <div>
                        {val + 1}: {item.shopAdditionalServices.name}
                      </div>
                    );
                  })}
              </Descriptions.Item>
            </Descriptions>
            <Divider dashed />
            <CustomCard title="商品价格信息" />
            <Table
              style={{ marginTop: 10 }}
              columns={this.columns}
              dataSource={this.state.isScale}
              rowKey={record => record.id}
              pagination={false}
            />
            <CustomCard title="商品图片" style={{ marginTop: 30 }} />
            <Row type="flex">
              {goodsInfo &&
                goodsInfo.images &&
                goodsInfo.images.map((item, index) => {
                  return (
                    <Col style={{ marginLeft: 10 }} key={item.id}>
                      <img
                        onClick={() => this.viewImg(goodsInfo.images, index)}
                        style={{ width: 85, height: 85 }}
                        src={item.src}
                        alt=""
                      />
                    </Col>
                  );
                })}
            </Row>
            <CustomCard title="商品详情" style={{ marginTop: 30 }} />
            <div dangerouslySetInnerHTML={{ __html: goodsInfo && goodsInfo.detail }} />
            <CustomCard title="商品参数" style={{ marginTop: 30 }} />
            <Descriptions>
              {goodsInfo &&
                goodsInfo.parameters &&
                goodsInfo.parameters.map((item, val) => {
                  return <Descriptions.Item label={item.name}>{item.value}</Descriptions.Item>;
                })}
            </Descriptions>
            <CustomCard title="归还地址" style={{ marginTop: 30 }} />
            <Table
              style={{ marginTop: 10 }}
              columns={this.addressColumns}
              dataSource={goodsInfo && goodsInfo.shopGiveBackAddressesList}
              rowKey={record => record.id}
              pagination={false}
            />
          </Card>
          {goodsInfo && goodsInfo.auditState === 0 ? (
            <Card bordered={false} style={{ marginTop: 20, marginBottom: 30 }}>
              <CustomCard title="商品审核" style={{ marginTop: 30 }} />
              <Form {...formItemLayout}>
                <Form.Item label="审批">
                  {getFieldDecorator('auditState', {
                    rules: [
                      {
                        required: true,
                        message: '商品状态不能为空',
                      },
                    ],
                    initialValue: (goodsInfo && goodsInfo.auditState) || 2,
                  })(
                    <Radio.Group>
                      <Radio value={2}>审核通过</Radio>
                      <Radio value={1}>审核拒绝</Radio>
                    </Radio.Group>,
                  )}
                </Form.Item>
                <Form.Item label="备注">
                  {getFieldDecorator('reason', {
                    initialValue: (goodsInfo && goodsInfo.auditRefuseReason) || undefined,
                  })(<TextArea />)}
                </Form.Item>
                <Form.Item wrapperCol={{ span: 24 }}>
                  <Button type="primary" onClick={this.submit}>
                    提交
                  </Button>
                </Form.Item>
              </Form>
            </Card>
          ) : null}
        </Spin>

        <Modal
          visible={visible}
          footer={
            <div style={{ textAlign: 'center' }}>
              <Button disabled={current <= 0 ? true : false} onClick={this.prev}>
                上一张
              </Button>
              <Button
                disabled={current >= imgLength - 1 ? true : false}
                type="primary"
                onClick={this.next}
              >
                下一张
              </Button>
            </div>
          }
          onCancel={this.handleCancel}
          width={600}
        >
          <img
            src={imgSrc.length > 0 ? imgSrc[current].src : null}
            alt=""
            style={{ width: '100%', paddingTop: 15 }}
          />
        </Modal>
      </PageHeaderWrapper>
    );
  }
}

export default GoodsDetail;
