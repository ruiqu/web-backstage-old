import React, { PureComponent, Fragment } from 'react';
// import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import {
  Card,
  Descriptions,
  Spin,
  message,
  Table,
  Row,
  Col,
  Modal,
  Button,
  Cascader,
  Input,
  Divider,
  Form,
  Select,
  Radio,
} from 'antd';
const { TextArea } = Input;
const { Option } = Select;
import { router } from 'umi';
import CustomCard from '@/components/CustomCard';
import {BuyOutEnum} from '@/utils/enum';
import { returnGuiHuanTxt } from "../../utils/utils";

const getStatus = new Map([
  [1, '全新'],
  [2, '99新'],
  [3, '95新'],
  [4, '9成新'],
  [5, '8成新'],
  [6, '7成新'],
]);
const getAuditState = new Map([
  [0, '正在审核'],
  [1, '审核不通过'],
  [2, '审核通过'],
]);

@connect(({ goodsIndex }) => ({
  ...goodsIndex,
}))
@Form.create()
class GoodsDetail extends PureComponent {
  state = {
    loading: true,
    visible: false,
    imgSrc: [],
    imgLength: 0,
    current: 0,
    categoryVisible: false,
    categoryId: null,
    actionVisible: false,
    confirmLoading: false,
    tittle: '',
    status: 0,
    reason: null,
    isScale: [],
  };

  columns = [
    {
      title: '规格/颜色',
      align: 'center',
      key: 'style',
      // width: '30%',
      dataIndex: 'speAndColer',
      // render: (text, record) => {
      //   return record.values.map((item, index) => {
      //     if (index + 1 !== record.values.length) {
      //       return (
      //         <Fragment key={item.name}>
      //           <span>{item.name}/</span>
      //         </Fragment>
      //       );
      //     } else {
      //       return (
      //         <Fragment key={item.name}>
      //           <span>{item.name}</span>
      //         </Fragment>
      //       );
      //     }
      //   });
      // },
    },
    {
      title: '官方售价',
      dataIndex: 'marketPrice',
      align: 'center',
      key: 'marketPrice',
    },
    {
      title: '租赁价格',
      align: 'center',
      key: 'rentPrice',
      dataIndex: 'rentPrice',
    },
    {
      title: '库存数量',
      dataIndex: 'inventory',
      align: 'center',
      key: 'inventory',
    },
    {
      title: '买断规则',
      dataIndex: 'buyOutSupport',
      key: 'buyOutSupport',
      align: 'center',
      // (buyOutSupport ? '可以买断' : '不可以买断')
      render: buyOutSupport => BuyOutEnum[buyOutSupport] || '-',
    },
    {
      title: '销售价',
      dataIndex: 'salePrice',
      key: 'salePrice',
      align: 'center',
    },
    // {
    //   title: '押金',
    //   dataIndex: 'sesameDeposit',
    //   key: 'sesameDeposit',
    //   align: 'center',
    // },
  ];
  columnses = [
    {
      title: '款式',
      align: 'center',
      key: 'style',
      width: '30%',
      render: (text, record) => {
        return record.values.map((item, index) => {
          if (index + 1 !== record.values.length) {
            return (
              <Fragment key={item.name}>
                <span>{item.name}/</span>
              </Fragment>
            );
          } else {
            return (
              <Fragment key={item.name}>
                <span>{item.name}</span>
              </Fragment>
            );
          }
        });
      },
    },
    {
      title: '市场价格',
      dataIndex: 'marketPrice',
      align: 'center',
      key: 'marketPrice',
    },
    {
      title: '租赁价格',
      dataIndex: 'cyclePrices',
      align: 'center',
      key: 'cyclePrices',
      // render: text => (
      //   <div>
      //     {text.map(item => (
      //       <p key={item.id}>
      //         {item.days}天：{item.price}
      //       </p>
      //     ))}
      //   </div>
      // ),
    },
  ];
  addcolumns=[
    {
      title: '名称',
      dataIndex: 'shopAdditionalServices.name',
      align: 'center',
      key: 'shopAdditionalServices.name',
    },
    {
      title: '内容',
      dataIndex: 'shopAdditionalServices.content',
      align: 'center',
      key: 'shopAdditionalServices.content',
    },
    {
      title: '价格(元)',
      dataIndex: 'shopAdditionalServices.price',
      align: 'center',
      key: 'shopAdditionalServices.price',
    },
    {
      title: '说明',
      dataIndex: 'shopAdditionalServices.description',
      align: 'center',
      key: 'shopAdditionalServices.description',
    }
    
  ]
  addressColumns = [
    {
      title: '姓名',
      dataIndex: 'name',
      align: 'center',
      key: 'name',
    },
    {
      title: '手机',
      dataIndex: 'telephone',
      align: 'center',
      key: 'telephone',
    },
    {
      title: '省',
      dataIndex: 'provinceStr',
      align: 'center',
      key: 'provinceSrc',
    },
    {
      title: '市',
      dataIndex: 'cityStr',
      align: 'center',
      key: 'cityStr',
    },
    {
      title: '区',
      dataIndex: 'areaStr',
      align: 'center',
      key: 'areaStr',
    },
    {
      title: '街道',
      dataIndex: 'street',
      align: 'center',
      key: 'street',
    },
  ];

  componentDidMount() {
    this.getGoodsInfo();
  }

  getGoodsInfo = () => {
    this.setState(
      {
        actionId: this.props.location.pathname.split('=')[2],
      },
      () => {},
    );
    let id = this.props.location.pathname.split('=')[1].split('/')[0];

    this.props.dispatch({
      type: 'goodsIndex/getGoodsInfo',
      payload: { productId: id },
      callback: res => {
        console.log(res.data, 'reasas');
        if (res.responseType === 'SUCCESS') {
          message.success('获取信息成功');
          this.setState({
            isScale: res.data.pricingInformations,
            loading: false,
          });
        } else {
          message.error(res.data.msg);
        }
      },
    });
  };
  viewImg = (data, index) => {
    this.setState({
      visible: true,
      imgSrc: data,
      imgLength: data.length,
      current: index,
    });
  };
  // 图片modal的关闭
  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };
  // 下一张
  next = () => {
    let a = this.state.current;
    a += 1;
    this.setState({
      current: a,
    });
  };
  // 上一张
  prev = () => {
    let a = this.state.current;
    a -= 1;
    this.setState({
      current: a,
    });
  };
  submit = () => {
    this.props.form.validateFields((err, value) => {
      if (!err) {
        console.log('value:', value);

        this.props.dispatch({
          type: 'goodsIndex/examineProductConfirm',
          payload: { ...value, id: this.state.actionId },
          callback: res => {
            console.log(res, 'resad');
            if (res.responseType === 'SUCCESS') {
              router.push('/goods/index');
              message.success('审核成功！');
            }
          },
        });
      }
    });
  };
  render() {
    const {
      loading,
      imgSrc,
      visible,
      current,
      imgLength,
      categoryVisible,
      actionVisible,
      tittle,
      confirmLoading,
      isScale,
    } = this.state;
    const {
      goodsInfo,
      selectData,
      goodsInfo: { shop,additionalServices },
    } = this.props;
  
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 2 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
    };
    let auditStates = {
      0: '为正在审核中',
      1: '为审核不通过',
      2: '为审核通过',
    };
    let freightTypes = {
      FREE: '商家包邮',
      PAY: '发货到付',
      SELF: '自提',
    };
    let oldNewDegrees = {
      1: '全新',
      2: '99新',
      3: '95新',
      4: '9成新',
      7: '98新',
      8: '85成新',
    };
    console.log(isScale, 'isScale');
    console.log("goodsInfo is >>>>", goodsInfo);
    return (
      <PageHeaderWrapper title={'租赁商品详情'}>
        <Spin spinning={false}>
          <Card bordered={false} style={{ marginTop: 20 }}>
            <Descriptions title={<CustomCard title="基本信息" />}>
              <Descriptions.Item label="店铺编号">{shop && shop.shopId}</Descriptions.Item>
              <Descriptions.Item label="店铺名称">{shop && shop.name}</Descriptions.Item>
              <Descriptions.Item label="商品类目">
                {goodsInfo && goodsInfo.categoryStr && goodsInfo.categoryStr.name}
              </Descriptions.Item>
              <Descriptions.Item label="商品编号">
                {this.props.location.pathname.split('=')[1].split('/')[0]}
              </Descriptions.Item>
              <Descriptions.Item label="商品名称">{goodsInfo && goodsInfo.name}</Descriptions.Item>
              <Descriptions.Item label="新旧">
                {oldNewDegrees[goodsInfo && goodsInfo.oldNewDegree]}
              </Descriptions.Item>
              <Descriptions.Item label="租赁标签"></Descriptions.Item>
              <Descriptions.Item label="审核状态">
                {auditStates[goodsInfo && goodsInfo.auditState]}
              </Descriptions.Item>
              <Descriptions.Item label="上架状态">
                {goodsInfo && goodsInfo.type === 1
                  ? '上架'
                  : goodsInfo.type === 2
                  ? '未上架'
                  : null}
              </Descriptions.Item>
              <Descriptions.Item label="发货地">{goodsInfo && goodsInfo.city}</Descriptions.Item>
              <Descriptions.Item label="发货快递">
                {freightTypes[goodsInfo && goodsInfo.freightType]}
              </Descriptions.Item>
              <Descriptions.Item label="创建时间">
                {goodsInfo && goodsInfo.createTime}
              </Descriptions.Item>
              <Descriptions.Item label="买断规则">
                { BuyOutEnum[goodsInfo && goodsInfo.buyOutSupport] || '-' }
              </Descriptions.Item>
              <Descriptions.Item label="归还规则">
                { goodsInfo && goodsInfo.returnRule == 1 ? '支持提前归还' : '仅到期可归还' }
              </Descriptions.Item>
              <Descriptions.Item label="归还快递">
                { returnGuiHuanTxt(goodsInfo) }
              </Descriptions.Item>
            </Descriptions>
            <Divider dashed />
            <CustomCard title="增值服务" />
            <Table
              style={{ marginTop: 10, marginBottom: 20 }}
              columns={this.addcolumns}
              dataSource={additionalServices}
              rowKey={record => record.id}
              pagination={false}
            />
            <CustomCard title="商品价格信息" />
            <Table
              style={{ marginTop: 10 }}
              columns={this.columns}
              dataSource={this.state.isScale}
              rowKey={record => record.id}
              pagination={false}
            />
            <CustomCard title="商品图片" style={{ marginTop: 30 }} />
            <Row type="flex">
              {goodsInfo &&
                goodsInfo.images &&
                goodsInfo.images.map((item, index) => {
                  return (
                    <Col style={{ marginLeft: 10 }} key={item.id}>
                      <img
                        onClick={() => this.viewImg(goodsInfo.images, index)}
                        style={{ width: 85, height: 85 }}
                        src={item.src}
                        alt=""
                      />
                    </Col>
                  );
                })}
            </Row>
            <CustomCard title="商品详情" style={{ marginTop: 30 }} />
            <div dangerouslySetInnerHTML={{ __html: goodsInfo && goodsInfo.detail }} />
            <CustomCard title="商品参数" style={{ marginTop: 30 }} />
            <Descriptions>
              {goodsInfo &&
                goodsInfo.parameters &&
                goodsInfo.parameters.map((item, val) => {
                  return <Descriptions.Item label={item.name}>{item.value}</Descriptions.Item>;
                })}
            </Descriptions>
            <CustomCard title="归还地址" style={{ marginTop: 30 }} />
            <Table
              style={{ marginTop: 10 }}
              columns={this.addressColumns}
              dataSource={goodsInfo && goodsInfo.shopGiveBackAddressesList}
              rowKey={record => record.id}
              pagination={false}
            />
          </Card>
          <Card bordered={false} style={{ marginTop: 20, marginBottom: 30 }}>
            <CustomCard title="商品审核" style={{ marginTop: 30 }} />
            <Form {...formItemLayout}>
              <Form.Item label="审批">
                {getFieldDecorator('auditState', {
                  rules: [
                    {
                      required: true,
                      message: '商品状态不能为空',
                    },
                  ],
                  initialValue: (goodsInfo && goodsInfo.auditState) || 2,
                })(
                  <Radio.Group>
                    <Radio value={2}>审核通过</Radio>
                    <Radio value={1}>审核拒绝</Radio>
                  </Radio.Group>,
                )}
              </Form.Item>
              <Form.Item label="备注">
                {getFieldDecorator('reason', {
                  initialValue: (goodsInfo && goodsInfo.auditRefuseReason) || '',
                })(<TextArea />)}
              </Form.Item>
              <Form.Item wrapperCol={{ span: 24 }}>
                <Button type="primary" onClick={this.submit}>
                  提交
                </Button>
              </Form.Item>
            </Form>
          </Card>
        </Spin>

        <Modal
          visible={visible}
          footer={
            <div style={{ textAlign: 'center' }}>
              <Button disabled={current <= 0 ? true : false} onClick={this.prev}>
                上一张
              </Button>
              <Button
                disabled={current >= imgLength - 1 ? true : false}
                type="primary"
                onClick={this.next}
              >
                下一张
              </Button>
            </div>
          }
          onCancel={this.handleCancel}
          width={600}
        >
          <img
            src={imgSrc.length > 0 ? imgSrc[current].src : null}
            alt=""
            style={{ width: '100%', paddingTop: 15 }}
          />
        </Modal>
      </PageHeaderWrapper>
    );
  }
}

export default GoodsDetail;
