import React, { Component } from 'react';
import moment from 'moment';
import { Form, Row, Col, DatePicker, Input, Select, Button } from 'antd';
const FormItem = Form.Item;
const { RangePicker } = DatePicker;
const { Option } = Select;
@Form.create()
export default class Search extends Component {
  //查询
  handleFilter = () => {
    this.props.form.validateFields((err, value) => {
      this.props.handleFilter(value);
    });
  };
  // 重置
  handleReset = async () => {
    this.props.form.resetFields();
    this.props.handleFilter({ type: '重置' });
  };
  // 导出
  exportOreders = () => {
    this.props.form.validateFields((err, value) => {
      this.props.exportOreders(value);
    });
  };
  render() {
    // console.log('查询',this.props)
    const { getFieldDecorator, validateFields } = this.props.form;
    return (
      <Form layout="inline">
        {this.props.newSoure && this.props.newSoure == '逾期' ? null : (
          <FormItem label="审批状态">
            {getFieldDecorator('status', {
              initialValue: '全部',
            })(
              <Select style={{ width: 180 }}>
                <Option value="全部">全部</Option>
                {this.props.arrStatus.map(item => {
                  return (
                    <Option key={item} value={item.split('/')[0]}>
                      {item.split('/')[1]}
                    </Option>
                  );
                })}
              </Select>,
            )}
          </FormItem>
        )}
        {/* 店铺审核，企业资质审核 */}
        {this.props.source && this.props.source === '企业资质' ? (
          <FormItem
            label="店铺id"
            style={{
              display:
                this.props.source && this.props.source === '企业资质' ? 'inline-block' : 'none',
            }}
          >
            {getFieldDecorator(
              'shopId',
              {},
            )(<Input placeholder="请输入店铺id" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        <FormItem
          label="公司名称"
          style={{
            display:
              this.props.source && this.props.type && this.props.source === '企业资质'
                ? 'inline-block'
                : 'none',
          }}
        >
          {getFieldDecorator(
            'name',
            {},
          )(<Input placeholder="请输入公司名称" style={{ width: 200 }} allowClear />)}
        </FormItem>
        {this.props.source && this.props.source === '企业资质' ? (
          <FormItem
            label="店铺名称"
            // style={{
            //   display:
            //     this.props.source && this.props.source === '企业资质' ? 'inline-block' : 'none',
            // }}
          >
            {getFieldDecorator(
              'shopName',
              {},
            )(<Input placeholder="请输入店铺名称" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        {/* 订单查询 */}
        {this.props.source && this.props.source === '订单' ? (
          <FormItem label="店铺名称">
            {getFieldDecorator(
              'shopName',
              {},
            )(<Input placeholder="请输入店铺名称" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        {this.props.source && this.props.source === '订单' ? (
          <FormItem label="店铺id">
            {getFieldDecorator(
              'shopId',
              {},
            )(<Input placeholder="请输入店铺id" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        {this.props.source && this.props.source === '订单' ? (
          <FormItem
            label="商品名称"
            // style={{
            //   display: this.props.source && this.props.source === '订单' ? 'inline-block' : 'none',
            // }}
          >
            {getFieldDecorator(
              'productName',
              {},
            )(<Input placeholder="请输入商品名称" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        {this.props.source && this.props.source === '订单' ? (
          <FormItem
            label={'商品编号'}
            // style={{
            //   display: this.props.source && this.props.source === '订单' ? 'inline-block' : 'none',
            // }}
          >
            {getFieldDecorator(
              'productId',
              {},
            )(<Input placeholder="请输入商品编号" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        {this.props.source && this.props.source === '订单' ? (
          <FormItem
            label={'下单人姓名'}
            // style={{
            //   display: this.props.source && this.props.source === '订单' ? 'inline-block' : 'none',
            // }}
          >
            {getFieldDecorator(
              'userName',
              {},
            )(<Input placeholder="请输入下单人姓名" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        {this.props.source && this.props.source === '订单' ? (
          <FormItem
            label={'下单人电话'}
            // style={{
            //   display: this.props.source && this.props.source === '订单' ? 'inline-block' : 'none',
            // }}
          >
            {getFieldDecorator(
              'telephone',
              {},
            )(<Input placeholder="请输入下单人电话" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        {this.props.source && this.props.source === '订单' ? (
          <FormItem
            label={'收货人手机号码'}
            // style={{
            //   display: this.props.source && this.props.source === '订单' ? 'inline-block' : 'none',
            // }}
          >
            {getFieldDecorator(
              'addressUserPhone',
              {},
            )(<Input placeholder="请输入收货人手机号码" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        {this.props.source && this.props.source === '订单' ? (
          <FormItem
            label={'店铺编号'}
            // style={{
            //   display: this.props.source && this.props.source === '订单' ? 'inline-block' : 'none',
            // }}
          >
            {getFieldDecorator(
              'orderId',
              {},
            )(<Input placeholder="请输入店铺编号" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        {this.props.source && this.props.oldOrders && this.props.source === '订单' ? (
          <FormItem
            label={'店铺编号'}
            // style={{
            //   display:
            //     this.props.source && this.props.oldOrders && this.props.source === '订单'
            //       ? 'inline-block'
            //       : 'none',
            // }}
          >
            {getFieldDecorator(
              'originalOrderId',
              {},
            )(<Input placeholder="请输入店铺编号" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}

        <FormItem
          label={'起租时间'}
          style={{
            display: this.props.source && this.props.source === '订单' ? 'inline-block' : 'none',
          }}
        >
          {getFieldDecorator(
            'time',
            {},
          )(
            <DatePicker
              format="YYYY-MM-DD HH:mm:ss"
              placeholder="请选择起租时间"
              showTime={{ defaultValue: moment('00:00:00', 'HH:mm:ss') }}
            />,
          )}
        </FormItem>
        <FormItem
          label={'创建时间'}
          style={{
            display: this.props.source && this.props.source === '订单' ? 'inline-block' : 'none',
          }}
        >
          {getFieldDecorator(
            'createTimeStart',
            {},
          )(
            <RangePicker
              ranges={{
                Today: [moment(), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
              }}
              showTime
              format="YYYY/MM/DD HH:mm:ss"
              placeholder={['开始下单日期', '结束下单日期']}
              // onChange={onChange}
            />,
          )}
        </FormItem>
        {this.props.source && this.props.source === '分期订单' ? (
          <FormItem label={'店铺编号'}>
            {getFieldDecorator(
              'orderId',
              {},
            )(<Input placeholder="请输入店铺编号" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        {this.props.source && this.props.source === '分期订单' ? (
          <FormItem
            label={'商品编号'}
            style={{
              display:
                this.props.source && this.props.source === '分期订单' ? 'inline-block' : 'none',
            }}
          >
            {getFieldDecorator(
              'productId',
              {},
            )(<Input placeholder="请输入商品编号" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        {this.props.source && this.props.source === '分期订单' ? (
          <FormItem
            label={'下单人电话'}
            style={{
              display:
                this.props.source && this.props.source === '分期订单' ? 'inline-block' : 'none',
            }}
          >
            {getFieldDecorator(
              'telephone',
              {},
            )(<Input placeholder="请输入下单人电话" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        {this.props.source && this.props.close && this.props.source === '分期订单' ? (
          <FormItem label={'收货人姓名'}>
            {getFieldDecorator(
              'addressUserName',
              {},
            )(<Input placeholder="请输入收货人姓名" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        {this.props.source && this.props.close && this.props.source === '分期订单' ? (
          <FormItem label={'下单人姓名'}>
            {getFieldDecorator(
              'userName',
              {},
            )(<Input placeholder="请输入下单人姓名" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        <FormItem
          style={{
            display:
              this.props.source && this.props.source === '分期订单' ? 'inline-block' : 'none',
          }}
        >
          {getFieldDecorator(
            'statementDate',
            {},
          )(
            <RangePicker
              ranges={{
                Today: [moment(), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
              }}
              showTime
              format="YYYY/MM/DD HH:mm:ss"
              placeholder={['开始下单日期', '结束下单日期']}
              // onChange={onChange}
            />,
          )}
        </FormItem>
        {/* 订单退款 */}
        {this.props.source && this.props.source === '订单退款' ? (
          <FormItem>
            {getFieldDecorator(
              'shopName',
              {},
            )(<Input placeholder="请输入店铺名称" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        {this.props.source && this.props.source === '订单退款' ? (
          <FormItem>
            {getFieldDecorator(
              'shopId',
              {},
            )(<Input placeholder="请输入店铺编号" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        {this.props.source && this.props.source === '订单退款' ? (
          <FormItem>
            {getFieldDecorator('refundNumber', {
              initialValue: this.props.refundNumber ? this.props.refundNumber : null,
            })(<Input placeholder="请输入退款编号" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        {this.props.source && this.props.source === '订单退款' ? (
          <FormItem>
            {getFieldDecorator(
              'orderId',
              {},
            )(<Input placeholder="请输入原订单号" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        {this.props.source && this.props.source === '订单退款' ? (
          <FormItem>
            {getFieldDecorator(
              'userName',
              {},
            )(<Input placeholder="请输入下单人姓名" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        {this.props.source && this.props.source === '订单退款' ? (
          <FormItem>
            {getFieldDecorator(
              'telephone',
              {},
            )(<Input placeholder="请输入下单人手机号" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        {this.props.source && this.props.source === '订单退款' ? (
          <FormItem>
            {getFieldDecorator(
              'addressUserName',
              {},
            )(<Input placeholder="请输入收货人姓名" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        {this.props.source && this.props.source === '订单退款' ? (
          <FormItem>
            {getFieldDecorator(
              'addressUserPhone',
              {},
            )(<Input placeholder="请输入收货人手机号" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}

        {/* 商品审核 */}
        <FormItem
          label={'上架状态'}
          style={{
            display:
              this.props.source && this.props.source === '商品审核' ? 'inline-block' : 'none',
          }}
        >
          {getFieldDecorator(
            'type',
            {},
          )(
            <Select placeholder="上架状态" style={{ width: 200 }} allowClear={true}>
              <Option value="">全部</Option>
              <Option value="1">已上架</Option>
              <Option value="2">已下架</Option>
              <Option value="0">回收站</Option>
            </Select>,
          )}
        </FormItem>

        {this.props.source && this.props.source === '商品审核' ? (
          <FormItem
          // style={{
          //   display:
          //     this.props.source && this.props.source === '商品审核' ? 'inline-block' : 'none',
          // }}
          >
            {getFieldDecorator(
              'productName',
              {},
            )(<Input placeholder="请输入商品名称" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        {this.props.source && this.props.source === '商品审核' ? (
          <FormItem
          // style={{
          //   display:
          //     this.props.source && this.props.source === '商品审核' ? 'inline-block' : 'none',
          // }}
          >
            {getFieldDecorator(
              'productId',
              {},
            )(<Input placeholder="请输入商品编号" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        {this.props.source && this.props.source === '商品审核' ? (
          <FormItem
          // style={{
          //   display:
          //     this.props.source && this.props.source === '商品审核' ? 'inline-block' : 'none',
          // }}
          >
            {getFieldDecorator(
              'shopId',
              {},
            )(<Input placeholder="请输入店铺编号" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        {this.props.source && this.props.source === '商品审核' ? (
          <FormItem
            style={{
              display:
                this.props.source && this.props.source === '商品审核' ? 'inline-block' : 'none',
            }}
          >
            {getFieldDecorator(
              'shopName',
              {},
            )(<Input placeholder="请输入店铺名称" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        <FormItem
          style={{
            display:
              this.props.source && this.props.source === '商品审核' ? 'inline-block' : 'none',
          }}
        >
          {getFieldDecorator(
            'creatTime',
            {},
          )(
            <RangePicker
              ranges={{
                Today: [moment(), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
              }}
              showTime
              format="YYYY/MM/DD HH:mm:ss"
              placeholder={['提交开始日期', '提交结束日期']}
              // onChange={onChange}
            />,
          )}
        </FormItem>

        {/* 押金订单 */}
        {this.props.source && this.props.source === '押金订单' ? (
          <FormItem>
            {getFieldDecorator(
              'outTradeNo',
              {},
            )(<Input placeholder="请输入商户订单号" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        {this.props.source && this.props.source === '押金订单' ? (
          <FormItem>
            {getFieldDecorator(
              'tradeNo',
              {},
            )(<Input placeholder="请输入支付宝订单号" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        {this.props.source && this.props.source === '押金订单' ? (
          <FormItem>
            {getFieldDecorator(
              'telephone',
              {},
            )(<Input placeholder="请输入用户手机号" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}
        {this.props.source && this.props.source === '押金订单' ? (
          <FormItem>
            {getFieldDecorator(
              'userName',
              {},
            )(<Input placeholder="请输入用户姓名" style={{ width: 200 }} allowClear />)}
          </FormItem>
        ) : null}

        {/* 店铺审核和企业资质审核 */}
        <FormItem
          style={{
            display:
              this.props.source && this.props.source === '企业资质' ? 'inline-block' : 'none',
          }}
        >
          {getFieldDecorator(
            'createDate',
            {},
          )(
            <RangePicker
              ranges={{
                Today: [moment(), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
              }}
              showTime
              format="YYYY/MM/DD HH:mm:ss"
              // onChange={onChange}
            />,
          )}
        </FormItem>
        {this.props.channelList ? (
          <FormItem>
            {getFieldDecorator('channelId', {
              initialValue: '',
            })(
              <Select style={{ width: 180 }}>
                {this.props.channelList.map(item => {
                  return (
                    <Option key={item.channelId} value={item.channelId}>
                      {item.channelName}
                    </Option>
                  );
                })}
              </Select>,
            )}
          </FormItem>
        ) : null}
        <FormItem>
          <Button type="primary" type="primary" onClick={this.handleFilter}>
            查询
          </Button>
          <Button style={{ marginLeft: 8 }} onClick={this.handleReset}>
            重置
          </Button>
          {this.props.newSoure && this.props.newSoure === '逾期' ? (
            <Button style={{ marginLeft: 8 }} onClick={this.exportOreders} type="primary">
              导出
            </Button>
          ) : null}
        </FormItem>
      </Form>
    );
  }
}
