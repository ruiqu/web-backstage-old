import React, { PureComponent } from 'react';

import { connect } from 'dva';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import {downloadFile, goToRouter} from "@/utils/utils";
import servicesApi from '@/services/tribe'

import {
  Table,
  Card,
  message,
  Button,
  Form,
  Badge,
  Divider, Popconfirm,
} from 'antd';
import Search from '../../components/Search';


@connect()
@Form.create()
class Tribe extends PureComponent {
  state = {
    tableData: [],
    loading: true,
    current: 1,
    pageNumber: 1,
    pageSize: 10,
    title: null,
    status: null,
  };
  
  columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
      align: 'center',
    },
    {
      title: '部落标题',
      dataIndex: 'title',
      align: 'center',
      key: 'title',
    },
    {
      title: '摘要',
      dataIndex: 'summary',
      align: 'center',
      key: 'summary',
    },
    {
      title: '部落内容',
      dataIndex: 'content',
      align: 'center',
      key: 'content',
      ellipsis: true
    },
    {
      title: '上线时间',
      dataIndex: 'createTime',
      align: 'center',
      key: 'createTime',
    },
  
    {
      title: '状态',
      dataIndex: 'status',
      align: 'center',
      key: 'status',
      render: text => {
        const success = text === 'VALID'
        const status = success? 'success' : 'default'
        const badgeText = success? '有效' : '失效'
        return (<Badge status={status} text={badgeText} />)
      },
    },
    {
      title: '操作',
      align: 'center',
      key: 'action',
      render: (text, record) => {
        return (
          <div className="table-action">
            <a onClick={()=> this.edit(record.id)}>修改</a>
            <Divider type="vertical" />
            <Popconfirm title="你确定删除吗?" onConfirm={() => this.deleteTribe(record.id)}>
              <a>删除</a>
            </Popconfirm>
          </div>
        );
      },
    },
  ];
  
  componentDidMount() {
    this.initData();
  }
  
  initData = () => {
    const payload = {
      pageNumber: this.state.pageNumber,
      pageSize: this.state.pageSize,
      title: this.state.title,
      status: this.state.status,
    };
    this.setState(
      {
        loading: true,
      },
      () => {
        servicesApi.queryTribePage(payload).then(res=>{
          message.success('获取信息成功');
          this.setState({
            tableData: res.records,
            total: res.total,
            loading: false,
            current: res.current,
          });
        }).catch(err=>{

          this.setState({
            loading: false
          });
        })
      },
    );
  };
  
  handleFilter = (data = {}) => {
    if (data.type && data.type === '分页') {
      this.initData();
    } else {
      this.setState(
        {
          pageNumber: 1,
          pageSize: 10,
          type: data.type ? data.type : null,
          title: data.title ? data.title : null,
          status: data.status ? data.status : null,
        },
        () => {
          this.initData();
        },
      );
    }
  };
  
  handleExportData = () => {
    downloadFile()
  }
  
  // 分页，下一页
  onChange = pageNumber => {
    this.setState(
      {
        pageNumber,
      },
      () => {
        this.handleFilter({ type: '分页' });
      },
    );
  };
  
  showTotal = () => {
    return `共有${this.state.total}条`;
  };
  
  // 切换每页数量
  onShowSizeChange = (current, pageSize) => {
    this.setState({ pageSize }, () => {
      this.handleFilter({ type: '分页' });
    });
  };
  
  edit = id => {
    goToRouter(this.props.dispatch, `/configure/tribe/edit/${ id}`)
  };
  
  deleteTribe = (id) => {
    servicesApi.deleteTribe({id}).then(res=>{
      message.success('删除成功');
      this.initData()
    })
  }
  
  handlePublish = ()=>{
    const { dispatch } = this.props;
    const path = '/configure/tribe/publish'
    goToRouter(dispatch, path)
  }
  
  render() {
    const { loading, current, total, tableData} = this.state;
    return (
      <PageHeaderWrapper title={false}>
        <Card bordered={false}>
          <Search needExport needReset source="部落配置" exportData={this.handleExportData}
                  handleFilter={this.handleFilter} />
          <Button type="primary" onClick={this.handlePublish}>发布图文</Button>
          <Table
            columns={this.columns}
            tableLayout="fixed"
            loading={loading}
            dataSource={tableData}
            rowKey={record => record.id}
            pagination={{
              current,
              total,
              onChange: this.onChange,
              showTotal: this.showTotal,
              showQuickJumper: true,
              pageSizeOptions: ['5', '10', '20'],
              showSizeChanger: true,
              onShowSizeChange: this.onShowSizeChange,
            }}
          />
        </Card>
      </PageHeaderWrapper>
    );
  }
}

export default Tribe;
