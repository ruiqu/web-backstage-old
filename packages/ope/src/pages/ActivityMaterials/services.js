import request from '@/services/baseService';

export default {
  //分页查询活动素材
  queryActivityConfigMaterialPage: data => {
    return request(`/hzsx/activityMaterial/queryActivityConfigMaterialPage`, data);
  },
  //查询一条活动素材
  queryActivityConfigMaterialDetail: data => {
    return request(`/hzsx/activityMaterial/queryActivityConfigMaterialDetail`, data);
  },
  //新增活动素材
  addActivityConfigMaterial: data => {
    return request(`/hzsx/activityMaterial/addActivityConfigMaterial`, data);
  },
  //更新活动素材
  modifyActivityConfigMaterial: data => {
    return request(`/hzsx/activityMaterial/modifyActivityConfigMaterial`, data);
  },
  
};
