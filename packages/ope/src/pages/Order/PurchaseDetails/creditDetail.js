import React, { PureComponent, Fragment } from 'react';
import './credit.less';
import styles from './creditDetail.less';

import { Card, Icon, Table, Popover, Divider, Tag } from 'antd';
import { Chart, Tooltip, Axis, Bar, Interval } from 'viser-react';
import {QuestionCircleOutlined} from '@ant-design/icons';

//信用分详情

const popoverContent = (
  <p>
    <p>0:(0W)</p>
    <p>1:(0W~0.2W]</p>
    <p>2:(0.2W~0.5W]</p>
    <p>3:(0.5W~1W]</p>
    <p>4:(1W~3W]</p>
    <p>5:(3W~5W]</p>
    <p>6:(5W~10W]</p>
    <p>7:(10W+)</p>
  </p>
)

class CreditDetail extends PureComponent {
  state = {
    creditInfo: this.props.creditInfo || {},
    userOrders: this.props.userOrders || {},
    userOrderInfoDto: this.props.userOrderInfoDto || {},
    orderAddressDto: this.props.orderAddressDto || {},
    productInfo: this.props.productInfo || {},
    userOrderCashesDto: this.props.userOrderCashesDto || {},
    timeArr: {
      '40': '[0,3)',
      '30': '[3,6)',
      '20': '[6,12)',
      '10': '[12,24)',
      '0': '[24,+)',
      '50': '查无记录',
      '60': '不支持该运营商',
    },
    barData: [
      { year: '近7天', num: 0 },
      { year: '近1个月', num: 0 },
      { year: '近3个月', num: 0 },
      { year: '近6个月', num: 0 },
    ],
    barScale: [
      {
        dataKey: 'num',
        // tickInterval: 20,
      },
    ],
    scoreTableColumns1:
      this.props.creditInfo &&
      this.props.creditInfo.verifyResult &&
      this.props.creditInfo.verifyResult.refuse_details
        ? [
            {
              title: '综合评分',
              dataIndex: 'score',
              key: 'score',
              align: 'center',
            },
            {
              title: '拒绝原因',
              dataIndex: 'refuse_details',
              key: 'refuse_details',
              align: 'center',
            },
          ]
        : [
            {
              title: '综合评分',
              dataIndex: 'score',
              key: 'score',
              align: 'center',
            },
            {
              title: '审核建议',
              dataIndex: 'suggest',
              key: 'suggest',
              align: 'center',
            },
            // {
            //   title: '命中风险标注',
            //   dataIndex: 'refuse_details',
            //   key: 'refuse_details',
            // },
            {
              title: '分值标注说明',
              dataIndex: 'detail',
              key: 'detail',
              render: text => (
                <div>
                  <p>分值在（0-100）之间，得分越高，风险越大：</p>
                  <p>（0-30），建议通过；</p>
                  <p>[30-80），建议审核；</p>
                  <p>[80-100），建议拒绝。</p>
                </div>
              ),
            },
          ],
    scoreTableData1: [],
    scoreTableColumns2: [
      {
        title: ' ',
        dataIndex: 'key',
        key: 'key',
        align: 'center',
      },
      {
        title: '一般消费分期平台',
        dataIndex: 'a1',
        key: 'a1',
        align: 'center',
      },
      {
        title: '信用卡     ',
        dataIndex: 'a2',
        key: 'a2',
        align: 'center',
      },
      {
        title: '大型消费金融公司',
        dataIndex: 'a3',
        key: 'a3',
        align: 'center',
      },
      {
        title: 'P2P平台',
        dataIndex: 'a5',
        key: 'a5',
        align: 'center',
      },
      {
        title: '小额贷款公司',
        dataIndex: 'a6',
        key: 'a6',
        align: 'center',
      },
      {
        title: '其他类型公司',
        dataIndex: 'a4',
        key: 'a4',
        align: 'center',
      },
      {
        title: '申请查询总数',
        dataIndex: 'a7',
        key: 'a7',
        align: 'center',
      },
    ],
    scoreTableData2: [],
    scoreTableColumns2_5: [
      {
        title: '3个月身份证关联手机号数',
        dataIndex: 'num1',
        key: 'num1',
        align: 'center',
      },
      {
        title: '3个月手机号关联身份证数',
        dataIndex: 'num2',
        key: 'num2',
        align: 'center',
      },
    ],
    scoreTableData2_5: [],
    /* scoreTableColumns3: [
      {
        title: '消费分期类申请机构数(个)',
        dataIndex: 'num1',
        key: 'num1',
      },
      {
        title: '网络贷款类申请机构数(个)',
        dataIndex: 'num2',
        key: 'num2',
      },
      {
        title: '最近一次申请日期',
        dataIndex: 'last_apply_time',
        key: 'last_apply_time',
      },
      {
        title: '距离最近一次申请日期已有(天)',
        dataIndex: 'day',
        key: 'day',
      },
    ],
    scoreTableData3: [],
    scoreTableColumns4: [
      {
        title: ' ',
        dataIndex: 'key',
        key: 'key',
      },
      {
        title: '近1个月',
        dataIndex: 'month1',
        key: 'month1',
      },
      {
        title: '近3个月',
        dataIndex: 'month3',
        key: 'month3',
      },
      {
        title: '近6个月',
        dataIndex: 'month6',
        key: 'month6',
      },
      {
        title: '近12个月',
        dataIndex: 'month12',
        key: 'month12',
      },
    ],
    scoreTableData4: [],
    scoreTableColumns5: [
      {
        title: '消费分期类放款机构数(个)',
        dataIndex: 'num1',
        key: 'num1',
      },
      {
        title: '网络贷款类放款机构数(个)',
        dataIndex: 'num2',
        key: 'num2',
      },
      {
        title: '最近一次放款日期',
        dataIndex: 'last_apply_time',
        key: 'last_apply_time',
      },
      {
        title: '距离最近一次放款日期已有(天)',
        dataIndex: 'day',
        key: 'day',
      },
    ],
    scoreTableData5: [],
    scoreTableColumns6: [
      {
        title: ' ',
        dataIndex: 'key',
        key: 'key',
      },
      {
        title: '近1个月',
        dataIndex: 'month1',
        key: 'month1',
      },
      {
        title: '近3个月',
        dataIndex: 'month3',
        key: 'month3',
      },
      {
        title: '近6个月',
        dataIndex: 'month6',
        key: 'month6',
      }
    ],
    scoreTableData6: [],
    scoreTableColumns7: [
      {
        title: ' ',
        dataIndex: 'key',
        key: 'key',
      },
      {
        title: '近1个月',
        dataIndex: 'month1',
        key: 'month1',
      },
      {
        title: '近12个月',
        dataIndex: 'month12',
        key: 'month12',
      },
    ],
    scoreTableData7: [],
    scoreTableColumns8: [
      {
        title: '序号',
        dataIndex: 'index',
        key: 'index',
      },
      {
        title: '逾期金额区间(元)',
        dataIndex: 'overdue_money',
        key: 'overdue_money',
      },
      {
        title: '逾期时间',
        dataIndex: 'overdue_time',
        key: 'overdue_time',
      },
      {
        title: '逾期时长',
        dataIndex: 'overdue_day',
        key: 'overdue_day',
      },
      {
        title: '是否结清',
        dataIndex: 'settlement',
        key: 'settlement',
      },
    ],
    scoreTableData8: [], */
    scoreTableColumns9: [
      {
        title: '序号',
        dataIndex: 'index',
        key: 'index',
        align: 'center',
      },
      {
        title: '审结日期',
        dataIndex: 'sort_time_string',
        key: 'sort_time_string',
        align: 'center',
      },
      {
        title: '类型',
        dataIndex: 'data_type',
        key: 'data_type',
        align: 'center',
      },
      {
        title: '摘要说明',
        dataIndex: 'summary',
        key: 'summary',
        align: 'center',
      },
      {
        title: '匹配度',
        dataIndex: 'compatibility',
        key: 'compatibility',
        align: 'center',
      },
    ],
    scoreTableData9: [],
    scoreTableColumns10: [
      {
        title: ' ',
        dataIndex: 'key',
        key: 'key',
      },
      {
        title: '还款次数',
        dataIndex: 'num',
        key: 'num',
        align: 'center',
      },
      {
        title: '还款平台数',
        dataIndex: 'num2',
        key: 'num2',
        align: 'center',
      },
      {
        title: (
          <span>
            还款金额 
            <Popover content={popoverContent} title={false} trigger="click">
              <QuestionCircleOutlined style={{marginLeft: 10}} />
            </Popover>
          </span>
        ),
        dataIndex: 'num3',
        key: 'num3',
        align: 'center',
      },
    ],
    scoreTableData10: [],
    scoreTableColumns11: [
      {
        title: ' ',
        dataIndex: 'key',
        key: 'key',
      },
      {
        title: '逾期次数',
        dataIndex: 'num',
        key: 'num',
        align: 'center',
      },
      {
        title: (
          <span>
            贷款逾期最大金额
            <Popover content={popoverContent} title={false} trigger="click">
              <QuestionCircleOutlined style={{marginLeft: 10}} />
            </Popover>
          </span>
        ),
        dataIndex: 'max',
        key: 'max',
        align: 'center',
      },
    ],
    scoreTableData11: [],
    scoreTableColumns12: [
      {
        title: ' ',
        dataIndex: 'key',
        key: 'key',
      },
      {
        title: '近1个月',
        dataIndex: 'm1',
        key: 'm1',
        align: 'center',
      },
      {
        title: '近3个月',
        dataIndex: 'm3',
        key: 'm3',
        align: 'center',
      },
      {
        title: '近6个月',
        dataIndex: 'm6',
        key: 'm6',
        align: 'center',
      },
      /* {
        title: '近12个月',
        dataIndex: 'm12',
        key: 'm12',
      } */
    ],
    scoreTableData12: [],
    scoreTableColumns13: [
      {
        title: '历史租赁最高分',
        dataIndex: 'max',
        key: 'max',
        align: 'center',
      },
      {
        title: '历史租赁风险标签',
        dataIndex: 'tag',
        key: 'tag',
        align: 'center',
      },
    ],
    scoreTableData13: [],
  };
  componentDidMount() {
    const {
      creditInfo,
      userOrders,
      userOrderInfoDto,
      orderAddressDto,
      productInfo,
      userOrderCashesDto,
    } = this.state;
    console.log(11, creditInfo);

    /* const scoreTableData8 = [];
    if (
      creditInfo.interfaceMultiplePlatformsV1 &&
      creditInfo.interfaceMultiplePlatformsV1.data_list
    ) {
      creditInfo.interfaceMultiplePlatformsV1.data_list.map((item, index) => {
        scoreTableData8.push({
          index: index + 1,
          overdue_money: item.overdue_money,
          overdue_time: item.overdue_time,
          overdue_day: item.overdue_day,
          settlement: item.settlement === 'Y' ? '是' : '否',
        });
      });
    } */

    const scoreTableData9 = [];
    if (creditInfo.interfacePersonalLawList) {
      creditInfo.interfacePersonalLawList.map((item, index) => {
        scoreTableData9.push({
          index: index + 1,
          sort_time_string: item.sort_time_string,
          data_type: item.data_type,
          summary: item.summary,
          compatibility: item.compatibility,
        });
      });
    }

    this.setState({
      scoreTableData1: [
        {
          key: 1,
          score: creditInfo.verifyResult && creditInfo.verifyResult.score,
          suggest: creditInfo.verifyResult && creditInfo.verifyResult.suggest,
          refuse_details: creditInfo.verifyResult && creditInfo.verifyResult.refuse_details,
        },
      ],
      scoreTableData2: [
        {
          key: '近7天',
          a1:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.common_amortize_platform_d7,
          a2:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.credit_card_d7,
          a3:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.large_finance_platform_d7,
          a4:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.other_platform_d7,
          a5:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.p2p_platform_d7,
          a6:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.petty_loan_platform_d7,
          a7:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.platform_total_d7,
        },
        {
          key: '近30天',
          a1:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.common_amortize_platform_d30,
          a2:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.credit_card_d30,
          a3:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.large_finance_platform_d30,
          a4:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.other_platform_d30,
          a5:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.p2p_platform_d30,
          a6:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.petty_loan_platform_d30,
          a7:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.platform_total_d30,
        },
        {
          key: '近90天',
          a1:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.common_amortize_platform_d90,
          a2:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.credit_card_d90,
          a3:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.large_finance_platform_d90,
          a4:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.other_platform_d90,
          a5:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.p2p_platform_d90,
          a6:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.petty_loan_platform_d90,
          a7:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.platform_total_d90,
        },
      ],
      scoreTableData2_5: [
        {
          key: 1,
          num1:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.ident_to_phone_counts,
          num2:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.phone_to_ident_counts,
        },
      ],
      /* scoreTableData3: [
        {
          key: 1,
          num1:
            (creditInfo.interfaceMultiplePlatformsV1 &&
              creditInfo.interfaceMultiplePlatformsV1.consumer_apply_mechanism_number !==
                undefined &&
              creditInfo.interfaceMultiplePlatformsV1.consumer_apply_mechanism_number + '') ||
            '----',
          num2:
            (creditInfo.interfaceMultiplePlatformsV1 &&
              creditInfo.interfaceMultiplePlatformsV1.network_loan_apply_number !== undefined &&
              creditInfo.interfaceMultiplePlatformsV1.network_loan_apply_number + '') ||
            '----',
          last_apply_time:
            (creditInfo.interfaceMultiplePlatformsV1 &&
              creditInfo.interfaceMultiplePlatformsV1.last_apply_time) ||
            '----',
          day:
            creditInfo.interfaceMultiplePlatformsV1 &&
            creditInfo.interfaceMultiplePlatformsV1.recent_apply_date_list &&
            creditInfo.interfaceMultiplePlatformsV1.recent_apply_date_list.length
              ? JSON.stringify(creditInfo.interfaceMultiplePlatformsV1.recent_apply_date_list)
              : '----',
        },
      ],
      scoreTableData4: [
        {
          key: '申请次数(次)',
          month1:
            creditInfo.interfaceMultiplePlatformsV1 &&
            creditInfo.interfaceMultiplePlatformsV1.apply_time1,
          month3:
            creditInfo.interfaceMultiplePlatformsV1 &&
            creditInfo.interfaceMultiplePlatformsV1.apply_time3,
          month6:
            creditInfo.interfaceMultiplePlatformsV1 &&
            creditInfo.interfaceMultiplePlatformsV1.apply_time6,
          month12:
            creditInfo.interfaceMultiplePlatformsV1 &&
            creditInfo.interfaceMultiplePlatformsV1.apply_time12,
        },
      ],
      scoreTableData5: [
        {
          key: 1,
          num1:
            (creditInfo.interfaceMultiplePlatformsV1 &&
              creditInfo.interfaceMultiplePlatformsV1.cflenders_12 !== undefined &&
              creditInfo.interfaceMultiplePlatformsV1.cflenders_12 + '') ||
            '----',
          num2:
            (creditInfo.interfaceMultiplePlatformsV1 &&
              creditInfo.interfaceMultiplePlatformsV1.nllenders_12 !== undefined &&
              creditInfo.interfaceMultiplePlatformsV1.nllenders_12 + '') ||
            '----',
          last_apply_time:
            (creditInfo.interfaceMultiplePlatformsV1 &&
              creditInfo.interfaceMultiplePlatformsV1.lend_time) ||
            '----',
          day:
            creditInfo.interfaceMultiplePlatformsV1 &&
            creditInfo.interfaceMultiplePlatformsV1.recent_lend_date_list &&
            creditInfo.interfaceMultiplePlatformsV1.recent_lend_date_list.length
              ? JSON.stringify(creditInfo.interfaceMultiplePlatformsV1.recent_lend_date_list)
              : '----',
        },
      ],
      scoreTableData6: [
        {
          key: '放款次数(次)',
          month1:
            creditInfo.interfaceMultiplePlatformsV1 &&
            creditInfo.interfaceMultiplePlatformsV1.lend_number1,
          month3:
            creditInfo.interfaceMultiplePlatformsV1 &&
            creditInfo.interfaceMultiplePlatformsV1.lend_number3,
          month6:
            creditInfo.interfaceMultiplePlatformsV1 &&
            creditInfo.interfaceMultiplePlatformsV1.lend_number6,
          // month12: creditInfo.interfaceMultiplePlatformsV1 && creditInfo.interfaceMultiplePlatformsV1.lend_number12,
        },
      ],
      scoreTableData7: [
        {
          key: '履约次数(次)',
          month1:
            creditInfo.interfaceMultiplePlatformsV1 &&
            creditInfo.interfaceMultiplePlatformsV1.repay_succ1,
          month12:
            creditInfo.interfaceMultiplePlatformsV1 &&
            creditInfo.interfaceMultiplePlatformsV1.repay_succ12,
        },
        {
          key: '还款异常次数(次)',
          month1:
            creditInfo.interfaceMultiplePlatformsV1 &&
            creditInfo.interfaceMultiplePlatformsV1.repay_fail1,
          month12:
            creditInfo.interfaceMultiplePlatformsV1 &&
            creditInfo.interfaceMultiplePlatformsV1.repay_fail12,
        },
      ],
      scoreTableData8, */
      scoreTableData9,
      scoreTableData10: [
        {
          key: '近7天',
          num:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.d7_payment_count,
          num2:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.d7_payment_platform,
          num3:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.d7_payment_money,
        },
        {
          key: '近1个月',
          num:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.m1_payment_count,
          num2:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.m1_payment_platform,
          num3:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.m1_payment_money,
        },
        {
          key: '近3个月',
          num:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.m3_payment_count,
          num2:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.m3_payment_platform,
          num3:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.m3_payment_money,
        },
        {
          key: '近6个月',
          num:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.m6_payment_count,
          num2:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.m6_payment_platform,
          num3:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.m6_payment_money,
        },
      ],
      scoreTableData11: [
        {
          key: '近7天',
          num:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.d7_overdue_count,
          max:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.d7_overdue_maxmoney,
        },
        {
          key: '近1个月',
          num:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.m1_overdue_count,
          max:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.m1_overdue_maxmoney,
        },
        {
          key: '近3个月',
          num:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.m3_overdue_count,
          max:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.m3_overdue_maxmoney,
        },
        {
          key: '近6个月',
          num:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.m6_overdue_count,
          max:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.m6_overdue_maxmoney,
        },
      ],
      scoreTableData12: [
        {
          key: '租赁申请次数',
          m1:
            creditInfo.interfaceRentHistory &&
            creditInfo.interfaceRentHistory.rent_history_times_m1,
          m3:
            creditInfo.interfaceRentHistory &&
            creditInfo.interfaceRentHistory.rent_history_times_m3,
          m6:
            creditInfo.interfaceRentHistory &&
            creditInfo.interfaceRentHistory.rent_history_times_m6,
          m12:
            creditInfo.interfaceRentHistory &&
            creditInfo.interfaceRentHistory.rent_history_times_m12,
        },
      ],
      scoreTableData13: [
        {
          key: '1',
          max:
            creditInfo.interfaceRentHistory &&
            creditInfo.interfaceRentHistory.rent_history_max_grade,
          tag:
            creditInfo.interfaceRentHistory &&
            creditInfo.interfaceRentHistory.rent_history_sign &&
            creditInfo.interfaceRentHistory.rent_history_sign.length
              ? creditInfo.interfaceRentHistory.rent_history_sign.split(',')
              : '--',
        },
      ],
      barData: [
        {
          year: '近7天',
          num:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.d7_register_count,
        },
        {
          year: '近1个月',
          num:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.m1_register_count,
        },
        {
          year: '近3个月',
          num:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.m3_register_count,
        },
        {
          year: '近6个月',
          num:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.m6_register_count,
        },
      ],
    });
  }

  render() {
    const {
      creditInfo,
      userOrders,
      userOrderInfoDto,
      orderAddressDto,
      productInfo,
      userOrderCashesDto,
    } = this.state;

    return (
      <div className={'container ' + styles.creditDetailContainer}>
        <div className={styles.containerDiv}>
          <Table
            bordered
            className="table-content"
            rowKey={record => record.key}
            columns={this.state.scoreTableColumns1}
            dataSource={this.state.scoreTableData1}
            pagination={false}
          />
          {creditInfo.verifyResult && creditInfo.verifyResult.refuse_details ? (
            ''
          ) : (
            <Fragment>
              <div className={styles.titleHeader}>
                <span className={styles.titleHeaderSpan}>///</span> 基本信息{' '}
                <span className={styles.titleHeaderSpan}>///</span>
              </div>
              <div className={styles.floatHalf}>
                <span>
                  姓名：
                  <span>{userOrders.userName && userOrders.userName.substr(0, 1) + '**'}</span>
                </span>
                <span>
                  身份证号：
                  <span>
                    {userOrders.idCard &&
                      userOrders.idCard.substr(0, 3) +
                        '***********' +
                        userOrders.idCard.substr(14, 18)}
                  </span>
                </span>
                <span>
                  手机号：
                  <span>
                    {userOrders.telephone &&
                      userOrders.telephone.substr(0, 3) +
                        '****' +
                        userOrders.telephone.substr(7, 11)}
                  </span>
                </span>
                <span>
                  年龄：
                  <span>{creditInfo.interfaceIdcard && creditInfo.interfaceIdcard.idcard_age}</span>
                </span>
                <span>
                  号码归属地：
                  <span>
                    {creditInfo.interfaceBeforeAntifraud &&
                      creditInfo.interfaceBeforeAntifraud.phone_address}
                  </span>
                </span>
                <span>
                  在网时长：
                  <span>
                    {this.state.timeArr[
                      creditInfo.interfacePhoneNetworkTime &&
                        creditInfo.interfacePhoneNetworkTime.phone_network_time
                    ] || ''}
                  </span>
                </span>
                <span>
                  户籍：
                  <span>
                    {creditInfo.interfaceBeforeAntifraud &&
                      creditInfo.interfaceBeforeAntifraud.ident_number_address}
                  </span>
                </span>
                <span>
                  运营商名称：
                  <span>
                    {creditInfo.interfacePhoneNetworkTime &&
                      creditInfo.interfacePhoneNetworkTime.isp}
                  </span>
                </span>
              </div>
              <div className={styles.titleHeader}>
                <span className={styles.titleHeaderSpan}>///</span> 租赁信息{' '}
                <span className={styles.titleHeaderSpan}>///</span>
              </div>
              <div className={styles.floatHalf}>
                <span>
                  收货人：
                  <span>
                    {orderAddressDto.realname && orderAddressDto.realname.substr(0, 1) + '**'}
                  </span>
                </span>
                <span>
                  收货人手机号：
                  <span>
                    {orderAddressDto.telephone &&
                      orderAddressDto.telephone.substr(0, 3) +
                        '****' +
                        orderAddressDto.telephone.substr(7, 11)}
                  </span>
                </span>
                <span>
                  在途订单：<span>{userOrderInfoDto.userPayCount}</span>
                </span>
                <span>
                  商品名称：
                  <span>{productInfo && productInfo.length && productInfo[0].productName}</span>
                </span>
                <span>
                  租赁期限(天)：<span>{userOrderInfoDto.rentDuration}</span>
                </span>
                <span>
                  完结订单：<span>{userOrderInfoDto.userFinishCount}</span>
                </span>
                <span>
                  下单时间：<span>{userOrderInfoDto.createTime}</span>
                </span>
                <span>
                  收货人地址：<span>{orderAddressDto.street}</span>
                </span>
              </div>
              <div className={styles.riskTotal + ' ' + styles.marginTop0}>
                <p>冻结额度</p>
                <p>
                  <span className={styles.errorScore}>
                    {userOrderCashesDto &&
                      userOrderCashesDto.length &&
                      userOrderCashesDto[0].freezePrice}
                  </span>
                </p>
              </div>
              {/* <div className={styles.rentInfo + ' ' + styles.rentInfo2}>
                  <div>
                    <p>冻结额度</p>
                    <p>{userOrderCashesDto && userOrderCashesDto.length && userOrderCashesDto[0].freezePrice}</p>
                  </div>
                  <div>
                    <p>信用减免</p>
                    <p>{userOrderCashesDto && userOrderCashesDto.length && userOrderCashesDto[0].creditDeposit}</p>
                  </div>
                  <div>
                    <p>实付押金</p>
                    <p>{userOrderCashesDto && userOrderCashesDto.length && userOrderCashesDto[0].userOrderCashesDto.deposit}</p>
                  </div>
              </div> */}
              <div className={styles.rentInfo + ' ' + styles.rentInfo2}>
                <div>
                  <p>总租金</p>
                  <p>
                    {userOrderCashesDto &&
                      userOrderCashesDto.length &&
                      userOrderCashesDto[0].totalRent}
                  </p>
                </div>
                <div>
                  <p>月租金</p>
                  <p>
                    {userOrderCashesDto &&
                      userOrderCashesDto.length &&
                      userOrderCashesDto[0].originalRent}
                  </p>
                </div>
              </div>
              <div className={styles.titleHeader}>
                <span className={styles.titleHeaderSpan}>///</span> 风险名单{' '}
                <span className={styles.titleHeaderSpan}>///</span>
              </div>
              {/* <div className={styles.riskTotal}>
                <p>特殊关注名单</p>
                <p>
                  {creditInfo.interfaceSpecialLevelList &&
                  creditInfo.interfaceSpecialLevelList.result_code_desc &&
                  !creditInfo.interfaceSpecialLevelList.result_code_desc.includes('未命中') ? (
                    <span className={styles.errorScore}>命中</span>
                  ) : (
                    <span>未命中</span>
                  )}
                </p>
              </div> */}
              <div className={styles.floatHalf}>
                <span>
                  {creditInfo.interfaceSpecialLevelList &&
                  creditInfo.interfaceSpecialLevelList.result_code_desc &&
                  !creditInfo.interfaceSpecialLevelList.result_code_desc.includes('未命中') ? (
                    <span className={styles.errorScore}>特殊关注名单：命中</span>
                  ) : (
                    <span>特殊关注名单：未命中</span>
                  )}
                </span>
                <span>
                  {!(
                    creditInfo.interfaceRentHistory &&
                    creditInfo.interfaceRentHistory.rent_history_black
                  ) ? (
                    <span>租赁特殊关注名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>租赁特殊关注名单：命中</span>
                  )}
                </span>
                <span>
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.census_register_hign_risk_area
                  ) ? (
                    <span>归属地位于高风险集中地区：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>归属地位于高风险集中地区：命中</span>
                  )}
                </span>
                <span>
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.court_break_faith_list
                  ) ? (
                    <span>法院失信名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>法院失信名单：命中</span>
                  )}
                </span>
                <span>
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.crime_manhunt_list
                  ) ? (
                    <span>犯罪通缉名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>犯罪通缉名单：命中</span>
                  )}
                </span>
                <span>
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.court_execute_list
                  ) ? (
                    <span>法院执行名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>法院执行名单：命中</span>
                  )}
                </span>
                <span>
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.student_loan_arrearage_list
                  ) ? (
                    <span>助学贷款欠费名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>助学贷款欠费名单：命中</span>
                  )}
                </span>
                <span>
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.credit_overdue_list
                  ) ? (
                    <span>信贷逾期名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>信贷逾期名单：命中</span>
                  )}
                </span>
                <span>
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.hign_risk_focus_list
                  ) ? (
                    <span>高风险关注名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>高风险关注名单：命中</span>
                  )}
                </span>
                <span>
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.car_rental_break_contract_list
                  ) ? (
                    <span>车辆租赁违约名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>车辆租赁违约名单：命中</span>
                  )}
                </span>
                <span>
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.court_case_list
                  ) ? (
                    <span>法院结案名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>法院结案名单：命中</span>
                  )}
                </span>
                <span>
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.riding_break_contract_list
                  ) ? (
                    <span>故意违章乘车名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>故意违章乘车名单：命中</span>
                  )}
                </span>
                <span>
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.owing_taxes_list
                  ) ? (
                    <span>欠税名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>欠税名单：命中</span>
                  )}
                </span>
                <span>
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.owing_taxes_legal_person_list
                  ) ? (
                    <span>欠税公司法人代表名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>欠税公司法人代表名单：命中</span>
                  )}
                </span>
                <span>
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.virtual_number_base
                  ) ? (
                    <span>虚拟号码库：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>虚拟号码库：命中</span>
                  )}
                </span>
                <span>
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.small_number_base
                  ) ? (
                    <span>通讯小号库：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>通讯小号库：命中</span>
                  )}
                </span>
              </div>
              {/* <div className={styles.titleHeader}>
                <span className={styles.titleHeaderSpan}>///</span> 分数类{' '}
                <span className={styles.titleHeaderSpan}>///</span>
              </div>
              <div className={styles.riskTotal + ' ' + styles.marginTop0}>
                <p>贝多分</p>
                <p>
                  <span className={styles.errorScore}>
                    {creditInfo.interfaceBetterScore &&
                      creditInfo.interfaceBetterScore.better_score}
                  </span>
                </p>
              </div> */}
              <div className={styles.titleHeader}>
                <span className={styles.titleHeaderSpan}>///</span> 关联类{' '}
                <span className={styles.titleHeaderSpan}>///</span>
              </div>
              <Table
                bordered
                className={styles.marginBottom + ' ' + styles.tableContent}
                rowKey={record => record.key}
                columns={this.state.scoreTableColumns2_5}
                dataSource={this.state.scoreTableData2_5}
                pagination={false}
              />
              <div className={styles.titleHeader}>
                <span className={styles.titleHeaderSpan}>///</span> 申请类{' '}
                <span className={styles.titleHeaderSpan}>///</span>
              </div>
              <Table
                bordered
                className={styles.tableContentFirstWhite}
                rowKey={record => record.key}
                columns={this.state.scoreTableColumns2}
                dataSource={this.state.scoreTableData2}
                pagination={false}
              />
              <div className={styles.titleHeader}>
                <span className={styles.titleHeaderSpan}>///</span> 注册类{' '}
                <span className={styles.titleHeaderSpan}>///</span>
              </div>
              <p className={styles.textTitle}>注册次数</p>
              <Chart forceFit height={400} data={this.state.barData} scale={this.state.barScale}>
                <Tooltip />
                <Axis />
                <Bar position="year*num" />
                <Interval 
                  position="year*num" 
                  opacity={1}
                  label={
                    ['num', {
                      offset: 10,
                      textStyle: {
                        fill: '#1E90FF',
                        fontSize: 14
                      }
                    }]
                  } 
                />
              </Chart>
              <div className={styles.titleHeader}>
                <span className={styles.titleHeaderSpan}>///</span> 还款类{' '}
                <span className={styles.titleHeaderSpan}>///</span>
              </div>
              <Table
                bordered
                className={styles.marginBottom + ' ' + styles.tableContentFirstWhite}
                rowKey={record => record.key}
                columns={this.state.scoreTableColumns10}
                dataSource={this.state.scoreTableData10}
                pagination={false}
              />
              <div className={styles.titleHeader}>
                <span className={styles.titleHeaderSpan}>///</span> 逾期类{' '}
                <span className={styles.titleHeaderSpan}>///</span>
              </div>
              <Table
                bordered
                className={styles.marginBottom + ' ' + styles.tableContentFirstWhite}
                rowKey={record => record.key}
                columns={this.state.scoreTableColumns11}
                dataSource={this.state.scoreTableData11}
                pagination={false}
              />
              <div className={styles.titleHeader}>
                <span className={styles.titleHeaderSpan}>///</span> 历史租赁{' '}
                <span className={styles.titleHeaderSpan}>///</span>
              </div>
              <Table
                bordered
                className={styles.marginBottom + ' ' + styles.tableContentFirstWhite}
                rowKey={record => record.key}
                columns={this.state.scoreTableColumns12}
                dataSource={this.state.scoreTableData12}
                pagination={false}
              />
              <Table
                bordered
                className={styles.marginBottom + ' ' + styles.tableContent}
                rowKey={record => record.key}
                columns={this.state.scoreTableColumns13}
                dataSource={this.state.scoreTableData13}
                pagination={false}
              />
              {/* <div className={styles.titleHeader + ' ' + styles.marginTop0}><span className={styles.titleHeaderSpan}>///</span> 历史借贷行为 <span className={styles.titleHeaderSpan}>///</span></div> */}
              {/* <p>
                近12个月申请机构总数(个):
                {(creditInfo.interfaceMultiplePlatformsV1 &&
                  creditInfo.interfaceMultiplePlatformsV1.apply_platforms_m12) ||
                  0}
              </p> */}
              {/* <Table
                bordered
                className={styles.marginBottom}
                rowKey={record => record.key}
                columns={this.state.scoreTableColumns3}
                dataSource={this.state.scoreTableData3}
                pagination={false}
              />
              <Table
                bordered
                className={styles.marginBottom}
                rowKey={record => record.key}
                columns={this.state.scoreTableColumns4}
                dataSource={this.state.scoreTableData4}
                pagination={false}
              />
              <p>
                近12个月放款机构总数(个):
                {(creditInfo.interfaceMultiplePlatformsV1 &&
                  creditInfo.interfaceMultiplePlatformsV1.lenders) ||
                  0}
              </p>
              <Table
                bordered
                className={styles.marginBottom}
                rowKey={record => record.key}
                columns={this.state.scoreTableColumns5}
                dataSource={this.state.scoreTableData5}
                pagination={false}
              />
              <Table
                bordered
                className={styles.marginBottom}
                rowKey={record => record.key}
                columns={this.state.scoreTableColumns6}
                dataSource={this.state.scoreTableData6}
                pagination={false}
              />
              <Table
                bordered
                className={styles.marginBottom}
                rowKey={record => record.key}
                columns={this.state.scoreTableColumns7}
                dataSource={this.state.scoreTableData7}
                pagination={false}
              />
              <div className={styles.titleHeader}><span className={styles.titleHeaderSpan}>///</span> 历史逾期记录 <span className={styles.titleHeaderSpan}>///</span></div>
              <div className={styles.floatHalf}>
                <span>
                  近6个月逾期机构次数：
                  <span>
                    {creditInfo.interfaceMultiplePlatformsV1 &&
                      creditInfo.interfaceMultiplePlatformsV1.overdue_mechanism_number}
                  </span>
                </span>
                <span>
                  近6个月逾期总次数：
                  <span>
                    {creditInfo.interfaceMultiplePlatformsV1 &&
                      creditInfo.interfaceMultiplePlatformsV1.overdue_total_counts}
                  </span>
                </span> */}
              {/* <span>近6个月未结清逾期次数：<span>{creditInfo.interfaceMultiplePlatformsV1 && creditInfo.interfaceMultiplePlatformsV1.apply_platforms_m12}</span></span> */}
              {/* <span>
                  近6个月逾期总金额(元)：
                  <span>
                    {creditInfo.interfaceMultiplePlatformsV1 &&
                      creditInfo.interfaceMultiplePlatformsV1.overdue_total_money}
                  </span>
                </span>
              </div>
              <Table
                bordered
                rowKey={record => record.index}
                columns={this.state.scoreTableColumns8}
                dataSource={this.state.scoreTableData8}
                pagination={false}
              />
              <p className={styles.tips}>
                说明：S代表期数，1期=7天，s0表示不到7天，s1代表7-14天，以此类推；M代表期数，1期=30天，m0表示不到30天，m1表示30-60天，以此类推。
              </p>
              <div className={styles.titleHeader}><span className={styles.titleHeaderSpan}>///</span> 关联风险检测 <span className={styles.titleHeaderSpan}>///</span></div>
              <div>
                <p>
                  3个月身份证关联手机号数(个)：
                  <span>
                    {creditInfo.interfaceBeforeAntifraud &&
                      creditInfo.interfaceBeforeAntifraud.ident_to_phone_counts}
                  </span>
                </p>
                <p>
                  3个月身份证关联身份证数(个)：
                  <span>
                    {creditInfo.interfaceBeforeAntifraud &&
                      creditInfo.interfaceBeforeAntifraud.phone_to_ident_counts}
                  </span>
                </p>
              </div> */}
              <div className={styles.titleHeader}>
                <span className={styles.titleHeaderSpan}>///</span> 法院信息{' '}
                <span className={styles.titleHeaderSpan}>///</span>
              </div>
              <Table
                bordered
                rowKey={record => record.index}
                columns={this.state.scoreTableColumns9}
                dataSource={this.state.scoreTableData9}
                pagination={false}
              />
            </Fragment>
          )}
        </div>
      </div>
    );
  }
}

export default CreditDetail;
