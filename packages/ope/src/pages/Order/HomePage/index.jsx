import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import { Card, Tooltip, Button, Form, Input, Select, DatePicker, Spin, message } from 'antd';
import MyPageTable from '@/components/MyPageTable';
import { onTableData, getParam, renderOrderStatus } from '@/utils/utils.js';
import CopyToClipboard from '@/components/CopyToClipboard';
import { getTimeDistance } from '@/utils/utils';
import moment from 'moment';
import { orderCloseStatusMap, orderStatusMap,orderCloseStatusMaps } from '@/utils/enum';
import { exportCenterHandler } from '../util';
import { UserRating } from 'zwzshared';
const { Option } = Select;
const { RangePicker } = DatePicker;

@connect(({ order, loading }) => ({
  ...order,
  loading: loading.effects['order/queryOpeOrderByConditionList'],
}))
@Form.create()
export default class HomePage extends Component {
  state = {
    current: 1,
    datas: {},
    yunTime: getTimeDistance('month'),
    gBtusua: '', // 默认状态: 待发货-取消该初始状态
  };
  componentWillUnmount() {
    this.setState = () => false;
}
  columns = [
    {
      title: '订单编号',
      // dataIndex: 'orderId',
      width: 160,
      render: e => (
        <>
          {e.isTransfer && <div>转单</div>}
          <CopyToClipboard text={e.orderId} />
        </>
      ),
      // render: orderId => <CopyToClipboard text={orderId} />,
    },
    {
      title: '店铺名称',
      dataIndex: 'shopName',
      width: '10%',
      render: shopName => {
        return (
          <>
            <Tooltip placement="top" title={shopName}>
              <div
                style={{
                  width: 60,
                  /* overflow: 'hidden',
                  textOverflow: 'ellipsis',
                  whiteSpace: 'nowrap', */
                }}
              >
                {shopName}
              </div>
            </Tooltip>
          </>
        );
      },
    },
    {
      title: '渠道来源',
      dataIndex: 'channelName',
      width: '10%',
    },
    {
      title: '商品名称',
      dataIndex: 'productName',
      width: 160,
      // ellipsis: true,
    },
    {
      title: '已付期数',
      width: 110,
      render: e => {
        return (
          <>
            <Tooltip placement="top" title={e.currentPeriods}>
              <div
                style={{
                  width: 60,
                }}
              >
                {e.payedPeriods}/{e.totalPeriods}
              </div>
            </Tooltip>
          </>
        );
      },
    },
    {
      title: '总租金',
      width: 90,
      render: e => {
        return <>{e.totalRentAmount}</>;
      },
    },
    {
      title: '已付租金',
      width: 90,
      render: e => {
        return <>{e.payedRentAmount}</>;
      },
    },
    {
      title: '下单人姓名',
      width: 120,
      render: e => {
        return <>{e.realName}</>;
      },
    },
    {
      title: '下单人手机号',
      width: 120,
      render: e => {
        return <>{e.telephone}</>;
      },
    },
    {
      title: '下单时间',
      dataIndex: 'placeOrderTime',
      width: 120,
    },
    {
      title: '收货人手机号',
      width: '120px',
      render: e => {
        return <>{e.addressUserPhone}</>;
      },
    },
    {
      title: '起租时间',
      width: '120px',
      render: e => {
        return <>{e.rentStart}</>;
      },
    },
    {
      title: '归还时间',
      width: '120px',
      render: e => {
        return <>{e.unrentTime}</>;
      },
    },
    {
      title: '订单状态',
      dataIndex: 'status',
      width: 120,
      render: (_, record) => renderOrderStatus(record),
    },
    {
      title: '操作',
      width: 120,
      fixed: 'right',
      align: 'center',
      render: (e, record) => {
        return (
          <div>
            <a
              className="primary-color"
              // onClick={() => router.push(`/Order/Details?id=${e.orderId}`)}
              href={`#/Order/HomePage/Details?id=${e.orderId}`}
              target="_blank"
            >
              处理
            </a>
          </div>
        );
      },
    },
  ];

  componentDidMount() {
    const orderStatusFromUrl = getParam('status') || ''; // 从链接中取出订单状态的值

    const { setFieldsValue } = this.props.form;
    setFieldsValue({ status: orderStatusFromUrl });
    this.setState({ gBtusua: orderStatusFromUrl });

    this.onList(1, 10, {
      status: orderStatusFromUrl
      // status: this.state.gBtusua,
    });
    this.props.dispatch({
      type: 'order/PlateformChannel',
    });
  }

  onList = (pageNumber, pageSize, data = {}) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'order/queryOpeOrderByConditionList',
      payload: {
        pageSize,
        pageNumber,
        // status: getParam('status') || '',
        ...data,
      },
    });
  };

  // 重置
  handleReset = e => {
    this.setState(
      {
        gBtusua: '',
      },
      () => {
        this.props.form.resetFields();
        this.props.form.setFieldsValue({
          status: undefined,
        });
        this.handleSubmit(e);
      },
    );
  };

  handleSubmit = e => {
    e && e.preventDefault();
    this.props.form.validateFields((err, values) => {
      this.setState({ current: 1 });
      if (values.createDate && values.createDate.length < 1) {
        values.createTimeStart = '';
        values.createTimeEnd = '';
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      } else if (values.createDate) {
        values.createTimeStart = moment(values.createDate[0]).format('YYYY-MM-DD HH:mm:ss');
        values.createTimeEnd = moment(values.createDate[1]).format('YYYY-MM-DD HH:mm:ss');
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      } else {
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      }
    });
  };

  onexport = () => {
    exportCenterHandler(this, 'exportRentOrder', true, false);
    // if (this.props.form.getFieldValue('createDate')) {
    //   this.props.dispatch({
    //     type: 'order/exportOpeAllUserOrders',
    //     payload: {
    //       createTimeEnd: moment(this.props.form.getFieldValue('createDate')[1]).format(
    //         'YYYY-MM-DD HH:mm:ss',
    //       ),
    //       createTimeStart: moment(this.props.form.getFieldValue('createDate')[0]).format(
    //         'YYYY-MM-DD HH:mm:ss',
    //       ),
    //     },
    //   });
    // } else {
    //   this.props.dispatch({
    //     type: 'order/exportOpeAllUserOrders',
    //     payload: {
    //       createTimeEnd: moment(yunTime[1]).format('YYYY-MM-DD HH:mm:ss'),
    //       createTimeStart: moment(yunTime[0]).format('YYYY-MM-DD HH:mm:ss'),
    //     },
    //   });
    // }
  };

  handleCancel = e => {
    this.setState({
      visible: '',
    });
  };

  // table 页数
  onPage = e => {
    this.setState(
      {
        current: e.current,
      },
      () => {
        const params = {
          status: this.state.gBtusua,
          ...this.state.datas,
        }
        this.onList(e.current, 10, params);
      },
    );
  };

  onChanges = e => {
    this.setState({
      gBtusua: e,
    });
  };

  render() {
    const { allList, allTotal, loading, form, PlateformList } = this.props;
    const paginationProps = {
      current: this.state.current,
      pageSize: 10,
      total: allTotal,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / 10)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };
    const { getFieldDecorator } = form;
    return (
      <PageHeaderWrapper>
        <Card bordered={false}>
          <Form layout="inline" onSubmit={this.handleSubmit}>
            <Form.Item label="商品名称">
              {getFieldDecorator(
                'productName',
                {},
              )(<Input allowClear placeholder="请输入商品名称" />)}
            </Form.Item>
            <Form.Item label="店铺名称">
              {getFieldDecorator('shopName', {})(<Input placeholder="请输入店铺名称" />)}
            </Form.Item>
            <Form.Item label="渠道来源">
              {getFieldDecorator(
                'channelId',
                {},
              )(
                <Select placeholder="渠道来源" style={{ width: 180 }} allowClear>
                  {PlateformList.map((item, val) => {
                    return (
                      <Option value={item.channelId} key={val}>
                        {item.channelName}
                      </Option>
                    );
                  })}
                </Select>,
              )}
            </Form.Item>
            <Form.Item label="下单人姓名">
              {getFieldDecorator(
                'userName',
                {},
              )(<Input allowClear placeholder="请输入下单人姓名" />)}
            </Form.Item>
            <Form.Item label="下单人手机号">
              {getFieldDecorator(
                'telephone',
                {},
              )(<Input allowClear placeholder="请输入下单人手机号" />)}
            </Form.Item>
            <Form.Item label="收货人手机号">
              {getFieldDecorator(
                'addressUserPhone',
                {},
              )(<Input allowClear placeholder="请输入收货人手机号" />)}
            </Form.Item>
            <Form.Item label="下单人身份证号">
              {getFieldDecorator(
                'idCard',
                {},
              )(<Input allowClear placeholder="请输入下单人身份证号" />)}
            </Form.Item>
            <Form.Item label="订单编号">
              {getFieldDecorator('orderId', {})(<Input allowClear placeholder="请输入订单编号" />)}
            </Form.Item>
            <Form.Item label="创建时间">
              {getFieldDecorator('createDate', {})(<RangePicker />)}
            </Form.Item>
            <Form.Item label="订单状态">
              {getFieldDecorator('status', {
                // initialValue: '04',
              })(
                <Select
                  placeholder="订单状态"
                  allowClear
                  style={{ width: 180 }}
                  onChange={this.onChanges}
                >
                  {['01', '11', '04', '05', '06', '07', '08', '09', '10'].map(value => {
                    return (
                      <Option value={value.toString()} key={value.toString()}>
                        {orderStatusMap[value.toString()]}
                      </Option>
                    );
                  })}
                </Select>,
              )}
            </Form.Item>
            {this.state.gBtusua === '11' ? (
              <Form.Item label="风控结论">
                {getFieldDecorator(
                  'rentCredit',
                  {},
                )(
                  <Select placeholder="风控结论" allowClear style={{ width: 180 }}>
                    {[{content:"系统通过",value:true},{content:"建议审核",value:false}].map(value => {
                      return (
                        <Option value={value.value} key={value.value}>
                          {value.content}
                        </Option>
                      );
                    })}
                  </Select>,
                )}
              </Form.Item>
            ) : null}
            {this.state.gBtusua === '10' ? (
              <Form.Item label="关单类型">
                {getFieldDecorator(
                  'closeType',
                  {},
                )(
                  <Select placeholder="关单类型" allowClear style={{ width: 180 }}>
                    {orderCloseStatusMaps.map(value => {
                      return (
                        <Option value={value.value} key={value.value}>
                          {value.content}
                        </Option>
                      );
                    })}
                  </Select>,
                )}
              </Form.Item>
            ) : null}
       {/*  <UserRating form={this.props.form} /> */}
            <div>
           
              <Form.Item>
                <Button type="primary" htmlType="submit">
                  查询
                </Button>
              </Form.Item>
              <Form.Item>
                <Button htmlType="button" onClick={this.handleReset}>
                  重置
                </Button>
              </Form.Item>
              <Form.Item>
                <Button onClick={this.onexport}>导出</Button>
              </Form.Item>
            </div>
          </Form>
          <Spin spinning={loading}>
            <MyPageTable
              scroll
              onPage={this.onPage}
              paginationProps={paginationProps}
              dataSource={onTableData(allList)}
              columns={this.columns}
            />
          </Spin>
        </Card>
      </PageHeaderWrapper>
    );
  }
}
