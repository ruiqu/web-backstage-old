import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Card } from 'antd';
import SearchList from '@/components/SearchList';
import TransferService from './service';
import CopyToClipboard from '@/components/CopyToClipboard';
export default class Transfer extends Component {
  state = {
    current: 1,
    list: [],
  };
  handleFilter = (pageNumber, pageSize, data = {}) => {
    this.setState(
      {
        loading: true,
      },
      () => {
        TransferService.list({
          pageNumber,
          pageSize,
          ...data,
        }).then(res => {
          if (res) {
            this.setState({
              list: res.records,
              total: res.total,
              loading: false,
              current: res.current,
            });
          }
        });
      },
    );
  };
  render() {
    const columns = [
      {
        title: '订单编号',
        dataIndex: 'orderId',
        render:(e)=><CopyToClipboard text={e} />
      },
      {
        title: '原商品ID',
        dataIndex: 'transferredProductId',
      },
      {
        title: '新商品ID',
        dataIndex: 'transferProductId',
      },
      {
        title: '转出商家',
        dataIndex: 'transferredShopName',
      },
      {
        title: '转入商家',
        dataIndex: 'transferShopName',
      },
      {
        title: '备注信息',
        dataIndex: 'remark',
      },
    ];

    let searchHander = [
      {
        label: '订单编号',
        type: 'input',
        key: 'orderId',
      },
      {
        label: '转出商家',
        type: 'input',
        key: 'transferredShopName',
      },
      {
        label: '转入商家',
        type: 'input',
        key: 'transferShopName',
      },
    ];

    const { loading, list = [], total = 0 } = this.state;
    return (
      <PageHeaderWrapper title={false}>
        <Card bordered={false}>
          <SearchList
            handleFilter={this.handleFilter}
            columns={columns}
            searchHander={searchHander}
            loading={loading}
            total={total}
            list={list}
            pagination={true}
          ></SearchList>
        </Card>
      </PageHeaderWrapper>
    );
  }
}
