import request from '@/services/baseService';

export default {
  //查询
 list: data => {
    return request(`/hzsx/ope/order/queryTransferOrderRecordPage`, data, 'post');
  },
};