import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import {
  Card,
  Tooltip,
  Button,
  Form,
  Input,
  Select,
  DatePicker,
  Spin,
} from 'antd';
import MyPageTable from '@/components/MyPageTable';
import { onTableData } from '@/utils/utils.js';
import CopyToClipboard from '@/components/CopyToClipboard';
import moment from 'moment';
const { Option } = Select;
const { RangePicker } = DatePicker;
import { getTimeDistance } from '@/utils/utils';
import { AuditStatus } from '@/utils/enum'
import { UserRating } from 'zwzshared';
@connect(({ order, loading }) => ({
  ...order,
  loading: loading.effects['order/queryTelephoneAuditOrder'],
}))
@Form.create()
export default class HomePage extends Component {
  state = {
    current: 1,
    datas: {},
    yunTime: getTimeDistance('month'),
  };

  componentDidMount() {
    this.onList(1, 10);
    console.log(this.props, 'props');
    this.props.dispatch({
      type: 'order/PlateformChannel',
    });
  }

  onList = (pageNumber, pageSize, data = {}) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'order/queryTelephoneAuditOrder',
      payload: {
        pageSize,
        pageNumber,
        ...data,
        examineStatus: (data && data.examineStatus) || null,
      },
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      this.setState({ current: 1 });
      if (values.createDate && values.createDate.length < 1) {
        values.createTimeStart = '';
        values.createTimeEnd = '';
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      } else if (values.createDate) {
        values.createTimeStart = moment(values.createDate[0]).format('YYYY-MM-DD HH:mm:ss');
        values.createTimeEnd = moment(values.createDate[1]).format('YYYY-MM-DD HH:mm:ss');
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      } else {
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      }
    });
  };

  //table 页数
  onPage = e => {
    this.setState(
      {
        current: e.current,
      },
      () => {
        this.onList(e.current, 10, this.state.datas);
      },
    );
  };

  // 重置
  handleReset = e => {
    this.props.form.resetFields();
    this.handleSubmit(e);
  };

  onexport = () => {
    const { yunTime } = this.state;
    if (this.props.form.getFieldValue('createDate')) {
      this.props.dispatch({
        type: 'order/exportTelephoneAuditOrders',
        payload: {
          createTimeEnd: moment(this.props.form.getFieldValue('createDate')[1]).format(
            'YYYY-MM-DD HH:mm:ss',
          ),
          createTimeStart: moment(this.props.form.getFieldValue('createDate')[0]).format(
            'YYYY-MM-DD HH:mm:ss',
          ),
        },
      });
    } else {
      this.props.dispatch({
        type: 'order/exportTelephoneAuditOrders',
        payload: {
          createTimeEnd: moment(yunTime[1]).format('YYYY-MM-DD HH:mm:ss'),
          createTimeStart: moment(yunTime[0]).format('YYYY-MM-DD HH:mm:ss'),
        },
      });
    }
  };

  render() {
    const { AuditList, AuditTotal, loading, PlateformList } = this.props;
    const paginationProps = {
      current: this.state.current,
      pageSize: 10,
      total: AuditTotal,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / 10)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };
    let detailsStute = {
      '00': '待平台审核',
      '01': '待商家审核',
      '02': '已审核',
    };
    const columns = [
      {
        title: '订单编号',
        dataIndex: 'orderId',
        width: 150,
        render: orderId => <CopyToClipboard text={orderId} />,
      },
      {
        title: '店铺名称',
        dataIndex: 'shopName',
        width: 80,
        render: shopName => {
          return (
            <>
              <Tooltip placement="top" title={shopName}>
                <div>{shopName}</div>
              </Tooltip>
            </>
          );
        },
      },
      {
        title: '渠道来源',
        width: 70,
        dataIndex: 'channelName',
      },
      {
        title: '商品名称',
        dataIndex: 'productName',
        width: '10%',
        render: productName => {
          return <div>{productName}</div>;
        },
      },
      {
        title: '已付期数',
        width: 90,
        render: e => {
          return (
            <>
              <div
                style={{
                  width: 60,
                  overflow: 'hidden',
                  textOverflow: 'ellipsis',
                  whiteSpace: 'nowrap',
                }}
              >
                {e.payedPeriods}/{e.totalPeriods}
              </div>
            </>
          );
        },
      },
      {
        title: '总租金/已付租金',
        width: 100,
        render: e => {
          return (
            <>
              {e.totalRentAmount}/{e.payedRentAmount}
            </>
          );
        },
      },
      {
        title: '下单人姓名/手机号',
        width: 100,
        render: e => {
          const styleObj = {
            wordBreak: 'break-all',
          }
          return (
            <span style={styleObj}>
              {e.realName}/<br/>{e.telephone}
            </span>
          );
        },
      },
      {
        title: '下单时间',
        dataIndex: 'placeOrderTime',
        width: 120,
      },
      {
        title: '电审状态',
        dataIndex: 'examineStatus',
        width: 90,
        render: examineStatus => {
          return <span>{detailsStute[examineStatus]}</span>;
        },
      },
      {
        title: '审核时间',
        width: 120,
        dataIndex: 'approveTime',
      },
      {
        title: '审核结论',
        dataIndex: 'approveStatus',
        width: 100,
        render: val => {
          const cn = AuditStatus[val] || '-'
          return cn
        }
      },
      {
        title: '操作',
        width: 60,
        fixed: 'right',
        render: e => {
          return (
            <div>
              <a
                href={`#/Order/Online/Details?id=${e.orderId}&telephoneAuditOrder=telephoneAuditOrder`}
                target="_blank"
              >
                处理
              </a>
            </div>
          );
        },
      },
    ];
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    return (
      <PageHeaderWrapper>
        <Card bordered={false}>
          <Form layout="inline" onSubmit={this.handleSubmit}>
            <Form.Item label="商品名称">
              {getFieldDecorator(
                'productName',
                {},
              )(<Input allowClear placeholder="请输入商品名称" />)}
            </Form.Item>
            <Form.Item label="店铺名称">
              {getFieldDecorator('shopName', {})(<Input placeholder="请输入店铺名称" />)}
            </Form.Item>
            <Form.Item label="渠道来源">
              {getFieldDecorator(
                'channelId',
                {},
              )(
                <Select placeholder="渠道来源" style={{ width: 180 }} allowClear>
                  {PlateformList.map((item, val) => {
                    return (
                      <Option value={item.channelId} key={val}>
                        {item.channelName}
                      </Option>
                    );
                  })}
                </Select>,
              )}
            </Form.Item>
            <Form.Item label="下单人姓名">
              {getFieldDecorator(
                'userName',
                {},
              )(<Input allowClear placeholder="请输入下单人姓名" />)}
            </Form.Item>
            <Form.Item label="下单人手机号">
              {getFieldDecorator(
                'telephone',
                {},
              )(<Input allowClear placeholder="请输入下单人手机号" />)}
            </Form.Item>
            <Form.Item label="订单编号">
              {getFieldDecorator('orderId', {})(<Input allowClear placeholder="请输入订单编号" />)}
            </Form.Item>
            <Form.Item label="创建时间">
              {getFieldDecorator('createDate', {})(<RangePicker allowClear />)}
            </Form.Item>
            <Form.Item label="订单状态">
              {getFieldDecorator('examineStatus', {

              })(
                <Select placeholder="订单状态" exportCenterHandlerallowClear style={{ width: 180 }}>
                  <Option value={'00'}>待平台审核</Option>
                  <Option value={'02'}>已审核</Option>
                </Select>,
              )}
            </Form.Item>
            <Form.Item label="风控结论">
              {getFieldDecorator('rentCredit', {

              })(
                <Select allowClear style={{ width: 180 }} placeholder="风控结论">
                  <Option value={true}>系统通过</Option>
                  <Option value={false}>建议审核</Option>
                </Select>,
              )}
            </Form.Item>
            <UserRating form={this.props.form} />
            <div>
              <Form.Item>
                <Button type="primary" htmlType="submit">
                  查询
                </Button>
              </Form.Item>
              <Form.Item>
                <Button htmlType="button" onClick={this.handleReset}>
                  重置
                </Button>
              </Form.Item>
              <Form.Item>
                <Button onClick={this.onexport}>导出</Button>
              </Form.Item>
            </div>
          </Form>
          <Spin spinning={loading}>
            <MyPageTable
              onPage={this.onPage}
              paginationProps={paginationProps}
              dataSource={onTableData(AuditList)}
              columns={columns}
              scroll={true}
            />
          </Spin>
        </Card>
      </PageHeaderWrapper>
    );
  }
}
