import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import { Card, Spin, Button, Form, Input, Select, Tooltip, DatePicker } from 'antd';
import MyPageTable from '@/components/MyPageTable';
import { onTableData } from '@/utils/utils.js';
import CopyToClipboard from '@/components/CopyToClipboard';
import { getTimeDistance } from '@/utils/utils';
import moment from 'moment';
import { exportCenterHandler } from '../util';
const { Option } = Select;
const { RangePicker } = DatePicker;
import { router } from 'umi';
@connect(({ order, loading }) => ({
  ...order,
  loading: loading.effects['order/queryOpeBuyOutOrdersByCondition'],
}))
@Form.create()
export default class BuyOut extends Component {
  state = {
    current: 1,
    yunTime: getTimeDistance('month'),
  };
  componentDidMount() {
    console.log(this.props, 'props');
    this.onList(1, 10, {
      state: '03',
    });
    this.props.dispatch({
      type: 'order/PlateformChannel',
    });
  }
  onList = (pageNumber, pageSize, data = {}) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'order/queryOpeBuyOutOrdersByCondition',
      payload: {
        pageSize,
        pageNumber,
        ...data,
      },
    });
  };
  onexport = () => {
    exportCenterHandler(this, 'exportBuyOutOrder', false, false);
    // const { yunTime } = this.state;
    // if (this.props.form.getFieldValue('createDate')) {
    //   this.props.dispatch({
    //     type: 'order/exportOpeBuyOutUserOrders',
    //     payload: {
    //       createTimeEnd: moment(this.props.form.getFieldValue('createDate')[1]).format(
    //         'YYYY-MM-DD HH:mm:ss',
    //       ),
    //       createTimeStart: moment(this.props.form.getFieldValue('createDate')[0]).format(
    //         'YYYY-MM-DD HH:mm:ss',
    //       ),
    //     },
    //   });
    // } else {
    //   this.props.dispatch({
    //     type: 'order/exportOpeBuyOutUserOrders',
    //     payload: {
    //       createTimeEnd: moment(yunTime[1]).format('YYYY-MM-DD HH:mm:ss'),
    //       createTimeStart: moment(yunTime[0]).format('YYYY-MM-DD HH:mm:ss'),
    //     },
    //   });
    // }
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      this.setState({ current: 1 });
      if (values.createDate && values.createDate.length < 1) {
        values.createTimeStart = '';
        values.createTimeEnd = '';
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      } else if (values.createDate) {
        values.createTimeStart = moment(values.createDate[0]).format('YYYY-MM-DD HH:mm:ss');
        values.createTimeEnd = moment(values.createDate[1]).format('YYYY-MM-DD HH:mm:ss');
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      } else {
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      }
    });
  };
  // 重置
  handleReset = e => {
    this.props.form.resetFields();
    this.props.form.setFieldsValue({
      state: undefined,
    });
    this.handleSubmit(e);
  };
  //table 页数
  onPage = e => {
    this.setState(
      {
        current: e.current,
      },
      () => {
        this.onList(e.current, 10, this.state.datas);
      },
    );
  };
  confirm = () => {};
  render() {
    const { BuyOutList, BuyOutTotal, PlateformList, loading } = this.props;
    const paginationProps = {
      current: this.state.current,
      pageSize: 10,
      total: BuyOutTotal,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / 10)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };
    const statusData = {
      '01': '取消',
      '02': '关闭',
      '03': '完成',
      '04': '待支付',
      '05': '支付中',
    };
    const columns = [
      {
        title: '订单编号',
        dataIndex: 'buyOutOrderId',
        render: buyOutOrderId => <CopyToClipboard text={buyOutOrderId} />,
        width: '10%',
      },
      /* {
        title: '店铺名称',
        dataIndex: 'shopName',
        width: '10%',
        render: shopName => {
          return (
            <>
              <Tooltip placement="top" title={shopName}>
                <div
                  style={{
                    width: 60,
                    //overflow: 'hidden',
                    //textOverflow: 'ellipsis',
                    //whiteSpace: 'nowrap',
                  }}
                >
                  {shopName}
                </div>
              </Tooltip>
            </>
          );
        },
      }, */
      {
        title: '渠道来源',
        dataIndex: 'channelName',
      },
      {
        title: '商品名称',
        dataIndex: 'productName',
      },
      {
        title: '原订单号',
        dataIndex: 'orderId',
        width: '10%',
        render: orderId => {
          return (
            <div style={{ textAlign: 'center' }}>
              <a
                style={{ cursor: 'pointer', color: '#3F66F5' }}
                href={`#/Order/BuyOut/Details?id=${orderId}`}
                target="_blank"
              >
                {orderId}
              </a>
            </div>
          );
        },
      },
      {
        title: '下单人姓名',
        render: e => <span>{e.userName}</span>,
      },
      {
        title: '手机号',
        render: e => <span>{e.telephone}</span>,
      },
      {
        title: '买断下单时间',
        dataIndex: 'createTime',
      },
      {
        title: '订单状态',
        dataIndex: 'state',
        render: state => <span>{statusData[state]}</span>,
      },

      {
        title: '操作',
        render: e => {
          return (
            <div style={{ textAlign: 'center' }}>
              <a
                href={`#/Order/BuyOut/BuyOutDetails?buyOutOrderId=${e.buyOutOrderId}&orderId=${e.orderId}`}
                target="_blank"
              >
                详情
              </a>
            </div>
          );
        },
      },
    ];
    const { getFieldDecorator } = this.props.form;

    return (
      <PageHeaderWrapper title={false}>
        <Card bordered={false}>
          <Form layout="inline" onSubmit={this.handleSubmit}>
            <Form.Item label="商品名称">
              {getFieldDecorator(
                'productName',
                {},
              )(<Input allowClear placeholder="请输入商品名称" />)}
            </Form.Item>
            {/* <Form.Item label="店铺名称">
              {getFieldDecorator('shopName', {})(<Input placeholder="请输入店铺名称" />)}
            </Form.Item> */}
            <Form.Item label="渠道来源">
              {getFieldDecorator(
                'channelId',
                {},
              )(
                <Select placeholder="渠道来源" style={{ width: 180 }} allowClear>
                  {PlateformList.map((item, val) => {
                    return (
                      <Option value={item.channelId} key={val}>
                        {item.channelName}
                      </Option>
                    );
                  })}
                </Select>,
              )}
            </Form.Item>
            <Form.Item label="下单人姓名">
              {getFieldDecorator(
                'userName',
                {},
              )(<Input allowClear placeholder="请输入下单人姓名" />)}
            </Form.Item>
            <Form.Item label="下单人手机号">
              {getFieldDecorator(
                'telephone',
                {},
              )(<Input allowClear placeholder="请输入下单人手机号" />)}
            </Form.Item>
            <Form.Item label="订单编号">
              {getFieldDecorator('orderId', {})(<Input allowClear placeholder="请输入订单编号" />)}
            </Form.Item>
            <Form.Item label="创建时间">
              {getFieldDecorator('createDate', {})(<RangePicker allowClear />)}
            </Form.Item>
            <Form.Item label="订单状态">
              {getFieldDecorator('status', {
                // initialValue: '03'
              })(
                <Select placeholder="订单状态" allowClear style={{ width: 180 }}>
                  <Option value="01">取消</Option>
                  {/* <Option value="02">关闭</Option> */}
                  <Option value="03">完成</Option>
                  <Option value="04">待支付</Option>
                  {/* <Option value="05">支付中</Option> */}
                </Select>,
              )}
            </Form.Item>
            <div>
              <Form.Item>
                <Button type="primary" htmlType="submit">
                  查询
                </Button>
              </Form.Item>
              <Form.Item>
                <Button htmlType="button" onClick={this.handleReset}>
                  重置
                </Button>
              </Form.Item>
              <Form.Item>
                <Button onClick={this.onexport}>导出</Button>
              </Form.Item>
            </div>
          </Form>
          <Spin spinning={loading}>
            <MyPageTable
              onPage={this.onPage}
              paginationProps={paginationProps}
              dataSource={onTableData(BuyOutList)}
              columns={columns}
            />
          </Spin>
        </Card>
      </PageHeaderWrapper>
    );
  }
}
{
  /* <Select placeholder="订单状态" style={{ width: 180 }}>
<Option value="">全部</Option>
<Option value="01">取消</Option>
<Option value="02">关闭</Option>
<Option value="03">完成</Option>
<Option value="04">待支付</Option>
<Option value="05">支付中</Option>
</Select>, */
}
