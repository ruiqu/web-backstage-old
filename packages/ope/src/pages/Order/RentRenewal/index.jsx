import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import { Card, Button, Form, Input, Select, DatePicker, Tooltip, Spin } from 'antd';
import MyPageTable from '@/components/MyPageTable';
import { onTableData, getParam } from '@/utils/utils.js';
import CopyToClipboard from '@/components/CopyToClipboard';
import moment from 'moment';
const { Option } = Select;
const { RangePicker } = DatePicker;
const { TextArea } = Input;
import { getTimeDistance, renderOrderStatus } from '@/utils/utils';
import { router } from 'umi';
import OrderService from '@/services/order';
@connect(({ order, loading }) => ({
  ...order,
  loading: loading.effects['order/queryReletOrderByCondition'],
}))
@Form.create()
export default class RentRenewal extends Component {
  state = {
    current: 1,
    yunTime: getTimeDistance('month'),
  };
  componentDidMount() {
    console.log(this.props, 'props');
    this.onList(1, 10);
    const { dispatch } = this.props;
    dispatch({
      type: 'order/PlateformChannel',
    });
  }
  onList = (pageNumber, pageSize, data = {}) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'order/queryReletOrderByCondition',
      payload: {
        pageSize,
        pageNumber,
        ...data,
      },
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      this.setState({ current: 1 });
      if (values.createDate && values.createDate.length < 1) {
        values.createTimeStart = '';
        values.createTimeEnd = '';
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      } else if (values.createDate) {
        values.createTimeStart = moment(values.createDate[0]).format('YYYY-MM-DD HH:mm:ss');
        values.createTimeEnd = moment(values.createDate[1]).format('YYYY-MM-DD HH:mm:ss');
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      } else {
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      }
    });
  };
  //table 页数
  onPage = e => {
    this.setState(
      {
        current: e.current,
      },
      () => {
        this.onList(e.current, 10, this.state.datas);
      },
    );
  };
  onexport = () => {
    const { yunTime } = this.state;
    if (this.props.form.getFieldValue('createDate')) {
      this.props.dispatch({
        type: 'order/exportOpeAllUserOrders',
        payload: {
          createTimeEnd: moment(this.props.form.getFieldValue('createDate')[1]).format(
            'YYYY-MM-DD HH:mm:ss',
          ),
          createTimeStart: moment(this.props.form.getFieldValue('createDate')[0]).format(
            'YYYY-MM-DD HH:mm:ss',
          ),
        },
      });
    } else {
      this.props.dispatch({
        type: 'order/exportOpeAllUserOrders',
        payload: {
          createTimeEnd: moment(yunTime[1]).format('YYYY-MM-DD HH:mm:ss'),
          createTimeStart: moment(yunTime[0]).format('YYYY-MM-DD HH:mm:ss'),
        },
      });
    }
  };
  // 重置
  handleReset = e => {
    this.props.form.resetFields();
    this.handleSubmit(e);
  };
  confirm = () => {};
  render() {
    const { ReletList, ReletTotal, PlateformList, loading } = this.props;
    const paginationProps = {
      current: this.state.current,
      pageSize: 10,
      total: ReletTotal,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / 10)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };

    const columns = [
      {
        title: '订单编号',
        dataIndex: 'orderId',
        width: '120px',
        render: orderId => <CopyToClipboard text={orderId} />,
      },
      {
        title: '店铺名称',
        dataIndex: 'shopName',
        width: '10%',
        render: shopName => {
          return (
            <>
              <Tooltip placement="top" title={shopName}>
                <div
                  style={{
                    width: 60,
                    /* overflow: 'hidden',
                    textOverflow: 'ellipsis',
                    whiteSpace: 'nowrap', */
                  }}
                >
                  {shopName}
                </div>
              </Tooltip>
            </>
          );
        },
      },
      {
        title: '渠道来源',
        width: '120px',
        dataIndex: 'channelName',
      },
      {
        title: '商品名称',
        dataIndex: 'productName',
        width: '200px',
      },
      {
        title: '原订单号',
        width: '120px',
        render: e => {
          return (
            <div>
              <a
                style={{ cursor: 'pointer', color: '#3F66F5' }}
                href={`#/Order/RentRenewal/Details?id=${e.originalOrderId}`}
                target="_blank"
              >
                {e.originalOrderId}
              </a>
            </div>
          );
        },
      },
      {
        title: '已付期数',
        // dataIndex: 'currentPeriods',
        width: '60px',
        render: e => {
          return (
            <>
              <div
                style={{
                  width: 60,
                  overflow: 'hidden',
                  textOverflow: 'ellipsis',
                  whiteSpace: 'nowrap',
                }}
              >
                {e.payedPeriods}/{e.totalPeriods}
              </div>
            </>
          );
        },
      },
      {
        title: '总租金',
        render: e => <span>{e.totalRentAmount}</span>,
        width: '100px',
      },
      {
        title: '已付租金',
        render: e => <span>{e.payedRentAmount}</span>,
        width: '100px',
      },
      {
        title: '下单人姓名',
        width: '100px',
        render: e => <span>{e.realName}</span>,
      },
      {
        title: '手机号',
        width: '120px',
        render: e => <span>{e.telephone}</span>,
      },
      {
        title: '起租时间',
        width: '120px',
        render: e => {
          return <>{e.rentStart}</>;
        },
      },
      {
        title: '归还时间',
        width: '120px',
        render: e => {
          return <>{e.unrentTime}</>;
        },
      },
      {
        title: '下单时间',
        dataIndex: 'placeOrderTime',
        width: '120px',
      },
      {
        title: '订单状态',
        dataIndex: 'status',
        width: 80,
        render: (_, record) => renderOrderStatus(record),
      },
      {
        title: '操作',
        width: '80px',
        fixed: 'right',
        render: e => {
          return (
            <div style={{ textAlign: 'center' }}>
              {/* <div
                style={{ cursor: 'pointer', color: '#3F66F5' }}
                onClick={() =>
                  router.push(`/Order/Details?id=${e.orderId}&RentRenewal=RentRenewal`)
                }
              >
                详情
              </div> */}
              <a
                href={`#/Order/RentRenewal/Details?id=${e.orderId}&RentRenewal=RentRenewal`}
                target="_blank"
              >
                处理
              </a>
            </div>
          );
        },
      },
    ];
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };

    return (
      <PageHeaderWrapper>
        <Card bordered={false}>
          <Form layout="inline" onSubmit={this.handleSubmit}>
            <Form.Item label="商品名称">
              {getFieldDecorator(
                'productName',
                {},
              )(<Input allowClear placeholder="请输入商品名称" />)}
            </Form.Item>
            <Form.Item label="店铺名称">
              {getFieldDecorator('shopName', {})(<Input placeholder="请输入店铺名称" />)}
            </Form.Item>
            <Form.Item label="渠道来源">
              {getFieldDecorator(
                'channelId',
                {},
              )(
                <Select placeholder="渠道来源" style={{ width: 180 }} allowClear>
                  {PlateformList.map((item, val) => {
                    return (
                      <Option value={item.channelId} key={val}>
                        {item.channelName}
                      </Option>
                    );
                  })}
                </Select>,
              )}
            </Form.Item>
            <Form.Item label="下单人姓名">
              {getFieldDecorator(
                'userName',
                {},
              )(<Input allowClear placeholder="请输入下单人姓名" />)}
            </Form.Item>
            <Form.Item label="下单人手机号">
              {getFieldDecorator(
                'telephone',
                {},
              )(<Input allowClear placeholder="请输入下单人手机号" />)}
            </Form.Item>
            <Form.Item label="订单编号">
              {getFieldDecorator('orderId', {})(<Input allowClear placeholder="请输入订单编号" />)}
            </Form.Item>
            <Form.Item label="创建时间">
              {getFieldDecorator('createDate', {})(<RangePicker allowClear />)}
            </Form.Item>
            <Form.Item label="订单状态">
              {getFieldDecorator(
                'status',
                {},
              )(
                <Select placeholder="订单状态" allowClear style={{ width: 180 }}>
                  <Option value="01">待支付</Option>
                  {/* <Option value="03">已支付申请关单</Option> */}
                  <Option value="04">待发货</Option>
                  <Option value="05">待确认收货</Option>
                  <Option value="06">租用中</Option>
                  <Option value="07">待结算</Option>
                  <Option value="08">结算待支付</Option>
                  <Option value="09">订单完成</Option>
                  <Option value="10">交易关闭</Option>
                </Select>,
              )}
            </Form.Item>
            <div>
              <Form.Item>
                <Button type="primary" htmlType="submit">
                  查询
                </Button>
              </Form.Item>
              <Form.Item>
                <Button htmlType="button" onClick={this.handleReset}>
                  重置
                </Button>
              </Form.Item>
              <Form.Item>
                <Button onClick={this.onexport}>导出</Button>
              </Form.Item>
            </div>
          </Form>
          <Spin spinning={loading}>
            <MyPageTable
              scroll
              onPage={this.onPage}
              paginationProps={paginationProps}
              dataSource={onTableData(ReletList)}
              columns={columns}
            />
          </Spin>
        </Card>
      </PageHeaderWrapper>
    );
  }
}
