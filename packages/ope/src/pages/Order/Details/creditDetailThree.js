import React, { PureComponent, Fragment } from 'react';
import './credit.less';
import styles from './creditDetail.less';

import { Card, Icon, Table, Popover, Divider, Tag } from 'antd';
import { Chart, Tooltip, Axis, Bar, Interval } from 'viser-react';
import { QuestionCircleOutlined } from '@ant-design/icons';
import { RiskReportHisttoryRent } from 'zwzshared'

//信用分详情

const popoverContent = (
  <p>
    <p>0:(0W)</p>
    <p>1:(0W~0.2W]</p>
    <p>2:(0.2W~0.5W]</p>
    <p>3:(0.5W~1W]</p>
    <p>4:(1W~3W]</p>
    <p>5:(3W~5W]</p>
    <p>6:(5W~10W]</p>
    <p>7:(10W+)</p>
  </p>
);
function gB(e) {
  if (e === -1) {
    return 0;
  } else {
    return e;
  }
}
class CreditDetail extends PureComponent {
  state = {
    creditInfo: this.props.creditInfo || {},
    userOrders: this.props.userOrders || {},
    userOrderInfoDto: this.props.userOrderInfoDto || {},
    orderAddressDto: this.props.orderAddressDto || {},
    productInfo: this.props.productInfo || {},
    userOrderCashesDto: this.props.userOrderCashesDto || {},
    timeArr: {
      '1': '[0,3)',
      '2': '[3,6)',
      '3': '[6,12)',
      '4': '[12,24)',
      '5': '[24,+)',
      '0': '查无记录',
      '-1': '不支持该运营商',
    },
    barData: [
      { year: '近7天', num: 0 },
      { year: '近1个月', num: 0 },
      { year: '近3个月', num: 0 },
      { year: '近6个月', num: 0 },
    ],
    barScale: [
      {
        dataKey: 'num',
        // tickInterval: 20,
      },
    ],
    scoreTableColumns1:
      this.props.creditInfo &&
      this.props.creditInfo.verifyResult &&
      this.props.creditInfo.verifyResult.refuse_details
        ? [
            
            {
              title: '拒绝原因',
              dataIndex: 'refuse_details',
              key: 'refuse_details',
              align: 'center',
            },
          ]
        : [
          
            {
              title: '审批建议',
              width: '100px',
              key: 'suggest',
              align: 'center',
              render: () => {
                return <div>{this.props.comprehensiveSuggest}</div>;
              },
            },
            {
              title: '审批说明',
              dataIndex: 'detail',
              key: 'detail',
              render: () => <div>{ this.returnAddvise() }</div>,
              // render: text => <div>通过：建议通过；人工：建议人工审核；拒绝：建议拒绝。</div>,
            },
          ],

    scoreTableData1: [],
    scoreTableData21: [],
    scoreTableColumns2: [
      {
        title: ' ',
        dataIndex: 'key',
        key: 'key',
        align: 'center',
      },
      {
        title: '消费金融',
        dataIndex: 'a1',
        key: 'a1',
        align: 'center',
        render: a1 => gB(a1),
      },
      {
        title: '互联网金融',
        dataIndex: 'a2',
        key: 'a2',
        align: 'center',
        render: a2 => gB(a2),
      },
      {
        title: 'P2P网贷',
        dataIndex: 'a3',
        key: 'a3',
        align: 'center',
        render: a3 => gB(a3),
      },
      {
        title: '银行个人业务',
        dataIndex: 'a4',
        key: 'a4',
        align: 'center',
        render: a4 => gB(a4),
      },
      {
        title: '信用卡',
        dataIndex: 'a5',
        key: 'a5',
        align: 'center',
        render: a5 => gB(a5),
      },
      {
        title: '网上银行',
        dataIndex: 'a6',
        key: 'a6',
        align: 'center',
        render: a6 => gB(a6),
      },

      {
        title: '银行小微贷款',
        dataIndex: 'a7',
        key: 'a7',
        align: 'center',
        render: a7 => gB(a7),
      },
      {
        title: '申请查询总数',
        dataIndex: 'a8',
        key: 'a8',
        align: 'center',
        render: a8 => gB(a8),
      },
    ],
    scoreTableColumns3: [
      {
        title: ' ',
        dataIndex: 'key',
        key: 'key',
        align: 'center',
      },
      {
        title: '注册次数',
        dataIndex: 'a1',
        key: 'a1',
        align: 'center',
        render: a1 => gB(a1),
      },
    ],
    scoreTableData2: [],
    scoreTableData3: [],
    scoreTableColumns2_5: [
      {
        title: '3个月身份证关联手机号数',
        dataIndex: 'num1',
        key: 'num1',
        align: 'center',
        render: num1 => gB(num1),
      },
      {
        title: '3个月手机号关联身份证数',
        dataIndex: 'num2',
        key: 'num2',
        align: 'center',
        render: num2 => gB(num2),
      },
    ],
    scoreTableData2_5: [],

    scoreTableColumns9: [
      // {
      //   title: '序号',
      //   dataIndex: 'index',
      //   key: 'index',
      //   align: 'center',
      // },
      {
        title: '审结日期',
        dataIndex: 'sortTimeString',
        key: 'sortTimeString',
        align: 'center',
      },
      {
        title: '类型',
        dataIndex: 'dataType',
        key: 'dataType',
        align: 'center',
      },
      {
        title: '标题',
        dataIndex: 'title',
        key: 'title',
        align: 'center',
      },
      {
        title: '匹配度',
        dataIndex: 'matchRatio',
        key: 'matchRatio',
        align: 'center',
      },
    ],
    scoreTableData9: [],
    scoreTableColumns10: [
      {
        title: ' ',
        dataIndex: 'key',
        key: 'key',
      },
      {
        title: '还款次数',
        dataIndex: 'num',
        key: 'num',
        align: 'center',
        render: num => gB(num),
      },
      {
        title: '还款平台数',
        dataIndex: 'num2',
        key: 'num2',
        align: 'center',
        render: num2 => gB(num2),
      },
      {
        title: (
          <span>
            还款金额
            <Popover content={popoverContent} title={false} trigger="click">
              <QuestionCircleOutlined style={{ marginLeft: 10 }} />
            </Popover>
          </span>
        ),
        dataIndex: 'num3',
        key: 'num3',
        align: 'center',
        render: num3 => gB(num3),
      },
    ],
    scoreTableData10: [],
    scoreTableColumns11: [
      {
        title: ' ',
        dataIndex: 'key',
        key: 'key',
      },
      {
        title: '逾期次数',
        dataIndex: 'num',
        key: 'num',
        align: 'center',
        render: num => gB(num),
      },
      {
        title: (
          <span>
            贷款逾期最大金额
            <Popover content={popoverContent} title={false} trigger="click">
              <QuestionCircleOutlined style={{ marginLeft: 10 }} />
            </Popover>
          </span>
        ),
        dataIndex: 'max',
        key: 'max',
        align: 'center',
        render: max => gB(max),
      },
    ],
    scoreTableData11: [],
    scoreTableColumns12: [
      {
        title: ' ',
        dataIndex: 'key',
        key: 'key',
      },
      {
        title: '近1个月',
        dataIndex: 'm1',
        key: 'm1',
        align: 'center',
      },
      {
        title: '近3个月',
        dataIndex: 'm3',
        key: 'm3',
        align: 'center',
      },
      {
        title: '近6个月',
        dataIndex: 'm6',
        key: 'm6',
        align: 'center',
      },
      /* {
        title: '近12个月',
        dataIndex: 'm12',
        key: 'm12',
      } */
    ],
    scoreTableData12: [],
    scoreTableColumns13: [
      {
        title: '历史租赁最高分',
        dataIndex: 'max',
        key: 'max',
        align: 'center',
      },
      {
        title: '历史租赁风险标签',
        dataIndex: 'tag',
        key: 'tag',
        align: 'center',
      },
    ],
    scoreTableData13: [],
  };

  // 返回审批建议
  returnAddvise() {
    const fengkongbaogao = this.props.creditInfo
    const yijin = this.props.comprehensiveSuggest
    if (yijin === "人工") {
      let remarkList = fengkongbaogao?.risk_remark_list || [] // 备注原因
      remarkList = Array.from(new Set(remarkList))
      if (!remarkList.length) {
        return "建议人工审核"
      } else {
        const strList = remarkList.map((str, idx) => `${idx+1}、${str}`)
        return strList.join("；")
      }
    } else if (yijin === "通过") {
      return "建议通过"
    } else {
      return "通过：建议通过；人工：建议人工审核；拒绝：建议拒绝。"
    }
  }

  componentDidMount() {
    const {
      creditInfo,
    } = this.state;

    const scoreTableData9 = [];
    if (creditInfo.interfacePersonalLawList) {
      creditInfo.interfacePersonalLawList.map((item, index) => {
        scoreTableData9.push({
          index: index + 1,
          sort_time_string: item.sort_time_string,
          data_type: item.data_type,
          summary: item.summary,
          compatibility: item.compatibility,
        });
      });
    }

    this.setState({
      scoreTableData1: [
        {
          key: 1,
          score: creditInfo.model_result && creditInfo.model_result.zwz_new_model,
          suggest: creditInfo.verifyResult && creditInfo.verifyResult.suggest,
          refuse_details: creditInfo.verifyResult && creditInfo.verifyResult.refuse_details,
        },
      ],

      scoreTableData2: [
        {
          key: '近7天',
          a1: creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.d7_apply_setup_time_xfjr_jc,
          a2: creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.d7_apply_setup_time_hlwjr_jc,
          a3: creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.d7_apply_setup_time_p2pwd_jc,
          a4: creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.d7_apply_setup_time_yhgryw_jc,
          a5: creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.d7_apply_setup_time_xyk_jc,
          a6: creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.d7_apply_setup_time_wsyh_jc,
          a7: creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.d7_apply_setup_time_yhxwdk_jc,
          a8: creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.d7_apply_setup_time_jc,
        },
        {
          key: '近30天',
          a1: creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.m1_apply_setup_time_xfjr_jc,
          a2: creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.m1_apply_setup_time_hlwjr_jc,
          a3: creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.m1_apply_setup_time_p2pwd_jc,
          a4: creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.m1_apply_setup_time_yhgryw_jc,
          a5: creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.m1_apply_setup_time_xyk_jc,
          a6: creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.m1_apply_setup_time_wsyh_jc,
          a7: creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.m1_apply_setup_time_yhxwdk_jc,
          a8: creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.m1_apply_setup_time_jc,
        },
        {
          key: '近90天',
          a1: creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.m3_apply_setup_time_xfjr_jc,
          a2: creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.m3_apply_setup_time_hlwjr_jc,
          a3: creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.m3_apply_setup_time_p2pwd_jc,
          a4: creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.m3_apply_setup_time_yhgryw_jc,
          a5: creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.m3_apply_setup_time_xyk_jc,
          a6: creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.m3_apply_setup_time_wsyh_jc,
          a7: creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.m3_apply_setup_time_yhxwdk_jc,
          a8: creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.m3_apply_setup_time_jc,
        },
      ],
      scoreTableData3: [
        {
          key: '近7天',
          a1:
            creditInfo.mode_multiple_loan_v3_0 &&
            creditInfo.mode_multiple_loan_v3_0.d7_register_times_jc,
        },
        {
          key: '近30天',
          a1:
            creditInfo.mode_multiple_loan_v3_0 &&
            creditInfo.mode_multiple_loan_v3_0.m1_register_times_jc,
        },
        {
          key: '近90天',
          a1:
            creditInfo.mode_multiple_loan_v3_0 &&
            creditInfo.mode_multiple_loan_v3_0.m3_register_times_jc,
        },
        {
          key: '近180天',
          a1:
            creditInfo.mode_multiple_loan_v3_0 &&
            creditInfo.mode_multiple_loan_v3_0.m6_register_times_jc,
        },
      ],
      scoreTableData2_5: [
        {
          key: 1,
          num1: creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.m3_idcard_to_phone_time,
          num2: creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.m3_phone_to_idcard_time_jc,
        },
      ],
      scoreTableData9,
      scoreTableData10: [
        {
          key: '近7天',
          num:
            creditInfo.mode_multiple_loan_v3_0 &&
            creditInfo.mode_multiple_loan_v3_0.d7_repay_times_jc,
          num2:
            creditInfo.mode_multiple_loan_v3_0 &&
            creditInfo.mode_multiple_loan_v3_0.d7_repay_setup_times_jc,
          num3:
            creditInfo.mode_multiple_loan_v3_0 &&
            creditInfo.mode_multiple_loan_v3_0.d7_repay_money_jc,
        },
        {
          key: '近1个月',
          num:
            creditInfo.mode_multiple_loan_v3_0 &&
            creditInfo.mode_multiple_loan_v3_0.m1_repay_times_jc,
          num2:
            creditInfo.mode_multiple_loan_v3_0 &&
            creditInfo.mode_multiple_loan_v3_0.m1_repay_setup_times_jc,
          num3:
            creditInfo.mode_multiple_loan_v3_0 &&
            creditInfo.mode_multiple_loan_v3_0.m1_repay_money_jc,
        },
        {
          key: '近3个月',
          num:
            creditInfo.mode_multiple_loan_v3_0 &&
            creditInfo.mode_multiple_loan_v3_0.m3_repay_times_jc,
          num2:
            creditInfo.mode_multiple_loan_v3_0 &&
            creditInfo.mode_multiple_loan_v3_0.m3_repay_setup_times_jc,
          num3:
            creditInfo.mode_multiple_loan_v3_0 &&
            creditInfo.mode_multiple_loan_v3_0.m3_repay_money_jc,
        },
        {
          key: '近6个月',
          num:
            creditInfo.mode_multiple_loan_v3_0 &&
            creditInfo.mode_multiple_loan_v3_0.m6_repay_times_jc,
          num2:
            creditInfo.mode_multiple_loan_v3_0 &&
            creditInfo.mode_multiple_loan_v3_0.m6_repay_setup_times_jc,
          num3:
            creditInfo.mode_multiple_loan_v3_0 &&
            creditInfo.mode_multiple_loan_v3_0.m6_repay_money_jc,
        },
      ],
      scoreTableData11: [
        {
          key: '近7天',
          num:
            creditInfo.mode_multiple_loan_v3_0 &&
            creditInfo.mode_multiple_loan_v3_0.d7_overdue_times_jc,
          max:
            creditInfo.mode_multiple_loan_v3_0 &&
            creditInfo.mode_multiple_loan_v3_0.d7_overdue_largest_money_jc,
        },
        {
          key: '近1个月',
          num:
            creditInfo.mode_multiple_loan_v3_0 &&
            creditInfo.mode_multiple_loan_v3_0.m1_overdue_times_jc,
          max:
            creditInfo.mode_multiple_loan_v3_0 &&
            creditInfo.mode_multiple_loan_v3_0.m1_overdue_largest_money_jc,
        },
        {
          key: '近3个月',
          num:
            creditInfo.mode_multiple_loan_v3_0 &&
            creditInfo.mode_multiple_loan_v3_0.m3_overdue_times_jc,
          max:
            creditInfo.mode_multiple_loan_v3_0 &&
            creditInfo.mode_multiple_loan_v3_0.m3_overdue_largest_money_jc,
        },
        {
          key: '近6个月',
          num:
            creditInfo.mode_multiple_loan_v3_0 &&
            creditInfo.mode_multiple_loan_v3_0.m6_overdue_times_jc,
          max:
            creditInfo.mode_multiple_loan_v3_0 &&
            creditInfo.mode_multiple_loan_v3_0.m6_overdue_largest_money_jc,
        },
      ],
      scoreTableData12: [
        {
          key: '租赁申请次数',
          m1:
            creditInfo.interfaceRentHistory &&
            creditInfo.interfaceRentHistory.rent_history_times_m1,
          m3:
            creditInfo.interfaceRentHistory &&
            creditInfo.interfaceRentHistory.rent_history_times_m3,
          m6:
            creditInfo.interfaceRentHistory &&
            creditInfo.interfaceRentHistory.rent_history_times_m6,
          m12:
            creditInfo.interfaceRentHistory &&
            creditInfo.interfaceRentHistory.rent_history_times_m12,
        },
      ],
      scoreTableData13: [
        {
          key: '1',
          max:
            creditInfo.interfaceRentHistory &&
            creditInfo.interfaceRentHistory.rent_history_max_grade,
          tag:
            creditInfo.interfaceRentHistory &&
            creditInfo.interfaceRentHistory.rent_history_sign &&
            creditInfo.interfaceRentHistory.rent_history_sign.length
              ? creditInfo.interfaceRentHistory.rent_history_sign.split(',')
              : '--',
        },
      ],
      barData: [
        {
          year: '近7天',
          num:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.d7_register_count,
        },
        {
          year: '近1个月',
          num:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.m1_register_count,
        },
        {
          year: '近3个月',
          num:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.m3_register_count,
        },
        {
          year: '近6个月',
          num:
            creditInfo.interfaceMultiplePlatformsV3 &&
            creditInfo.interfaceMultiplePlatformsV3.m6_register_count,
        },
      ],
    });
  }

  render() {
    const {
      creditInfo,
      userOrders,
      userOrderInfoDto,
      orderAddressDto,
      productInfo,
      userOrderCashesDto,
    } = this.state;
    const hitrotyRentObj = this.props.creditInfo?.mode_rent_history || {}

    let sm = {
      A: '用户拒付风险较低，建议服务',
      B: '用户拒付风险中等，建议结合其他信息进行判断，或者拒绝服务。',
      C: '用户拒付风险较高，建议拒绝服务',
      D: '用户支付宝信息有误，需要核实处理',
    };
    let jj = {
      A: '准入',
      B: '审核',
      C: '拒绝',
      D: '审核',
    };
    let fsm = {
      rank0: '表示信息不足或提供的参数有误。',
      rank1: '表示用户作弊风险为低或者无风险。',
      rank2: '表示用户作弊风险为中。',
      rank3: '表示用户作弊风险为高。',
    };
    let fjj = {
      rank0: '请检查入参信息。',
      rank1: '允许用户参加营销活动。',
      rank2: '建议给用户营销权益降权或拦截，或者根据客户自身数据做进一步判断。',
      rank3: '不允许用户参加营销活动。',
    };
    let fdj = {
      rank0: 'A',
      rank1: 'B',
      rank2: 'C',
      rank3: 'D',
    };
    const scoreTableCol11 = [
      {
        title: '等级',
        key: 'detail',
        render: text => <div>{this.props.nsfLevel}</div>,
      },
      {
        title: '审核建议',
        key: 'detail',
        render: text => <div>{jj[this.props.nsfLevel]}</div>,
      },
      {
        title: '说明',
        key: 'detail',
        render: text => <div>{sm[this.props.nsfLevel]}</div>,
      },
    ];
    const scoreTableCol111 = [
      {
        title: '等级',
        key: 'detail',
        render: text => <div>{fdj[this.props.antiCheatingLevel]}</div>,
      },
      {
        title: '审核建议',
        key: 'detail',
        render: text => <div>{fjj[this.props.antiCheatingLevel]}</div>,
      },
      {
        title: '说明',
        key: 'detail',
        render: text => <div>{fsm[this.props.antiCheatingLevel]}</div>,
      },
    ];
    return (
      <div className={'container ' + styles.creditDetailContainer}>
        {/* <h2 className="center">风险报告</h2> */}
        <div className={styles.containerDiv}>
          <div className={styles.titleHeader}>
            <span className={styles.titleHeaderSpan}>///</span> 审批结论{' '}
            <span className={styles.titleHeaderSpan}>///</span>
          </div>
          <Table
            bordered
            className="table-content"
            rowKey={record => record.key}
            columns={this.state.scoreTableColumns1}
            dataSource={this.state.scoreTableData1}
            pagination={false}
          />
          {this.props.nsfLevel ? (
            <div style={{ marginTop: 20 }}>
              <div className={styles.titleHeader}>
                <span className={styles.titleHeaderSpan}>///</span> 支付风险等级{' '}
                <span className={styles.titleHeaderSpan}>///</span>
              </div>
              <Table
                bordered
                className="table-content"
                rowKey={record => record.key}
                columns={scoreTableCol11}
                dataSource={this.state.scoreTableData1}
                pagination={false}
              />
            </div>
          ) : null}
          {this.props.antiCheatingLevel && this.props.antiCheatingLevel !== "paramMissingError" ? (
            <div style={{ marginTop: 20 }}>
              <div className={styles.titleHeader}>
                <span className={styles.titleHeaderSpan}>///</span> 营销反作弊{' '}
                <span className={styles.titleHeaderSpan}>///</span>
              </div>
              <Table
                bordered
                className="table-content"
                rowKey={record => record.key}
                columns={scoreTableCol111}
                dataSource={this.state.scoreTableData1}
                pagination={false}
              />
            </div>
          ) : null}
          {creditInfo.verifyResult && creditInfo.verifyResult.refuse_details ? (
            ''
          ) : (
            <Fragment>
              <div className={styles.titleHeader}>
                <span className={styles.titleHeaderSpan}>///</span> 基本信息{' '}
                <span className={styles.titleHeaderSpan}>///</span>
              </div>
              <div className={styles.floatHalf}>
                <span>
                  姓名：
                  <span>
                    {creditInfo.input_params && creditInfo.input_params.name.substr(0, 1) + '**'}
                  </span>
                </span>
                <span>
                  身份证号：
                  <span>
                    {userOrders.idCard &&
                      userOrders.idCard.substr(0, 3) +
                        '***********' +
                        userOrders.idCard.substr(14, 18)}
                  </span>
                </span>
                <span>
                  手机号：
                  <span>
                    {userOrders.telephone &&
                      userOrders.telephone.substr(0, 3) +
                        '****' +
                        userOrders.telephone.substr(7, 11)}
                  </span>
                </span>
                <span>
                  年龄：
                  <span>{userOrderInfoDto.age}</span>
                </span>
                <span>
                  性别：
                  <span>{userOrderInfoDto.gender}</span>
                </span>
                <span>
                  户籍：
                  <span>
                    {creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.ident_number_address_jc}
                  </span>
                </span>
                <span>
                  号码归属地：
                  <span>
                    {creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.phone_address_jc}
                  </span>
                </span>
                <span>
                  运营商名称：
                  <span>{creditInfo.mode_time_online && creditInfo.mode_time_online.isp_jc}</span>
                </span>
                <span>
                  在网时长：
                  <span>
                    {this.state.timeArr[
                      creditInfo.mode_time_online && creditInfo.mode_time_online.result_jc
                    ] || ''}
                  </span>
                </span>
              </div>
              <div className={styles.titleHeader}>
                <span className={styles.titleHeaderSpan}>///</span> 租赁信息{' '}
                <span className={styles.titleHeaderSpan}>///</span>
              </div>
              <div className={styles.floatHalf}>
                <span>
                  收货人：
                  <span>
                    {orderAddressDto.realname && orderAddressDto.realname.substr(0, 1) + '**'}
                  </span>
                </span>
                <span>
                  收货人手机号：
                  <span>
                    {orderAddressDto.telephone &&
                      orderAddressDto.telephone.substr(0, 3) +
                        '****' +
                        orderAddressDto.telephone.substr(7, 11)}
                  </span>
                </span>
                <span>
                  收货人地址：<span>{orderAddressDto.street}</span>
                </span>
                <span>
                  商品名称：
                  <span>{productInfo && productInfo.length && productInfo[0].productName}</span>
                </span>
                <span>
                  租赁期限(天)：<span>{userOrderInfoDto.rentDuration}</span>
                </span>
                <span>
                  下单时间：<span>{userOrderInfoDto.createTime}</span>
                </span>
                <span>
                  在途订单：<span>{userOrderInfoDto.userPayCount}</span>
                </span>
                <span>
                  完结订单：<span>{userOrderInfoDto.userFinishCount}</span>
                </span>

                <span>
                  当前逾期的订单数：
                  <span>
                    {creditInfo.mode_zuwuzu_jc && creditInfo.mode_zuwuzu_jc.overdue_counts_jc === -1 ? "- -":creditInfo.mode_zuwuzu_jc && creditInfo.mode_zuwuzu_jc.overdue_counts_jc}
                  </span>
                </span>
                <span>
                  租借订单历史最大逾期天数：
                  <span>
                    {creditInfo.mode_zuwuzu_jc && creditInfo.mode_zuwuzu_jc.overdue_days_jc}
                  </span>
                </span>
              </div>
              <div className={styles.riskTotal + ' ' + styles.marginTop0}>
                <p>冻结额度</p>
                <p>
                  <span className={styles.errorScore}>
                    {userOrderCashesDto &&
                      userOrderCashesDto.length &&
                      userOrderCashesDto[0].freezePrice}
                  </span>
                </p>
              </div>
              <div className={styles.rentInfo + ' ' + styles.rentInfo2}>
                <div>
                  <p>总租金</p>
                  <p>
                    {userOrderCashesDto &&
                      userOrderCashesDto.length &&
                      userOrderCashesDto[0].totalRent}
                  </p>
                </div>
                <div>
                  <p>月租金</p>
                  <p>
                    {userOrderCashesDto &&
                      userOrderCashesDto.length &&
                      userOrderCashesDto[0].originalRent}
                  </p>
                </div>
              </div>
              <RiskReportHisttoryRent historyRentObj={hitrotyRentObj} />
              <div className={styles.titleHeader}>
                <span className={styles.titleHeaderSpan}>///</span> 风险名单{' '}
                <span className={styles.titleHeaderSpan}>///</span>
              </div>
              <div className={styles.floatHalf}>
                <span>
                
                  {creditInfo.mode_loan_risk && creditInfo.mode_loan_risk.result_xd_jc !== 1 ? (
                    <span>特殊关注名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>特殊关注名单：命中</span>
                  )}
                </span>
                <span>
                  {creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.idcard_hit_fysx_jc !== 1 ? (
                    <span>身份证命中法院失信名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>身份证命中法院失信名单：命中</span>
                  )}
                </span>
                <span>
                  {creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.idcard_hit_fztj_jc !== 1 ? (
                    <span>身份证命中犯罪通缉名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>身份证命中犯罪通缉名单：命中</span>
                  )}
                </span>
                <span>
                  {creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.idcard_hit_fyzx_jc !== 1 ? (
                    <span>身份证命中法院执行名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>身份证命中法院执行名单：命中</span>
                  )}
                </span>
                <span>
                  {creditInfo.mode_loan_fqz &&
                  creditInfo.mode_loan_fqz.idcard_hit_zxdkqf_jc !== 1 ? (
                    <span>身份证命中助学贷款欠费名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>身份证命中助学贷款欠费名单：命中</span>
                  )}
                </span>
                <span>
                  {creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.idcard_hit_xdyq_jc !== 1 ? (
                    <span>身份证命中信贷逾期名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>身份证命中信贷逾期名单：命中</span>
                  )}
                </span>
                <span>
                  {creditInfo.mode_loan_fqz &&
                  creditInfo.mode_loan_fqz.idcard_hit_gfxgz_jc !== 1 ? (
                    <span>身份证命中高风险关注名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>身份证命中高风险关注名单：命中</span>
                  )}
                </span>
                <span>
                  {creditInfo.mode_loan_fqz &&
                  creditInfo.mode_loan_fqz.idcard_hit_clzlwy_jc !== 1 ? (
                    <span>身份证命中车辆租赁违约名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>身份证命中车辆租赁违约名单：命中</span>
                  )}
                </span>
                <span>
                  {creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.idcard_hit_fyja_jc !== 1 ? (
                    <span>身份证命中法院结案名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>身份证命中法院结案名单：命中</span>
                  )}
                </span>
                <span>
                  {creditInfo.mode_loan_fqz &&
                  creditInfo.mode_loan_fqz.idcard_hit_gywzcc_jc !== 1 ? (
                    <span>身份证命中故意违章乘车名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>身份证命中故意违章乘车名单：命中</span>
                  )}
                </span>
                <span>
                  {creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.idcard_hit_qs_jc !== 1 ? (
                    <span>身份证命中欠税名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>身份证命中欠税名单：命中</span>
                  )}
                </span>
                <span>
                  {creditInfo.mode_loan_fqz &&
                  creditInfo.mode_loan_fqz.idcard_hit_qsgsfrdb_jc !== 1 ? (
                    <span>身份证命中欠税公司法人代表名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>身份证命中欠税公司法人代表名单：命中</span>
                  )}
                </span>
                <span>
                  {creditInfo.mode_loan_fqz &&
                  creditInfo.mode_loan_fqz.idcard_hit_qkgsfrdb_jc !== 1 ? (
                    <span>身份证命中欠款公司法人代表名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>身份证命中欠款公司法人代表名单：命中</span>
                  )}
                </span>
                <span>
                  {creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.phone_hit_txxh_jc !== 1 ? (
                    <span>手机号命中通讯小号库：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>手机号命中通讯小号库：命中</span>
                  )}
                </span>
                <span>
                  {creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.phone_hit_xdyq_jc !== 1 ? (
                    <span>手机号命中信贷逾期名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>手机号命中信贷逾期名单：命中</span>
                  )}
                </span>
                <span>
                  {creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.phone_hit_xjhm_jc !== 1 ? (
                    <span>手机号命中虚拟号码库：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>手机号命中虚拟号码库：命中</span>
                  )}
                </span>
                <span>
                  {creditInfo.mode_loan_fqz &&
                  creditInfo.mode_loan_fqz.phone_hit_qkgsfrdb_jc !== 1 ? (
                    <span>手机号命中欠款公司法人代表名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>手机号命中欠款公司法人代表名单：命中</span>
                  )}
                </span>
                <span>
                  {creditInfo.mode_loan_fqz &&
                  creditInfo.mode_loan_fqz.phone_hit_clzlwy_jc !== 1 ? (
                    <span>手机号命中车辆租赁违约名单：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>手机号命中车辆租赁违约名单：命中</span>
                  )}
                </span>
                <span>
                  {creditInfo.mode_loan_fqz && creditInfo.mode_loan_fqz.phone_hit_zpss_jc !== 1 ? (
                    <span>手机号命中诈骗骚扰库：未命中</span>
                  ) : (
                    <span className={styles.errorScore}>手机号命中诈骗骚扰库：命中</span>
                  )}
                </span>
              </div>
              {/* <div className={styles.titleHeader}>
                <span className={styles.titleHeaderSpan}>///</span> 分数类{' '}
                <span className={styles.titleHeaderSpan}>///</span>
              </div>
              <div className={styles.riskTotal + ' ' + styles.marginTop0}>
                <p>贝多分</p>
                <p>
                  <span className={styles.errorScore}>
                    {creditInfo.mode_beethoven_score && creditInfo.mode_beethoven_score.score_jc}
                  </span>
                </p>
              </div> */}
              <div className={styles.titleHeader}>
                <span className={styles.titleHeaderSpan}>///</span> 关联类{' '}
                <span className={styles.titleHeaderSpan}>///</span>
              </div>
              <Table
                bordered
                className={styles.marginBottom + ' ' + styles.tableContent}
                rowKey={record => record.key}
                columns={this.state.scoreTableColumns2_5}
                dataSource={this.state.scoreTableData2_5}
                pagination={false}
              />
              <div className={styles.titleHeader}>
                <span className={styles.titleHeaderSpan}>///</span> 申请类{' '}
                <span className={styles.titleHeaderSpan}>///</span>
              </div>
              <Table
                bordered
                className={styles.tableContentFirstWhite}
                rowKey={record => record.key}
                columns={this.state.scoreTableColumns2}
                dataSource={this.state.scoreTableData2}
                pagination={false}
              />
              <div className={styles.titleHeader}>
                <span className={styles.titleHeaderSpan}>///</span> 注册类{' '}
                <span className={styles.titleHeaderSpan}>///</span>
              </div>
              <Table
                bordered
                className={styles.tableContentFirstWhite}
                rowKey={record => record.key}
                columns={this.state.scoreTableColumns3}
                dataSource={this.state.scoreTableData3}
                pagination={false}
              />
              <div className={styles.titleHeader}>
                <span className={styles.titleHeaderSpan}>///</span> 还款类{' '}
                <span className={styles.titleHeaderSpan}>///</span>
              </div>
              <Table
                bordered
                className={styles.marginBottom + ' ' + styles.tableContentFirstWhite}
                rowKey={record => record.key}
                columns={this.state.scoreTableColumns10}
                dataSource={this.state.scoreTableData10}
                pagination={false}
              />
              <div className={styles.titleHeader}>
                <span className={styles.titleHeaderSpan}>///</span> 逾期类{' '}
                <span className={styles.titleHeaderSpan}>///</span>
              </div>
              <Table
                bordered
                className={styles.marginBottom + ' ' + styles.tableContentFirstWhite}
                rowKey={record => record.key}
                columns={this.state.scoreTableColumns11}
                dataSource={this.state.scoreTableData11}
                pagination={false}
              />
              <div className={styles.titleHeader}>
                <span className={styles.titleHeaderSpan}>///</span> 法院信息{' '}
                <span className={styles.titleHeaderSpan}>///</span>
              </div>
              <Table
                bordered
                rowKey={record => record.index}
                columns={this.state.scoreTableColumns9}
                dataSource={
                  creditInfo.mode_personal_law_info_accurate &&
                  creditInfo.mode_personal_law_info_accurate.alllist
                    ? JSON.parse(creditInfo.mode_personal_law_info_accurate.alllist)
                    : null
                }
                pagination={false}
              />
            </Fragment>
          )}
        </div>
      </div>
    );
  }
}

export default CreditDetail;
