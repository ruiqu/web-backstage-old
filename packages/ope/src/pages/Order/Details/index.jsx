import React, { Component, Fragment } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import {
  Card,
  Button,
  Form,
  Input,
  Modal,
  Descriptions,
  Steps,
  Divider,
  Drawer,
  Timeline,
  Spin,
  Cascader,
  message,
  DatePicker,
  Tabs,
  Select,
  Radio,
  InputNumber,
  Popconfirm,
  Table,
  BackTop,
  Icon,
  Tooltip,
} from 'antd';
import MyPageTable from '@/components/MyPageTable';
import AntTable from '@/components/AntTable';
import { onTableData, getParam, makeSub } from '@/utils/utils.js';
import OrderService from '@/services/order';
import moment from 'moment';
import request from '../../../services/baseService';
const { TextArea } = Input;
const FormItem = Form.Item;
const Option = Select.Option;
const { Step } = Steps;
const { TabPane } = Tabs;
import CustomCard from '@/components/CustomCard';
import {
  AuditReason,
  AuditStatus,
  confirmSettlementStatus,
  confirmSettlementType,
  orderStatusMap,
  BuyOutEnum,
  defaultPlaceHolder,
} from '@/utils/enum';
import { optionsdata } from './data.js';
import { router } from 'umi';
import {
  YouhuiMoneyWithTooltip,
  TLXProRiskReport,
  BaironProRiskReport,
  zhouqikoukuanReturnLabel,
  zhouqikoukuanReturnText,
  DaiKeZhiFuBtn,
  Daikou,
} from 'zwzshared';
import SearchList from '@/components/SearchList';

const columnsByStagesStute = {
  '1': '待支付',
  '2': '已支付',
  '3': '逾期已支付',
  '4': '逾期待支付',
  '5': '已取消',
  '6': '已结算',
  '7': '已退款,可用',
};
let collstus = {
  '01': '承诺还款',
  '02': '申请延期还款',
  '03': '拒绝还款',
  '04': '电话无人接听',
  '05': '电话拒接',
  '06': '电话关机',
  '07': '电话停机',
  '08': '客户失联',
};
const BusinessCollection = [
  {
    title: '记录人',
    dataIndex: 'userName',
  },
  {
    title: '记录时间',
    dataIndex: 'createTime',
  },
  {
    title: '结果',
    dataIndex: 'result',
    render: result => collstus[result],
  },
  {
    title: '小记',
    dataIndex: 'notes',
  },
];
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};
//平台备注
const columns = [
  {
    title: '备注人姓名',
    dataIndex: 'userName',
  },
  {
    title: '备注时间',
    dataIndex: 'createTime',
  },
  {
    title: '备注内容',
    dataIndex: 'remark',
  },
];
//商家备注
const columnsBusiness = [
  {
    title: '备注人姓名',
    dataIndex: 'userName',
  },
  {
    title: '备注时间',
    dataIndex: 'createTime',
  },
  {
    title: '备注内容',
    dataIndex: 'remark',
  },
];
// 增值服务
const columnsAddService = [
  {
    title: '增值服务ID',
    dataIndex: 'shopAdditionalServicesId',
  },
  {
    title: '增值服务名称',
    dataIndex: 'shopAdditionalServicesName',
  },
  {
    title: '增值服务价格',
    dataIndex: 'price',
  },
];
//商品信息
const columnsInformation = [
  {
    title: '商品图片',
    dataIndex: 'imageUrl',
    render: imageUrl => {
      return (
        <img
          src={imageUrl}
          style={{
            width: 116,
            height: 62,
          }}
        />
      );
    },
  },
  {
    title: '商品名称',
    dataIndex: 'productName',
  },
  {
    title: '商品编号',
    dataIndex: 'productId',
    render: (text, record) => {
      return (
        <a
          className="primary-color"
          onClick={() =>
            router.push(`/goods/index/goodsDetail/id=${record.productId}/actionId=${record.id}`)
          }
        >
          {text}
        </a>
      );
    },
  },
  {
    title: '规格颜色',
    dataIndex: 'spec',
  },
  {
    title: '数量',
    dataIndex: 'num',
  },
  {
    title: '买断规则',
    dataIndex: 'buyOutSupportV1',
    render: buyOutSupport => {
      // buyOutSupport ? '可买断' : '不可买断'
      return <span>{BuyOutEnum[buyOutSupport] || '-'}</span>;
    },
  },
];

/**
 * 渲染带有优惠提示的优惠金额
 * @param {*} money : 优惠金额
 * @param {*} youhuiList : 具体优惠原因
 * @returns
 */
const renderYouhuiWithTooltip = (money, youhuiList) => {
  if (Object.prototype.toString.call(youhuiList) !== '[object Array]') return money;
  return (
    <div>
      {money}&nbsp;
      <Tooltip
        title={() => {
          return (
            <div>
              {youhuiList.map((obj, idx) => (
                <div key={idx}>
                  {obj.couponName}: {obj.discountAmount}元
                </div>
              ))}
            </div>
          );
        }}
      >
        <Icon type="question-circle" />
      </Tooltip>
    </div>
  );
};

//账单信息
const columnsBill = [
  {
    title: '总租金',
    dataIndex: 'totalRent',
  },
  {
    title: '运费',
    dataIndex: 'freightPrice',
  },
  {
    title: '平台优惠',
    dataIndex: 'platformCouponReduction',
    render: (val, row) => {
      const youhuiList = row.userOrderCouponDtos || [];
      const platformYouhuiList = youhuiList.filter(obj => {
        const str = obj.platform || '';
        return str.includes('OPE');
      });
      return <YouhuiMoneyWithTooltip money={val} youhuiList={platformYouhuiList} />;
      // return renderYouhuiWithTooltip(val, platformYouhuiList)
    },
  },
  {
    title: '店铺优惠',
    dataIndex: 'couponReduction',
    render: (val, row) => {
      const youhuiList = row.userOrderCouponDtos || [];
      const shopYouhuiList = youhuiList.filter(obj => {
        const str = obj.platform || '';
        return str.includes('SHOP');
      });
      return <YouhuiMoneyWithTooltip money={val} youhuiList={shopYouhuiList} />;
      // return renderYouhuiWithTooltip(val, shopYouhuiList)
    },
  },
];

//买断
const columnsBuyOutYes = [
  {
    title: '是否已买断',
    dataIndex: 'createTime',
    render: () => '是',
  },
  {
    title: '买断价格',
    dataIndex: 'buyOutAmount',
  },
];
const columnsBuyOutNo = [
  {
    title: '是否已买断',
    dataIndex: 'createTime',
    render: () => '否',
  },
  {
    title: '当前买断价格',
    dataIndex: 'currentBuyOutAmount',
  },
  {
    title: '到期买断价格',
    dataIndex: 'dueBuyOutAmount',
  },
];
//结算
const columnsSettlement = [
  {
    title: '宝贝状态',
    dataIndex: 'settlementType',
    render: (text, record) => {
      return confirmSettlementType[text];
    },
  },
  {
    title: '违约金',
    dataIndex: 'amount',
  },
  {
    title: '是否支付',
    dataIndex: 'settlementStatus',
    render: (text, record) => {
      return confirmSettlementStatus[text];
    },
  },
];
const paginationProps = {
  current: 1,
  pageSize: 1000,
  total: 1,
};

@connect(({ order, loading, RentRenewalLoading }) => ({
  ...order,
  loading: loading.effects['order/queryOpeUserOrderDetail'],
  RentRenewalLoading: loading.effects['order/queryUserReletOrderDetail'],
}))
@Form.create()
export default class Details extends Component {
  state = {
    drawerVisible: false,
    drawerData: [],
    visible: false,
    visibles: false,
    visibles2: false,
    visibles3: false,
    visiblesimg: false,
    settlement: null,
    baironRisk: null, // 百融风控报告
    baironRisk1: null, // 百融风控报告
    xcurrent: 1,
    scurrent: 1,
    titles: '',
    subLists: [],
    creditInfo: null, // 风控报告结果
    img: '',
    comprehensiveSuggest: '',
    deliverOptions: [],
    auditRecord: {},
    processDetail: [],

    orderVisible: '',
    orderId: '',
    radioValue: '',
    damageValue: '',

    queryOrderStagesDetail: {},

    activeTab: '1',
    creditEmpty: false,

    HastenList: [],
    HastenTotal: 1,
    HastenCurrent: 1,

    opeHastenList: [],
    opeHastenTotal: 1,
    opeHastenCurrent: 1,
    antiCheatingLevel: '',

    // 确认收货弹窗
    takeOverModal: false,

    // 收货日期
    takeOverDate: '',

    userOrderCashesDtoBusiness: {},

    reportType: '',

    //押金信息
    depositData: {},
    //修改押金弹框
    depositModal: false,
    riskErrData: {}, // 风险反欺诈信息接口所返回的数据；可能会与原接口返回的内容存在不一致，经过了部分加工
    loadingRiskErrData: false, // 是否正在加载风险反欺诈信息数据中
    showRiskReportCallBtn: false, // 是否显示调用风控报告的按钮
    showRiskReportBaironBtn: false, // 是否显示调用风控报告的按钮
    orderDetailApiRes: {}, // 订单详情接口queryOpeUserOrderDetail所返回的数据
    transfer: [], // 转单商家列表
    transferModal: false, // 是否显示转单模态框
    list: [], // 转单记录列表数据
    total: 0, // 转单记录列表数据总数
    current: 1, // 转单记录当前页码
    loadingTransfer: false, // 是否正在加载转单记录列表数据
  };

  componentDidMount() {
    request('/hzsx/opeShop/listAllShop', {}, 'get').then(res => {
      this.setState({ transfer: res });
    });
    const orderId = getParam('id');
    this.setState({
      orderId,
    });
    this.onRentRenewalDetail();
    this.onPageBusiness({ current: 1 });
    this.onPage({ current: 1 });
    if (getParam('settlement')) {
      this.onHasten(1, 3, '02');
      this.onHasten(1, 3, '01');
    }

    this.fetchFenQiData()

    //押金信息
    OrderService.queryPayDepositLog({ orderId }).then(res => {
      if (res) {
        this.setState({
          depositData: res,
        });
      }
    });
  }

  // 进行转单的时候触发
  getTransferModal = e => {
    const orderId = getParam('id');
    e.preventDefault();
    this.props.form.validateFields(['transferShopId', 'remark'], (err, values) => {
      if (!err) {
        OrderService.transferOrder({
          orderId,
          transferShopId: values.transferShopId,
          remark: values.remark,
        }).then(res => {
          message.success('转单成功');
          router.push('/Order/HomePage');
        });
      }
    });
  };

  // 加载转单记录数据
  handleFilter = (pageNumber, pageSize) => {
    this.setState(
      {
        loadingTransfer: true,
      },
      () => {
        request(
          '/hzsx/ope/order/queryTransferOrderRecordPage',
          { pageNumber, pageSize, orderId: getParam('id') },
          'post',
        ).then(res => {
          if (res) {
            this.setState({
              list: res.records,
              total: res.total,
              loadingTransfer: false,
              current: res.current,
            });
          }
        });
      },
    );
  };

  //转单记录
  renderTransferTab() {
    const { loadingTransfer = false, list, total = 1 } = this.state;
    const columnsTransfer = [
      {
        title: '原商品ID',
        dataIndex: 'transferredProductId',
      },
      {
        title: '新商品ID',
        dataIndex: 'transferProductId',
      },
      {
        title: '转出商家',
        dataIndex: 'transferredShopName',
      },
      {
        title: '转入商家',
        dataIndex: 'transferShopName',
      },
      {
        title: '备注信息',
        dataIndex: 'remark',
      },
    ];
    return (
      <SearchList
        handleFilter={this.handleFilter}
        columns={columnsTransfer}
        searchHander={[]}
        loading={loadingTransfer}
        total={total}
        list={list}
        pagination={true}
      ></SearchList>
    );
  }

  /**
   * 加载分期账单列表数据
   */
  fetchFenQiData = () => {
    this.props.dispatch({
      type: 'order/queryOrderStagesDetail',
      payload: {
        orderId: getParam('id'),
      },
      callback: res => {
        if (res.responseType === 'SUCCESS') {
          this.setState({
            queryOrderStagesDetail: res.data,
          });
        }
      },
    });
  }

  //分期
  columnsByStages = [
    // {
    //   title: '总期数',
    //   dataIndex: 'totalPeriods',
    // },
    {
      title: '当前期数',
      dataIndex: 'currentPeriods',
    },
    {
      title: '租金',
      dataIndex: 'currentPeriodsRent',
    },
    {
      title: '状态',
      dataIndex: 'status',
      render: status => <span>{columnsByStagesStute[status]}</span>,
    },
    {
      title: '支付时间',
      dataIndex: 'repaymentDate',
    },
    {
      title: '账单到期时间',
      dataIndex: 'statementDate',
    },
    {
      title: '操作',
      render: (_, record) => {
        return (
          <>
            <Daikou periodItem={record} refetchCb={this.fetchFenQiData} orderInfoObj={this.props.userOrderInfoDto} apiRes={this.state.queryOrderStagesDetail} />
            <DaiKeZhiFuBtn periodItem={record} refetchCb={this.fetchFenQiData} orderInfo={this.props.userOrderInfoDto} />
          </>
        )
      }
    }
  ];

  fetchProcessDetail() {
    const orderId = getParam('id');
    OrderService.queryOrderStatusTransfer({
      orderId,
    }).then(res => {
      this.setState({
        processDetail: res || [],
      });
    });
  }
  renderCollectionRecordModal() {
    const { orderVisible } = this.state;
    const { getFieldDecorator } = this.props.form;
    const orderId = getParam('id');

    const handleOk = e => {
      e.preventDefault();
      this.props.form.validateFields(['jg', 'xj'], (err, values) => {
        if (!err) {
          OrderService.orderHasten({ orderId, notes: values.xj, result: values.jg }).then(res => {
            this.onHasten(1, 3, '01');
            this.handleCancel();
          });
        }
      });
    };

    return (
      <Modal
        title="记录催收"
        visible={orderVisible === 'remarks'}
        onOk={handleOk}
        onCancel={this.handleCancel}
        destroyOnClose
      >
        <Form>
          <Form.Item label="结果" {...formItemLayout}>
            {getFieldDecorator('jg', {
              rules: [{ required: true, message: '请选择结果' }],
            })(
              <Select style={{ width: '100%' }} placeholder="请选择结果">
                <Option value="01">承诺还款</Option>
                <Option value="02">申请延期还款</Option>
                <Option value="03">拒绝还款</Option>
                <Option value="04">电话无人接听</Option>
                <Option value="05">电话拒接</Option>
                <Option value="06">电话关机</Option>
                <Option value="07">电话停机</Option>
                <Option value="08">客户失联</Option>
              </Select>,
            )}
          </Form.Item>
          <Form.Item label="小记" {...formItemLayout}>
            {getFieldDecorator('xj', {
              rules: [{ required: true, message: '请输入小记' }],
            })(<TextArea placeholder="请输入" />)}
          </Form.Item>
        </Form>
      </Modal>
    );
  }
  onRentRenewalDetail = () => {
    const { dispatch } = this.props;
    if (getParam('RentRenewal')) {
      dispatch({
        type: 'order/queryUserReletOrderDetail',
        payload: {
          orderId: getParam('id'),
        },
        callback: e => {
          const eData = e?.data?.userOrderCashesDto || {};
          this.setState({
            userOrderCashesDtoBusiness: eData,
            orderDetailApiRes: e.data,
          });
        },
      });
    } else {
      dispatch({
        type: 'order/queryOpeUserOrderDetail',
        payload: {
          orderId: getParam('id'),
        },
        callback: e => {
          this.setState({
            orderDetailApiRes: e.data,
            userOrderCashesDtoBusiness: e.data.userOrderCashesDto,
          });
        },
      });
    }
  };
  onQueryOrderRemark = (pageNumber, pageSize, source) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'order/queryOrderRemark',
      payload: {
        orderId: getParam('id'),
        pageNumber,
        pageSize,
        source,
      },
    });
  };
  //新建备注翻页
  onPage = (e = { current: 1 }) => {
    this.setState(
      {
        xcurrent: e.current,
      },
      () => {
        this.onQueryOrderRemark(e.current, 3, '01');
      },
    );
  };
  //商机备注翻页
  onPageBusiness = (e = { current: 1 }) => {
    this.setState(
      {
        scurrent: e.current,
      },
      () => {
        this.onQueryOrderRemark(e.current, 3, '02');
      },
    );
  };
  //物流信息
  onClose = () => {
    this.setState({
      drawerVisible: false,
    });
  };
  //查看物流
  onLogistics = (e, i) => {
    this.setState(
      {
        drawerVisible: true,
        drawerTitle: e,
      },
      () => {
        this.onQueryExpressInfo(i);
      },
    );
  };
  handleOk = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { dispatch, userOrderInfoDto } = this.props;
        if (this.state.titles === '备注') {
          if (getParam('RentRenewal')) {
            dispatch({
              type: 'order/telephoneAuditOrder',
              payload: {
                orderId: getParam('id'),
                remark: values.beizhu,
              },
              callback: res => {
                this.onQueryOrderRemark(1, 3, '01');
                this.onRentRenewalDetail();
                this.setState({
                  visible: false,
                });
              },
            });
          } else {
            dispatch({
              type: 'order/orderRemark',
              payload: {
                orderId: getParam('id'),
                remark: values.beizhu,
                orderType: userOrderInfoDto.type,
              },
              callback: res => {
                this.onQueryOrderRemark(1, 3, '01');
                this.onRentRenewalDetail();
                this.setState({
                  visible: false,
                });
              },
            });
          }
        } else {
          dispatch({
            type: 'order/opeOrderAddressModify',
            payload: {
              realName: values.realName,
              street: values.street,
              telephone: values.telephone,
              province: values && values.city && values.city[0],
              city: values && values.city && values.city[1],
              area: values && values.city && values.city[2],
              orderId: getParam('id'),
            },
            callback: res => {
              this.onRentRenewalDetail();
              this.setState({
                visible: false,
              });
            },
          });
        }
      }
    });
  };
  onQueryExpressInfo = i => {
    const { dispatch } = this.props;
    dispatch({
      type: 'order/queryExpressInfo',
      payload: {
        expressNo: i.expressNo,
        receiverPhone: i.receiverPhone,
        shortName: i.shortName,
      },
    });
  };

  // 确认收货
  takeOverSure() {
    const { dispatch } = this.props;

    dispatch({
      type: 'order/forceConfirmReceipt',
      payload: {
        confirmDate: this.state.takeOverDate,
        orderId: this.state.orderId,
      },
      callback: res => {
        if (res.responseType === 'SUCCESS') {
          this.onRentRenewalDetail();

          this.setState({
            takeOverModal: false,
          });
        }
      },
    });
  }

  getAuditRecord = () => {
    OrderService.queryOrderAuditRecord({
      orderId: this.state.orderId,
    }).then(res => {
      this.setState({
        auditRecord: res || [],
      });
    });
  };

  handleCancel = e => {
    this.setState({
      visible: false,
      orderVisible: false,
    });
  };
  handleCancels = e => {
    this.setState({
      visibles: false,
      visibles2: false,
      visibles3: false,
      visiblesimg: false,
    });
  };
  onKaiImg = e => {
    this.setState({
      img: e,
      visiblesimg: true,
    });
  };
  showModal = e => {
    this.setState({
      visible: true,
      titles: e,
    });
  };

  settleDia = (type, orderId, userOrderCashId, record) => {
    const { userOrderCashesDto, userViolationRecords } = this.props;
    if (userViolationRecords && userViolationRecords[0]) {
      let radioValue = 'good';
      radioValue = userOrderCashesDto.damagePrice ? 'damage' : radioValue;
      radioValue = userOrderCashesDto.lostPrice ? 'lose' : radioValue;
      this.setState({
        checkValue: true,
        radioValue,
        damageValue: userOrderCashesDto.damagePrice || userOrderCashesDto.lostPrice,
      });
    }
    this.setState({
      orderId,
      userOrderCashId,
      record,
      orderVisible: 'settle',
    });
  };

  showOrderModal = (type, flag) => {
    if (type === 'settle') {
      this.settleDia();
    }
    if (type === 'audit') {
      this.setState({
        titles: flag ? '审批通过' : '审批拒绝',
      });
    }
    this.setState({
      orderVisible: type,
    });
  };
  onXY = () => {
    location.href = this.props.contractUrl;
  };

  // 获取天狼星旗舰版报告数据
  viewCredit = () => {
    const uoiObj = this.props.userOrderInfoDto || {};
    const uid = uoiObj.uid;
    const phone = uoiObj.telephone;
    const idCardNo = uoiObj.idCard;
    const userName = uoiObj.realName;

    this.props.dispatch({
      type: 'order/getCredit',
      payload: {
        uid,
        orderId: getParam('id'),
        phone,
        idCardNo,
        userName,
      },

      callback: res => {
        if (res.responseType === 'SUCCESS') {
          const resultStr = (res.data && res.data.siriusRiskReport) || ''; // 风控报告结果字符串
          const str2 = resultStr.replaceAll('"', '"');
          const finalResult = JSON.parse(str2);
          this.setState({ creditInfo: finalResult, showRiskReportCallBtn: false });
        } else {
          message.error(res.errorMessage);
        }
      },
    });
  };

  // 加载百融报告数据
  getbairon = () => {
    const uoiObj = this.props.userOrderInfoDto || {}
    const uid = uoiObj.uid
    const phone = uoiObj.telephone
    const idCardNo = uoiObj.idCard
    const userName = uoiObj.realName
    this.props.dispatch({
      type: 'order/getbairon',
      payload: {
        uid,
        orderId: getParam('id'),
        phone,
        idCardNo,
        userName,
      },
      callback: res => {
        if (res.responseType === 'SUCCESS') {
          const resultStr = (res.data && res.data.resultJson) || "" // 风控报告结果字符串
          //console.log("获取百度风控报告内容",resultStr)
          const str2 = resultStr.replaceAll('\"', '"')
          const finalResult = JSON.parse(str2)
          console.log("获取百融风控报告内容",finalResult)
            this.setState({ baironRisk: finalResult, showRiskReportBaironBtn: false })
        }
      },
    });
  };
  // 加载百融报告数据
  getbairon1 = () => {
    const uoiObj = this.props.userOrderInfoDto || {}
    const uid = uoiObj.uid
    const phone = uoiObj.telephone
    const idCardNo = uoiObj.idCard
    const userName = uoiObj.realName
    this.props.dispatch({
      type: 'order/getbairon',
      payload: {
        uid,
        type:1,
        orderId: getParam('id'),
        phone,
        idCardNo,
        userName,
      },
      callback: res => {
        if (res.responseType === 'SUCCESS') {
          const resultStr = (res.data && res.data.resultJson) || "" // 风控报告结果字符串
          //console.log("获取百度风控报告内容",resultStr)
          const str2 = resultStr.replaceAll('\"', '"')
          const finalResult = JSON.parse(str2)
          console.log("获取百融风控报告内容",finalResult) 
          this.setState({ baironRisk1: finalResult, showRiskReportBaironBtn1: false })
        }
      },
    });
  };

  tabChange = key => {
    if (key === '3') {
      this.getAuditRecord();
    }
    if (key === '6') {
      this.fetchProcessDetail();
    }
    if (key === '2') {
      // 表明切换到了风险报告这一栏
      this.setState({ showRiskReportCallBtn: true }); // FIXME:
    }
    if (key === '12') {
      this.setState({ showRiskReportBaironBtn: true })
    }
    if (key === '13') {
      this.setState({ showRiskReportBaironBtn1: true })
    }
    this.setState({
      activeTab: key,
    });
  };

  /**
   * 下载预审单的处理方法
   */
  downloadYuShenDanHandler = oId => {
    if (!oId) return; // 订单ID必传
    if (this.loading) return; // 点击中也不处理
    this.loading = true;
    const url = `/hzsx/export/prequalificationSheet?orderId=${oId}`;
    request(url, {}, 'get')
      .then(() => {
        message.success('导出任务创建成功，请前往“数据管理-导出数据下载”完成下载。');
      })
      .finally(() => {
        this.loading = false;
      });
  };

  renderOrderInfoTab() {
    const {
      userOrderInfoDto = {},
      orderAdditionalServicesDto = {},
      orderAddressDto,
      orderLocationAddress = {},
      opeRemarkDtoPage,
      businessRemarkDtoPage,
      productInfo,
      rentStart,
      rentDuration,
      unrentTime,
      shopInfoDto = {},
    } = this.props;

    const zengzhiList = this.props.orderAdditionalServicesList || [];

    const orderId = getParam('id');
    return (
      <div>
        <Descriptions>
          <Descriptions.Item label="订单号" span={3}>
            {orderId}
          </Descriptions.Item>
          {this.props.contractUrl ? (
            <Descriptions.Item label="用户租赁协议" span={3}>
              <a onClick={this.onXY}>《租赁协议》</a>
            </Descriptions.Item>
          ) : null}
          <Descriptions.Item label="" span={3}>
            <a className="blackClickableA" onClick={() => this.downloadYuShenDanHandler(orderId)}>
              预审单下载
            </a>
          </Descriptions.Item>
        </Descriptions>

        {orderAddressDto ? (
          <>
            <Descriptions
              title={
                <>
                  <CustomCard title="收货人信息" />
                </>
              }
            >
              <Descriptions.Item label="收货人姓名">
                {orderAddressDto && orderAddressDto.realname}
              </Descriptions.Item>
              <Descriptions.Item label="收货人手机号">
                {orderAddressDto && orderAddressDto.telephone}
              </Descriptions.Item>
              <Descriptions.Item label="收货人地址">
                {orderAddressDto && orderAddressDto.provinceStr}
                {orderAddressDto && orderAddressDto.cityStr}
                {orderAddressDto && orderAddressDto.areaStr}
                {orderAddressDto && orderAddressDto.street}
              </Descriptions.Item>
              <Descriptions.Item label="用户备注">{userOrderInfoDto.remark}</Descriptions.Item>
            </Descriptions>
            <Divider />
          </>
        ) : null}

        <Descriptions title={<CustomCard title="商家信息" />}>
          <Descriptions.Item label="商家名称">{shopInfoDto.shopName}</Descriptions.Item>
          <Descriptions.Item label="商家电话" span={2}>
            {shopInfoDto.telephone}
          </Descriptions.Item>
        </Descriptions>
        <Divider />

        <CustomCard title="商品信息" style={{ marginBottom: 20 }} />
        <AntTable
          columns={columnsInformation}
          dataSource={onTableData(productInfo)}
          paginationProps={paginationProps}
        />

        <Descriptions title={<CustomCard title="租用信息" />}>
          <Descriptions.Item label="租用天数">{rentDuration}</Descriptions.Item>
          <Descriptions.Item label="起租时间">{rentStart}</Descriptions.Item>
          <Descriptions.Item label="归还时间">{unrentTime}</Descriptions.Item>
        </Descriptions>
        <Divider />

        <CustomCard title="增值服务" style={{ marginBottom: 20 }} />
        <AntTable
          columns={columnsAddService}
          dataSource={zengzhiList}
          // dataSource={onTableData([orderAdditionalServicesDto])}
          paginationProps={paginationProps}
        />

        <CustomCard title="商家备注" style={{ marginBottom: 20 }} />
        <MyPageTable
          onPage={this.onPageBusiness}
          paginationProps={{
            current: this.state.scurrent,
            pageSize: 3,
            total: businessRemarkDtoPage.total,
          }}
          dataSource={onTableData(businessRemarkDtoPage.records)}
          columns={columnsBusiness}
        />
        <Divider />
        <CustomCard title="平台备注" />
        <MyPageTable
          onPage={this.onPage}
          paginationProps={{
            current: this.state.xcurrent,
            pageSize: 3,
            total: opeRemarkDtoPage.total,
          }}
          dataSource={onTableData(opeRemarkDtoPage.records)}
          columns={columns}
        />
      </div>
    );
  }

  // 渲染风控报告结果
  renderRiskTab() {
    const riskApiRes = this.state.creditInfo;
    if (!riskApiRes) return null;
    if (riskApiRes.resp_code === 'SW0000') {
      const obj = riskApiRes.resp_data || {};
      return <TLXProRiskReport riskReport={obj} />;
    } else {
      return <div style={{ marginTop: 15 }}>获取风险报告出现问题</div>;
    }
  }

  // 渲染风控报告数据
  renderRiskbaironTab() {
    const riskApiRes = this.state.baironRisk
    if (!riskApiRes) return null
    if (riskApiRes.code === "00") {
      const obj = riskApiRes || {}
      return (
        <BaironProRiskReport riskReport={obj} />
      )
    } else {
      return <div style={{ marginTop: 15 }}>获取风险报告出现问题</div>
    }
  }
  // 渲染风控报告数据，三要素
  renderRiskbaironTab1() {
    const riskApiRes = this.state.baironRisk1
    if (!riskApiRes) return null
    if (riskApiRes.code === "00") {
      const obj = riskApiRes || {}
      return (
        <div className="booleandataTotalContainer">
        <div className="booleanDataWrap">
          <div>
          <Divider name="手机三要素" />
            <div className="riskDetection">
              <Descriptions column={4}>
                <Descriptions.Item label="运营商">
                {riskApiRes.TelCheck_s.operation =="1" ? '电信' :riskApiRes.TelCheck_s.operation =="2" ?'联通':riskApiRes.TelCheck_s.operation =="3" ?'移动':'其它'}
                </Descriptions.Item>
                
                <Descriptions.Item label="与身份信息是否一致">
                {riskApiRes.TelCheck_s.result =="1" ? '一致' :riskApiRes.TelCheck_s.result =="2" ?'不一致':'查无此号'}
                </Descriptions.Item>

                <Descriptions.Item label="在网时长">
                {riskApiRes.TelPeriod.data.value=="1"?'0-6':riskApiRes.TelPeriod.data.value=="2"?'6-12':riskApiRes.TelPeriod.data.value=="3"?'12-24':'24+'}
                个月
                </Descriptions.Item>
                <Descriptions.Item label="在网状态">
                {riskApiRes.TelStatus.data.value=="1"?'正常':riskApiRes.TelPeriod.data.value=="2"?'停机':riskApiRes.TelPeriod.data.value=="3"?'销号':'异常'}
                </Descriptions.Item>
              </Descriptions>
            </div>
          </div>
        </div>
      </div>
      )
    } else {
      return <div style={{ marginTop: 15 }}>获取风险报告出现问题</div>
    }
  }

  renderAuditTab() {
    const { auditRecord = {} } = this.state;

    return (
      <div>
        <Descriptions>
          <Descriptions.Item label="审批时间" span={3}>
            {auditRecord.approveTime}
          </Descriptions.Item>
          <Descriptions.Item label="审批人" span={3}>
            {auditRecord.approveUserName}
          </Descriptions.Item>
          <Descriptions.Item label="审批结果" span={3}>
            {AuditStatus[auditRecord.approveStatus]}
          </Descriptions.Item>
          {auditRecord.approveStatus === '02' && (
            <Descriptions.Item label="拒绝类型" span={3}>
              {AuditReason[auditRecord.refuseType]}
            </Descriptions.Item>
          )}
          <Descriptions.Item label="小记" span={3}>
            {auditRecord.remark}
          </Descriptions.Item>
        </Descriptions>
      </div>
    );
  }
  //押金管理
  renderDeposit() {
    const { depositData } = this.state;

    let sm = {
      rank0: '提供信息不足，提供参数信息有误，或提供的支付宝账号不存在。',
      rank1: '表示用户拒付风险为低。',
      rank2: '表示用户拒付风险为中。',
      rank3: '表示用户拒付风险为高。',
    };
    let jj = {
      rank0: '等级0',
      rank1: '等级1',
      rank2: '等级2',
      rank3: '等级3',
    };
    const scoreTableCol11 = [
      {
        title: '风险评级',
        key: 'detail',
        render: text => <div>{this.props.nsfLevel}</div>,
      },
      {
        title: '风险描述',
        key: 'detail',
        render: text => <div>{jj[this.props.nsfLevel]}</div>,
      },
      {
        title: '评级备注',
        key: 'detail',
        render: text => <div>{sm[this.props.nsfLevel]}</div>,
      },
    ];
    const yajinZhifuList = []; // 押金支付列表数据
    const { amount, creditAmount, paidAmount, waitPayAmount } = depositData || {};
    if (
      amount != undefined ||
      creditAmount != undefined ||
      paidAmount != undefined ||
      waitPayAmount != undefined
    ) {
      // 只要有任意一者存在有效值即进行展示
      const item = { key: 1, amount, creditAmount, paidAmount, waitPayAmount };
      yajinZhifuList.push(item);
    }

    let columns = [
      {
        title: '押金总额',
        dataIndex: 'amount',
      },
      {
        title: '已支付押金',
        dataIndex: 'paidAmount',
      },
      {
        title: '待支付押金',
        dataIndex: 'waitPayAmount',
      },
    ];
    let columnsLogs = [
      {
        title: '押金总额',
        dataIndex: 'afterAmount',
      },
      {
        title: '修改时间',
        dataIndex: 'createTime',
      },
      {
        title: '修改人',
        dataIndex: 'backstageUserName',
      },
    ];
    //芝麻额度冻结信息
    const columnsZm = [
      {
        title: '冻结额度',
        dataIndex: 'freezePrice',
      },
      {
        title: '信用减免',
        dataIndex: 'creditDeposit',
      },
      {
        title: '实际冻结',
        dataIndex: '', // todo
        render: (text, record) => {
          return makeSub(record.freezePrice, record.creditDeposit);
        },
      },
    ];
    return (
      <>
        <CustomCard title="芝麻额度冻结" style={{ marginBottom: 20 }} />
        <AntTable
          columns={columnsZm}
          dataSource={onTableData([this.state.userOrderCashesDtoBusiness])}
          paginationProps={paginationProps}
        />
       {
this.props.nsfLevel&& <>
<CustomCard title="先享后付评级" style={{ marginBottom: 20 }} />
        <Table columns={scoreTableCol11} dataSource={onTableData([{}])} pagination={false} /></>
        }
        <CustomCard title="押金支付" style={{ marginBottom: 20 }} />
        <AntTable columns={columns} dataSource={yajinZhifuList} paginationProps={paginationProps} />
        <CustomCard title="修改记录" style={{ marginBottom: 20 }} />
        <AntTable
          columns={columnsLogs}
          dataSource={onTableData(depositData && depositData.logs)}
          paginationProps={paginationProps}
        />
      </>
    );
  }
  renderExpressTab() {
    const { receiptExpressInfo, giveBackExpressInfo } = this.props;
    return (
      <div>
        {giveBackExpressInfo ? (
          <>
            <Descriptions title={<CustomCard title="归还物流信息" />}>
              <Descriptions.Item label="发货物流公司">
                {giveBackExpressInfo.expressCompany}
              </Descriptions.Item>
              <Descriptions.Item label="发货物流单号">
                {giveBackExpressInfo.expressNo}
              </Descriptions.Item>
              <Descriptions.Item label="归还时间">
                {giveBackExpressInfo.deliveryTime}
              </Descriptions.Item>
            </Descriptions>
            <Button onClick={() => this.onLogistics('归还物流信息', giveBackExpressInfo)}>
              查看物流
            </Button>
            <Divider />
          </>
        ) : null}
        {receiptExpressInfo ? (
          <>
            <Descriptions title={<CustomCard title="发货物流信息" />}>
              <Descriptions.Item label="发货物流公司">
                {receiptExpressInfo.expressCompany}
              </Descriptions.Item>
              <Descriptions.Item label="发货物流单号">
                {receiptExpressInfo.expressNo}
              </Descriptions.Item>
              <Descriptions.Item label="发货时间">
                {receiptExpressInfo.deliveryTime}
              </Descriptions.Item>
            </Descriptions>
            <Button onClick={() => this.onLogistics('发货物流信息', receiptExpressInfo)}>
              查看物流
            </Button>
            <Divider />
          </>
        ) : null}
      </div>
    );
  }

  renderTitle = () => {
    const { orderByStagesDtoList } = this.state.queryOrderStagesDetail
    return `分期信息-共${orderByStagesDtoList && orderByStagesDtoList.length}期`
  }

  renderBillTab() {
    const {
      orderByStagesDtoList = [],
      orderBuyOutDto = {},
      settlementInfoDto = {},
    } = this.state.queryOrderStagesDetail;
    const productInfoList = this.props.productInfo || [];
    const productInfoObj = productInfoList[0] || {};
    return (
      <Card bordered={false}>
        {settlementInfoDto ? (
          <>
            <CustomCard title="结算信息" style={{ marginBottom: 20 }} />
            <AntTable
              columns={columnsSettlement}
              dataSource={onTableData([settlementInfoDto])}
              paginationProps={paginationProps}
            />
          </>
        ) : null}
        <CustomCard title="账单信息(金额单位：元)" style={{ marginBottom: 20 }} />
        <AntTable
          columns={columnsBill}
          dataSource={onTableData([this.state.userOrderCashesDtoBusiness])}
          paginationProps={paginationProps}
        />
        <CustomCard title={this.renderTitle()} style={{ marginBottom: 20 }} />
        <AntTable
          isLimitHeightTable={true}
          columns={this.columnsByStages}
          dataSource={onTableData(orderByStagesDtoList)}
          paginationProps={paginationProps}
        />
        {orderBuyOutDto.orderId ? (
          <>
            <CustomCard title="买断信息" style={{ marginBottom: 20 }} />
            <AntTable
              columns={orderBuyOutDto.payFlag ? columnsBuyOutYes : columnsBuyOutNo}
              dataSource={onTableData([orderBuyOutDto])}
              paginationProps={paginationProps}
            />
          </>
        ) : null}

        <div style={{ marginBottom: '20px' }}>
          <Descriptions
            title={
              <>
                <CustomCard title="交易快照(金额单位：元)" />
              </>
            }
          >
            <Descriptions.Item label="销售价">
              {productInfoObj.salePrice || defaultPlaceHolder}
            </Descriptions.Item>
          </Descriptions>
        </div>
      </Card>
    );
  }

  renderProcessTab() {
    const { processDetail = [] } = this.state;
    return (
      <Card
        className="remove-card-bottom-border"
        title={<CustomCard title="订单进度" />}
        bordered={false}
      >
        <Steps
          direction="vertical"
          progressDot={document.body.clientWidth >= 1025}
          current={processDetail.length}
        >
          {processDetail.map(item => {
            const desc = (
              <div style={{ width: 300 }}>
                <div>{item.operatorName}</div>
                <div>{item.createTime}</div>
              </div>
            );
            return <Step title={item.operate} key={item.operate} description={desc} />;
          })}
        </Steps>
      </Card>
    );
  }

  renderContentTabCard() {
    const { userOrderInfoDto } = this.props;
    const { activeTab } = this.state;
    const status = userOrderInfoDto.status;

    return (
      <Card bordered={false} style={{ marginTop: 20 }}>
        <Tabs activeKey={activeTab} onChange={this.tabChange} animated={false}>
          <TabPane tab="订单信息" key="1">
            {this.renderOrderInfoTab()}
          </TabPane>
          {!['02', '01'].includes(status) ? (
            <TabPane tab="风控报告" key="2">
              <>
                {this.state.showRiskReportCallBtn && (
                  <Button onClick={this.viewCredit} type="primary">
                    点击查询
                  </Button>
                )}
                {this.renderRiskTab()}
              </>
            </TabPane>
          ) : null}

     {/*  {!['02', '01'].includes(status) ? (
            <TabPane tab="百融三要素(测试)" key="13">
              <>
                {
                  this.state.showRiskReportBaironBtn1 && (
                    <Button onClick={this.getbairon1} type="primary">点击查询</Button>
                  )
                }
                { this.renderRiskbaironTab1() }
              </>
            </TabPane>
          ) : null} */}

          {/* {!['02', '01'].includes(status) ? (
            <TabPane tab="百融报告" key="12">
              <>
                {
                  this.state.showRiskReportBaironBtn && (
                    <Button onClick={this.getbairon} type="primary">点击查询</Button>
                  )
                }
                { this.renderRiskbaironTab() }
              </>
            </TabPane>
          ) : null} */}
          {!['01', '02', '11'].includes(status) ? (
            <TabPane tab="审批结论" key="3">
              {this.renderAuditTab()}
            </TabPane>
          ) : null}
          {!['01', '02', '04', '11'].includes(status) ? (
            <TabPane tab="物流信息" key="4">
              {this.renderExpressTab()}
            </TabPane>
          ) : null}
          <TabPane tab="押金管理" key="11">
            {this.renderDeposit()}
          </TabPane>
          <TabPane tab="账单信息" key="5">
            {this.renderBillTab()}
          </TabPane>
          {getParam('settlement') ? (
            <TabPane tab="催收记录" key="10">
              {this.renderCollectionRecord()}
            </TabPane>
          ) : null}
          <TabPane tab="流程进度" key="6">
            {this.renderProcessTab()}
          </TabPane>
          <TabPane tab="转单记录" key="20">
            {this.renderTransferTab()}
          </TabPane>
        </Tabs>
      </Card>
    );
  }
  // OrderService
  onHasten = (pageNumber, pageSize, source) => {
    OrderService.queryOrderHasten({ pageNumber, pageSize, source, orderId: getParam('id') }).then(
      res => {
        if (source === '02') {
          this.setState({
            HastenList: res.records,
            HastenTotal: res.total,
          });
        } else {
          this.setState({
            opeHastenList: res.records,
            opeHastenTotal: res.total,
          });
        }
      },
    );
  };
  onHastenBusiness = e => {
    this.setState(
      {
        HastenCurrent: e.current,
      },
      () => {
        this.onHasten(e.current, 3, '02');
      },
    );
  };
  onHastenOpe = e => {
    this.setState(
      {
        opeHastenCurrent: e.current,
      },
      () => {
        this.onHasten(e.current, 3, '01');
      },
    );
  };
  renderCollectionRecord() {
    const { orderVisible } = this.state;
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <CustomCard title="商家催收" style={{ marginBottom: 20 }} />
        <MyPageTable
          onPage={this.onHastenBusiness}
          columns={BusinessCollection}
          dataSource={onTableData(this.state.HastenList)}
          paginationProps={{
            current: this.state.HastenCurrent,
            pageSize: 3,
            total: this.state.HastenTotal,
          }}
        />
        <CustomCard title="平台催收" style={{ marginBottom: 20 }} />
        <MyPageTable
          onPage={this.onHastenOpe}
          columns={BusinessCollection}
          dataSource={onTableData(this.state.opeHastenList)}
          paginationProps={{
            current: this.state.opeHastenCurrent,
            pageSize: 3,
            total: this.state.opeHastenTotal,
          }}
        />
      </div>
    );
  }
  renderRemarkModal() {
    const { orderVisible } = this.state;
    const { getFieldDecorator } = this.props.form;
    const orderId = getParam('id');
    const handleOk = e => {
      e.preventDefault();
      this.props.form.validateFields(['beizhu'], (err, values) => {
        if (!err) {
          const { dispatch } = this.props;
          dispatch({
            type: 'order/orderRemark',
            payload: {
              orderId,
              remark: values.beizhu,
              orderType: '01',
            },
            callback: res => {
              this.onPage();
              this.handleCancel();
              this.props.form.resetFields();
            },
          });
        }
      });
    };

    return (
      <div>
        <Modal
          title="备注"
          visible={orderVisible === 'remark'}
          onOk={handleOk}
          onCancel={this.handleCancel}
        >
          <Form>
            <Form.Item label="备注内容" {...formItemLayout}>
              {getFieldDecorator('beizhu', {
                rules: [{ required: true, message: '请输入备注' }],
              })(<TextArea placeholder="请输入" />)}
            </Form.Item>
          </Form>
        </Modal>
      </div>
    );
  }

  renderCloseModal() {
    const { form } = this.props;
    const { orderVisible, orderId } = this.state;
    const handleOk = e => {
      this.props.form.validateFields(['closeReason'], (err, fieldsValue) => {
        if (err) return;
        OrderService.closeUserOrderAndRefundPrice({
          ...fieldsValue,
          closeType: '07',
          orderId,
        }).then(res => {
          this.onRentRenewalDetail();
          this.props.form.resetFields();
          this.handleCancel();
        });
      });
    };

    return (
      <Modal
        destroyOnClose
        title={'关闭订单'}
        visible={orderVisible === 'close'}
        onOk={handleOk}
        width="450px"
        onCancel={this.handleCancel}
      >
        <Form>
          <FormItem label="小记">
            {form.getFieldDecorator('closeReason', {})(<Input placeholder="请输入关单原因" />)}
          </FormItem>
        </Form>
      </Modal>
    );
  }

  renderSettleModal() {
    const { form, userOrderCashesDto = {} } = this.props;
    const { orderId, orderVisible, radioValue, damageValue } = this.state;

    const onRadioChange = e => {
      this.setState({
        radioValue: e.target.value,
      });
    };

    const handleDamageValue = val => {
      this.setState({
        damageValue: val,
      });
    };

    const handleOk = e => {
      this.props.form.validateFields(['penaltyAmount', 'cancelReason'], (err, fieldsValue) => {
        if (err) return;
        const payload = {
          lossAmount: 0,
          damageAmount: 0,
          penaltyAmount: 0,
          settlementType: radioValue,
          ...fieldsValue,
          orderId,
        };
        if (radioValue === '03') {
          payload.lossAmount = damageValue;
        } else if (radioValue === '02') {
          payload.damageAmount = damageValue;
        }
        OrderService.merchantsIssuedStatements(payload).then(res => {
          this.onRentRenewalDetail();
          this.props.form.resetFields();
          this.handleCancel();
        });
      });
    };

    return (
      <Modal
        title="是否确认结算？"
        zIndex={2000}
        visible={orderVisible === 'settle'}
        onOk={handleOk}
        onCancel={this.handleCancel}
      >
        <div>
          <div>
            <span style={{ marginRight: '20px' }}>宝贝状态</span>
            <Radio.Group onChange={onRadioChange} value={radioValue}>
              <Radio value="01">完好</Radio>
              <Radio value="02">损坏</Radio>
              <Radio value="03">丢失</Radio>
              <Radio value="04">其他</Radio>
            </Radio.Group>
            {radioValue === '02' && (
              <div style={{ display: 'inline-block', marginTop: '20px' }}>
                <span>损坏赔偿金：</span>
                <InputNumber
                  min={0}
                  defaultValue={0}
                  value={damageValue}
                  onChange={handleDamageValue}
                />
                <span>元</span>
              </div>
            )}
            {radioValue === '03' && (
              <div style={{ display: 'inline-block', marginTop: '20px' }}>
                <span>丢失赔偿金：</span>
                <InputNumber
                  min={0}
                  defaultValue={0}
                  value={damageValue}
                  onChange={handleDamageValue}
                />
                <span>元</span>
              </div>
            )}
            {radioValue === '04' && (
              <div style={{ marginTop: '20px' }}>
                <Form.Item labelCol={{ span: 4 }} label="违约金：">
                  {form.getFieldDecorator('penaltyAmount', {
                    initialValue: userOrderCashesDto.penaltyAmount,
                  })(<InputNumber min={0} />)}
                  <span> 元</span>
                </Form.Item>
                <Form.Item labelCol={{ span: 4 }} label="违约原因：">
                  {form.getFieldDecorator('cancelReason', {
                    initialValue: userOrderCashesDto.cancelReason,
                  })(<Input style={{ width: '60%' }} placeholder="请输入违约原因" />)}
                </Form.Item>
              </div>
            )}
          </div>
        </div>
      </Modal>
    );
  }

  renderAddressModal() {
    const {
      userOrderInfoDto = {},
      orderAddressDto,
      dispatch,
      productInfo,
      userOrderCashesDto,
      orderBuyOutDto,
      wlList,
    } = this.props;
    const { orderVisible, orderId } = this.state;
    const { getFieldDecorator } = this.props.form;
    const handleOk = e => {
      this.props.form.validateFields(['realName', 'street', 'city', 'telephone'], (err, values) => {
        if (err) return;
        dispatch({
          type: 'order/opeOrderAddressModify',
          payload: {
            realName: values.realName,
            street: values.street,
            telephone: values.telephone,
            province: values && values.city && values.city[0],
            city: values && values.city && values.city[1],
            area: values && values.city && values.city[2],
            orderId,
          },
          callback: res => {
            this.onRentRenewalDetail();
            this.props.form.resetFields();
            this.handleCancel();
          },
        });
        // OrderService.businessClosePayedOrder({
        //   ...fieldsValue,
        //   orderId,
        // }).then(res => {
        //   this.onRentRenewalDetail()
        //   this.props.form.resetFields();
        //   this.handleCancel();
        // });
      });
    };

    return (
      <Modal
        title="修改收货信息"
        visible={orderVisible === 'address'}
        onOk={handleOk}
        onCancel={this.handleCancel}
      >
        <Form>
          <Form.Item label="所在城市" {...formItemLayout}>
            {getFieldDecorator('city', {
              rules: [
                {
                  required: true,
                  message: '请输入备注',
                },
              ],
              initialValue: [
                orderAddressDto && orderAddressDto.province && orderAddressDto.province.toString(),
                orderAddressDto && orderAddressDto.city && orderAddressDto.city.toString(),
                orderAddressDto && orderAddressDto.area && orderAddressDto.area.toString(),
              ],
            })(
              <Cascader
                options={optionsdata}
                fieldNames={{ label: 'name', value: 'value', children: 'subList' }}
              />,
            )}
          </Form.Item>
          <Form.Item label="收货人姓名" {...formItemLayout}>
            {getFieldDecorator('realName', {
              rules: [{ required: true, message: '请输入备注' }],
              initialValue: orderAddressDto && orderAddressDto.realname,
            })(<Input placeholder="请输入" />)}
          </Form.Item>
          <Form.Item label="收货人手机号" {...formItemLayout}>
            {getFieldDecorator('telephone', {
              rules: [{ required: true, message: '请输入备注' }],
              initialValue: orderAddressDto && orderAddressDto.telephone,
            })(<Input placeholder="请输入" />)}
          </Form.Item>
          <Form.Item label="详细地址" {...formItemLayout}>
            {getFieldDecorator('street', {
              rules: [{ required: true, message: '请输入备注' }],
              initialValue: orderAddressDto && orderAddressDto.street,
            })(<TextArea placeholder="请输入" />)}
          </Form.Item>
        </Form>
      </Modal>
    );
  }
  //退押金
  confirm = e => {
    const orderId = getParam('id');
    OrderService.forceDepositRefund({
      orderId,
    }).then(res => {
      if (res) message.success('成功');
      this.props.dispatch({
        type: 'order/queryOpeUserOrderDetail',
        payload: {
          orderId: getParam('id'),
        },
        callback: e => {
          this.setState({
            userOrderCashesDtoBusiness: e.data.userOrderCashesDto,
          });
        },
      });
    });
  };
  renderAuditModal() {
    const { form } = this.props;
    const { orderVisible, orderId, titles } = this.state;
    const handleOk = e => {
      this.props.form.validateFields(['refuseType', 'auditRemark'], (err, fieldsValue) => {
        if (err) return;
        OrderService.telephoneAuditOrder({
          ...fieldsValue,
          orderId,
          remark: fieldsValue.auditRemark,
          orderAuditStatus: titles === '审批通过' ? '01' : '02',
        }).then(res => {
          this.onRentRenewalDetail();
          this.props.form.resetFields();
          this.handleCancel();
        });
      });
    };

    return (
      <Modal
        destroyOnClose
        title={titles}
        visible={orderVisible === 'audit'}
        onOk={handleOk}
        width="450px"
        onCancel={this.handleCancel}
      >
        <Form>
          {titles === '审批拒绝' ? (
            <FormItem label="拒绝类型" {...formItemLayout}>
              {form.getFieldDecorator('refuseType', {
                rules: [{ required: true, message: '请选择拒绝类型' }],
              })(
                <Select style={{ width: '100%' }} placeholder="请选择">
                  {Object.keys(AuditReason).map(option => {
                    return (
                      <Option key={option} value={option}>
                        {AuditReason[option]}
                      </Option>
                    );
                  })}
                </Select>,
              )}
            </FormItem>
          ) : null}
          <FormItem label="小记" {...formItemLayout}>
            {form.getFieldDecorator('auditRemark', {
              rules: [{ required: true, message: '请输入小记' }],
            })(<Input.TextArea placeholder="请输入小记" />)}
          </FormItem>
        </Form>
      </Modal>
    );
  }

  // 基础信息按钮组
  renderBaseInfoButtons() {
    const { userOrderInfoDto = {} } = this.props;
    const orderId = getParam('id');
    const { auditLabel, status, paidDeposit } = userOrderInfoDto;
    return (
      <Fragment>
        {status === '11' ? (
          <Fragment>
            {auditLabel !== '01' ? (
              <Fragment>
                <Button type="primary" onClick={() => this.showOrderModal('audit', true)}>
                  审批通过
                </Button>
                <Button type="primary" onClick={() => this.showOrderModal('audit', false)}>
                  审批拒绝
                </Button>
              </Fragment>
            ) : null}
            <Button type="primary" onClick={() => this.showOrderModal('close')}>
              关闭订单
            </Button>
            <Button type="primary" onClick={() => this.showOrderModal('address')}>
              修改收货信息
            </Button>
          </Fragment>
        ) : null}
        {['01', '02'].includes(status) ? (
          <Button type="primary" onClick={() => this.showOrderModal('address')}>
            修改收货信息
          </Button>
        ) : null}
        {status === '04' ? (
          <Fragment>
            <Button type="primary" onClick={() => this.showOrderModal('close')}>
              关闭订单
            </Button>
            <Button type="primary" onClick={() => this.showOrderModal('address')}>
              修改收货信息
            </Button>
          </Fragment>
        ) : null}
        {status === '05' ? (
          <Fragment>
            <Button type="primary" onClick={() => this.showOrderModal('close')}>
              关闭订单
            </Button>
            <Button
              type="primary"
              onClick={() => this.setState({ takeOverModal: true, takeOverDate: '' })}
            >
              确认收货
            </Button>
          </Fragment>
        ) : null}
        {getParam('settlement') ? (
          <Button type="primary" onClick={() => this.showOrderModal('remarks')}>
            记录催收
          </Button>
        ) : null}
        {['11', '04', '05'].includes(status) ? (
          <Button type="primary" onClick={() => this.setState({ depositModal: true })}>
            修改押金
          </Button>
        ) : null}
        {paidDeposit && (
          <Popconfirm
            title={`押金：${paidDeposit}元，确认退款？`}
            onConfirm={this.confirm}
            okText="确认"
            cancelText="取消"
          >
            <Button type="primary">退押金</Button>
          </Popconfirm>
        )}
        <Button type="primary" onClick={() => this.showOrderModal('remark')}>
          备注
        </Button>
        {['11', '04','13'].includes(status) ? (
          <Button type="primary" onClick={() => this.setState({ transferModal: true })}>
            转单
          </Button>
        ) : null}
      </Fragment>
    );
  }

  renderBaseInfo() {
    const { userOrderInfoDto = {}, orderLocationAddress = {}, shopInfoDto = {} } = this.props;

    return (
      <Card bordered={false} style={{ marginTop: 20 }}>
        <Descriptions title={<CustomCard title="下单人信息" />}>
          <Descriptions.Item label="姓名">{userOrderInfoDto.userName}</Descriptions.Item>
          <Descriptions.Item label="手机号">{userOrderInfoDto.telephone}</Descriptions.Item>
          <Descriptions.Item label="身份证号">{userOrderInfoDto.idCard}</Descriptions.Item>
          <Descriptions.Item label="年龄">{userOrderInfoDto.age}</Descriptions.Item>
          <Descriptions.Item label="性别">{userOrderInfoDto.gender}</Descriptions.Item>
          <Descriptions.Item label="下单时间">{userOrderInfoDto.createTime}</Descriptions.Item>
          <Descriptions.Item label="在途订单数">
            {userOrderInfoDto.userPayCount > 1 ? (
              <span className="red-status">{userOrderInfoDto.userPayCount}</span>
            ) : (
              userOrderInfoDto.userPayCount
            )}
          </Descriptions.Item>
          <Descriptions.Item label="完结订单数">
            {userOrderInfoDto.userFinishCount}
          </Descriptions.Item>
          <Descriptions.Item label="订单状态">
            {orderStatusMap[userOrderInfoDto.status]}
          </Descriptions.Item>
          <Descriptions.Item label="人脸认证" span={1}>
            {userOrderInfoDto.userFaceCertStatus ? (
              <span className="green-status">已通过</span>
            ) : (
              <span className="red-status">未通过</span>
            )}
          </Descriptions.Item>
          <Descriptions.Item label="渠道来源">{userOrderInfoDto.channelName}</Descriptions.Item>
          <Descriptions.Item label="所在位置">
            {orderLocationAddress && orderLocationAddress.province
              ? `${orderLocationAddress.province}${orderLocationAddress.city}${orderLocationAddress.district}${orderLocationAddress.streetNumber}`
              : '暂无定位'}
          </Descriptions.Item>
          <Descriptions.Item
            label={zhouqikoukuanReturnLabel(this.state.orderDetailApiRes, '周期扣款')}
          >
            {zhouqikoukuanReturnText(this.state.orderDetailApiRes, '周期扣款')}
          </Descriptions.Item>

          <Descriptions.Item label="身份证照片" span={2}>
            {userOrderInfoDto.idCardBackUrl ? (
              <>
                <img
                  src={userOrderInfoDto.idCardBackUrl}
                  style={{ width: 146, height: 77, marginRight: 20 }}
                  onClick={() => this.onKaiImg(userOrderInfoDto.idCardBackUrl)}
                />
                <img
                  onClick={() => this.onKaiImg(userOrderInfoDto.idCardFrontUrl)}
                  src={userOrderInfoDto.idCardFrontUrl}
                  style={{ width: 146, height: 77 }}
                />
              </>
            ) : (
              <span className="red-status">未上传</span>
            )}
          </Descriptions.Item>
        </Descriptions>
        {this.renderBaseInfoButtons()}
        <Divider />
      </Card>
    );
  }
  depositOverSure = e => {
    const orderId = getParam('id');
    e.preventDefault();
    this.props.form.validateFields(['afterAmount'], (err, values) => {
      if (!err) {
        OrderService.updatePayDepositAmount({ orderId, afterAmount: values.afterAmount }).then(
          res => {
            if (res) {
              message.success('修改成功');
              //押金信息
              OrderService.queryPayDepositLog({ orderId }).then(res => {
                if (res) {
                  this.setState({
                    depositData: res,
                    depositModal: false,
                  });
                }
              });
            }
          },
        );
      }
    });
  };
  render() {
    const { orderBuyOutDto, wlList } = this.props;
    const { drawerVisible } = this.state;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const { getFieldDecorator } = this.props.form;
    return (
      <PageHeaderWrapper>
        <Spin
          spinning={getParam('RentRenewal') ? this.props.RentRenewalLoading : this.props.loading}
        >
          {this.renderBaseInfo()}
          {this.renderContentTabCard()}
        </Spin>
        <BackTop />
        {this.renderRemarkModal()}
        {this.renderCloseModal()}
        {this.renderSettleModal()}
        {this.renderAddressModal()}
        {this.renderAuditModal()}
        {this.renderCollectionRecordModal()}
        <Drawer
          width={420}
          title={this.state.drawerTitle || '发货物流信息'}
          placement="right"
          onClose={this.onClose}
          visible={drawerVisible}
        >
          <Timeline>
            {wlList.map((item, idx) => {
              let color = 'blue';
              if (idx === 0) {
                color = 'green';
              }
              return (
                <Timeline.Item
                  style={{ color: idx !== 0 ? '#aaa' : '#333' }}
                  key={idx}
                  color={color}
                >
                  <p>{item.remark}</p>
                  <p>{item.datetime}</p>
                </Timeline.Item>
              );
            })}
          </Timeline>
        </Drawer>
        <Modal
          title="身份证图片"
          visible={this.state.visiblesimg}
          onCancel={this.handleCancels}
          destroyOnClose
          footer={null}
        >
          <img
            src={this.state.img}
            alt="alt"
            style={{ width: '100%', transform: `rotate(${this.state.rotate}deg)` }}
          />
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
              boxSizing: 'border-box',
              padding: '50px 150px 0 150px',
            }}
          >
            <img
              src={require('../../../../public/rotate-icon.png')}
              alt="alt"
              onClick={() => this.setState({ rotate: (this.state.rotate || 0) - 90 })}
              style={{ transform: 'rotateY(180deg)', cursor: 'pointer' }}
            />
            <img
              src={require('../../../../public/rotate-icon.png')}
              alt="alt"
              onClick={() => this.setState({ rotate: (this.state.rotate || 0) + 90 })}
              style={{ cursor: 'pointer' }}
            />
          </div>
        </Modal>
        <Modal
          title="确认收货"
          visible={this.state.takeOverModal}
          onOk={() => this.takeOverSure()}
          onCancel={() => this.setState({ takeOverModal: false })}
          destroyOnClose
        >
          <span>收货时间： </span>
          <DatePicker
            showTime
            onChange={value =>
              this.setState({ takeOverDate: moment(value).format('YYYY-MM-DD hh:mm:ss') })
            }
          />
        </Modal>
        <Modal
          title="修改押金"
          visible={this.state.depositModal}
          onOk={this.depositOverSure}
          onCancel={() => this.setState({ depositModal: false })}
          destroyOnClose
        >
          <Form>
            <Form.Item label="当前押金总额" {...formItemLayout}>
              {this.state.depositData && this.state.depositData.amount}
            </Form.Item>
            <Form.Item label="修改金额(元)" {...formItemLayout}>
              {getFieldDecorator('afterAmount', {
                rules: [
                  { required: true, message: '请输入押金金额' },
                  {
                    pattern: /^\d+$|^\d+[.]?\d+$/,
                    message: '请正确输入金额',
                  },
                ],
              })(<Input placeholder="请输入" />)}
            </Form.Item>
          </Form>
        </Modal>
        <Modal
          title="转单"
          visible={this.state.transferModal}
          onOk={this.getTransferModal}
          onCancel={() => this.setState({ transferModal: false })}
          destroyOnClose
        >
          <Form>
            <Form.Item label="转单商家" {...formItemLayout}>
              {getFieldDecorator('transferShopId', {
                rules: [{ required: true, message: '请选择' }],
              })(
                <Select
                  placeholder="请选择"
                  showSearch
                  filterOption={(input, option) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {this.state.transfer.map(item => (
                    <Option value={item.shopId} key={item.shopId}>
                      {item.name}
                    </Option>
                  ))}
                </Select>,
              )}
            </Form.Item>
            <Form.Item label="备注" {...formItemLayout}>
              {getFieldDecorator('remark', {
                rules: [{ required: true, message: '请输入' }],
              })(<Input.TextArea placeholder="请输入" />)}
            </Form.Item>
          </Form>
        </Modal>
      </PageHeaderWrapper>
    );
  }
}
