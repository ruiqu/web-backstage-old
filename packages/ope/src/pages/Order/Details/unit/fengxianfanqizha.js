/**
 * 风险反欺诈模块
 */
import ReportUnitHead from "../../../../components/reportUnitHead/index"
import { Table } from "antd"
import styles from "./fengxianfanqizha.less"
import { orderStatusMap } from "../../../../utils/enum"

const statusMap = {
  ...orderStatusMap,
  '02': '支付中',
  '03': '已支付申请关单',
}


/**
 * 返回需要进行合并的栏数据
 * @param {Array} arr : 列表总数据
 * @param {Number} startIdx : 开始的索引位置
 * @param {String} checkKey : 使用哪个键名来进行核对
 */
function returnRowSpan(arr, startIdx, checkKey="name") {
  const item = arr[startIdx] // 当前数据
  let same = 1 // 对相同数据进行计数

  for (let i = startIdx + 1; i < arr.length; i++) { // 找出
    const currentItem = arr[i] // 下一个值
    if (item[checkKey] != currentItem[checkKey]) {
      break
    } else {
      same++
    }
  }

  return same
}

/**
 * 渲染风险反欺诈的报告数据
 * @param {Object} uiData : 接口返回的数据
 * @param {boolean} inLoading : 是否处于加载过程中
 * @param {Object} addressObj : 地址对象
 * @returns 
 */
export const renderFengxianfanqizha = (uiData, inLoading, addressObj) => {
  const KEY1 = "sameMobilePhoneList"
  const KEY2 = "sameReceiptMobilePhoneList"
  const KEY3 = "sameAddressList"
  const KEY4 = "sameLocationList"
  const KEY5 = "sameIdcardList"

  const jindu = addressObj?.longitude // 经度信息
  const weidu = addressObj?.latitude // 纬度信息

  const returnKeyObj = key => {
    return uiData[key] || {}
  }

  /**
   * 返回Table组件所需要的source
   * @param {string} key : 取值为sameAddressList、sameLocationList等
   * @returns 
   */
  const returnSource = key => {
    const obj = returnKeyObj(key)
    return obj.list || []
  }
  
  const returnColumn = key => {
    const currentListData = returnSource(key) // 当前列表数据
    return [
      {
        title: "下单人身份证号",
        dataIndex: "ident_num",
        render: (val, row, idx) => {
          const obj = {
            children: val,
            props: {},
          }

          const currentId = row.ident_num_1 // 当前这个人的身份证
          const prevId = currentListData[idx-1]?.ident_num_1 // 表格上一个人的姓名
          const len = currentListData.length

          if (currentId == prevId) { // 当前行的用户名和上一行的用户名是同一个，那么进行合并
            obj.props.rowSpan = 0
          } else { // 不一样
            if (idx < len - 1) { // 计算接下来的行有多少行和当前行名字一样
              obj.props.rowSpan = returnRowSpan(currentListData, idx, "ident_num_1")
            } else { // 最后一行了，无需计算
              obj.props.rowSpan = 1
            }
          }

          return obj
        }
      },
      {
        title: "下单人姓名",
        dataIndex: "name",
        width: "108px",
        render: (val, row, idx) => {
          const obj = {
            children: val,
            props: {},
          }

          const currentId = row.ident_num_1 // 当前这个人的身份证
          const prevId = currentListData[idx-1]?.ident_num_1 // 表格上一个人的姓名
          const len = currentListData.length

          if (currentId == prevId) { // 当前行的用户名和上一行的用户名是同一个，那么进行合并
            obj.props.rowSpan = 0
          } else { // 不一样
            if (idx < len - 1) { // 计算接下来的行有多少行和当前行名字一样
              obj.props.rowSpan = returnRowSpan(currentListData, idx, "ident_num_1")
            } else { // 最后一行了，无需计算
              obj.props.rowSpan = 1
            }
          }

          return obj
        }
      },
      {
        title: "订单编号",
        dataIndex: "match_order_id",
        width: "20%",
      },
      {
        title: "订单状态",
        width: "100px",
        dataIndex: "status",
        render: function(val) {
          const cn = statusMap[val]
          return cn || "-"
        } 
      },
      { title: "下单手机号", dataIndex: "user_phone", width: "114px" },
      { title: "收货手机号", dataIndex: "receive_phone", width: "114px" },
      { title: "下单时间", dataIndex: "create_time" },
      { title: "总租金", dataIndex: "money" },
    ]
  }

  const xiadanPhoneObj = returnKeyObj(KEY1)
  const shouhuoPhoneobj = returnKeyObj(KEY2)
  const shuohuoAddressObj = returnKeyObj(KEY3)
  const currentAddressObj = returnKeyObj(KEY4)
  const sameIdObj = returnKeyObj(KEY5)

  const strUi = val => val || "-"

  return (
    <section className={styles.fengxianfanqizha_wrapper}>
      <div className={styles.part}>
        <ReportUnitHead title="相同下单手机号" />
        <div className={styles.amount_wrapper}>
          <span className={styles.mr20}>关联客户数：{ strUi(xiadanPhoneObj.customers) }</span>
          <span>关联订单数：{ strUi(xiadanPhoneObj.orderAssociations) }</span>
        </div>
        <Table
          bordered
          className={styles.table_content}
          rowKey={record => record.id}
          columns={returnColumn(KEY1)}
          dataSource={returnSource(KEY1)}
          pagination={false}
          loading={inLoading}
        />
      </div>

      <div className={styles.part}>
        <ReportUnitHead title="相同收货手机号" />
        <div className={styles.amount_wrapper}>
          <span className={styles.mr20}>关联客户数：{ strUi(shouhuoPhoneobj.customers) }</span>
          <span>关联订单数：{ strUi(shouhuoPhoneobj.orderAssociations) }</span>
        </div>
        <Table
          bordered
          className={styles.table_content}
          rowKey={record => record.id}
          columns={returnColumn(KEY2)}
          dataSource={returnSource(KEY2)}
          pagination={false}
          loading={inLoading}
        />
      </div>

      <div className={styles.part}>
        <ReportUnitHead title="相似收货地址" />
        <div className={styles.amount_wrapper}>
          <span className={styles.mr20}>关联客户数：{ strUi(shuohuoAddressObj.customers) }</span>
          <span>关联订单数：{ strUi(shuohuoAddressObj.orderAssociations) }</span>
        </div>
        <Table
          bordered
          className={styles.table_content}
          rowKey={record => record.id}
          columns={returnColumn(KEY3)}
          dataSource={returnSource(KEY3)}
          pagination={false}
          loading={inLoading}
        />
      </div>

      <div className={styles.part}>
        <ReportUnitHead title="相似当前位置" />
        <div className={styles.amount_wrapper}>
          <span className={styles.mr20}>关联客户数：{ strUi(currentAddressObj.customers) }</span>
          <span className={styles.mr20}>关联订单数：{ strUi(currentAddressObj.orderAssociations) }</span>
          <span className={styles.mr20}>当前经度：{ strUi(jindu) }</span>
          <span>当前纬度：{ strUi(weidu) }</span>
        </div>
        <Table
          bordered
          className={styles.table_content}
          rowKey={record => record.id}
          columns={returnColumn(KEY4)}
          dataSource={returnSource(KEY4)}
          pagination={false}
          loading={inLoading}
        />
      </div>

      <div className={styles.part}>
        <ReportUnitHead title="近7天身份证前6位相同" />
        <div className={styles.amount_wrapper}>
          <span className={styles.mr20}>关联客户数：{ strUi(sameIdObj.customers) }</span>
          <span>关联订单数：{ strUi(sameIdObj.orderAssociations) }</span>
        </div>
        <Table
          bordered
          className={styles.table_content}
          rowKey={record => record.id}
          columns={returnColumn(KEY5)}
          dataSource={returnSource(KEY5)}
          pagination={false}
          loading={inLoading}
        />
      </div>
    </section>
  )
}