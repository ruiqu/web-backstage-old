/**
 * 适用于Order模块的公用方法封装
 */
import moment from 'moment';
import { message } from 'antd';

/**
 * 导出订单的工具方法
 * @param {*} actionType : 所需要dispatch的type
 * @param {*} that : 组件实例
 * @param {*} needChannelId : 传参是否需要包括渠道来源数据
 * @param {*} isEmptyData : post接口是否不需要传参
 * @param {*} statusKey : 订单状态在接口中所对应的字段名
 */
export const exportCenterHandler = (that, actionType, needChannelId=false, isEmptyData, statusKey='status') => {
  if (that.exporting) return; // 防止快速点击
  that.exporting = true;

  let postData = {}; // 接口请求参数

  if (!isEmptyData) {
    const { yunTime } = that.state; // 默认导出时间范围差不多是上一个月

    const getFormValue = that.props.form.getFieldValue;
    const formatDateValue = val => moment(val).format('YYYY-MM-DD HH:mm:ss');

    const formChoseDateVal = getFormValue('createDate'); // 用户在表单中所选中的订单时间范围

    const createTimeStart = formatDateValue(formChoseDateVal ? formChoseDateVal[0] : yunTime[0]); // 导出开始时间段，如果表单没有，那就上个月
    const createTimeEnd = formatDateValue(formChoseDateVal ? formChoseDateVal[1] : yunTime[1]); // 导出结束时间段，如果表单没有，那就当前
    const cId = getFormValue('channelId'); // 用户选中的渠道来源
    const stat = getFormValue(statusKey); // 用户选中的订单状态
    postData = { createTimeStart, createTimeEnd };
    cId && needChannelId && (postData.channelId = cId);

    if (stat) {
      let val;
      if (statusKey === 'state') val = stat;
      else val = [stat];
      postData[statusKey] = val;
    }
  }

  const responseCb = () => {
    that.exporting = false;
    message.success('导出任务创建成功，请前往“数据管理-导出数据下载”完成下载。');
  };

  that.props.dispatch({ type: `order/${actionType}`, payload: postData, callback: responseCb });
};
