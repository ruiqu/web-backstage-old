/**
 * 新增/修改问题
 */
import React from "react"
import { PageHeaderWrapper } from "@ant-design/pro-layout"
import { Card, Form, Select, Input, Button, Popconfirm, message } from "antd"
import BraftEditor from "braft-editor"
import styles from "./index.less"
import request from "@/services/baseService"
import { bfUploadFn } from "@/utils/utils"
import router from "umi/router"

// FIXME: 富文本校验时机的问题

const Option = Select.Option

@Form.create()
class QuesConfigOperation extends React.Component {
  state = {
    quesDetail: {}, // 问题详情数据
    categoryList: [], // 分类列表集合
    pageLoading: false,
    submiting: false, // 修改|新增中
  }

  componentDidMount() {
    this.setState({ pageLoading: true })
    this.fetchQuestionDetail().then(this.fetchCategoryList).finally(() => {
      this.setState({ pageLoading: false })
    })
  }

  returnTabIdFromUrl = () => {
    const s1 = this.props.location.search
    const searchStr = s1.substring(1)
    const searchObj = new URLSearchParams(searchStr)
    return searchObj.get("tid")
  }

  // 加载问题详情数据
  fetchQuestionDetail = () => {
    if (!this.checkIsModify()) return Promise.resolve(1)
    const postData = { id: this.returnQuestionId() }
    return request("/hzsx/noticeCenter/queryOpeNoticeItemDetail", postData, "post").then(res => {
      this.setState({ quesDetail: res })
    })
  }

  // 加载类目列表数据
  fetchCategoryList = () => {
    return request("/hzsx/noticeCenter/queryOpeNoticeTabDetailList").then(res => {
      if (Object.prototype.toString.call(res) !== "[object Array]") return
      const tid = this.returnTabIdFromUrl()
      let exists
      if (tid) exists = res.some(obj => obj.id == tid)
      this.setState({ categoryList: res })
      if (exists && !this.checkIsModify()) { // 新增过程且tid有效
        this.setState({ quesDetail: { tabId: +tid } })
      }
    })
  }

  // 返回修改问题的ID
  returnQuestionId = () => {
    return this.props.match.params.id
  }

  // 判断是否是修改
  checkIsModify = () => {
    return this.returnQuestionId()
  }

  // 增加&&修改的处理方法
  addHandler = () => {
    this.props.form.validateFields((err, value) => {
      if (!err) {
        value.detail = value.detail.toHTML()
        let text = "新增成功"
        let url
        let postData = value
        if (this.checkIsModify()) { // 修改过程
          text = "修改成功"
          url = "/hzsx/noticeCenter/modifyOpeNoticeItem"
          postData = {
            ...this.state.quesDetail,
            ...postData,
          }
        }
        else url = "/hzsx/noticeCenter/addOpeNoticeItem" // 新建过程
        this.setState({ submiting: true })
        request(url, postData, "post").then(() => {
          message.success(text)
          router.go(-1)
        }).finally(() => {
          this.setState({ submiting: false })
        })
      }
    })
  }

  // 删除的时候触发
  delHandler = () => {
    const url = `/hzsx/noticeCenter/deleteOpeNoticeItem?id=${this.returnQuestionId()}`
    if (this.deling) {
      message.warn("删除中")
      return
    }
    this.deling = true
    request(url, {}, "get").then(() => {
      message.success("删除成功")
      router.go(-1)
    }).finally(() => {
      this.deling = false
    })
  }

  // 富文本的校验
  checkDetail = (_, value, callback) => {
    if (value.toHTML() === "<p></p>") {
      callback('请输入内容')
      return
    }
    callback()
  }

  // 取消，返回上一步
  cancelHandler = () => {
    router.go(-1)
  }

  render() {
    const { getFieldDecorator } = this.props.form
    const { quesDetail } = this.state

    return (
      <PageHeaderWrapper>
        <Card loading={this.state.pageLoading} bordered={false}>
          <Form labelCol={{ span: 2 }} wrapperCol={{ span: 20 }} className={styles.mb15}>
            <Form.Item label="类目">
              {
                getFieldDecorator(
                  "tabId",
                  {
                    initialValue: quesDetail.tabId,
                    rules: [
                      { required: true, message: "类目名称不能为空" },
                    ],
                  }
                )(
                  <Select allowClear placeholder="请选择类目" className={styles.w300}>
                    {
                      this.state.categoryList.map(cObj => (
                        <Option value={cObj.id} key={cObj.id}>{cObj.name}</Option>
                      ))
                    }
                  </Select>
                )
              }
            </Form.Item>

            <Form.Item label="标题">
              {
                getFieldDecorator(
                  "name",
                  {
                    initialValue: quesDetail.name,
                    rules: [
                      { required: true, message: "标题不能为空" },
                      { max: 36, message: "长度不能超过36" },
                    ],
                  }
                )(
                  <Input placeholder="请输入标题" allowClear />
                )
              }
            </Form.Item>

            <Form.Item label="排序">
              {
                getFieldDecorator(
                  "indexSort",
                  {
                    initialValue: quesDetail.indexSort,
                    rules: [
                      { required: true, message: "排序不能为空" },
                    ],
                  }
                )(
                  <Input placeholder="请输入排序序号" allowClear />
                )
              }
            </Form.Item>

            <Form.Item label="内容">
              {
                getFieldDecorator(
                  "detail",
                  {
                    initialValue: BraftEditor.createEditorState(quesDetail.detail),
                    // validateTrigger: "onFocus",
                    rules: [{ validator: this.checkDetail }],
                  }
                )(
                  <BraftEditor
                    style={{ border: '1px solid #d1d1d1', borderRadius: 5, backgroundColor: '#fff' }}
                    contentStyle={{ height: 500, boxShadow: 'inset 0 1px 3px rgba(0,0,0,.1)' }}
                    excludeControls={['emoji', 'clear', 'blockquote', 'code']}
                    media={{ uploadFn: bfUploadFn }}
                  />
                )
              }
            </Form.Item>
          </Form>
          <div className={styles.btnWrapper}>
            <Button onClick={this.cancelHandler}>取消</Button>
            {
              this.checkIsModify() && (
                <Popconfirm
                  title="确定删除吗?"
                  onConfirm={this.delHandler}
                >
                  <Button>删除</Button>
                </Popconfirm>
              )
            }
            <Button loading={this.state.submiting} onClick={this.addHandler} type="primary">&nbsp;&nbsp;&nbsp;确定&nbsp;&nbsp;&nbsp;</Button>
          </div>
        </Card>
      </PageHeaderWrapper>
    )
  }
}

export default QuesConfigOperation
