/**
 * 商家中心首页问题配置
 */
import React from "react"
import { Table, Button, Modal, Form, Input, message, Popconfirm } from "antd"
import request from "@/services/baseService"
import { onTableData } from "@/utils/utils"
import styles from "./index.less"

const defaultActiveQuestionObj = {
  materialItemId: "", // 素材图片ID
  indexSort: "", // 排序序号
  jumpUrl: "", // 跳转链接
}

@Form.create()
class QuestionConfig extends React.Component {
  state = {
    pageNumber: 1,
    pageSize: 10,
    questionApiResult: {}, // 加载问题列表接口时所返回的数据
    fetching: false, // 是否处于加载过程中
    showModal: false, // 是否显示弹窗
    submitting: false, // 正在新建中
    activeQuestionObj: defaultActiveQuestionObj, // 处于焦点的问题对象
  }

  componentDidMount() {
    this.fetchQuestionList()
  }

  // 加载问题列表数据
  fetchQuestionList() {
    this.setState({ fetching: true })
    const postData = { pageNumber: this.state.pageNumber, pageSize: this.state.pageSize }
    return request("/hzsx/noticeCenter/queryOpeNoticePage", postData, "post").then(res => {
      this.setState({ questionApiResult: res })
    }).finally(() => {
      this.setState({ fetching: false })
    })
  }

  // 删除的时候触发
  delHandler = itemObj => {
    const { id } = itemObj
    const url = `/hzsx/noticeCenter/deleteOpeNotice?id=${id}`
    if (this.deling) return // 删除中
    this.deling = true
    request(url, {}, "get").then(res => {
      message.success("删除成功")
      this.fetchQuestionList()
    }).finally(() => {
      this.deling = false
    })
  }

  // 修改的时候触发
  modifyHandler = rowData => {
    this.setState({ showModal: true, activeQuestionObj: rowData })
  }

  tableColumns = [
    { title: "素材ID", dataIndex: "materialItemId" },
    { title: "问题ID", dataIndex: "jumpUrl" },
    { title: "排序", dataIndex: "indexSort" },
    {
      title: "操作",
      render: (_, r) => {
        return (
          <div>
            <a onClick={() => this.modifyHandler(r)} className={styles.acls}>修改</a>
            <Popconfirm
              title="确定删除吗？"
              onConfirm={() => this.delHandler(r)}
            >
              <a>删除</a>
            </Popconfirm>
          </div>
        )
      }
    }
  ]

  // 创建新问题的时候触发
  newQuestionHandler = () => {
    this.setState({ showModal: true, activeQuestionObj: defaultActiveQuestionObj })
  }

  // 创建新问题的处理方法
  submitNewQuestionAction = () => {
    this.props.form.validateFields((err, values) => {
      if (!err) { // 校验通过
        const postObj = { ...values }
        const qid = this.state.activeQuestionObj?.id
        this.setState({ submitting: true })
        let text
        let url
        if (qid) { // 修改过程
          text = "修改成功"
          url = "/hzsx/noticeCenter/modifyOpeNotice"
          postObj.id = qid
        } else {
          text = "新建成功"
          url = "/hzsx/noticeCenter/addOpeNotice"
        }
        postObj.materialItemId = +postObj.materialItemId
        request(url, postObj, "post").then(_ => { // 成功新建
          message.success(text)
          this.fetchQuestionList()
          this.cancelModalHandler()
        }).finally(() => {
          this.setState({ submitting: false })
        })
      }
    })
  }

  // 取消模态弹窗
  cancelModalHandler = () => {
    this.props.form.resetFields()
    this.setState({ showModal: false })
  }

  // 改变页码的时候触发
  changePageHandler = page => {
    this.setState({ pageNumber: page }, this.fetchQuestionList)
  }

  // 每页条数改变时触发
  onShowSizeChange = (_, size) => {
    this.setState({ pageNumber: 1, pageSize: size }, this.fetchQuestionList)
  }

  render() {
    const { questionApiResult, fetching, showModal, activeQuestionObj, pageNumber, pageSize } = this.state
    const { getFieldDecorator } = this.props.form

    const questionList = onTableData(questionApiResult?.records || [])
    const total = questionApiResult?.total

    const showTotal = () => `共有${total}条`

    const formItemLayout = {
      labelCol: {
        xs: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 16 },
      },
    };

    return (
      <div className={styles.wrapper}>
        <Button
          type="primary"
          className={styles.mb15}
          onClick={this.newQuestionHandler}
        >
          +&nbsp;新增通知
        </Button>

        <Table
          loading={fetching}
          columns={this.tableColumns}
          dataSource={questionList}
          pagination={{
            current: pageNumber,
            pageSize,
            total,
            onChange: this.changePageHandler,
            showTotal: showTotal,
            showQuickJumper: true,
            pageSizeOptions: ['5', '10', '20'],
            showSizeChanger: true,
            onShowSizeChange: this.onShowSizeChange,
          }}
        />

        <Modal
          title={this.state.activeQuestionObj?.id ? "修改通知" : "新增通知"}
          visible={showModal}
          onOk={this.submitNewQuestionAction}
          okButtonProps={{ disabled: this.state.submitting }}
          onCancel={this.cancelModalHandler}
        >
          <Form {...formItemLayout} className={styles.mb15} layout="inline">
            <Form.Item label="素材ID" className={styles.row}>
              {
                getFieldDecorator(
                  "materialItemId",
                  {
                    initialValue: activeQuestionObj.materialItemId,
                    rules: [
                      { required: true, message: "素材ID不能为空" },
                    ],
                  }
                )(
                  <Input className={styles.inputWrapper} placeholder="请输入素材ID" allowClear/>
                )
              }
            </Form.Item>
            <Form.Item label="问题ID" className={styles.row}>
              {
                getFieldDecorator(
                  "jumpUrl",
                  {
                    initialValue: activeQuestionObj.jumpUrl,
                    rules: [
                      { required: true, message: "问题ID不能为空" },
                    ]
                  },
                )(
                  <Input className={styles.inputWrapper} placeholder="请输入问题ID" allowClear/>
                )
              }
            </Form.Item>
            <Form.Item label="排序" className={styles.row}>
              {
                getFieldDecorator(
                  "indexSort",
                  {
                    initialValue: activeQuestionObj.indexSort,
                    rules: [
                      { required: true, message: "排序编号不能为空" },
                    ]
                  }
                )(
                  <Input className={`${styles.inputWrapper} ${styles.iw3}`} placeholder="请输入排序编号" allowClear/>
                )
              }
            </Form.Item>
          </Form>
        </Modal>
      </div>
    )
  }
}

export default QuestionConfig