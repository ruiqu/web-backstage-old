import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Card, Tabs, Form, Input, Button, message } from 'antd';
import BraftEditor from 'braft-editor';
import NoticeService from './services';
import { bfUploadFn, onTableData } from '@/utils/utils';
import AntTable from '@/components/AntTable';
import QuestionConfig from './components/questionConfig';
const { TextArea } = Input;
const { TabPane } = Tabs;
@Form.create({})
export default class Notice extends Component {
  state = {
    detail: null,
    title: undefined,
    miniList: [], // 小程序滚动
    Key: '01',
    tabPaneList: [
      {
        key: '01',
        content: '商家中心公告',
      },
      {
        key: '03',
        content: '商家中心通知',
      },
      {
        key: '02',
        content: '小程序公告',
      },
    ],
  };
  componentDidMount() {
    // 商家公共
    NoticeService.selectNotice({}).then(res => {
      this.setState({
        detail: res.detail,
        title: res.title,
      });
    });

    // 小程序公告
    NoticeService.selectApiNotice({}).then(res => {
      this.setState({
        miniList: res,
      });
    });
  }
  handleOk = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        if (this.state.Key === '01') {
          NoticeService.updateNotice({ title: values.title, detail: values.detail.toHTML() }).then(
            res => {
              if (res) {
                message.success('修改成功～');
                NoticeService.selectNotice({}).then(res => {
                  this.setState({
                    detail: res.detail,
                    title: res.title,
                  });
                });
              }
            },
          );
        } else if (this.state.Key === '02') {
          NoticeService.updateApiNotice({ title: values.miniList }).then(res => {
            if (res) {
              message.success('修改成功～');
              NoticeService.selectApiNotice({}).then(res => {
                this.setState({
                  miniList: res,
                });
              });
            }
          });
        }
      }
    });
  };
  save = (e, id) => {
    NoticeService.updateApiNotice({ channelId: id, content: e.currentTarget.value }).then(res => {
      message.success('修改成功。');
      // 小程序公告
      NoticeService.selectApiNotice({}).then(res => {
        this.setState({
          miniList: res,
        });
      });
    });
  };
  render() {
    const largeFormItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 3 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 },
      },
    };
    let columns = [
      {
        title: '渠道',
        dataIndex: 'channelName',
        width: '25%',
        align: 'center',
      },

      {
        title: '公告内容',

        ellipsis: true,
        render: (text, record, index) => (
          <Input
            defaultValue={record.content}
            onPressEnter={e => this.save(e, record.channelId)}
            onBlur={e => this.save(e, record.channelId)}
          />
        ),
      },
    ];
    const checkDetail = (_, value, callback) => {
      if (value.toHTML() === '<p></p>') {
        callback('请输入弹窗内容');
        return;
      }
      callback();
    };
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <PageHeaderWrapper>
        <Card bordered={false} className="noMbTableCls">
          <Tabs
            activeKey={this.state.Key}
            animated={false}
            onChange={e => this.setState({ Key: e })}
          >
            {this.state.tabPaneList.map(item => {
              return <TabPane tab={item.content} key={item.key}></TabPane>;
            })}
          </Tabs>
        </Card>
        <Card className="noPaddingTopCardCls" bordered={false}>
          {this.state.Key === '01' && (
            <Form>
              <Form.Item {...largeFormItemLayout} required label="滚动条内容">
                {getFieldDecorator('title', {
                  initialValue: this.state.title,
                  rules: [{ required: true, message: '请输入滚动条内容' }],
                })(<TextArea rows={4} placeholder="请输入滚动条内容" />)}
              </Form.Item>
              <Form.Item {...largeFormItemLayout} required label="弹窗内容">
                {getFieldDecorator('detail', {
                  initialValue: BraftEditor.createEditorState(this.state.detail),
                  rules: [{ validator: checkDetail }],
                })(
                  <BraftEditor
                    style={{
                      border: '1px solid #d1d1d1',
                      borderRadius: 5,
                      backgroundColor: '#fff',
                    }}
                    contentStyle={{ height: 300, boxShadow: 'inset 0 1px 3px rgba(0,0,0,.1)' }}
                    excludeControls={['emoji', 'clear', 'blockquote', 'code']}
                    media={{ uploadFn: bfUploadFn }}
                  />,
                )}
              </Form.Item>
            </Form>
          )}
          {this.state.Key === '02' && (
            <>
              <AntTable dataSource={onTableData(this.state.miniList)} columns={columns} />
            </>
          )}
          {this.state.Key === '01' && (
            <div
              style={{
                textAlign: 'center',
              }}
            >
              <Button
                type="primary"
                onClick={this.handleOk}
                style={{
                  width: 120,
                  margin: '70px',
                }}
              >
                确定
              </Button>
            </div>
          )}
          {
            this.state.Key === '03' && (
              <QuestionConfig />
            )
          }
        </Card>
      </PageHeaderWrapper>
    );
  }
}
