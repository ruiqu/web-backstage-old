/**
 * 常见问题配置页面
 */
import React from "react"
import { connect } from "dva"
import { PageHeaderWrapper } from "@ant-design/pro-layout"
import { Card, Button, Modal, Form, Input, Tabs, message, Popconfirm, Table } from "antd"
import request from "@/services/baseService"
import router from "umi/router"
import styles from "./index.less"

const { TabPane } = Tabs

// LOG: 在请求常见问题分页这里时，接口是分页的，前端这里就不透出分页功能了。毕竟：1、数据不会很多

@connect(({ quesConfigModel }) => ({
  ...quesConfigModel,
}))
@Form.create()
class QuesConfig extends React.Component {
  state = {
    showDialog: false, // 是否显示弹窗
    activeTabObj: {}, // 处于焦点状态的tab对象，在修改tab的时候需要用到
    tabList: [], // 类目列表数据
    quesListApiResult: {}, // 获取当前类目所对应的问题列表接口所返回的数据
    tabListFetching: false, // 加载tab列表中
    quesListFetching: false, // 加载问题列表中
    tabAdding: false, // 在对tab进行ajax操作中
    tabDeling: false, // 删除tab中
  }

  componentDidMount() {
    const needResetFlag = !this.props.activeTabId
    this.fetchTabListHandler(needResetFlag)
  }

  componentWillReceiveProps(nextProps) {
    const currentTabId = this.props.activeTabId
    const nextTabId = nextProps.activeTabId

    if (currentTabId != nextTabId) { // tab发生切换，那么请求该tab所对应的常见问题列表数据
      this.fetchQuesionListHandler(nextTabId)
    }
  }

  // 加载类目列表数据
  fetchTabListHandler = (needReset=true) => {
    this.setState({ tabListFetching: true })
    return request("/hzsx/noticeCenter/queryOpeNoticeTabDetailList").then(res => {
      const firstId = res && res[0]?.id
      needReset && this.props.dispatch({ type: "quesConfigModel/quesConfigMuActiveTab", payload: firstId })
      this.setState({ tabList: res })
      if (!this.state.quesListApiResult?.records) { // 首次加载
        this.fetchQuesionListHandler(this.props.activeTabId || firstId)
      }
    }).finally(() => {
      this.setState({ tabListFetching: false })
    })
  }

  // 加载常见问题列表数据
  fetchQuesionListHandler = tabId => {
    const url = `/hzsx/noticeCenter/queryOpeNoticeItemPage`
    const activeTabId = tabId || this.props.activeTabId
    const queryData = { tabId: activeTabId, pageNumber: 1, pageSize: 100000 } // 就不分页了，1、分页后需要考虑分页数据维护在redux（因为具有详情页面）；2、需要考虑不同的tab对应不同的分页数据；3、商家的消息通知功能接口并没有提供分页功能
    this.setState({ quesListFetching: true })
    request(url, queryData, "post").then(res => {
      this.setState({ quesListApiResult: res })
    }).finally(() => {
      this.setState({ quesListFetching: false })
    })
  }

  // 新增或者修改tab
  modalOkHandler = () => {
    this.props.form.validateFields((err, value) => {
      if (!err) {
        let postObj = { ...value }
        this.setState({ tabAdding: true })
        let url = "/hzsx/noticeCenter/addOpeNoticeTab"
        let txt = "新增成功"
        if (this.state.activeTabObj?.id) {
          postObj = {
            ...this.returnActiveTabObj(),
            ...postObj
          }
          txt = "修改成功"
          url = "/hzsx/noticeCenter/modifyOpeNoticeTab"
        }
        
        request(url, postObj, "post").then(_ => {
          message.success(txt)
          this.fetchTabListHandler(false) // 新增或者修改的话不重设处于焦点位置的tab
          this.modalCancelHandler()
        }).finally(() => {
          this.setState({ tabAdding: false })
        })
      }
    })
  }

  // 取消模态
  modalCancelHandler = () => {
    this.setState({ showDialog: false, activeTabObj: {} }) // 副作用，关闭弹窗后会把处于焦点的tab给初始化一下
    this.props.form.resetFields()
  }

  // 返回处于焦点的菜单对象
  returnActiveTabObj = () => {
    const matchCb = obj => obj.id == this.props.activeTabId
    return this.state.tabList.find(matchCb) || {}
  }

  // 修改tab的时候触发
  modifyTabHandler = () => {
    this.setState({ showDialog: true, activeTabObj: this.returnActiveTabObj() })
  }

  // 删除分类
  delTabHandler = () => {
    const id = this.returnActiveTabObj()?.id
    if (!id) return
    const url = `/hzsx/noticeCenter/deleteOpeNoticeTab?id=${id}`
    this.setState({ tabDeling: true })
    request(url, {}, "get").then(() => {
      message.success("删除成功")
      this.fetchTabListHandler() // 删除成功后重新加载一下tab列表数据，此时副作用会将处于焦点的tab初始化为第一个
    }).finally(() => {
      this.setState({ tabDeling: false })
    })
  }

  // tab切换时触发
  tabChangeHandler = tv => {
    this.props.dispatch({ type: "quesConfigModel/quesConfigMuActiveTab", payload: tv })
  }

  // 新建常见问题的处理方法
  newQuesHandler = () => {
    router.push(`/configure/quesConfig/add?tid=${this.props.activeTabId}`)
  }

  // 修改常见问题的时候触发
  modifyQuestionHandler = qObj => {
    const url = `/configure/quesConfig/modify/${qObj.id}?tid=${this.props.activeTabId}`
    router.push(url)
  }

  // 删除常见问题
  delQuestionHandler = qObj => {
    const { id } = qObj
    const url = `/hzsx/noticeCenter/deleteOpeNoticeItem?id=${id}`
    if (this.deleting) return
    this.deleting = true
    request(url, {}, "get").then(res => {
      message.success("删除成功")
      this.fetchQuesionListHandler()
    }).finally(() => {
      this.deleting = false
    })
  }

  // table组件的配置数据
  tableColumns = [
    { title: "ID", dataIndex: "id" },
    { title: "标题", dataIndex: "name" },
    { title: "排序", dataIndex: "indexSort" },
    {
      title: "操作",
      render: (_, rowObj) => {
        return (
          <div>
            <a onClick={() => this.modifyQuestionHandler(rowObj)} className={styles.mr8}>修改</a>
            <Popconfirm
              title="确定删除吗？"
              onConfirm={() => this.delQuestionHandler(rowObj)}
            >
              <a>删除</a>
            </Popconfirm>
          </div>
        )
      }
    }
  ]

  render() {
    const { showDialog, activeTabObj, tabList } = this.state
    const { getFieldDecorator } = this.props.form
    const { activeTabId } = this.props // 处于焦点的分类ID
    const quesList = this.state.quesListApiResult?.records || [] // 问题列表数据

    return (
      <PageHeaderWrapper>
        <Card bordered={false} loading={this.state.tabListFetching}>
          <div className={styles.btnWrpper}>
            <Button onClick={() => this.setState({ showDialog: true })} type="primary">添加类目</Button>
            <Button onClick={this.modifyTabHandler}>修改类目</Button>
            <Popconfirm
              title="确定删除吗？"
              onConfirm={this.delTabHandler}
            >
              <Button>删除类目</Button>
            </Popconfirm>
          </div>

          <Tabs animated={false} activeKey={`${activeTabId}`} onChange={this.tabChangeHandler}>
            {
              tabList.map(tabObj => (
                <TabPane tab={tabObj.name} key={tabObj.id}></TabPane>
              ))
            }
          </Tabs>

          <Button
            className={styles.mb20}
            onClick={this.newQuesHandler}
            type="primary"
          >新建常见问题</Button>

          <Table
            columns={this.tableColumns}
            loading={this.state.quesListFetching}
            dataSource={quesList}
            rowKey={record => record.id}
            pagination={false}
          />

        </Card>

        <Modal
          title={activeTabObj.id ? "修改类目" : "添加类目"}
          visible={showDialog}
          onOk={this.modalOkHandler}
          onCancel={this.modalCancelHandler}
          okButtonProps={{ disabled: this.state.tabAdding }}
        >
          <Form className={styles.mb15} layout="inline">
            <Form.Item label="名称" className={styles.mb20}>
              {
                getFieldDecorator(
                  "name",
                  {
                    initialValue: activeTabObj.name,
                    rules: [
                      { required: true, message: "类目名称不能为空" },
                    ],
                  }
                )(
                  <Input className={styles.inputWrapper} placeholder="请输入类目名称" allowClear/>
                )
              }
            </Form.Item>
            <Form.Item label="排序">
              {
                getFieldDecorator(
                  "indexSort",
                  {
                    initialValue: activeTabObj.indexSort,
                    rules: [
                      { required: true, message: "类目排序不能为空" },
                    ],
                  }
                )(
                  <Input className={styles.inputWrapper} placeholder="请输入类目排序" allowClear/>
                )
              }
            </Form.Item>
          </Form>
        </Modal>
      </PageHeaderWrapper>
    )
  }
}

export default QuesConfig