import request from '@/services/baseService';

export default {
  // 商家公告查询接口
  selectNotice: data => {
    return request(`/hzsx/notice/selectNotice`, data, 'get');
  },
  // 商家公告修改接口
  updateNotice: data => {
    return request(`/hzsx/notice/updateNotice`, data);
  },
  // 小程序公告查询接口
  selectApiNotice: data => {
    return request(`/hzsx/notice/selectApiNotice`, data, 'get');
  },
  // 小程序公告修改接口
  updateApiNotice: data => {
    return request(`/hzsx/notice/updateApiNotice`, data);
  },

  // updateApiNotice: data => {
  //   return request(`/hzsx/notice/selectApiNotice`, data, 'get');
  // },
  
};
