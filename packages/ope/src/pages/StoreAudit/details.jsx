import React, { PureComponent, Fragment } from 'react';
// import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import {
  Card,
  Descriptions,
  Spin,
  message,
  Table,
  Row,
  Col,
  Modal,
  Button,
  Cascader,
  Input,
  Divider,
  Form,
  Select,
  Radio,
} from 'antd';
const { TextArea } = Input;
import CustomCard from '@/components/CustomCard';
import { getParam } from '@/utils/utils.js';
const status = {
  3: '待审核',
  4: '审核拒绝',
  5: '审核成功',
};
@connect(({ shop }) => ({ ...shop }))
@Form.create()
class details extends PureComponent {
  state = {};
  componentDidMount() {
    console.log(this.props, 'props');
    const { dispatch } = this.props;
    dispatch({
      type: 'shop/selectBusShopInfo',
      payload: {
        shopId: getParam('id'),
      },
    });
  }
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        const { dispatch } = this.props;
        dispatch({
          type: 'shop/toShopExamineConform',
          payload: {
            id: getParam('ids'),
            ...values,
          },
        });
      }
    });
  };
  render() {
    const { InfoData, shopEnterpriseInfos, shopEnterpriseCertificates } = this.props;
    const { getFieldDecorator } = this.props.form;
    console.log('shopEnterpriseCertificates.status:', shopEnterpriseCertificates);
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 3 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
    };
    return (
      <PageHeaderWrapper title={'店铺详情'}>
        <Spin spinning={false}>
          <Card bordered={false} style={{ marginTop: 20 }}>
            <Descriptions title={<CustomCard title="店铺信息" />}>
              <Descriptions.Item label="店铺编号">{InfoData.shopId}</Descriptions.Item>
              <Descriptions.Item label="店铺名称">{InfoData.name}</Descriptions.Item>
              <Descriptions.Item label="店铺联系人姓名">{InfoData.userName}</Descriptions.Item>
              <Descriptions.Item label="店铺客服电话">{InfoData.serviceTel}</Descriptions.Item>
              <Descriptions.Item label="店铺联系人电话">{InfoData.userTel}</Descriptions.Item>
              <Descriptions.Item label="店铺联系邮箱">{InfoData.userEmail}</Descriptions.Item>
              <Descriptions.Item label="店铺创建时间">{InfoData.registTime}</Descriptions.Item>
              <Descriptions.Item label="店铺审核状态">{status[InfoData.status]}</Descriptions.Item>
              <Descriptions.Item label="店铺审核时间">{InfoData.approvalTime}</Descriptions.Item>
              <Descriptions.Item label="店铺合同">
                <a href={InfoData.shopContractLink}>点击下载</a>
              </Descriptions.Item>
              <Descriptions.Item label="支付宝账号">{InfoData.zfbCode}</Descriptions.Item>
              <Descriptions.Item label="支付宝姓名">{InfoData.zfbName}</Descriptions.Item>
              <Descriptions.Item label="店铺logo">
                <img src={InfoData && InfoData.logo} style={{ width: 66, height: 66 }} />
              </Descriptions.Item>
              <Descriptions.Item label="店铺描述">{InfoData.description}</Descriptions.Item>
            </Descriptions>
            <Divider />
            <Descriptions title={<CustomCard title="企业信息" />}>
              <Descriptions.Item label="企业名称">{shopEnterpriseInfos.name}</Descriptions.Item>
              <Descriptions.Item label="企业注册资金">
                {shopEnterpriseInfos.registrationCapital}
              </Descriptions.Item>
              <Descriptions.Item label="营业执照号">
                {shopEnterpriseInfos.businessLicenseNo}
              </Descriptions.Item>
              <Descriptions.Item label="营业执照地址" span={1}>
                {shopEnterpriseInfos.licenseCityStr}
              </Descriptions.Item>
              <Descriptions.Item label="营业执照经营范围" span={2}>
                {shopEnterpriseInfos.businessScope}
              </Descriptions.Item>
              <Descriptions.Item label="营业执照照片">
                {shopEnterpriseInfos.licenseSrc}
                <img
                  src={
                    shopEnterpriseCertificates &&
                    shopEnterpriseCertificates.length > 1 &&
                    shopEnterpriseCertificates[0].image
                  }
                  style={{
                    width: 146,
                    height: 77,
                  }}
                />
              </Descriptions.Item>
              <Descriptions.Item label="生效时间">
                {shopEnterpriseInfos.licenseStart}
              </Descriptions.Item>
              <Descriptions.Item label="结束时间">
                {shopEnterpriseInfos.licenseEnd}
              </Descriptions.Item>
            </Descriptions>
            <Divider />
            <Descriptions>
              <Descriptions.Item label="法人姓名" span={1}>
                {shopEnterpriseInfos.realname}
              </Descriptions.Item>
              <Descriptions.Item label="法人身份证号" span={2}>
                {shopEnterpriseInfos.identity}
              </Descriptions.Item>
              <Descriptions.Item label="法人身份证信息" span={3}>
                <img
                  src={
                    shopEnterpriseCertificates &&
                    shopEnterpriseCertificates.length > 1 &&
                    shopEnterpriseCertificates[1].image
                  }
                  style={{ width: 146, height: 77, marginRight: 20 }}
                />
                <img
                  src={
                    shopEnterpriseCertificates &&
                    shopEnterpriseCertificates.length > 1 &&
                    shopEnterpriseCertificates[2].image
                  }
                  style={{ width: 146, height: 77 }}
                />
              </Descriptions.Item>
            </Descriptions>
            <Divider />
            <CustomCard title="平台电审" style={{ marginTop: 30, marginBottom: 20 }} />
            <Descriptions>
              <Descriptions.Item label="是否需要平台电审" span={3}>
                {InfoData.isPhoneExamination === 0 ? '不需要' : '需要'}
              </Descriptions.Item>
              <Descriptions.Item label="店铺冻结状态">
                {InfoData.isLocked === 0 ? '未冻结' : '冻结'}
              </Descriptions.Item>
            </Descriptions>
          </Card>
          <Card bordered={false} style={{ marginTop: 20, marginBottom: 30 }}>
            <CustomCard title="商品审核" style={{ marginTop: 30, marginBottom: 10 }} />
            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
              {InfoData && InfoData.status === 3 ? (
                <Form.Item label="审核状态">
                  {getFieldDecorator('status', {
                    rules: [
                      {
                        required: true,
                        message: '商品状态不能为空',
                      },
                    ],
                    initialValue: 5,
                  })(
                    <Radio.Group>
                      <Radio value={5}>审核通过</Radio>
                      <Radio value={4}>审核拒绝</Radio>
                    </Radio.Group>,
                  )}
                </Form.Item>
              ) : null}

              <Form.Item label="是否冻结店铺">
                {getFieldDecorator('isLocked', {
                  rules: [
                    {
                      required: true,
                      message: '不能为空',
                    },
                  ],
                  initialValue: InfoData.isLocked,
                })(
                  <Radio.Group>
                    <Radio value={0}>未冻结</Radio>
                    <Radio value={1}>冻结</Radio>
                  </Radio.Group>,
                )}
              </Form.Item>
              {InfoData && InfoData.status === 3 ? (
                <Form.Item label="审核理由">
                  {getFieldDecorator('reason', {
                    initialValue: InfoData.reason,
                  })(<TextArea />)}
                </Form.Item>
              ) : null}

              <Form.Item wrapperCol={{ span: 24 }}>
                <Button type="primary" htmlType="submit">
                  提交
                </Button>
              </Form.Item>
            </Form>
          </Card>
        </Spin>
      </PageHeaderWrapper>
    );
  }
}

export default details;
