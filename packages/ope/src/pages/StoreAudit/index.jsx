import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import {
  Card,
  message,
  Tooltip,
  Button,
  Form,
  Input,
  Select,
  DatePicker,
  Divider,
  Popconfirm,
  Badge,
} from 'antd';
import MyPageTable from '@/components/MyPageTable';
import CopyToClipboard from '@/components/CopyToClipboard';
import { onTableData, openLinkInNewWindow } from '@/utils/utils.js';
import moment from 'moment';

const { Option } = Select;
const { RangePicker } = DatePicker;
import { router } from 'umi';
import LZSearch from "@/components/Search";
@connect(({ shop }) => ({ ...shop }))
@Form.create()
export default class StoreAudit extends Component {
  state = {
    current: 1,
    data: {},
  };
  componentDidMount() {
    this.onList(1, 10);
    console.log(this.props);
  }
  onList = (pageNumber, pageSize, data = {}) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'shop/list',
      payload: {
        pageSize,
        pageNumber,
        ...data,
      },
    });
  };
  onExportShop = () => {
    const { dispatch } = this.props;
    const { current, data } = this.state;
    dispatch({
      type: 'shop/exportShop',
      payload: {
        pageSize: current,
        pageNumber: 10,
        ...data,
      },
    });
  };
  handleSubmit = (values = {}) => {
    this.setState({ current: 1 }); // 点击查询的时候从第一页查起
    if (values.createDate && values.createDate.length < 1) {
      values.createTimeStart = '';
      values.createTimeEnd = '';
      this.setState(
        {
          data: { ...values },
        },
        () => {
          this.onList(1, 10, { ...values });
        },
      );
    } else if (values.createDate) {
      values.createTimeStart = moment(values.createDate[0]).format('YYYY-MM-DD HH:mm:ss');
      values.createTimeEnd = moment(values.createDate[1]).format('YYYY-MM-DD HH:mm:ss');
      this.setState(
        {
          data: { ...values },
        },
        () => {
          this.onList(1, 10, { ...values });
        },
      );
    } else {
      this.setState(
        {
          data: { ...values },
        },
        () => {
          this.onList(1, 10, { ...values });
        },
      );
    }
    // e.preventDefault();
    // this.props.form.validateFields((err, values) => {
    //   console.log('Received values of form: ', values);
    //
    // });
  };
  //table 页数
  onPage = e => {
    const { current, data } = this.state;
    this.setState(
      {
        current: e.current,
      },
      () => {
        this.onList(this.state.current, 10, data);
      },
    );
  };
  confirm = e => {
    console.log('冻结');
  };
  render() {
    const { shopList, shopTotal } = this.props;
    const paginationProps = {
      current: this.state.current,
      pageSize: 10,
      total: shopTotal,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / 10)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };
    const columns = [
      {
        title: '店铺编号',
        dataIndex: 'shopId',
        key: 'shopId',
        render: shopId => <CopyToClipboard text={shopId} />,
        width: '10%',
      },
      {
        title: '店铺名称',
        dataIndex: 'name',
        key: 'name',
      },
      {
        title: '企业资质名称',
        dataIndex: 'enterpriseName',
        key: 'enterpriseName',
        width: '10%',
      },
      {
        title: '提交时间',
        dataIndex: 'updateTime',
        key: 'updateTime',
      },
      {
        title: '是否需要平台电审',
        dataIndex: 'isPhoneExamination',
        key: 'isPhoneExamination',
        render: isPhoneExamination => {
          return (
            <>
              {isPhoneExamination === 1 ? (
                <Badge status="success" text="是" />
              ) : (
                <Badge status="default" text="否" />
              )}
            </>
          );
        },
      },
      {
        title: '审核状态',
        dataIndex: 'status',
        key: 'status',
        render: status => {
          return (
            <>
              {status === 3 ? <span style={{ color: '#FA8C16' }}>待审核</span> : null}
              {status === 4 ? <span style={{ color: '#000000' }}>审核拒绝</span> : null}
              {status === 5 ? <span style={{ color: '#52C41A' }}>审核通过</span> : null}
            </>
          );
        },
      },
      {
        title: '店铺冻结状态',
        dataIndex: 'isLocked',
        render: isLocked => {
          return (
            <>
              {isLocked === 0 ? (
                <span style={{ color: '#000000' }}>未冻结</span>
              ) : (
                <span style={{ color: '#000000' }}>已冻结</span>
              )}
            </>
          );
        },
      },
      {
        title: '操作',
        // dataIndex: 'operate',
        key: 'operate',
        render: e => {
          return (
            <div>
              <Button
                type="link"
                onClick={() => openLinkInNewWindow(`/StoreAudit/list/details?id=${e.shopId}&ids=${e.id}`)}
                // onClick={() => router.push(`/StoreAudit/list/details?id=${e.shopId}&ids=${e.id}`)}
                style={{ padding: 0 }}
              >
                查看
              </Button>
            </div>
          );
        },
      },
    ];
    const { getFieldDecorator } = this.props.form;
    return (
      <PageHeaderWrapper>
        <Card bordered={false}>
          <LZSearch needExport needReset source={'店铺审核'} exportData={this.handleExportData}
                  handleFilter={this.handleSubmit} />
          <MyPageTable
            onPage={this.onPage}
            paginationProps={paginationProps}
            dataSource={onTableData(shopList)} //list
            columns={columns}
          />
        </Card>
      </PageHeaderWrapper>
    );
  }
}
