/**
 * 不是很重要的UI关注点逻辑
 */
import request from '@/services/baseService';


//-------------------------- ajax请求方法开始 -----------------------//

/**
 * 创建banner的方法
 * @param {*} reqData : banner表单输入数据
 * @param {*} sucCb : 创建成功之后的回调方法
 * @param {bool} isModify : 表示是否是修改数据
 */
export const createNewBannerHandler = (reqData, sucCb, isModify) => {
  const url = isModify ? '/hzsx/competenceCenter/modifyCompetenceCenterBanner' : '/hzsx/competenceCenter/addCompetenceCenterBanner';
  request(url, reqData, 'post').then(resData => {
    sucCb && sucCb(resData);
  });
}

/**
 * 请求banner列表和商品列表
 * @param {*} props 
 * @param {*} listReq 
 * @param {*} tabActiveKey 
 * @param {*} sucCb 
 * @param {*} reqData 
 */
export const passiveFetchHandler = (props, listReq, tabActiveKey, sucCb, reqData={}) => {
  const tabConfObj = tabKeyConfigs[tabActiveKey]; // 获取到标签栏的配置数据
  if (!tabConfObj) return;

  const reqParams = {
    ...reqData,
    type: getType(props),
    ...getListReqParams(listReq, tabActiveKey),
  };  // 合成请求参数

  request(tabConfObj.url, reqParams, 'post').then(resData => {
    sucCb && sucCb(resData);
  })
}

/**
 * 批量添加能力中心的商品
 * @param {Array} products : 所选中的商品列表
 * @param {String} type : 能力中心类型，表示是全新 还是 二手
 */
export const batchCreateProductHandler = (products, type, sucCb) => {
  if (Object.prototype.toString.call(products) !== '[object Array]') return;
  const reqParam = products.map(obj => (obj.type = type, obj.sale = obj.price, obj));
  request('/hzsx/competenceCenter/saveCenterPrdouct', reqParam, 'post').then(resData => {
    sucCb && sucCb(resData);
  });
}

/**
 * 请求直购商品的接口
 * @param {*} reqData : 传给接口的数据
 * @param {*} sucCb : 请求成功的回调方法
 */
export const fetchDirectProductListHandler = (reqData, type, sucCb) => {
  const reqParams = reqData || { pageNumber: 1, pageSize: 10 };
  reqParams.type = type;
  request('/hzsx/examineProduct/ableProductDirect', reqParams, 'get').then(resData => {
    sucCb && sucCb(resData);
  });
}

/**
 * 删除商品
 * @param {*} id : 商品id
 * @param {*} sucCb 
 */
export const deleteProductHandler = (id, sucCb) => {
  request('/hzsx/competenceCenter/deleteCenterPrdouct', { id }, 'get').then(resData => {
    sucCb && sucCb(resData);
  });
}

/**
 * 删除banner
 * @param {*} id ： banner的ID
 * @param {*} sucCb 
 */
export const deleteBannerHandler = (id, sucCb) => {
  request('/hzsx/competenceCenter/deleteCompetenceCenterBanner', { id }, 'get').then(resData => {
    sucCb && sucCb(resData);
  });
}

/**
 * 对商品进行排序
 * @param {*} data : 所需要传的数据
 * @param {*} sucCb 
 */
export const reorderProductHandler = (data, props, sucCb) => {
  const reqData = {
    ...data,
    type: getType(props),
  };
  request('/hzsx/competenceCenter/updateCenterPrdouctSort', reqData, 'post').then(resData => {
    sucCb && sucCb(resData);
  })
}
//-------------------------- ajax请求方法结束 ---------------------//





//-------------------------- 一些变量开始 --------------------//
export const BANNER_KEY = 'banner_key'; // banner模式的key
export const PRODUCT_KEY = 'product_key'; // 商品模块的key

// 路由name和type之间的对应关系
export const urlName2Type = {
  secondHand: 'OLD',
  newPhone: 'NEW',
};

// tab栏的配置数据
export const tabKeyConfigs = {
  [BANNER_KEY]: {
    url: '/hzsx/competenceCenter/queryCompetenceCenterBannerPage',
  },
  [PRODUCT_KEY]: {
    url: '/hzsx/competenceCenter/queryCompetenceCenterProductPage',
  }
};

export const defaultListRes = {
  [BANNER_KEY]: { records: [] },
  [PRODUCT_KEY]: { records: [] },
};

// 模块定义
export const tabs = [
  { ui: 'Banner', key: BANNER_KEY },
  { ui: '商品', key: PRODUCT_KEY },
]

// 请求列表数据的默认参数
export const defaultListReqParams = {
  [BANNER_KEY]: {
    pageNumber: 1,
    pageSize: 10,
  },
  [PRODUCT_KEY]: {
    pageNumber: 1,
    pageSize: 10,
  },
};
//-------------------------- 一些变量结束 ---------------------//





//-------------------------- 一些工具方法开始 ---------------------//

/**
 * 获取到url中的name信息
 * @param {*} props 
 */
export const getUrlName = (props={}) => {
  const { route={} } = props;
  const { name } = route;
  return name;
};

// 获取到接口所需要的type参数
export const getType = props => {
  const routeName = getUrlName(props); // 获取到当前路由的name
  return urlName2Type[routeName]; // 获取到该route name所对应的type
};

/**
 * 获取到接口所返回的响应数据
 * @param {*} resList : state状态
 * @param {*} key : 键名
 */
export const getDataSource = (resList, key) => {
  const keyRes = (resList && resList[key]) || {};
  const datas = keyRes.records || [];
  return datas;
};

export const getListReqParams = (reqObj, key) => {
  const keyObj = (reqObj && reqObj[key]) || {};
  return keyObj;
}

/**
 * 生成pagination配置数据
 * @param {*} listParamsObj : 列表接口请求参数挂载点
 * @param {*} key : tab的名字
 */
export const generatePagination = (listParamsObj, resList, key) => {
  const resObj = resList[key] || {};

  const { total } = resObj; // 获取到数据总条数
  const { pageNumber=1, pageSize=10 } = getListReqParams(listParamsObj, key);

  return {
    current: pageNumber,
    pageSize,
    total,
    showTotal: t => `共有${t}条数据`,
    showQuickJumper: true,
    pageSizeOptions: ['5', '10', '20'],
    showSizeChanger: true,
  };
};
//-------------------------- 一些工具方法结束 ---------------------//