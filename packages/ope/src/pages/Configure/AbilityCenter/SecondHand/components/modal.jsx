/**
 * 模态框组件，即用于新增Banner，也用于新增商品
 */
import React, { Component } from 'react';
import { Modal, Form, Select, Upload, Input, Radio, Icon, message, Table, Button } from 'antd';
import PropTypes from 'prop-types';
import {
  BANNER_KEY,
  PRODUCT_KEY,
  createNewBannerHandler,
  batchCreateProductHandler,
  fetchDirectProductListHandler,
} from '../notDirectUiLogic';
import { uploadUrl } from "@/config";
import { getToken } from "@/utils/localStorage";
import commonApi from "@/services/common";
import styles from './index.module.less';

const { Option } = Select;

const delayTime = 600; // 隐藏弹窗的延时时间
const defaultModifyObj = {
  title: '', // banner名称
  link: '', // 跳转地址
  status: true, // banner是否生效的状态
  imgSrc: '', // banner的图片链接
  sort: '', // banner的序号
  channel: [], // banner的上架渠道
}; // 关于banner对象的数据建模

@Form.create()
class CreateModal extends Component {
  state = {
    bannerVO: this.props.modifyObj || defaultModifyObj,
    fileList: [], // 新增banner时所上传的文件列表数据
    channelData: [], // 新增banner时的渠道列表数据
    imgVisible: false, // 预览图片
    // 商品模块
    selectedRowKeys: [], // 所选中的商品IDs
    selectedRows: [], // 所选中的商品列表
    keyWord: '', // 搜索值
    pageNumber: 1, // 搜索商品时的页码
    pageSize: 10, // 搜索商品时每页的数据条数
    directProductApiRes: {}, // 请求/ableProductDirect接口所返回的数据
  }

  // 进行初始化工作
  resetAllState = () => {
    // this.props.form.setFieldsValue(defaultModifyObj);
    this.setState({
      // bannerVO: defaultModifyObj,
      // fileList: [],
      imgVisible: false,
      selectedRowKeys: [],
      selectedRows: [],
      keyWord: '',
      directProductApiRes: {},
    })
  }

  // 加载默认数据
  UNSAFE_componentWillMount() {
    // this.props.form.setFieldsValue({ ...defaultModifyObj }); // 组件一挂载的时候就设置上一个默认值
    this.fetchChannelListHandler(); // 由于默认是Banner这个tab，所以默认加载渠道列表
  }

  // 在这里进行数据加载
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.modifyObj !== this.props.modifyObj) { // 当修改对象发生了变化的时候，需要将其同步到state中
      const nextModifyObj = nextProps.modifyObj;
      const obj = nextModifyObj && nextModifyObj.id ? nextModifyObj : defaultModifyObj;
      this.props.form.setFieldsValue({
        title: obj.title,
        status: obj.status,
        link: obj.link,
        imgSrc: obj.imgSrc,
        sort: obj.sort,
        channel: obj.channel,
      });
      if (nextModifyObj && nextModifyObj.imgSrc) {
        const newFileList = [ { uid: 1, url: nextModifyObj.imgSrc || '' } ];
        this.setState({ fileList: newFileList });
      } else {
        this.setState({ fileList: [] });
      }
    }
    // nextProps.tabKey !== this.props.tabKey
    if (nextProps.visible !== this.props.visible) { // 当菜单栏发生切换的时候
      if (nextProps.tabKey === BANNER_KEY && (!this.state.channelData || !this.state.channelData.length)) { // 表明是banner菜单项，且当前还没有加载过banner列表数据
        this.fetchChannelListHandler();
      }
      if (nextProps.tabKey === PRODUCT_KEY && (!this.directProductApiRes || !this.directProductApiRes.records)) { // 表明是商品菜单项，且当前还没有加载过商品列表数据
        this.searchProductHandler();
      }
    }
  }

  // 加载渠道列表数据
  fetchChannelListHandler = () => {
    commonApi.selectPlateformChannel().then(res => {
      this.setState({ channelData: res });
    });
  }

  formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  }

  uploadButton = () => (
    <div>
      <Icon type="plus" />
      <div className="ant-upload-text">上传</div>
    </div>
  )

  /**
   * 提交的处理方法，先进行数据校验，校验通过后在进行提交
   */
  submitHandler = () => {
    if (this.submiting) return;
    this.submiting = true;
    let that = this;
    this.props.form.validateFields((err, formObj) => {
      if (!err) { // 表示校验通过了
        const { tabKey } = this.props; // 获取到当前处于焦点状态tab的键名
        if (tabKey === BANNER_KEY) { // 进行创建banner的提交
          // const isModify = !!this.props.modifyObj; // 表示是否是修改
          const isModify = this.props.modifyObj && this.props.modifyObj.id; // 表示是修改
          const sucCbHandler = () => {
            message.success(`${isModify ? '修改' : '新建'}banner成功`);
            this.props.refreshListCb && this.props.refreshListCb();
            const suc = () => {
              this.hideModalHandler();
              that.submiting = false;
            }
            setTimeout(suc, delayTime);
          }
          const channels = formObj.channel;
          // const channels = this.state.bannerVO.channel;
          const init = this.props.modifyObj || {};
          const submitData = {
            ...init,
            ...formObj,
            // ...this.state.bannerVO,
            imgSrc: typeof formObj.imgSrc === 'string' ? formObj.imgSrc : formObj.imgSrc.file.response.data,
            // imgSrc: this.state.fileList[0],
            type: this.props.type,
            channel: Object.prototype.toString.call(channels) === '[object Array]' ? channels.join('&') : channels,
          }
          createNewBannerHandler(submitData, sucCbHandler, isModify);
        } else {  // 进行创建商品的提交

        }
      }
    });
  }


  // 上传的回调
  handleUploadChange = ({ file, fileList }) => {
    const filelist = [...fileList] || []; // 文件列表数据
    if (file.status == 'done') {
      filelist.map(item => {
        const imgSrc = item.response.data;
        this.props.form.setFieldsValue({ imgSrc });
        // const newBannerVO = { ...this.state.bannerVO, imgSrc };
        // this.setState({ bannerVO: newBannerVO })
      });
    }
    this.setState({ fileList });
  }

  // 下载图片的回调
  onDownload = () => {
    // const { bannerVO={} } = this.state;
    // const { imgSrc } = bannerVO;
    const imgSrc = this.props.form.getFieldValue('imgSrc');
    return window.open(imgSrc)
  }

  // 上传后删除的回调
  onRemove = () => {
    this.setState({ fileList: [], imgSrc: '' });
  }

  // 上传后预览的回调
  handlePreview = () => {
    this.setState({ imgVisible: true });
  }

  // table组件选择数据的时候触发
  tableSelctHandler = (selectedRowKeys, selectedRows) => {
    this.setState({ selectedRowKeys, selectedRows })
  }

  // 改变搜索值的时候触发
  searchValueChangeHandler = event => {
    this.setState({ keyWord: event.target.value });
  }

  // 点击查询的时候触发
  searchProductHandler = () => {
    const { keyWord, pageNumber, pageSize } = this.state;
    const reqData = { keyWord, pageNumber, pageSize }; // 所生成的新的请求参数
    const sucSearchHandler = data => { this.setState({ directProductApiRes: data }); };
    fetchDirectProductListHandler(reqData, this.props.type, sucSearchHandler);
  }

  // 重置的时候触发
  resetHandler = () => {
    this.setState({ keyWord: '' }, this.searchProductHandler); // 初始化搜索值，并且使用新的搜索值去获取一下商品列表数据
  }

  // 新增商品的监听方法
  createProductHandler = () => {
    if (this.createProducting) return; // 如果当前已经在请求该接口的话直接返回
    const successCreateHandler = () => {
      message.success('操作成功');
      this.createProducting = false;
      this.props.refreshListCb(); // 拉取数据
      setTimeout(this.hideModalHandler, delayTime);
    };  // 成功创建商品时的回调方法
    this.createProducting = true;
    batchCreateProductHandler(this.state.selectedRows, this.props.type, successCreateHandler);
  }

  // 设置页码的时候触发
  changePageNumberHandler = pageNumber => {
    this.setState({ pageNumber }, this.searchProductHandler);
  }

  // 改变每页条数的时候触发
  changePageSizeHandler = (_, pageSize) => {
    this.setState({ pageNumber: 1, pageSize }, this.searchProductHandler);
  }

  /**
   * 改变bannerVO的值
   * @param {*} key 
   * @param {*} val 
   */
  changeBannerVoKeyVal = (key, val) => {
    this.props.setFieldsValue({ [key]: val });
    // const newBannerVO = { ...this.state.bannerVO, [key]: val };
    // this.setState({ bannerVO: newBannerVO });
  }

  // 隐藏弹窗的方法
  hideModalHandler = () => {
    this.resetAllState();
    this.props.hideModal && this.props.hideModal();
  }

  // 渲染modal的底部footer
  renderFooter = () => {
    if (this.props.tabKey === BANNER_KEY) {
      return (
        <div className={styles.modalFooter}>
          <div className={styles.flexg} />
          <Button onClick={this.hideModalHandler}>取消</Button>
          <Button type="primary" onClick={this.submitHandler}>确定</Button>
        </div>
      );
    } else {
      return null;
    }
  }

  render() {
    const { visible, tabKey } = this.props;
    const { getFieldDecorator } = this.props.form;
    // const { bannerVO={} } = this.state;
    const { fileList, channelData, directProductApiRes={}, pageNumber, pageSize } = this.state;

    const { records:directProducts=[], total } = directProductApiRes;
    const pagination = {
      current: pageNumber,
      pageSize,
      pageSizeOptions: ['10', '20', '30', '50', '100'],
      showQuickJumper: true,
      total,
      showTotal: t => `共有${t}条数据`,
      onChange: this.changePageNumberHandler,
      onShowSizezChange: this.changePageSizeHandler,
    }; // 分页器的配置数据

    return (
      <Modal
        title={tabKey === BANNER_KEY ? '新增Banner' : '新增商品'}
        visible={visible}
        footer={this.renderFooter()}
        onCancel={this.hideModalHandler}
        wrapClassName={styles.wrapperClsName}
      >
        {
          tabKey === BANNER_KEY ?
          <section>
            {/** 这是新增banner的弹窗 */}
            <Form {...this.formItemLayout}>
              <Form.Item label="banner名称">
                {getFieldDecorator('title', {
                  rules: [
                    {
                      required: true,
                      message: `benner名称不能为空`,
                    },
                  ],
                  // initialValue: bannerVO.title,
                })(<Input placeholder="请输入banner名称" />)}
                { /** value={bannerVO.title} onChange={event => this.changeBannerVoKeyVal('title', event.target.value)} */}
              </Form.Item>

              <Form.Item label="跳转地址">
                {getFieldDecorator('link', {
                  rules: [
                    {
                      required: true,
                      message: '跳转地址不能为空',
                    },
                  ],
                  // initialValue: bannerVO.link,
                })(<Input placeholder="请输入跳转地址" />)}
                {/** value={bannerVO.link} onChange={event => this.changeBannerVoKeyVal('link', event.target.value)} */}
              </Form.Item>

              <Form.Item label="排序规则">
                {getFieldDecorator('sort', {
                  rules: [
                    {
                      required: true,
                      message: '排序规则不能为空',
                    },
                  ],
                  // initialValue: bannerVO.sort,
                })(<Input placeholder="请输入排序规则" />)}
                {/** value={bannerVO.sort} onChange={event => this.changeBannerVoKeyVal('sort', event.target.value)} */}
              </Form.Item>

              <Form.Item label="是否生效">
                {getFieldDecorator('status', {
                  initialValue: true,
                })(
                  <Radio.Group>
                    <Radio value={true}>生效</Radio>
                    <Radio value={false}>不生效</Radio>
                  </Radio.Group>
                )}
                {/** value={bannerVO.status} onChange={event => this.changeBannerVoKeyVal('status', event.target.value)} */}
              </Form.Item>

              <Form.Item label="上传图片">
                {getFieldDecorator('imgSrc', {
                  rules: [
                    {
                      validator: (rule, value, callback) => {
                        // if (!this.state.bannerVO.imgSrc) {
                        //   callback('图片不能为空');
                        // }
                        if (!value) {
                          callback('图片不能为空');
                        }
                        callback();
                      },
                    },
                  ],
                  // initialValue: bannerVO.imgSrc,
                })(
                  <Upload
                    accept="image/*"
                    action={uploadUrl}
                    headers={{
                      token: getToken()
                    }}
                    listType="picture-card"
                    fileList={fileList}
                    onPreview={this.handlePreview} // 预览
                    onRemove={this.onRemove} // 删除
                    onDownload={this.onDownload}
                    onChange={this.handleUploadChange}
                  >
                    {fileList.length >= 1 ? null : this.uploadButton()}
                  </Upload>
                )}
              </Form.Item>

              <Form.Item label="上架渠道">
                {getFieldDecorator('channel', {
                  rules: [
                    {
                      required: true,
                      message: '上架渠道不能为空',
                    },
                  ],
                  // initialValue: bannerVO.channel
                })(
                  <Select
                    // value={bannerVO.channel}
                    mode="multiple"
                    style={{ width: '100%' }}
                    placeholder="请选择上架渠道"
                    // onChange={val => this.changeBannerVoKeyVal('channel', val)}
                  >
                    {
                      channelData.map((item, sign) => {
                        return (
                          <Option key={item.appId} value={item.channelId} > {item.channelName} </Option>
                        )
                      })
                    }
                  </Select>
                )}
              </Form.Item>
            </Form>
          </section>
          :
          <section>
            {/** 新增商品的弹窗 */}
            <div className={styles.topSearchArea}>
              <span className={styles.noS}>查询：</span>
              <Input value={this.state.keyWord} onPressEnter={this.searchProductHandler} onChange={this.searchValueChangeHandler} placeholder="请输入查询内容" className={styles.searchInput} />
              <Button onClick={this.searchProductHandler} type="primary" disabled={!this.state.keyWord}>查询</Button>
              <Button onClick={this.resetHandler} disabled={!this.state.keyWord}>重置</Button>
              <Button onClick={this.createProductHandler} disabled={!this.state.selectedRowKeys.length}>新增</Button>
            </div>
            <Table
              dataSource={directProducts}
              rowSelection={{ onChange: this.tableSelctHandler, selectedRowKeys: this.state.selectedRowKeys }}
              columns={[
                { title: '商品编号', dataIndex: 'productId', key: 'productId' },
                { title: '商品名称', dataIndex: 'productName', key: 'productName' },
                { title: '店铺名称', dataIndex: 'shopName', key: 'shopName' },
              ]}
              rowKey={record => record.productId}
              pagination={pagination}
            />
          </section>
        }

        <Modal
          visible={this.state.imgVisible}
          footer={null}
          onCancel={() => this.setState({ imgVisible: false })}
          width={600}
        >
          {/** this.state.bannerVO.imgSrc || null */}
          <img src={this.props.form.getFieldValue('imgSrc') || null} alt="" style={{ width: '100%', paddingTop: 15 }} />
        </Modal>
      </Modal>
    );
  }
}

CreateModal.propTypes = {
  visible: PropTypes.bool, // 是否显示模态框
  tabKey: PropTypes.string, // tab栏的标题
  hideModal: PropTypes.func, // 隐藏弹窗的回调
  type: PropTypes.string, // 表示是新机易购还是二手
  modifyObj: PropTypes.object, // 修改的时候会把这个值传下来
  refreshListCb: PropTypes.func, // 重新请求列表数据的回调方法
};

export default CreateModal;
