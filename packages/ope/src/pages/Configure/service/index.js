import request from "@/services/baseService";

export default {
  getBannerData:(data)=>{
    return request(`/hzsx/index/getIndexBannerListByPage`, data, 'get')
  },
  getIconData:(data)=>{
    return request(`/hzsx/index/getIndexIconListByPage`, data, 'get')
  },
  getSealData:(data)=>{
    return request(`/hzsx/index/getIndexLoinBannerListByPage`, data, 'get')
  },
  getCubeData:(data)=>{
    return request(`/hzsx/index/pageIndexWaterfallBanner`, data, 'get')
  },
  addBanner:(data)=>{
    return request(`/hzsx/index/saveIndexBannerById`, data)
  },
  deleteBanner:(data)=>{
    return request(`/hzsx/index/delIndexBannerById`, data, 'get')
  },
  addSeal:(data)=>{
    return request(`/hzsx/index/saveIndexLoinBanner`, data)
  },
  deleteSeal:(data)=>{
    return request(`/hzsx/index/delIndexLoinBanner`, data, 'get')
  },
  addCube:(data)=>{
    return request(`/hzsx/index/saveIndexWaterfallBanner`, data)
  },
  deleteCube:(data)=>{
    return request(`/hzsx/index/deleteIndexWaterfallBanner`, data, 'get')
  },
  updateBanner:(data)=>{
    return request(`/hzsx/index/updateIndexBannerById`, data)
  },
  updateSeal:(data)=>{
    return request(`/hzsx/index/updateIndexLoinBannerById`, data)
  },
  updateCube:(data)=>{
    return request(`/hzsx/index/updateIndexWaterfallBanner`, data)
  },
}
