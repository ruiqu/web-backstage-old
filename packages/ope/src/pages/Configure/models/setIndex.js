import * as servicesApi from '../service/index';

export default {
  spacename: 'setIndex',
  state: {
    banner: [],
    icon: [],
    seal: [],
    cube: [],
  },
  effects: {
    // banner
    *getBanner({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.getBannerData, payload);
      if (res.data.msg === '操作成功') {
        yield put({
          type: 'saveBanner',
          data: res.data.data,
        });
      }
      if (callback) callback(res);
    },
    // icon
    *getIcon({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.getIconData, payload);
      if (res.data.msg === '操作成功') {
        yield put({
          type: 'saveIcon',
          data: res.data.data,
        });
      }
      if (callback) callback(res);
    },
    // 腰封
    *getSeal({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.getSealData, payload);
      
      if (res.data.msg === '操作成功') {
        yield put({
          type: 'saveSeal',
          data: res.data.data,
        });
      }
      if (callback) callback(res);
    },
    // 豆腐块
    *getCube({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.getCubeData, payload);
      if (res.data.msg === '操作成功') {
        yield put({
          type: 'saveCube',
          data: res.data.data,
        });
      }
      callback(res);
    },

    //新增

    //添加banner
    *addBanner({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.addBanner, payload);
      if (callback) callback(res);
    },
    //删除banner
    *deleteBanner({ payload, callback }, { call }) {
      let res = yield call(servicesApi.deleteBanner, payload);
      if (callback) callback(res);
    },
    //添加ICon
    *addIcon({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.addIcon, payload);
      if (callback) callback(res);
    },
    //删除icon
    *deleteIcon({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.deleteIcon, payload);
      if (callback) callback(res);
    },
    //添加腰封
    *addSeal({ payload, callback }, { call }) {
      let res = yield call(servicesApi.addSeal, payload);
      if (callback) callback(res);
    },
    *deleteSeal({ payload, callback }, { call }) {
      let res = yield call(servicesApi.deleteSeal, payload);
      if (callback) callback(res);
    },
    //添加豆腐块
    *addCube({ payload, callback }, { call }) {
      let res = yield call(servicesApi.addCube, payload);
      callback(res);
    },
    *deleteCube({ payload, callback }, { call }) {
      let res = yield call(servicesApi.deleteCube, payload);
      callback(res);
    },

    //修改

    *updateBanner({ payload, callback }, { call }) {
      let res = yield call(servicesApi.updateBanner, payload);
      callback(res);
    },
    *updateIcon({ payload, callback }, { call }) {
      let res = yield call(servicesApi.updateIcon, payload);
      callback(res);
    },
    *updateSeal({ payload, callback }, { call }) {
      let res = yield call(servicesApi.updateSeal, payload);
      callback(res);
    },
    *updateCube({ payload, callback }, { call }) {
      let res = yield call(servicesApi.updateCube, payload);
      callback(res);
    },
    //获取上架信息
    *getChannel({ payload, callback }, { call, put }) {
      const response = yield call(servicesApi.getChannel, payload);
      if (response.data.code == 1) {
        callback(response.data.data);
      }
    }
  },
  reducers: {
    saveBanner(state, payload) {
      let _state = JSON.parse(JSON.stringify(state));
      _state.banner = payload.data.data.records;
      return _state;
    },
    saveIcon(state, payload) {
      let _state = JSON.parse(JSON.stringify(state));
      _state.icon = payload.data.data.records;
      return _state;
    },
    saveSeal(state, payload) {
      let _state = JSON.parse(JSON.stringify(state));
      _state.seal = payload.data.records;
      return _state;
    },
    saveCube(state, payload) {
      let _state = JSON.parse(JSON.stringify(state));
      _state.cube = payload.data.records;
      return _state;
    },
    initProps(state, payload) {
      let _state = JSON.parse(JSON.stringify(state));
      _state.banner = [];
      _state.icon = [];
      _state.seal = [];
      _state.cube = [];
      return _state;
    },
  },
};
