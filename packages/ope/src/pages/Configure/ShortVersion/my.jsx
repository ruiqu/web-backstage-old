import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import {
  Card,
  Form,
  Input,
  Button,
  Icon,
  message,
  Modal,
  Popconfirm,
  Descriptions,
  Select,
} from 'antd';
import myService from './service';
import { getParam, onTableData } from '@/utils/utils';
import CustomCard from '@/components/CustomCard';
import AntTable from '@/components/AntTable';
const { Option } = Select;
@Form.create()
export default class my extends Component {
  state = {
    title: '',
    visible: false,
    MY_ORDER: [],
    MY_SERVICE: [],
  };
  componentDidMount() {
    this.onListIndex();
  }
  //查询全局数据
  onListIndex = () => {
    myService.listMy({ channelId: getParam('id') }).then(res => {
      if (res) {
        this.setState({
          MY_ORDER: res.MY_ORDER,
          MY_SERVICE: res.MY_SERVICE,
        });
      }
    });
  };
  //打开Modal框
  showModal = (title, type) => {
    this.setState({
      visible: true,
      title,
      type,
    });
  };
  //关闭Modal框
  handleCancel = e => {
    this.setState({
      visible: false,
    });
  };
  //增加按钮
  onAddButton = (name, type) => {
    return (
      <Button
        type="dashed"
        onClick={() => this.showModal(name, type)}
        style={{ width: '100%', marginBottom: 20 }}
      >
        <Icon type="plus" />
        {name}
      </Button>
    );
  };
  //删除列表
  onPageElementConfigDelete = id => {
    myService.pageElementConfigDelete({ id }).then(res => {
      if (res) {
        message.success('删除成功');
        this.onListIndex();
      }
    });
  };
  //新增列表
  onPageElementConfigAdd = (values, type) => {
    myService.pageElementConfigAdd({ ...values, type, channelId: getParam('id') }).then(res => {
      if (res) {
        message.success('新增成功');
        this.onListIndex();
        this.handleCancel();
      }
    });
  };
  //提交
  handleOk = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        const { type, title } = this.state;
        if (title === '新增订单') {
          const types = {
            ORDER_WAIT_PAY: '待付款',
            ORDER_WAIT_DELIVERY: '待发货',
            ORDER_WAIT_RECEIVE: '待收货',
            ORDER_RENTING: '租用中',
            ORDER_WAIT_SETTLE: '待结算',
            ORDER_OVERDUE: '已逾期',
          };
          values.describeInfo = types[values.extCode];
        }
        if (values.describeInfo === '优惠券') values.extCode = 'COUPON';

        this.onPageElementConfigAdd(values, type);
      }
    });
  };
  render() {
    const { title, visible, MY_ORDER, MY_SERVICE } = this.state;
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 },
      },
    };
    const columns = [
      {
        title: '素材ID',
        dataIndex: 'materialCenterItemId',
        width: '20%',
        align: 'center',
      },
      {
        title: '名称',
        dataIndex: 'describeInfo',
        width: '20%',
        align: 'center',
      },
      {
        title: '跳转链接',
        dataIndex: 'link',
        width: '20%',
        align: 'center',
      },
      {
        title: '排序',
        dataIndex: 'sortNum',
        width: '20%',
        align: 'center',
      },
      {
        title: '操作',
        dataIndex: 'id',
        width: '20%',
        align: 'center',
        render: e => (
          <Popconfirm title="你确定删除吗?" onConfirm={() => this.onPageElementConfigDelete(e)}>
            <Button type="link">删除</Button>
          </Popconfirm>
        ),
      },
    ];

    return (
      <PageHeaderWrapper title={false}>
        <Card bordered={false} style={{ marginBottom: 20 }}>
          <Descriptions title={<CustomCard title="基本信息" />}>
            <Descriptions.Item label="渠道">{getParam('channelName')}</Descriptions.Item>
            <Descriptions.Item label="位置">{getParam('name')}</Descriptions.Item>
          </Descriptions>
        </Card>
        <Card bordered={false} style={{ marginBottom: 20 }}>
          <CustomCard title="我的订单" />
          <AntTable dataSource={onTableData(MY_ORDER)} columns={columns} />
          {this.onAddButton('新增订单', 'MY_ORDER')}
          <CustomCard title="我的服务" />
          <AntTable dataSource={onTableData(MY_SERVICE)} columns={columns} />
          {this.onAddButton('新增服务', 'MY_SERVICE')}
        </Card>
        <Modal
          destroyOnClose
          title={title}
          visible={visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Form {...formItemLayout}>
            <Form.Item label="素材ID">
              {getFieldDecorator('materialCenterItemId', {
                rules: [
                  {
                    required: true,
                    message: '请输入',
                  },
                ],
              })(<Input placeholder="请输入" />)}
            </Form.Item>
            {title === '新增订单' && (
              <Form.Item label="名称">
                {getFieldDecorator('extCode', {
                  rules: [
                    {
                      required: true,
                      message: '名称不能为空',
                    },
                  ],
                })(
                  <Select placeholder="全部">
                    <Option value="ORDER_WAIT_PAY">待付款</Option>
                    <Option value="ORDER_WAIT_DELIVERY">待发货</Option>
                    <Option value="ORDER_WAIT_RECEIVE">待收货</Option>
                    <Option value="ORDER_RENTING">租用中</Option>
                    <Option value="ORDER_WAIT_SETTLE">待结算</Option>
                    <Option value="ORDER_OVERDUE">已逾期</Option>
                  </Select>,
                )}
              </Form.Item>
            )}
            {title === '新增服务' && (
              <Form.Item label="名称">
                {getFieldDecorator('describeInfo', {
                  rules: [
                    {
                      required: true,
                      message: '请输入',
                    },
                  ],
                })(<Input placeholder="请输入" />)}
              </Form.Item>
            )}
            <Form.Item label="跳转链接">
              {getFieldDecorator('link', {
                rules: [
                  {
                    required: true,
                    message: '请输入',
                  },
                ],
              })(<Input placeholder="请输入" />)}
            </Form.Item>
            <Form.Item label="排序">
              {getFieldDecorator('sortNum', {
                rules: [
                  {
                    required: true,
                    message: '请输入',
                  },
                ],
              })(<Input placeholder="请输入" />)}
            </Form.Item>
          </Form>
        </Modal>
      </PageHeaderWrapper>
    );
  }
}