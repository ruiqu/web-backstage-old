import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import {
  Card,
  Form,
  Input,
  Row,
  Col,
  Button,
  Icon,
  message,
  Modal,
  Popconfirm,
  Descriptions,
} from 'antd';
import homeService from './service';
import { getParam, onTableData } from '@/utils/utils';
import CustomCard from '@/components/CustomCard';
import AntTable from '@/components/AntTable';
import Tab from './component/tab';
import Tab1 from './component/tab1';
@Form.create()
export default class home extends Component {
  state = {
    hotList: [], //热门板块列表
    hotType: false,
    bannerType: false,
    mainPictureType: false,
    sidePictureType: false,
    mainTitleType: false,
    sideTitleType: false,
    INDEX_BANNER: [], //横幅列表
    SPECIAL_AREA_MAIN: [], //主图列表
    MY_TYPE: [], //主图列表
    MY_LUNBO: [], //主图列表
    SPECIAL_AREA_SUB: [], //副图列表
    SPECIAL_TITLE_MAIN: {}, //主标题
    SPECIAL_TITLE_SUB: {}, //副标题
    visible: false,
    title: '',
    type: null,
  };
  componentDidMount() {
    this.onListIndex();
    this.onHotSearchConfigList();
  }
  //查询全局数据
  onListIndex = () => {
    homeService.listIndex({ channelId: getParam('id') }).then(res => {
      if (res) {
        this.setState({
          INDEX_BANNER: res.INDEX_BANNER,
          SPECIAL_AREA_MAIN: res.SPECIAL_AREA_MAIN,
          MY_TYPE: res.MY_TYPE,
          MY_LUNBO: res.MY_LUNBO||[],
          SPECIAL_AREA_SUB: res.SPECIAL_AREA_SUB,
          SPECIAL_TITLE_MAIN:
            (res.SPECIAL_TITLE_MAIN &&
              res.SPECIAL_TITLE_MAIN.length &&
              res.SPECIAL_TITLE_MAIN[0]) ||
            {},
          SPECIAL_TITLE_SUB:
            (res.SPECIAL_TITLE_SUB && res.SPECIAL_TITLE_SUB.length && res.SPECIAL_TITLE_SUB[0]) ||
            {},
        });
      }
    });
  };
  //查询热门板块列表
  onHotSearchConfigList =  () => {
    homeService.hotSearchConfigList({ channelId: getParam('id') }).then(res => {
      if (res) {
        this.setState({
          hotList: res,
        });
      }
    });
  };
  //修改状态
  setType = (type, Boolean, setType=true) => {
    if(setType){
      const { hotList, SPECIAL_TITLE_MAIN,SPECIAL_TITLE_SUB } = this.state;
      let keys = {
        hotType: 'words',
        mainTitleType: 'SPECIAL_TITLE_MAIN',
        sideTitleType: 'SPECIAL_TITLE_SUB',
      };
      let values = {
        hotType: hotList,
        mainTitleType: SPECIAL_TITLE_MAIN.describeInfo,
        sideTitleType: SPECIAL_TITLE_SUB.describeInfo,
      };
      this.props.form.setFieldsValue({
        [keys[type]]: values[type],
      });
    }
    
    this.setState({
      [type]: Boolean,
    });
  };
  //添加热门板块
  onAddHot = e => {
    e.preventDefault();
    this.props.form.validateFields(['words'], (err, values) => {
      if (!err) {
        homeService
          .hotSearchConfigSave({ words: values.words, channelId: getParam('id') })
          .then(res => {
            if (res) {
              message.success('修改成功');
               this.onHotSearchConfigList();
               this.setType('hotType', false,false);
            }
          });
      }
    });
  };
  //修改主标题
  onMainTitle = e => {
    e.preventDefault();
    this.props.form.validateFields(['SPECIAL_TITLE_MAIN'], (err, values) => {
      if (!err) {
        const { SPECIAL_TITLE_MAIN } = this.state;
        if (SPECIAL_TITLE_MAIN && SPECIAL_TITLE_MAIN.id) {
          homeService
            .pageElementConfigUpdate({
              id: SPECIAL_TITLE_MAIN.id,
              describeInfo: values.SPECIAL_TITLE_MAIN,
              type: 'SPECIAL_TITLE_MAIN',
              channelId: getParam('id'),
            })
            .then(res => {
              if (res) {
                this.setState({
                  mainTitleType: false,
                });
                message.success('成功');
                this.onListIndex();
              }
            });
        } else {
          homeService
            .pageElementConfigAdd({
              describeInfo: values.SPECIAL_TITLE_MAIN,
              type: 'SPECIAL_TITLE_MAIN',
              channelId: getParam('id'),
            })
            .then(res => {
              if (res) {
                this.setState({
                  mainTitleType: false,
                });
                message.success('成功');
                this.onListIndex();
              }
            });
        }
      }
    });
  };
  //副标题主标题
  onSideTitle = e => {
    e.preventDefault();
    this.props.form.validateFields(['SPECIAL_TITLE_SUB'], (err, values) => {
      if (!err) {
        const { SPECIAL_TITLE_SUB } = this.state;
        if (SPECIAL_TITLE_SUB && SPECIAL_TITLE_SUB.id) {
          homeService
            .pageElementConfigUpdate({
              id: SPECIAL_TITLE_SUB.id,
              describeInfo: values.SPECIAL_TITLE_SUB,
              type: 'SPECIAL_TITLE_SUB',
              channelId: getParam('id'),
            })
            .then(res => {
              if (res) {
                this.setState({
                  sideTitleType: false,
                });
                message.success('成功');
                this.onListIndex();
              }
            });
        } else {
          homeService
            .pageElementConfigAdd({
              describeInfo: values.SPECIAL_TITLE_SUB,
              type: 'SPECIAL_TITLE_SUB',
              channelId: getParam('id'),
            })
            .then(res => {
              if (res) {
                this.setState({
                  sideTitleType: false,
                });
                message.success('成功');
                this.onListIndex();
              }
            });
        }
      }
    });
  };

  //热门关键词列表
  getFields = () => {
    const { getFieldDecorator } = this.props.form;
    const children = [];
    const { hotList, hotType } = this.state;
    for (let i = 0; i < 10; i++) {
      children.push(
        <Col span={6} key={i}>
          <Form.Item label={`${i + 1}`} style={{ display: 'flex' }}>
            {getFieldDecorator(`words[${i}]`, {
              rules: [
                {
                  required: true,
                  message: '请输入',
                },
              ],
              initialValue: hotList[i],
            })(<Input placeholder="请输入" disabled={!hotType} />)}
          </Form.Item>
        </Col>,
      );
    }
    return children;
  };
  //提交
  handleOk = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll(
      ['materialCenterItemId', 'link', 'sortNum',"describeInfo"],
      (err, values) => {
        if (!err) {
          const { type } = this.state;
          this.onPageElementConfigAdd(values, type);
        }
      },
    );
  };
  //打开Modal框
  showModal = (title, type) => {
    this.setState({
      visible: true,
      title,
      type,
    });
  };
  //关闭Modal框
  handleCancel = e => {
    this.setState({
      visible: false,
    });
  };
  //新增列表
  onPageElementConfigAdd = (values, type) => {
    homeService.pageElementConfigAdd({ ...values, type, channelId: getParam('id') }).then(res => {
      if (res) {
        message.success('新增成功');
        this.onListIndex();
        this.handleCancel();
      }
    });
  };
  //删除列表
  onPageElementConfigDelete = id => {
    homeService.pageElementConfigDelete({ id }).then(res => {
      if (res) {
        message.success('删除成功');
        this.onListIndex();
      }
    });
  };
  //标题状态切换
  onTitleStatus = (title, type, name, onOk) => {
    return (
      <div className="fb">
        <CustomCard title={title} />
        <div>
          {type ? (
            <div>
              <Button onClick={() => this.setType(name, false)}>取消</Button>
              <Button
                type="primary"
                onClick={onOk ? e => onOk(e) : () => this.setType(name, false)}
              >
                确定
              </Button>
            </div>
          ) : (
            <a onClick={() => this.setType(name, true)}>
              <Icon type="edit" />
              修改
            </a>
          )}
        </div>
      </div>
    );
  };

  //增加按钮
  onAddButton = (name, type) => {
    return (
      <Button
        type="dashed"
        onClick={() => this.showModal(name, type)}
        style={{ width: '100%', marginBottom: 20 }}
      >
        <Icon type="plus" />
        {name}
      </Button>
    );
  };

  //table列
  columns = type => {
    return [
      {
        title: '素材ID',
        dataIndex: 'materialCenterItemId',
        width: '25%',
        align: 'center',
      },
      {
        title: '跳转链接',
        dataIndex: 'link',
        width: '25%',
        align: 'center',
      },
      {
        title: '名称或介绍',
        dataIndex: 'describeInfo',
        width: '25%',
        align: 'center',
      },
      {
        title: '排序',
        dataIndex: 'sortNum',
        width: '25%',
        align: 'center',
      },
      {
        title: '操作',
        dataIndex: 'id',
        width: '25%',
        align: 'center',
        render: e => (
          <Popconfirm
            disabled={type}
            title="你确定删除吗?"
            onConfirm={() => this.onPageElementConfigDelete(e)}
          >
            <Button type="link" disabled={type}>
              删除
            </Button>
          </Popconfirm>
        ),
      },
    ];
  };

  render() {
    const {
      hotType,
      bannerType,
      INDEX_BANNER,
      SPECIAL_AREA_MAIN,
      MY_TYPE,
      MY_LUNBO,
      SPECIAL_AREA_SUB,
      visible,
      title,
      mainPictureType,
      sidePictureType,
      mainTitleType,
      sideTitleType,
      SPECIAL_TITLE_MAIN,
      SPECIAL_TITLE_SUB,
    } = this.state;
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 },
      },
    };
    return (
      <PageHeaderWrapper title={false}>
        <Card bordered={false} style={{ marginBottom: 20 }}>
          <Descriptions title={<CustomCard title="基本信息" />}>
            <Descriptions.Item label="渠道">{getParam('channelName')}</Descriptions.Item>
            <Descriptions.Item label="位置">{getParam('name')}</Descriptions.Item>
          </Descriptions>
        </Card>
        <Card bordered={false}>
          {this.onTitleStatus('热门搜索关键词', hotType, 'hotType', this.onAddHot)}
          <Form>
            <Row gutter={24}>{this.getFields()}</Row>
          </Form>
          <CustomCard title="横幅" />
          <AntTable dataSource={onTableData(INDEX_BANNER)} columns={this.columns(bannerType)} />
          {this.onAddButton('新增横幅', 'INDEX_BANNER')}
          {this.onTitleStatus('专区-标题-主标题', mainTitleType, 'mainTitleType', this.onMainTitle)}
          <Form>
            <Form.Item label="主标题" style={{ display: 'flex' }}>
              {getFieldDecorator(`SPECIAL_TITLE_MAIN`, {
                rules: [
                  {
                    required: true,
                    message: '请输入',
                  },
                ],
                initialValue: (SPECIAL_TITLE_MAIN && SPECIAL_TITLE_MAIN.describeInfo) || undefined,
              })(<Input placeholder="请输入" disabled={!mainTitleType} />)}
            </Form.Item>
          </Form>
          {this.onTitleStatus('专区-标题-副标题', sideTitleType, 'sideTitleType', this.onSideTitle)}
          <Form>
            <Form.Item label="副标题" style={{ display: 'flex' }}>
              {getFieldDecorator(`SPECIAL_TITLE_SUB`, {
                rules: [
                  {
                    required: true,
                    message: '请输入',
                  },
                ],
                initialValue: (SPECIAL_TITLE_SUB && SPECIAL_TITLE_SUB.describeInfo) || undefined,
              })(<Input placeholder="请输入" disabled={!sideTitleType} />)}
            </Form.Item>
          </Form>
          <CustomCard title="专区-主图" />
          <AntTable
            dataSource={onTableData(SPECIAL_AREA_MAIN)}
            columns={this.columns(mainPictureType)}
          />
          {this.onAddButton('新增主图', 'SPECIAL_AREA_MAIN')}
          <CustomCard title="专区-分类图标" />
          <AntTable
            dataSource={onTableData(MY_TYPE)}
            columns={this.columns(mainPictureType)}
          />
          {this.onAddButton('新增分类图标', 'MY_TYPE')}
          <CustomCard title="专区-新海报(限一个)" />
          <AntTable
            dataSource={onTableData(MY_LUNBO)}
            columns={this.columns(mainPictureType)}
          />
          {this.onAddButton('新增新海报', 'MY_LUNBO')}
          <CustomCard title="专区-副图" />
          <AntTable
            dataSource={onTableData(SPECIAL_AREA_SUB)}
            columns={this.columns(sidePictureType)}
          />
          {this.onAddButton('新增副图', 'SPECIAL_AREA_SUB')}
        </Card>
        <Card bordered={false} style={{ margin: '20px 0' }}>
          <Tab />
        </Card>
        <Card bordered={false} style={{ margin: '20px 0' }}>
          <Tab1 />
        </Card>
        <Modal
          destroyOnClose
          title={title}
          visible={visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Form {...formItemLayout}>
            <Form.Item label="素材ID">
              {getFieldDecorator('materialCenterItemId', {
                rules: [
                  {
                    required: true,
                    message: '请输入',
                  },
                ],
              })(<Input placeholder="请输入" />)}
            </Form.Item>
            <Form.Item label="跳转链接">
              {getFieldDecorator('link', {
                rules: [
                  {
                    required: true,
                    message: '请输入',
                  },
                ],
              })(<Input placeholder="请输入" />)}
            </Form.Item>
            <Form.Item label="名称">
              {getFieldDecorator('describeInfo', {
              })(<Input placeholder="请输入" />)}
            </Form.Item>
            <Form.Item label="排序">
              {getFieldDecorator('sortNum', {
                rules: [
                  {
                    required: true,
                    message: '请输入',
                  },
                ],
              })(<Input placeholder="请输入" />)}
            </Form.Item>
          </Form>
        </Modal>
      </PageHeaderWrapper>
    );
  }
}
