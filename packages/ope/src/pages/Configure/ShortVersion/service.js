import request from '@/services/baseService';

export default {
  //获取需要配置的小程序列表
  listLitePlatformChannel: data => {
    return request(`/hzsx/platform/listLitePlatformChannel`, data, 'get');
  },
  //查询首页数据
  listIndex: data => {
    return request(`/hzsx/pageElementConfig/listIndex`, data, 'post');
  },
  //分页查询热门板块
  hotSearchConfigList: data => {
    return request(`/hzsx/hotSearchConfig/list`, data, 'get');
  },
  //添加热门板块
  hotSearchConfigSave: data => {
    return request(`/hzsx/hotSearchConfig/save`, data, 'post');
  },
  //配置添加
  pageElementConfigAdd: data => {
    return request(`/hzsx/pageElementConfig/add`, data, 'post');
  },
  //配置删除
  pageElementConfigDelete: data => {
    return request(`/hzsx/pageElementConfig/delete`, data, 'get');
  },
  //配置修改
  pageElementConfigUpdate: data => {
    return request(`/hzsx/pageElementConfig/update`, data, 'post');
  },
  //查询Tab列表
  tabList: data => {
    return request(`/hzsx/tab/list`, data, 'get');
  },
  //查询可以添加到Tab下的商品
  getAvailableProduct: data => {
    return request(`/hzsx/tab/getAvailableProduct`, data, 'post');
  },
  //查询Tab下商品
  listProduct: data => {
    return request(`/hzsx/tab/listProduct`, data, 'get');
  },
  //新增标题
  tabAdd: data => {
    return request(`/hzsx/tab/add`, data, 'post');
  },
  //根据ID查询Tab
  getById: data => {
    return request(`/hzsx/tab/getById`, data, 'get');
  },
  //修改标题
  tabUpdata: data => {
    return request(`/hzsx/tab/update`, data, 'post');
  },
  //删除标题
  tabDelete: data => {
    return request(`/hzsx/tab/delete`, data, 'get');
  },
  //添加商品到Tab下
  addProduct: data => {
    return request(`/hzsx/tab/addProduct`, data, 'post');
  },
  deleteProduct: data => {
    return request(`/hzsx/tab/deleteProduct`, data, 'get');
  },
  //更新Tab下商品排序
  updateProductSort: data => {
    return request(`/hzsx/tab/updateProductSort`, data, 'post');
  },
  //查看我的所有数据
  listMy: data => {
    return request(`/hzsx/pageElementConfig/listMy`, data, 'post');
  },
};
