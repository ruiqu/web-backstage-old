import React, { Component } from 'react';
import { Form, Input, Button, message, Modal, Popconfirm, Tabs, Divider, Select } from 'antd';
import CustomCard from '@/components/CustomCard';
import tabService from '../service';
import { getParam, onTableData } from '@/utils/utils';
import MyPageTable from '@/components/MyPageTable';
import Products from './products';
const { TabPane } = Tabs;
@Form.create()
export default class tab extends Component {
  state = {
    tabList: [],
    tabData: {},
    pageNum: 0,
    total: 0,
    list: [],
    title: '',
    tabVisible: false,
    addTabProductVisible: false,
    sortVisible: false,
    tabId: '',
    data: {},
  };
  async componentDidMount() {
    this.onTabs(true);
  }
  onTabs = type => {
    tabService.tabList({ channelId: getParam('id') }).then(res => {

      if (res) {
        /* res = [{
          channelId:"000",
          createTime:"2023-03-03 16:28:48",
          id:0,
          indexSort:1,
          jumpUrl:"pages/classified/classified",
          name:"热销",
          shopId:null
        },...res] */
        console.log("res...", res)
        this.setState(
          {
            tabList: res,
            tabData: (res && res[0]) || {},
            tabId: (res && res[0] && res[0].id) || '',
          },
          () => {
            if (type) {
              if (res && res[0] && res[0].id) {
                this.onGetAvailableProduct(res[0].id, 1, 100);
              } else {
                this.setState({
                  list: [],
                  pageNum: 0,
                  total: 0,
                });
              }
            }
          },
        );
      }
    });
  };
  onGetAvailableProduct = (tabId, pageNum, pageSize) => {
    tabService.listProduct({ tabId, pageNum, pageSize }).then(res => {
      if (res) {
        this.setState({
          list: res.records,
          pageNum: res.current,
          total: res.total,
        });
      }
    });
  };
  // 页签的跳转
  onTabChange = key => {
    const tabData = this.state.tabList.find(item => item.id === Number(key)) || {};
    this.setState(
      {
        tabData,
        tabId: key,
      },
      () => {
        this.onGetAvailableProduct(tabData.id, 1, 10);
      },
    );
  };
  //修改状态
  setType = (type, Boolean) => {
    this.setState({
      [type]: Boolean,
    });
  };
  // 新增Tab
  addTab = () => {
    this.setState({
      tabVisible: true,
      title: '新增标签',
      tabData: {},
    });
  };
  //修改Tab
  setTab = () => {
    const { tabId } = this.state;
    tabService.getById({ id: tabId }).then(res => {
      if (res) {
        this.setState({
          tabVisible: true,
          title: '修改标签',
          tabData: res,
        });
      }
    });
  };
  //删除标签
  deleteTab = () => {
    const { tabId } = this.state;
    tabService.tabDelete({ id: tabId }).then(res => {
      if (res) {
        this.onTabs(true);
      }
    });
  };

  // 新增tab modal的确认
  handleOk = e => {
    e.preventDefault();
    this.props.form.validateFields(['tab'], (err, value) => {
      if (!err) {
        const { tabId, title } = this.state;
        if (title === '新增标签') {
          tabService.tabAdd({ ...value.tab, channelId: getParam('id') }).then(res => {
            if (res) {
              message.success('成功');
              this.setState(
                {
                  tabVisible: false,
                },
                () => {
                  tabService.tabList({ channelId: getParam('id') }).then(res => {
                    if (res) this.setState({ tabList: res });
                  });
                },
              );
            }
          });
        } else {
          tabService.tabUpdata({ id: tabId, ...value.tab, channelId: getParam('id') }).then(res => {
            if (res) {
              message.success('成功');
              this.setState(
                {
                  tabVisible: false,
                },
                () => {
                  tabService.tabList({ channelId: getParam('id') }).then(res => {
                    if (res) this.setState({ tabList: res });
                  });
                },
              );
            }
          });
        }
      }
    });
  };
  tabProductCancel = () => {
    this.setState({
      addTabProductVisible: false,
    });
  };
  onPage = e => {
    this.setState(
      {
        pageNum: e.current,
      },
      () => {
        this.onGetAvailableProduct(this.state.tabId, e.current, 10);
      },
    );
  };
  deleteProduct = e => {
    tabService.deleteProduct({ id: e.id }).then(res => {
      if (res) {
        this.onGetAvailableProduct(this.state.tabId, this.state.pageNum, 10);
        message.success('删除成功');
      }
    });
  };
  onOk = e => {
    e.preventDefault();
    this.props.form.validateFields(['sort'], (err, value) => {
      if (!err) {
        tabService
          .updateProductSort({ indexSort: value.sort, id: this.state.data.id })
          .then(res => {
            if (res)
              this.setState(
                {
                  sortVisible: false,
                },
                () => {
                  this.onGetAvailableProduct(this.state.tabId, this.state.pageNum, 10);
                  message.success('修改成功');
                },
              );
          });
      }
    });
  };
  render() {
    const {
      tabList,
      pageNum = 0,
      pageSize = 100,
      total = 0,
      loading,
      tabData,
      list,
      title,
      tabVisible,
    } = this.state;
    const paginationProps = {
      current: pageNum,
      pageSize: pageSize,
      total: total,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{total}条</span>
        </span>
      ),
    };
    const columns = [
      {
        title: '商品编号',
        dataIndex: 'itemId',
        align: 'center',
        key: 'itemId',
      },
      {
        title: '商品名称',
        dataIndex: 'name',
        align: 'center',
        key: 'name',
      },
      {
        title: '排序',
        dataIndex: 'indexSort',
        align: 'center',
        key: 'indexSort',
      },
      {
        title: '操作',
        align: 'center',
        key: 'action',
        render: (text, record) => (
          <div className="table-action">
            <a
              onClick={() =>
                this.setState({
                  sortVisible: true,
                  data: record,
                })
              }
            >
              排序
            </a>
            <Divider type="vertical" />
            <Popconfirm title="确定要删除吗？" onConfirm={() => this.deleteProduct(record)}>
              <a>删除</a>
            </Popconfirm>
          </div>
        ),
      },
    ];
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const { getFieldDecorator } = this.props.form;
    return (
      <>
        <CustomCard title="标签" />
        <Button type="primary" onClick={this.addTab}>
          新增标签
        </Button>
        <Button onClick={this.setTab}>修改标签</Button>
        <Popconfirm
          title={'确定要删除' + ` ${tabData.name} ` + '标签吗？'}
          onConfirm={this.deleteTab}
        >
          <Button>删除标签</Button>
        </Popconfirm>
        <Tabs size="small" onChange={this.onTabChange} animated={false}>
          {tabList &&
            tabList.map(item => {
              return <TabPane tab={item.name} key={item.id} />;
            })}
        </Tabs>
        <Button
          type="primary"
          style={{ margin: '10px 0 20px 0' }}
          onClick={() => this.setState({ addTabProductVisible: true })}
        >
          新增商品
        </Button>
        <MyPageTable
          scroll
          onPage={this.onPage}
          paginationProps={paginationProps}
          dataSource={onTableData(list)}
          columns={columns}
          loading={loading}
        />
        <Modal
          title={title}
          visible={tabVisible}
          onOk={this.handleOk}
          destroyOnClose
          onCancel={() => {
            this.setState({
              tabVisible: false,
            });
          }}
        >
          <Form {...formItemLayout}>
            <Form.Item label="Tab名称">
              {getFieldDecorator('tab.name', {
                rules: [
                  {
                    required: true,
                    message: 'Tab名称不能为空',
                  },
                ],
                initialValue: tabData.name,
              })(<Input placeholder="请输入" />)}
            </Form.Item>
            <Form.Item label="跳转地址">
              {getFieldDecorator('tab.jumpUrl', {
                rules: [
                  {
                    required: true,
                    message: '跳转地址不能为空',
                  },
                ],
                initialValue: tabData.jumpUrl,
              })(<Input placeholder="请输入" />)}
            </Form.Item>
            <Form.Item label="排序">
              {getFieldDecorator('tab.indexSort', {
                rules: [
                  {
                    required: true,
                    message: '排序不能为空',
                  },
                ],
                initialValue: tabData.indexSort,
              })(<Input placeholder="请输入" />)}
            </Form.Item>
          </Form>
        </Modal>
        <Modal
          visible={this.state.addTabProductVisible}
          title="新增Tab产品"
          destroyOnClose
          onCancel={this.tabProductCancel}
          width={1000}
          footer={null}
        >
          <Products
            tabProductCancel={this.tabProductCancel}
            onTabChange={this.onTabChange}
            tabActiveKey={this.state.tabId}
          />
        </Modal>
        <Modal
          visible={this.state.sortVisible}
          title="排序"
          destroyOnClose
          onCancel={() => this.setState({ sortVisible: false })}
          onOk={this.onOk}
        >
          <Form {...formItemLayout}>
            <Form.Item label="排序">
              {getFieldDecorator('sort', {
                rules: [
                  {
                    required: true,
                    message: '排序不能为空',
                  },
                ],
                initialValue: this.state.data.indexSort || undefined,
              })(<Input placeholder="请输入" />)}
            </Form.Item>
          </Form>
        </Modal>
      </>
    );
  }
}
