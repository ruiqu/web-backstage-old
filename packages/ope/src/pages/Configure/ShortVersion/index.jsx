import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Table, Card } from 'antd';
import ShortVersionSeeService from './service';
import { router } from 'umi';

export default class ShortVersion extends Component {
  state = {};
  componentDidMount() {
    ShortVersionSeeService.listLitePlatformChannel().then(res => {
      if (res) this.setState({ list: res });
    });
  }
  render() {
    const { list = [] } = this.state;
    const columns = [
      {
        title: '渠道/位置',
        dataIndex: 'channelName',
      },
      {
        title: '首页',
        render: e => (
          <a
            onClick={() =>
              router.push(
                `/configure/ShortVersion/index/home?id=${e.channelId}&channelName=${e.channelName}&name=首页`,
              )
            }
          >
            进入
          </a>
        ),
      },
      {
        title: '我的',
        render: e => (
          <a
            onClick={() =>
              router.push(
                `/configure/ShortVersion/index/my?id=${e.channelId}&channelName=${e.channelName}&name=我的`,
              )
            }
          >
            进入
          </a>
        ),
      },
    ];
    return (
      <PageHeaderWrapper title={false}>
        <Card bordered={false}>
          <Table columns={columns} dataSource={list} bordered pagination={false} />
        </Card>
      </PageHeaderWrapper>
    );
  }
}
