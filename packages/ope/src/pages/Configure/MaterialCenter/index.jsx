import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Button, Card, Modal, Form, Input, message, Divider, Popconfirm } from 'antd';
import { router } from 'umi';
import SearchList from '@/components/SearchList';
import MaterialCenterService from './service';
@Form.create()
export default class MaterialCenter extends Component {
  state = {
    current: 1,
  };
  handleFilter = (pageNumber, pageSize, data = {}) => {
    this.setState(
      {
        loading: true,
      },
      () => {
        MaterialCenterService.pageCategory({
          pageNumber,
          pageSize,
          ...data,
        }).then(res => {
          if (res) {
            this.setState({
              list: res.records,
              total: res.total,
              loading: false,
              current: res.current,
            });
          }
        });
      },
    );
  };
  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        MaterialCenterService.addCategory({ ...values }).then(res => {
          if (res) {
            message.success('新建成功');
            this.handleFilter(1, 10);
            this.handleCancel();
          }
        });
      }
    });
  };
  delete = id => {
    const { current } = this.state;
    MaterialCenterService.deleteCategory({ id }).then(res => {
      if (res) {
        message.success('删除成功');
        this.handleFilter(current, 10);
      }
    });
  };
  handleCancel = e => {
    this.setState({
      visible: false,
    });
  };
  render() {
    const columns = [
      {
        title: '分类名称',
        dataIndex: 'name',
      },
      {
        title: '素材尺寸（px）',
        render: e => (
          <span>
            {e.width}*{e.height}
          </span>
        ),
      },
      {
        title: '操作',
        render: e => (
          <>
            <a
              onClick={() =>
                router.push(`/configure/MaterialCenter/index/see?id=${e.id}`)
              }
            >
              查看
            </a>
            <Divider type="vertical" />
            <Popconfirm title="确定删除吗？" onConfirm={() => this.delete(e.id)}>
              <a>删除</a>
            </Popconfirm>
          </>
        ),
      },
    ];

    let searchHander = [
      {
        label: '分类名称',
        type: 'input',
        key: 'name',
      },
    ];
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 },
      },
    };
    let { loading, list = [], total = 0, visible } = this.state;
    const { getFieldDecorator } = this.props.form;
    return (
      <PageHeaderWrapper title={false}>
        <Card bordered={false}>
          <SearchList
            handleFilter={this.handleFilter}
            columns={columns}
            searchHander={searchHander}
            loading={loading}
            total={total}
            list={list}
            pagination={true}
          >
            <Button
              type="primary"
              onClick={this.showModal}
              style={{ width: 111, marginBottom: 10 }}
            >
              新增
            </Button>
          </SearchList>
        </Card>
        <Modal title="新增" visible={visible} onOk={this.handleOk} onCancel={this.handleCancel}>
          <Form {...formItemLayout}>
            <Form.Item label="分类名称">
              {getFieldDecorator('name', {
                rules: [
                  {
                    required: true,
                    message: '请输入',
                  },
                ],
              })(<Input placeholder="请输入" />)}
            </Form.Item>
            <Form.Item label="素材长度">
              {getFieldDecorator('width', {
                rules: [
                  {
                    required: true,
                    message: '请输入',
                  },
                ],
              })(<Input placeholder="请输入" addonAfter={'px'} />)}
            </Form.Item>
            <Form.Item label="素材宽度">
              {getFieldDecorator('height', {
                rules: [
                  {
                    required: true,
                    message: '请输入',
                  },
                ],
              })(<Input placeholder="请输入" addonAfter={'px'} />)}
            </Form.Item>
          </Form>
        </Modal>
      </PageHeaderWrapper>
    );
  }
}
