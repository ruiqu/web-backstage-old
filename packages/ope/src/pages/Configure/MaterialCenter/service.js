import request from '@/services/baseService';

export default {
  pageCategory: data => {
    return request(`/hzsx/materialCenter/pageCategory`, data, 'post');
  },
  addCategory: data => {
    return request(`/hzsx/materialCenter/addCategory`, data, 'post');
  },
  deleteCategory: data => {
    return request(`/hzsx/materialCenter/deleteCategory`, data, 'get');
  },
  //素材中心-素材-查看列表
  pageItem: data => {
    return request(`/hzsx/materialCenter/pageItem`, data, 'post');
  },
  //素材中心-添加素材
  addItem: data => {
    return request(`/hzsx/materialCenter/addItem`, data, 'post');
  },
   //素材中心-分类-查看详情
   detailCategory: data => {
    return request(`/hzsx/materialCenter/detailCategory`, data, 'get');
  },
  //素材中心-素材-查看详细
  detailItem: data => {
    return request(`/hzsx/materialCenter/detailItem`, data, 'get');
  },
  //素材中心-素材-修改
  updateItem: data => {
    return request(`/hzsx/materialCenter/updateItem`, data, 'post');
  },
  //素材中心-素材-删除
  deleteItem: data => {
    return request(`/hzsx/materialCenter/deleteItem`, data, 'get');
  },

};
