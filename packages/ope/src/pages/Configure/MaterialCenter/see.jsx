import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Button, Card, Modal, Form, Input, message, Divider, Popconfirm, Upload, Icon } from 'antd';
import { uploadUrl } from '@/config/index';
import { router } from 'umi';
import SearchList from '@/components/SearchList';
import MaterialCenterSeeService from './service';
import { getParam, compression } from '@/utils/utils';
@Form.create()
export default class see extends Component {
  state = {
    current: 1,
    fileList: [],
    imgVisible: false,
    imgSrc: '',
    width: '',
    height: '',
    disabledType: false,
    id: '',
  };
  componentDidMount() {
    MaterialCenterSeeService.detailCategory({ id: getParam('id') }).then(res => {
      if (res) {
        this.setState({
          width: res.width,
          height: res.height,
        });
      }
    });
  }
  handleFilter = (pageNumber, pageSize, data = {}) => {
    this.setState(
      {
        loading: true,
      },
      () => {
        MaterialCenterSeeService.pageItem({
          pageNumber,
          pageSize,
          ...data,
          categoryId: getParam('id'),
        }).then(res => {
          if (res) {
            this.setState({
              list: res.records,
              total: res.total,
              loading: false,
              current: res.current,
            });
          }
        });
      },
    );
  };
  showModal = () => {
    this.setState({
      visible: true,
      disabledType: false,
      data: {},
      fileList: [],
      imgSrc: '',
    });
  };

  handleOk = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        values.fileUrl = this.state.imgSrc;
        if (this.state.disabledType) {
          MaterialCenterSeeService.updateItem({
            ...values,
            categoryId: getParam('id'),
            id: this.state.id,
          }).then(res => {
            if (res) {
              message.success('修改成功');
              this.handleFilter(this.state.current, 10);
              this.handleCancel();
            }
          });
        } else {
          MaterialCenterSeeService.addItem({ ...values, categoryId: getParam('id') }).then(res => {
            if (res) {
              message.success('新建成功');
              this.handleFilter(1, 10);
              this.handleCancel();
            }
          });
        }
      }
    });
  };
  delete = id => {
    const { current } = this.state;
    MaterialCenterSeeService.deleteItem({ id }).then(res => {
      if (res) {
        message.success('删除成功');
        this.handleFilter(current, 10);
      }
    });
  };
  handleCancel = e => {
    this.setState({
      visible: false,
    });
  }; // 上传的回调
  handleChange = ({ file, fileList, event }) => {
    const filelist = [...fileList] || [];
    if (file.status == 'done') {
      filelist.map(item => {
        this.setState({
          imgSrc: item.response.data,
        });
      });
    }
    this.setState({
      fileList,
    });
  };
  //上传图片前
  beforeUpload = file => {
    const { width, height } = this.state;
   /*  const width=450
    const height=200 */
    return compression(file, width, height).then(res => {
      return new Promise(resolve => {
        resolve(res.file);
      });
    });
  };
  // 上传后预览的回调
  handlePreview = () => {
    this.setState({
      imgVisible: true,
    });
  };
  //修改
  onModify = id => {
    console.log(id,'------------------------------id>')
    MaterialCenterSeeService.detailItem({ id }).then(res => {
      if (res) {
        this.setState({
          data: res,
          fileList: [{ uid: 1, url: res.fileUrl }],
          visible: true,
          disabledType: true,
          imgSrc: res.fileUrl,
          id,
        });
      }
    });
  };
  render() {
    const columns = [
      {
        title: '素材ID',
        dataIndex: 'id',
      },
      {
        title: '素材名称',
        dataIndex: 'name',
      },
      {
        title: '图片',
        dataIndex: 'fileUrl',
        render: e => <img src={e} alt="alt" style={{ width: 101, height: 54 }} />,
      },
      {
        title: '操作',
        render: e => (
          <>
            <a onClick={() => this.onModify(e.id)}>修改</a>
            <Divider type="vertical" />
            <Popconfirm title="确定删除吗？" onConfirm={() => this.delete(e.id)}>
              <a>删除</a>
            </Popconfirm>
          </>
        ),
      },
    ];

    let searchHander = [
      {
        label: '素材ID',
        type: 'input',
        key: 'id',
      },
      {
        label: '素材名称',
        type: 'input',
        key: 'name',
      },
    ];
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 },
      },
    };
    const uploadButton = (
      <div>
        <Icon type="plus" />
        <div className="ant-upload-text">上传图片</div>
      </div>
    );
    let {
      loading,
      list = [],
      total = 0,
      visible,
      fileList,
      imgVisible,
      imgSrc,
      disabledType,
      data,
    } = this.state;
    const { getFieldDecorator } = this.props.form;
    return (
      <PageHeaderWrapper title={false}>
        <Card bordered={false}>
          <SearchList
            handleFilter={this.handleFilter}
            columns={columns}
            searchHander={searchHander}
            loading={loading}
            total={total}
            list={list}
            pagination={true}
          >
            <Button
              type="primary"
              onClick={this.showModal}
              style={{ width: 111, marginBottom: 10 }}
            >
              新增
            </Button>
          </SearchList>
        </Card>
        <Modal destroyOnClose title="素材" visible={visible} onOk={this.handleOk} onCancel={this.handleCancel}>
          <Form {...formItemLayout}>
            <Form.Item label="素材名称">
              {getFieldDecorator('name', {
                rules: [
                  {
                    required: true,
                    message: '请输入',
                  },
                ],
                initialValue: data && data.name,
              })(<Input placeholder="请输入" disabled={disabledType} />)}
            </Form.Item>
            <Form.Item label="素材图片">
              {getFieldDecorator('fileUrl', {
                rules: [
                  {
                    required: true,
                    message: '请上传图片，图片不能为空',
                  },
                ],
                initialValue: this.state.imgSrc,
              })(
                <Upload
                  accept="image/*"
                  action={uploadUrl}
                  headers={{
                    token: localStorage.getItem('token'),
                  }}
                  listType="picture-card"
                  onPreview={this.handlePreview} //预览
                  onChange={this.handleChange}
                  fileList={fileList}
                  /* beforeUpload={this.beforeUpload} */
                >
                  {fileList.length >= 1 ? null : uploadButton}
                </Upload>,
              )}
            </Form.Item>
          </Form>
        </Modal>
        <Modal
          visible={imgVisible}
          footer={null}
          onCancel={() => this.setState({ imgVisible: false })}
          width={600}
        >
          <img src={imgSrc} alt="alt" className="preview-img" />
        </Modal>
      </PageHeaderWrapper>
    );
  }
}
