import React, { PureComponent } from 'react';

import { connect } from 'dva';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { goToRouter } from '@/utils/utils';
import { Table, Card, message, Button, Form, Divider, Popconfirm } from 'antd';
import Search from '../../components/Search';
import ActivityServices from './services';
import CopyToClipboard from 'react-copy-to-clipboard';
@connect()
@Form.create()
class Activity extends PureComponent {
  state = {
    tableData: [],
    loading: true,
    current: 1,
    pageNumber: 1,
    pageSize: 10,
    title: null,
    status: null,
    list: [],
    total: 1,
    datas: {},
  };

  columns = [
    {
      title: '活动ID',
      dataIndex: 'id',
      key: 'id',
      align: 'center',
    },
    {
      title: '主标题',
      dataIndex: 'activityName',
      align: 'center',
    },
    {
      title: '操作',
      align: 'center',
      key: 'action',
      render: (text, record) => {
        return (
          <div className="table-action">
            <a onClick={() => this.edit(record.id)}>修改</a>
            <Divider type="vertical" />
            <Popconfirm title="你确定删除吗?" onConfirm={() => this.deleteTribe(record.id)}>
              <a>删除</a>
            </Popconfirm>
            <Divider type="vertical" />
            <CopyToClipboard
              text={`/pages/configureActivities/index?id=${record.id}`}
              onCopy={this.onCopy}
            >
              <a>复制链接</a>
            </CopyToClipboard>
          </div>
        );
      },
    },
  ];
  onCopy = e => {
    message.destroy();
    message.success(`复制内容：${e}`);
  };
  componentDidMount() {
    this.onQuery(1, 10);
  }

  /**
   * 查询活动列表
   */
  onQuery = (pageNumber, pageSize, data = {}) => {
    this.setState(
      {
        loading: true,
      },
      () => {
        ActivityServices.listActivity({ pageNumber, pageSize, ...data }).then(res => {
          this.setState({
            list: res.records,
            total: res.total,
            loading: false,
            current: res.current,
          });
        });
      },
    );
  };

  handleFilter = (data = {}) => {
    this.setState(
      {
        datas: data,
      },
      () => {
        this.onQuery(1, 10, { ...data });
      },
    );
  };

  // 分页，下一页
  onChange = pageNumber => {
    this.onQuery(pageNumber, 10, { ...this.state.data });
  };

  showTotal = () => {
    return `共有${this.state.total}条`;
  };

  // 切换每页数量
  onShowSizeChange = (current, pageSize) => {
    this.setState({ pageSize }, () => {
      this.handleFilter({ type: '分页' });
    });
  };

  edit = id => {
    goToRouter(this.props.dispatch, `/configure/Activity/edit/${id}`);
  };
  /**
   * 删除
   * @param {*} id
   */
  deleteTribe = id => {
    ActivityServices.deleteActivity({ id }).then(res => {
      message.success('删除成功');
      this.onQuery(this.state.current, 10, { ...this.state.datas });
    });
  };

  handlePublish = () => {
    const { dispatch } = this.props;
    const path = '/configure/Activity/index/publish';
    goToRouter(dispatch, path);
  };

  render() {
    const { loading, current, total, tableData } = this.state;
    return (
      <PageHeaderWrapper title={false}>
        <Card bordered={false}>
          <Search needReset source="活动配置" handleFilter={this.handleFilter} />
          <Button type="primary" onClick={this.handlePublish}>
            + 新增
          </Button>
          <Table
            columns={this.columns}
            tableLayout="fixed"
            loading={loading}
            dataSource={this.state.list}
            rowKey={record => record.id}
            pagination={{
              current,
              total,
              onChange: this.onChange,
              showTotal: this.showTotal,
              showQuickJumper: true,
              pageSizeOptions: ['5', '10', '20'],
              showSizeChanger: true,
              onShowSizeChange: this.onShowSizeChange,
            }}
          />
        </Card>
      </PageHeaderWrapper>
    );
  }
}

export default Activity;
