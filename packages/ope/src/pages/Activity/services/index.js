import request from '@/services/baseService';

export default {
  ableProductV1: data => {
    return request(`/hzsx/index/ableProductV1`, data, 'get');
  },
  //查询活动列表
  listActivity: data => {
    return request(`/hzsx/activityConfig/listActivity`, data);
  },
  //删除活动
  deleteActivity: data => {
    return request(`/hzsx/activityConfig/deleteActivity`, data);
  },
  //新增活动
  addActivity: data => {
    return request(`/hzsx/activityConfig/addActivity`, data);
  },
//查询活动详情
  queryActivityDetail: data => {
    return request(`/hzsx/activityConfig/queryActivityDetail`, data);
  },
//修改活动详情
  modifyActivity: data => {
    return request(`/hzsx/activityConfig/modifyActivity`, data);
  },
  
  
};
