import { parse } from 'querystring';
import pathRegexp from 'path-to-regexp';
import moment from 'moment';
import { routerRedux } from 'dva/router';
import { uploadUrl } from '../config';
import { getToken } from '@/utils/localStorage';
import { orderCloseStatusMap } from '@/utils/enum.js'

/* eslint no-useless-escape:0 import/prefer-default-export:0 */
const reg = /(((^https?:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+(?::\d+)?|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)$/;
export const isUrl = path => reg.test(path);
export const isAntDesignPro = () => {
  if (ANT_DESIGN_PRO_ONLY_DO_NOT_USE_IN_YOUR_PRODUCTION === 'site') {
    return true;
  }

  return window.location.hostname === 'preview.pro.ant.design';
}; // 给官方演示站点用，用于关闭真实开发环境不需要使用的特性

export const isAntDesignProOrDev = () => {
  const { NODE_ENV } = process.env;

  if (NODE_ENV === 'development') {
    return true;
  }

  return isAntDesignPro();
};
export const getPageQuery = () => parse(window.location.href.split('?')[1]);
/**
 * props.route.routes
 * @param router [{}]
 * @param pathname string
 */

export const getAuthorityFromRouter = (router = [], pathname) => {
  const authority = router.find(
    ({ routes, path = '/' }) =>
      (path && pathRegexp(path).exec(pathname)) ||
      (routes && getAuthorityFromRouter(routes, pathname)),
  );
  if (authority) return authority;
  return undefined;
};
export const getRouteAuthority = (path, routeData) => {
  let authorities;
  routeData.forEach(route => {
    // match prefix
    if (pathRegexp(`${route.path}/(.*)`).test(`${path}/`)) {
      if (route.authority) {
        authorities = route.authority;
      } // exact match

      if (route.path === path) {
        authorities = route.authority || authorities;
      } // get children authority recursively

      if (route.routes) {
        authorities = getRouteAuthority(path, route.routes) || authorities;
      }
    }
  });
  return authorities;
};
export function getTimeDistance(type) {
  const now = new Date();
  const oneDay = 1000 * 60 * 60 * 24;

  if (type === 'today') {
    now.setHours(0);
    now.setMinutes(0);
    now.setSeconds(0);
    return [moment(now), moment(now.getTime() + (oneDay - 1000))];
  }
  if (type === 'yesterday') {
    now.setHours(0);
    now.setMinutes(0);
    now.setSeconds(0);
    var day1 = new Date();
    day1.setTime(day1.getTime() - 24 * 60 * 60 * 1000);
    var s1 = day1.getFullYear() + '-' + (day1.getMonth() + 1) + '-' + day1.getDate();
    return [moment(s1), moment(s1)];
  }

  if (type === 'week') {
    let day = now.getDay();

    const beginTime = now.getTime();

    return [moment(beginTime - (6 * oneDay - 1000)), moment(beginTime)];
  }

  if (type === 'month') {
    let day = now.getDay();

    const beginTime = now.getTime();

    return [moment(beginTime - (30 * oneDay - 1000)), moment(beginTime)];
  }

  const year = now.getFullYear();
  return [moment(`${year}-01-01 00:00:00`), moment(`${year}-12-31 23:59:59`)];
}
//名字格式
export const formatName = name => {
  // let newStr;
  // if (name.length === 2) {
  //   newStr = name.substr(0, 1) + '*';
  // } else if (name.length > 2) {
  //   let char = '';
  //   for (let i = 0, len = name.length - 2; i < len; i++) {
  //     char += '*';
  //   }
  //   newStr = name.substr(0, 1) + char + name.substr(-1, 1);
  // } else {
  //   newStr = name;
  // }
  let newStr = name.substr(0, 1) + '**';
  return newStr;
};
//身份证
export const forIdCard = num => {
  let newNum = num.substr(0, 4) + '*******' + num.substr(14, 18);
  return newNum;
};
export function onTableData(e) {
  if (!!e) {
    const customData =
      e === []
        ? []
        : e.map((item, sign) => {
            const newsItem = { ...item };
            const keys = sign + 1;
            newsItem.key = keys;
            return newsItem;
          });
    return customData;
  } else {
    return [];
  }
}
export function getParam(name) {
  const search = document.location.href;
  const pattern = new RegExp('[?&]' + name + '=([^&]+)', 'g');
  const matcher = pattern.exec(search);
  let items = null;
  if (matcher !== null) {
    try {
      items = decodeURIComponent(decodeURIComponent(matcher[1]));
    } catch (e) {
      try {
        items = decodeURIComponent(matcher[1]);
      } catch (e) {
        items = matcher[1];
      }
    }
  }
  return items;
}

export function goToRouter(dispatch, path) {
  dispatch(routerRedux.push(path));
}

export function downloadFile(href, filename = '') {
  if (href) {
    const download = document.createElement('a');
    download.download = filename;
    download.style.display = 'none';
    download.href = href;
    document.body.appendChild(download);
    download.click();
    document.body.removeChild(download);
  } else {
    throw Error('下载链接不正确');
  }
}

export const bfUploadFn = param => {
  const serverURL = uploadUrl;
  const xhr = new XMLHttpRequest();
  const fd = new FormData();

  const successFn = () => {
    // 假设服务端直接返回文件上传后的地址
    // 上传成功后调用param.success并传入上传后的文件地址
    console.log(xhr.response);
    param.success({
      url: JSON.parse(xhr.responseText).data,
    });
  };

  const progressFn = event => {
    // 上传进度发生变化时调用param.progress
    param.progress((event.loaded / event.total) * 100);
  };

  const errorFn = () => {
    // 上传发生错误时调用param.error
    param.error({
      msg: 'unable to upload.',
    });
  };

  xhr.upload.addEventListener('progress', progressFn, false);
  xhr.addEventListener('load', successFn, false);
  xhr.addEventListener('error', errorFn, false);
  xhr.addEventListener('abort', errorFn, false);

  fd.append('file', param.file);
  xhr.open('POST', serverURL, true);
  xhr.setRequestHeader('token', getToken());
  xhr.send(fd);
};

/**
 * 在新的窗口中打开链接
 * @param {*} path ： 链接信息
 */
export const openLinkInNewWindow = path => {
  let { origin } = location; // 域名URL信息
  if (!(origin.includes('127.0.0.1') || origin.includes('localhost'))) {
    // 在非开发环境上面都需要携带上 ope 这个前缀
    origin = `${origin}/ope`;
  }
  let fullUrl;
  if (path.startsWith('/')) fullUrl = `${origin}/#${path}`;
  else fullUrl = `${origin}/#/${path}`;
  window.open(fullUrl, '_blank');
};

/**
 * 返回收货人地址，street在设计上就只是街道数据，但是有些用户在写街道的时候会自个加上省市区，所以这里做个判断
 * @param {object} addressVO : 地址信息数据对象
 */
export const returnAddress = addressVO => {
  if (!addressVO || Object.prototype.toString.call(addressVO) !== '[object Object]') return '';
  const { provinceStr, cityStr, areaStr, street } = addressVO;
  if (
    street &&
    street.includes(provinceStr) &&
    street.includes(cityStr) &&
    street.includes(areaStr)
  )
    return street; // 如果街道信息包含了省市区的话，那么默认它就是详细地址
  return `${provinceStr}-${cityStr}-${areaStr}-${street}`;
};
/**
 * 图片压缩
 * @param {object} file :图片文件信息
 * @param {string} width :宽
 * @param {string} height :高
 */
export const compression = (file, width, height) => {
  return new Promise(resolve => {
    const reader = new FileReader(); // 创建 FileReader
    reader.onload = ({ target: { result: src } }) => {
      const image = new Image(); // 创建 img 元素
      image.onload = async () => {
        const canvas = document.createElement('canvas'); // 创建 canvas 元素
        canvas.width = width || image.width;
        canvas.height = height || image.height;
        let context = canvas.getContext('2d');
        // 在canvas绘制前填充白色背景
        context.fillStyle = '#fff';
        context.fillRect(0, 0, width || image.width, height || image.height);
        context.drawImage(image, 0, 0, width || image.width, height || image.height); // 绘制 canvas
        const canvasURL = canvas.toDataURL('image/jpeg', 0.5);
        const buffer = atob(canvasURL.split(',')[1]);
        let length = buffer.length;
        const bufferArray = new Uint8Array(new ArrayBuffer(length));
        while (length--) {
          bufferArray[length] = buffer.charCodeAt(length);
        }
        const miniFile = new File([bufferArray], file.name, { type: 'image/jpeg' });
        miniFile.uid = 0;
        resolve({
          file: miniFile,
          origin: file,
          beforeSrc: src,
          afterSrc: canvasURL,
          beforeKB: Number((file.size / 1024).toFixed(2)),
          afterKB: Number((miniFile.size / 1024).toFixed(2)),
        });
      };
      image.src = src;
    };
    reader.readAsDataURL(file);
  });
};

/**
 * 返回归还快递的中文描述
 * @param {Object} productObj ： 接口返回的商品对象
 */
export const returnGuiHuanTxt = productObj => {
  const placeholder = '-';
  if (Object.prototype.toString.call(productObj) !== '[object Object]') return placeholder;
  const { returnfreightType } = productObj;

  const en2cnMap = {
    FREE: '用户支付',
    PAY: '商家承担',
  };
  return en2cnMap[returnfreightType] || placeholder;
};

// 进行减法操作
export const makeSub = (arg1, arg2) => {
  let r1, r2, m
  try {
    r1 = arg1.toString().split('.')[1].length;
  } catch (e) {
    r1 = 0
  }
  try {
    r2 = arg2.toString().split('.')[1].length;
  } catch (e) {
    r2 = 0;
  }
  m = Math.pow(10, Math.max(r1, r2))
  return (arg1 * m - arg2 * m) / m
}

/**
 * 渲染订单状态，如果订单已关闭的话，那么把关单原因也给展示出来
 * @param {*} record 
 */
export const renderOrderStatus = record => {
  if (!record || !record.status) return ""
  const { status } = record
  
  const orderStatusMap = {
    '01': '待支付',
    '02': '支付中',
    '03': '已支付申请关单',
    '11': '待审批',
    '04': '待发货',
    '05': '待确认收货',
    '06': '租用中',
    '07': '待结算',
    '08': '结算待支付',
    '09': '订单完成',
    '10': '交易关闭',
    '12': '待归还',
  };

  if (status === "10") {
    return (
      <span>
        {orderStatusMap[status]}({orderCloseStatusMap[record.closeType]})
      </span>
    );
  }
  return <span>{orderStatusMap[status]}</span>
}


export const convertCurrency = money => {
  //汉字的数字
  let cnNums = new Array('零', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖');
  //基本单位
  let cnIntRadice = new Array('', '拾', '佰', '仟');
  //对应整数部分扩展单位
  let cnIntUnits = new Array('', '万', '亿', '兆');
  //对应小数部分单位
  let cnDecUnits = new Array('角', '分', '毫', '厘');
  //整数金额时后面跟的字符
  let cnInteger = '整';
  //整型完以后的单位
  let cnIntLast = '元';
  //最大处理的数字
  let maxNum = 999999999999999.9999;
  //金额整数部分
  let integerNum;
  //金额小数部分
  let decimalNum;
  //输出的中文金额字符串
  let chineseStr = '';
  //分离金额后用的数组，预定义
  let parts;
  if (money == '') {
    return '';
  }
  money = parseFloat(money);
  if (money >= maxNum) {
    //超出最大处理数字
    return '';
  }
  if (money == 0) {
    chineseStr = cnNums[0] + cnIntLast + cnInteger;
    return chineseStr;
  }
  //转换为字符串
  money = money.toString();
  if (money.indexOf('.') == -1) {
    integerNum = money;
    decimalNum = '';
  } else {
    parts = money.split('.');
    integerNum = parts[0];
    decimalNum = parts[1].substr(0, 4);
  }
  //获取整型部分转换
  if (parseInt(integerNum, 10) > 0) {
    let zeroCount = 0;
    let IntLen = integerNum.length;
    for (let i = 0; i < IntLen; i++) {
      let n = integerNum.substr(i, 1);
      let p = IntLen - i - 1;
      let q = p / 4;
      let m = p % 4;
      if (n == '0') {
        zeroCount++;
      } else {
        if (zeroCount > 0) {
          chineseStr += cnNums[0];
        }
        //归零
        zeroCount = 0;
        chineseStr += cnNums[parseInt(n)] + cnIntRadice[m];
      }
      if (m == 0 && zeroCount < 4) {
        chineseStr += cnIntUnits[q];
      }
    }
    chineseStr += cnIntLast;
  }
  //小数部分
  if (decimalNum != '') {
    let decLen = decimalNum.length;
    for (let i = 0; i < decLen; i++) {
      let n = decimalNum.substr(i, 1);
      if (n != '0') {
        chineseStr += cnNums[Number(n)] + cnDecUnits[i];
      }
    }
  }
  if (chineseStr == '') {
    chineseStr += cnNums[0] + cnIntLast + cnInteger;
  } else if (decimalNum == '') {
    chineseStr += cnInteger;
  }
  return chineseStr;
}

/**
 * 字符串的判空显示
 * @param {*} val 
 * @param {*} placeholder 
 * @returns 
 */
 export const strUi = (val, placeholder="-") => {
  if (val == undefined || val === "") return placeholder
  return val
}

/**
 * 对antd timepicker所选择的时间做一个转换，接口所需的时间格式有些不同
 * @param {Object} originalData : 传参对象
 */
export function formatTime2ApiNeed(originalData) {
  if (Object.prototype.toString.call(originalData) !== "[object Object]") {
    console.error("入参必须为对象")
    return {}
  }

  const { times } = originalData
  if (times && times.length) { // 说明用户交互选中了时间
    const time1 = times[0] // 开始时间
    const time2 = times[1] // 结束时间
    time1 && (originalData.startTime = time1.format("YYYY-MM-DD 00:00:00"))
    time2 && (originalData.endTime = time2.format("YYYY-MM-DD 23:59:59"))
  }

  return originalData
}