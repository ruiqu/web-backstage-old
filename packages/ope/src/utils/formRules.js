export default {
  required: function (message) {
    return { required: true, message, whitespace: true }
  },
  phone: { required: true, pattern: new RegExp(/^(?:(?:\+|00)86)?1[3-9]\d{9}$/, "g") , message: '请输入正确的手机号' },
  float: { pattern: /^(([1-9]\d*)(\.\d{1,2})?|0\.\d{1,2})$/g, message: '请输入最多两位小数，且不为0', trigger: 'change' }, // 正整数或小数
  
}
