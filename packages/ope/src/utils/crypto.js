import CryptoJS from 'crypto-js';

export const decrypt = (str, key = 'Card@Decode12345') => {
  var key = CryptoJS.enc.Utf8.parse(key);
  var decrypt = CryptoJS.AES.decrypt(str, key, {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7,
  });
  return CryptoJS.enc.Utf8.stringify(decrypt).toString();
};

export const encrypt = (str, key = 'Card@Decode12345') => {
  var sKey = CryptoJS.enc.Utf8.parse(key);
  var sContent = CryptoJS.enc.Utf8.parse(str);
  var encrypted = CryptoJS.AES.encrypt(sContent, sKey, {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7,
  });
  return encrypted.toString();
};
