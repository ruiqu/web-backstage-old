// 本地如果用域名接口报错，请用ip，发布的时候请用域名
// export const baseUrl = 'http://139.224.164.75:9000/zwzrent-ope/';//广鼎环境

export const baseUrl = 'http://zyjtest.zwzrent.net/'; //自己搭建的测试环境

// export const baseUrl = 'https://www.zwzrent.com/zwzrent-ope/';//广鼎线上环境
const { NODE_ENV } = process.env;


export const uploadUrl = NODE_ENV === 'development' ? 'http://192.168.110.152:9002/zyj-backstage-web/hzsx/busShop/doUpLoadwebp' : '/zyj-backstage-web/hzsx/busShop/doUpLoadwebp';
