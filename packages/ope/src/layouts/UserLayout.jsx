import { DefaultFooter, getMenuData, getPageTitle } from '@ant-design/pro-layout';
import { Helmet } from 'react-helmet';
import { Link } from 'umi';
import React, { useEffect } from 'react';
import { connect } from 'dva';
import { formatMessage } from 'umi-plugin-react/locale';
import SelectLang from '@/components/SelectLang';
import logo from '../assets/logo.svg';
import styles from './UserLayout.less';
import logoBg from '@/assets/image/logo-bg.png';
import autofit from 'autofit.js';

const UserLayout = props => {
  useEffect(() => {
    autofit.init(
      {
        designHeight: 1080,
        designWidth: 1920,
        renderDom: '#root',
        resize: true,
      },
      false,
    );
    return () => {
      autofit.off();
    };
  });
  const {
    route = {
      routes: [],
    },
  } = props;
  const { routes = [] } = route;
  const {
    children,
    location = {
      pathname: '',
    },
  } = props;
  const { breadcrumb } = getMenuData(routes);
  const title = getPageTitle({
    pathname: location.pathname,
    breadcrumb,
    formatMessage,
    ...props,
  });
  return (
    <>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={title} />
      </Helmet>

      <div
        className={styles.container}
        style={{
          position: 'relative',
        }}
      >
        <div
          style={{
            position: 'absolute',
            left: 56,
            top: 46,
            color: '#1442E3',
            fontSize: '32px',
            fontWeight: 800,
          }}
        >
          {window.location.hostname == 'kksc.richtech123.com' ? '可可运营后台管理系统' : ''}
          {window.location.hostname == 'ddyg.yuexiaozu.com' ? '百合运营后台管理系统' : ''}
          {window.location.hostname == 'wxsc.lzyigou.com' ? '大福运营后台管理系统' : ''}
          {window.location.hostname == 'xksc.lzyigou.com' ? '星空运营后台管理系统' : ''}
          {window.location.hostname == 'ys.yuexiaozu.com' ? '来这易购运营后台管理系统' : ''}
          {window.location.hostname == 'hwg.rhdzsw.cn' ? '三只鸭运营后台管理系统' : ''}
        </div>
        <div
          style={{
            position: 'absolute',
            top: 14,
            right: 253,
          }}
        >
          <img width={586} height={254} src={logoBg} alt="" srcset="" />
        </div>
        {children}
      </div>
    </>
  );
};

export default connect(({ settings }) => ({ ...settings }))(UserLayout);
