import request from '@/utils/request';
function getToken() {
  const token = localStorage.getItem('token');
  return token;
}

//查询部落评论分页
export async function queryTribeCommentPage(params) {
  return request(`/hzsx/tribeComment/queryTribeCommentPage`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//获取小程序评论分页
export async function queryAppletsCommentPage(params) {
  return request(`/hzsx/appletsComment/queryAppletsCommentPage`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//获取小程序订单投诉分页
export async function queryOrderComplaintsPage(params) {
  return request(`/hzsx/orderComplaints/queryOrderComplaintsPage`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//获取小程序订单投诉详情
export async function queryOrderComplaintsDetail(params) {
  return request(`/hzsx/orderComplaints/queryOrderComplaintsDetail`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//录入小程序订单投诉处理结果
export async function modifyOrderComplaints(params) {
  return request(`/hzsx/orderComplaints/modifyOrderComplaints`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}

//获取订单投诉类型集合
export async function getOrderComplaintsTypes(params) {
  return request(`/hzsx/orderComplaints/getOrderComplaintsTypes`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}

//录入小程序处理信息
export async function modifyAppletsComment(params) {
  return request(`/hzsx/appletsComment/modifyAppletsComment`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}

//分页查询运营导入商品评论
export async function queryProductEvaluationPage(params) {
  return request(`/hzsx/productEvaluation/queryProductEvaluationPage`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}

//查询部落评论编辑信息
export async function selectTribeCommentEdit(params) {
  return request(`/hzsx/tribeComment/selectTribeCommentEdit?id=${params.id}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}
//部落评论客服回复确认
export async function confirmTribeCommment(params) {
  return request(`/hzsx/tribeComment/confirmTribeCommment`, {
    method: 'post',
    data: params,
    headers: { token: getToken() },
  });
}
//商品评论客服回复确认
export async function confirmProductEvaluation(params) {
  return request(`/hzsx/productEvaluation/confirmProductEvaluation`, {
    method: 'post',
    data: params,
    headers: { token: getToken() },
  });
}

//批量生效部落评论
export async function batchEffectUserComment(params) {
  return request(`/hzsx/tribeComment/batchEffectUserComment`, {
    method: 'post',
    data: params,
    headers: { token: getToken() },
  });
}
//批量生效商品评论
export async function batchEffectProductEvaluation(params) {
  return request(`/hzsx/productEvaluation/batchEffectProductEvaluation`, {
    method: 'post',
    data: params,
    headers: { token: getToken() },
  });
}

export async function selectExamineProductEdit(params) {
  return request(`/hzsx/productEvaluation/selectExamineProductEdit?id=${params.id}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}

export async function exportUserComment(params) {
  return request(`/hzsx/tribeComment/exportUserComment`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}
export async function exportProductEvaluation(params) {
  return request(`/hzsx/productEvaluation/exportProductEvaluation`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}

//导入部落评论
export async function importUserComment(params) {
  return request(`/hzsx/tribeComment/importUserComment?url=${params.url}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}
//导入商品评论

export async function importProductEvaluation(params) {
  return request(`/hzsx/productEvaluation/importProductEvaluation?url=${params.url}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}
//删除部落评论
export async function deleteUserComment(params) {
  return request(`/hzsx/tribeComment/deleteUserComment?id=${params.id}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}
//删除商品评论
export async function deleteProductEvaluation(params) {
  return request(`/hzsx/productEvaluation/deleteProductEvaluation?id=${params.id}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}

//导出部落评论
export async function exportUserCommentMessage(params) {
  return request(`/hzsx/tribeComment/exportUserCommentMessage`, {
    method: 'post',
    data: params,
    headers: { token: getToken() },
  });
}
//导出商品评论消息
export async function exportProductEvaluationMessage(params) {
  return request(`/hzsx/productEvaluation/exportProductEvaluationMessage`, {
    method: 'post',
    data: params,
    headers: { token: getToken() },
  });
}
