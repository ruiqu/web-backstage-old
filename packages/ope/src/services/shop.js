import request from '@/utils/request';
function getToken() {
  const token = localStorage.getItem('token');
  return token;
}
//店铺审核列表
export async function shopList(params) {
  return request('/hzsx/opeShop/toShopExamineList', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//店铺导出
export async function exportShop(params) {
  return request('/hzsx/opeShop/exportShop', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//店铺详情
export async function selectBusShopInfo(params) {
  return request(`/hzsx/busShop/selectBusShopInfo?shopId=${params.shopId}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}

//查询店铺信息
export async function selectShopInfo(params) {
  return request(`/hzsx/opeShop/selectShopInfo?shopId=${params.shopId}`, {
    method: 'GET',
    data: params,
    headers: { token: getToken() },
  });
}
//店铺审核确认
export async function toShopExamineConform(params) {
  debugger;
  console.log(typeof params.isLocked, 'params.isLocked');
  return request(
    `/hzsx/opeShop/toShopExamineConform?id=${params.id}&reason=${
      params.reason ? params.reason : ''
    }&status=${params.status || params.status === 0 ? params.status : ''}&isLocked=${
      params.isLocked || params.isLocked === 0 ? params.isLocked : ''
    }`,
    {
      method: 'GET',
      data: params,
      headers: { token: getToken() },
    },
  );
}
