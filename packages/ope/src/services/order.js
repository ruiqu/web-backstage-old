import request from '@/utils/request';
function getToken() {
  const token = localStorage.getItem('token');
  return token;
}
//查询所有订单
export async function queryOpeOrderByCondition(params) {
  return request(`/hzsx/ope/order/queryOpeOrderByCondition`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//查询购买订单列表
export async function userOrderPurchase(params) {
  return request(`/hzsx/purchaseOrder/page`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}

//查询关闭订单
export async function queryPendingOrderClosureList(params) {
  return request('/hzsx/ope/order/queryPendingOrderClosureList', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//关闭订单并退款
export async function closeUserOrderAndRefundPrice(params) {
  return request('/hzsx/ope/order/closeUserOrderAndRefundPrice', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//查询逾期订单列表
export async function queryOpeOverDueOrdersByCondition(params) {
  return request('/hzsx/ope/order/queryOpeOverDueOrdersByCondition', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
// 查询未归还订单列表
export async function queryWaitingGiveBackOrder(params) {
  return request('/hzsx/ope/order/queryWaitingGiveBackOrder', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//查询电审订单
export async function queryTelephoneAuditOrder(params) {
  return request('/hzsx/ope/order/queryTelephoneAuditOrder', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//电审审核
export async function telephoneAuditOrder(params) {
  return request('/hzsx/ope/order/telephoneAuditOrder', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//查询运营买断订单
export async function queryOpeBuyOutOrdersByCondition(params) {
  return request('/hzsx/ope/order/queryOpeBuyOutOrdersByCondition', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//查询续租订单
export async function queryReletOrderByCondition(params) {
  return request('/hzsx/ope/order/queryReletOrderByCondition', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//获取上架渠道集合
export async function PlateformChannel(params) {
  return request('/hzsx/platform/selectPlateformChannel', {
    method: 'GET',
    data: params,
    headers: { token: getToken() },
  });
}
//订单备注
export async function orderRemark(params) {
  return request('/hzsx/ope/order/orderRemark', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//查询订单详细
export async function queryOpeUserOrderDetail(params) {
  return request(`/hzsx/ope/order/queryOpeUserOrderDetail`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//查询备注
export async function queryOrderRemark(params) {
  return request(`/hzsx/ope/order/queryOrderRemark`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}

//查询物流信息
export async function queryExpressInfo(params) {
  return request(
    `/hzsx/ope/order/queryExpressInfo?expressNo=${params.expressNo}&receiverPhone=${params.receiverPhone}&shortName=${params.shortName}`,
    {
      method: 'GET',
      headers: { token: getToken() },
    },
  );
}
export async function forceConfirmReceipt(params) {
  return request(
    `/hzsx/ope/order/forceConfirmReceipt`,
    {
      method: 'POST',
      data: params,
      headers: { token: getToken() },
    },
  );
}
export async function expressInfo(params) {
  return request(`/hzsx/purchaseOrder/expressInfo?orderId=${params.orderId}`, {
    method: 'GET',
    headers: { token: getToken() },
  });
}

export async function contractReport(params) {
  return request(`/hzsx/ope/order/contractReport`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//查询运营买断订单详情
export async function queryOpeBuyOutOrderDetail(params) {
  return request(`/hzsx/ope/order/queryOpeBuyOutOrderDetail`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}

//查询续租订单详情
export async function queryUserReletOrderDetail(params) {
  return request(`/hzsx/ope/order/queryUserReletOrderDetail`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//查询续租订单详情
export async function userOrdersPurchase(params) {
  return request(`/hzsx/purchaseOrder/detail?orderId=${params.orderId}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}
export async function purchaseOrder(params) {
  return request('/hzsx/purchaseOrder/cancel', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//后台获取区域信息
export async function selectDistrict(params) {
  return request('/hzsx/district/selectDistrict', {
    method: 'GET',
    headers: { token: getToken() },
  });
}

//修改用户收货地址
export async function opeOrderAddressModify(params) {
  return request(`/hzsx/ope/order/opeOrderAddressModify`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//订单导出
export async function exportOpeAllUserOrders(params) {
  return request(`/hzsx/ope/order/exportOpeAllUserOrders`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
export async function purchaseOrderExport(params) {
  return request(`/hzsx/purchaseOrder/export`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
export async function purchaseOrderDelivery(params) {
  return request(`/hzsx/purchaseOrder/delivery`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//买断订单导出

export async function exportOpeBuyOutUserOrders(params) {
  return request(`/hzsx/ope/order/exportOpeBuyOutUserOrders`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}

//逾期订单导出
export async function exportOpeOverDueUserOrders(params) {
  return request(`/hzsx/ope/order/exportOpeOverDueUserOrders`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//续租订单导出
export async function exportReletOrders(params) {
  return request(`/hzsx/ope/order/exportReletOrders`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//待归还订单导出
export async function exportWaitingGiveBack(params) {
  return request(`/hzsx/ope/order/exportWaitingGiveBack`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//电审订单导出
export async function exportTelephoneAuditOrders(params) {
  return request(`/hzsx/ope/order/exportTelephoneAuditOrders`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//订单关闭和退款导出
export async function exportCloseRefundOrders(params) {
  return request(`/hzsx/ope/order/exportCloseRefundOrders`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}

export async function getSiriusReportRecord(params) {
  return request(`/hzsx/ope/order/querySiriusReportByUid`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}

//获取百融风控
export async function getbaironReportRecord(params) {
  let tp=params.type
  if(tp!=1)tp=0
  return request(`/hzsx/business/order/queryWhetherRiskReportNew?orderId=${params.orderId}&type=${tp}`, {
    method: 'GET',
    //data: params,
    headers: { token: getToken() },
  });
}

//查询订单账单相关信息
export async function queryOrderStagesDetail(params) {
  return request('/hzsx/ope/order/queryOrderStagesDetail', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}

// 查询最近导出的订单列表
export async function fetchAllDownloadUrls() {
  return request('/hzsx/export/exportHistory', {
    method: 'GET',
    headers: { token: getToken() },
  });
}

import _request from '@/services/baseService';

export default {
  orderDelivery: data => {
    return _request(`/hzsx/ope/order/orderDelivery`, data);
  },
  merchantsIssuedStatements: data => {
    return _request(`/hzsx/ope/order/merchantsIssuedStatements`, data);
  },
  selectExpressList: data => {
    return _request(`/hzsx/platformExpress/selectExpressList`, data, 'get');
  },
  businessClosePayedOrder: data => {
    return _request(`/hzsx/ope/order/businessClosePayedOrder`, data);
  },
  queryOrderStatusTransfer: data => {
    return _request(`/hzsx/ope/order/queryOrderStatusTransfer`, data);
  },
  queryOrderAuditRecord: data => {
    return _request(`/hzsx/ope/order/queryOrderAuditRecord`, data);
  },
  telephoneAuditOrder: data => {
    return _request(`/hzsx/ope/order/telephoneAuditOrder`, data);
  },
  closeUserOrderAndRefundPrice: data => {
    return _request(`/hzsx/ope/order/closeUserOrderAndRefundPrice`, data);
  },
  queryOrderHasten: data => {
    return _request(`/hzsx/ope/order/queryOrderHasten`, data);
  },
  orderHasten: data => {
    return _request(`/hzsx/ope/order/orderHasten`, data);
  },
  ajaxForExportRentOrders: data => {
    return _request(`/zyj-backstage-web/hzsx/export/rentOrder`, data);
  }, // 导出租赁订单数据
  ajaxForExportBuyOutOrders: data => {
    return _request(`/zyj-backstage-web/hzsx/export/buyOut`, data);
  }, // 适用于导出中心的导出买断订单
  ajaxForExportNotReturnOrders: data => {
    return _request(`/zyj-backstage-web/hzsx/export/notGiveBack`, data);
  }, // 适用于导出中心的导出未归还订单
  ajaxForExportOverdueOrders: data => {
    return _request('/zyj-backstage-web/hzsx/export/overdueOrder', data);
  }, // 导出中心的导出逾期订单
  ajaxForExportPurchaseOrders: data => {
    return _request('/zyj-backstage-web/hzsx/export/purchaseOrder', data);
  }, // 导出中心导出购买订单
  forceDepositRefund: data => {
    return _request('/hzsx/ope/order/forceDepositRefund', data,'get');
  }, // 退押金
  queryPayDepositLog: data => {
    return _request('/hzsx/ope/order/queryPayDepositLog', data,'get');
  }, // 查询押金信息
  updatePayDepositAmount: data => {
    return _request('/hzsx/ope/order/updatePayDepositAmount', data);
  }, // 修改押金信息
  transferOrder: data => {
    return _request('/hzsx/ope/order/transferOrder', data);
  }, // 转单
};
