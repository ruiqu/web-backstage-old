import request from '@/utils/request';
import request2 from '@/utils/lz-request2';
function getToken() {
  const token = localStorage.getItem('token');
  return token;
}
export async function Login(params) {
  return request('/hzsx/user/login', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
export async function getFakeCaptcha(mobile) {
  return request(`/api/login/captcha?mobile=${mobile}`);
}

export async function homeData(params) {
  return request('/hzsx/ope/order/opeOrderStatistics', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
export async function queryOrderReport(params) {
  return request('/hzsx/ope/order/queryOrderReport', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
export async function selectProductCounts(params) {
  return request('/hzsx/home/selectProductCounts', {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}

export async function sendValidateCode(params) {
  return request2(`/zyj-api-web/hzsx/aliPay/user/sendSmsCodePaaS?mobile=${params.mobile}&code=001&overtime=${params.overtime}&isCheckCaptcha=${params.isCheckCaptcha}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}

export async function restPassword(params) {
  return request('/hzsx/user/restUserPassword', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
export async function getUserStatistics(params) {
  return request(`/hzsx/user/getUserStatistics`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}
