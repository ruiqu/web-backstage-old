import request from "@/services/baseService";

export default {
  selectPlateformChannel:(data)=>{
    return request(`/hzsx/platform/selectPlateformChannel`, data, 'get')
  },
  getTabList:(data)=>{
    return request(`/hzsx/index/getTabList`, data, 'get')
  },
}
