import baseRequest from '@/utils/lz-request';
import baseRequest2 from '@/utils/lz-request2';
import { getToken } from "@/utils/localStorage";


export default function request(url, value = {}, method = 'post', options = {}) {
  let token = getToken()
  if (url.includes('/zyj-api-web/')) {
    if (method === 'post') {
      return baseRequest2(url, {
        method: 'post',
        data: value,
        headers: {
          token
        }
      })
    } else if (method === 'get') {
      return baseRequest2(url, { params: value, headers: { token } }, options)
    } else if (method === 'formdata') {
      return baseRequest2({
        method: 'post',
        url: url,
        data: value,
        transformRequest: [
          function (data) {
            let ret = ''
            for (const it in data) {
              ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
            }
            ret = ret.substring(0, ret.length - 1)
            return ret
          }
        ],
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          token
        }
      })
    }
  } else {
    if (method === 'post') {
      return baseRequest(url, {
        method: 'post',
        data: value,
        headers: {
          token
        }
      })
    } else if (method === 'get') {
      return baseRequest(url, { params: value, headers: { token } }, options)
    } else if (method === 'formdata') {
      return baseRequest({
        method: 'post',
        url: url,
        data: value,
        transformRequest: [
          function (data) {
            let ret = ''
            for (const it in data) {
              ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
            }
            ret = ret.substring(0, ret.length - 1)
            return ret
          }
        ],
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          token
        }
      })
    } else if (method === 'delete'){
      return baseRequest(url, {
        method: 'delete',
        data: value,
        headers: {
          token
        }
      })
    }else if (method === 'put'){
      return baseRequest(url, {
        method: 'put',
        data: value,
        headers: {
          token
        }
      })
    }



  }




}
