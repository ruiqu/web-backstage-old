import request from '@/utils/request';
function getToken() {
  const token = localStorage.getItem('token');
  return token;
}
export const getCouponListByPage = body =>
  request('business/shop/getCouponListByPage', {
    method: 'GET',
    body,
  });

export const saveCouponByShopId = body =>
  request('business/shop/saveCouponByShopId', {
    method: 'POST',
    body,
  });

export const updateShopCoupon = body =>
  request('business/shop/updateShopCoupon', {
    method: 'POST',
    body,
  });

export const delCouponById = body =>
  request('business/shop/delCouponById', {
    method: 'GET',
    body,
  });
export const setBusinessPrdouct = body =>
  request('business/product/selectBusinessPrdouct', {
    method: 'POST',
    body,
  });
export const getUpload = body =>
  request('business/coupon/readPhoneFromExcel', {
    method: 'POST',
    body,
  });
// export const couponAdd = body =>
//   request('ope/coupon/add', {
//     method: 'POST',
//     body,
//   });

export async function couponAdd(params) {
  return request('/hzsx/couponTemplate/add', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//优惠券-新增
export async function couponLiteAdd(params) {
  return request('/hzsx/liteCouponTemplate/add', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}


export async function modify(params) {
  return request('/hzsx/couponTemplate/modify', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//优惠券-提交修改
export async function liteModify(params) {
  return request('/hzsx/liteCouponTemplate/modify', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}


export async function couponPackagemodify(params) {
  return request('/hzsx/couponPackage/modify', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//大礼包-修改
export async function liteCouponPackagemodify(params) {
  return request('/hzsx/liteCouponPackage/modify', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}

// export async function list(params) {
//   return request('/hzsx/couponPackage/queryPage', {
//     method: 'POST',
//     data: params,
//     headers: { token: getToken() },
//   });
// }
// export const deleteList = body =>
//   request(`ope/coupon/delete?id=${body.id}`, {
//     method: 'GET',
//   });
export async function deleteList(params) {
  return request(`/hzsx/couponTemplate/delete?id=${params.id}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}
//优惠券-删除
export async function liteDeleteList(params) {
  return request(`/hzsx/liteCouponTemplate/delete?id=${params.id}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}


export async function couponPackageDelete(params) {
  return request(`/hzsx/couponPackage/delete?id=${params.id}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}
//大礼包-删除
export async function liteCouponPackageDelete(params) {
  return request(`/hzsx/liteCouponPackage/delete?id=${params.id}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}
// export const getSecondaryCategory = body =>
//   request(`ope/category/selectReceptionCategoryList?categoryId=${body.id}`,{
//       method: 'GET'
//   })

// export const detailBindList = body =>
//   request('ope/coupon/detailBindList', {
//     method: 'POST',
//     body,
//   });
export const upDate = body =>
  request('ope/coupon/update', {
    method: 'POST',
    body,
  });
export const couponPackage = body =>
  request('ope/couponPackage/add', {
    method: 'POST',
    body,
  });
// export const couponPackageList = body =>
//   request('ope/couponPackage/list', {
//     method: 'POST',
//     body,
//   });
export async function queryPage(params) {
  return request('/hzsx/couponPackage/queryPage', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//lite小程序
export async function liteQueryPage(params) {
  return request('/hzsx/liteCouponPackage/queryPage', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//获取需要配置的小程序
export async function listLitePlatformChannel(params) {
  return request('/hzsx/platform/listLitePlatformChannel', {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}


export async function couponTemplatequeryPage(params) {
  return request('/hzsx/couponTemplate/queryPage', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//lite优惠券-分页查询
export async function liteCouponTemplatequeryPage(params) {
  return request('/hzsx/liteCouponTemplate/queryPage', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}

export async function getAssignAbleTemplate(params) {
  return request('/hzsx/couponTemplate/getAssignAbleTemplate', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//大礼包-查询可以添加到大礼包的优惠券
export async function getliteCouponTemplate(params) {
  return request('/hzsx/liteCouponTemplate/getAssignAbleTemplate', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}


// export const couponPackageDelete = body =>
//   request(`ope/couponPackage/delete?id=${body.id}`, {
//     method: 'get',
//   });
export const getPageData = body =>
  request(`ope/couponPackage/getUpdatePageData?id=${body.id}`, {
    method: 'get',
  });

export async function getUpdatePageDatas(params) {
  return request(`/hzsx/couponPackage/getUpdatePageData?id=${params.id}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}
//大礼包-修改
export async function getLietUpdatePageDatas(params) {
  return request(`/hzsx/liteCouponPackage/getUpdatePageData?id=${params.id}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}

export const getupdate = body =>
  request('ope/couponPackage/update', {
    method: 'POST',
    body,
  });

export async function add(params) {
  return request('/hzsx/couponPackage/add', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}

export async function liteAdd(params) {
  return request('/hzsx/liteCouponPackage/add', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}

// export const detailDataList = body => {
//   return request('ope/coupon/detail', {
//     method: 'GET',
//     body,
//   });
// };

export const getGoods = body => {
  return request('ope/product/selectExaminePoroductList', {
    method: 'POST',
    body,
  });
};

// export const getSelectAlls = body =>
//   request(`ope/category/selectAll`, {
//     method: 'GET',
//   });

export async function getSelectAlls(params) {
  return request(`/hzsx/category/selectParentCategoryList`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}

// export async function couponTemplatequeryPage(params) {
//   return request('/hzsx/couponTemplate/queryPage', {
//     method: 'POST',
//     data: params,
//     headers: { token: getToken() },
//   });
// }
export async function detailBindList(params) {
  return request(`/hzsx/userCoupon/queryPage`, {
    method: 'post',
    data: params,
    headers: { token: getToken() },
  });
}
//查看领取用户情况
export async function liteDetailBindList(params) {
  return request(`/hzsx/liteUserCoupon/queryPage`, {
    method: 'post',
    data: params,
    headers: { token: getToken() },
  });
}
export async function exportEntityNum(params) {
  return request(`/hzsx/couponTemplate/exportEntityNum?id=${params.id}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}
//优惠券-券码券导出券码
export async function liteExportEntityNum(params) {
  return request(`/hzsx/liteCouponTemplate/exportEntityNum?id=${params.id}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}
// export const getUpdatePageData = body =>
//   request(`ope/coupon/getUpdatePageData?id=${body.id}`, {
//     method: 'get',
//   });
export async function getUpdatePageData(params) {
  return request(`/hzsx/couponTemplate/getUpdatePageData?id=${params.id}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}
//优惠券-编辑页面获取数据
export async function getLiteUpdatePageData(params) {
  return request(`/hzsx/liteCouponTemplate/getUpdatePageData?id=${params.id}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}


export async function addBindUrl(params) {
  return request(`/hzsx/couponTemplate/addBindUrl`, {
    method: 'post',
    data: params,
    headers: { token: getToken() },
  });
}


//优惠券-券码券-配置链接
export async function liteAddBindUrl(params) {
  return request(`/hzsx/liteCouponTemplate/addBindUrl`, {
    method: 'post',
    data: params,
    headers: { token: getToken() },
  });
}


