export default {
  navTheme: 'dark',
  // 拂晓蓝
  primaryColor: '#5476F0',
  layout: 'sidemenu',
  contentWidth: 'Fluid',
  fixedHeader: false,
  autoHideHeader: false,
  fixSiderbar: false,
  colorWeak: false,
  menu: {
    locale: true,
  },
  title: '运营管理平台',
  pwa: false,
  iconfontUrl: '',
};
