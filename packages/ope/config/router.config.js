export default [
  {
    path: '/user',
    component: '../layouts/UserLayout',
    routes: [
      {
        name: 'login',
        path: '/user/login',
        component: './user/login',
      },
      {
        name: 'ChangePassword',
        path: '/user/ChangePassword',
        component: './user/ChangePassword',
      },
      {
        name: 'PasswordStatuspage',
        path: '/user/PasswordStatuspage',
        component: './user/PasswordStatuspage',
      },
      {
        component: './404',
      },
    ],
  },
  {
    path: '/',
    component: '../layouts/SecurityLayout',

    routes: [
      {
        path: '/',
        component: '../layouts/BasicLayout',
        // authority: ['admin', 'user'],
        routes: [
          {
            path: '/',
            redirect: '/Home',
          },
          {
            path: '/Home',
            name: 'Home',

            icon: 'bank',
            component: './Home',
          },
          // 商品管理
          {
            path: '/goods',
            name: 'goods',
            icon: 'appstore',
            authority: ['商品审核'],
            routes: [
              {
                path: '/goods/index',
                name: 'index',
                component: './Goods',
                // newTab: true,
              },
              {
                path: '/goods/index/goodsDetail/:id/:actionId',
                name: 'goodsDetail',
                component: './Goods/goodsDetail',
                hideInMenu: true,
              },
              // {
              //   path: '/goods/Purchase',
              //   name: 'Purchase',
              //   component: './Goods/Purchase',
              //   // newTab: true,
              // },
              // {
              //   path: '/goods/Purchase/goodsDetail/:id/:actionId',
              //   name: 'PurchaseGoodsDetail',
              //   component: './Goods/Purchase/goodsDetail',
              //   hideInMenu: true,
              // },
            ],
          },
          {
            path: '/StoreAudit',
            name: 'StoreAudit',
            icon: 'shop',
            authority: ['店铺审核'],
            routes: [
              {
                path: '/StoreAudit/list',
                name: 'index',
                component: './StoreAudit',
                // newTab: true,
              },
              {
                path: '/StoreAudit/list/details',
                name: 'details',
                component: './StoreAudit/details',
                hideInMenu: true,
              },
              {
                component: './404',
              },
            ],
          },
          {
            path: '/Order',
            name: 'Order',
            icon: 'audit',
            hideInMenu: false,
            authority: ['订单管理'],
            routes: [
              {
                path: '/Order/HomePage',
                component: './Order/HomePage',
                hideInMenu: false,
                name: 'HomePage',
                authority: ['订单列表'],
               },

              {
                path: '/Order/Close',
                component: './Order/Close',
                hideInMenu: false,
                name: 'Close',
                authority: ['订单关闭和退货'],
              },

              {
                path: '/Order/BeOverdue',
                component: './Order/BeOverdue',
                hideInMenu: false,
                name: 'BeOverdue',
                authority: ['逾期订单'],
              },
              // {
              //   path: '/Order/OverdueNoReturn',
              //   component: './Order/OverdueNoReturn',
              //   hideInMenu: false,
              //   name: 'OverdueNoReturn',
              //   authority: ['到期未归还订单'],
              // },
              // {
              //   path: '/Order/BuyOut',
              //   component: './Order/BuyOut',
              //   hideInMenu: false,
              //   name: 'BuyOut',
              //   authority: ['买断订单'],
              // },

              // {
              //   path: '/Order/RentRenewal',
              //   component: './Order/RentRenewal',
              //   hideInMenu: false,
              //   name: 'RentRenewal',
              //   authority: ['续租订单'],
              // },
              {
                path: '/Order/RentRenewal/Details',
                component: './Order/Details',
                hideInMenu: true,
                name: 'Details',
              },
              {
                path: '/Order/Details',
                component: './Order/Details',
                hideInMenu: true,
                name: 'Details',
              },
              {
                path: '/Order/HomePage/Details',
                component: './Order/Details',
                hideInMenu: true,
                name: 'Details',
              },
              {
                path: '/Order/BeOverdue/Details',
                component: './Order/Details',
                hideInMenu: true,
                name: 'Details',
              },
              {
                path: '/Order/OverdueNoReturn/Details',
                component: './Order/Details',
                hideInMenu: true,
                name: 'Details',
              },
              {
                path: '/Order/Close/Details',
                component: './Order/Details',
                hideInMenu: true,
                name: 'Details',
              },
              {
                path: '/Order/Purchase/PurchaseDetails',
                component: './Order/PurchaseDetails',
                hideInMenu: true,
                name: 'Details',
              },

              {
                path: '/Order/BuyOut/BuyOutDetails',
                component: './Order/BuyOutDetails',
                hideInMenu: true,
                name: 'BuyOutDetails',
              },

              // {
              //   path: '/Order/Online',
              //   component: './Order/Online',
              //   hideInMenu: false,
              //   name: 'Online',
              //   authority: ['电审订单'],
              // },
              {
                path: '/Order/Online/Details',
                component: './Order/Details',
                hideInMenu: true,
                name: 'Details',
              },
              // {
              //   path: '/Order/Purchase',
              //   component: './Order/Purchase',
              //   hideInMenu: false,
              //   name: 'Purchase',
              //   authority: ['电审订单'],
              // },
              {
                path: '/Order/Transfer',
                component: './Order/Transfer',
                hideInMenu: false,
                name: 'Transfer',
                authority: ['转单订单'],
              },
              {
                component: './404',
              },
            ],
          },

          {
            path: '/Marketing',
            name: 'Marketing',
            icon: 'shopping-cart',
            authority: ['营销管理'],
            hideInMenu: false,
            routes: [
              {
                path: '/Marketing/HomePage',
                component: './Marketing/HomePage',
                hideInMenu: false,
                name: 'HomePage',
                authority: ['模块消息'],
              },
              {
                path: '/Marketing/Comment',
                component: './Marketing/Comment',
                hideInMenu: false,
                name: 'Comment',
                authority: ['评论管理'],
              },
              {
                path: '/Marketing/Comment/See',
                component: './Marketing/Comment/See',
                hideInMenu: true,
                name: 'See',
                authority: ['评论管理'],
              },
              {
                name:'zuwuzuPackage',
                path: '/Marketing/zuwuzuPackage',
                hideInMenu: true,
                routes:[
                  {
                    path: '/Marketing/zuwuzuPackage/Package',
                    component: './Marketing/Package',
                    hideInMenu: false,
                    name: 'Package',
                    authority: ['大礼包'],
                  },
                  {
                    path: '/Marketing/zuwuzuPackage/Coupon/list',
                    hideInMenu: false,
                    name: 'Coupon',
                    authority: ['优惠券'],
                    routes: [
                      {
                        path: '/Marketing/zuwuzuPackage/Coupon/list',
                        component: './Marketing/Coupon/HomePage',
                        hideInMenu: true,
                      },
                      {
                        path: '/Marketing/zuwuzuPackage/Coupon/list/ToView',
                        component: './Marketing/Coupon/ToView',
                        hideInMenu: true,
                      },
                      {
                        path: '/Marketing/zuwuzuPackage/Coupon/list/TheEditorList',
                        component: './Marketing/Coupon/TheEditorList',
                        hideInMenu: true,
                      },
                      {
                        path: '/Marketing/zuwuzuPackage/Coupon/list/add',
                        component: './Marketing/Coupon/AddList',
                        hideInMenu: true,
                      },
                    ],
                  },
                ]
              },
              // {
              //   name:'litePackage',
              //   path: '/Marketing/LitePackage',
              //   routes:[
              //     {
              //       path: '/Marketing/LitePackage/Package',
              //       component: './LiteMarketing/Package',
              //       hideInMenu: false,
              //       name: 'Package',
              //       authority: ['大礼包'],
              //     },
              //     {
              //       path: '/Marketing/LitePackage/Litelist',
              //       hideInMenu: false,
              //       name: 'Coupon',
              //       authority: ['优惠券'],
              //       routes: [
              //         {
              //           path: '/Marketing/LitePackage/Litelist',
              //           component: './LiteMarketing/Coupon/HomePage',
              //           hideInMenu: true,
              //         },
              //         {
              //           path: '/Marketing/LitePackage/Litelist/LiteToView',
              //           component: './LiteMarketing/Coupon/ToView',
              //           hideInMenu: true,
              //         },
              //         {
              //           path: '/Marketing/LitePackage/Litelist/LiteTheEditorList',
              //           component: './LiteMarketing/Coupon/TheEditorList',
              //           hideInMenu: true,
              //         },
              //         {
              //           path: '/Marketing/LitePackage/Litelist/Liteadd',
              //           component: './LiteMarketing/Coupon/AddList',
              //           hideInMenu: true,
              //         },
              //       ],
              //     },
              //   ]
              // },
              
            ],
          },
          {
            path: '/commission',
            name: 'commission',
            icon: 'shopping',
            authority: ['佣金管理'],
            hideInMenu: true,
            routes: [
              {
                path: '/commission/list',
                name: 'index',
                hideInMenu: true,
                component: './Commission',
              },
              {
                path: '/commission/douyin',
                name: 'douyin',
                hideInMenu: true,
                component: './Commission',
              },
              {
                path: '/commission/liteList',
                name: 'liteList',
                component: './Commission/liteList',
              },
              {
                path: '/commission/cost',
                name: 'cost',
                component: './Commission/cost',
              },
              {
                path: '/commission/list/add',
                name: 'add',
                component: './Commission/addAndEdit',
                hideInMenu: true,
              },
              {
                path: '/commission/douyin/add',
                name: 'add',
                component: './Commission/addAndEdit',
                hideInMenu: true,
              },
              {
                path: '/commission/liteList/liteAdd',
                name: 'liteAdd',
                component: './Commission/liteAdd',
                hideInMenu: true,
              },
              
              {
                path: '/commission/list/edit/:id',
                name: 'edit',
                component: './Commission/addAndEdit',
                hideInMenu: true,
              },
              {
                path: '/commission/douyin/edit/:id',
                name: 'edit',
                component: './Commission/addAndEdit',
                hideInMenu: true,
              },
              {
                path: '/commission/liteList/edit/:id',
                name: 'edit',
                component: './Commission/liteAdd',
                hideInMenu: true,
              },
              {
                path: '/commission/list/commissionDetail/:id',
                name: 'commissionDetail',
                component: './Commission/detail',
                hideInMenu: true,
              },
              {
                path: '/commission/douyin/commissionDetail/:id',
                name: 'commissionDetail',
                component: './Commission/detail',
                hideInMenu: true,
              },
              {
                path: '/commission/liteList/commissionDetail/:id',
                name: 'commissionDetail',
                component: './Commission/liteDetail',
                hideInMenu: true,
              },
            ],
          },
          {
            path: '/finance',
            name: 'finance',
            icon: 'pay-circle',
            authority: ['财务管理'],
            hideInMenu: true,
            routes: [
              {
                path: '/finance/capitalAccount',
                name: 'capitalAccount',
                component: './Finance/capitalAccount',
                authority: ['商家资金账户'],
              },
              {
                path: '/finance/capitalAccountDetail/:id',
                name: 'capitalAccountDetail',
                component: './Finance/capitalAccountDetail',
                authority: ['商家资金账户'],
                hideInMenu: true,
              },
              {
                path: '/finance/audit',
                name: 'audit',
                component: './Finance/audit',
                authority: ['佣金审批'],
              },
              {
                path: '/finance/audit/:id',
                name: 'audit',
                component: './Commission/detail',
                hideInMenu: true,
              },
              {
                
                path: '/finance/settlement',
                name: 'settlement',
                component: './Finance/settlement',
                authority: ['财务结算'],
              },
              {
                path: '/finance/overview',
                name: 'overview',
                component: './Finance/overview',
                authority: ['结算明细查询'],
              },
              {
                path: '/finance/feeDetail',
                name: 'feeDetail',
                component: './Finance/feeDetail',
                authority: ['费用结算明细'],
              },
              {
                path: '/finance/settlement/detail',
                name: 'settlementdetail',
                component: './Finance/settlementDetail',
                hideInMenu: true,
              },
              {
                path: '/finance/normal',
                name: 'normal',
                component: './Finance/normal',
           
                hideInMenu: true,
              },
              {
                path: '/finance/normal/detail/:id',
                name: 'detail',
                component: './Finance/orderDetail',
                hideInMenu: true,
              },
              {
                path: '/finance/buyout',
                name: 'buyout',
                component: './Finance/buyout',
             
                hideInMenu: true,
              },
              {
                path: '/finance/buyout/detail/:id',
                name: 'detail',
                component: './Finance/buyoutOrderDetail',
                hideInMenu: true,
              },
              {
                path: '/finance/purchase',
                name: 'purchase',
                component: './Finance/purchase',
       
                hideInMenu: true,
              },
              {
                path: '/finance/purchase/purchaseDetail/:id',
                name: 'purchaseDetail',
                component: './Finance/purchaseDetail',
                hideInMenu: true,
              },
            ],
          },
          // 配置管理
          {
            path: '/configure',
            name: 'configure',
            icon: 'setting',
            authority: ['配置管理'],
            routes: [
             
              // {
              //   path: '/configure/tribe',
              //   name: 'tribe',
              //   component: './Tribe/index',
              //   authority: ['部落配置'],
              // },
              
              {
                path: '/configure/tribe/publish',
                name: 'publish',
                component: './Tribe/publish',
                hideInMenu: true,
              },
              {
                path: '/configure/tribe/edit/:id',
                name: 'tribe-edit',
                component: './Tribe/publish',
                hideInMenu: true,
              },
              {
                path: '/configure/category/index',
                name: 'category',
                component: './Category/index',
                authority: ['分类配置'],
              },
              // Activity
              // {
              //   path: '/configure/Activity/index',
              //   name: 'Activity',
              //   component: './Activity/index',
              // },
              {
                path: '/configure/Activity/edit/:id',
                name: 'Apublish',
                component: './Activity/publish',
                hideInMenu: true,
              },
              {
                path: '/configure/Activity/index/publish',
                name: 'Apublish',
                component: './Activity/publish',
                hideInMenu: true,
              },
              // {
              //   path: '/configure/ActivityMaterials/index',
              //   name: 'ActivityMaterials',
              //   component: './ActivityMaterials/index',
              // },
              {
                path: '/configure/MaterialCenter/index',
                name: 'MaterialCenter',
                component: './Configure/MaterialCenter/index',
              },
              {
                path: '/configure/Notice/index',
                name: 'Notice',
                component: './Notice/index',
                authority: ['公告配置'],
              },
              {
                path: '/configure/quesConfig',
                name: 'quesConfig',
                component: './Notice/quesConfig',
                authority: ['常见问题配置'],
              },
              {
                path: '/configure/quesConfig/add',
                name: 'addQuesConfig',
                component: './Notice/quesConfigOperation',
                hideInMenu: true,
                authority: ['常见问题配置'],
              },
              {
                path: '/configure/quesConfig/modify/:id',
                name: 'modifyQuesConfig',
                component: './Notice/quesConfigOperation',
                hideInMenu: true,
              },
              {
                path: '/configure/MaterialCenter/index/see',
                name: 'see',
                component: './Configure/MaterialCenter/see',
                hideInMenu: true,
              },
              {
                path: '/configure/ShortVersion/index',
                name: 'ShortVersion',
                component: './Configure/ShortVersion/index',
              },
              {
                path: '/configure/ShortVersion/index/home',
                name: 'home',
                component: './Configure/ShortVersion/home',
                hideInMenu: true,
              },
              {
                path: '/configure/ShortVersion/index/my',
                name: 'my',
                component: './Configure/ShortVersion/my',
                hideInMenu: true,
              },
              // {
              //   path: '/configure/channel',
              //   name: 'channel',
              //   component: './channel',
              // },
              {
                name: 'zuwuzu',
                hideInMenu: true,
                routes:[
                  {
                    path: '/configure/index',
                    name: 'index',
                    component: './Configure/index',
                    authority: ['首页配置'],
                  },
                  {
                    path: '/configure/tab',
                    name: 'tab',
                    component: './Tab/index',
                    authority: ['Tab配置'],
                  },
                  {
                    path: '/configure/spike',
                    name: 'spike',
                    component: './Spike/index',
                    authority: ['秒杀配置'],
                  },
                  {
                    path: '/configure/my/index',
                    name: 'my',
                    component: './My/index',
                    authority: ['我的配置'],
                  },
                  {
                    path: '/configure/spike/topicDetail/:id',
                    name: 'topicDetail',
                    component: './Spike/TopicDetail',
                    hideInMenu: true,
                  },
                  {
                    path: '/configure/spike/TopicDetailCopy/:id',
                    name: 'topicDetail',
                    component: './Spike/TopicDetailCopy',
                    hideInMenu: true,
                  },
                  {
                    path: '/configure/spike/spikeDetail/:id',
                    name: 'SpikeDetail',
                    component: './Spike/SpikeDetail',
                    hideInMenu: true,
                  },
                  {
                    path: '/configure/abilityCenter',
                    name: 'abilityCenter',
                    routes: [
                      {
                        path: '/configure/abilityCenter/secondHand',
                        name: 'secondHand',
                        component: './Configure/AbilityCenter/SecondHand/index',
                        authority: ['二手购买配置'],
                      },
                      {
                        path: '/configure/abilityCenter/newPhone',
                        name: 'newPhone',
                        component: './Configure/AbilityCenter/SecondHand/index',
                        authority: ['新机购买配置'],
                      },
                      {
                        path: '/configure/abilityCenter/tribe',
                        name: 'tribe',
                        component: './Tribe/index',
                        authority: ['部落配置'],
                      }
                    ]
                  }
                ]
               
              },
              
      
            ],
          },
          // 数据管理
          {
            path: '/dataManagement',
            name: 'dataManagement',
            icon: 'table',
            authority: ['数据管理'],
            routes: [
              // {
              //   path: '/dataManagement/index',
              //   name: 'index',
              //   component: './DataManagement',
              // },
              {
                path: '/dataManagement/dataDownloadCenter',
                name: 'dataDownloadCenter',
                component: './DataManagement/ExportDownloadCenter',
                authority: ['导出数据下载'],
              }
            ],
          },
  
          // {
          //   path: '/Motion',
          //   name: 'Motion',
          //   icon: 'bar-chart',
          //   authority: ['配置管理'],
          //   routes: [
          //     {
          //       path: '/Motion/RollUp',
          //       name: 'RollUp',
          //       component: './Motion/RollUp',
          //       authority: ['首页配置'],
          //     },
          //     {
          //       path: '/Motion/RollUp/RollUpDetails',
          //       name: 'RollUpDetails',
          //       hideInMenu: true,
          //       component: './Motion/RollUpDetails',
          //       authority: ['首页配置'],
          //     },
          //     {
          //       path: '/Motion/RollUp/add',
          //       name: 'add',
          //       hideInMenu: true,
          //       component: './Motion/RollUpDetails',
          //       authority: ['首页配置'],
          //     },
          //     // {
          //     //   path: '/Motion/Order',
          //     //   name: 'Order',
          //     //   component: './Motion/Order',
          //     // },
          //     // {
          //     //   path: '/Motion/Order/OrderDetails',
          //     //   name: 'OrderDetails',
          //     //   hideInMenu: true,
          //     //   component: './Motion/OrderDetails',
          //     // },
          //     {
          //       path: '/Motion/DataView',
          //       name: 'DataView',
          //       hideInMenu: true,
          //       component: './Motion/DataView',
          //     },
          //   ],
          // },
          {
            path: '/tool',
            name: 'tool',
            icon: 'qrcode',
            hideInMenu: true,
            routes: [
              {
                path: '/tool/buyOut',
                name: 'buyOut',
                component: './Tool/buyOut',
              },
            ]
          },
          {
            path: '/permission',
            name: 'permission',
            icon: 'tool',
            authority: ['权限管理'],
            routes: [
              {
                path: '/permission/index',
                name: 'index',
                component: './Permission/index',
                authority: ['部门列表'],
              },
              {
                path: '/permission/index/config/:id',
                name: 'config',
                component: './Permission/PermissionConfig',
                hideInMenu: true,
              },
              {
                path: '/permission/member',
                name: 'member',
                component: './Member/index',
                authority: ['成员管理'],
              },
              {
                path: '/permission/member/config/:id',
                name: 'config',
                component: './Permission/PermissionConfig',
                hideInMenu: true,
              },
            ],
          },

          {
            component: './404',
          },
        ],
      },
      {
        component: './404',
      },
    ],
  },
  {
    component: './404',
  },
];