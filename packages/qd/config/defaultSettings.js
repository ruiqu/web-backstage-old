module.exports = {
  navTheme: 'dark',
  primaryColor: '#E83F40',
  layout: 'sidemenu',
  contentWidth: 'Fluid',
  fixedHeader: false,
  autoHideHeader: false,
  fixSiderbar: false,
  colorWeak: false,
  menu: {
    locale: true,
  },
  title: '渠道后台',
  pwa: false,
  iconfontUrl: '',
};
