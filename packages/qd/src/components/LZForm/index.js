import React from 'react';
import { Form } from 'antd';

const FormItem = Form.Item;

const BaseFormItem = ({label,formItemLayout, children})=>{
  return (
    <FormItem {...formItemLayout} label={<span>{label}</span>}>
      {children}
    </FormItem>
  )
}

const LZFormItem = ({
                      lzIf=true, label, getFieldDecorator, children, field, initialValue, rules=[],
                      formItemLayout,required,validateStatus, help,className,requiredMessage,
                      ...props})=>{
  // 默认展示
  if (!lzIf) {
    return null
  }
  const labelDOM = label?<span>{label}</span>:null
  if (requiredMessage) {
    rules.push({required: true, message: requiredMessage})
  }
  return (
    <FormItem {...formItemLayout} className={className} label={labelDOM} required={required} validateStatus={validateStatus}
    help={help}>
      {getFieldDecorator ? getFieldDecorator(field, {
        rules,
        initialValue,
        ...props
      })(children)
        : null}
    </FormItem>
  )
}

export {
  BaseFormItem,
  LZFormItem
}
