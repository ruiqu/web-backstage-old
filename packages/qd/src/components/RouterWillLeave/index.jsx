import React, { PureComponent } from 'react';
import router from "umi/router";
import Prompt from "umi/prompt";
import {Modal} from "antd";

const RouterWillLeaveOld = ({isPrompt, isSubmit})=>{
  const nextStep = (pathname) => {
    isPrompt = false
    pathname && setTimeout(() => {
      router.push(pathname);
    });
  }
  
  return (
    <div>
      <Prompt
        when={isPrompt}
        message={(location) => {
          if (!isPrompt || isSubmit) {
            return true;
          }
          Modal.confirm({
            content: '确定离开当前页面吗？未保存的数据将会丢失',
            okText: '确定',
            cancelText: '取消',
            okType: 'danger',
            onOk: ()=> {
              nextStep(location.pathname);
            },
            onCancel: ()=> {
            
            }
          });
          return false;
        }}
      />
    </div>
  )
}

export default class RouterWillLeave extends PureComponent {
  
  constructor(props) {
    super(props);
    this.state = {
      isPrompt: this.props.isPrompt,
      isSubmit: this.props.isSubmit,
    }
  }
  
  state = {
    isPrompt: false
  }
  
  componentWillReceiveProps(nextProps) {
    this.setState({
      isPrompt: nextProps.isPrompt,
      isSubmit: nextProps.isSubmit,
    });
  }
  
  nextStep = (pathname) => {
    this.setState({
      isPrompt: false
    });
    pathname && setTimeout(() => {
      router.push(pathname);
    });
  }
  
  render() {
    return (
      <div>
        <Prompt
          when={this.state.isPrompt}
          message={(location) => {
            if (!this.state.isPrompt || this.state.isSubmit) {
              return true;
            }
            Modal.confirm({
              content: '确定离开当前页面吗？未保存的数据将会丢失',
              okText: '确定',
              cancelText: '取消',
              okType: 'danger',
              onOk: ()=> {
                this.nextStep(location.pathname);
              },
              onCancel: ()=> {
              
              }
            });
            return false;
          }}
        />
      </div>
    )
  }
}


// export default RouterWillLeave;
