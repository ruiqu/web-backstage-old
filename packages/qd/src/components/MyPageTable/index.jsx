import React, { Component } from 'react';
import styles from './index.less';
import { Table } from 'antd';
export default class MyTable extends Component {
  //获取子组件页数
  onChange = e => {
    e && this.props.onPage(e);
  };
  render() {
    const { columns, dataSource, paginationProps, scroll } = this.props;
    return (
      <div>
        <Table
          scroll={{x: scroll}}
          columns={columns}
          dataSource={dataSource}
          pagination={paginationProps}
          onChange={this.onChange}
        />
      </div>
    );
  }
}
