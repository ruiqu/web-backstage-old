import request from "@/services/baseService";

export default {
  selectShopAdditionalServicesList:(data)=>{
    return request(`/hzsx/shopAdditionalServices/selectShopAdditionalServicesList`, data, 'get')
  },
  deletShopAdditionService:(data)=>{
    return request(`/hzsx/shopAdditionalServices/deletShopAdditionService`, data, 'get')
  },
  insertShopAdditionServices:(data)=>{
    return request(`/hzsx/shopAdditionalServices/insertShopAdditionServices`, data )
  },
}
