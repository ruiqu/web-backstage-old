import request from '@/utils/request';

function getToken() {
  const token = localStorage.getItem('token');
  return token;
}
import _request from '@/services/baseService';

export default {
  // 查询征信报告
  queryCreditReportByUid: data => {
    return _request(`/hzsx/business/yunxin/queryCreditReportByUid`, data, 'post');
  },
  // 查询征信基础信息
  getUserCreditBaseInfo: data => {
    return _request(`/hzsx/business/yunxin/getUserCreditBaseInfo`, data, 'get');
  },
  // 更新征信基础信息
  updateUserCreditBaseInfo: data => {
    return _request(`/hzsx/business/yunxin/updateUserCreditBaseInfo`, data, 'post');
  },
  // 申请报送
  applyCredit: data => {
    return _request(`/hzsx/business/yunxin/applyCredit`, data, 'post');
  },
  // 放款报送
  loanCredit: data => {
    return _request(`/hzsx/business/yunxin/loanCredit`, data, 'post');
  },
  // 取消放款
  cancelLoanCredit: data => {
    return _request(`/hzsx/business/yunxin/cancelLoanCredit`, data, 'post');
  },
  // 结清
  settleCredit: data => {
    return _request(`/hzsx/business/yunxin/settleCredit`, data, 'post');
  },
  // 追偿报送
  dunRepayCredit: data => {
    return _request(`/hzsx/business/yunxin/dunRepayCredit`, data, 'post');
  },
  // 追偿结清
  dunRepaySettleCredit: data => {
    return _request(`/hzsx/business/yunxin/dunRepaySettleCredit`, data, 'post');
  },


};
