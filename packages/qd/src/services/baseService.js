import baseRequest from '@/utils/lz-request';
import {getToken} from "@/utils/localStorage";


export default function request(url, value = {}, method = 'post', options = {}) {
  let token = getToken()
  if (method === 'post') {
    return baseRequest(url, {
      method: 'post',
      data: value,
      headers: {
        token
      }
    })
  } else if (method === 'get') {
    return baseRequest(url, { params: value, headers: {token} }, options)
  } else if (method === 'formdata') {
    return baseRequest({
      method: 'post',
      url: url,
      data: value,
      transformRequest: [
        function(data) {
          let ret = ''
          for (const it in data) {
            ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
          }
          ret = ret.substring(0, ret.length - 1)
          return ret
        }
      ],
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        token
      }
    })
  }
}
