import request from '@/utils/request';
function getToken() {
  const token = localStorage.getItem('token');
  return token;
}

//查询部落评论分页
export async function queryTribeCommentPage(params) {
  return request(`/hzsx/tribeComment/queryTribeCommentPage`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
//分页查询运营导入商品评论
export async function queryProductEvaluationPage(params) {
  return request(`/hzsx/productEvaluation/queryProductEvaluationPage`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}

//查询部落评论编辑信息
export async function selectTribeCommentEdit(params) {
  return request(`/hzsx/tribeComment/selectTribeCommentEdit?id=${params.id}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}

export async function selectExamineProductEdit(params) {
  return request(`/hzsx/productEvaluation/selectExamineProductEdit?id=${params.id}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}
