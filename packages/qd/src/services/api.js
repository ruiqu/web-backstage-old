// import { stringify } from 'qs';
import request from '@/utils/request';
import axios from 'axios';

// 订单
export async function queryAllOrder(params) {
  return request('/hzsx/business/user/order/selectBusinessAllUserOrders', {
    method: 'POST',
    body: params,
  });
}
export async function businessFindBuyOutOrder(params) {
  return request('/hzsx/business/user/order/businessFindBuyOutOrder', {
    method: 'GET',
    body: params,
  });
}
export async function businessBuyOutOrder(params) {
  return request('/hzsx/business/user/order/businessBuyOutOrder', {
    method: 'GET',
    body: params,
  });
}

// 次数
export async function reportUserInfo(params) {
  return request('/hzsx/business/user/userInfo', {
    method: 'GET',
    body: params,
  });
}
// 信用
export async function queryAllReport(params) {
  return request('/hzsx/business/user/report/pageRecords', {
    method: 'POST',
    body: params,
  });
}
// 三要素查询
export async function generateReportByTreeEls(params) {
  return request('/hzsx/business/user/report/generateReportByTreeEls', {
    method: 'POST',
    body: params,
  });
}
// 信用详情
export async function reportDetail(params) {
  return request('/hzsx/business/user/report/detail', {
    method: 'GET',
    body: params,
  });
}

// 退款
export async function forGetPwd(params) {
  return request('/hzsx/business/user/forGetPwd', {
    method: 'POST',
    body: params,
  });
}

// 同意退款
export async function busAgreeToRefund(params) {
  return request('/hzsx/business/user/order/busAgreeToRefund', {
    method: 'GET',
    body: params,
  });
}

// 确认收货
export async function refundOrderConfirmsReceipt(params) {
  return request('/hzsx/business/user/order/refundOrderConfirmsReceipt', {
    method: 'GET',
    body: params,
  });
}

// 拒绝退款

export async function businessFefundClose(params) {
  return request('/hzsx/business/user/order/businessFefundClose', {
    method: 'GET',
    body: params,
  });
}
// 同意申请

export async function busAgreeToApplyFor(params) {
  return request('/hzsx/business/user/order/busAgreeToApplyFor', {
    method: 'GET',
    body: params,
  });
}

// 退款详情

export async function busGetRefundOrderDetail(params) {
  return request('/hzsx/business/user/order/busGetRefundOrderDetail', {
    method: 'GET',
    body: params,
  });
}

// 查看凭证

export async function busGetRefundOrderPicture(params) {
  return request('/hzsx/business/user/order/busGetRefundOrderPicture', {
    method: 'GET',
    body: params,
  });
}

// 物流

export async function busGetExpressList(params) {
  return request('/hzsx/business/user/order/busGetExpressList', {
    method: 'GET',
    body: params,
  });
}

export async function businessFefundOrderList(params) {
  return request('/hzsx/business/user/order/businessFefundOrderList', {
    method: 'POST',
    body: params,
  });
}
export async function generateReportByOrderId(params) {
  return request(`/hzsx/business/user/order/getSiriusReportRecord`, {
    method: 'POST',
    body: params,
  });
}

export async function buyOutUserOrderDetail(params) {
  return request('/hzsx/business/user/order/buyOutUserOrderDetail', {
    body: {
      orderId: params,
    },
  });
}
export async function queryDeliverOps() {
  return request('/hzsx/business/user/order/expressList');
}

export async function sendDeliver(params) {
  return request('/hzsx/business/user/order/orderShipment', {
    body: params,
  });
}
export async function addRemark(params) {
  return request('/hzsx/business/user/order/orderRemark', {
    method: 'POST',
    body: params,
  });
}
export async function closeOrder(params) {
  return request('/hzsx/business/user/order/businessCloseOrder', {
    body: params,
  });
}
export async function settleOrder(params) {
  return request('/hzsx/business/user/order/merchantsIssuedStatements', {
    method: 'POST',
    body: params,
  });
}
export async function exportOrder(params) {
  return request('/hzsx/business/user/order/exportBusinessAllUserOrders', {
    method: 'POST',
    body: params,
  });
}

export async function sendSmsCode(params) {
  return request('/hzsx/business/user/sendSmsCode', {
    body: params,
  });
}

export async function fakeAccountLogin(params) {
  return request('/hzsx/business/userChannel/login', {
    method: 'POST',
    body: params,
  });
}

export const selectEnterpriseInfo = () => request('/hzsx/business/shop/selectEnterpriseInfo');

export const updateEnterpriseInfo = params =>
  request('/hzsx/business/shop/updateEnterpriseInfo', {
    method: 'POST',
    body: params,
  });

export const selectShopInfo = () => request('/hzsx/business/shop/selectShopInfo');

export const findPlatformShopType = () => request('/hzsx/business/shop/findPlatformShopType');

export const findCategories = () => request('/hzsx/business/shop/findCategories');

export const updateShop = params =>
  request('/hzsx/business/shop/updateShop', {
    method: 'POST',
    body: params,
  });

export const insertSettlementAgreement = () => request('/hzsx/business/shop/insertSettlementAgreement');

export const getUserIdCardPhotoInfo = params =>
  request('/hzsx/business/user/order/queryUserIdCardInfo', {
    method: 'GET',
    body: params,
  });

//商家拒绝订单
export const rejectOrder = params =>
  request('/hzsx/business/user/order/shopRiskReviewCloseOrder', {
    method: 'POST',
    body: params,
  });
//查看拒绝订单详情
export const viewRejectDetail = params =>
  request('/hzsx/business/user/order/getShopRiskReviewCloseOrderDetail', {
    method: 'GET',
    body: params,
  });

//提交备注信息
export const submitRemark = params => {
  return request('/hzsx/business/user/order/orderRemark', {
    method: 'POST',
    body: params,
  });
};

//获取买断订单table列表信息
export const getTableLsit = params => {
  return request('/hzsx/business/user/order/selectBusinessBuyOutOrders', {
    method: 'POST',
    body: params,
  });
};
export const getCard = params => {
  return request(`/hzsx/business/user/order/getUserCertInfo`, {
    method: 'GET',
    body: { orderId: params },
  });
};

export const getOcr = params =>
  request(`/hzsx/business/user/order/getUserCertInfo`, {
    method: 'GET',
    body: params,
  });
export async function getDetailinformation(params) {
  return request(`/hzsx/business/user/order/selectBusinessBuyOutOrderDetailByOrderId`, {
    body: {
      orderId: params,
    },
  });
}
export async function queryOrderDetail(params) {
  return request('/hzsx/business/user/order/ope/order/selectOpeUserOrderDetail', {
    body: {
      orderId: params,
    },
  });
}

//导出文件
export const exportData = params => {
  return request('/hzsx/business/user/order/exportBusinessBuyOutOrders', {
    method: 'POST',
    body: params,
  });
};

//获取证件详情
export const getCardinformation = (params) => {
  return request(`/hzsx/business/user/order/getUserOrderCertInfo`, {
    method: 'GET',
    body: params
  })
}