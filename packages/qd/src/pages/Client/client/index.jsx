import React, { Component } from 'react';
import request from '@/services/baseService';
import MyPageTable from '@/components/MyPageTable';
import moment from 'moment';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import {
  Card,
  Spin,
  Form,
  DatePicker,
  Input,
  Button,
  Select,
  message,
  Row,
  Col,
  TreeSelect,
  InputNumber,
} from 'antd';
import styles from './index.less';
import { clientRegisterStatus, clientRegisterStatusMap, orderCloseStatusMap } from '@/utils/enum';

const { RangePicker } = DatePicker;
const { SHOW_PARENT } = TreeSelect;

@Form.create()
class clientList extends Component {
  state = {
    search: {
      current: 1,
      size: 30,
      mobile: '',
    },
    loading: false,
    form: {
      mobile: '',
    },
    tableList: [],
    total: 0,
    pages: 1,
    modal: {
      type: 'add',
      title: '',
    },
    queryLoad: false,
    modalLoad: false,
    channelList: [],
    userList: [],
    registerStatusValue: [],
    maxQuota: 10,
    minQuota: 0,
  };

  componentDidMount() {
    this.getList();
  }

  columns = [
    {
      title: 'No.',
      dataIndex: 'id',
      rowScope: 'row',
      width: 60,
      render: (record, index, indent, expanded) => {
        return indent + 1;
      },
    },
    {
      title: '渠道名称',
      dataIndex: 'channelName',
    },
    {
      title: '姓名',
      dataIndex: 'userName',
    },
    {
      title: '手机号',
      dataIndex: 'telephone',
    },
    {
      title: '进件时间',
      dataIndex: 'createTime',
    },
    {
      title: '认证状态',
      dataIndex: 'registerStatus',
      render: (record, index) => {
        return clientRegisterStatus[index.registerStatus];
      },
    },
    {
      title: '风控流水号',
      dataIndex: 'serialNum',
    },
  ];

  getList = () => {
    this.setState({
      loading: true,
    });
    const { search, form } = this.state;

    let obj = Object.fromEntries(
      Object.entries(search).filter(([key, value]) => {
        // 过滤空值
        return value !== '' && value !== null && value !== undefined;
      }),
    );

    request(`/zyj-backstage-web/hzsx/userChannel/getRiskLimitList`, obj, 'post').then(res => {
      this.setState({
        tableList: res.records,
        queryLoad: false,
        pages: res.pages,
        total: res.total,
        loading: false,
      });
    });
  };

  handSearch = () => {
    this.props.form.validateFields(
      [

        'rangePicker',
        'serialNum',
        'registerStatus',
      ],
      (err, values) => {
        if (!err) {
          let registerStatusListArray = [];
          let registerStatusList = '';
          let stageStatusListArray = [];
          let stageStatusList = '';

          // console.log(123456,values.registerStatus)
          if (values.registerStatus) {
            // console.log(values.registerStatus);
            values.registerStatus.forEach(element => {
              if (element.includes('-')) {
                let split = element.split('-');
                stageStatusListArray.push(Number(split[1]));
              } else {
                registerStatusListArray.push(Number(element));
              }
            });
            // console.log('小类', stageStatusListArray);
            // console.log('大类', registerStatusListArray);
            // stageStatusList = stageStatusListArray.join();
            // registerStatusList = registerStatusListArray.join();
          }

          let createTimeStart = '';
          let createTimeEnd = '';

          if (values.rangePicker && values.rangePicker.length > 0) {
            createTimeStart = `${moment(values.rangePicker[0]).format('YYYY-MM-DD')} 00:00:00`;
            createTimeEnd = `${moment(values.rangePicker[1]).format('YYYY-MM-DD')} 23:59:59`;
          }
          this.setState(
            {
              queryLoad: true,
              search: {
                current: 1,
                size: 30,
                // registerStatus: values.registerStatus,
                serialNum: values.serialNum,
                rangePicker: values.rangePicker,
                stageStatusList: registerStatusListArray,
                registerStatusList: stageStatusListArray,
                createTimeStart,
                createTimeEnd,
              },
            },
            () => this.getList(),
          );
        }
      },
    );
  };

  onPage = e => {
    let data = Object.assign(this.state.search, { current: e.current, size: e.pageSize });
    this.setState(
      {
        search: data,
      },
      () => this.getList(),
    );
  };

  onChange = value => {
    this.setState({ value });
  };

  render() {
    const { pages, total, search, queryLoad } = this.state;
    const { getFieldDecorator } = this.props.form;
    const paginationProps = {
      current: search.current,
      pageSize: search.size,
      total: total,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / search.size)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };

    const treeData = [
      {
        title: '申请前',
        value: '1',
        key: '1',
        field: 'stageStatusList',
        children: [
          {
            title: '注册通过',
            value: '1-1',
            key: '1-1',
            field: 'registerStatusList',
          },
          {
            title: 'OCR通过',
            value: '1-2',
            key: '1-2',
            field: 'registerStatusList',
          },
          {
            title: '活体通过',
            value: '1-3',
            key: '1-3',
            field: 'registerStatusList',
          },
          {
            title: '联系人通过',
            value: '1-4',
            key: '1-4',
            field: 'registerStatusList',
          },
        ],
      },
      {
        title: '等待申请结果',
        value: '2',
        key: '2',
        field: 'stageStatusList',
        children: [
          {
            title: '待人审',
            value: '2-6',
            key: '2-6',
            field: 'registerStatusList',
          },
        ],
      },
      {
        title: '申请通过',
        value: '3',
        key: '3',
        field: 'stageStatusList',
        children: [
          {
            title: '审核通过',
            value: '3-7',
            key: '3-7',
            field: 'registerStatusList',
          },
        ],
      },
      {
        title: '申请未通过',
        value: '4',
        key: '4',
        field: 'stageStatusList',
        children: [
          {
            title: '风控未通过',
            value: '4-5',
            key: '4-5',
            field: 'registerStatusList',
          },
          {
            title: '审核未通过',
            value: '4-8',
            key: '4-8',
            field: 'registerStatusList',
          },
        ],
      },
    ];

    const tProps = {
      treeData,
      allowClear: true,
      // value: this.state.registerStatusValue,
      // onChange: this.onChange,
      treeCheckable: true,
      showCheckedStrategy: SHOW_PARENT,
      searchPlaceholder: '请选择认证状态',
      style: {
        width: '185px',
      },
    };

    return (
      <PageHeaderWrapper title={false} className="nav-tab">
        <Spin spinning={this.state.loading}>
          <Card bordered={false}>
            <Form layout="inline" className="mb-20">
              <Row>
                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>风控流水号</span>}>
                    {getFieldDecorator('serialNum', {
                      rules: [],
                    })(<Input className={styles.formInput} placeholder="请输入风控流水号" />)}
                  </Form.Item>
                </Col>

                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>认证状态</span>}>
                    {getFieldDecorator(
                      'registerStatus',
                      {},
                    )(
                      <TreeSelect className={styles.formInput} {...tProps} />,
                    )}
                  </Form.Item>
                </Col>

                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>进件时间</span>}>
                    {getFieldDecorator('rangePicker')(<RangePicker format="YYYY-MM-DD" />)}
                  </Form.Item>
                </Col>
      
              </Row>
              <div>
                <Form.Item>
                  <Button loading={queryLoad} onClick={() => this.handSearch()} type="primary">
                    查询
                  </Button>
                </Form.Item>
              </div>
            </Form>
            <MyPageTable
              scroll="max-content"
              onPage={this.onPage}
              style={{ marginTop: '20px' }}
              paginationProps={paginationProps}
              dataSource={this.state.tableList}
              columns={this.columns}
              rowKey="id"
            />
          </Card>
        </Spin>
      </PageHeaderWrapper>
    );
  }
}

export default clientList;
