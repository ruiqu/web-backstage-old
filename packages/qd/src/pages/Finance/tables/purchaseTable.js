/**
 * 购买订单表格
 */
import React from "react"
import statusMap from "@/config/status.config"
import PropTypes from "prop-types"
import { router } from "umi"
import { Table } from "antd"
import CopyToClipboard from "@/components/CopyToClipboard"

class PurchaseTable extends React.Component {
  /**
  * 查看订单详情
  * @param {*} id 
  */
  preview = id => {
    router.push(`/finance/purchase/purchaseDetail/${id}`)
  };

  columns = [
    {
      title: '订单编号',
      dataIndex: 'orderId',
      width: 200,
      render: text => <CopyToClipboard text={text} />,
    },
    {
      title: '总金额',
      dataIndex: 'userPayAmount',
      width: 80,
      render: text => (text < 0 ? 0 : text),
    },
    { title: '结算金额', dataIndex: 'toShopAmount', render: text => (text < 0 ? 0 : text) },
    { title: '佣金', dataIndex: 'toOpeAmount', render: text => (text < 0 ? 0 : text) },
    { title: '结算状态', dataIndex: 'status', render: text => statusMap[text] },
    { title: '账单生成时间', dataIndex: 'splitBillTime', key: 'splitBillTime' },
    {
      title: '操作',
      align: 'center',
      key: 'action',
      fixed: 'right',
      render: (text, record) => {
        return (
          <div className="table-action">
            <a onClick={() => this.preview(record.id)}>查看</a>
          </div>
        );
      },
    },
  ];

  onChange = pageNumber => {
    this.props.onChangeHandler(pageNumber)
  }

  // 切换每页数量
  onShowSizeChange = (_, pageSize) => {
    this.props.onShowSizeChangeHandler(pageSize)
  };

  showTotal = () => {
    return `共有${this.props.total}条`;
  };

  render() {
    const { loading, tableData, total, current, pageSize } = this.props

    return (
      <Table
        scroll={{x: true}}
        columns={this.columns}
        loading={loading}
        dataSource={tableData}
        rowKey={record => record.id}
        pagination={{
          current,
          pageSize,
          total,
          onChange: this.onChange,
          showTotal: this.showTotal,
          showQuickJumper: true,
          pageSizeOptions: ['5', '10', '20'],
          showSizeChanger: true,
          onShowSizeChange: this.onShowSizeChange,
        }}
      />
    )
  }
}

PurchaseTable.propTypes = {
  tableData: PropTypes.array, // 列表数据
  pageSize: PropTypes.number, // 每页条数
  loading: PropTypes.bool, // 是否正在请求列表数据中
  total: PropTypes.number, // 列表数据总条数
  current: PropTypes.number, // 当前页码
  onChangeHandler: PropTypes.func, // 页码改变时触发
  onShowSizeChangeHandler: PropTypes.func, // 每页数据条数改变时触发
}

export default PurchaseTable