/**
 * 买断账单列表表格
 */
import React from "react"
import statusMap from "@/config/status.config"
import PropTypes from "prop-types"
import { router } from "umi"
import { Table } from "antd"
import CopyToClipboard from "@/components/CopyToClipboard"

class BuyOutTable extends React.Component {
  /**
 * 查看订单详情
 * @param {*} id 
 */
  preview = id => {
    router.push(`/finance/buyout/detail/${id}`)
  };

  columns = [
    {
      title: '买断订单号',
      dataIndex: 'orderId',
      width: 140,
      render: text => <CopyToClipboard text={text} />,
    },
    {
      title: '原订单号',
      dataIndex: 'id',
      width: 140,
      render: (_, obj) => {
        const { orderId } = obj
        return (
          <div style={{ textAlign: 'center' }}>
            <a
              style={{ cursor: 'pointer', color: '#3F66F5' }}
              onClick={() => router.push(`/Order/Details?id=${orderId}`)} // FIXME: 修复这个跳转行为
            >
              {orderId}
            </a>
          </div>
        );
      },
    },
    {
      title: '已付租金',
      dataIndex: 'userPayAmount',
      align: 'center',
      width: 90,
      render: text => (text < 0 ? 0 : text),
    },
    {
      title: '买断尾款',
      dataIndex: 'endFund',
      align: 'center',
      width: 90,
      render: text => (text < 0 ? 0 : text),
    },
    {
      title: '结算金额',
      dataIndex: 'toShopAmount',
      width: 120,
      render: text => (text < 0 ? 0 : text),
    },
    { title: '佣金', dataIndex: 'toOpeAmount', width: 120, render: text => (text < 0 ? 0 : text) },
    { title: '结算状态', dataIndex: 'status', width: 140, render: text => statusMap[text] },
    { title: '账单生成时间', dataIndex: 'userPayTime', width: 120 },
    {
      title: '操作',
      align: 'center',
      key: 'action',
      fixed: 'right',
      width: 80,
      render: (text, record) => {
        return (
          <div className="table-action">
            <a onClick={() => this.preview(record.id)}>详情</a>
          </div>
        );
      },
    },
];

  onChange = pageNumber => {
    this.props.onChangeHandler(pageNumber)
  }

  // 切换每页数量
  onShowSizeChange = (_, pageSize) => {
    this.props.onShowSizeChangeHandler(pageSize)
  };

  showTotal = () => {
    return `共有${this.props.total}条`;
  };

  render() {
    const { loading, tableData, total, current, pageSize } = this.props

    return (
      <Table
        scroll={{x: true}}
        columns={this.columns}
        loading={loading}
        dataSource={tableData}
        rowKey={record => record.id}
        pagination={{
          current,
          pageSize,
          total,
          onChange: this.onChange,
          showTotal: this.showTotal,
          showQuickJumper: true,
          pageSizeOptions: ['5', '10', '20'],
          showSizeChanger: true,
          onShowSizeChange: this.onShowSizeChange,
        }}
      />
    )
  }
}

BuyOutTable.propTypes = {
  tableData: PropTypes.array, // 列表数据
  pageSize: PropTypes.number, // 每页条数
  loading: PropTypes.bool, // 是否正在请求列表数据中
  total: PropTypes.number, // 列表数据总条数
  current: PropTypes.number, // 当前页码
  onChangeHandler: PropTypes.func, // 页码改变时触发
  onShowSizeChangeHandler: PropTypes.func, // 每页数据条数改变时触发
}

export default BuyOutTable