/**
 * 资金账户页面
 * 需求文档：https://buershujv.yuque.com/ktbs5p/yft9p7/prgoz0
 */
import React from "react"
import { connect } from "dva"
import { PageHeaderWrapper } from "@ant-design/pro-layout"
import { Card, Descriptions, Button, Table, Form } from "antd"
import CustomCard from "@/components/CustomCard"
import PayDialog from "./components/payDialog"
import TixianDialog from "./components/tixianDialog"
import PinzhengDialog from "./components/pinzhengDialog"
import request from "@/services/baseService"
import { strUi, onTableData } from "@/utils/utils"
import router from "umi/router"
import styles from "./index.less"
import { tab1, tab2 } from "@/models/feeDetail"


const pinzhengTypes = ["WITHDRAW", "RECHARGE"]

@connect(({ capitalAccountModel }) => ({
  ...capitalAccountModel,
}))
@Form.create()
class CapitalAccount extends React.Component {
  state = {
    userDataLoading: false, // 账户信息数据加载中
    userData: {}, // 账户信息数据
    moneyLoading: false, // 账户余额加载中
    availableMoney: 0, // 账户余额
    tableLoading: false, // 列表数据加载中
    tableData: {},
    showModal: false, // 是否显示充值弹窗
    showTixianModal: false, // 是否显示提现弹窗
    showPinzhengModal: false, // 是否显示凭证弹窗
    flowId: "", // 凭证的流水号
  }

  componentDidMount() {
    this.fetchDataHandler()
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    let beforeQueryStr = JSON.stringify(this.props.listQuery)
    let afterQueryStr = JSON.stringify(nextProps.listQuery)
    if (beforeQueryStr != afterQueryStr) {
      this.fetchListHandler(nextProps.listQuery)
    }
  }

  // 获取账户信息数据
  fetchUserDetail = () => {
    this.setState({ userDataLoading: true })
    return request("/hzsx/shopFund/getShopAccountInfo", {}, "get").then(res => {
      this.setState({ userData: res })
    }).finally(() => {
      this.setState({ userDataLoading: false })
    })
  }

  // 加载账户余额数据
  fetchMoneyData = () => {
    this.setState({ moneyLoading: true })
    return request("/hzsx/shopFund/getShopFundBalance", {}, "get").then(res => {
      this.setState({ availableMoney: res })
    }).finally(() => {
      this.setState({ moneyLoading: false })
    })
  }

  // 加载资金明细列表数据
  fetchListHandler = postObj => {
    this.setState({ tableLoading: true })
    const postData = postObj || { ...this.props.listQuery }
    return request("/hzsx/shopFund/pageShopFundFlow", postData).then(res => {
      this.setState({ tableData: res })
    }).finally(() => {
      this.setState({ tableLoading: false })
    })
  }

  // Table组件的配置数据
  tableColumns = [
    { title: "时间", dataIndex: "createTime" },
    {
      title: "类型",
      dataIndex: "operate",
      render: val => {
        const uimap = {
          WITHDRAW: "提现",
          RECHARGE: "充值",
          BROKERAGE_ZWZ: "佣金结算",
          ASSESSMENT: "芝麻租押分离接口费用",
          E_SIGN: "e签宝费用",
          BROKERAGE_LITE: "简版小程序结算",
          BROKERAGE_TOUTIAO: "抖音小程序佣金结算",
        }
        const cn = strUi(uimap[val])
        return cn
      }
    },
    {
      title: "变更金额",
      dataIndex: "changeAmount",
    },
    { title: "变更人", dataIndex: "operator" },
    { title: "余额", dataIndex: "afterAmount" },
    {
      title: "操作",
      render: (_, row) => {
        const tv = row.operate // 类型
        let cn
        if (pinzhengTypes.includes(tv)) cn = "凭证"
        else cn = "明细"
        return <a onClick={() => this.actionHandler(row)} className="blackClickableA">{ cn }</a>
      }
    }
  ]

  actionHandler = rowData => {
    const ty = rowData.operate // 操作类型
    if (pinzhengTypes.includes(ty)) { // 查看凭证弹窗的
      this.setState({ showPinzhengModal: true, flowId: rowData.id })
    } else { // 其它操作
      const jiesuanType = ["BROKERAGE_ZWZ", "BROKERAGE_LITE", "BROKERAGE_TOUTIAO"] // 结算类型
      if (jiesuanType.includes(ty)) { // 跳转到佣金结算明细详情页面
        const url = `/finance/settlement/detail?fromCapitalAccount=1&id=${rowData.id}`
        router.push(url)
      } else { // 跳转到费用结算明细详情页面
        const vMap = {
          ASSESSMENT: tab1,
          E_SIGN: tab2,
        }
        const v = vMap[ty] // 菜单ID
        this.props.dispatch({ type: "feeDetailModel/mutFeeDetailActiveTab", payload: v }) // 修改redux中所存储的焦点tab
        this.props.dispatch({ type: "feeDetailModel/muFeeDetailListApiParamsComplete", payload: { tab: v, val: { pageNumber: 1, pageSize: 10 } }}) // 修改页码
        const url = `/finance/feeDetail?id=${rowData.id}`
        router.push(url)
      }
    }
  }

  /**
   * 加载页面的所有数据
   */
  fetchDataHandler = () => {
    this.fetchUserDetail()
    this.fetchMoneyData()
    this.fetchListHandler()
  }

  // 充值按钮的点击回调
  inHandler = () => {
    this.setState({ showModal: true })
  }

  // 提现按钮的点击回调
  outHandler = () => {
    this.setState({ showTixianModal: true })
  }

  showTotal = () => {
    return `共有${this.state.tableData?.total}条`
  }

  // 改变页码的时候触发
  changePageHandler = val => {
    const payload = { pageNumber: val }
    this.props.dispatch({ type: "capitalAccountModel/mutListQuery", payload })
  }

  // 改变每页条数时触发
  showSizeChangeHandler = (_, size) => {
    const payload = { pageSize: size, pageNumber: 1 }
    this.props.dispatch({ type: "capitalAccountModel/mutListQuery", payload })
  }

  render() {
    const queryObj = this.props.listQuery || {}
    const { pageNumber, pageSize } = queryObj
    const userDetailObj = this.state.userData || {}
    const tableDataSource = onTableData(this.state.tableData?.records || [])

    return (
      <PageHeaderWrapper>
        <Card loading={this.state.userDataLoading} bordered={false} className={styles.mb20}>
          <Descriptions title={<CustomCard title="账户信息" />}>
            <Descriptions.Item label="店铺名称">{ strUi(userDetailObj.shopName) }</Descriptions.Item>
            <Descriptions.Item label="支付宝账户">{ strUi(userDetailObj.identity) }</Descriptions.Item>
            <Descriptions.Item label="结算人">{ strUi(userDetailObj.name) }</Descriptions.Item>
          </Descriptions>
        </Card>

        <Card loading={this.state.moneyLoading} bordered={false} className={styles.mb20}>
          <Descriptions title={<CustomCard title="资金明细" />}>
            <Descriptions.Item label="账户余额">{ this.state.availableMoney }元</Descriptions.Item>
          </Descriptions>
          <div className={styles.mb20}>
            <Button type="primary" onClick={this.inHandler}>
              充值
            </Button>
            <Button onClick={this.outHandler}>
              提现
            </Button>
          </div>

          <Table
            loading={this.state.tableLoading}
            columns={this.tableColumns}
            dataSource={tableDataSource}
            pagination={{
              current: pageNumber,
              pageSize: pageSize,
              total: this.state.tableData?.total,
              onChange: this.changePageHandler,
              onShowSizeChange: this.showSizeChangeHandler,
              showTotal: this.showTotal,
              showQuickJumper: true,
              pageSizeOptions: ['5', '10', '20'],
              showSizeChanger: true,
            }}
          />
        </Card>

        {/** 充值弹窗 */}
        <PayDialog
          money={this.state.availableMoney}
          showPayModal={this.state.showModal}
          hideModalHandler={() => this.setState({ showModal: false })}
          refreshPageHandler={this.fetchDataHandler}
        />

        {/** 提现弹窗 */}
        <TixianDialog
          show={this.state.showTixianModal}
          hideModalHandler={() => this.setState({ showTixianModal: false })}
          initMoney={this.state.availableMoney}
          refreshPageHandler={this.fetchDataHandler}
        />

        <PinzhengDialog
          showPinzheng={this.state.showPinzhengModal}
          flowNo={this.state.flowId}
          hideModalHandler={() => { this.setState({ showPinzhengModal: false, flowId: "" })}}
        />
      </PageHeaderWrapper>
    )
  }
}

export default CapitalAccount