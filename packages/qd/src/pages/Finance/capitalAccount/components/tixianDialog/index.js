/**
 * 提现的弹窗
 */
import React from "react"
import { Form, Modal, InputNumber, message } from "antd"
import PropTypes from "prop-types"
import request from "@/services/baseService"
import styles from "./index.less"

const Min = 0.1
const Max = 50000

@Form.create()
class TixianDialog extends React.PureComponent {
  state = {
    fetching: false, // 提现中
  }

  // 隐藏模态
  closeModal = () => {
    this.props.form.resetFields() // 重置输入的金额
    this.props.hideModalHandler()
    this.setState({ fetching: false })
  }

  // 确定提现
  sureTixianHandler = () => {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const money = values.money
        const url = `/hzsx/shopFund/withDraw?amount=${money}`
        this.setState({ fetching: true })
        request(url, {}, "get").then(() => {
          message.success("提现成功")
          this.props.refreshPageHandler()
          this.closeModal()
        }).finally(() => {
          this.setState({ fetching: false })
        })
      }
    })
  }

  render() {
    const { getFieldDecorator } = this.props.form

    return (
      <Modal
        title="提现"
        visible={this.props.show}
        onCancel={this.closeModal}
        onOk={this.sureTixianHandler}
        okButtonProps={{ disabled: this.state.fetching }}
      >
        <Form layout="inline">
          <Form.Item label="金额（元）：">
            {
              getFieldDecorator(
                "money",
                {
                  initialValue: this.props.initMoney,
                  rules: [
                    {
                      validator: (_, value, callback) => {
                        if (value == null || value == "") {
                          callback("提现金额不可为空")
                        } else if (value < Min) {
                          callback("低于0.1元不可提现")
                        } else if (value >= Max) {
                          callback(`单笔提现需小于${Max}元`)
                        } else {
                          callback()
                        }
                      }
                    }
                  ]
                },
              )(
                <InputNumber
                  min={Min}
                  max={Max}
                  placeholder="请输入提现金额"
                  allowClear
                  disabled={this.state.fetching}
                  className={styles.inputCls}
                />
              )
            }
          </Form.Item>
        </Form>
      </Modal>
    )
  }
}

TixianDialog.propTypes = {
  show: PropTypes.bool, // 是否显示弹窗
  hideModalHandler: PropTypes.func, // 关闭弹窗的方法
  initMoney: PropTypes.number, // 默认充值金额
  refreshPageHandler: PropTypes.func, // 更新账户明细的回调方法
}

export default TixianDialog