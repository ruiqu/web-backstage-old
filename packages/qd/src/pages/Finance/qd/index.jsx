import React, { PureComponent } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import MyPageTable from '@/components/MyPageTable';
import request from "@/services/baseService"
import {
  Tabs,
  Button,
  Select,
  Table,
  Card,
  message,
  Icon,
  Modal,
  Form,
  Input,
  DatePicker,

  Spin, Row, Col
} from 'antd';
const { TabPane } = Tabs;
import moment from "moment";
const { RangePicker } = DatePicker;
const { Option } = Select;
import { onTableData } from '@/utils/utils.js';
const QRCode = require('qrcode.react');
@connect(({ messageIndex }) => ({
  ...messageIndex,
}))
@Form.create()
class MessageSetting extends PureComponent {
  state = {
    loading: false,
    onChannel: '1', //默认
    newChannel: null,
    pageNumber: 1,
    pageSize: 10,
    total: 10,
    current: 1,
    selectedRowKeys: [],
    addvisibe: false, //控制添加modal的
    mobile: 'Banner添加', //添加modal的标题
    imgSrc: '', //上传成功后的url
    qname: '', //渠道名称
    istime: false,
    lookewm: false, //控制添加modal的
    ewmurl: "", //控制添加modal的
    createTimeStart: '', //查询开始时间
    createTimeEnd: '', //查询结束时间
    nameTitle: 'Banner名称',
    tabActiveKey: '1', //tab组件默认激活的
    editVisible: false,
    modalScore: 'add',
    address: 'banner', //编辑modal，目标位置的默认值
    name: null,
    url: null,
    sort: null,
    status: null,
    img: null,
    id: null,
    channelData: [],
    // 增加、编辑相关
    level: null,
    mobile: null,
    type: 1,
    tempKeywords: [],
    remark: null,
    tempTime: null,
    tempChannel: null,
    tid: null,
  };
  level = {
    1: '授权登录后但是未成功下单',
    2: '点击立即租赁但未成功下单',
    3: '收藏成功后未成功下单',
  };
  // List表头
  columns = [
    {
      title: '渠道名称',
      dataIndex: 'name',
      key: 'name',
      width: 100,
    },
    {
      title: 'uv',
      dataIndex: 'uv',
      key: 'uv',
      width: 100,
    },
    {
      title: '注册数量',
      dataIndex: 'registerNum',
      key: 'registerNum',
      width: 100,
    },
    {
      title: '实名数量',
      dataIndex: 'realNameNum',
      key: 'realNameNum',
      width: 100,
    },
    // {
    //   title: '下单人数',
    //   dataIndex: 'orderUserNum',
    //   key: 'orderUserNum',
    //   width: 100,
    // },
    {
      title: '机审通过数',
      dataIndex: 'newRiskPassNum',
      key: 'newRiskPassNum',
      width: 100,
    },
    {
      title: '成交订单数',
      dataIndex: 'dealNum',
      key: 'dealNum',
      width: 100,
    },
    // {
    //   title: '成交人数',
    //   dataIndex: '',
    //   width: 100,
    // },
    {
      title: '待转化',
      dataIndex: 'daizhuanhua',
      key: 'daizhuanhua',
      width: 100,
      render: (e, text) => {
        return text.daizhuanhua ? text.daizhuanhua : '';
      },
    },
    {
      title: '注册率',
      dataIndex: 'registerRate',
      key: 'registerRate',
      width: 100,
      render: (e, text) => {
        return text.registerRate ? `${text.registerRate.toFixed(2)}%` : '';
      },
    },
    // {
    //   title: '下单率',
    //   dataIndex: 'orderRate',
    //   key: 'orderRate',
    //   width: 100,
    //   render: (e, text) => {
    //     return text.orderRate ? `${text.orderRate.toFixed(2)}%` : '';
    //   },
    // },
    // {
    //   title: '机审通过率',
    //   dataIndex: 'riskPassRate',
    //   key: 'riskPassRate',
    //   width: 100,
    //   render: (e, text) => {
    //     return text.riskPassRate ? `${text.riskPassRate.toFixed(2)}%` : '';
    //   },
    // },
    {
      title: '成交率',
      dataIndex: 'dealRate',
      key: 'dealRate',
      width: 100,
      render: (e, text) => {
        return text.dealRate ? `${text.dealRate.toFixed(2)}%` : '';
      },
    },
  ];

  componentDidMount() {
    this.getList();
    //this.getChannel();
  }
  //获取渠道信息
  getChannel = () => {
    this.props.dispatch({
      type: 'order/PlateformChannel',
      payload: {},
      callback: res => {
        this.setState({
          channelData: res.data,
        });
      },
    });
  };
  //获取List的数据
  getList = () => {
    this.setState(
      {
        loading: true,
      },
      () => {
        console.log("获取渠道首页")
        let url = `/zyj-backstage-web/hzsx/business/channel/getChannelBySelf`
        /* if (this.state.istime) {
          url = `/zyj-backstage-web/hzsx/business/channel/getChannelListByDate`
        } */
        let param = {
          pageNumber: this.state.pageNumber,
          pageSize: this.state.pageSize,
          createTimeStart: this.state.createTimeStart,
          createTimeEnd: this.state.createTimeEnd,
          // name: this.state.qname,
          queryType:'07'
        }
        request(url, param, "post").then(res => {
          console.log(res, "返回的列表数据999")
          message.success('获取信息成功');
          this.setState({
            // total: res.total,
            channelData: res,
            // current: res.current,
            // loading: false,
          });
        })
        return
        this.props.dispatch({
          type: 'messageIndex/getList',
          payload: {
            pageNumber: this.state.pageNumber,
            pageSize: this.state.pageSize,
          },
          callback: res => {
            console.log(res);
            if (res.responseType === 'SUCCESS') {
              message.success('获取信息成功');
              this.setState({
                total: res.data.total,
                current: res.data.current,
                loading: false,
              });
            } else {
              message.error(res.msg);
            }
          },
        });
      },
    );
  };
  //删除渠道
  del = id => {
    //return console.log("开始删除")
    request(`/zyj-backstage-web/hzsx/business/channel/delChannel?channelId=${id}`, {}, "get").then(res => {
      message.success('删除成功');
      this.getList();
    })

  };

  edit = record => {
    this.setState(
      {
        addvisibe: true,
        modalScore: 'edit',
        tid: record.id,
        tempKeywords: JSON.parse(record.keywords || '[]'),
      },
      () => {
        this.props.form.setFieldsValue({
          tempChannel: record.channelId,
          remark: record.mark,
          mobile: record.mobile,
          type: record.status,
          tempTime: record.triggerInterval,
          level: record.level,
          name: record.name,
          id: record.id,
        });
      },
    );
  };
  createqr = (record) => {
    //console.log(record)
    return this.urlcreate(record.id, record.appid, true)
  }
  ewm = (record) => {
    let url = this.urlcreate(record.id, record.appid, true)
    console.log("erm:",url)
    this.setState(
      {
        lookewm: true,
        ewmurl: url,
      },
      () => {

      },
    );
  }
  copy = (record, isurl = false) => {
    //console.log("点击了复制", record)
    let transfer = document.createElement('input');
    let url = this.urlcreate(record.id, record.appid, isurl)
    document.body.appendChild(transfer);
    transfer.value = url;  // 这里表示想要复制的内容
    transfer.focus();
    transfer.select();
    if (document.execCommand('copy')) { //判断是否支持document.execCommand('copy')       document.execCommand('copy');
    }
    transfer.blur();
    message.success('复制成功');
    document.body.removeChild(transfer);
  };

  urlcreate = (id, appid, isurl = false) => {
    //alipays://platformapi/startapp?appId=1231111&page=pages/iii&query=pid%3D1
    let url = "alipays://platformapi/startapp?appId=" + appid + "&page=src/pages/index/index"
    url += "&query=cid%3D" + id
    if (isurl) url = "https://ds.alipay.com/?scheme=" + encodeURIComponent(url)
    return url
  };

  handleFilter = () => {
    this.getList();
  };

  addTempKeyword() {
    const tempKeywords = [...this.state.tempKeywords];
    tempKeywords.push({
      keyword: '',
      value: '',
    });
    //console.log(tempKeywords)
    this.setState({
      tempKeywords,
    });
  }
  deleteTempKeyword(index) {
    const tempKeywords = [...this.state.tempKeywords];
    tempKeywords.splice(index, 1);
    //console.log(tempKeywords)
    this.setState({
      tempKeywords,
    });
  }
  save(e, index, type) {
    const tempKeywords = [...this.state.tempKeywords];
    tempKeywords[index][type] = e.currentTarget.value;
    //console.log(tempKeywords)
    this.setState(
      {
        tempKeywords,
      },
      () => {
        this.props.form.validateFields(['tempKeywords'], { force: true });
      },
    );
  }

  //分页，下一页
  onChange = pageNumber => {
    // alert(pageNumber)
    this.setState(
      {
        pageNumber: pageNumber.current,
      },
      () => {
        this.handleFilter();
      },
    );
  };
  showTotal = () => {
    return `共有${this.state.total}条`;
  };
  //切换每页数量
  onShowSizeChange = pageSize => {
    //console.log(pageSize, 'pageSize');
    this.setState(
      {
        pageSize: pageSize.current,
        pageNumber: 1,
      },
      () => {
        this.handleFilter();
      },
    );
  };
  //表格多选框的回调
  onSelectChange = (selectedRowKeys, selectedRows) => {
    this.setState({ selectedRowKeys });
  };

  // 新增
  add = () => {
    this.setState(
      {
        addvisibe: true,
        modalScore: 'add',
        tid: '',
        tempKeywords: [],
      },
      () => {
        this.props.form.setFieldsValue({
          tempChannel: null,
          remark: null,
          tempKeywords: [],
          mobile: null,
          type: 1,
          tempTime: null,
          level: null,
          name: null,
        });
      },
    );
  };
  // modal的确认，确认添加和确认更新的
  addOk = () => {
    this.setState({ loading: true })
    this.props.form.validateFields({ force: true }, (err, value) => {
      if (!err) {
        let param = {
          channelId: value.tempChannel,
          mark: value.remark || "",
          mobile: value.mobile,
          level: value.level,
          password: value.password,
          name: value.name,
        }
        let url = `/zyj-backstage-web/hzsx/business/channel/insertChannel`
        if (this.state.modalScore != 'add') {  //如果是编辑
          url = `/zyj-backstage-web/hzsx/business/channel/uptChannel`
          param.id = this.state.tid
        }
        //return console.log(param, url)
        request(url, param, "post").then(resData => {
        }).finally(() => {
          this.setState({ loading: false, addvisibe: false, })
          this.getList();
        })
      }
    });
  };
  tempKeywordsValidator = (rule, val, callback) => {
    val = this.state.tempKeywords;
    if (!(val && val.length)) {
      callback('模板关键词不能为空');
    }
    let validateResult = true;
    val.forEach(item => {
      if (!(item.keyword && item.value)) {
        validateResult = false;
      }
    });
    if (!validateResult) {
      callback('模板关键词不能有空格');
    }
    callback();
  };
  tempTimeValidator = (rule, val, callback) => {
    if (/^[0-6]$/.test(val)) {
      callback();
    } else {
      callback('只能输入0-6的数字');
    }
  };
  //查询
  handleSubmit1 = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (values.createDate && values.createDate.length > 1) {
        values.createTimeStart = `${moment(values.createDate[0]).format('YYYY-MM-DD')} 00:00:00`;
        values.createTimeEnd = `${moment(values.createDate[1]).format('YYYY-MM-DD')} 23:59:59`;
        values.istime = true;
      }
      this.setState(
        {
          loading: true,
          ...values,
        },
        () => {
          this.getList();
          console.log("values", values)

        },
      );
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      this.setState(
        {
          loading: true,
        },
        () => {
          console.log("values", values)
          this.props.dispatch({
            type: 'messageIndex/getList',
            payload: {
              pageNumber: 1,
              pageSize: 10,
              mobile: values.title1,
              channelId: values.tempChannel1,
            },
            callback: res => {
              console.log(res);
              if (res.responseType === 'SUCCESS') {
                message.success('获取信息成功');
                this.setState({
                  total: res.data.total,
                  current: res.data.current,
                  loading: false,
                });
              } else {
                message.error(res.msg);
              }
            },
          });
        },
      );
    });
  };

  handleReset = e => {
    this.props.form.resetFields();
    this.props.form.setFieldsValue({
      status: undefined,
      type: undefined,
    });
    this.handleSubmit(e);
  };

  render() {
    const {
      total,
      current,
      addvisibe,
      modalScore,
      channelData,
      level,
      name,
      mobile,
      type,
      tempKeywords,
      remark,
      ewmurl,
      lookewm,
      tempChannel,
    } = this.state;
    const paginationProps = {
      current: current,
      pageSize: 10,
      total: total,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / 10)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };
    const { list } = this.props;
    const editColumns = [
      {
        mobile: '关键词',
        dataIndex: 'keyword',
        editable: true,
        width: '40%',
        render: (text, record, index) => (
          <Input
            defaultValue={text}
            onPressEnter={e => this.save(e, index, 'keyword')}
            onBlur={e => this.save(e, index, 'keyword')}
          />
        ),
      },
      {
        mobile: '内容',
        dataIndex: 'value',
        editable: true,
        width: '40%',
        render: (text, record, index) => (
          <Input
            defaultValue={text}
            onPressEnter={e => this.save(e, index, 'value')}
            onBlur={e => this.save(e, index, 'value')}
          />
        ),
      },
      {
        mobile: (
          <Icon
            onClick={() => this.addTempKeyword()}
            style={{ cursor: 'pointer' }}
            type="plus-circle"
          />
        ),
        dataIndex: 'operation',
        align: 'center',
        width: '20%',
        render: (text, record, index) => {
          return (
            <Icon
              onClick={() => this.deleteTempKeyword(index)}
              style={{ cursor: 'pointer' }}
              type="minus-circle"
            />
          );
        },
      },
    ];

    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    return (
      <PageHeaderWrapper>
        <Card bordered={false}>
          <Form layout="inline" onSubmit={this.handleSubmit1}>
            <Row>
              {/* <Col span={8}>
                <Form.Item label="渠道名称">
                  {getFieldDecorator(
                    'qname',
                    {},
                  )(<Input allowClear placeholder="请输入渠道名称" />)}
                </Form.Item>
              </Col> */}

              <Col span={16}>
                <Form.Item label="创建时间">
                  {getFieldDecorator('createDate', {})(<RangePicker />)}
                </Form.Item>
              </Col>
            </Row>
            <div>
              <Form.Item>
                <Button type="primary" htmlType="submit">
                  查询
                </Button>
              </Form.Item>
              {/* <Form.Item>
                <Button type="primary" onClick={this.add}>
                  新增
                </Button>
              </Form.Item> */}

            </div>
          </Form>

          <MyPageTable
            paginationProps={false}
            dataSource={channelData}
            scroll
            bordered
            columns={this.columns}
            onPage={this.onChange}
          />
        </Card>
        <Modal
          mobile={modalScore == 'add' ? '新增' : '修改'}
          visible={addvisibe}
          onCancel={() => {
            this.setState({
              addvisibe: false,
            });
            this.props.form.resetFields();
          }}
          onOk={this.addOk}
        >
          <Form {...formItemLayout}>
            <Form.Item label="渠道名称">
              {getFieldDecorator('name', {
                rules: [
                  {
                    required: true,
                    message: '渠道名称不能为空',
                  },
                ],
                initialValue: name ? name : null,
              })(<Input />)}
            </Form.Item>
            <Form.Item label="手机号">
              {getFieldDecorator('mobile', {
                rules: [
                  {
                    required: true,
                    message: '手机号不能为空',
                  },
                ],
                initialValue: mobile ? mobile : null,
              })(<Input />)}
            </Form.Item>
            <Form.Item label="密码">
              {getFieldDecorator('password', {})(<Input type="password" />)}
            </Form.Item>
            <Form.Item label="备注">
              {getFieldDecorator('remark', {
                initialValue: remark ? remark : null,
              })(<Input />)}
            </Form.Item>
          </Form>
        </Modal>
        <Modal
          mobile='渠道二维码'
          visible={lookewm}
          onCancel={() => {
            this.setState({
              lookewm: false,
            });
          }}
        >
          <QRCode value={ewmurl} size={256} id={'qrCode'} />
        </Modal>
      </PageHeaderWrapper>
    );
  }
}

export default MessageSetting;
