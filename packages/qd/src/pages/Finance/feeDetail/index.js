/**
 * 费用结算明细查询页面
 */
import React from "react"
import { connect } from "dva"
import { PageHeaderWrapper } from "@ant-design/pro-layout"
import { Card, Form, Tabs, Row, Select, DatePicker, Table, Button } from "antd"
import { tab1, tab2, status_weijiesuan, status_yijiesuan, defaultPageSize } from "../../../models/feeDetail"
import styles from "./index.less"
import request from "@/services/baseService"
import { formatTime2ApiNeed, getParam, onTableData } from "../../../../src/utils/utils"

const { TabPane } = Tabs
const { RangePicker } = DatePicker


const tabs = [
  { key: tab1, tab: "芝麻租押分离接口费用" },
  // { key: tab2, tab: "e签宝费用" }
]

const options = [
  { value: status_yijiesuan, cn: "已结算" },
  { value: status_weijiesuan, cn: "未结算" },
]

@connect(({ feeDetailModel }) => ({
  ...feeDetailModel,
}))
@Form.create()
class FeeDetail extends React.Component {
  state = {
    listApiResult: {}, // 列表接口所返回的数据
    loading: false, // 是否已经处于加载状态中
    hasCarryed: false, // 是否已经携带过fundFlowId查过一次数据了
  }

  componentDidMount() {
    const atab = this.props.activeTab
    const qObj = this.props.listApiParams[atab] || {}
    this.fetchDataHandler(qObj)
  }

  componentWillReceiveProps(nextProps) {
    const currentTab = this.props.activeTab
    const nextTab = nextProps.activeTab

    if (currentTab != nextTab) { // 说明切换了tab，那么是一定要进行数据加载的
      const newQueryObj = this.props.listApiParams[nextTab] // 新的查询条件
      this.fetchDataHandler(newQueryObj, nextTab)
    } else { // 没有切换tab的话，也有可能请求参数发生了变化
      const currentQuery = this.props.listApiParams[currentTab]
      const nextQuery = nextProps.listApiParams[currentTab]

      const currentStr = JSON.stringify(currentQuery)
      const nextStr = JSON.stringify(nextQuery)

      if (currentStr != nextStr) { // 查询条件有变
        this.fetchDataHandler(nextQuery)
      }
    }
  }

  /**
   * 查询列表数据
   * @param {*} queryObj : 接口所需的查询数据
   * @param {*} tab : 当前菜单栏
   */
  fetchDataHandler = (queryObj, tab) => {
    const newObj = { ...queryObj }
    const postData = { ...formatTime2ApiNeed(newObj) }

    const activeTab = tab || this.returnActiveTab()
    const url = "/hzsx/feeBill/page"
    if (activeTab === tab1) { // 芝麻租押分离
      postData.type = "ASSESSMENT"
    } else if (activeTab === tab2) { // e签宝
      postData.type = "E_SIGN"
    }
    
    delete postData.times // 所选时间的字段发生变化
    const fid = getParam("id")
    fid && !this.state.hasCarryed && (postData.fundFlowId = fid)

    this.setState({ loading: true })
    request(url, postData, "post").then(res => {
      let resultFlag = false
      fid && (resultFlag = true)
      this.setState({ listApiResult: res, hasCarryed: resultFlag })
    }).finally(() => {
      this.setState({ loading: false })
    })
  }

  // 切换菜单栏的时候触发
  changeTabHandler = tab => {
    this.props.form.resetFields() // 这个是必要的，尽管redux覆盖了值
    this.props.dispatch({ type: "feeDetailModel/mutFeeDetailActiveTab", payload: tab }) // 修改redux中所存储的焦点tab
  }

  // 返回处于焦点的tab
  returnActiveTab = () => this.props.activeTab || tab1 // 默认是第一个tab，优先级，先本地，在redux，在默认

  // 返回当前的筛选数据
  returnQueryObj = () => {
    const tab = this.returnActiveTab()
    return this.props.listApiParams?.[tab] || {}
  }

  // 表单的提交按钮
  onSearch = event => { // 点击搜索的时候把值存储到redux
    event.preventDefault()
    this.props.form.validateFields((err, value) => {
      if (!err) {
        // 合成表单的搜索数据，然后进行提交上传
        const currentQueryObj = this.returnQueryObj()
        const newQueryObj = {
          ...currentQueryObj,
          ...value,
          pageNumber: 1, // 从第一页开始进行搜索
          actionTime: Date.now(), // 确保每次点击按钮都会调用接口
        }
        const payload = { tab: this.returnActiveTab(), val: newQueryObj }
        this.props.dispatch({ type: "feeDetailModel/muFeeDetailListApiParamsComplete", payload })
      }
    })
  }

  // 初始化搜索表单
  resetForm = () => {
    this.props.form.resetFields()
    const resetPostData = {
      // status: status_yijiesuan,
      status: null,
      times: [],
      pageNumber: 1,
      pageSize: defaultPageSize,
    }
    const payload = { tab: this.returnActiveTab(), val: resetPostData }
    this.props.dispatch({ type: "feeDetailModel/muFeeDetailListApiParamsComplete", payload }) // 修改redux，componentWillReceiveProps中会调用接口
  }

  // 渲染搜索区域，两者的搜索区域都是相似的
  renderSearchArea = () => {
    const { getFieldDecorator } = this.props.form
    const cacheForm = this.returnQueryObj()

    return (
      <Form className={styles.mb15} layout="inline" onSubmit={this.onSearch}>
        <Row className={styles.mb20}>
          <Form.Item label="结算状态">
            {
              getFieldDecorator(
                "status",
                {
                  initialValue: cacheForm.status,
                  // rules: [
                  //   { required: true, message: "请选择结算状态" },
                  // ],
                }
              )(
                <Select className={styles.selectWrapper}>
                  {
                    options.map(obj => (
                      <Select.Option key={obj.value} value={obj.value}>{ obj.cn }</Select.Option>
                    ))
                  }
                </Select>
              )
            }
          </Form.Item>
          <Form.Item label="账单生成时间">
            {
              getFieldDecorator("times",{ initialValue: cacheForm.times })(
                <RangePicker />
              )
            }
          </Form.Item>
        </Row>
        <Button type="primary" htmlType="submit">
          查询
        </Button>
        <Button onClick={this.resetForm}>
          重置
        </Button>
      </Form>
    )
  }

  // 渲染表格区域
  renderTableArea = () => {
    const tableColumns = [
      { title: "订单编号", dataIndex: "orderId" },
      { title: "费用（元）", dataIndex: "amount" },
      {
        title: "结算状态",
        dataIndex: "status",
        render: val => {
          const sMap = {
            WAITING_SETTLEMENT: "待结算",
            SETTLED: "已结算"
          }
          const cn = sMap[val] || "-"
          return cn
        }
      },
      { title: "账单生成时间", dataIndex: "createTime" }
    ]
    const { pageNumber, pageSize } = this.returnQueryObj() || {}

    // 改变页码时触发
    const changePageHandler = pn => {
      const payload = {
        tab: this.returnActiveTab(),
        key: "pageNumber",
        val: pn,
      }
      this.props.dispatch({ type: "feeDetailModel/muFeeDetailListApiParams", payload })
    }

    // 改变每页显示数量时触发，修改分页时从第一页查起
    const onShowSizeChangeHandler = (_, size) => {
      const completeQueryObj = {
        ...this.returnQueryObj(),
        pageNumber: 1,
        pageSize: size,
      }
      const payload = {
        tab: this.returnActiveTab(),
        val: completeQueryObj,
      }
      this.props.dispatch({ type: "feeDetailModel/muFeeDetailListApiParamsComplete", payload })
    }

    const tableSource = onTableData(this.state.listApiResult?.records || [])
    const totalAmount = this.state.listApiResult?.total || 1

    const showTotal = () => `共有${totalAmount}条`

    return (
      <Table
        columns={tableColumns}
        dataSource={tableSource}
        loading={this.state.loading}
        pagination={{
          current: pageNumber,
          pageSize,
          total: totalAmount,
          onChange: changePageHandler,
          showTotal,
          showQuickJumper: true,
          pageSizeOptions: ['5', '10', '20'],
          showSizeChanger: true,
          onShowSizeChange: onShowSizeChangeHandler
        }}
      />
    )
  }

  render() {
    return (
      <PageHeaderWrapper>
        <Card bordered={false}>
          <Tabs animated={false} onChange={this.changeTabHandler} activeKey={this.returnActiveTab()}>
            {
              tabs.map(obj => (
                <TabPane tab={obj.tab} key={obj.key}>
                  {/** 搜索区域 */}
                  { this.renderSearchArea() }
                  
                  {/** 表格区域 */}
                  { this.renderTableArea() }
                </TabPane>
              ))
            }
          </Tabs>
        </Card>
      </PageHeaderWrapper>
    )
  }
}

export default FeeDetail