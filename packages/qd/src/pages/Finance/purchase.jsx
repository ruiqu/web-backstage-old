import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { getParam } from '@/utils/utils';
import { Card, message, Form } from 'antd';
import SplitBillService from './services/splitBill';
import PurchaseTable from './tables/purchaseTable';

const defaultPageSize = 10
@connect()
@Form.create()
class NormalSplitBill extends PureComponent {
  state = {
    tableData: [],
    loading: true,
    current: 1,
    pageNumber: 1,
    pageSize: defaultPageSize,
  };

  componentDidMount() {
    this.initData();
  }

  initData = () => {
    let payload = {
      pageNumber: this.state.pageNumber,
      pageSize: this.state.pageSize,
      accountPeriodId:getParam("id")
    };
    this.setState(
      {
        loading: true,
      },
      () => {
        SplitBillService.purchasePage(payload).then(res => {
          message.success('获取信息成功');
          this.setState({
            tableData: res.records,
            total: res.total,
            loading: false,
            current: res.current,
          });
        });
      },
    );
  };

  handleFilter = (data = {}) => {
    if (data.queryType && data.queryType === '分页') {
      this.initData();
    } else {
      this.setState(
        {
          pageNumber: 1,
          pageSize: 10,
        },
        () => {
          this.initData();
        },
      );
    }
  };

  // 分页，下一页
  onChange = pageNumber => {
    this.setState(
      {
        pageNumber: pageNumber,
      },
      () => {
        this.handleFilter({ queryType: '分页' });
      },
    );
  };

  // 切换每页数量
  onShowSizeChange = pageSize => {
    this.setState({ pageSize, pageNumber: 1 }, () => {
      this.handleFilter({ queryType: '分页' });
    });
  };

  render() {
    const { loading, current, total, pageSize, tableData } = this.state;

    return (
      <PageHeaderWrapper>
        <Card bordered={false} style={{ marginTop: 20 }}>
          <PurchaseTable
            tableData={tableData}
            pageSize={pageSize}
            loading={loading}
            total={total}
            current={current}
            onChangeHandler={this.onChange}
            onShowSizeChangeHandler={this.onShowSizeChange}
          />
        </Card>
      </PageHeaderWrapper>
    );
  }
}

export default NormalSplitBill;
