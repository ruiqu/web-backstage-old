import request from '@/services/baseService';

export default {
    queryAccountPeriodPage: data => {
    return request(`/hzsx/accountPeriod/queryAccountPeriodPage`, data);
  },
  queryAccountPeriodDetail: data => {
    return request(`/hzsx/accountPeriod/queryAccountPeriodDetail`, data,'get');
  },
  listRemark: data => {
    return request(`/hzsx/accountPeriod/listRemark`, data);
  },
  addRemark: data => {
    return request(`/hzsx/accountPeriod/addRemark`, data);
  },
  submitSettle: data => {
    return request(`/hzsx/accountPeriod/submitSettle`, data);
  },
  submitAudit: data => {
    return request(`/hzsx/accountPeriod/submitAudit`, data);
  },
  submitPay: data => {
    return request(`/hzsx/accountPeriod/submitPay`, data);
  },
  selectPayInfo: data => {
    return request(`/hzsx/accountPeriod/selectPayInfo`, data,'get');
  },
  accountPeriodRent: data => {
    return request(`/hzsx/export/accountPeriodRent`, data);
  },
  accountPeriodPurchase: data => {
    return request(`/hzsx/export/accountPeriodPurchase`, data);
  },
  accountPeriodBuyOut: data => {
    return request(`/hzsx/export/accountPeriodBuyOut`, data);
  },
};
