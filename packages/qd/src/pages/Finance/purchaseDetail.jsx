import React, { PureComponent, Fragment } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import {
  Card,
  Spin,
  message,
  Table,
  Modal,
  Button,
  Divider,
  Form,
} from 'antd';
import CustomCard from '@/components/CustomCard';
import { renderDescriptionsItems } from '@/components/LZDescriptions';
import status from '@/config/status.config';
import SplitBillService from './services/splitBill';
import { onTableData } from '@/utils/utils';
import { router } from 'umi';

@connect()
@Form.create()
class GoodsDetail extends PureComponent {
  state = {
    id: '',
    loading: true,
    detail: {},
    visible: false,
    imgSrc: [],
    imgLength: 0,
    current: 0,
    categoryVisible: false,
    categoryId: null,
    actionVisible: false,
    confirmLoading: false,
    tittle: '',
    status: 0,
    reason: null,
    isScale: null,
    ByStagesDtoList: [],
    CashesDto: [],
  };

  columns = [
    {
      title: '款式',
      align: 'center',
      key: 'style',
      width: '30%',
      render: (text, record) => {
        return record.values.map((item, index) => {
          if (index + 1 !== record.values.length) {
            return (
              <Fragment key={item.name}>
                <span>{item.name}/</span>
              </Fragment>
            );
          } else {
            return (
              <Fragment key={item.name}>
                <span>{item.name}</span>
              </Fragment>
            );
          }
        });
      },
    },
    {
      title: '市场价格',
      dataIndex: 'marketPrice',
      align: 'center',
      key: 'marketPrice',
    },
    {
      title: '销售价格',
      align: 'center',
      key: 'salePrice',
      render: record => {
        return (
          <div>
            {record.marketPrice * 1.24 > record.salePrice ? (
              <span>{record.salePrice}</span>
            ) : (
              <span style={{ color: 'red' }}>{record.salePrice}</span>
            )}
          </div>
        );
      },
    },
    {
      title: '租赁价格',
      dataIndex: 'cyclePrices',
      align: 'center',
      key: 'cyclePrices',
      render: text => (
        <div>
          {text.map(item => (
            <p key={item.id}>
              {item.days}天：{item.price}
            </p>
          ))}
        </div>
      ),
    },
  ];
  columnses = [
    {
      title: '款式',
      align: 'center',
      key: 'style',
      width: '30%',
      render: (text, record) => {
        return record.values.map((item, index) => {
          if (index + 1 !== record.values.length) {
            return (
              <Fragment key={item.name}>
                <span>{item.name}/</span>
              </Fragment>
            );
          } else {
            return (
              <Fragment key={item.name}>
                <span>{item.name}</span>
              </Fragment>
            );
          }
        });
      },
    },
    {
      title: '市场价格',
      dataIndex: 'marketPrice',
      align: 'center',
      key: 'marketPrice',
    },
    {
      title: '租赁价格',
      dataIndex: 'cyclePrices',
      align: 'center',
      key: 'cyclePrices',
      render: text => (
        <div>
          {text.map(item => (
            <p key={item.id}>
              {item.days}天：{item.price}
            </p>
          ))}
        </div>
      ),
    },
  ];
  addressColumns = [
    {
      title: '姓名',
      dataIndex: 'name',
      align: 'center',
      key: 'name',
    },
    {
      title: '手机',
      dataIndex: 'telephone',
      align: 'center',
      key: 'telephone',
    },
    {
      title: '省',
      dataIndex: 'provinceSrc',
      align: 'center',
      key: 'provinceSrc',
    },
    {
      title: '市',
      dataIndex: 'citySrc',
      align: 'center',
      key: 'citySrc',
    },
    {
      title: '区',
      dataIndex: 'areaSrc',
      align: 'center',
      key: 'areaSrc',
    },
    {
      title: '街道',
      dataIndex: 'street',
      align: 'center',
      key: 'street',
      width: '200px',
    },
  ];
  goodsColumns = [
    {
      title: '商品图片',
      dataIndex: 'src',
      align: 'center',
      render: text => <img style={{ width: 85, hight: 85 }} src={text} />,
    },
    {
      title: '商品名称',
      dataIndex: 'productName',
      align: 'center',
      key: 'productName',
      width: '20%',
    },
    {
      title: '商品编号',
      dataIndex: 'productId',
      align: 'center',
      key: 'productId',
      width: '20%',
    },
    {
      title: '规格颜色',
      dataIndex: 'specAll',
      render: specAll => {
        return (
          <span>
            {specAll && specAll[0] && specAll[0].platformSpecValue}/
            {specAll && specAll[0] && specAll[1].platformSpecValue}
          </span>
        );
      },
    },
    {
      title: '数量',
      render: e => 1,
      align: 'center',
    },
    // {
    //   title: '是否可分期',
    //   dataIndex: 'isSupportStage',
    //   align: 'center',
    //   render: text => (text === 1 ? '分期' : '不分期'),
    // },
    {
      title: '运费',
      dataIndex: 'freightType',

      align: 'center',
    },
  ];
  billColumns = [
    // {
    //   title: '总押金',
    //   dataIndex: 'totalDeposit',
    //   key: 'totalDeposit',
    //   align: 'center',
    // },
    // {
    //   title: '押金减免',
    //   dataIndex: 'depositReduction',
    //   align: 'center',
    //   key: 'depositReduction',
    // },
    // {
    //   title: '冻结额度',
    //   dataIndex: 'freezePrice',
    // },
    // {
    //   title: '信用减免',
    //   dataIndex: 'creditDeposit',
    //   align: 'center',
    // },
    // {
    //   title: '实付押金',
    //   dataIndex: 'deposit',
    //   align: 'center',
    //   key: 'deposit',
    // },
    {
      title: '总金额',
      dataIndex: 'totalAmount',
      align: 'center',
      key: 'totalAmount',
    },
    // {
    //   title: '运费',
    //   dataIndex: 'freightPrice',
    //   align: 'center',
    //   key: 'freightPrice',
    // },
    {
      title: '平台优惠',
      dataIndex: 'platformCouponReduction',
      align: 'center',
      key: 'platformCouponReduction',
    },
    {
      title: '店铺优惠',
      dataIndex: 'shopCouponReduction',
      align: 'center',
      key: 'shopCouponReduction',
    },
    {
      title: '分期信息',
      dataIndex: 'hbPeriodNum',
      align: 'center',
      key: 'hbPeriodNum',
    },
    {
      title: '运费',
      dataIndex: 'freightAmount',
      align: 'center',
      key: 'freightAmount',
    },
  ];
  stagingColumns = [
    {
      title: '总金额',
      dataIndex: 'totalAmount',
      align: 'center',
    },
    {
      title: '支付时间',
      dataIndex: 'createTime',
      align: 'center',
      render: text => {
        return <span>{text ? text : '--'}</span>;
      },
    },
    { title: '商家结算金额', dataIndex: 'toShopAmount', align: 'center' },
    { title: '平台佣金', dataIndex: 'toOpeAmount', align: 'center' },
    {
      title: '与商家结算状态',
      dataIndex: 'status',
      align: 'center',
      render: text => status[text],
    },
    { title: '结算人', dataIndex: 'name', align: 'center' },
    { title: '结算时间', dataIndex: 'splitBillTime', align: 'center' },
  ];
  buyOrderColumns = [
    {
      title: '创建时间',
      dataIndex: 'createTime',
      key: 'createTime',
      align: 'center',
    },
    {
      title: '订单编号',
      dataIndex: 'orderId',
      align: 'center',
      key: 'orderId',
      render: orderId => {
        return <a onClick={() => router.push(`/Order/Details?id=${orderId}`)}>{orderId}</a>;
      },
    },
    {
      title: '商品名称',
      dataIndex: 'productName',
      align: 'center',
      key: 'productName',
    },
    {
      title: '销售价',
      dataIndex: 'realSalePrice',
      align: 'center',
      key: 'realSalePrice',
    },
    {
      title: '已付租金',
      dataIndex: 'paidRent',
      align: 'center',
      key: 'paidRent',
    },
    {
      title: '买断尾款',
      dataIndex: 'endFund',
      align: 'center',
      key: 'endFund',
    },
    { title: '商家结算金额', dataIndex: 'toShopAmount', align: 'center' },
    { title: '平台佣金', dataIndex: 'toOpeAmount', align: 'center' },
    {
      title: '与商家结算状态',
      dataIndex: 'splitBillStatus',
      align: 'center',
      render: text => status[text],
    },
    { title: '结算人', dataIndex: 'userName', align: 'center' },
    { title: '结算时间', dataIndex: 'splitBillTime', align: 'center' },
    // {title: '操作', align: 'center', key: 'action',
    //   render: (text, record) => {
    //     return (
    //       <div className="table-action">
    //         <Popconfirm title="是否确定进行代发申请?" onConfirm={()=> this.daifa(record)}>
    //           <a>
    //             代发申请
    //           </a>
    //         </Popconfirm>
    //       </div>
    //     );
    //   },
    // },
  ];

  componentDidMount() {
    this.getDetail();
  }

  getDetail = () => {
    const {
      match: {
        params: { id },
      },
    } = this.props;
    this.setState({
      loading: true,
      id,
    });
    SplitBillService.purchaseDetail({ id })
      .then(res => {
        this.setState({
          detail: res || {},
          ByStagesDtoList: [
            {
              totalAmount: res.totalAmount,
              status: res.status,
              splitBillTime: res.splitBillTime,
              toShopAmount: res.toShopAmount,
              toOpeAmount: res.toOpeAmount,
              name: res.name,
              createTime: res.createTime,
              orderId: res.orderId,
            },
          ],
          CashesDto: [
            {
              totalAmount: res.totalAmount,
              shopCouponReduction: res.shopCouponReduction,
              platformCouponReduction: res.platformCouponReduction,
              freightAmount: res.freightAmount,
              hbPeriodNum: res.hbPeriodNum,
            },
          ],
        });
      })
      .finally(() => {
        this.setState({
          loading: false,
        });
      });
  };

  viewImg = (data, index) => {
    this.setState({
      visible: true,
      imgSrc: data,
      imgLength: data.length,
      current: index,
    });
  };
  // 图片modal的关闭
  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };
  // 下一张
  next = () => {
    let a = this.state.current;
    a += 1;
    this.setState({
      current: a,
    });
  };
  // 上一张
  prev = () => {
    let a = this.state.current;
    a -= 1;
    this.setState({
      current: a,
    });
  };

  daifa = record => {
    SplitBillService.paySplitBills({ orderId: record.orderId, period: 1 }).then(res => {
      message.success('代发成功');
    });
  };

  render() {
    const { loading, imgSrc, visible, current, imgLength, detail } = this.state;
    const {
      orderInfoDto = {},
      shopInfoDto = {},
      productInfo = {},
      userOrderCashesDto = {},
      orderByStagesDtoList = [],
      buyOutOrderList = [],
    } = detail;
    console.log(detail, 'detail');
    const orderInfoFields = [
      { label: '下单人姓名', value: detail.userName },
      { label: '下单人手机号', value: detail.telephone },
      { label: '下单时间', value: detail.createTime },
      { label: '订单编号', value: detail.orderId },
    ];

    const shopInfoField = [
      { label: '商家名称', value: detail.shopName },
      { label: '商家电话', value: detail.telephone },
    ];

    return (
      <PageHeaderWrapper>
        <Spin spinning={loading}>
          <Card bordered={false} style={{ marginTop: 20 }}>
            {renderDescriptionsItems(orderInfoFields, <CustomCard title="订单信息" />)}
            <Divider dashed />
            {renderDescriptionsItems(shopInfoField, <CustomCard title="商家信息" />)}
            <CustomCard title="商品信息" />
            <Table
              style={{ marginTop: 20 }}
              columns={this.goodsColumns}
              dataSource={onTableData([productInfo])}
              rowKey={record => record.id}
              pagination={false}
            />
            <CustomCard title="账单信息" style={{ marginTop: 30 }} />
            <Table
              style={{ marginTop: 10 }}
              columns={this.billColumns}
              dataSource={onTableData(this.state.CashesDto)}
              rowKey={record => record.id}
              pagination={false}
            />
            <CustomCard title="结算信息" style={{ marginTop: 30 }} />
            <Table
              style={{ marginTop: 10 }}
              columns={this.stagingColumns}
              dataSource={onTableData(this.state.ByStagesDtoList)}
              rowKey={record => record.id}
              pagination={false}
            />
          </Card>
        </Spin>

        <Modal
          visible={visible}
          footer={
            <div style={{ textAlign: 'center' }}>
              <Button disabled={current <= 0 ? true : false} onClick={this.prev}>
                上一张
              </Button>
              <Button
                disabled={current >= imgLength - 1 ? true : false}
                type="primary"
                onClick={this.next}
              >
                下一张
              </Button>
            </div>
          }
          onCancel={this.handleCancel}
          width={600}
        >
          <img
            src={imgSrc.length > 0 ? imgSrc[current].src : null}
            alt=""
            style={{ width: '100%', paddingTop: 15 }}
          />
        </Modal>
      </PageHeaderWrapper>
    );
  }
}

export default GoodsDetail;
