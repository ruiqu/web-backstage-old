import React, { Fragment } from 'react';
import { connect } from 'dva';
import {
  Tabs,
  Form,
  Spin,
  Card,
  Input,
  Upload,
  Icon,
  message,
  Row,
  Col,
  Button,
  DatePicker,
  Divider,
  Radio,
} from 'antd';
// import Mods from './Agreement.jsx'
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { uploadUrl } from '@/config';
import { forIdCard, goToRouter } from '../../utils/utils';
import router from 'umi/router';
import CustomCard from '@/components/CustomCard';
import { LZFormItem } from '@/components/LZForm';
import moment from 'moment';
import { renderDescriptionsItems } from '@/components/LZDescriptions';
import busShopService from '@/services/busShop';

const { TabPane } = Tabs;
const { RangePicker } = DatePicker;
const FormItem = Form.Item;

@connect()
@Form.create()
class ShopInfo extends React.PureComponent {
  state = {
    loading: false,
    uploading: false,
    detail: {},
    shopInfo: {},
    enterpriseInfo: {},
    shopEnterpriseCertificates: [],
    isPhoneExamination: null,
    visible: false,
  };

  componentWillMount = () => {
    this.selectBusShopInfo();
  };

  selectBusShopInfo = () => {
    this.setState({
      loading: true,
    });
    busShopService
      .selectBusShopInfo()
      .then(res => {
        this.setState({
          detail: res || {},
          shopInfo: res.shop || {},
          shopEnterpriseCertificates:
            (res.enterpriseInfoDto && res.enterpriseInfoDto.shopEnterpriseCertificates) || [],
          enterpriseInfo:
            (res.enterpriseInfoDto && res.enterpriseInfoDto.shopEnterpriseInfos) || {},
          isPhoneExamination: res.shop && res.shop.isPhoneExamination,
        });
      })
      .finally(() => {
        this.setState({
          loading: false,
        });
      });
  };

  handleSubmit = () => {
    const { detail, isPhoneExamination } = this.state;
    if (!detail) {
      message.error('请新增店铺信息');
      return;
    }
    detail.shop.isPhoneExamination = isPhoneExamination;
    busShopService.updateShopAndEnterpriseInfo(detail).then(res => {
      message.success('更新成功');
    });
  };

  handleCancel = () => {
    this.setState({
      isPhoneExamination: this.state.shopInfo.isPhoneExamination,
    });
  };

  handleEdit = () => {
    const { dispatch } = this.props;
    goToRouter(dispatch, '/shop/info/addInfo');
  };

  handlePhoneExaminationChange = evt => {
    this.setState({
      isPhoneExamination: evt.target.value,
    });
  };

  renderShopInfo = () => {
    const { shopInfo } = this.state;
    const shopField = [
      { label: '店铺名称', value: shopInfo.name },
      { label: '店铺客服电话', value: shopInfo.serviceTel },
      { label: '店铺联系邮箱', value: shopInfo.userEmail },
      { label: '店铺联系人姓名', value: shopInfo.userName },
      { label: '店铺联系人电话', value: shopInfo.userTel },
      { label: '店铺创建时间', value: shopInfo.createTime },
      { label: '支付宝账号', value: shopInfo.zfbCode },
      { label: '支付宝姓名', value: shopInfo.zfbName },
      {
        label: '店铺合同',
        value: shopInfo.shopContractLink,
        render: shopInfo.shopContractLink ? (
          <a href={shopInfo.shopContractLink} target="_blank">
            《租赁商家入驻合作协议》
          </a>
        ) : (
          <a onClick={() => router.push('/shop/info/Agreement')}>点击签约</a>
        ),
      },
      {
        label: '店铺logo',
        value: shopInfo.logo,
        className: 'df',
        render: <img src={shopInfo.logo} className="table-cell-img" alt="" />,
      },
      { label: '店铺描述', value: shopInfo.description, className: 'vertical-align-baseline' },
      { label: '店铺地址', value: shopInfo.shopAddress || '-' }
    ];
    return (
      <div>
        <Card
          bordered={false}
          style={{ marginTop: 20 }}
          title={<CustomCard title="店铺信息" />}
          extra={
            <a onClick={this.handleEdit}>
              <Icon type="edit" />
              修改
            </a>
          }
        >
          {renderDescriptionsItems(shopField)}
        </Card>
      </div>
    );
  };

  renderCompanyInfo = () => {
    const { enterpriseInfo, shopEnterpriseCertificates } = this.state;
    const hasImgs = shopEnterpriseCertificates && shopEnterpriseCertificates.length > 1;
    const getDate = date => {
      return date && date.substr(0, 10);
    };
    const companyField = [
      { label: '企业名称', value: enterpriseInfo.name },
      { label: '企业注册资金', value: enterpriseInfo.registrationCapital },
      { label: '营业执照号', value: enterpriseInfo.businessLicenseNo },
      { label: '营业执照地址', value: enterpriseInfo.licenseStreet },
      { label: '营业执照经营范围', value: enterpriseInfo.businessScope },
      {
        label: '营业执照有效时间',
        value: getDate(enterpriseInfo.licenseStart) + '~' + getDate(enterpriseInfo.licenseEnd),
      },
      {
        label: '营业执照照片',
        value: enterpriseInfo.licenseSrc,
        className: 'df',
        render: (
          <img
            src={(hasImgs && shopEnterpriseCertificates[0].image) || undefined}
            className="w-146 h-77"
          />
        ),
      },
    ];

    const corporateField = [
      { label: '法人姓名', value: enterpriseInfo.realname },
      { label: '法人身份证号', value: enterpriseInfo.identity },
      { label: '', value: '' },
      {
        label: '法人身份证信息',
        value: enterpriseInfo.enterpriseName,
        render: (
          <div>
            <img
              src={(hasImgs && shopEnterpriseCertificates[1].image) || undefined}
              className="w-146 h-77"
              alt=""
            />
            <img
              src={(hasImgs && shopEnterpriseCertificates[2].image) || undefined}
              className="w-146 h-77"
              alt=""
            />
          </div>
        ),
      },
    ];

    return (
      <div>
        <Card
          bordered={false}
          style={{ marginTop: 20 }}
          title={<CustomCard title="企业信息" />}
          extra={
            <a onClick={this.handleEdit}>
              <Icon type="edit" />
              修改
            </a>
          }
        >
          {renderDescriptionsItems(companyField)}
          <Divider />
          {renderDescriptionsItems(corporateField)}
        </Card>
      </div>
    );
  };

  renderAudit = () => {
    const { form } = this.props;
    const { isPhoneExamination } = this.state;
    return (
      <Card
        bordered={false}
        style={{ marginTop: 20 }}
        title={
          <CustomCard
            title="审核确认"
            isExplain
            explain="仅允许商家或平台其中一方做订单电话审核，勿双重审核打扰用户，请谨慎选择"
          />
        }
        extra={
          <div className="df">
            <Button onClick={this.handleCancel}>取消</Button>
            <Button type="primary" onClick={this.handleSubmit}>
              确定
            </Button>
          </div>
        }
      >
        <Form layout="inline">
          <FormItem label="是否需要平台进行电审">
            <Radio.Group onChange={this.handlePhoneExaminationChange} value={isPhoneExamination}>
              <Radio value={1}>是</Radio>
              <Radio value={0}>否</Radio>
            </Radio.Group>
          </FormItem>
        </Form>
      </Card>
    );
  };
  handleCance = e => {
    this.setState({
      visible: false,
    });
  };
  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  render() {
    const { loading } = this.state;
    return (
      <PageHeaderWrapper title="店铺信息">
        <Spin spinning={loading}>
          {this.renderShopInfo()}
          {this.renderCompanyInfo()}
          {this.renderAudit()}
        </Spin>
      </PageHeaderWrapper>
    );
  }
}
export default ShopInfo;
