import { message } from 'antd';
import * as servicesApi from '../../../services/api';

export default {
  spacename: 'info',
  state: {
    shopInfo: {},
    enterpriseInfo: {},
  },
  effects: {
    *selectShopInfo({callback}, { call, put }) {
      const res = yield call(servicesApi.selectShopInfo);
      if (res.code) {
        yield put({
          type: 'saveShopInfo',
          payload: res.data,
        });
      }
    },
    *selectEnterpriseInfo({callback}, { call, put }) {
      const res = yield call(servicesApi.selectEnterpriseInfo);
      if (res.code) {
        yield put({
          type: 'saveCompanyInfo',
          payload: res.data,
        });
      }
    },
    *updateShop({ payload }, { call }) {
      const res = yield call(servicesApi.updateShop, payload);
      if (res) {
        message.success('保存成功');
      }
    },
  },
  reducers: {
    saveShopInfo(state, { payload }) {
      return { ...state, shopInfo: payload };
    },
    saveCompanyInfo(state, { payload }) {
      return { ...state, enterpriseInfo: payload };
    },
    setShopInfo(state, { payload }) {
      const newInfo = { ...state.shopInfo };
      newInfo[payload.key] = payload.value;
      return { ...state, shopInfo: newInfo };
    },
  },
};
