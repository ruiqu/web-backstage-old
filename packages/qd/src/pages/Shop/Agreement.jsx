import React, { Component } from 'react';
import busShopService from '@/services/busShop';
import router from 'umi';
import { Button, message } from 'antd';
export default class Mods extends Component {
  state = {
    data: {},
  };
  componentDidMount() {
    this.selectBusShopInfo();
  }
  selectBusShopInfo = () => {
    busShopService.getShopContractData().then(res => {
      this.setState({
        data: res,
      });
    });
  };
  onGo = () => {
    busShopService.signShopContract().then(res => {
      if (res) {
        message.success('签署成功');
        this.props.history.goBack();
      }
    });
  };
  render() {
    const fontW = {
      fontWeight: 700,
    };
    const paddingRig = {
      textIndent: '2em',
      fontWeight: 700,
    };
    const paddingRi = {
      textIndent: '2em',
    };
    const { data } = this.state;
    return (
      <div>
        <div style={{ textAlign: 'center' }}>
          <h1>租赁商家入驻合作协议</h1>
        </div>
        <div style={{ textAlign: 'right' }}>
          <h3>签订地点：{data.signAddress}</h3>
        </div>
        <p>甲方：{data.name}</p>
        <p> 营业执照注册号：{data.businessLicenseNo}</p>
        <p>联系人：{data.contactName}</p>
        <p>联系电话：{data.contactTelephone}</p>
        <p>乙 方：{data.platformEnterpriseName}</p>
        <p>营业执照注册号：{data.platformLicenseNo}</p>
        <p>联系人：{data.platformContactName}</p>
        <p>联系电话：{data.platformContactTelephone}</p>
        <p>地 址：{data.platformAddress}</p>
        <p style={paddingRig}>
          甲、乙双方依据《中华人民共和国合同法》及相关法律法规的规定，本着平等互利原则达成如下长期合作协议内容，供双方共同遵照执行。
        </p>
        <h3>第一条 总则</h3>
        <p style={paddingRi}>
          甲、乙双方经友好协商，本着诚信、平等、自愿、互惠互利的原则，发挥自身资源优势，共同开展线上线下结合的信用租赁/分期/售卖业务，为推动双方共同事业的繁荣和发展并根据中华人民共和国有关法律、法规的规定签订本协议，由双方共同遵守。
        </p>
        <p style={paddingRi}>
          本协议中的甲方代指的是申请并成功入驻租物平台的第三方租赁商户；乙方代指的是
          {data.platformEnterpriseName}
          。对于甲方申请认证入驻乙方旗下的“租物平台”成为平台的第三方租赁商，即视为对本服务协议各条款的完全理解和接受：
        </p>
        <h3>二.合作内容</h3>
        <p style={paddingRi}>1、甲乙双方在资源共享和共赢的基础上进行合作。</p>
        <p style={paddingRi}>
          2、通过租物平台、支付宝租赁小程序以及相关线上线下渠道在全国范围内进行信用租赁/分期业务推广。
        </p>
        <p style={paddingRi}>
          3、合作期限自{data.start}起{data.end}
          止。服务期满后，若不存在本协议所约定的协议终止情形，任意一方未提出书面异议，则甲乙双方自动顺延一年。服务期满后，乙方仍需保留甲方所有在租订单信息给予甲方查核，甲方仍能登录后台操作并正常结算，直至甲方所有订单租期完结为止。
        </p>
        <h3>第三条 双方权利义务</h3>
        <p style={paddingRig}>1、甲方的权利与义务</p>
        <p style={paddingRi}>1.1按约定的比例获得租金收入，拥有出租设备所有权和残值处理权。</p>
        <p style={paddingRi}>
          1.2收到乙方发送的订单信息后，甲方负责订单发货，保证租户收到设备后能正常使用。
        </p>
        <p style={paddingRi}>
          1.3租赁期间，所租设备自然损坏均由甲方负责维修或者更换处理，甲方须按统一的服务标准对待租户。负责设备发货后售后问题，租赁期满后负责回收设备和残值处置等。
        </p>
        <p style={paddingRi}>1.4在租户咨询过程中，协助乙方工作人员回答租户相关技术问题。</p>
        <p style={paddingRi}>
          1.5甲方有权利对乙方的业务推广过程中进行检查和监督。如果乙方有不符合本协议规定的行为，甲方有权终止合作。
        </p>
        <p style={paddingRi}>
          1.6甲方负责向租户开具增值税普通发票/增值税专用发票（半年为周期，默认租金不含税，甲方须在方案注明是否含税）。
        </p>
        <p style={paddingRi}>1.7甲方向乙方提供设备租赁价格及配置，价格调整时须及时告知乙方。</p>
        <p style={paddingRi}>1.8甲方需保证供应量，库存缺货的需提前5天通知乙方。</p>
        <p style={paddingRi}>
          1.9甲方必须保证所出租的产品来源合法，不存在侵犯第三方权益的行为，包括不限于专利、品牌、图片、文字、影音等。
        </p>
        <p style={paddingRi}>2、乙方的权利与义务</p>
        <p style={paddingRi}>2.1乙方按约定比例获得租金分成。</p>
        <p style={paddingRi}>
          2.2根据甲方提供的产品资料设计租赁方案，上架产品，上线支付宝端，在全国范围展示产品，上线产品型号、租金、押金、租期等等租赁关键要素的建议权。
        </p>
        <p style={paddingRi}>2.3乙方负责产品线上线下推广，提供交易后台以及风控工具。</p>
        <p style={paddingRi}>
          2.4对于大额订单需要甲方业务员跟进的，乙方明确设备型号、数量、价格、配送时间、软件需求、客户信息后告知甲方，甲方安排业务人员协助跟进。
        </p>
        <p style={paddingRi}>
          2.5租户订单通过风控体系后，乙方按照要求的样式将租户订单信息及时发送给甲方或指定一方或提醒甲方及时处理订单。
        </p>
        <p style={paddingRi}>
          2.6若甲方未能向租户提供质量保证和租后服务保障，或甲方设备存在严重缺陷导致租户投诉，乙方有权终止合作。
        </p>
        <p style={paddingRi}>
          2.7在甲方产品供应量有保证条件下，乙方优先推广甲方的产品，线上线下把甲方作为核心供应商，产品展示给予优先位置。
        </p>
        <h3>第四条 合作佣金</h3>
        <p style={paddingRi}>
          1、甲方获得订单租金收入的（含续租），乙方获得订单租金收入的（含续租），佣金收入分配调整和特殊活动佣金另外以邮件形式约定。
        </p>
        <p style={paddingRi}>
          2、消费者通过乙方平台支付甲方设备的买断尾款，甲方应当按照其与消费者买断尾款价格的
          {data.shopRate}。
          额外向乙方支付该设备的买断费佣金；消费者通过乙方平台直接购买甲方商品，甲方应当按照其与消费者实际支付的金额的3.5%
          向乙方支付该设备的购买费佣金，该费用在甲方向乙方申请提现时，一并结算、支付。
        </p>
        <p style={paddingRi}>
          3、租户所支付的租金/购买费由乙方代收，租户按月支付租金则甲乙双方按月获得收益；租户支付每一笔租金后，每周五/周六结算当期租金至甲方在租物平台的账号。
        </p>
        <p style={paddingRi}>4、乙方获得的订单租金收入，可开具同等金额的服务费用发票给甲方。</p>
        <h3>第五条 风险声明</h3>
        <p style={paddingRi}>
          1、甲方理解并确认，乙方平台基于芝麻信用、第三方风控大数据以及自身风控策略输出的免押额度等仅供甲方参考，平台本着审慎的态度，平台不做任何明示或暗示的保证、担保，平台无需就甲方使用平台免押额度所做出的任何判断、决策、行为引发或导致的结果承担责任。甲方进驻平台应确保接受上述免责声明，且不得在任何情形下向平台主张责任。
        </p>
        <p style={paddingRi}>
          2、在发生订单逾期时，乙方将积极配合、协助甲方进行追缴，包括不限于提供租户信息、线下资源、法律咨询等。
        </p>
        <h3>第六条 甲方承诺</h3>
        <p style={paddingRi}>1、保证经营合法，租赁的产品拥有合法来源；</p>
        <p style={paddingRi}>2、诚信经营，不得存在欺诈行为，不得故意隐瞒租赁产品的缺陷等；</p>
        <p style={paddingRi}>3、不得威迫、诱导租户做出不真实的评价；</p>
        <p style={paddingRi}>4、不得恶性竞争，价格明显低于所在地区行业平均水平；</p>
        <p style={paddingRi}>5、不得使用暴力、污秽、侮辱等评价词语，不得带有攻击、诽谤他人；</p>
        <p style={paddingRi}>
          6、提供的入驻材料真实有效，并且在入驻材料变更时14日内向平台提供书面说明。
        </p>
        <p style={paddingRi}>
          7、用户订单出现商品不归还、分期账单逾期不支付时，甲方不得暴力催收，包括但不限于辱骂、威胁用户，也不得殴打用户等，甲方如因暴力催收等非法行为导致的任何后果，均由甲方自行承担，与乙方无关。
        </p>
        <h3>第七条、违约责任</h3>
        <p style={paddingRi}>
          1、因违约方行为致使守约方及/或其关联公司遭受损失（包括但不限于自身的直接经济损失、商誉损失及对外支付的赔偿金、律师费、诉讼费等间接经济损失），违约方应承担上述全部损失之赔偿责任。
        </p>
        <p style={paddingRi}>
          2、甲方发生违反本合同及相关交易规则的情形时，乙方有权下架甲方产品，甲方产品下架后，乙方仍需保留甲方所有在租订单信息给予甲方查核，甲方仍能登录后台操作并正常结算，直至甲方所有订单租期完结为止。
        </p>
        <p style={paddingRi}>
          3、以下情形为严重损害乙方的情形，乙方可视情节严重性终止和甲方的合作。
        </p>
        <p style={paddingRi}>
          3.1明示或暗示消费者通过其他方式转移其可以通过乙方平台在线达成的交易，或达成交易后通过其他渠道收取交易款的；提供质量不合格产品致消费者人身损害的。
        </p>
        <p style={paddingRi}>
          3.2若甲方未能向租户提供质量保证和租后服务保障，或甲方设备存在严重缺陷导致租户多次投诉，甲方多次整改无果；其他乙方认为甲方涉嫌违规或者违约的行为。
        </p>
        <h3>第八条 保密义务</h3>
        <p style={paddingRi}>
          1、甲乙双方均应充分保守本合同所涉及的对方及其第三方的商业秘密，包括但不限于经营信息、生产信息、技术信息、财务信息、合作方式、利益分享方案等。
        </p>
        <h3>第九条 法律适用、管辖和争议解决</h3>
        <p style={paddingRi}>
          1、本协议根据中华人民共和国的法律签订，其解释和执行也适用中华人民共和国的相关法律、法规和规章。
        </p>
        <p style={paddingRi}>
          2、因解释和执行本协议所发生的一切争议，以及与本协议相关的一切争议，由双方友好协商解决。未能协商一致解决的，提交被告所在地有管辖权法院解决。
        </p>
        <h3>第十条：通知及送达</h3>
        <p style={paddingRi}>
          1、商家确认其在商家注册时提供的联系地址和联系方式为真实的、有效的、完整的，且所提供的联系地址和联系方式作为平台通知商家的有效途径。如因联系地址或联系方式错误无法送达的，商家应承担由此产生的相关不利后果。平台联系地址和联系方式详见平台公示内容，或者联系平台客服获取。
        </p>
        <p style={paddingRi}>
          2、一方发给另一方的与本协议有关的通知应以书面形式送达，或以传真、电报、电传、电子邮件等发送。以传真、电报、电传或电子邮件发送的，发送当日为送达日，以邮资预付的挂号信件，特快专递寄送的，签收日为送达日。
        </p>
        <p style={paddingRi}>
          3、甲方为商家，其具体信息以商家注册时提交给***名字***平台后台审核身份信息为准。如遇信息变更，商家身份信息以商家最新申请修改并经平台审核通过留存于平台系统的信息为准。商家应确认其提交给***名字***平台的信息应为真实、有效、合法的，包括但不限于营业执照、法定代表人身份信息、联系地址、联系电话、银行账户、支付宝账户等。
        </p>
        <h3>第十一条 双方来往账户</h3>
        <p>甲方：{data.name}</p>
        <p>支付宝账户名：{data.zfbName}</p>
        <p>支付宝账号：{data.zfbCode}</p>
        <p>乙方：{data.platformEnterpriseName}</p>
        <p>开户行：{data.platformBank}</p>
        <p>银行账号：{data.platformBankCardNo}</p>
        <p>支付宝账号：{data.platformAlipay}</p>
        <h3>第十二条 其他</h3>
        <p style={paddingRi}>
          1、本协议为电子协议，在甲方、乙方确认后，通过杭州天谷信息科技有限公司（e签宝）电子签约平台完成电子签名和三方存档。
        </p>
        <p style={paddingRi}>
          2、丙方及本次电签服务提供方“杭州天谷信息科技有限公司（e签宝）”有权将甲乙双方的个人/机构身份信息、证件复印件或扫描件及签约合同信息相应传输至电子商务认证授权机构（CA，Certiflcate
          Authority）或公证处，由CA以出具电子签名认证证书为目的或以出具公正文件为目的的使用并保存，且前述信息至少保存至电子签名或相关电子数据（包括但不限于签约合同电子数据）产生后5年，并且该等信息不因终止使用平台的服务而停止保存。
        </p>
        <p style={paddingRi}>
          3、本协议未尽事宜，由双方协商，以书面形式签订补充协议。补充协议为本协议不可分割的一部分，与本协议具有同等法律效力。
        </p>
        <p style={paddingRi}>4、本合同经甲方（商家）在租物商家管理后台注册后生效。</p>

        <br />
        <br />
        <p>甲方：{data.name}</p>
        <p>签约时间：{data.createTime}</p>
        <br />
        <p>乙方：{data.platformEnterpriseName}</p>
        <p>签约时间：{data.createTime}</p>
        <div
          style={{
            margin: '30px 0',
            textAlign: 'center',
          }}
        >
          <Button
            style={{
              marginRight: 70,
              width: 120,
            }}
            onClick={this.props.history.goBack}
          >
            取消
          </Button>
          <Button
            type="primary"
            style={{
              width: 120,
            }}
            onClick={this.onGo}
          >
            确定
          </Button>
        </div>
      </div>
    );
  }
}
