import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import MyPageTable from '@/components/MyPageTable';
import { onTableData } from '@/utils/utils.js';
import CopyToClipboard from '@/components/CopyToClipboard';
import {
  Card,
  message,
  Tooltip,
  Button,
  Form,
  Input,
  Select,
  DatePicker,
  InputNumber,
  Row,
  Radio,
  Divider,
  Modal,
  Descriptions,
  Spin,
  Popconfirm,
  Table,
} from 'antd';
import { connect } from 'dva';
import QRCode from 'qrcode.react';
function getShopId() {
  const shopId = localStorage.getItem('shopId');
  return shopId;
}
import styles from './index.less';
@connect(({ order, loading }) => ({
  ...order,
  loading: loading.effects['order/queryOpeOrderByConditionList'],
}))
@Form.create()
export default class Notice extends Component {
  state = {
    current: 1,
    visible: false,
    orderId: null,
    datas: {},
    record: {},
    deliverOptions: [],

    List: [],
    total: 0,
    data: '',
    editing: false,
  };
  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = e => {
    console.log(e);
    this.onList(this.state.current, 10);
    this.setState({
      visible: false,
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  //table 页数
  onPage = e => {
    this.setState(
      {
        current: e.current,
      },
      () => {
        this.onList(e.current, 10, this.state.datas);
      },
    );
  };
  urls = () => {
    if (location && location.protocol.split(':')[0] === 'http') {
      return 'http%3A%2F%2Fzyjtest.zwzrent.net%2Fbusiness%2F%23%2F';
    } else {
      return 'https%3A%2F%2Fwww.zwzrent.cn%2Fbusiness%2F%23%2F';
    }
  };
  componentDidMount() {
    this.onList(1, 10);
    console.log(this.urls(), 'asdasds');
    let data = `https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxbdafef23cb8966ed&redirect_uri=${this.urls()}&response_type=code&scope=snsapi_userinfo&state=${getShopId()}&connect_redirect=1#wechat_redirect`;
    this.setState({
      data,
    });
  }
  handle = (id, status) => {
    const { dispatch } = this.props;
    if (status === 1) {
      dispatch({
        type: 'order/modifyUserWeixin',
        payload: {
          id,
          status: 0,
        },
        callback: res => {
          this.onList(this.state.current, 10);
        },
      });
    } else {
      dispatch({
        type: 'order/modifyUserWeixin',
        payload: {
          id,
          status: 1,
        },
        callback: res => {
          this.onList(this.state.current, 10);
        },
      });
    }
  };
  //   modifyUserWeixin
  //   hzsx/weixin/modifyUserWeixin
  handleDelete = id => {
    const { dispatch } = this.props;
    dispatch({
      type: 'order/deleteUserWeixin',
      payload: {
        id,
      },
      callback: res => {
        this.onList(this.state.current, 10);
      },
    });
  };
  onEditing = () => {
    let editing = !this.state.editing;
    this.setState({
      editing,
    });
  };
  save(e, id) {
    this.props.dispatch({
      type: 'order/modifyUserWeixin',
      payload: {
        id,
        remark: e.currentTarget.value,
      },
      callback: res => {
        message.success('编辑成功');
        this.onList(this.state.current, 10);
      },
    });
    // tempKeywords[index][type] = e.currentTarget.value;
    // //console.log(tempKeywords)
    // this.setState(
    //   {
    //     tempKeywords,
    //   },
    //   () => {
    //     this.props.form.validateFields(['tempKeywords'], { force: true });
    //   },
    // );
  }
  onList = (pageNumber, pageSize, data = {}) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'order/queryUserWeixinPage',
      payload: {
        pageSize,
        pageNumber,
        ...data,
      },
      callback: res => {
        console.log(res.data, 'res');
        this.setState({
          List: res.data.records,
          total: res.data.total,
        });
      },
    });
  };
  columns = [
    {
      title: '微信昵称',
      dataIndex: 'nickName',
      width: 160,
    },
    // onPressEnter
    {
      title: '备注',
      //   dataIndex: 'remark',
      width: 160,
      ellipsis: true,
      render: (text, record, index) => (
        <Input
          defaultValue={record.remark}
          onPressEnter={e => this.save(e, record.id)}
          onBlur={e => this.save(e, record.id)}
        />
      ),
    },
    {
      title: '状态',
      dataIndex: 'status',
      width: 160,
      ellipsis: true,
      render: status => {
        return <span>{status === 1 ? '推送正常' : '关闭推送'}</span>;
      },
    },
    {
      title: '绑定时间',
      dataIndex: 'createTime',
      width: 160,
      ellipsis: true,
    },
    {
      title: '操作',
      width: 140,
      //   fixed: 'right',
      align: 'center',
      render: (e, record) => {
        return (
          <div>
            <a
              className="primary-color"
              style={{ marginRight: 10 }}
              onClick={() => this.handle(e.id, e.status)}
            >
              {e.status === 1 ? '关闭推送' : '打开推送'}
            </a>
            <Popconfirm title="是否删除？" onConfirm={() => this.handleDelete(e.id, e.status)}>
              <a className="primary-color">删除</a>
            </Popconfirm>
          </div>
        );
      },
    },
  ];
  render() {
    const paginationProps = {
      current: this.state.current,
      pageSize: 10,
      total: this.state.total,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / 10)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };
    return (
      <PageHeaderWrapper>
        <div className={styles.inps}>
          <Button type="primary" style={{ margin: '20px 0' }} onClick={this.showModal}>
            新增
          </Button>
          <Table
            //   scroll={true}
            //   onPage={this.onPage}
            paginationProps={paginationProps}
            dataSource={onTableData(this.state.List)}
            columns={this.columns}
            rowKey="id"
          />
          <Modal
            title="新增"
            visible={this.state.visible}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
            width="700px"
          >
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              <div style={{ textAlign: 'center' }}>
                <p>1.请打开微信扫码关注***名字***公众号，已关注可跳过</p>
                <img
                  src="https://booleandata-zuwuzu.oss-cn-beijing.aliyuncs.com/520/qrcode_for_gh_989f7d57e900_1280.jpg"
                  alt="alt"
                  style={{
                    width: 200,
                  }}
                />
              </div>

              <div style={{ textAlign: 'center' }}>
                <p>2.微信扫描下方二维码，同意授权平台消息推送</p>
                <div>
                  <QRCode
                    value={this.state.data} //value参数为生成二维码的链接
                    size={180} //二维码的宽高尺寸
                    fgColor="#000000" //二维码的颜色
                    style={{ margin: '0 auto', marginTop: 26 }}
                  />
                </div>
              </div>
            </div>
          </Modal>
        </div>
      </PageHeaderWrapper>
    );
  }
}
