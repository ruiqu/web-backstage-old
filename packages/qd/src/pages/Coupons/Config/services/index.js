import request from "@/services/baseService";

export default {
  queryOpeIndexShopBannerPage:(data)=>{
    return request(`/hzsx/shopbanner/queryOpeIndexShopBannerPage`, data)
  },
  deleteOpeIndexShopBanner:(data)=>{
    return request(`/hzsx/shopbanner/deleteOpeIndexShopBanner`, data, 'get')
  },
  addOpeIndexShopBanner:(data)=>{
    return request(`/hzsx/shopbanner/addOpeIndexShopBanner`, data)
  },
  modifyOpeIndexShopBanner:(data)=>{
    return request(`/hzsx/shopbanner/modifyOpeIndexShopBanner`, data)
  },
}
