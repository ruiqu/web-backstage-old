import React, { Component } from 'react';
import { Card, Badge, Row, Col } from 'antd';
import CustomCard from '@/components/CustomCard';
import styles from './index.less';
import { router } from 'umi';
import { connect } from 'dva';
import MoneyModule from './components/moneyModule'
import TongzhiModule from './components/tongzhiModule'
import { getParam } from '@/utils/utils';

@connect(({ order }) => ({ ...order }))
export default class Home extends Component {
  state = {
    stateList: [
      {
        text: '待审批',
        num: 0,
        icon: '',
        icon2: '',
      },
      {
        text: '待发货',
        num: 0,
        icon: '',
        icon2: '',
      },
      {
        text: '待归还',
        num: 0,
        icon: '',
        icon2: '',
      },
      {
        text: '待结算',
        num: 0,
        icon: '',
        icon2: '',
      },
      {
        text: '已完成',
        num: 0,
        icon: '',
        icon2: '',
      },
    ],
    card2List: [
      {
        text: '逾期订单',
        img: './yuqi.png',
        url: '/Order/BeOverdue',
      },
      {
        text: '到期未归还订单',
        img: './daoqi.png',
        url: '/Order/OverdueNoRetrun',
      },
      {
        text: '买断订单',
        img: './maiduan.png',
        url: '/Order/BuyOut',
      },
      {
        text: '续租订单',
        img: './xuzu.png',
        url: '/Order/RentRenewal',
      },
      {
        text: '购买订单',
        img: './goumai.png',
        url: '/Order/Purchase',
      },
    ],
  };
  componentDidMount() {
    if (getParam('code')) {
      router.push(`/user`);
    } else {
      this.props.dispatch({
        type: 'order/businessOrderStatisticsUsing',
        callback: res => {
          this.setState({
            stateList: [
              {
                text: '待审批',
                num: res.waitingAuditOrderCount || 0,
                icon: '',
                icon2: '',
              },
              {
                text: '待发货',
                num: res.pendingOrderCount || 0,
                icon: '',
                icon2: '',
              },
              {
                text: '租用中',
                num: res.rentingOrderCount || 0,
                icon: '',
                icon2: '',
              },
              {
                text: '待结算',
                num: res.waitingSettlementOrderCount || 0,
                icon: '',
                icon2: '',
              },
              {
                text: '已完成',
                num: 0,
                icon: '',
                icon2: '',
              },
            ],
          });
        },
      });
    }
  }

  // 跳转
  iconRouter(url) {
    router.push(url);
  }

  router2(index, isRouter) {
    if (!isRouter) {
      return;
    }
    const statusList = ['11', '04', '06', '07', '09'];

    router.push({
      pathname: '/Order/HomePage',
      query: {
        status: statusList[index],
      },
    });
  }

  render() {
    const { stateList, card2List } = this.state;

    return (
      <div className="index">
        <Card bordered={false} style={{ marginTop: 20 }} title={<CustomCard title="订单流程" />}>
          <div className={styles.steps}>
            {stateList.map((item, index) => (
              <div key={`steps-${index}`} className={styles.steps_div}>
                <div>
                  {index === stateList.length - 1 ? (
                    <img
                      alt=""
                      onClick={() => this.router2(index, true)}
                      className={styles.icon_img}
                      src={`./step${index + 1}.png`}
                    />
                  ) : (
                    <Badge count={item.num} overflowCount={99}>
                      <img
                        alt=""
                        onClick={() => this.router2(index, item.num)}
                        className={styles.icon_img}
                        src={`./step${index + 1}${item.num === 0 ? '_2' : ''}.png`}
                      />
                    </Badge>
                  )}
                  <p className={styles.steps_title}>{item.text}</p>
                </div>
                {index === stateList.length - 1 ? '' : <div className={styles.line}></div>}
              </div>
            ))}
          </div>
        </Card>

        <Card bordered={false} style={{ marginTop: 20 }}>
          <div className={styles.tongzhiContainer}>
            <TongzhiModule />
            <MoneyModule />
          </div>
          {/* <Row gutter={20}>
            <Col span={18}>
              <TongzhiModule />
            </Col>
            <Col span={6}>
              <MoneyModule /> 
            </Col>
          </Row> */}
        </Card>

        <Card bordered={false} style={{ marginTop: 20, marginBottom: 40 }} title={<CustomCard title="常用功能" />}>
          <div className={styles.card2}>
            {card2List.map((item, index) => (
              <div
                key={`card2list-${index}`}
                className={styles.card2_div}
                onClick={() => this.iconRouter(item.url)}
              >
                <img alt="" className={styles.icon_img2} src={item.img} />
                <p className={styles.steps_title2}>{item.text}</p>
              </div>
            ))}
          </div>
        </Card>
      </div>
    );
  }
}
