import React, { Component, Fragment } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import {
  Card,
  Button,
  Form,
  Input,
  Modal,
  Descriptions,
  Steps,
  Divider,
  Drawer,
  Timeline,
  Spin,
  Cascader,
  message,
  Tabs,
  Select,
  Radio,
  InputNumber,
  Popconfirm,
  DatePicker,
  Table,
  BackTop,
} from 'antd';
import MyPageTable from '@/components/MyPageTable';
import AntTable from '@/components/AntTable';
import { onTableData, getParam, makeSub } from '@/utils/utils.js';
import orderService, {queryCreditReportByUid} from '@/services/order';

import yunxinCreditService from '@/services/yunxinCredit';
import CreditDetail from './creditDetail';
import CreditDetailOld from './creditDetailOld';
import CreditDetailThree from './creditDetailThree';
const { TextArea } = Input;
const FormItem = Form.Item;
const Option = Select.Option;
const { Step } = Steps;
const { TabPane } = Tabs;
const { confirm } = Modal;
import CustomCard from '@/components/CustomCard';
import {
  AuditReason,
  AuditStatus,
  confirmSettlementStatus,
  confirmSettlementType,
  orderStatusMap,
  BuyOutEnum,
  defaultPlaceHolder
} from '@/utils/enum';
import { optionsdata } from './data.js';
import OrderService from '@/services/order';
import { router } from 'umi';
import busShopService from '@/services/busShop';
import moment from 'moment';
import request from '../../../services/baseService';
import { renderFengxianfanqizha } from './unit/fengxianfanqizha'
import { fetchRiskErrHandler } from './fetchs/riskErrModule'
import { YouhuiMoneyWithTooltip, TLXProRiskReport,BaironProRiskReport, zhouqikoukuanReturnText, zhouqikoukuanReturnLabel } from 'zwzshared'
import {number} from "prop-types";

const columnsByStagesStute = {
  '1': '待支付',
  '2': '已支付',
  '3': '逾期已支付',
  '4': '逾期待支付',
  '5': '已取消',
  '6': '已结算',
  '7': '已退款,可用',
};
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};
//平台备注
const columns = [
  {
    title: '备注人姓名',
    dataIndex: 'userName',
  },
  {
    title: '备注时间',
    dataIndex: 'createTime',
  },
  {
    title: '备注内容',
    dataIndex: 'remark',
  },
];
//商家备注
const columnsBusiness = [
  {
    title: '备注人姓名',
    dataIndex: 'userName',
  },
  {
    title: '备注时间',
    dataIndex: 'createTime',
  },
  {
    title: '备注内容',
    dataIndex: 'remark',
  },
];
// 增值服务
const columnsAddService = [
  {
    title: '增值服务ID',
    dataIndex: 'shopAdditionalServicesId',
  },
  {
    title: '增值服务名称',
    dataIndex: 'shopAdditionalServicesName',
  },
  {
    title: '增值服务价格',
    dataIndex: 'price',
  },
];
//商品信息
const columnsInformation = [
  {
    title: '商品图片',
    dataIndex: 'imageUrl',
    render: imageUrl => {
      return (
        <img
          src={imageUrl}
          style={{
            width: 116,
            height: 62,
          }}
        />
      );
    },
  },
  {
    title: '商品名称',
    dataIndex: 'productName',
  },
  {
    title: '商品编号',
    dataIndex: 'productId',
    render: (text, record) => {
      return (
        <a className="primary-color" onClick={() => router.push(`/goods/list/detail/${record.id}`)}>
          {text}
        </a>
      );
    },
  },
  {
    title: '规格颜色',
    dataIndex: 'spec',
  },
  {
    title: '数量',
    dataIndex: 'num',
  },
  {
    title: '买断规则',
    dataIndex: 'buyOutSupportV1',
    render: buyOutSupport => {
      // buyOutSupport ? '可买断' : '不可买断'
      return <span>{BuyOutEnum[buyOutSupport] || '-'}</span>;
    },
  },
];

//账单信息
const columnsBill = [

  {
    title: '总租金',
    dataIndex: 'totalRent',
  },
  {
    title: '运费',
    dataIndex: 'freightPrice',
  },
  {
    title: '平台优惠',
    dataIndex: 'platformCouponReduction',
    render: (val, row) => {
      const youhuiList = row.userOrderCouponDtos || []
      const platformYouhuiList = youhuiList.filter(obj => {
        const str = obj.platform || ""
        return str.includes("OPE")
      }) // 筛选出只属于平台的优惠
      return <YouhuiMoneyWithTooltip money={val} youhuiList={platformYouhuiList} />
    }
  },
  {
    title: '店铺优惠',
    dataIndex: 'couponReduction',
    render: (val, row) => {
      const youhuiList = row.userOrderCouponDtos || []
      const shopYouhuiList = youhuiList.filter(obj => {
        const str = obj.platform || ""
        return str.includes("SHOP")
      }) // 筛选出只属于店铺的优惠
      return <YouhuiMoneyWithTooltip money={val} youhuiList={shopYouhuiList} />
    }
  },
];

//分期
const columnsByStages = [
  {
    title: '总期数',
    dataIndex: 'totalPeriods',
  },
  {
    title: '当前期数',
    dataIndex: 'currentPeriods',
  },
  {
    title: '租金',
    dataIndex: 'currentPeriodsRent',
  },
  {
    title: '状态',
    dataIndex: 'status',
    render: status => <span>{columnsByStagesStute[status]}</span>,
  },
  {
    title: '支付时间',
    dataIndex: 'repaymentDate',
  },
  {
    title: '账单到期时间',
    dataIndex: 'statementDate',
  },
];
//买断
const columnsBuyOutYes = [
  {
    title: '是否已买断',
    dataIndex: 'createTime',
    render: () => '是',
  },
  {
    title: '买断价格',
    dataIndex: 'buyOutAmount',
  },
];
const columnsBuyOutNo = [
  {
    title: '是否已买断',
    dataIndex: 'createTime',
    render: () => '否',
  },
  {
    title: '当前买断价格',
    dataIndex: 'currentBuyOutAmount',
  },
  {
    title: '到期买断价格',
    dataIndex: 'dueBuyOutAmount',
  },
];

//结算
const columnsSettlement = [
  {
    title: '宝贝状态',
    dataIndex: 'settlementType',
    render: (text, record) => {
      return confirmSettlementType[text];
    },
  },
  {
    title: '违约金',
    dataIndex: 'amount',
  },
  {
    title: '是否支付',
    dataIndex: 'settlementStatus',
    render: (text, record) => {
      return confirmSettlementStatus[text];
    },
  },
];

let collstus = {
  '01': '承诺还款',
  '02': '申请延期还款',
  '03': '拒绝还款',
  '04': '电话无人接听',
  '05': '电话拒接',
  '06': '电话关机',
  '07': '电话停机',
  '08': '客户失联',
};
//商家催收
const BusinessCollection = [
  {
    title: '记录人',
    dataIndex: 'userName',
  },
  {
    title: '记录时间',
    dataIndex: 'createTime',
  },
  {
    title: '结果',
    dataIndex: 'result',
    render: result => collstus[result],
  },
  {
    title: '小记',
    dataIndex: 'notes',
  },
];


// 账户情况
const columnsCreditAccountService = [
  {
    title: '',
    dataIndex: 'title',
  },
  {
    title: '非循环贷账户信息',
    dataIndex: 'fxhdzhxx',
  },
  {
    title: '循环额度分账户信息',
    dataIndex: 'xhedfzhxx',
  },
  {
    title: '循环贷账户信息',
    dataIndex: 'xhdzhxx',
  },
];




const paginationProps = {
  current: 1,
  pageSize: 1000,
  total: 1,
};

const KUAI_DI = 0; // 以快递形式发货
const ZI_TI = 1; // 以自提形式发货

@connect(({ order, loading, RentRenewalLoading }) => ({
  ...order,
  loading: loading.effects['order/queryOpeUserOrderDetail'],
  RentRenewalLoading: loading.effects['order/queryUserReletOrderDetail'],
}))
@Form.create()
export default class Details extends Component {
  state = {
    drawerVisible: false,
    drawerData: [],
    visible: false,
    visibles: false,
    visibles2: false,
    settlement: null,
    xcurrent: 1,
    scurrent: 1,
    titles: '',
    subLists: [],
    creditInfo: null, // 天狼星旗舰版风控报告
    baironRisk: null, // 百融风控报告
    baironRisk1: null, // 百融风控报告
    comprehensiveSuggest: '',
    deliverOptions: [],
    auditRecord: {},
    processDetail: [],

    orderVisible: '',
    orderId: '',
    radioValue: '',
    damageValue: '',

    queryOrderStagesDetail: {},

    activeTab: '1',
    cost: '',
    sign: false,

    HastenList: [],
    HastenTotal: 1,
    HastenCurrent: 1,

    opeHastenList: [],
    opeHastenTotal: 1,
    opeHastenCurrent: 1,
    visibles3: false,
    creditEmpty: false,
    showConfirmOrderReturnModal: false, // 是否显示确认归还的弹窗
    sendMethod: KUAI_DI, // 发货方式，默认快递
    userOrderCashesDtoBusiness: {},

    //征信弹窗
    showCreditDrawer:false,
    creditAccountInfo:null,

    showCreditBaseInfoDrawer:false,
    creditBaseInfo: null,
    //申请报送
    showConfirmApplyCreditModal: false,
    //放款报送
    showConfirmLoanCreditModal: false,
    //取消放款
    showConfirmCancelCreditModal: false,
    //结清
    showConfirmSettleCreditModal: false,
    //追偿报送
    showConfirmDunRepayCreditModal: false,
    //追偿结清
    showConfirmDunRepaySettleCreditModal: false,

    //押金信息
    depositData: {},
    //修改押金弹框
    depositModal: false,
    riskErrData: {}, // 风险反欺诈信息接口所返回的数据；可能会与原接口返回的内容存在不一致，经过了部分加工
    loadingRiskErrData: false, // 是否正在加载风险反欺诈信息数据中
    // loadingRiskReport: false, // 是否正在加载风控报告接口数据中
    showRiskReportCallBtn: false, // 是否显示查询风控报告按钮
    showRiskReportBaironBtn: false, // 是否显示查询风控报告按钮
    showRiskReportBaironBtn1: false, // 是否显示查询风控报告按钮
    orderDetailApiRes: {},

  };
  componentDidMount() {
    const { dispatch } = this.props;
    const orderId = getParam('id');
    this.setState({
      orderId,
    });
    this.onRentRenewalDetail();
    this.onPageBusiness({ current: 1 });
    this.onPage({ current: 1 });
    this.onHasten(1, 3, '02');
    this.onHasten(1, 3, '01');
    // config/getByCode?code=esign:fee
    busShopService.getByCode().then(res => {
      this.setState({
        cost: res,
      });
    });
    this.getExpressList();

    dispatch({
      type: 'order/queryOrderStagesDetail',
      payload: {
        orderId: getParam('id'),
      },
      callback: res => {
        if (res.responseType === 'SUCCESS') {
          this.setState({
            queryOrderStagesDetail: res.data,
          });
        }
      },
    });

    //押金信息
    OrderService.queryPayDepositLog({ orderId }).then(res => {
      if (res) {
        this.setState({
          depositData: res,
        });
      }
    });
  }

  fetchProcessDetail() {
    const orderId = getParam('id');
    orderService
      .queryOrderStatusTransfer({
        orderId,
      })
      .then(res => {
        this.setState({
          processDetail: res || [],
        });
      });
  }

  onRentRenewalDetail = () => {
    const { dispatch } = this.props;
    if (getParam('RentRenewal')) {
      dispatch({
        type: 'order/queryUserReletOrderDetail',
        payload: {
          orderId: getParam('id'),
        },
        callback: e => {
          const eData = e?.data?.userOrderCashesDto || {}
          this.setState({
            userOrderCashesDtoBusiness: eData,
            orderDetailApiRes: e.data,
          });
        },
      });
    } else {
      dispatch({
        type: 'order/queryOpeUserOrderDetail',
        payload: {
          orderId: getParam('id'),
        },
        callback: e => {
          this.setState({
            userOrderCashesDtoBusiness: e.data.userOrderCashesDto,
            orderDetailApiRes: e.data,
          });
        },
      });
    }
  };
  onQueryOrderRemark = (pageNumber, pageSize, source) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'order/queryOrderRemark',
      payload: {
        orderId: getParam('id'),
        pageNumber,
        pageSize,
        source,
      },
    });
  };
  //新建备注翻页
  onPage = (e = { current: 1 }) => {
    this.setState(
      {
        xcurrent: e.current,
      },
      () => {
        this.onQueryOrderRemark(e.current, 3, '01');
      },
    );
  };
  onHasten = (pageNumber, pageSize, source) => {
    orderService
      .queryOrderHasten({ pageNumber, pageSize, source, orderId: getParam('id') })
      .then(res => {
        if (source === '02') {
          this.setState({
            HastenList: res?.records,
            HastenTotal: res?.total,
          });
        } else {
          this.setState({
            opeHastenList: res?.records,
            opeHastenTotal: res?.total,
          });
        }
      });
  };
  onHastenBusiness = e => {
    this.setState(
      {
        HastenCurrent: e.current,
      },
      () => {
        this.onHasten(e.current, 3, '02');
      },
    );
  };
  onHastenOpe = e => {
    this.setState(
      {
        opeHastenCurrent: e.current,
      },
      () => {
        this.onHasten(e.current, 3, '01');
      },
    );
  };
  //商机备注翻页
  onPageBusiness = (e = { current: 1 }) => {
    this.setState(
      {
        scurrent: e.current,
      },
      () => {
        this.onQueryOrderRemark(e.current, 3, '02');
      },
    );
  };
  //物流信息
  onClose = () => {
    this.setState({
      drawerVisible: false,
    });
  };
  //查看物流
  onLogistics = (e, i) => {
    this.setState(
      {
        drawerVisible: true,
        drawerTitle: e,
      },
      () => {
        this.onQueryExpressInfo(i);
      },
    );
  };
  handleOk = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { dispatch, userOrderInfoDto } = this.props;
        if (this.state.titles === '备注') {
          if (getParam('RentRenewal')) {
            dispatch({
              type: 'order/orderRemark',
              payload: {
                orderId: getParam('id'),
                remark: values.beizhu,
                orderType: userOrderInfoDto.type,
              },
              callback: res => {
                this.onQueryOrderRemark(1, 3, '02');
                this.onRentRenewalDetail();
                this.setState({
                  visible: false,
                });
              },
            });
          } else {
            dispatch({
              type: 'order/orderRemark',
              payload: {
                orderId: getParam('id'),
                remark: values.beizhu,
                orderType: userOrderInfoDto.type,
              },
              callback: res => {
                this.onQueryOrderRemark(1, 3, '01');
                this.onRentRenewalDetail();
                this.setState({
                  visible: false,
                });
              },
            });
          }
        } else {
          dispatch({
            type: 'order/opeOrderAddressModify',
            payload: {
              realName: values.realName,
              street: values.street,
              telephone: values.telephone,
              province: values && values.city && values.city[0],
              city: values && values.city && values.city[1],
              area: values && values.city && values.city[2],
              orderId: getParam('id'),
            },
            callback: res => {
              this.onRentRenewalDetail();
              this.setState({
                visible: false,
              });
            },
          });
        }
      }
    });
  };
  onQueryExpressInfo = i => {
    const { dispatch } = this.props;
    console.log(i, '======>');
    dispatch({
      type: 'order/queryExpressInfo',
      payload: {
        expressNo: i.expressNo,
        receiverPhone: i.receiverPhone,
        shortName: i.shortName,
      },
    });
  };

  getExpressList = () => {
    OrderService.selectExpressList().then(res => {
      this.setState({
        deliverOptions: res || [],
      });
    });
  };

  getAuditRecord = () => {
    OrderService.queryOrderAuditRecord({
      orderId: this.state.orderId,
    }).then(res => {
      this.setState({
        auditRecord: res || [],
      });
    });
  };

  handleCancel = e => {
    this.setState({
      visible: false,
      orderVisible: false,
    });
  };
  handleCancels = e => {
    this.setState({
      visibles: false,
      visibles2: false,
      visibles3: false,
    });
  };
  showModal = e => {
    this.setState({
      visible: true,
      titles: e,
    });
  };

  settleDia = (type, orderId, userOrderCashId, record) => {
    const { userOrderCashesDto, userViolationRecords } = this.props;
    if (userViolationRecords && userViolationRecords[0]) {
      let radioValue = 'good';
      radioValue = userOrderCashesDto.damagePrice ? 'damage' : radioValue;
      radioValue = userOrderCashesDto.lostPrice ? 'lose' : radioValue;
      this.setState({
        checkValue: true,
        radioValue,
        damageValue: userOrderCashesDto.damagePrice || userOrderCashesDto.lostPrice,
      });
    }
    this.setState({
      orderId,
      userOrderCashId,
      record,
      orderVisible: 'settle',
    });
  };

  showOrderModal = (type, flag) => {
    if (type === 'deliver') {
      OrderService.checkOrderIsAuth({
        // ...fieldsValue,
        orderId: this.state.orderId,
      }).then(checkRes => {
        if (checkRes) {
          confirm({
            title: '发货失败',
            content: (
              <div>
                <p style={{ marginBottom: '0' }}>{checkRes}</p>
              </div>
            ),
          });
        } else {
          this.getExpressList();

          this.setState({
            orderVisible: type,
          });
        }
      });
    } else {
      if (type === 'express') {
        this.getExpressList();
      }
      if (type === 'settle') {
        this.settleDia();
      }
      if (type === 'audit') {
        this.setState({
          titles: flag ? '审批通过' : '审批拒绝',
        });
      }
      this.setState({
        orderVisible: type,
      });
    }
  };
  onXY = () => {
    location.href = this.props.contractUrl;
  };

  // 加载天狼星报告数据
  viewCredit = () => {
    const uoiObj = this.props.userOrderInfoDto || {}
    const uid = uoiObj.uid
    const phone = uoiObj.telephone
    const idCardNo = uoiObj.idCard
    const userName = uoiObj.realName
    this.props.dispatch({
      type: 'order/getCredit',
      payload: {
        uid,
        orderId: getParam('id'),
        phone,
        idCardNo,
        userName,
      },
      callback: res => {
        if (res.responseType === 'SUCCESS') {
          const resultStr = (res.data && res.data.siriusRiskReport) || "" // 风控报告结果字符串
          const str2 = resultStr.replaceAll('\"', '"')
          const finalResult = JSON.parse(str2)
          this.setState({ creditInfo: finalResult, showRiskReportCallBtn: false })
        }
      },
    });
  };
  // 加载百融报告数据
  getbairon = () => {
    const uoiObj = this.props.userOrderInfoDto || {}
    const uid = uoiObj.uid
    const phone = uoiObj.telephone
    const idCardNo = uoiObj.idCard
    const userName = uoiObj.realName
    this.props.dispatch({
      type: 'order/getbairon',
      payload: {
        uid,
        orderId: getParam('id'),
        phone,
        idCardNo,
        userName,
      },
      callback: res => {
        if (res.responseType === 'SUCCESS') {
          const resultStr = (res.data && res.data.resultJson) || "" // 风控报告结果字符串
          //console.log("获取百度风控报告内容",resultStr)
          const str2 = resultStr.replaceAll('\"', '"')
          const finalResult = JSON.parse(str2)
          console.log("获取百融风控报告内容",finalResult)
            this.setState({ baironRisk: finalResult, showRiskReportBaironBtn: false })
        }
      },
    });
  };
  // 加载百融报告数据
  getbairon1 = () => {
    const uoiObj = this.props.userOrderInfoDto || {}
    const uid = uoiObj.uid
    const phone = uoiObj.telephone
    const idCardNo = uoiObj.idCard
    const userName = uoiObj.realName
    this.props.dispatch({
      type: 'order/getbairon',
      payload: {
        uid,
        type:1,
        orderId: getParam('id'),
        phone,
        idCardNo,
        userName,
      },
      callback: res => {
        if (res.responseType === 'SUCCESS') {
          const resultStr = (res.data && res.data.resultJson) || "" // 风控报告结果字符串
          //console.log("获取百度风控报告内容",resultStr)
          const str2 = resultStr.replaceAll('\"', '"')
          const finalResult = JSON.parse(str2)
          console.log("获取百融风控报告内容",finalResult)
          this.setState({ baironRisk1: finalResult, showRiskReportBaironBtn1: false })
        }
      },
    });
  };

  tabChange = key => {
    const { userOrderInfoDto } = this.props;
    if (key === '3') {
      this.getAuditRecord();
    }
    if (key === '6') {
      this.fetchProcessDetail();
    }
    if (key === '2') {
      this.setState({ showRiskReportCallBtn: true })
    }
    if (key === '12') {
      this.setState({ showRiskReportBaironBtn: true })
    }
    if (key === '13') {
      this.setState({ showRiskReportBaironBtn1: true })
    }

    this.setState({
      activeTab: key,
    });
  };

  /**
   * 下载回执单的处理方法
   */
   downloadHuizhidanHandler = oId => {
    if (!oId) return // 订单ID必传
    if (this.loading) return // 点击中也不处理
    this.loading = true
    const url = `/hzsx/export/receiptConfirmationReceipt?orderId=${oId}`
    request(url, {}, 'get').then(() => {
      message.success('导出任务创建成功，请前往“数据管理-导出数据下载”完成下载。')
    }).finally(() => {
      this.loading = false
    })
  }
  /**
   * 下载合同的处理方法
   */
   downloadHetongHandler = orderId => {
    if (!orderId) return // 订单ID必传
    if (this.loading) return // 点击中也不处理
    this.loading = true
    const url = `/hzsx/business/order/downOrderContract`
    request(url, {orderId}, 'get').then((res) => {
      console.log("返回的数据是：",res)
      //message.success('导出任务创建成功，请前往“数据管理-导出数据下载”完成下载。')
      this.loading = false
    }).finally(() => {
      this.loading = false
    })
  }

  renderOrderInfoTab() {
    const {
      userOrderInfoDto = {},
      orderAdditionalServicesDto = {},
      orderAddressDto,
      opeRemarkDtoPage,
      businessRemarkDtoPage,
      productInfo,
      rentStart,
      rentDuration,
      unrentTime,
    } = this.props;

    const zengzhi = this.props.orderAdditionalServicesList || []; // 增值列表数据

    const orderId = getParam('id');
    return (
      <div>
        <Descriptions>
          <Descriptions.Item label="订单号" span={3}>
            {orderId}
          </Descriptions.Item>
          {this.props.contractUrl ? (
            <Descriptions.Item span={3} label="用户租赁协议">
              {/* <a href={this.props.contractUrl} target="_blank">《租赁协议》 </a> */}
              <a className="blackClickableA" onClick={() => this.downloadHetongHandler(orderId)}>协议下载</a>
            </Descriptions.Item>
          ) : null}
          <Descriptions.Item label="" span={3}>
            <a className="blackClickableA" onClick={() => this.downloadHuizhidanHandler(orderId)}>回执单下载</a>
          </Descriptions.Item>
        </Descriptions>

        {orderAddressDto ? (
          <>
            <Descriptions
              title={
                <>
                  <CustomCard title="收货人信息" />
                </>
              }
            >
              <Descriptions.Item label="收货人姓名">
                {orderAddressDto && orderAddressDto.realname}
              </Descriptions.Item>
              <Descriptions.Item label="收货人手机号">
                {orderAddressDto && orderAddressDto.telephone}
              </Descriptions.Item>
              <Descriptions.Item label="收货人地址">
                {orderAddressDto && orderAddressDto.provinceStr}
                {orderAddressDto && orderAddressDto.cityStr}
                {orderAddressDto && orderAddressDto.areaStr}
                {orderAddressDto && orderAddressDto.street}
              </Descriptions.Item>
              <Descriptions.Item label="用户备注">{userOrderInfoDto.remark}</Descriptions.Item>
            </Descriptions>
            <Divider />
          </>
        ) : null}

        <CustomCard title="商品信息" style={{ marginBottom: 20 }} />
        <AntTable
          columns={columnsInformation}
          dataSource={onTableData(productInfo)}
          paginationProps={paginationProps}
        />

        <Descriptions title={<CustomCard title="租用信息" />}>
          <Descriptions.Item label="租用天数">{rentDuration}</Descriptions.Item>
          <Descriptions.Item label="起租时间">{rentStart}</Descriptions.Item>
          <Descriptions.Item label="归还时间">{unrentTime}</Descriptions.Item>
        </Descriptions>
        <Divider />

        <CustomCard title="增值服务" style={{ marginBottom: 20 }} />
        <AntTable
          columns={columnsAddService}
          dataSource={zengzhi}
          // dataSource={onTableData([orderAdditionalServicesDto])}
          paginationProps={paginationProps}
        />

        <CustomCard title="商家备注" style={{ marginBottom: 20 }} />
        {/*<Button*/}
        {/*  type="primary"*/}
        {/*  style={{ margin: '20px 0' }}*/}
        {/*  onClick={() => this.showOrderModal('remark')}*/}
        {/*>*/}
        {/*  + 新建备注*/}
        {/*</Button>*/}
        <MyPageTable
          onPage={this.onPageBusiness}
          paginationProps={{
            current: this.state.scurrent,
            pageSize: 3,
            total: businessRemarkDtoPage.total,
          }}
          dataSource={onTableData(businessRemarkDtoPage.records)}
          columns={columnsBusiness}
        />
        <Divider />
        <CustomCard title="平台备注" />
        <MyPageTable
          onPage={this.onPage}
          paginationProps={{
            current: this.state.xcurrent,
            pageSize: 3,
            total: opeRemarkDtoPage.total,
          }}
          dataSource={onTableData(opeRemarkDtoPage.records)}
          columns={columns}
        />
      </div>
    );
  }

  // 渲染风控报告数据
  renderRiskTab() {
    const riskApiRes = this.state.creditInfo
    if (!riskApiRes) return null
    if (riskApiRes.resp_code === "SW0000") {
      console.log("riskapires",riskApiRes)
      const obj = riskApiRes.resp_data || {}
      return (
        <TLXProRiskReport riskReport={obj} />
      )
    } else {
      return <div style={{ marginTop: 15 }}>获取风险报告出现问题</div>
    }
  }

  // 渲染风控报告数据
  renderRiskbaironTab() {
    const riskApiRes = this.state.baironRisk
    if (!riskApiRes) return null
    if (riskApiRes.code === "00") {
      const obj = riskApiRes || {}
      return (
        <BaironProRiskReport riskReport={obj} />
      )
    } else {
      return <div style={{ marginTop: 15 }}>获取风险报告出现问题</div>
    }
  }
  // 渲染风控报告数据，三要素
  renderRiskbaironTab1() {
    const riskApiRes = this.state.baironRisk1
    if (!riskApiRes) return null
    if (riskApiRes.code === "00") {
      const obj = riskApiRes || {}
      return (
        <div className="booleandataTotalContainer">
        <div className="booleanDataWrap">
          <div>
          <Divider name="手机三要素" />
            <div className="riskDetection">
              <Descriptions column={4}>
                <Descriptions.Item label="运营商">
                {riskApiRes.TelCheck_s.operation =="1" ? '电信' :riskApiRes.TelCheck_s.operation =="2" ?'联通':riskApiRes.TelCheck_s.operation =="3" ?'移动':'其它'}
                </Descriptions.Item>

                <Descriptions.Item label="与身份信息是否一致">
                {riskApiRes.TelCheck_s.result =="1" ? '一致' :riskApiRes.TelCheck_s.result =="2" ?'不一致':'查无此号'}
                </Descriptions.Item>

                <Descriptions.Item label="在网时长">
                {riskApiRes.TelPeriod.data.value=="1"?'0-6':riskApiRes.TelPeriod.data.value=="2"?'6-12':riskApiRes.TelPeriod.data.value=="3"?'12-24':'24+'}
                个月
                </Descriptions.Item>
                <Descriptions.Item label="在网状态">
                {riskApiRes.TelStatus.data.value=="1"?'正常':riskApiRes.TelPeriod.data.value=="2"?'停机':riskApiRes.TelPeriod.data.value=="3"?'销号':'异常'}
                </Descriptions.Item>
              </Descriptions>
            </div>
          </div>
        </div>
      </div>
      )
    } else {
      return <div style={{ marginTop: 15 }}>获取风险报告出现问题</div>
    }
  }

  renderAuditTab() {
    const { auditRecord = {} } = this.state;

    return (
      <div>
        <Descriptions>
          <Descriptions.Item label="审批时间" span={3}>
            {auditRecord.approveTime}
          </Descriptions.Item>
          <Descriptions.Item label="审批人" span={3}>
            {auditRecord.approveUserName}
          </Descriptions.Item>
          <Descriptions.Item label="审批结果" span={3}>
            {AuditStatus[auditRecord.approveStatus]}
          </Descriptions.Item>
          {auditRecord.approveStatus === '02' && (
            <Descriptions.Item label="拒绝类型" span={3}>
              {AuditReason[auditRecord.refuseType]}
            </Descriptions.Item>
          )}
          <Descriptions.Item label="小记" span={3}>
            {auditRecord.remark}
          </Descriptions.Item>
        </Descriptions>
      </div>
    );
  }



  /**
   * 渲染云信上报信息
   * @returns {JSX.Element}
   */
  renderCreditApplyInfoTab(){

    let time = '2023年02月20日17:39:15';
    let money = 1000;

    let repayCreditListColumns = [
      {
        title: '期数',
        dataIndex: 'currentPeriods',
      },
      {
        title: '租金',
        dataIndex: 'currentPeriodsRent',
      },
      {
        title: '状态',
        dataIndex: 'status',
      },
      {
        title: '支付时间',
        dataIndex: 'repaymentDate',
      },
      {
        title: '账期到账时间',
        dataIndex: 'statementDate',
      },
      {
        title: '上报时间',
        dataIndex: 'reportTime',
      },
      {
        title: '上报类型',
        dataIndex: 'reportType',
      },
      {
        title: '上报状态',
        dataIndex: 'reportStatus',
      },
    ];



    let repayCreditListData = [
      {
        currentPeriods: 10,
        currentPeriodsRent: money,
        status: '已还款',
        repaymentDate: time,
        statementDate:time,
        reportTime: time,
        reportType: '什么意思?',
        reportStatus: '什么意思?'
      },
      {
        currentPeriods: 10,
        currentPeriodsRent: money,
        status: '已还款',
        repaymentDate: time,
        statementDate:time,
        reportTime: time,
        reportType: '什么意思?',
        reportStatus: '什么意思?'
      },
      {
        currentPeriods: 10,
        currentPeriodsRent: money,
        status: '已还款',
        repaymentDate: time,
        statementDate:time,
        reportTime: time,
        reportType: '什么意思?',
        reportStatus: '什么意思?'
      }
    ];

    let dunRepayCreditListColumns = [
      {
        title: '上报时间',
        dataIndex: 'reportTime',
      },
      {
        title: '上报类型',
        dataIndex: 'reportType',
      },
      {
        title: '追偿金额',
        dataIndex: 'amount',
      },
      {
        title: '当前追偿后剩余金额',
        dataIndex: 'surplusAmount',
      },
    ];

    let dunRepayCreditListData = [
      {
        reportTime: time,
        reportType: '什么意思?',
        amount: money,
        surplusAmount: money
      },
      {
        reportTime: time,
        reportType: '什么意思?',
        amount: money,
        surplusAmount: money
      },
      {
        reportTime: time,
        reportType: '什么意思?',
        amount: money,
        surplusAmount: money
      }
    ];


    return (
       <div>
         <Descriptions title={<CustomCard title="申请、放款报送" />}>
           <Descriptions.Item label="发货时间">{time}</Descriptions.Item>
           <Descriptions.Item label="申请报送时间">{time}</Descriptions.Item>
           <Descriptions.Item label="放款报送时间">{time}</Descriptions.Item>
         </Descriptions>
         <Divider />
         <Descriptions title={<CustomCard title="还款报送" />}>
         </Descriptions>
         <AntTable
           columns={repayCreditListColumns}
           dataSource={repayCreditListData}/>

         <Descriptions title={<CustomCard title="结清报送" />}>
           <Descriptions.Item label="上报时间">{time}</Descriptions.Item>
           <Descriptions.Item label="催收总金额">¥: {money}</Descriptions.Item>
         </Descriptions>
         <Divider />
         <Descriptions title={<CustomCard title="追偿,追偿结清报送" />}>
         </Descriptions>
         <AntTable
           columns={dunRepayCreditListColumns}
           dataSource={dunRepayCreditListData} />
         <Divider />

       </div>
     )
  }

//押金管理
renderDeposit() {
  const { depositData } = this.state;
  const yajinZhifuList = [] // 押金支付列表数据
  const { amount, creditAmount, paidAmount, waitPayAmount } = depositData || {}
  if (
    amount != undefined ||
    creditAmount != undefined ||
    paidAmount != undefined ||
    waitPayAmount != undefined
  ) { // 只要有任意一者存在有效值即进行展示
    const item = { key: 1, amount, creditAmount, paidAmount, waitPayAmount }
    yajinZhifuList.push(item)
  }
  let sm = {
    rank0: '提供信息不足，提供参数信息有误，或提供的支付宝账号不存在。',
    rank1: '表示用户拒付风险为低。',
    rank2: '表示用户拒付风险为中。',
    rank3: '表示用户拒付风险为高。',
  };
  let jj = {
    rank0: '等级0',
    rank1: '等级1',
    rank2: '等级2',
    rank3: '等级3',
  };
  const scoreTableCol11 = [
    {
      title: '风险评级',
      key: 'detail',
      render: text => <div>{this.props.nsfLevel}</div>,
    },
    {
      title: '风险描述',
      key: 'detail',
      render: text => <div>{jj[this.props.nsfLevel]}</div>,
    },
    {
      title: '评级备注',
      key: 'detail',
      render: text => <div>{sm[this.props.nsfLevel]}</div>,
    },
  ];

  let columns = [
    {
      title: '押金总额',
      dataIndex: 'amount',
    },
    {
      title: '已支付押金',
      dataIndex: 'paidAmount',
    },
    {
      title: '待支付押金',
      dataIndex: 'waitPayAmount',
    },
  ];
  let columnsLogs = [
    {
      title: '押金总额',
      dataIndex: 'afterAmount',
    },
    {
      title: '修改时间',
      dataIndex: 'createTime',
    },
    {
      title: '修改人',
      dataIndex: 'backstageUserName',
    },
  ];
 //芝麻额度冻结信息
 const columnsZm = [
  {
    title: '冻结额度',
    dataIndex: 'freezePrice',
  },
  {
    title: '信用减免',
    dataIndex: 'creditDeposit',
  },
  {
    title: '实际冻结',
    dataIndex: '', // todo
    render: (text, record) => {
      return makeSub(record.freezePrice, record.creditDeposit);
    },
  }

];
  return (
    <>
    <CustomCard title="芝麻额度冻结" style={{ marginBottom: 20 }} />
        <AntTable
          columns={columnsZm}
          dataSource={onTableData([this.state.userOrderCashesDtoBusiness])}
          paginationProps={paginationProps}
        />
        {
this.props.nsfLevel&& <>
<CustomCard title="先享后付评级" style={{ marginBottom: 20 }} />
        <Table columns={scoreTableCol11} dataSource={onTableData([{}])} pagination={false} /></>
        }

      <CustomCard title="押金支付" style={{ marginBottom: 20 }} />
      <AntTable
        columns={columns}
        dataSource={yajinZhifuList}
        paginationProps={paginationProps}
      />
      <CustomCard title="修改记录" style={{ marginBottom: 20 }} />
      <AntTable
        columns={columnsLogs}
        dataSource={onTableData(depositData && depositData.logs)}
        paginationProps={paginationProps}
      />
    </>
  );
}
  renderExpressTab() {
    const { receiptExpressInfo, giveBackExpressInfo } = this.props;
    return (
      <div>
        {giveBackExpressInfo ? (
          <>
            <Descriptions title={<CustomCard title="归还物流信息" />}>
              <Descriptions.Item label="发货物流公司">
                {giveBackExpressInfo.expressCompany}
              </Descriptions.Item>
              <Descriptions.Item label="发货物流单号">
                {giveBackExpressInfo.expressNo}
              </Descriptions.Item>
              <Descriptions.Item label="归还时间">
                {giveBackExpressInfo.deliveryTime}
              </Descriptions.Item>
            </Descriptions>
            <Button onClick={() => this.onLogistics('归还物流信息', giveBackExpressInfo)}>
              查看物流
            </Button>
            <Divider />
          </>
        ) : null}
        {receiptExpressInfo ? (
          <>
            <Descriptions title={<CustomCard title="发货物流信息" />}>
              <Descriptions.Item label="发货物流公司">
                {receiptExpressInfo.expressCompany}
              </Descriptions.Item>
              <Descriptions.Item label="发货物流单号">
                {receiptExpressInfo.expressNo}
              </Descriptions.Item>
              <Descriptions.Item label="发货时间">
                {receiptExpressInfo.deliveryTime}
              </Descriptions.Item>
            </Descriptions>
            <Button onClick={() => this.onLogistics('发货物流信息', receiptExpressInfo)}>
              查看物流
            </Button>
            <Divider />
          </>
        ) : null}
      </div>
    );
  }

  renderBillTab() {
    const {
      userOrderCashesDto = {},
      orderByStagesDtoList = [],
      orderBuyOutDto = {},
      settlementInfoDto = {},
    } = this.state.queryOrderStagesDetail;
    const productInfoList = this.props.productInfo || []
    const productInfoObj = productInfoList[0] || {} // 接口返回的productInfo对象
    return (
      <Card bordered={false}>
        {settlementInfoDto ? (
          <>
            <CustomCard title="结算信息" style={{ marginBottom: 20 }} />
            <AntTable
              columns={columnsSettlement}
              dataSource={onTableData([settlementInfoDto])}
              paginationProps={paginationProps}
            />
          </>
        ) : null}

        <CustomCard title="账单信息(金额单位：元)" style={{ marginBottom: 20 }} />

        <AntTable
          columns={columnsBill}
          dataSource={onTableData([this.state.userOrderCashesDtoBusiness])}
          paginationProps={paginationProps}
        />

        <CustomCard title="分期信息" style={{ marginBottom: 20 }} />
        <AntTable
          isLimitHeightTable={true}
          columns={columnsByStages}
          dataSource={onTableData(orderByStagesDtoList)}
          paginationProps={paginationProps}
        />
        {orderBuyOutDto.orderId ? (
          <>
            <CustomCard title="买断信息" style={{ marginBottom: 20 }} />
            <AntTable
              columns={orderBuyOutDto.payFlag ? columnsBuyOutYes : columnsBuyOutNo}
              dataSource={onTableData([orderBuyOutDto])}
              paginationProps={paginationProps}
            />
          </>
        ) : null}

        <div style={{ marginBottom: "20px" }}>
          <Descriptions
            title={
              <>
                <CustomCard title="交易快照(金额单位：元)" />
              </>
            }
          >
            <Descriptions.Item label="销售价">{ productInfoObj.salePrice || defaultPlaceHolder }</Descriptions.Item>
          </Descriptions>
        </div>
      </Card>
    );
  }

  renderProcessTab() {
    const { processDetail = [] } = this.state;
    const { userOrderInfoDto = {} } = this.props;
    return (
      <Card
        className="remove-card-bottom-border"
        title={<CustomCard title="订单进度" />}
        bordered={false}
      >
        <Steps
          direction="vertical"
          progressDot={document.body.clientWidth >= 1025}
          current={processDetail.length}
        >
          {processDetail.map(item => {
            const desc = (
              <div style={{ width: 300 }}>
                <div>{item.operatorName}</div>
                <div>{item.createTime}</div>
              </div>
            );
            return <Step title={item.operate} key={item.operate} description={desc} />;
          })}
        </Steps>
      </Card>
    );
  }

  renderContentTabCard() {
    const { userOrderInfoDto } = this.props;
    const { activeTab } = this.state;
    const status = userOrderInfoDto.status;

    return (
      <Card bordered={false} style={{ marginTop: 20 }}>
        <Tabs activeKey={activeTab} onChange={this.tabChange} animated={false}>
          <TabPane tab="订单信息" key="1">
            {this.renderOrderInfoTab()}
          </TabPane>
          {!['02', '01'].includes(status) ? (
            <TabPane tab="风控报告" key="2">
              <>
                {
                  this.state.showRiskReportCallBtn && (
                    <Button onClick={this.viewCredit} type="primary">点击查询</Button>
                  )
                }
                { this.renderRiskTab() }
              </>
            </TabPane>
          ) : null}

    {/*   {!['02', '01'].includes(status) ? (
            <TabPane tab="百融三要素(测试)" key="13">
              <>
                {
                  this.state.showRiskReportBaironBtn1 && (
                    <Button onClick={this.getbairon1} type="primary">点击查询</Button>
                  )
                }
                { this.renderRiskbaironTab1() }
              </>
            </TabPane>
          ) : null} */}

          <TabPane tab="云信-上报征信" key="213">
            <>
              {
                this.state.showRiskReportBaironBtn1 && (
                  <Button onClick={this.getbairon1} type="primary">点击查询</Button>
                )
              }
              { this.renderCreditApplyInfoTab() }
            </>
          </TabPane>

          {/* {!['02', '01'].includes(status) ? (
            <TabPane tab="百融报告" key="12">
              <>
                {this.renderRiskbaironTab1()}
              </>
            </TabPane>
          ) : null} */}


          {!['01', '02', '11'].includes(status) ? (
            <TabPane tab="审批结论" key="3">
              {this.renderAuditTab()}
            </TabPane>
          ) : null}
          {!['01', '02', '04', '11'].includes(status) ? (
            <TabPane tab="物流信息" key="4">
              {this.renderExpressTab()}
            </TabPane>
          ) : null}
          <TabPane tab="押金管理" key="11">
            {this.renderDeposit()}
          </TabPane>
          <TabPane tab="账单信息" key="5">
            {this.renderBillTab()}
          </TabPane>
          {getParam('settlement') ? (
            <TabPane tab="催收记录" key="10">
              {this.renderCollectionRecord()}
            </TabPane>
          ) : null}

          <TabPane tab="流程进度" key="6">
            {this.renderProcessTab()}
          </TabPane>
        </Tabs>
      </Card>
    );
  }
  renderCollectionRecord() {
    const { orderVisible } = this.state;

    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <CustomCard title="商家催收" style={{ marginBottom: 20 }} />
        <MyPageTable
          onPage={this.onHastenBusiness}
          columns={BusinessCollection}
          dataSource={onTableData(this.state.HastenList)}
          paginationProps={{
            current: this.state.HastenCurrent,
            pageSize: 3,
            total: this.state.HastenTotal,
          }}
        />
        <CustomCard title="平台催收" style={{ marginBottom: 20 }} />
        <MyPageTable
          onPage={this.onHastenOpe}
          columns={BusinessCollection}
          dataSource={onTableData(this.state.opeHastenList)}
          paginationProps={{
            current: this.state.opeHastenCurrent,
            pageSize: 3,
            total: this.state.opeHastenTotal,
          }}
        />
      </div>
    );
  }
  renderCollectionRecordModal() {
    const { orderVisible } = this.state;
    const { getFieldDecorator } = this.props.form;
    const orderId = getParam('id');

    const handleOk = e => {
      e.preventDefault();
      this.props.form.validateFields(['jg', 'xj'], (err, values) => {
        if (!err) {
          orderService.orderHasten({ orderId, notes: values.xj, result: values.jg }).then(res => {
            this.onHasten(1, 3, '02');
            this.handleCancel();
          });
        }
      });
    };

    return (
      <Modal
        title="记录催收"
        visible={orderVisible === 'remarks'}
        onOk={handleOk}
        onCancel={this.handleCancel}
        destroyOnClose
      >
        <Form>
          <Form.Item label="结果" {...formItemLayout}>
            {getFieldDecorator('jg', {
              rules: [{ required: true, message: '请选择结果' }],
            })(
              <Select style={{ width: '100%' }} placeholder="请选择结果">
                <Option value="01">承诺还款</Option>
                <Option value="02">申请延期还款</Option>
                <Option value="03">拒绝还款</Option>
                <Option value="04">电话无人接听</Option>
                <Option value="05">电话拒接</Option>
                <Option value="06">电话关机</Option>
                <Option value="07">电话停机</Option>
                <Option value="08">客户失联</Option>
              </Select>,
            )}
          </Form.Item>
          <Form.Item label="小记" {...formItemLayout}>
            {getFieldDecorator('xj', {
              rules: [{ required: true, message: '请输入小记' }],
            })(<TextArea placeholder="请输入" />)}
          </Form.Item>
        </Form>
      </Modal>
    );
  }
  renderRemarkModal() {
    const { orderVisible } = this.state;
    const { getFieldDecorator } = this.props.form;
    const orderId = getParam('id');
    const handleOk = e => {
      e.preventDefault();
      this.props.form.validateFields(['beizhu'], (err, values) => {
        if (!err) {
          const { dispatch } = this.props;
          dispatch({
            type: 'order/orderRemark',
            payload: {
              orderId,
              remark: values.beizhu,
              orderType: '01',
            },
            callback: res => {
              this.onPageBusiness();
              this.handleCancel();
              this.props.form.resetFields();
            },
          });
        }
      });
    };

    return (
      <div>
        <Modal
          title="备注"
          visible={orderVisible === 'remark'}
          onOk={handleOk}
          onCancel={this.handleCancel}
        >
          <Form>
            <Form.Item label="备注内容" {...formItemLayout}>
              {getFieldDecorator('beizhu', {
                rules: [{ required: true, message: '请输入备注' }],
              })(<TextArea placeholder="请输入" />)}
            </Form.Item>
          </Form>
        </Modal>
      </div>
    );
  }

  /**
   * 改变发货方式的时候触发
   * @param {*} event
   */
  changeSendMethod = event => {
    this.setState({ sendMethod: event.target.value });
  };

  renderDeliverModal() {
    const { form } = this.props;
    const { orderVisible, deliverOptions } = this.state;
    const { getFieldDecorator } = form;
    const orderId = getParam('id');

    const handleOk = e => {
      e.preventDefault();

      this.props.form.validateFields(['expressId', 'expressNo',], (err, fieldsValue) => {
        if (err) return;
        const resetCb = () => {
          this.onRentRenewalDetail();
          this.props.form.resetFields();
          this.setState({ sendMethod: KUAI_DI }); // 将发货方式重置为快递
          this.handleCancel();
        };
        if (this.state.sendMethod === ZI_TI) {
          // 自提走不一样的流程
          OrderService.expressZitiHandler(orderId).then(res => {
            resetCb();
          });
          return;
        }
        OrderService.orderDelivery({
          ...fieldsValue,
          orderId,
        }).then(res => {
          resetCb();
        });
      });
    };

    return (
      <Modal
        destroyOnClose
        title={orderVisible === 'express' ? '修改物流信息' : '快速发货'}
        visible={orderVisible === 'deliver' || orderVisible === 'express'}
        onOk={handleOk}
        width="450px"
        onCancel={this.handleCancel}
      >
        <Form labelCol={{ span: 5 }} labelAlign="left" wrapperCol={{ span: 19 }}>
          {this.props.contractUrl ? null : (
            <div style={{ color: '#E83F40', marginBottom: 10 }}>
              注意：该订单尚未签约存证，发货后无法签约，请确认！
            </div>
          )}
          {/** 修改物流信息的时候不显示发货方式 */}
          {orderVisible === 'deliver' && (
            <FormItem label="发货方式">
              <Radio.Group value={this.state.sendMethod} onChange={this.changeSendMethod}>
                <Radio value={KUAI_DI}>快递</Radio>
                <Radio value={ZI_TI}>自提</Radio>
              </Radio.Group>
            </FormItem>
          )}

          {this.state.sendMethod === 0 && (
            <>
              <FormItem>
                {form.getFieldDecorator('expressId', {
                  rules: [{ required: true, message: '请选择物流公司' }],
                })(
                  <Select style={{ width: '100%' }} placeholder="请选择物流公司">
                    {deliverOptions.map(option => {
                      return (
                        <Option key={option.id} value={option.id}>
                          {option.name}
                        </Option>
                      );
                    })}
                  </Select>,
                )}
              </FormItem>
              <FormItem>
                {form.getFieldDecorator('expressNo', {
                  rules: [{ required: true, message: '请输入物流单号' }],
                })(<Input placeholder="请输入物流单号" />)}
              </FormItem>

            </>
          )}
        </Form>
      </Modal>
    );
  }

  renderCloseModal() {
    const { form } = this.props;
    const { orderVisible, orderId } = this.state;
    const handleOk = e => {
      this.props.form.validateFields(['closeReason'], (err, fieldsValue) => {
        if (err) return;
        OrderService.businessClosePayedOrder({
          ...fieldsValue,
          closeType: '06',
          orderId,
        }).then(res => {
          this.onRentRenewalDetail();
          this.props.form.resetFields();
          this.handleCancel();
        });
      });
    };

    return (
      <Modal
        destroyOnClose
        title={'关闭订单'}
        visible={orderVisible === 'close'}
        onOk={handleOk}
        width="450px"
        onCancel={this.handleCancel}
      >
        <Form>
          {/*<FormItem>*/}
          {/*  {form.getFieldDecorator(*/}
          {/*    'closeType',*/}
          {/*    {*/}
          {/*      rules: [*/}
          {/*        {required: true, message: '请选择关单类型'}*/}
          {/*      ]*/}
          {/*    },*/}
          {/*  )(*/}
          {/*    <Radio.Group>*/}
          {/*      <Radio value={'06'}>客户要求关单</Radio>*/}
          {/*      <Radio value={'07'}>商家风控关单</Radio>*/}
          {/*    </Radio.Group>,*/}
          {/*  )}*/}
          {/*</FormItem>*/}
          <FormItem label="小记">
            {form.getFieldDecorator('closeReason', {})(<Input placeholder="请输入关单原因" />)}
          </FormItem>
        </Form>
      </Modal>
    );
  }

  renderSettleModal() {
    const { form, userOrderCashesDto = {} } = this.props;
    const { orderId, orderVisible, radioValue, damageValue } = this.state;

    const onRadioChange = e => {
      this.setState({
        radioValue: e.target.value,
      });
    };

    const handleDamageValue = val => {
      this.setState({
        damageValue: val,
      });
    };

    const handleOk = e => {
      this.props.form.validateFields(['penaltyAmount', 'cancelReason'], (err, fieldsValue) => {
        if (err) return;
        const payload = {
          lossAmount: 0,
          damageAmount: 0,
          penaltyAmount: 0,
          settlementType: radioValue,
          ...fieldsValue,
          orderId,
        };
        if (radioValue === '03') {
          payload.lossAmount = damageValue;
        } else if (radioValue === '02') {
          payload.damageAmount = damageValue;
        }
        OrderService.merchantsIssuedStatements(payload).then(res => {
          this.onRentRenewalDetail();
          this.props.form.resetFields();
          this.handleCancel();
        });
      });
    };

    return (
      <Modal
        title="是否确认结算？"
        zIndex={2000}
        visible={orderVisible === 'settle'}
        onOk={handleOk}
        onCancel={this.handleCancel}
      >
        <div>
          <div>
            <span style={{ marginRight: '20px' }}>宝贝状态</span>
            <Radio.Group onChange={onRadioChange} value={radioValue}>
              <Radio value="01">完好</Radio>
              <Radio value="02">损坏</Radio>
              <Radio value="03">丢失</Radio>
              <Radio value="04">其他</Radio>
            </Radio.Group>
            {radioValue === '02' && (
              <div style={{ display: 'inline-block', marginTop: '20px' }}>
                <span>损坏赔偿金：</span>
                <InputNumber
                  min={0}
                  defaultValue={0}
                  value={damageValue}
                  onChange={handleDamageValue}
                />
                <span>元</span>
              </div>
            )}
            {radioValue === '03' && (
              <div style={{ display: 'inline-block', marginTop: '20px' }}>
                <span>丢失赔偿金：</span>
                <InputNumber
                  min={0}
                  defaultValue={0}
                  value={damageValue}
                  onChange={handleDamageValue}
                />
                <span>元</span>
              </div>
            )}
            {radioValue === '04' && (
              <div style={{ marginTop: '20px' }}>
                <Form.Item labelCol={{ span: 4 }} label="违约金：">
                  {form.getFieldDecorator('penaltyAmount', {
                    initialValue: userOrderCashesDto.penaltyAmount,
                  })(<InputNumber min={0} />)}
                  <span> 元</span>
                </Form.Item>
                <Form.Item labelCol={{ span: 4 }} label="违约原因：">
                  {form.getFieldDecorator('cancelReason', {
                    initialValue: userOrderCashesDto.cancelReason,
                  })(<Input style={{ width: '60%' }} placeholder="请输入违约原因" />)}
                </Form.Item>
              </div>
            )}
          </div>
        </div>
      </Modal>
    );
  }

  renderAddressModal() {
    const {
      userOrderInfoDto = {},
      orderAddressDto,
      dispatch,
      productInfo,
      userOrderCashesDto,
      orderBuyOutDto,
      wlList,
    } = this.props;
    const { orderVisible, orderId } = this.state;
    const { getFieldDecorator } = this.props.form;
    const handleOk = e => {
      this.props.form.validateFields(['realName', 'street', 'city', 'telephone'], (err, values) => {
        if (err) return;
        dispatch({
          type: 'order/opeOrderAddressModify',
          payload: {
            realName: values.realName,
            street: values.street,
            telephone: values.telephone,
            province: values && values.city && values.city[0],
            city: values && values.city && values.city[1],
            area: values && values.city && values.city[2],
            orderId,
          },
          callback: res => {
            this.onRentRenewalDetail();
            this.props.form.resetFields();
            this.handleCancel();
          },
        });
        // OrderService.businessClosePayedOrder({
        //   ...fieldsValue,
        //   orderId,
        // }).then(res => {
        //   this.onRentRenewalDetail()
        //   this.props.form.resetFields();
        //   this.handleCancel();
        // });
      });
    };

    return (
      <Modal
        title="修改收货信息"
        visible={orderVisible === 'address'}
        onOk={handleOk}
        onCancel={this.handleCancel}
      >
        <Form>
          <Form.Item label="所在城市" {...formItemLayout}>
            {getFieldDecorator('city', {
              rules: [
                {
                  required: true,
                  message: '请输入备注',
                },
              ],
              initialValue: [
                orderAddressDto && orderAddressDto.province && orderAddressDto.province.toString(),
                orderAddressDto && orderAddressDto.city && orderAddressDto.city.toString(),
                orderAddressDto && orderAddressDto.area && orderAddressDto.area.toString(),
              ],
            })(
              <Cascader
                options={optionsdata}
                fieldNames={{ label: 'name', value: 'value', children: 'subList' }}
              />,
            )}
          </Form.Item>
          <Form.Item label="收货人姓名" {...formItemLayout}>
            {getFieldDecorator('realName', {
              rules: [{ required: true, message: '请输入备注' }],
              initialValue: orderAddressDto && orderAddressDto.realname,
            })(<Input placeholder="请输入" />)}
          </Form.Item>
          <Form.Item label="收货人手机号" {...formItemLayout}>
            {getFieldDecorator('telephone', {
              rules: [{ required: true, message: '请输入备注' }],
              initialValue: orderAddressDto && orderAddressDto.telephone,
            })(<Input placeholder="请输入" />)}
          </Form.Item>
          <Form.Item label="详细地址" {...formItemLayout}>
            {getFieldDecorator('street', {
              rules: [{ required: true, message: '请输入备注' }],
              initialValue: orderAddressDto && orderAddressDto.street,
            })(<TextArea placeholder="请输入" />)}
          </Form.Item>
        </Form>
      </Modal>
    );
  }

  renderAuditModal() {
    const { form } = this.props;
    const { orderVisible, orderId, titles } = this.state;
    const handleOk = e => {
      this.props.form.validateFields(['refuseType', 'auditRemark'], (err, fieldsValue) => {
        if (err) return;
        OrderService.telephoneAuditOrder({
          ...fieldsValue,
          orderId,
          remark: fieldsValue.auditRemark,
          orderAuditStatus: titles === '审批通过' ? '01' : '02',
        }).then(res => {
          this.onRentRenewalDetail();
          this.props.form.resetFields();
          this.handleCancel();
        });
      });
    };

    return (
      <Modal
        destroyOnClose
        title={titles}
        visible={orderVisible === 'audit'}
        onOk={handleOk}
        width="450px"
        onCancel={this.handleCancel}
      >
        <Form>
          {titles === '审批拒绝' ? (
            <FormItem label="拒绝类型" {...formItemLayout}>
              {form.getFieldDecorator('refuseType', {
                rules: [{ required: true, message: '请选择拒绝类型' }],
              })(
                <Select style={{ width: '100%' }} placeholder="请选择">
                  {Object.keys(AuditReason).map(option => {
                    return (
                      <Option key={option} value={option}>
                        {AuditReason[option]}
                      </Option>
                    );
                  })}
                </Select>,
              )}
            </FormItem>
          ) : null}
          <FormItem label="小记" {...formItemLayout}>
            {form.getFieldDecorator('auditRemark', {
              rules: [{ required: true, message: '请输入小记' }],
            })(<Input.TextArea placeholder="请输入小记" />)}
          </FormItem>
        </Form>
      </Modal>
    );
  }
  onConfirm = () => {
    const { orderId } = this.state;
    if (this.loading) return // 点击中也不处理
    this.loading = true
    busShopService.generateOrderContract({ orderId }).then(res => {
      if (res) {
        message.success('签约成功');
        this.onRentRenewalDetail();
        this.loading = false
      }
    });
  };

  
  /**
   * 归还的处理
   */
  returnBackHandler = () => {
    this.setState({ showConfirmOrderReturnModal: true });
  };

  /**
   * 取消归还的回调方法
   */
  cancelReturnHandler = () => {
    this.setState({ showConfirmOrderReturnModal: false });
  };

  /**
   * 渲染归还的弹窗
   */
  renderConfirmOrderReturnModal = () => {
    const { form } = this.props;

    // 确认归还的回调
    const confirmReturnHandler = e => {
      e.preventDefault();
      this.props.form.validateFields(['returnTime', 'expressId', 'expressNo'], (err, values) => {
        if (!err) {
          if (this.confirmReturning) return;
          this.confirmReturning = true;
          const data = {
            expressId: values.expressId,
            expressNo: values.expressNo,
            orderId: this.state.orderId,
            returnTime: moment(values.returnTime).format('YYYY-MM-DD hh:mm:ss'),
          };
          orderService
            .confirmReturnOrder(data)
            .then(res => {
              this.onRentRenewalDetail(); // 加载商品详情数据
              this.cancelReturnHandler();
            })
            .finally(() => {
              this.confirmReturning = false;
            });
        }
      });
    };

    return (
      <Modal
        title="确认归还"
        visible={this.state.showConfirmOrderReturnModal}
        onOk={confirmReturnHandler}
        onCancel={this.cancelReturnHandler}
        destroyOnClose
      >
        <Form>
          <Form.Item label="归还时间" {...formItemLayout}>
            {form.getFieldDecorator('returnTime', {
              rules: [{ required: true, message: '请选择归还时间' }],
            })(
              <DatePicker
                style={{ width: '100%' }}
                format="YYYY-MM-DD HH:mm:ss"
                placeholder="请选择归还时间"
              />,
            )}
          </Form.Item>
          <Form.Item label="物流公司" {...formItemLayout}>
            {form.getFieldDecorator('expressId', {
              rules: [{ required: true, message: '请选择物流公司' }],
            })(
              <Select style={{ width: '100%' }} placeholder="请选择物流公司">
                {this.state.deliverOptions.map(option => {
                  return (
                    <Option key={option.id} value={option.id}>
                      {option.name}
                    </Option>
                  );
                })}
              </Select>,
            )}
          </Form.Item>
          <Form.Item label="物流单号" {...formItemLayout}>
            {form.getFieldDecorator('expressNo', {
              rules: [{ required: true, message: '请输入物流单号' }],
            })(<Input placeholder="请输入" />)}
          </Form.Item>
        </Form>
      </Modal>
    );
  };

  /**
   * 征信drawer
   */
  returnCreditDrawerHandler = () => {
    const uoiObj = this.props.userOrderInfoDto || {}
    const uid = uoiObj.uid;
    const phone = uoiObj.telephone
    const idCardNo = uoiObj.idCard
    const idCardFrontUrl = uoiObj.idCardFrontUrl
    const idCardBackUrl = uoiObj.idCardBackUrl;
    const userName = uoiObj.realName
    // const orderId = getParam('id');
    // console.log('看下参数信息',uid,phone,idCardNo,userName)
    yunxinCreditService.queryCreditReportByUid({
        uid,
        orderId: getParam('id'),
        phone,
        idCardNo,
        userName,
        idCardFrontUrl,
        idCardBackUrl
    }
    ).then(res => {
      let response = JSON.parse(res);
      // console.log('查询征信返回',response)
      if(!response.resp_code=='SW0000'){
        message.error(response.resp_msg);
        return;
      }
      this.setState({ creditAccountInfo: response.resp_data});

      this.setState({ showCreditDrawer: true });
    });
  };
  cancelCreditDrawerHandler = () => {
    this.setState({ showCreditDrawer: false });
  };

  renderCreditDrawer(){
    let creditAccountInfo = this.state.creditAccountInfo;
    if(!creditAccountInfo){
      return ;
    }
    let modelTypeDes = '';
    switch (creditAccountInfo.model_type) {
      case "M1":
        modelTypeDes = '白户';
        break;
      case "M2":
        modelTypeDes = '少信用记录';
        break;
      case "M3":
        modelTypeDes = '多信用记录';
        break;
    }

    let columnsCreditAccountData = [
      {
        title: '授信总额',
        fxhdzhxx: creditAccountInfo.labels.c0001,
        xhedfzhxx: creditAccountInfo.labels.c0008,
        xhdzhxx: creditAccountInfo.labels.c0015,
      },
      {
        title: '账户数',
        fxhdzhxx: creditAccountInfo.labels.c0002,
        xhedfzhxx: creditAccountInfo.labels.c0007,
        xhdzhxx: creditAccountInfo.labels.c0011,
      },
      {
        title: '机构数',
        fxhdzhxx: creditAccountInfo.labels.c0003,
        xhedfzhxx: creditAccountInfo.labels.c0009,
        xhdzhxx: creditAccountInfo.labels.c0012,
      },
      {
        title: '最近6个月平均应还款',
        fxhdzhxx: creditAccountInfo.labels.c0004,
        xhedfzhxx: creditAccountInfo.labels.c0006,
        xhdzhxx: creditAccountInfo.labels.c0013,
      },
      {
        title: '贷款余额',
        fxhdzhxx: creditAccountInfo.labels.c0005,
        xhedfzhxx: creditAccountInfo.labels.c0010,
        xhdzhxx: creditAccountInfo.labels.c0010,
      },
    ]
    return (
      <Drawer
        title="征信报告"
        width="60%"
        placement="right"
        closable={true}
        onClose={this.cancelCreditDrawerHandler}
        visible={this.state.showCreditDrawer}>
        <Descriptions title={<CustomCard title="征信规则" />}>
          <Descriptions.Item label="p1">通过</Descriptions.Item>
          <Descriptions.Item label="p2">拒绝</Descriptions.Item>
          <Descriptions.Item label="p3">通过</Descriptions.Item>
        </Descriptions>
        <Divider />
        <Descriptions title={<CustomCard title="征信记录" />} column={2}>
          <Descriptions.Item label="记录">{creditAccountInfo.model_type}-{modelTypeDes}</Descriptions.Item>
          <Descriptions.Item label="评分">{creditAccountInfo.score}</Descriptions.Item>

          <Descriptions.Item label="有无信贷记录">{creditAccountInfo.labels.j0006=='是' ? '无' : '有'  }</Descriptions.Item>
          <Descriptions.Item label="近一个月查询记录">{creditAccountInfo.labels.a0017}</Descriptions.Item>
        </Descriptions>
        <Divider />
        <Descriptions title={<CustomCard title="贷款、信用卡、融资审批查询次数" />} column={2}>
          <Descriptions.Item label="近三个月">{creditAccountInfo.labels.j0022}</Descriptions.Item>
          <Descriptions.Item label="近六个月">{creditAccountInfo.labels.j0021}</Descriptions.Item>
        </Descriptions>
        <Divider />
        <Descriptions title={<CustomCard title="贷记卡账户信息" />}>
          <Descriptions.Item label="已使用额度">{creditAccountInfo.labels.c0017}</Descriptions.Item>
          <Descriptions.Item label="授信额度">{creditAccountInfo.labels.c0018}</Descriptions.Item>
          <Descriptions.Item label="发卡机构数">{creditAccountInfo.labels.c0022}</Descriptions.Item>
          <Descriptions.Item label="账户数">{creditAccountInfo.labels.c0020} / {creditAccountInfo.labels.c0023}</Descriptions.Item>
          <Descriptions.Item label="额度使用率">{creditAccountInfo.labels.xs010}</Descriptions.Item>
          <Descriptions.Item label="额度使用率(单张最高)">{creditAccountInfo.labels.xs011}</Descriptions.Item>
        </Descriptions>
        <Divider />
        <Descriptions title={<CustomCard title="征信情况" />} column={2}>
          <Descriptions.Item label="未结清贷账户数">{creditAccountInfo.labels.xs001}</Descriptions.Item>
          <Descriptions.Item label="房贷账户数">{creditAccountInfo.labels.xs002}</Descriptions.Item>
          <Descriptions.Item label="未结清房贷账户余额">{creditAccountInfo.labels.xs003}</Descriptions.Item>
          <Descriptions.Item label="除房贷以外未结清抵押/质押类贷款">{creditAccountInfo.labels.xs004}数/{creditAccountInfo.labels.xs005}额</Descriptions.Item>
          <Descriptions.Item label="经营类贷款账户数">{creditAccountInfo.labels.xs006}</Descriptions.Item>
          <Descriptions.Item label="未结清经营类贷款账户余额">{creditAccountInfo.labels.xs007}</Descriptions.Item>
          <Descriptions.Item label="近一个月要到期贷款">{creditAccountInfo.labels.xs008}数/{creditAccountInfo.labels.xs009}额</Descriptions.Item>
          <Descriptions.Item label="是否存在对外担保">{creditAccountInfo.labels.xs012}</Descriptions.Item>
          <Descriptions.Item label="担保金额">{creditAccountInfo.labels.xs013}</Descriptions.Item>
          <Descriptions.Item label="剩余担保金额">{creditAccountInfo.labels.xs014}</Descriptions.Item>
          <Descriptions.Item label="是否当前逾期">{creditAccountInfo.labels.xs015}</Descriptions.Item>
          <Descriptions.Item label="是否存在低保救助记录">{creditAccountInfo.labels.xs016}</Descriptions.Item>
        </Descriptions>

        <Divider />
        <Descriptions title={<CustomCard title="账户情况" />}>

        </Descriptions>
        <AntTable
          columns={columnsCreditAccountService}
          dataSource={columnsCreditAccountData}/>

      </Drawer>
    );
  };

  /**
   * 征信drawer
   */
  returnCreditBaseInfoDrawerHandler = () => {

    const uoiObj = this.props.userOrderInfoDto || {}
    const uid = uoiObj.uid;

    yunxinCreditService.getUserCreditBaseInfo({
        uid,
      }
    ).then(res => {
      // console.log('查询征信基础信息返回',res)
      let tempData = res;
      tempData.ocrStartValidDate = moment(tempData.ocrStartValidDate)
      this.setState({ creditBaseInfo: tempData});
      this.setState({ showCreditBaseInfoDrawer: true });
    });

  };
  cancelCreditBaseInfoDrawerHandler = () => {
    this.setState({ showCreditBaseInfoDrawer: false });
  };

  renderCreditBaseInfoDrawer(){
    console.log('renderCreditBaseInfoDrawer exe')
    if(!this.state.creditBaseInfo){
      return ;
    }

    let creditBaseInfo = this.state.creditBaseInfo

    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
      },
    };

    const handleCreditBaseInfoSubmit = e =>{
      e.preventDefault();

      const fileds = ['email','homeAddress','resiStatus','resiPc','resiDist','registerDist','ocrStartValidDate','ocrAddress','ocrSex','ocrNation','ocrIssueOrg','firstContactName','firstContactPhone','firstContactRelation','annualInterestRate','coverCharge','coverChargeRatio','marriStatus','spoName','spoIdNum','spoTel','spsCmpyNm','eduLevel','acaDegree','empStatus','cpnName','cpnType','industry','cpnAddr','occupation','title','techTitle','workStartDate'];
      this.props.form.validateFields(fileds,(err, values) => {
        if (!err) {
          let postData = Object.assign({},values);
          postData.ocrStartValidDate = moment(postData.ocrStartValidDate).format('YYYY-MM-DD');
          postData.id = creditBaseInfo.id;
          yunxinCreditService.updateUserCreditBaseInfo(postData).then(res => {
            message.success('更新成功')
            this.setState({ creditBaseInfo: null});
            this.setState({ showCreditBaseInfoDrawer: false });
          });
        } else {
          message.error('请补全所有必填信息');
        }
      });
    }
    return (
      <Drawer
        title="征信基础信息编辑"
        width="50%"
        placement="right"
        closable={true}
        onClose={this.cancelCreditBaseInfoDrawerHandler}
        visible={this.state.showCreditBaseInfoDrawer}>

        <Form onSubmit={handleCreditBaseInfoSubmit}>
          <Form.Item  label="邮箱" {...formItemLayout}>
            {getFieldDecorator('email', {
              initialValue: creditBaseInfo.email
            })(<Input placeholder="请输入邮箱" />)}
          </Form.Item>
          <Form.Item  label="家庭地址" {...formItemLayout}>
            {getFieldDecorator('homeAddress', {
              initialValue: creditBaseInfo.homeAddress
            })(<Input placeholder="请输入家庭地址" />)}
          </Form.Item>
          <Form.Item label="居住状况" {...formItemLayout}>
            {getFieldDecorator('resiStatus', {
              initialValue: creditBaseInfo.homeAddress || '9'
            })(
              <Select>
                <Option value="1">自置</Option>
                <Option value="2">按揭</Option>
                <Option value="3">亲属楼宇</Option>
                <Option value="4">集体宿舍</Option>
                <Option value="5">租房</Option>
                <Option value="6">共有住宅</Option>
                <Option value="12">借住</Option>
                <Option value="9">未知</Option>
              </Select>
            )}
          </Form.Item>
          <Form.Item label="居住地邮编" {...formItemLayout}>
            {getFieldDecorator('resiPc', {
              initialValue: creditBaseInfo.resiPc
            })(<Input placeholder="请输入居住地邮编" />)}
          </Form.Item>
          <Form.Item label="居住地行政区划编号" {...formItemLayout}>
            {getFieldDecorator('resiDist', {
              initialValue: creditBaseInfo.resiDist
            })(<Input placeholder="请输入居住地行政区划（市县大区号）" />)}
          </Form.Item>
          <Form.Item label="户籍所在地行政区划编号" {...formItemLayout}>
            {getFieldDecorator('registerDist', {
              initialValue: creditBaseInfo.registerDist
            })(<Input placeholder="户籍所在地行政区划编号（市县大区号）" />)}
          </Form.Item>
          <Form.Item label="ocr结果中的身份证有效期开始日期" {...formItemLayout}>
            {getFieldDecorator('ocrStartValidDate', {
              initialValue: creditBaseInfo.ocrStartValidDate
            })( <DatePicker format={'YYYY-MM-DD'} allowClear={false}/>
              // <Input placeholder="请输入ocr结果中的身份证有效期开始日期" />
            )}
          </Form.Item>
          <Form.Item label="OCR结果中的地址" {...formItemLayout}>
            {getFieldDecorator('ocrAddress', {
              initialValue: creditBaseInfo.ocrAddress
            })(<Input placeholder="请输入OCR结果中的地址" />)}
          </Form.Item>
          <Form.Item label="OCR结果中的性别" {...formItemLayout}>
            {getFieldDecorator('ocrSex', {
              initialValue: creditBaseInfo.ocrSex || '1'
            })(
              <Select>
                <Option value="1">男性</Option>
                <Option value="2">女性</Option>
              </Select>
            )}
          </Form.Item>
          <Form.Item label="OCR结果中的民族" {...formItemLayout}>
            {getFieldDecorator('ocrNation', {
              initialValue: creditBaseInfo.ocrNation || '汉族'
            })(<Input placeholder="请输入OCR结果中的民族" />)}
          </Form.Item>
          <Form.Item label="OCR结果中的发证机关" {...formItemLayout}>
            {getFieldDecorator('ocrIssueOrg', {
              initialValue: creditBaseInfo.ocrIssueOrg
            })(<Input placeholder="请输入OCR结果中的发证机关" />)}
          </Form.Item>
          <Form.Item label="紧急联系人姓名" {...formItemLayout}>
            {getFieldDecorator('firstContactName', {
              initialValue: creditBaseInfo.firstContactName
            })(<Input placeholder="请输入紧急联系人姓名" />)}
          </Form.Item>
          <Form.Item label="紧急联系人电话" {...formItemLayout}>
            {getFieldDecorator('firstContactPhone', {
              initialValue: creditBaseInfo.firstContactPhone
            })(<Input placeholder="请输入紧急联系人电话" />)}
          </Form.Item>
          <Form.Item label="紧急联系人关系" {...formItemLayout}>
            {getFieldDecorator('firstContactRelation', {
              initialValue: creditBaseInfo.firstContactRelation
            })(
              <Select>
                <Option value="1">父母</Option>
                <Option value="2">子女</Option>
                <Option value="3">兄弟姐妹</Option>
                <Option value="4">朋友</Option>
                <Option value="5">配偶</Option>
                <Option value="6">亲戚</Option>
                <Option value="7">同学</Option>
                <Option value="8">同事</Option>
                <Option value="9">其他</Option>
              </Select>
            )}
          </Form.Item>

          <Form.Item label="婚姻状况" {...formItemLayout}>
            {getFieldDecorator('marriStatus', {
              initialValue: creditBaseInfo.marriStatus || '99'
            })(
              <Select>
                <Option value="10">未婚</Option>
                <Option value="20">已婚</Option>
                <Option value="30">丧偶</Option>
                <Option value="40">离婚</Option>
                <Option value="99">未知</Option>
              </Select>
            )}
          </Form.Item>

          <Form.Item label="配偶姓名" {...formItemLayout}>
            {getFieldDecorator('spoName', {
              initialValue: creditBaseInfo.spoName
            })(<Input placeholder="请输入配偶姓名" />)}
          </Form.Item>
          <Form.Item label="配偶证件号码" {...formItemLayout}>
            {getFieldDecorator('spoIdNum', {
              initialValue: creditBaseInfo.spoIdNum
            })(<Input placeholder="请输入配偶证件号码" />)}
          </Form.Item>
          <Form.Item label="配偶联系电话" {...formItemLayout}>
            {getFieldDecorator('spoTel', {
              initialValue: creditBaseInfo.spoTel
            })(<Input placeholder="请输入配偶联系电话" />)}
          </Form.Item>
          <Form.Item label="配偶工作单位" {...formItemLayout}>
            {getFieldDecorator('spsCmpyNm', {
              initialValue: creditBaseInfo.spsCmpyNm
            })(<Input placeholder="请输入配偶工作单位" />)}
          </Form.Item>
          <Form.Item label="学历" {...formItemLayout}>
            {getFieldDecorator('eduLevel', {
              initialValue: creditBaseInfo.eduLevel || '99'
            })(
              <Select>
                <Option value="10">研究生</Option>
                <Option value="20">本科</Option>
                <Option value="30">大专</Option>
                <Option value="40">中专、职高、技校</Option>
                <Option value="60">高中</Option>
                <Option value="91">初中及以下</Option>
                <Option value="99">未知</Option>
              </Select>
            )}
          </Form.Item>
          <Form.Item label="学位" {...formItemLayout}>
            {getFieldDecorator('acaDegree', {
              initialValue: creditBaseInfo.acaDegree || '99'
            })(
              <Select>
                <Option value="1">名誉博士</Option>
                <Option value="2">博士</Option>
                <Option value="3">硕士</Option>
                <Option value="4">学士</Option>
                <Option value="5">无</Option>
                <Option value="99">未知</Option>
              </Select>
            )}
          </Form.Item>
          <Form.Item label="就业状况" {...formItemLayout}>
            {getFieldDecorator('empStatus', {
              initialValue: creditBaseInfo.empStatus || '99'
            })(
              <Select>
                <Option value="11">国家公务员</Option>
                <Option value="13">专业技术人员</Option>
                <Option value="21">企业管理人员</Option>
                <Option value="27">农民</Option>
                <Option value="31">学生</Option>
                <Option value="37">现役军人</Option>
                <Option value="51">自由职业者</Option>
                <Option value="54">个体经营者</Option>
                <Option value="70">无业人员</Option>
                <Option value="80">退（离）休人员</Option>
                <Option value="90">其他</Option>
                <Option value="99">未知</Option>
              </Select>
            )}
          </Form.Item>
          <Form.Item label="单位名称" {...formItemLayout}>
            {getFieldDecorator('cpnName', {
              initialValue: creditBaseInfo.cpnName
            })(
              <Fragment>
                <Input placeholder="请输入单位名称" />
                <div>
                  当“就业状况”为“国家公务员、专业技术人员、职员、企业管理人员、工人”，必填
                  * 填写信息主体就职单位的名称。其他情况合作方可以不提供
                </div>
              </Fragment>
            )}
          </Form.Item>
          <Form.Item label="单位性质" {...formItemLayout}>
            {getFieldDecorator('cpnType', {
              initialValue: creditBaseInfo.cpnType || '99'
            })(
              <Select>
                <Option value="10">机关、事业单位</Option>
                <Option value="20">国有企业</Option>
                <Option value="30">外资企业</Option>
                <Option value="40">个体、私营企业</Option>
                <Option value="50">其他（包括三资企业、民营企业、民间团体等）</Option>
                <Option value="99">未知</Option>
              </Select>
            )}
          </Form.Item>

          <Form.Item label="单位性质" {...formItemLayout}>
            {getFieldDecorator('industry', {
              initialValue: creditBaseInfo.industry || '9'
            })(
              <Select>
                <Option value="A">农、林、牧、渔业</Option>
                <Option value="B">采矿业</Option>
                <Option value="C">制造业</Option>
                <Option value="D">电力、热力、燃气及水生产和供应业</Option>
                <Option value="E">建筑业</Option>
                <Option value="F">批发和零售业</Option>
                <Option value="G">交通运输、仓储和邮储业</Option>
                <Option value="H">住宿和餐饮业</Option>
                <Option value="I">信息传输、软件和信息技术服务</Option>
                <Option value="J">金融业</Option>
                <Option value="K">房地产业</Option>
                <Option value="L">租赁和商务服务业</Option>
                <Option value="M">科学研究和技术服务业</Option>
                <Option value="N">水利、环境和公共设施管理业</Option>
                <Option value="O">居民服务、修理和其他服务业</Option>
                <Option value="P">教育</Option>
                <Option value="Q">卫生和社会工作</Option>
                <Option value="R">文化、体育和娱乐业</Option>
                <Option value="S">公共管理、社会保障和社会组织</Option>
                <Option value="T">国际组织</Option>
                <Option value="9">未知</Option>
              </Select>
            )}
          </Form.Item>
          <Form.Item label="单位详细地址" {...formItemLayout}>
            {getFieldDecorator('cpnAddr', {
              initialValue: creditBaseInfo.cpnAddr || '无'
            })(<Input placeholder="请输入单位详细地址" />)}
          </Form.Item>
          <Form.Item label="职业" {...formItemLayout}>
            {getFieldDecorator('occupation', {
              initialValue: creditBaseInfo.occupation || 'Z'
            })(
              <Select>
                <Option value="0">国家机关、党群组织、企业、事业单位负责人</Option>
                <Option value="1">专业技术人员</Option>
                <Option value="3">办事人员和有关人员</Option>
                <Option value="4">商业、服务业人员</Option>
                <Option value="5">农、林、牧、渔、水利业生产人员</Option>
                <Option value="6">生产、运输设备操作人员及有关人员</Option>
                <Option value="X">军人</Option>
                <Option value="Y">不便分类的其他从业人员</Option>
                <Option value="Z">未知</Option>
              </Select>
            )}
          </Form.Item>

          <Form.Item label="职务" {...formItemLayout}>
            {getFieldDecorator('title', {
              initialValue: creditBaseInfo.title || '9'
            })(
              <Select>
                <Option value="1">高级领导（行政级别局级及以上领导或大公司高级管理人员）</Option>
                <Option value="2">中级领导（行政级别处级领导或大公司中级管理人员）</Option>
                <Option value="3">一般员工</Option>
                <Option value="9">未知</Option>
              </Select>
            )}
          </Form.Item>

          <Form.Item label="职称" {...formItemLayout}>
            {getFieldDecorator('techTitle', {
              initialValue: creditBaseInfo.techTitle || '9'
            })(
              <Select>
                <Option value="1">高级</Option>
                <Option value="2">中级</Option>
                <Option value="3">初级</Option>
                <Option value="0">无</Option>
                <Option value="9">未知</Option>
              </Select>
            )}
          </Form.Item>

          <Form.Item label="本单位工作起始年份" {...formItemLayout}>
            {getFieldDecorator('workStartDate', {
              initialValue: creditBaseInfo.workStartDate
            })(
              <Fragment>
                <InputNumber min={1950} max={2023} />
                {/*<DatePicker mode={"year"} onChange={workStartDateOnChange}/>*/}
                <div>
                  当“就业状况”为“国家公务员、专业技术人员、职员、企业管理人员、工人、农民、学生、现役军人”时，必填
                </div>
              </Fragment>
            )}
          </Form.Item>

          <Fragment>
            <div style={{display:'flex',justifyContent:'center'}}>
              <Button type="primary" htmlType="submit">
                保存
              </Button>
            </div>
          </Fragment>

        </Form>


        <Divider />

      </Drawer>
    );
  };

  /**
   * 申请报送 modal
   */
  returnApplyCreditModalHandler = () => {
    this.setState({ showConfirmApplyCreditModal: true });
  };

  cancelApplyCreditModalHandler = () => {
    this.setState({ showConfirmApplyCreditModal: false });
  };

  renderApplyCreditModalModal = () => {
    const { form } = this.props;

    const btnConfirmHandler = e => {
      e.preventDefault();
      yunxinCreditService
        .applyCredit({
          orderId: getParam('id')
        })
        .then(res => {
          message.success('操作成功')
          this.setState({ showConfirmApplyCreditModal: false });
        });
    };

    return (
      <Modal
        title="申请报送"
        visible={this.state.showConfirmApplyCreditModal}
        onOk={btnConfirmHandler}
        onCancel={this.cancelApplyCreditModalHandler}
        destroyOnClose>
        <p>请确保征信基础信息填写完整,否则会报送失败.确认进行 [ 申请报送 ] 吗?</p>
      </Modal>
    );
  };


  /**
   * 放款报送 modal
   */
  returnLoanCreditModalHandler = () => {
    this.setState({ showConfirmLoanCreditModal: true });
  };

  cancelLoanCreditModalHandler = () => {
    this.setState({ showConfirmLoanCreditModal: false });
  };

  renderLoanCreditModalModal = () => {
    const { form } = this.props;

    const btnConfirmHandler = e => {
      e.preventDefault();
      yunxinCreditService
        .loanCredit({
          orderId: getParam('id')
        })
        .then(res => {
          message.success('操作成功')
          this.setState({ showConfirmLoanCreditModal: false });
        });
    };

    return (
      <Modal
        title="放款报送"
        visible={this.state.showConfirmLoanCreditModal}
        onOk={btnConfirmHandler}
        onCancel={this.cancelLoanCreditModalHandler}
        destroyOnClose>
        <p>确认进行 [ 放款报送 ] 吗?</p>
      </Modal>
    );
  };

  /**
   * 取消放款 modal
   */
  returnCancelCreditModalHandler = () => {
    this.setState({ showConfirmCancelCreditModal: true });
  };

  cancelCancelCreditModalHandler = () => {
    this.setState({ showConfirmCancelCreditModal: false });
  };

  renderCancelCreditModalModal = () => {
    const { form } = this.props;

    const btnConfirmHandler = e => {
      e.preventDefault();
      yunxinCreditService
        .cancelLoanCredit({
          orderId: getParam('id')
        })
        .then(res => {
          message.success('操作成功')
          this.setState({ showConfirmCancelCreditModal: false });
        });
    };

    return (
      <Modal
        title="取消放款"
        visible={this.state.showConfirmCancelCreditModal}
        onOk={btnConfirmHandler}
        onCancel={this.cancelCancelCreditModalHandler}
        destroyOnClose>
        <p>确认 [ 取消放款 ] 吗?</p>
      </Modal>
    );
  };

  /**
   * 结清 modal
   */
  returnSettleCreditModalHandler = () => {
    this.setState({ showConfirmSettleCreditModal: true });
  };

  cancelSettleCreditModalHandler = () => {
    this.setState({ showConfirmSettleCreditModal: false });
  };

  renderSettleCreditModalModal = () => {
    const { form } = this.props;

    const btnConfirmHandler = e => {
      e.preventDefault();
      yunxinCreditService
        .settleCredit({
          orderId: getParam('id')
        })
        .then(res => {
          message.success('操作成功')
          this.setState({ showConfirmSettleCreditModal: false });
        });
    };

    return (
      <Modal
        title="结清"
        visible={this.state.showConfirmSettleCreditModal}
        onOk={btnConfirmHandler}
        onCancel={this.cancelSettleCreditModalHandler}
        destroyOnClose>
        <p>确认 [ 结清 ] 吗?</p>
      </Modal>
    );
  };


  /**
   * 追偿报送 modal
   */
  returnDunRepayCreditModalHandler = () => {
    this.setState({ showConfirmDunRepayCreditModal: true });
  };

  cancelDunRepayCreditModalHandler = () => {
    this.setState({ showConfirmDunRepayCreditModal: false });
  };

  renderDunRepayCreditModalModal = () => {
    const { form } = this.props;

    const btnConfirmHandler = e => {
      e.preventDefault();
      message.success('追偿报送')

      this.props.form.validateFields(['amount', 'surplusAmount', 'fivecate'], (err, values) => {
        if (!err) {
          console.log('看下数据',values)
          let postData = Object.assign({},values);
          postData.orderId = getParam('id');
          yunxinCreditService
            .dunRepayCredit(postData)
            .then(res => {
              message.success('操作成功')
              this.setState({ showConfirmDunRepayCreditModal: false });
            });
        }else{
          message.error('请补全所有必填信息');
        }
      });
    };

    return (
      <Modal
        title="追偿报送"
        visible={this.state.showConfirmDunRepayCreditModal}
        onOk={btnConfirmHandler}
        width={ 750}
        onCancel={this.cancelDunRepayCreditModalHandler}
        destroyOnClose>
        <Form>
          <Form.Item label="追偿本金额（不含利息）" {...formItemLayout}>
            {form.getFieldDecorator('amount', {
              rules: [{ required: true, message: '请输入追偿本金额' }],
            })(
              <InputNumber min={0}  />
            )}
          </Form.Item>

          <Form.Item label="当前追偿后剩余金额" {...formItemLayout}>
            {form.getFieldDecorator('surplusAmount', {
              rules: [{ required: true, message: '请输入剩余金额' }],
            })(
              <InputNumber min={0}  />
            )}
          </Form.Item>

          <Form.Item label="五级分类" {...formItemLayout}>
            {form.getFieldDecorator('fivecate', {
              rules: [{ required: true, message: '请选择分类' }],
            })(
              <Select>
                <Option value="1">正常 （0-30天）</Option>
                <Option value="2">关注 （31-60天）</Option>
                <Option value="3">次级 （61-120天）</Option>
                <Option value="4">可疑 （121天-180天）</Option>
                <Option value="5">损失 （180天以上）</Option>
              </Select>
            )}
          </Form.Item>
        </Form>
      </Modal>
    );
  };


  /**
   * 追偿结清 modal
   */
  returnDunRepaySettleCreditModalHandler = () => {
    this.setState({ showConfirmDunRepaySettleCreditModal: true });
  };

  cancelDunRepaySettleCreditModalHandler = () => {
    this.setState({ showConfirmDunRepaySettleCreditModal: false });
  };

  renderDunRepaySettleCreditModal = () => {
    const { form } = this.props;

    const btnConfirmHandler = e => {
      e.preventDefault();
      yunxinCreditService
        .dunRepaySettleCredit({
          orderId: getParam('id')
        })
        .then(res => {
          message.success('操作成功')
          this.setState({ showConfirmDunRepaySettleCreditModal: false });
        });
    };

    return (
      <Modal
        title="追偿结清"
        visible={this.state.showConfirmDunRepaySettleCreditModal}
        onOk={btnConfirmHandler}
        onCancel={this.cancelDunRepaySettleCreditModalHandler}
        destroyOnClose>
        <p>确认 [ 追偿结清 ] 吗?</p>
      </Modal>
    );
  };


  //退押金
  // confirm = ()=>{
  //   const orderId = getParam('id');
  //   orderService
  //     .forceDepositRefund({
  //       orderId,
  //     })
  //     .then(res => {
  //   console.log(res,'退押金')
  //     });
  // }
  renderBaseInfo() {
    const { userOrderInfoDto = {}, contractUrl } = this.props;
    const orderId = getParam('id');
    const { auditLabel, status } = userOrderInfoDto;
    return (
      <Card bordered={false} style={{ marginTop: 20 }}>
        <Descriptions title={<CustomCard title="下单人信息" />}>
          <Descriptions.Item label="姓名">{userOrderInfoDto.userName}</Descriptions.Item>
          <Descriptions.Item label="手机号">{userOrderInfoDto.telephone}</Descriptions.Item>
          <Descriptions.Item label="身份证号">{userOrderInfoDto.idCard}</Descriptions.Item>
          <Descriptions.Item label="年龄">{userOrderInfoDto.age}</Descriptions.Item>
          <Descriptions.Item label="性别">{userOrderInfoDto.gender}</Descriptions.Item>
          <Descriptions.Item label="下单时间">{userOrderInfoDto.createTime}</Descriptions.Item>
          <Descriptions.Item label="在途订单数">
            {userOrderInfoDto.userPayCount > 1 ? (
              <span className="red-status">{userOrderInfoDto.userPayCount}</span>
            ) : (
              userOrderInfoDto.userPayCount
            )}
          </Descriptions.Item>
          <Descriptions.Item label="完结订单数">
            {userOrderInfoDto.userFinishCount}
          </Descriptions.Item>
          <Descriptions.Item label="订单状态">
            {orderStatusMap[userOrderInfoDto.status]}
          </Descriptions.Item>
          <Descriptions.Item label="人脸认证" span={1}>
            {userOrderInfoDto.userFaceCertStatus ? (
              <span className="green-status">已通过</span>
            ) : (
              <span className="red-status">未通过</span>
            )}
          </Descriptions.Item>
          {/* <Descriptions.Item label="渠道来源">{userOrderInfoDto.channelName}</Descriptions.Item> */}
          <Descriptions.Item label="所在位置">
            {

              userOrderInfoDto.orderLocationAddress && userOrderInfoDto.orderLocationAddress.province
                ? `${userOrderInfoDto.orderLocationAddress.province}${userOrderInfoDto.orderLocationAddress.city}${userOrderInfoDto.orderLocationAddress.district}${userOrderInfoDto.orderLocationAddress.streetNumber}`
                : '暂无定位'
            }
          </Descriptions.Item>
          <Descriptions.Item label={zhouqikoukuanReturnLabel(this.state.orderDetailApiRes, "周期扣款")}>
            {zhouqikoukuanReturnText(this.state.orderDetailApiRes, "周期扣款")}
          </Descriptions.Item>

          <Descriptions.Item label="身份证照片" span={2}>
            {userOrderInfoDto.idCardBackUrl ? (
              <>
                <img
                  src={userOrderInfoDto.idCardBackUrl}
                  style={{ width: 146, height: 77, marginRight: 20 }}
                />
                <img src={userOrderInfoDto.idCardFrontUrl} style={{ width: 146, height: 77 }} />
              </>
            ) : (
              <span className="red-status">未上传</span>
            )}
          </Descriptions.Item>
          {/*<Descriptions.Item label="风险分">*/}
          {/*      <span className="primary-color">*/}
          {/*        {userOrderInfoDto.score}分*/}
          {/*       */}
          {/*      </span>*/}
          {/*</Descriptions.Item>*/}
        </Descriptions>
        {status === '11' ? (
          <Fragment>
            {auditLabel !== '00' ? (
              <Fragment>
                <Button type="primary" onClick={() => this.showOrderModal('audit', true)}>
                  审批通过
                </Button>
                <Button type="primary" onClick={() => this.showOrderModal('audit', false)}>
                  审批拒绝
                </Button>
              </Fragment>
            ) : null}
            <Button type="primary" onClick={() => this.showOrderModal('close')}>
              关闭订单
            </Button>
            <Button type="primary" onClick={() => this.showOrderModal('address')}>
              修改收货信息
            </Button>
          </Fragment>
        ) : null}
        {['01', '02'].includes(status) ? (
          <Button type="primary" onClick={() => this.showOrderModal('address')}>
            修改收货信息
          </Button>
        ) : null}
        {status === '04' ? (
          <Fragment>
            {/* {
  contractUrl?<Button type="primary" onClick={() => this.showOrderModal('deliver')}>
  发货
</Button>:<Button type="primary" onClick={() => this.showOrderModal('deliver')}>
  发货
</Button>
} */}
            {contractUrl ? null : (
              <Popconfirm
                title={<div>费用：{this.state.cost}元</div>}
                style={{
                  textAlign: 'center',
                }}
                icon={null}
                okText="确定"
                cancelText="取消"
                disabled={this.state.sign}
                onConfirm={this.onConfirm}
              >
                <Button type="primary">签约存证</Button>
              </Popconfirm>
            )}

            <Button type="primary" onClick={() => this.showOrderModal('deliver')}>
              发货
            </Button>
            <Button type="primary" onClick={() => this.showOrderModal('close')}>
              关闭订单
            </Button>
            <Button type="primary" onClick={() => this.showOrderModal('address')}>
              修改收货信息
            </Button>
          </Fragment>
        ) : null}
        {status === '05' ? (
          <Fragment>
            <Button type="primary" onClick={() => this.showOrderModal('close')}>
              关闭订单
            </Button>
            <Button type="primary" onClick={() => this.showOrderModal('express')}>
              修改物流信息
            </Button>
          </Fragment>
        ) : null}
        {status === '07' ? (
          <Button
            type="primary"
            onClick={() =>
              this.settleDia('settle', orderId, userOrderInfoDto.orderCashId, userOrderInfoDto)
            }
          >
            结算
          </Button>
        ) : null}
        {getParam('settlement') ? (
          <Button type="primary" onClick={() => this.showOrderModal('remarks')}>
            记录催收
          </Button>
        ) : null}
        {['11', '04', '05'].includes(status) ? (
          <Button type="primary" onClick={() => this.setState({ depositModal: true })}>
            修改押金
          </Button>
        ) : null}
        <Button type="primary" onClick={() => this.showOrderModal('remark')}>
          备注
        </Button>

        {/* <Popconfirm title="确认退款？" onConfirm={this.confirm} okText="确认" cancelText="取消">
          <Button type="primary">退押金</Button>
        </Popconfirm> */}
        {this.props.userOrderInfoDto.status !== '06' ? null : (
          <Button type="primary" onClick={this.returnBackHandler}>
            确认归还
          </Button>
        )}
        <Button type="primary" onClick={this.returnCreditDrawerHandler}>
          查看征信报告
        </Button>
        <Button type="primary" onClick={this.returnCreditBaseInfoDrawerHandler}>
          编辑征信信息
        </Button>

        <Button type="primary" onClick={this.returnApplyCreditModalHandler}>
          申请报送
        </Button>
        <Button type="primary" onClick={this.returnLoanCreditModalHandler}>
          放款报送
        </Button>
        <Button type="primary" onClick={this.returnCancelCreditModalHandler}>
          取消放款
        </Button>
        <Button type="primary" onClick={this.returnSettleCreditModalHandler}>
          结清
        </Button>
        <Button type="primary" onClick={this.returnDunRepayCreditModalHandler}>
          追偿报送
        </Button>
        <Button type="primary" onClick={this.returnDunRepaySettleCreditModalHandler}>
          追偿结清
        </Button>

        <Divider />

        {/*<Descriptions title={<CustomCard title="商家信息" />}>*/}
        {/*  <Descriptions.Item label="商家名称">*/}
        {/*    {shopInfoDto && shopInfoDto.shopName}*/}
        {/*  </Descriptions.Item>*/}
        {/*  <Descriptions.Item label="商家电话" span={2}>*/}
        {/*    {shopInfoDto && shopInfoDto.telephone}*/}
        {/*  </Descriptions.Item>*/}
        {/*</Descriptions>*/}
        {/*<Divider />*/}
      </Card>
    );
  }
  depositOverSure = e => {
    const orderId = getParam('id');
    e.preventDefault();
    this.props.form.validateFields(['afterAmount'], (err, values) => {
      if (!err) {
        OrderService.updatePayDepositAmount({ orderId, afterAmount: values.afterAmount }).then(
          res => {
            if (res) {
              message.success('修改成功');
              //押金信息
              OrderService.queryPayDepositLog({ orderId }).then(res => {
                if (res) {
                  this.setState({
                    depositData: res,
                    depositModal: false,
                  });
                }
              });
            }
          },
        );
      }
    });
  };
  render() {
    const { orderBuyOutDto, wlList } = this.props;
    const { drawerVisible } = this.state;
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    return (
      <PageHeaderWrapper>
        <Spin
          spinning={getParam('RentRenewal') ? this.props.RentRenewalLoading : this.props.loading}
        >
          {this.renderBaseInfo()}
          {this.renderContentTabCard()}
        </Spin>
        <BackTop />
        {this.renderRemarkModal()}
        {this.renderDeliverModal()}
        {this.renderCloseModal()}
        {this.renderSettleModal()}
        {this.renderAddressModal()}
        {this.renderAuditModal()}
        {this.renderCollectionRecordModal()}
        {this.renderConfirmOrderReturnModal()}
        {this.renderCreditDrawer()}
        {this.renderCreditBaseInfoDrawer()}

        {this.renderApplyCreditModalModal()}
        {this.renderLoanCreditModalModal()}
        {this.renderCancelCreditModalModal()}
        {this.renderSettleCreditModalModal()}
        {this.renderDunRepayCreditModalModal()}
        {this.renderDunRepaySettleCreditModal()}
        <Drawer
          width={420}
          title={this.state.drawerTitle || '发货物流信息'}
          placement="right"
          onClose={this.onClose}
          visible={drawerVisible}>
          <Timeline>
            {wlList.map((item, idx) => {
              let color = 'blue';
              if (idx === 0) {
                color = 'green';
              }
              return (
                <Timeline.Item
                  style={{ color: idx !== 0 ? '#aaa' : '#333' }}
                  key={idx}
                  color={color}
                >
                  <p>{item.remark}</p>
                  <p>{item.datetime}</p>
                </Timeline.Item>
              );
            })}
          </Timeline>
        </Drawer>
        <Modal
          title="修改押金"
          visible={this.state.depositModal}
          onOk={this.depositOverSure}
          onCancel={() => this.setState({ depositModal: false })}
          destroyOnClose
        >
          <Form>
            <Form.Item label="当前押金总额" {...formItemLayout}>
              {this.state.depositData && this.state.depositData.amount}
            </Form.Item>
            <Form.Item label="修改金额(元)" {...formItemLayout}>
              {getFieldDecorator('afterAmount', {
                rules: [
                  { required: true, message: '请输入押金金额' },
                  {
                    pattern: /^\d+$|^\d+[.]?\d+$/,
                    message: '请正确输入金额',
                  },
                ],
              })(<Input placeholder="请输入" />)}
            </Form.Item>
          </Form>
        </Modal>
      </PageHeaderWrapper>
    );
  }
}
