import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import {
  Card,
  message,
  Tooltip,
  Button,
  Form,
  Input,
  Select,
  DatePicker,
  Modal,
  Descriptions,
  Steps,
  Divider,
  Drawer,
  Timeline,
} from 'antd';
import MyPageTable from '@/components/MyPageTable';
import AntTable from '@/components/AntTable';
import { onTableData, getParam } from '@/utils/utils.js';
import moment from 'moment';
const { Option } = Select;
const { RangePicker } = DatePicker;
const { TextArea } = Input;
const { Step } = Steps;
import CustomCard from '@/components/CustomCard';
import { router } from 'umi';
@connect(({ order }) => ({
  ...order,
}))
@Form.create()
export default class Details extends Component {
  state = {
    visible: false,
    xcurrent: 1,
    scurrent: 1,
  };

  componentDidMount() {
    this.getData()
  }
  
  getData = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'order/queryOpeBuyOutOrderDetail',
      payload: {
        orderId: getParam('orderId'),
        buyOutOrderId: getParam('buyOutOrderId'),
      },
    });
  }
  
  onQueryOrderRemark = (pageNumber, pageSize, source) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'order/queryOrderRemark',
      payload: {
        orderId: getParam('buyOutOrderId'),
        pageNumber,
        pageSize,
        source,
      },
    });
  };
  //新建备注翻页
  onPage = e => {
    console.log(e, 'e');
    this.setState(
      {
        xcurrent: e.current,
      },
      () => {
        this.onQueryOrderRemark(e.current, 3, '01');
      },
    );
  };
  //商机备注翻页
  onPageBusiness = e => {
    console.log(e, 'e');

    this.setState(
      {
        scurrent: e.current,
      },
      () => {
        this.onQueryOrderRemark(e.current, 3, '02');
      },
    );
  };
  //物流信息
  onClose = () => {
    this.setState({
      drawerVisible: false,
    });
  };

  handleOk = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { dispatch } = this.props;
        dispatch({
          type: 'order/orderRemark',
          payload: {
            orderId: getParam('buyOutOrderId'),
            remark: values.beizhu,
            orderType: '04',
          },
          callback: res => {
            this.onQueryOrderRemark(1, 3, '02');
            this.getData()
            this.setState({
              visible: false,
            });
          },
        });
      }
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  showModal = () => {
    this.setState({
      visible: true,
    });
  };
  render() {
    const {
      userOrderInfoDto,
      shopInfoDto,
      orderAddressDto,
      receiptExpressInfo,
      giveBackExpressInfo,
      opeRemarkDtoPage={},
      businessRemarkDtoPage={},
      productInfo,
      userOrderCashesDto,
      orderByStagesDtoList,
      orderBuyOutDto,
      originalOrderDto,
      userOrderBuyOutDto,
      wlList,
    } = this.props;
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const statedata = {
      '04': 0,
      '05': 1,
      '03': 2,
    };
    //平台备注
    const columns = [
      {
        title: '备注人姓名',
        dataIndex: 'userName',
      },
      {
        title: '备注时间',
        dataIndex: 'createTime',
      },
      {
        title: '备注内容',
        dataIndex: 'remark',
      },
    ];
    //商家备注
    const columnsBusiness = [
      {
        title: '备注人姓名',
        dataIndex: 'userName',
      },
      {
        title: '备注时间',
        dataIndex: 'createTime',
      },
      {
        title: '备注内容',
        dataIndex: 'remark',
      },
    ];

    //买断
    const columnsBuyOut = [
      {
        title: '市场价',
        dataIndex: 'marketPrice',
      },
      {
        title: '销售价',
        dataIndex: 'salePrice',
      },
      {
        title: '买断尾款',
        dataIndex: 'endFund',
      },
    ];
    let detailsStute = {
      '01': '待支付',
      '02': '支付中',
      '03': '已支付申请关单',
      '04': '待发货',
      '05': '待确认收货',
      '06': '租用中',
      '07': '待结算',
      '08': '结算待支付',
      '09': '订单完成',
      '10': '交易关闭',
    };
    //原订单
    const columnsOriginalOrder = [
      {
        title: '商品图片',
        dataIndex: 'imageUrl',
        render: imageUrl => <img src={imageUrl} alt="" style={{ width: 90 }} />,
      },
      {
        title: '商品名称',
        dataIndex: 'productName',
      },
      {
        title: '规格颜色',
        dataIndex: 'spec',
      },
      {
        title: '原订单号',
        render: () => {
          return (
            <a className="primary-color"
               onClick={() => router.push(`/Order/HomePage/Details?id=${getParam('orderId')}`)}>
              查看原订单
            </a>
          );
        },
      },
      {
        title: '总租金',
        dataIndex: 'totalRent',
      },
      {
        title: '已付租金',
        dataIndex: 'payedRent',
      },
      {
        title: '订单状态',
        dataIndex: 'status',
        render: status => <span>{detailsStute[status]}</span>,
      },
    ];
    const paginationProps = {
      current: 1,
      pageSize: 1000,
      total: 1,
    };

    return (
      <PageHeaderWrapper>
        <Card title={<CustomCard title="订单进度" />} bordered={false}>
          <Steps
            progressDot={document.body.clientWidth < 1025 ? false : true}
            current={
              userOrderBuyOutDto && userOrderBuyOutDto[0] && statedata[userOrderBuyOutDto[0].state]
            }
          >
            <Step title="买家下单" />
            <Step title="买家付款" />
            <Step title="订单完成" />
          </Steps>
        </Card>
        <Card bordered={false} style={{ marginTop: 20 }}>
          {/* <Descriptions title={<CustomCard title="订单信息" />}>
            <Descriptions.Item label="下单人姓名"></Descriptions.Item>
            <Descriptions.Item label="下单人手机号"></Descriptions.Item>
            <Descriptions.Item label="身份证号"></Descriptions.Item>
            <Descriptions.Item label="下单时间"></Descriptions.Item>
            <Descriptions.Item label="订单状态"></Descriptions.Item>
            <Descriptions.Item label="完结订单数"></Descriptions.Item>
            <Descriptions.Item label="在途订单数"></Descriptions.Item>
            <Descriptions.Item label="信用分"></Descriptions.Item>
            <Descriptions.Item label="用户租赁协议">
              <a>《租赁协议》</a>
            </Descriptions.Item>
          </Descriptions>
          <Divider /> */}
          <CustomCard title="平台备注" />
          <MyPageTable
            onPage={this.onPage}
            paginationProps={{
              current: this.state.xcurrent,
              pageSize: 3,
              total: opeRemarkDtoPage.total,
            }}
            dataSource={onTableData(opeRemarkDtoPage.records)}
            columns={columns}
          />
          <Divider />
          <CustomCard title="商家备注" style={{ marginBottom: 20 }} />
          <Button type="primary" style={{ margin: '20px 0' }} onClick={() => this.showModal()}>
            + 新建备注
          </Button>
          <MyPageTable
            onPage={this.onPageBusiness}
            paginationProps={{
              current: this.state.scurrent,
              pageSize: 3,
              total: businessRemarkDtoPage.total,
            }}
            dataSource={onTableData(businessRemarkDtoPage.records)}
            columns={columnsBusiness}
          />
          <Divider />
          <Descriptions
            title={
              <>
                <CustomCard title="收货人信息" />
                {/*<Button>修改地址</Button>*/}
              </>
            }
          >
            <Descriptions.Item label="收货人姓名">
              {orderAddressDto && orderAddressDto.realname}
            </Descriptions.Item>
            <Descriptions.Item label="收货人手机号">
              {orderAddressDto && orderAddressDto.telephone}
            </Descriptions.Item>
            <Descriptions.Item label="收货人地址">
              {orderAddressDto && orderAddressDto.street}
            </Descriptions.Item>
            {/* <Descriptions.Item label="用户备注">{userOrderInfoDto.orderRemark}</Descriptions.Item> */}
          </Descriptions>
        </Card>
        <Card bordered={false} style={{ margin: '30px 0' }}>
          <CustomCard title="买断信息" style={{ marginBottom: 20 }} />
          <AntTable
            columns={columnsBuyOut}
            dataSource={onTableData(userOrderBuyOutDto)}
            paginationProps={paginationProps}
          />
          <CustomCard title="原订单信息" style={{ marginBottom: 20 }} />
          <AntTable
            columns={columnsOriginalOrder}
            dataSource={onTableData(originalOrderDto)}
            paginationProps={paginationProps}
          />
        </Card>
        <Modal
          title="备注"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Form>
            <Form.Item label="备注内容" {...formItemLayout}>
              {getFieldDecorator('beizhu', {
                rules: [{ required: true, message: '请输入备注' }],
              })(<TextArea placeholder="请输入" />)}
            </Form.Item>
          </Form>
        </Modal>
      </PageHeaderWrapper>
    );
  }
}
