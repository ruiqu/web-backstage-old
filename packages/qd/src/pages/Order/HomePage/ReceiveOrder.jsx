import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Button, Card, Form, Spin, Table, Tooltip, Modal, message, } from 'antd';
import { connect } from 'dva';
import { getToken } from '@/utils/localStorage';
import { routerRedux } from 'dva/router';
import orderService from "@/services/order";
import { getTimeDistance, renderOrderStatus } from '@/utils/utils';

@Form.create()
export default class ReceiveOrder extends Component {
    state = {
        loading: false,
        userLoading: false,
        orderList: [],
        userList: [],
        userVisible: false,
        // 选择的用户
        selectUserItem: {},
        // 选择订单
        selectOrderItem: []

    };

    // 页面装载前 在render 之前调用
    componentWillMount() {
        this.setState({
            loading: true
        })
    }

    // 页面装载后
    componentDidMount() {
        this.initData();
    }


    //未认领的订单数据
    initData = () => {
        orderService.listQueryOpeOrderByCondition().then(res => {
            if (res) {
                console.log("orderList", res)
                this.setState({
                    orderList: res,
                    loading: false
                })
            } else {
                this.setState({
                    loading: false
                })
            }
        })

    }

    selectUser = () => {
        if(this.state.selectOrderItem && this.state.selectOrderItem.length == 0){
            message.warning('请先选择订单');
        }else{
            this.setState({
                userLoading: true
            }, () => {
                // 用户
                orderService.listBackstageUser().then(res => {
                    if (res) {
                        console.log("userList", res)
                        this.setState({
                            userList: res,
                            userLoading: false,
                            userVisible: true
                        })
                    } else {
                        message.warning('无有效用户');
                    }
                })
            })
        }
    }





    render() {
        const { loading, orderList, userLoading, userList, userVisible, } = this.state

        const columns = [
            {
                title: '订单编号',
                dataIndex: 'orderId',
                width: 160,
            },
            {
                title: '渠道',
                dataIndex: 'channelName',
                width: 80,
                render: e => {
                    function handldqd(value) {
                        if (value == "000") return "默认"
                        let val = value.split(",")
                        return val[1]
                    }

                    return (
                        handldqd(e)
                    );
                },
            },
            {
                title: '商品名称',
                dataIndex: 'productName',
                width: 160,
            },
            {
                title: '规格',
                dataIndex: 'productInfo',
                width: 120,
                render: e => {
                    return <>{e.spec}</>;
                },
            },
            {
                title: '已付期数',
                width: 110,
                render: e => {
                    return (
                        <>
                            <Tooltip placement="top" title={e.currentPeriods}>
                                <div
                                    style={{
                                        width: 60,
                                    }}
                                >
                                    {e.payedPeriods}/{e.totalPeriods}
                                </div>
                            </Tooltip>
                        </>
                    );
                },
            },
            {
                title: '总租金',
                width: 90,
                render: e => {
                    return <>{e.totalRentAmount}</>;
                },
            },
            {
                title: '已付租金',
                width: 90,
                render: e => {
                    return <>{e.payedRentAmount}</>;
                },
            },
            {
                title: '租赁方式',
                dataIndex: 'isBuyOutOrSend',
                width: 120,
                render: text => (text === '0' ? `默认` : text === '1' ? '租完买断' : '租完即送'),
            },
            {
                title: '下单人姓名',
                width: 120,
                render: e => {
                    return <>{e.realName}</>;
                },
            },
            {
                title: '下单人手机号',
                width: 120,
                render: e => {
                    return <>{e.telephone}</>;
                },
            },
            {
                title: '下单时间',
                dataIndex: 'placeOrderTime',
                width: 120,
            },
            {
                title: '收货人手机号',
                width: '120px',
                render: e => {
                    return <>{e.addressUserPhone}</>;
                },
            },
            {
                title: '起租时间',
                width: '120px',
                render: e => {
                    return <>{e.rentStart}</>;
                },
            },
            {
                title: '归还时间',
                width: '120px',
                render: e => {
                    return <>{e.unrentTime}</>;
                },
            },
            {
                title: '订单状态',
                dataIndex: 'status',
                width: 120,
                render: (_, record) => renderOrderStatus(record),
            },
        ];

        const columnsUser = [
            {
                title: '成员账号',
                dataIndex: 'mobile',
            },
            {
                title: '姓名',
                dataIndex: 'name',
            },
            {
                title: '邮箱地址',
                dataIndex: 'email',
            },
            {
                title: '是否启用',
                dataIndex: 'status',
                render: text => text === 'VALID' ? '启用' : '未启用'
            },
        ]

        const handleOk = () => {
            const { selectOrderItem, selectUserItem } = this.state

            this.setState({
                userVisible: true
            }, () => {
                orderService.allocationClaim({
                    orderId: selectOrderItem,
                    buckstageUserId: selectUserItem.buckstageUserId,
                    name: selectUserItem.name
                }).then(res => {
                    // console.log("认领", res)
                    message.success("认领成功")
                    this.initData();
                    this.setState({
                        userVisible: false
                    })
                })
            })


        };

        const handleCancel = () => {
            this.setState({
                userVisible: false
            })
        };


        return (
            <PageHeaderWrapper>
                <Card>
                    <Spin spinning={loading}>
                        <Button type="primary" onClick={() => this.selectUser()}>关联用户</Button>
                        <Table
                            columns={columns}
                            dataSource={orderList}
                            pagination={false}
                            rowKey={record => record.orderId}
                            rowSelection={{
                                type: 'checkbox',
                                onChange: (selectedRowKeys, selectedRows) => {
                                    console.log(selectedRowKeys)
                                    console.log(selectedRows)

                                    this.setState({
                                        selectOrderItem: selectedRowKeys
                                    })
                                }
                            }}
                            scroll={{ x: scroll }}
                        />
                    </Spin>
                    <Spin spinning={userLoading}>
                        <Modal title="选择用户" visible={userVisible}
                            onOk={handleOk}
                            onCancel={handleCancel}
                            width={1000}
                        >
                            <Table
                                columns={columnsUser}
                                dataSource={userList}
                                pagination={false}
                                rowKey={record => record.id}
                                rowSelection={{
                                    type: 'radio',
                                    onChange: (selectedRowKeys, selectedRows) => {
                                        console.log(selectedRowKeys)
                                        console.log(selectedRows)
                                        this.setState({
                                            selectUserItem: {
                                                buckstageUserId: selectedRows[0].id,
                                                name: selectedRows[0].name,
                                            }
                                        })
                                    }
                                }}
                                scroll={{ y: 400 }}
                            />
                        </Modal>
                    </Spin>
                </Card>
            </PageHeaderWrapper >
        )
    }

}