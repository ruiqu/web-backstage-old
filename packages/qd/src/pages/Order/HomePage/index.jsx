import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import { Card, Tooltip, Button, Form, Input, Select, DatePicker, Spin, Row, Col } from 'antd';
import MyPageTable from '@/components/MyPageTable';
import { onTableData } from '@/utils/utils.js';
import CopyToClipboard from '@/components/CopyToClipboard';
import { getTimeDistance, renderOrderStatus } from '@/utils/utils';
import moment from 'moment';
const { Option } = Select;
const { RangePicker } = DatePicker;
import { routerRedux } from 'dva/router';
import { getCurrentUser } from '@/utils/localStorage';

import { orderCloseStatusMap, orderStatusMap,verfiyStatusMap } from '@/utils/enum';
import { exportCenterHandler } from '../util';
import { UserRating } from 'zwzshared';
@connect(({ order, loading }) => ({
  ...order,
  loading: loading.effects['order/queryOpeOrderByConditionList'],
}))
@Form.create()
export default class HomePage extends Component {
  state = {
    current: 1,
    visible: '',
    orderId: null,
    datas: {},
    record: {},

    expressList: [],
    yunTime: getTimeDistance('month'),
    gBtusua: '04', // 默认状态: 待发货
  };

  columns = [
    {
      title: '订单编号',
      dataIndex: 'orderId',
      width: 150,
      render: (text, record, index) => {
        return (
          <>
            <span style={{ color: "red" }}>{record.children == null ? null : `(共${record.children.length + 1}单) `}</span>
            <span>{text}</span>
          </>
        )
      }
    },
    {
      title: '渠道名称',
      dataIndex: 'channelName',
      width: 120,
      render: text => text === "000" ? "默认" : text,
    },
    {
      title: '商品名称',
      dataIndex: 'productName',
      width: 160,
      // ellipsis: true,
    },

    {
      title: '姓名',
      width: 120,
      render: e => {
        return <>{e.realName}</>;
      },
    },
    {
      title: '手机号',
      width: 120,
      render: e => {
        return <>{e.telephone}</>;
      },
    },
    {
      title: '下单时间',
      dataIndex: 'placeOrderTime',
      width: 120,
    },
    {
      title: '收货人手机号',
      width: '120px',
      render: e => {
        return <>{e.addressUserPhone}</>;
      },
    },
    {
      title: '总租金',
      width: 90,
      render: e => {
        return <>{e.totalRentAmount}</>;
      },
    },
    {
      title: '复审状态',
      dataIndex: 'preVerify',
      width: 120,
      render: (text, record, index) => {
        return <>{verfiyStatusMap[text||0]}-{record.orderRemark}</>;
      },
    },
    {
      title: '订单状态',
      dataIndex: 'status',
      width: 120,
      render: (_, record) => renderOrderStatus(record),
    },
    /* {
      title: '操作',
      width: 120,
      fixed: 'right',
      align: 'center',
      render: (e, record) => {
        return (
          <div>
            <a
              className="primary-color"
              // onClick={() => router.push(`/Order/Details?id=${e.orderId}`)}
              href={`#/Order/HomePage/Details?id=${e.orderId}`}
              target="_blank"
            >
              处理
            </a>
          </div>
        );
      },
    }, */
  ];

  componentDidMount() {
    const { status } = this.props.location.query;
    if (status) {
      this.setState(
        {
          gBtusua: status,
        },
        () => {
          this.props.form.setFieldsValue({
            status,
          });
          this.onList(1, 10, {
            status: this.state.gBtusua,
          });
          // this.getExpressList();
        },
      );
    } else {
      this.onList(1, 10);
      // this.getExpressList();
    }

    this.props.dispatch({
      type: 'order/PlateformChannel',
    });
  }
  onList = (pageNumber, pageSize, data = {}) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'order/queryOpeOrderByConditionList',
      payload: {
        pageSize,
        pageNumber,
        ...data,
      },
    });
  };
  // 重置
  handleReset = e => {
    this.setState(
      {
        gBtusua: '',
      },
      () => {
        this.props.form.resetFields();
        this.props.form.setFieldsValue({
          status: undefined,
        });
        this.handleSubmit(e);
      },
    );
  };
  handleSubmit = e => {
    e && e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (values.createDate && values.createDate.length < 1) {
        values.createTimeStart = '';
        values.createTimeEnd = '';
        this.setState(
          {
            datas: { ...values },
            current: 1,
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      } else if (values.createDate) {
        values.createTimeStart = moment(values.createDate[0]).format('YYYY-MM-DD HH:mm:ss');
        values.createTimeEnd = moment(values.createDate[1]).format('YYYY-MM-DD HH:mm:ss');
        this.setState(
          {
            datas: { ...values },
            current: 1,
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      } else {
        this.setState(
          {
            datas: { ...values },
            current: 1,
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      }
    });
  };

  onexport = () => {
    exportCenterHandler(this, 'exportRentOrder', true, false);
    // const { yunTime } = this.state;
    // if (this.props.form.getFieldValue('createDate')) {
    //   this.props.dispatch({
    //     type: 'order/exportOpeAllUserOrders',
    //     payload: {
    //       createTimeEnd: moment(this.props.form.getFieldValue('createDate')[1]).format(
    //         'YYYY-MM-DD HH:mm:ss',
    //       ),
    //       createTimeStart: moment(this.props.form.getFieldValue('createDate')[0]).format(
    //         'YYYY-MM-DD HH:mm:ss',
    //       ),
    //     },
    //   });
    // } else {
    //   this.props.dispatch({
    //     type: 'order/exportOpeAllUserOrders',
    //     payload: {
    //       createTimeEnd: moment(yunTime[1]).format('YYYY-MM-DD HH:mm:ss'),
    //       createTimeStart: moment(yunTime[0]).format('YYYY-MM-DD HH:mm:ss'),
    //     },
    //   });
    // }
  };

  handleCancel = e => {
    this.setState({
      visible: '',
    });
  };
  //table 页数
  onPage = e => {
    this.setState(
      {
        current: e.current,
      },
      () => {
        const params = {
          ...this.state.datas,
        }
        this.onList(e.current, 10, params);
      },
    );
  };
  onChanges = e => {
    this.setState({
      gBtusua: e,
    });
  };

  // 认领订单
  lingqu = () => {
    // console.log("user",getCurrentUser());
    this.props.dispatch(
      routerRedux.push({
        pathname: `/Order/HomePage/ReceiveOrder`,
      })
    );
  }

  render() {
    const { allList, allTotal, loading, form } = this.props;
    const paginationProps = {
      current: this.state.current,
      pageSize: 10,
      total: allTotal,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / 10)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };

    const user = getCurrentUser();

    const { getFieldDecorator } = form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    return (
      <PageHeaderWrapper>
        <Card bordered={false}>
          <Form layout="inline" onSubmit={this.handleSubmit}>
            <Row>
              <Col span={4}>
                {' '}
                <Form.Item label="">
                  {getFieldDecorator(
                    'orderId',
                    {},
                  )(<Input allowClear placeholder="请输入订单编号" />)}
                </Form.Item>
              </Col>
              <Col span={4}>
                <Form.Item label="">
                  {getFieldDecorator('status', {
                    // initialValue: '',
                  })(
                    <Select
                      placeholder="订单状态"
                      allowClear
                      style={{ width: 180 }}
                      onChange={this.onChanges}
                    >
                      {['01', '11', '04', '05', '06', '07', '08', '09', '10','16','17'].map(value => {
                        return (
                          <Option value={value.toString()} key={value.toString()}>
                            {orderStatusMap[value.toString()]}
                          </Option>
                        );
                      })}
                    </Select>,
                  )}
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="">
                  {getFieldDecorator('createDate', {})(<RangePicker />)}
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item>
                  <Button type="primary" htmlType="submit">
                    查询
                  </Button>
                </Form.Item>
                <Form.Item>
                  <Button htmlType="button" onClick={this.handleReset}>
                    重置
                  </Button>
                </Form.Item>
              </Col>
            </Row>
<div></div>
<p></p>
<p></p>



          </Form>
          <Spin spinning={loading}>
            <MyPageTable
              scroll={true}
              onPage={this.onPage}
              paginationProps={paginationProps}
              dataSource={onTableData(allList)}
              columns={this.columns}
            />
          </Spin>
        </Card>
      </PageHeaderWrapper>
    );
  }
}
