import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import {
  Card,
  message,
  Tooltip,
  Button,
  Form,
  Input,
  Select,
  DatePicker,
  Modal,
  Descriptions,
  Spin,
} from 'antd';
import MyPageTable from '@/components/MyPageTable';
import { onTableData } from '@/utils/utils.js';
import CopyToClipboard from '@/components/CopyToClipboard';
import moment from 'moment';
const { Option } = Select;
const { RangePicker } = DatePicker;
const { TextArea } = Input;
import { router } from 'umi';
import { getTimeDistance } from '@/utils/utils';
@connect(({ order, loading }) => ({
  ...order,
  loading: loading.models.order,
}))
@Form.create()
export default class HomePage extends Component {
  state = {
    current: 1,
    visible: false,
    orderId: null,
    datas: {},
    yunTime: getTimeDistance('month'),
  };
  componentDidMount() {
    this.onList(1, 10);
    console.log(this.props, 'props');
    this.props.dispatch({
      type: 'order/PlateformChannel',
    });
  }
  onList = (pageNumber, pageSize, data = {}) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'order/queryTelephoneAuditOrder',
      payload: {
        pageSize,
        pageNumber,
        ...data,
        examineStatus: (data && data.examineStatus) || null,
      },
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      console.log('Received values of form: ', values);
      if (values.createDate && values.createDate.length < 1) {
        values.createTimeStart = '';
        values.createTimeEnd = '';
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      } else if (values.createDate) {
        values.createTimeStart = moment(values.createDate[0]).format('YYYY-MM-DD HH:mm:ss');
        values.createTimeEnd = moment(values.createDate[1]).format('YYYY-MM-DD HH:mm:ss');
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      } else {
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      }
    });
  };
  showModal = orderId => {
    this.setState({
      visible: true,
      orderId,
    });
  };

  handleOk = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { dispatch } = this.props;
        dispatch({
          type: 'order/telephoneAuditOrder',
          payload: {
            orderId: this.state.orderId,
            remark: values.beizhu,
            // orderType: '01',
          },
          callback: res => {
            this.setState({
              visible: false,
            });
          },
        });
      }
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  //table 页数
  onPage = e => {
    this.setState(
      {
        current: e.current,
      },
      () => {
        this.onList(e.current, 10, this.state.datas);
      },
    );
  };
  onexport = () => {
    const { yunTime } = this.state;
    if (this.props.form.getFieldValue('createDate')) {
      this.props.dispatch({
        type: 'order/exportTelephoneAuditOrders',
        payload: {
          createTimeEnd: moment(this.props.form.getFieldValue('createDate')[1]).format(
            'YYYY-MM-DD HH:mm:ss',
          ),
          createTimeStart: moment(this.props.form.getFieldValue('createDate')[0]).format(
            'YYYY-MM-DD HH:mm:ss',
          ),
        },
      });
    } else {
      this.props.dispatch({
        type: 'order/exportTelephoneAuditOrders',
        payload: {
          createTimeEnd: moment(yunTime[1]).format('YYYY-MM-DD HH:mm:ss'),
          createTimeStart: moment(yunTime[0]).format('YYYY-MM-DD HH:mm:ss'),
        },
      });
    }
  };

  render() {
    const { AuditList, AuditTotal, loading, PlateformList } = this.props;
    const paginationProps = {
      current: this.state.current,
      pageSize: 10,
      total: AuditTotal,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / 10)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };
    let detailsStute = {
      '00': '待平台审批',
      '01': '待商家审批',
      '02': '已审批,可用值',
    };
    const columns = [
      {
        title: '订单编号',
        dataIndex: 'orderId',
        width: '10%',
        render: orderId => <CopyToClipboard text={orderId} />,
      },
      {
        title: '店铺名称',
        dataIndex: 'shopName',
        width: '10%',
        render: shopName => {
          return (
            <>
              <Tooltip placement="top" title={shopName}>
                <div
                  style={{
                    width: 60,
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                    whiteSpace: 'nowrap',
                  }}
                >
                  {shopName}
                </div>
              </Tooltip>
            </>
          );
        },
      },
      {
        title: '渠道来源',
        dataIndex: 'channelName',
        width: '10%',
      },
      {
        title: '商品名称',
        dataIndex: 'productName',
        width: '10%',
        render: productName => {
          return (
            <>
              <Tooltip placement="top" title={productName}>
                <div
                  style={{
                    width: 60,
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                    whiteSpace: 'nowrap',
                  }}
                >
                  {productName}
                </div>
              </Tooltip>
            </>
          );
        },
      },
      {
        title: '已付期数',
        // dataIndex: 'currentPeriods',
        width: '10%',
        render: e => {
          return (
            <>
              <div
                style={{
                  width: 60,
                  overflow: 'hidden',
                  textOverflow: 'ellipsis',
                  whiteSpace: 'nowrap',
                }}
              >
                {e.payedPeriods}/{e.totalPeriods}
              </div>
            </>
          );
        },
      },
      {
        title: '总租金/已付租金',
        width: '10%',
        render: e => {
          return (
            <>
              {e.totalRentAmount}/{e.payedRentAmount}
            </>
          );
        },
      },
      {
        title: '下单人姓名/手机号',
        width: '10%',
        render: e => {
          return (
            <>
              {e.realName}/{e.telephone}
            </>
          );
        },
      },
      {
        title: '下单时间',
        dataIndex: 'placeOrderTime',
        width: '15%',
      },
      {
        title: '电审状态',
        dataIndex: 'examineStatus',
        width: '10%',
        // render: status => {
        //   return (
        //     <>
        //       <div style={{ color: '#FA8C16' }}>待支付</div>
        //       <div style={{ color: '#52C41A' }}>待商家发货</div>
        //       <div style={{ color: '#000000' }}>租用中</div>
        //       <div style={{ color: '#000000' }}>支付中</div>
        //       <div style={{ color: '#F5222D' }}>订单关闭</div>
        //       <div style={{ color: '#FA8C16' }}>待用户确认收货</div>
        //       <div style={{ color: '#FA8C16' }}>待结算</div>
        //       <div style={{ color: '#F5222D' }}>已逾期</div>
        //       <div style={{ color: '#000000' }}>已完成</div>
        //     </>
        //   );
        // },
        render: examineStatus => {
          return <span>{detailsStute[examineStatus]}</span>;
        },
      },
      {
        title: '操作',
        width: '100px',
        render: e => {
          return (
            <div>
              <span
                style={{ cursor: 'pointer', color: '#3F66F5' }}
                onClick={() =>
                  router.push(
                    `/Order/Details?id=${e.orderId}&telephoneAuditOrder=telephoneAuditOrder`,
                  )
                }
              >
                查看
              </span>
              <span
                className="primary-color"
                onClick={() => this.showModal(e.orderId)}
              >
                备注
              </span>
            </div>
          );
        },
      },
    ];
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    return (
      <PageHeaderWrapper>
        <Card bordered={false}>
          <Form layout="inline" onSubmit={this.handleSubmit}>
            <Form.Item>
              {getFieldDecorator('productName', {})(<Input placeholder="请输入商品名称" />)}
            </Form.Item>
            {/* <Form.Item>
              {getFieldDecorator(
                'channelId',
                {},
              )(
                <Select placeholder="渠道来源" style={{ width: 180 }}>
                  <Option value="">全部</Option>

                  {PlateformList.map((item, val) => {
                    return (
                      <Option value={item.channelId} key={val}>
                        {item.channelName}
                      </Option>
                    );
                  })}
                </Select>,
              )}
            </Form.Item> */}
            <Form.Item>
              {getFieldDecorator('userName', {})(<Input placeholder="请输入下单人姓名" />)}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator('telephone', {})(<Input placeholder="请输入下单人手机号" />)}
            </Form.Item>
            <div>
              <Form.Item>
                <Button type="primary" htmlType="submit">
                  查询
                </Button>
              </Form.Item>
              <Form.Item>
                <Button onClick={this.onexport}>导出</Button>
              </Form.Item>
            </div>
            <Form.Item>
              {getFieldDecorator('orderId', {})(<Input placeholder="请输入订单编号" />)}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator('shopName', {})(<Input placeholder="请输入店铺名称" />)}
            </Form.Item>
            <Form.Item>{getFieldDecorator('createDate', {})(<RangePicker />)}</Form.Item>
            <Form.Item>
              {getFieldDecorator('examineStatus', {
                initialValue: null,
              })(
                <Select placeholder="订单状态" style={{ width: 180 }}>
                  <Option value={null}>全部</Option>
                  <Option value={'00'}>待平台审批</Option>
                  <Option value={'02'}>已审批</Option>
                </Select>,
              )}
            </Form.Item>
          </Form>
          <Spin spinning={loading}>
            <MyPageTable
              onPage={this.onPage}
              paginationProps={paginationProps}
              dataSource={onTableData(AuditList)}
              columns={columns}
            />
          </Spin>
        </Card>

        <Modal
          title="备注"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Form>
            <Form.Item label="备注内容" {...formItemLayout}>
              {getFieldDecorator('beizhu', {
                rules: [{ required: true, message: '请输入备注' }],
              })(<TextArea placeholder="请输入" />)}
            </Form.Item>
          </Form>
        </Modal>
      </PageHeaderWrapper>
    );
  }
}
