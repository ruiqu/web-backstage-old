import React, { Component } from 'react';
import Frame from '@/components/Frame';
import { Button } from 'antd';
import { router } from 'umi';
import styles from './index.less';
export default class PasswordStatuspage extends Component {
  state = {
    number: '3',
    timer: null,
  };
  componentDidMount() {
    if (sessionStorage.getItem('result') === 'null') return this.onTimer();
  }
  onPush = () => {
    router.push('/');
  };
  onFail = () => {
    router.push('/user/ChangePassword');
  };
  onTimer = () => {
    this.state.timer = setInterval(() => {
      const time = this.state.number--;
      this.setState({
        time,
      });
      if (this.state.number < 0) {
        clearInterval(this.state.timer);
        this.onPush();
      }
    }, 1000);
  };
  componentWillUnmount() {
    this.onTimer;
  }
  render() {
    const { number, statue } = this.state;
    return (
      <Frame frame={{ width: '599px', height: '566px' }}>
        <div className={styles.Verificationbox}>
          <p className={styles.ChangePassword}>修改登录密码</p>
          <img
            src="https://booleandata-crmmanagement-front.oss-cn-beijing.aliyuncs.com/xian.png"
            alt="xian"
            className={styles.imgs}
          />
          {sessionStorage.getItem('result') === 'null' ? (
            <div>
              <img
                src="https://booleandata-supplier.oss-cn-beijing.aliyuncs.com/duihao.svg"
                alt="duihao"
                className={styles.duihao}
              />
              <p className={styles.Success}>密码修改成功</p>
              <p className={styles.stopwatch}>
                <span>{number}s</span>后，跳转至登录页面
              </p>
              <Button
                type="primary"
                style={{ width: '123px', height: '54px' }}
                onClick={() => this.onPush()}
              >
                立即跳转
              </Button>
            </div>
          ) : (
            <div>
              <img
                src="https://booleandata-supplier.oss-cn-beijing.aliyuncs.com/cuiwu.svg"
                alt="cuiwu"
                className={styles.duihao}
              />
              <p className={styles.Success}>密码修改失败</p>
              <p className={styles.stopwatch}>修改失败，请重新修改</p>
              <Button
                type="primary"
                style={{ width: '123px', height: '54px' }}
                onClick={() => this.onFail()}
              >
                重试
              </Button>
            </div>
          )}
        </div>
      </Frame>
    );
  }
}
