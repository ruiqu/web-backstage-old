import React, { Component } from 'react';
import { Form, Input, Button, Row, Col, message, Icon } from 'antd';
import { router } from 'umi';
import { connect } from 'dva';
import styles from './index.less';
import UserService from '@/services/login'

@connect(() => ({}))
class ResetForm extends Component {
  state = {
    isBotton: true,
    number: '60',
    codeData: {}
  };
  
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log(values, 'values');
        const { codeData } = this.state
        UserService.restPassword({
          ...values,
          ...codeData
        }).then(res => {
          message.success('重置密码成功')
          router.push('/user/login')
        })
      }
    });
  };

  onEmailCode = () => {
    const { form, } = this.props;
    form.validateFields(['mobile'], (err, values) => {
      if (!err) {
        this.setState({
          isBotton: false,
        });
        this.state.timer = setInterval(() => {
          const time = this.state.number--;
          this.setState({
            time: time,
          });
          if (this.state.number < 1) {
            clearInterval(this.state.timer);
            this.setState({
              number: '60',
            });
            return this.setState({
              isBotton: true,
            });
          }
        }, 1000);
        UserService.sendValidateCode({
          code: 123,
          mobile: values.mobile
        }).then(res => {
          this.setState({
            codeData: res || {}
          });
          message.success('发送验证成功')
        })
      }
    });
  };
  onError = res => {
    message.error(res);
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div className={styles.resetFrom}>
        <Form onSubmit={this.handleSubmit} className="login-form">
          <div className={styles.loginInput}>
            <Form.Item>
              {getFieldDecorator('mobile', {
                rules: [
                  { required: true, message: '请输入手机号' },
                  {
                    pattern: /^1\d{10}$/,
                    message: '手机号格式错误！',
                  },
                  ],
              })(
                <Input
                  prefix={<Icon type="mobile" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  placeholder="请输入手机号"
                />,
              )}
            </Form.Item>

            <div className={styles.verification}>
              <Form.Item>
                {getFieldDecorator('code', {
                  rules: [{ required: true, message: '请输入验证码' }],
                })(<Input className={styles.verificationInput}
                          prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
                          placeholder="请输入验证码" />)}
                {this.state.isBotton ? (
                  <Button
                    style={{ width: '118px', marginLeft: '11px', height: '40px' }}
                    onClick={this.onEmailCode}
                  >
                    获取验证码
                  </Button>
                ) : (
                  <Button
                    style={{ width: '118px', marginLeft: '11px', height: '40px' }}
                    disabled={true}
                  >
                    {this.state.number}s
                  </Button>
                )}
              </Form.Item>
            </div>
            <Form.Item>
              {getFieldDecorator('newPassword', {
                rules: [{ required: true, message: '请输入新密码' }],
              })(
                <Input
                  type="password"
                  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  placeholder="请输入新密码"
                />,
              )}
            </Form.Item>
          </div>
          <div style={{ display: 'flex', justifyContent: 'space-between', marginLeft: 40 }}>
            <Button className={styles.button} type="primary" htmlType="submit">
              {' '}
              确认{' '}
            </Button>
            <Button
              className={styles.button}
              type="link"
              onClick={() => router.push('/user/login')}
            >
              使用已有帐户登录
            </Button>
          </div>
        </Form>
      </div>
    );
  }
}
const WrappedNormalResetForm = Form.create({ name: 'normal_login' })(ResetForm);
export default WrappedNormalResetForm;
