import React, { Component } from 'react';
import { connect } from 'dva';
import styles from './style.less';
import LoginForm from './components/LoginForm';
import Frame from '@/components/Frame';
import { router } from 'umi';
import { setAuthority } from '@/utils/authority';
class Login extends Component {
  
  state = {
    title: '登录',
  }
  
  componentDidMount() {
    setAuthority(['admin']);
  }
  
  
  changeTitle = (val) => {
    this.setState({
      title: val
    });
  }
  
  render() {
    const { title } = this.state
    return (
      <Frame
        frame={{
          borderRadius: '10px',
          backgroundColor: 'transparent',
          boxShadow: 'none',
        }}
      >
        <div className={styles.layouts}>
          <div className={styles.frameleft}>
            <img
              style={{
                width: 535,
                height: 546,
              }}
              src="./login_bg.png"
            />
          </div>
          <div className={styles.frameright}>
            <div>
              <div className={styles.denglu}>{title}</div>
            </div>
            <LoginForm changeTitle={this.changeTitle} />
          </div>
        </div>
      </Frame>
    );
  }
}

export default Login;
