import { message } from 'antd';
import { routerRedux } from 'dva/router';
import * as servicesApi from '../services';

export default {
  spacename: 'compensate',
  state: {
    list: [],
    currentId: null,
  },
  effects: {
    *getShopCompensateRulesByShopId(_, { call, put }) {
      const res = yield call(servicesApi.getShopCompensateRulesByShopId);
      if (res) {
        yield put({
          type: 'saveList',
          payload: res.data,
        });
      }
    },

    *saveShopCompensateRentRule({ payload }, { call, put }) {
      let work = servicesApi.saveShopCompensateRentRule;
      if (payload.id) {
        work = servicesApi.updateShopCompensateRentRulesById;
      }
      const res = yield call(work, payload);
      if (res) {
        message.success('保存成功');
        yield put(routerRedux.push('/goods/compensateList'));
      }
    },

    *delShopCompensateRentRulesById({ payload }, { call, put }) {
      const res = yield call(servicesApi.delShopCompensateRentRulesById, payload);
      if (res) {
        yield put({
          type: 'getShopCompensateRulesByShopId',
        });
        message.success('删除成功');
      }
    },
  },
  reducers: {
    saveList(state, { payload }) {
      return { ...state, list: payload };
    },
    setCurrentId(state, { payload }) {
      return { ...state, currentId: payload };
    },
  },
};
