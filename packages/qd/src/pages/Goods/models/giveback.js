import { message } from 'antd';
import { routerRedux } from 'dva/router';
import * as servicesApi from '../services';

export default {
  spacename: 'giveback',
  state: {
    list: [],
    currentId: null,
  },
  effects: {
    *selectShopGiveBackAddressListByShopId(_, { call, put }) {
      const res = yield call(servicesApi.selectShopGiveBackAddressListByShopId);
      if (res) {
        yield put({
          type: 'saveList',
          payload: res.data,
        });
      }
    },

    *saveShopGiveBackAddress({ payload }, { call, put }) {
      let work = servicesApi.saveShopGiveBackAddress;
      if (payload.id) {
        work = servicesApi.updateShopGiveBackAddressByid;
      }
      const res = yield call(work, payload);
      if (res) {
        message.success('保存成功');
        yield put(routerRedux.push('/goods/givebackList'));
      }
    },

    *delShopGiveBackAddressByid({ payload }, { call, put }) {
      const res = yield call(servicesApi.delShopGiveBackAddressByid, payload);
      if (res) {
        yield put({
          type: 'selectShopGiveBackAddressListByShopId',
        });
        message.success('删除成功');
      }
    },
  },
  reducers: {
    saveList(state, { payload }) {
      return { ...state, list: payload };
    },
    setCurrentId(state, { payload }) {
      return { ...state, currentId: payload };
    },
  },
};
