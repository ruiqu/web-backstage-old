import request from '@/utils/request';

export const selectBusinessPrdouct = body =>
  request('/hzsx/product/select/hzsxPrdouct', {
    method: 'POST',
    body,
  });

export const findCategories = () => request('/hzsx/shop/findCategories');

export const busUpdateProductByRecycle = body =>
  request('/hzsx/product/busUpdateProductByRecycle', {
    method: 'GET',
    body,
  });

export const busUpdateProductByRecycleDel = body =>
  request('/hzsx/product/busUpdateProductByRecycleDel', {
    method: 'GET',
    body,
  });

export const selectShopRuleAndGiveBackByShopId = () =>
  request('/hzsx/shop/selectShopRuleAndGiveBackByShopId');

export const busInsertProduct = body =>
  request('/hzsx/product/busInsertProduct', {
    method: 'POST',
    body,
  });

export const updateBusProduct = body =>
  request('/hzsx/product/updateBusProduct', {
    method: 'POST',
    body,
  });

export const selectProductEdit = body =>
  request('/hzsx/product/selectProductEdit', {
    method: 'GET',
    body,
  });

export const busUpdateProductByRecycleRecover = body =>
  request('/hzsx/product/busUpdateProductByRecycleRecover', {
    method: 'GET',
    body,
  });

export const getShopRentRulesByShopId = () =>
  request('/hzsx/shopRule/getShopRentRulesByShopId', {
    method: 'GET',
  });

export const delShopRentRulesByshopId = body =>
  request('/hzsx/shopRule/delShopRentRulesByshopId', {
    method: 'GET',
    body,
  });

export const saveShopRentRule = body =>
  request('/hzsx/shopRule/saveShopRentRule', {
    method: 'POST',
    body,
  });

export const updateShopRentRulesById = body =>
  request('/hzsx/shopRule/updateShopRentRulesById', {
    method: 'POST',
    body,
  });

export const getShopCompensateRulesByShopId = () =>
  request('/hzsx/shopRule/getShopCompensateRulesByShopId', {
    method: 'GET',
  });

export const delShopCompensateRentRulesById = body =>
  request('/hzsx/shopRule/delShopCompensateRentRulesById', {
    method: 'GET',
    body,
  });

export const saveShopCompensateRentRule = body =>
  request('/hzsx/shopRule/saveShopCompensateRentRule', {
    method: 'POST',
    body,
  });

export const updateShopCompensateRentRulesById = body =>
  request('/hzsx/shopRule/updateShopCompensateRentRulesById', {
    method: 'POST',
    body,
  });

export const selectShopGiveBackAddressListByShopId = () =>
  request('/hzsx/shopRule/selectShopGiveBackAddressListByShopId', {
    method: 'GET',
  });

export const delShopGiveBackAddressByid = body =>
  request('/hzsx/shopRule/delShopGiveBackAddressByid', {
    method: 'GET',
    body,
  });

export const saveShopGiveBackAddress = body =>
  request('/hzsx/shopRule/saveShopGiveBackAddress', {
    method: 'POST',
    body,
  });

export const updateShopGiveBackAddressByid = body =>
  request('/hzsx/shopRule/updateShopGiveBackAddressByid', {
    method: 'POST',
    body,
  });

export const busUpdateProductByDown = body =>
  request('/hzsx/product/busUpdateProductByDown', {
    method: 'GET',
    body,
  });

export const busUpdateProductByUp = body =>
  request('/hzsx/product/busUpdateProductByUp', {
    method: 'GET',
    body,
  });
