/**
 * Ant Design Pro v4 use `@ant-design/pro-layout` to handle Layout.
 * You can view component api by:
 * https://github.com/ant-design/ant-design-pro-layout
 */
import ProLayout, { DefaultFooter } from '@ant-design/pro-layout';
import React, { useEffect, useState } from 'react';
import { Link } from 'umi';
import { connect } from 'dva';
import { Icon, Result, Button, Alert, Modal } from 'antd';
import { formatMessage } from 'umi-plugin-react/locale';
import Authorized from '@/utils/Authorized';
import RightContent from '@/components/GlobalHeader/RightContent';
import NoticeService from './services';
import { isAntDesignPro, getAuthorityFromRouter } from '@/utils/utils';
import logo from '../assets/logo.svg';
import TextScroll from 'react-textscroll';

const noMatch = (
  <Result
    status={403}
    title="403"
    subTitle="Sorry, you are not authorized to access this page."
    extra={
      <Button type="primary">
        <Link to="/user/login">Go Login</Link>
      </Button>
    }
  />
);

/**
 * use Authorized check all menu item
 */
const menuDataRender = menuList =>
menuList.map(item => {
  const localItem = { ...item, children: item.children ? menuDataRender(item.children) : [] };
  
    return Authorized.check(item.authority, localItem, null);
  });

// const defaultFooterDom = (
//   <DefaultFooter
//     copyright="2019 蚂蚁金服体验技术部出品"
//     links={[
//       {
//         key: 'Ant Design Pro',
//         title: 'Ant Design Pro',
//         href: 'https://pro.ant.design',
//         blankTarget: true,
//       },
//       {
//         key: 'github',
//         title: <Icon type="github" />,
//         href: 'https://github.com/ant-design/ant-design-pro',
//         blankTarget: true,
//       },
//       {
//         key: 'Ant Design',
//         title: 'Ant Design',
//         href: 'https://ant.design',
//         blankTarget: true,
//       },
//     ]}
//   />
// );

const footerRender = () => {
  // if (!isAntDesignPro()) {
  //   return defaultFooterDom;
  // }

  return (
    <>
      {/* {defaultFooterDom} */}
      <div
        style={{
          padding: '0px 24px 24px',
          textAlign: 'center',
        }}
      >
        <a href="https://www.netlify.com" target="_blank" rel="noopener noreferrer">
          <img
            src="https://www.netlify.com/img/global/badges/netlify-color-bg.svg"
            width="82px"
            alt="netlify logo"
          />
        </a>
      </div>
    </>
  );
};

const BasicLayout = props => {
  const {
    dispatch,
    children,
    settings,
    location = {
      pathname: '/',
    },
  } = props;
  /**
   * constructor
   */
  const [detail, setDetail] = useState(null);
  const [title, setTitle] = useState(undefined);
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    return;
    NoticeService.selectNotice({}).then(res => {
      setDetail(res.detail);
      setTitle(res.title);
    });
  }, []);
  /**
   * init variables
   */
  const showModal = () => {
    setVisible(true);
  };

  const handleOk = e => {
    setVisible(false);
  };

  const handleCancel = e => {
    setVisible(false);
  };
  const handleMenuCollapse = payload => {
    if (dispatch) {
      dispatch({
        type: 'global/changeLayoutCollapsed',
        payload,
      });
    }
  }; // get children authority

  const authorized = getAuthorityFromRouter(props.route.routes, location.pathname || '/') || {
    authority: undefined,
  };
  return (
    <>
      {title && (
        <Alert
          showIcon={false}
          message={
            <div
              style={{
                textAlign: 'center',
              }}
            >
              <span
                onClick={showModal}
                style={{
                  cursor: 'pointer',
                }}
              >
                <TextScroll mode="horizontal" text={['公告：' + title]} speed={5000} />
              </span>
            </div>
          }
          banner
        />
      )}
      <ProLayout
        logo={
          'favicon.png'
        }
        menuHeaderRender={(logoDom, titleDom) => (
          <Link to="/">
            {logoDom}
            {titleDom}
          </Link>
        )}
        onCollapse={handleMenuCollapse}
        menuItemRender={(menuItemProps, defaultDom) => {
          if (menuItemProps.isUrl || menuItemProps.children || !menuItemProps.path) {
            return defaultDom;
          }

          return <Link to={menuItemProps.path}>{defaultDom}</Link>;
        }}
        breadcrumbRender={(routers = []) => [
          {
            path: '/',
            breadcrumbName: formatMessage({
              id: 'menu.home',
              defaultMessage: 'home',
            }),
          },
          ...routers,
        ]}
        itemRender={(route, params, routes, paths) => {
          let breadcrumbName = route.breadcrumbName;
          const first = routes.indexOf(route) === 0;
          const third = routes.indexOf(route) === 2;
          
          const hides = [
            "买断账单",
            "购买账单",
            "常规账单",
          ]
          if (hides.includes(breadcrumbName)) {
            return null
          }

          if (first) {
            return (
              <Link className="lz-breadcrumb-click" to={paths.join('/')}>
                {breadcrumbName}
              </Link>
            );
          }
          if (third) {
            return (
              <Link className="lz-breadcrumb-click" to={route.path}>
                {breadcrumbName}
              </Link>
            );
          }
          breadcrumbName = /\/add$/g.test(route.path) ? '新增' : breadcrumbName;
          return <span>{breadcrumbName}</span>;
        }}
        // footerRender={footerRender}
        menuDataRender={menuDataRender}
        formatMessage={formatMessage}
        rightContentRender={() => <RightContent />}
        {...props}
        {...settings}
      >
        <Authorized authority={authorized.authority} noMatch={noMatch}>
          {children}
        </Authorized>
      </ProLayout>
      <Modal title="公告" visible={visible} onOk={handleOk} onCancel={handleCancel} footer={false}>
        <div dangerouslySetInnerHTML={{ __html: detail }} />
      </Modal>
    </>
  );
};

export default connect(({ global, settings }) => ({
  collapsed: global.collapsed,
  settings,
}))(BasicLayout);
