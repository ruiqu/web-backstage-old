import { parse } from 'querystring';
import pathRegexp from 'path-to-regexp';
import moment from 'moment';
import { routerRedux } from 'dva/router';
import { uploadUrl } from '../config';
import { getToken } from '@/utils/localStorage';

/* eslint no-useless-escape:0 import/prefer-default-export:0 */
const reg = /(((^https?:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+(?::\d+)?|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)$/;
export const isUrl = path => reg.test(path);
export const isAntDesignPro = () => {
  if (ANT_DESIGN_PRO_ONLY_DO_NOT_USE_IN_YOUR_PRODUCTION === 'site') {
    return true;
  }

  return window.location.hostname === 'preview.pro.ant.design';
}; // 给官方演示站点用，用于关闭真实开发环境不需要使用的特性

export const isAntDesignProOrDev = () => {
  const { NODE_ENV } = process.env;

  if (NODE_ENV === 'development') {
    return true;
  }

  return isAntDesignPro();
};
export const getPageQuery = () => parse(window.location.href.split('?')[1]);
/**
 * props.route.routes
 * @param router [{}]
 * @param pathname string
 */

export const getAuthorityFromRouter = (router = [], pathname) => {
  const authority = router.find(
    ({ routes, path = '/' }) =>
      (path && pathRegexp(path).exec(pathname)) ||
      (routes && getAuthorityFromRouter(routes, pathname)),
  );
  if (authority) return authority;
  return undefined;
};
export const getRouteAuthority = (path, routeData) => {
  let authorities;
  routeData.forEach(route => {
    // match prefix
    if (pathRegexp(`${route.path}/(.*)`).test(`${path}/`)) {
      if (route.authority) {
        authorities = route.authority;
      } // exact match

      if (route.path === path) {
        authorities = route.authority || authorities;
      } // get children authority recursively

      if (route.routes) {
        authorities = getRouteAuthority(path, route.routes) || authorities;
      }
    }
  });
  return authorities;
};
export function getTimeDistance(type) {
  const now = new Date();
  const oneDay = 1000 * 60 * 60 * 24;

  if (type === 'today') {
    now.setHours(0);
    now.setMinutes(0);
    now.setSeconds(0);
    return [moment(now), moment(now.getTime() + (oneDay - 1000))];
  }

  if (type === 'week') {
    let day = now.getDay();

    const beginTime = now.getTime();

    return [moment(beginTime - (6 * oneDay - 1000)), moment(beginTime)];
  }

  if (type === 'month') {
    let day = now.getDay();

    const beginTime = now.getTime();

    return [moment(beginTime - (30 * oneDay - 1000)), moment(beginTime)];
  }

  const year = now.getFullYear();
  return [moment(`${year}-01-01 00:00:00`), moment(`${year}-12-31 23:59:59`)];
}
export function onTableData(e) {
  if (!!e) {
    const customData =
      e === []
        ? []
        : e.map((item, sign) => {
            const newsItem = { ...item };
            const keys = sign + 1;
            newsItem.key = keys;
            return newsItem;
          });
    return customData;
  } else {
    return [];
  }
}
export function getParam(name) {
  const search = document.location.href;
  const pattern = new RegExp('[?&]' + name + '=([^&]+)', 'g');
  const matcher = pattern.exec(search);
  let items = null;
  if (matcher !== null) {
    try {
      items = decodeURIComponent(decodeURIComponent(matcher[1]));
    } catch (e) {
      try {
        items = decodeURIComponent(matcher[1]);
      } catch (e) {
        items = matcher[1];
      }
    }
  }
  return items;
}

export function goToRouter(dispatch, path) {
  dispatch(routerRedux.push(path));
}

export function downloadFile(href, filename = '') {
  if (href) {
    const download = document.createElement('a');
    download.download = filename;
    download.style.display = 'none';
    download.href = href;
    document.body.appendChild(download);
    download.click();
    document.body.removeChild(download);
  } else {
    throw Error('下载链接不正确');
  }
}
/**
 * 图片压缩
 * @param {object} file :图片文件信息
 * @param {string} width :宽
 * @param {string} height :高
 */

export const compression = file => {
  return new Promise(resolve => {
    const reader = new FileReader(); // 创建 FileReader
    reader.onload = ({ target: { result: src } }) => {
      const image = new Image(); // 创建 img 元素
      image.onload = async () => {
        const canvas = document.createElement('canvas'); // 创建 canvas 元素
        canvas.width = image.width;
        canvas.height = image.height;
        canvas.getContext('2d').drawImage(image, 0, 0, image.width, image.height); // 绘制 canvas
        const canvasURL = canvas.toDataURL('image/jpeg', 0.5);
        const buffer = atob(canvasURL.split(',')[1]);
        let length = buffer.length;
        const bufferArray = new Uint8Array(new ArrayBuffer(length));
        while (length--) {
          bufferArray[length] = buffer.charCodeAt(length);
        }
        const miniFile = new File([bufferArray], file.name, { type: 'image/jpeg' });
        resolve({
          file: miniFile,
          origin: file,
          beforeSrc: src,
          afterSrc: canvasURL,
          beforeKB: Number((file.size / 1024).toFixed(2)),
          afterKB: Number((miniFile.size / 1024).toFixed(2)),
        });
      };
      image.src = src;
    };
    reader.readAsDataURL(file);
  });
};

export const bfUploadFn = param => {
  // compression(param.file).then(res => {
  //   const serverURL = uploadUrl;
  //   const xhr = new XMLHttpRequest();
  //   const fd = new FormData();
  //   const successFn = () => {
  //     // 假设服务端直接返回文件上传后的地址
  //     // 上传成功后调用param.success并传入上传后的文件地址
  //     param.success({
  //       url: JSON.parse(xhr.responseText).data,
  //     });
  //   };

  //   const progressFn = event => {
  //     // 上传进度发生变化时调用param.progress
  //     param.progress((event.loaded / event.total) * 100);
  //   };

  //   const errorFn = () => {
  //     // 上传发生错误时调用param.error
  //     param.error({
  //       msg: 'unable to upload.',
  //     });
  //   };

  //   xhr.upload.addEventListener('progress', progressFn, false);
  //   xhr.addEventListener('load', successFn, false);
  //   xhr.addEventListener('error', errorFn, false);
  //   xhr.addEventListener('abort', errorFn, false);

  //   fd.append('file', res.file);
  //   xhr.open('POST', serverURL, true);
  //   xhr.setRequestHeader('token', getToken());
  //   xhr.send(fd);
  // });
  const serverURL = uploadUrl;
    const xhr = new XMLHttpRequest();
    const fd = new FormData();
    const successFn = () => {
      // 假设服务端直接返回文件上传后的地址
      // 上传成功后调用param.success并传入上传后的文件地址
      param.success({
        url: JSON.parse(xhr.responseText).data,
      });
    };

    const progressFn = event => {
      // 上传进度发生变化时调用param.progress
      param.progress((event.loaded / event.total) * 100);
    };

    const errorFn = () => {
      // 上传发生错误时调用param.error
      param.error({
        msg: 'unable to upload.',
      });
    };

    xhr.upload.addEventListener('progress', progressFn, false);
    xhr.addEventListener('load', successFn, false);
    xhr.addEventListener('error', errorFn, false);
    xhr.addEventListener('abort', errorFn, false);

    fd.append('file', param.file);
    xhr.open('POST', serverURL, true);
    xhr.setRequestHeader('token', getToken());
    xhr.send(fd);
};

/**
 * 从接口返回的数据中找出详细地址
 * @param {*} addressVO
 */
export const addressHandler = addressVO => {
  if (Object.prototype.toString.call(addressVO) !== '[object Object]') return '';
  const { provinceStr, cityStr, areaStr, street } = addressVO;

  if (!street) return `${provinceStr}${cityStr}${areaStr}`; // 街道信息不存在的话，那么街道信息的优先级放高
  let finalStr = street;
  if (!finalStr.includes(areaStr)) {
    finalStr = `${areaStr}${finalStr}`;
  }
  if (!finalStr.includes(cityStr)) {
    finalStr = `${cityStr}${finalStr}`;
  }
  if (!finalStr.includes(provinceStr)) {
    finalStr = `${provinceStr}${finalStr}`;
  }
  return finalStr;
};

// 做减法
export const makeSub = (arg1, arg2) => {
  let r1; // 第一个数字的小数点位数
  let r2; // 第二个数字的小数点位数
  let m;

  // 首先计算出两个数字的小数点位数
  try {
    r1 = arg1.toString().split('.')[1].length;
  } catch (e) {
    r1 = 0;
  }
  try {
    r2 = arg2.toString().split('.')[1].length;
  } catch (e) {
    r2 = 0;
  }

  const large = Math.max(r1, r2);
  m = Math.pow(10, large); // 根据最长的小数点为计算出所需要乘的倍数
  return ((arg1 * m - arg2 * m) / m).toFixed(large); // 变成整数后在进行相减操作，接着除以倍数，然后在保留指定小数位
};


/**
 * 渲染订单状态，如果订单已关闭的话，那么把关单原因也给展示出来
 * @param {*} record 
 */
 export const renderOrderStatus = record => {
  if (!record || !record.status) return ""
  const { status } = record
  
  const orderStatusMap = {
    '01': '待支付',
    '02': '支付中',
    '03': '已支付申请关单',
    '11': '待人审',
    '04': '待发货',
    '05': '待确认收货',
    '06': '租用中',
    '07': '待结算',
    '08': '结算待支付',
    '09': '订单完成',
    '10': '交易关闭',
    '12': '待归还',
    '16':'机审拒绝',
    '17': '待审批',
  };

  const orderCloseStatusMap = {
    '01': '未支付用户主动申请',
    '02': '支付失败',
    '03': '超时未支付',
    '04': '已支付用户主动申请',
    '05': '风控拒绝',
    '06': '商家关闭(客户要求)',
    '07': '商家风控关闭订单',
    '08': '商家超时发货',
    '09': '平台关闭订单',
    '11': '平台风控关闭订单',
  };

  if (status === "10") {
    return (
      <span>
        {orderStatusMap[status]}({orderCloseStatusMap[record.closeType]})
      </span>
    );
  }
  return <span>{orderStatusMap[status]}</span>
}

export const convertCurrency = money => {
  //汉字的数字
  let cnNums = new Array('零', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖');
  //基本单位
  let cnIntRadice = new Array('', '拾', '佰', '仟');
  //对应整数部分扩展单位
  let cnIntUnits = new Array('', '万', '亿', '兆');
  //对应小数部分单位
  let cnDecUnits = new Array('角', '分', '毫', '厘');
  //整数金额时后面跟的字符
  let cnInteger = '整';
  //整型完以后的单位
  let cnIntLast = '元';
  //最大处理的数字
  let maxNum = 999999999999999.9999;
  //金额整数部分
  let integerNum;
  //金额小数部分
  let decimalNum;
  //输出的中文金额字符串
  let chineseStr = '';
  //分离金额后用的数组，预定义
  let parts;
  if (money == '') {
    return '';
  }
  money = parseFloat(money);
  if (money >= maxNum) {
    //超出最大处理数字
    return '';
  }
  if (money == 0) {
    chineseStr = cnNums[0] + cnIntLast + cnInteger;
    return chineseStr;
  }
  //转换为字符串
  money = money.toString();
  if (money.indexOf('.') == -1) {
    integerNum = money;
    decimalNum = '';
  } else {
    parts = money.split('.');
    integerNum = parts[0];
    decimalNum = parts[1].substr(0, 4);
  }
  //获取整型部分转换
  if (parseInt(integerNum, 10) > 0) {
    let zeroCount = 0;
    let IntLen = integerNum.length;
    for (let i = 0; i < IntLen; i++) {
      let n = integerNum.substr(i, 1);
      let p = IntLen - i - 1;
      let q = p / 4;
      let m = p % 4;
      if (n == '0') {
        zeroCount++;
      } else {
        if (zeroCount > 0) {
          chineseStr += cnNums[0];
        }
        //归零
        zeroCount = 0;
        chineseStr += cnNums[parseInt(n)] + cnIntRadice[m];
      }
      if (m == 0 && zeroCount < 4) {
        chineseStr += cnIntUnits[q];
      }
    }
    chineseStr += cnIntLast;
  }
  //小数部分
  if (decimalNum != '') {
    let decLen = decimalNum.length;
    for (let i = 0; i < decLen; i++) {
      let n = decimalNum.substr(i, 1);
      if (n != '0') {
        chineseStr += cnNums[Number(n)] + cnDecUnits[i];
      }
    }
  }
  if (chineseStr == '') {
    chineseStr += cnNums[0] + cnIntLast + cnInteger;
  } else if (decimalNum == '') {
    chineseStr += cnInteger;
  }
  return chineseStr;
}

/**
 * 对antd timepicker所选择的时间做一个转换，接口所需的时间格式有些不同
 * @param {Object} originalData : 传参对象
 */
export function formatTime2ApiNeed(originalData) {
  if (Object.prototype.toString.call(originalData) !== "[object Object]") {
    console.error("入参必须为对象")
    return {}
  }

  const { times } = originalData
  if (times && times.length) { // 说明用户交互选中了时间
    const time1 = times[0] // 开始时间
    const time2 = times[1] // 结束时间
    time1 && (originalData.startTime = time1.format("YYYY-MM-DD 00:00:00"))
    time2 && (originalData.endTime = time2.format("YYYY-MM-DD 23:59:59"))
  }

  return originalData
}

/**
 * 根据设计稿所设计的尺寸进行响应式变化
 * @param {*} pxNum 
 * @returns 
 */
export const responsivePx = pxNum => {
  const designWidth = 1366
  const screenWidth = document.body.clientWidth
  const result = (screenWidth / designWidth) * pxNum
  return result
}

/**
 * 字符串的判空显示
 * @param {*} val 
 * @param {*} placeholder 
 * @returns 
 */
export const strUi = (val, placeholder="-") => {
  if (val == undefined || val === "") return placeholder
  return val
}