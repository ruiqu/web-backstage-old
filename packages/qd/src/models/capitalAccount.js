export const dps = 10

export const dpn = 1

export const defaultQuery = {
  pageNumber: dpn,
  pageSize: dps,
}

export default {
  namespace: "capitalAccountModel",

  state: {
    listQuery: defaultQuery, // 资金账户明细列表的传参数据
  },

  reducers: {
    /**
     * 修改传参数据
     * @param {*} state 
     * @param {*} param1 
     * @returns 
     */
    mutListQuery(state, { payload }) {
      return {
        ...state,
        listQuery: {
          ...state.listQuery,
          ...payload
        }
      }
    },


  }
}