import { stringify } from 'querystring';
import { router } from 'umi';
import {
  Login,
  getFakeCaptcha,
  homeData,
  queryOrderReport,
  selectProductCounts,
} from '@/services/login';
import { setAuthority } from '@/utils/authority';
import { getPageQuery } from '@/utils/utils';
import { reloadAuthorized } from '@/utils/Authorized';
import { setToken, setShopId, setShopAdminId, setCurrentUser } from '../utils/localStorage';

const Model = {
  namespace: 'login',
  state: {
    status: undefined,
  },
  effects: {
    *login({ payload }, { call, put }) {
      const response = yield call(Login, payload);
      sessionStorage.setItem('token', response.data.token);
      localStorage.setItem('token', response.data.token);
      localStorage.setItem('shopId', response.data.shopId);

      localStorage.setItem('email', payload.mobile);
      //setCurrentUser(response.data.backUserDto)
      setAuthority(["订单管理","订单列表","财务管理","渠道管理"]);
      router.push('/Order/HomePage');
    },
    *home({ payload, callback }, { call, put }) {
      const response = yield call(homeData, payload);
      if (callback && typeof callback === 'function') {
        callback(response);
      }
    },
    *selectProduct({ payload, callback }, { call, put }) {
      const response = yield call(selectProductCounts, payload);
      if (callback && typeof callback === 'function') {
        callback(response);
      }
    },
    *queryOrder({ payload, callback }, { call, put }) {
      const response = yield call(queryOrderReport, payload);
      if (callback && typeof callback === 'function') {
        callback(response);
      }
    },

    *getCaptcha({ payload }, { call }) {
      yield call(getFakeCaptcha, payload);
    },

    logout() {
      const { redirect } = getPageQuery(); // Note: There may be security issues, please note

      if (window.location.pathname !== '/user/login' && !redirect) {
        router.replace({
          pathname: '/user/login',
          search: stringify({
            redirect: window.location.href,
          }),
        });
      }
    },
  },
  reducers: {
    changeLoginStatus(state, { payload }) {
      console.log(payload);
      setToken(payload.token);
      setAuthority(payload.token);
      setCurrentUser(payload.user);
      return { ...state, status: payload.status, type: payload.type };
    },
  },
};
export default Model;
