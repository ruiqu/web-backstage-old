import * as servicesApi from '@/services/message';

export default {
  spacename: 'messageIndex',
  state: {
    list: [],
  },
  effects: {
    // list
    *getList({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.queryOpeOrderByCondition, payload);

      yield put({
        type: 'saveList',
        payload: res,
      });

      if (callback) callback(res);
    },

    //添加list
    *addList({ payload, callback }, { put, call }) {
      let res = yield call(servicesApi.saveMessageTemplate, payload);
      if (callback) callback(res);
    },
    // //删除list
    *deleteList({ payload, callback }, { call }) {
      let res = yield call(servicesApi.delMessageTemplate, payload);
      if (callback) callback(res);
    },
    //编辑list
    *updateList({ payload, callback }, { call }) {
      let res = yield call(servicesApi.updateMessageTemplate, payload);
      if (callback) callback(res);
    },
    // //获取上架信息
    // *getChannel({ payload, callback }, { call, put }) {
    //   const response = yield call(servicesApi.getChannel, payload);
    //   if (response.data.code == 1) {
    //     callback && callback(response.data.data);
    //   }
    // },
  },
  reducers: {
    saveList(state, { payload }) {
      console.log();
      // let _state = JSON.parse(JSON.stringify(state));
      // _state.list = (payload.data && payload.data.records) || [];
      // return _state;
      return {
        ...state,
        list: payload.data.records,
      };
    },
    /* initProps(state, payload) {
      let _state = JSON.parse(JSON.stringify(state));
      _state.list = [];
      return _state;
    }, */
  },
};
