module.exports = {
  navTheme: 'light',
  primaryColor: '#E83F40',
  layout: 'sidemenu',
  contentWidth: 'Fluid',
  fixedHeader: false,
  autoHideHeader: false,
  fixSiderbar: false,
  menu: {
    disableLocal: false,
  },
  title: '渠道后台',
  pwa: true,
  iconfontUrl: '',
  collapse: true,
};
