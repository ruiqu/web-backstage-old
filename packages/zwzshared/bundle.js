'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var React = require('react');
var antd = require('antd');
var PropTypes = require('prop-types');
var baseRequest = require('@/utils/lz-request');
var localStorage$1 = require('@/utils/localStorage');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var React__default = /*#__PURE__*/_interopDefaultLegacy(React);
var PropTypes__default = /*#__PURE__*/_interopDefaultLegacy(PropTypes);
var baseRequest__default = /*#__PURE__*/_interopDefaultLegacy(baseRequest);

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);

    if (enumerableOnly) {
      symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
    }

    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread2(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(Object(source), true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(Object(source)).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

function _isNativeReflectConstruct() {
  if (typeof Reflect === "undefined" || !Reflect.construct) return false;
  if (Reflect.construct.sham) return false;
  if (typeof Proxy === "function") return true;

  try {
    Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
    return true;
  } catch (e) {
    return false;
  }
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _possibleConstructorReturn(self, call) {
  if (call && (typeof call === "object" || typeof call === "function")) {
    return call;
  } else if (call !== void 0) {
    throw new TypeError("Derived constructors may only return object or undefined");
  }

  return _assertThisInitialized(self);
}

function _createSuper(Derived) {
  var hasNativeReflectConstruct = _isNativeReflectConstruct();

  return function _createSuperInternal() {
    var Super = _getPrototypeOf(Derived),
        result;

    if (hasNativeReflectConstruct) {
      var NewTarget = _getPrototypeOf(this).constructor;

      result = Reflect.construct(Super, arguments, NewTarget);
    } else {
      result = Super.apply(this, arguments);
    }

    return _possibleConstructorReturn(this, result);
  };
}

var YouhuiMoneyWithTooltip = /*#__PURE__*/function (_React$PureComponent) {
  _inherits(YouhuiMoneyWithTooltip, _React$PureComponent);

  var _super = _createSuper(YouhuiMoneyWithTooltip);

  function YouhuiMoneyWithTooltip() {
    _classCallCheck(this, YouhuiMoneyWithTooltip);

    return _super.apply(this, arguments);
  }

  _createClass(YouhuiMoneyWithTooltip, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          money = _this$props.money,
          youhuiList = _this$props.youhuiList;
      if (Object.prototype.toString.call(youhuiList) !== "[object Array]") return money;
      return /*#__PURE__*/React__default["default"].createElement("div", null, money, "\xA0", /*#__PURE__*/React__default["default"].createElement(antd.Tooltip, {
        title: function title() {
          return /*#__PURE__*/React__default["default"].createElement("div", null, youhuiList && youhuiList.length ? youhuiList.map(function (obj, idx) {
            return /*#__PURE__*/React__default["default"].createElement("div", {
              key: idx
            }, obj.couponName, ": ", obj.discountAmount, "\u5143");
          }) : "未使用优惠券");
        }
      }, /*#__PURE__*/React__default["default"].createElement(antd.Icon, {
        type: "question-circle"
      })));
    }
  }]);

  return YouhuiMoneyWithTooltip;
}(React__default["default"].PureComponent);

var RiskReportHisttoryRent = /*#__PURE__*/function (_React$PureComponent) {
  _inherits(RiskReportHisttoryRent, _React$PureComponent);

  var _super = _createSuper(RiskReportHisttoryRent);

  function RiskReportHisttoryRent() {
    var _this;

    _classCallCheck(this, RiskReportHisttoryRent);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "tableColumns", [{
      title: "",
      dataIndex: "key",
      key: "key",
      align: "center"
    }, {
      title: "近7天",
      dataIndex: "rent_history_times_d7_mode_rent_history_jc"
    }, {
      title: "近1个月",
      dataIndex: "rent_history_times_m1_mode_rent_history_jc"
    }, {
      title: "近3个月",
      dataIndex: "rent_history_times_m3_mode_rent_history_jc"
    }, {
      title: "近个6月",
      dataIndex: "rent_history_times_m6_mode_rent_history_jc"
    }]);

    _defineProperty(_assertThisInitialized(_this), "numHelper", function (num) {
      if (num == -1) return 0; // -1的话作为0处理

      return num || 0; // 取不到或者类null的情况也作为0处理
    });

    return _this;
  }

  _createClass(RiskReportHisttoryRent, [{
    key: "render",
    value: function render() {
      var obj = this.props.historyRentObj || {}; // 接口返回的mode_rent_history对象

      var dataSource = [_objectSpread2({
        key: "申请租赁次数"
      }, obj)];
      return /*#__PURE__*/React__default["default"].createElement("div", {
        className: "riskReportHistoryRentContainer-jsjsdscsc121"
      }, /*#__PURE__*/React__default["default"].createElement(antd.Table, {
        bordered: true,
        columns: this.tableColumns,
        dataSource: dataSource,
        pagination: false
      }), /*#__PURE__*/React__default["default"].createElement("div", {
        className: "riskReportHistoryRow-cls"
      }, /*#__PURE__*/React__default["default"].createElement("div", null, /*#__PURE__*/React__default["default"].createElement("span", null, "\u5386\u53F2\u7533\u8BF7\u6700\u9AD8\u98CE\u9669\u5206\u6570\uFF1A"), this.numHelper(obj.rent_history_max_grade_mode_rent_history_jc)), /*#__PURE__*/React__default["default"].createElement("div", null, /*#__PURE__*/React__default["default"].createElement("span", null, "\u662F\u5426\u547D\u4E2D\u79DF\u8D41\u9ED1\u540D\u5355\uFF1A"), obj.rent_history_black_mode_rent_history_jc == 1 ? "命中" : "未命中")));
    }
  }]);

  return RiskReportHisttoryRent;
}(React__default["default"].PureComponent);

antd.Select.Option;
var UserRating = /*#__PURE__*/function (_React$PureComponent) {
  _inherits(UserRating, _React$PureComponent);

  var _super = _createSuper(UserRating);

  function UserRating() {
    _classCallCheck(this, UserRating);

    return _super.apply(this, arguments);
  }

  _createClass(UserRating, [{
    key: "render",
    value: function render() {
      this.props.form.getFieldDecorator;
      return {
        /* <Form.Item label="用户评级">
         {getFieldDecorator(
           "creditLevel",
         )(
           <Select placeholder="用户评级" allowClear style={{ width: 180 }}>
             <Option value="A">仅A</Option>
             <Option value="B">仅B</Option>
             <Option value="C">仅C</Option>
             <Option value="D">仅D</Option>
             <Option value="E">仅E</Option>
             <Option value="CD">仅CD</Option>
             <Option value="CE">仅CE</Option>
             <Option value="DE">仅DE</Option>
             <Option value="CDE">仅CDE</Option>
             <Option value="OTHER">其他</Option>
           </Select>
         )}
        </Form.Item> */
      };
    }
  }]);

  return UserRating;
}(React__default["default"].PureComponent);

var Suggest = (function (_ref) {
  var name = _ref.name,
      style = _ref.style,
      children = _ref.children;
  return /*#__PURE__*/React__default["default"].createElement("div", {
    style: _objectSpread2({}, style)
  }, /*#__PURE__*/React__default["default"].createElement("span", null, name), children);
});

var HeaderTable = /*#__PURE__*/function (_Component) {
  _inherits(HeaderTable, _Component);

  var _super = _createSuper(HeaderTable);

  function HeaderTable() {
    _classCallCheck(this, HeaderTable);

    return _super.apply(this, arguments);
  }

  _createClass(HeaderTable, [{
    key: "render",
    value: function render() {
      var _this$props$dataValid = this.props.dataValidation,
          synthesizeGrade = _this$props$dataValid.synthesizeGrade,
          riskLabeling = _this$props$dataValid.riskLabeling;
      var riskSign = /*#__PURE__*/React__default["default"].createElement("div", {
        className: "signLess"
      }, riskLabeling.map(function (item, sign) {
        return /*#__PURE__*/React__default["default"].createElement("span", {
          key: sign
        }, sign + 1, "\u3001", item);
      }));
      var scoreStandard = /*#__PURE__*/React__default["default"].createElement("div", {
        className: "signLess"
      }, /*#__PURE__*/React__default["default"].createElement("span", null, "\u5206\u503C\u5728200\u81F3700\u4E4B\u95F4\uFF0C\u5F97\u5206\u8D8A\u4F4E\uFF0C\u98CE\u9669\u8D8A\u9AD8:"), /*#__PURE__*/React__default["default"].createElement("span", null, "640\u5206\u4EE5\u4E0A\uFF0C\u5EFA\u8BAE\u901A\u8FC7\uFF1B"), /*#__PURE__*/React__default["default"].createElement("span", null, "640\u81F3560\u5206\uFF0C\u5EFA\u8BAE\u5BA1\u6838\uFF1B"), /*#__PURE__*/React__default["default"].createElement("span", null, "560\u5206\u4EE5\u4E0B\uFF0C\u5EFA\u8BAE\u62D2\u7EDD\u3002"));
      return /*#__PURE__*/React__default["default"].createElement("div", {
        className: "tableHeader1008"
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions, {
        column: 4,
        layout: "vertical",
        bordered: true
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u7EFC\u5408\u8BC4\u5206"
      }, /*#__PURE__*/React__default["default"].createElement("span", {
        style: {
          fontSize: 20,
          color: '#333',
          fontWeight: 'bold'
        }
      }, synthesizeGrade)), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u5BA1\u6838\u5EFA\u8BAE"
      }, synthesizeGrade > 640 ? /*#__PURE__*/React__default["default"].createElement(Suggest, {
        name: "\u5EFA\u8BAE\u901A\u8FC7",
        style: {
          color: '#07880E',
          fontSize: 16
        }
      }) : null, synthesizeGrade >= 560 && synthesizeGrade <= 640 ? /*#__PURE__*/React__default["default"].createElement(Suggest, {
        name: "\u5EFA\u8BAE\u5BA1\u6838",
        style: {
          color: '#07880E',
          fontSize: 16
        }
      }) : null, synthesizeGrade < 560 ? /*#__PURE__*/React__default["default"].createElement(Suggest, {
        name: "\u5EFA\u8BAE\u62D2\u7EDD",
        style: {
          color: '#FF3C2D',
          fontSize: 16
        }
      }) : null), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u547D\u4E2D\u98CE\u9669\u6807\u6CE8"
      }, riskSign), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u5206\u503C\u6807\u6CE8\u8BF4\u660E"
      }, scoreStandard)));
    }
  }]);

  return HeaderTable;
}(React.Component);

var Divider$1 = (function (_ref) {
  var name = _ref.name,
      style = _ref.style,
      children = _ref.children;
  return /*#__PURE__*/React__default["default"].createElement("div", {
    style: _objectSpread2({}, style)
  }, /*#__PURE__*/React__default["default"].createElement(antd.Divider, {
    style: {
      fontSize: '20px',
      marginTop: 50
    }
  }, name), children);
});

var BasicInformation = /*#__PURE__*/function (_Component) {
  _inherits(BasicInformation, _Component);

  var _super = _createSuper(BasicInformation);

  function BasicInformation() {
    _classCallCheck(this, BasicInformation);

    return _super.apply(this, arguments);
  }

  _createClass(BasicInformation, [{
    key: "render",
    value: function render() {
      var basicInformation = this.props.dataValidation.basicInformation;
      return /*#__PURE__*/React__default["default"].createElement("div", {
        className: "basicWrap0423"
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions, {
        column: 2
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u59D3\u540D"
      }, basicInformation.name), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8EAB\u4EFD\u8BC1\u53F7"
      }, basicInformation.ident_number), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u624B\u673A\u53F7"
      }, basicInformation.phone), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u5E74\u9F84"
      }, basicInformation.age), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u6237\u7C4D"
      }, basicInformation.ident_number_address), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u53F7\u7801\u5F52\u5C5E\u5730"
      }, basicInformation.phone_address)));
    }
  }]);

  return BasicInformation;
}(React.Component);

var RiskList = /*#__PURE__*/function (_Component) {
  _inherits(RiskList, _Component);

  var _super = _createSuper(RiskList);

  function RiskList() {
    _classCallCheck(this, RiskList);

    return _super.apply(this, arguments);
  }

  _createClass(RiskList, [{
    key: "render",
    value: function render() {
      var riskMonitoring = this.props.dataValidation.riskMonitoring;
      return /*#__PURE__*/React__default["default"].createElement("div", {
        className: "descriptionsWrap0424"
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions, {
        column: 2
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u7279\u6B8A\u5173\u6CE8\u540D\u5355"
      }, riskMonitoring.result_xd === 1 ? /*#__PURE__*/React__default["default"].createElement("img", {
        src: "https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg",
        alt: "error"
      }) : '未命中'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u5F52\u5C5E\u5730\u4F4D\u4E8E\u9AD8\u98CE\u9669\u96C6\u4E2D\u5730\u533A"
      }, riskMonitoring.census_register_high_risk_area === 1 ? /*#__PURE__*/React__default["default"].createElement("img", {
        src: "https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg",
        alt: "error"
      }) : '未命中'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u6CD5\u9662\u5931\u4FE1\u540D\u5355"
      }, riskMonitoring.idcard_hit_fysx === 1 ? /*#__PURE__*/React__default["default"].createElement("img", {
        src: "https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg",
        alt: "error"
      }) : '未命中'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u72AF\u7F6A\u901A\u7F09\u540D\u5355"
      }, riskMonitoring.idcard_hit_fztj === 1 ? /*#__PURE__*/React__default["default"].createElement("img", {
        src: "https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg",
        alt: "error"
      }) : '未命中'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u6CD5\u9662\u6267\u884C\u540D\u5355"
      }, riskMonitoring.idcard_hit_fyzx === 1 ? /*#__PURE__*/React__default["default"].createElement("img", {
        src: "https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg",
        alt: "error"
      }) : '未命中'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u52A9\u5B66\u8D37\u6B3E\u6B20\u8D39\u5386\u53F2"
      }, riskMonitoring.idcard_hit_zxdkqf === 1 ? /*#__PURE__*/React__default["default"].createElement("img", {
        src: "https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg",
        alt: "error"
      }) : '未命中'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u4FE1\u8D37\u903E\u671F\u540D\u5355"
      }, riskMonitoring.idcard_hit_xdyq === 1 ? /*#__PURE__*/React__default["default"].createElement("img", {
        src: "https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg",
        alt: "error"
      }) : '未命中'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u9AD8\u98CE\u9669\u5173\u6CE8\u540D\u5355"
      }, riskMonitoring.idcard_hit_gfxgz === 1 ? /*#__PURE__*/React__default["default"].createElement("img", {
        src: "https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg",
        alt: "error"
      }) : '未命中'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8F66\u8F86\u79DF\u8D41\u8FDD\u7EA6\u540D\u5355"
      }, riskMonitoring.idcard_hit_clzlwy === 1 ? /*#__PURE__*/React__default["default"].createElement("img", {
        src: "https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg",
        alt: "error"
      }) : '未命中'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u6CD5\u9662\u7ED3\u6848\u540D\u5355"
      }, riskMonitoring.idcard_hit_fyja === 1 ? /*#__PURE__*/React__default["default"].createElement("img", {
        src: "https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg",
        alt: "error"
      }) : '未命中'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u6545\u610F\u8FDD\u7AE0\u4E58\u8F66\u540D\u5355"
      }, riskMonitoring.idcard_hit_gywzcc === 1 ? /*#__PURE__*/React__default["default"].createElement("img", {
        src: "https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg",
        alt: "error"
      }) : '未命中'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u6B20\u7A0E\u540D\u5355"
      }, riskMonitoring.idcard_hit_qs === 1 ? /*#__PURE__*/React__default["default"].createElement("img", {
        src: "https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg",
        alt: "error"
      }) : '未命中'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u6B20\u7A0E\u516C\u53F8\u6CD5\u4EBA\u4EE3\u8868\u540D\u5355"
      }, riskMonitoring.idcard_hit_qsgsfrdb === 1 ? /*#__PURE__*/React__default["default"].createElement("img", {
        src: "https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg",
        alt: "error"
      }) : '未命中'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u865A\u62DF\u53F7\u7801\u5E93"
      }, riskMonitoring.phone_hit_xjhm === 1 ? /*#__PURE__*/React__default["default"].createElement("img", {
        src: "https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg",
        alt: "error"
      }) : '未命中'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u901A\u4FE1\u5C0F\u53F7\u5E93"
      }, riskMonitoring.phone_hit_txxh === 1 ? /*#__PURE__*/React__default["default"].createElement("img", {
        src: "https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg",
        alt: "error"
      }) : '未命中')));
    }
  }]);

  return RiskList;
}(React.Component);

var FourItem = /*#__PURE__*/function (_Component) {
  _inherits(FourItem, _Component);

  var _super = _createSuper(FourItem);

  function FourItem() {
    _classCallCheck(this, FourItem);

    return _super.apply(this, arguments);
  }

  _createClass(FourItem, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          style = _this$props.style,
          title = _this$props.title,
          makeLoans = _this$props.makeLoans;
      return /*#__PURE__*/React__default["default"].createElement("div", {
        className: "fourWrap0425",
        style: _objectSpread2({}, style)
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions, {
        column: 4,
        layout: "vertical",
        bordered: true
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: title.first
      }, makeLoans.cflenders === '' ? '----' : makeLoans.cflenders), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: title.second
      }, makeLoans.nllenders === '' ? '----' : makeLoans.nllenders), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: title.three
      }, makeLoans.lend_time === '' ? '----' : makeLoans.lend_time), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: title.four
      }, makeLoans.lend_time_distance === '' ? '----' : makeLoans.lend_time_distance)));
    }
  }]);

  return FourItem;
}(React.Component);

var FourItemapplys = /*#__PURE__*/function (_Component) {
  _inherits(FourItemapplys, _Component);

  var _super = _createSuper(FourItemapplys);

  function FourItemapplys() {
    _classCallCheck(this, FourItemapplys);

    return _super.apply(this, arguments);
  }

  _createClass(FourItemapplys, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          style = _this$props.style,
          title = _this$props.title;
      var applyFor = this.props.dataValidation.applyFor;
      return /*#__PURE__*/React__default["default"].createElement("div", {
        className: "fourWrap0425",
        style: _objectSpread2({}, style)
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions, {
        column: 4,
        layout: "vertical",
        bordered: true
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: title.first
      }, applyFor.consumer_apply_mechanism_number === '' ? '无记录' : applyFor.consumer_apply_mechanism_number), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: title.second
      }, applyFor.network_loan_apply_mechanis_mnumber === '' ? '无记录' : applyFor.network_loan_apply_mechanis_mnumber), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: title.three
      }, applyFor.last_apply_time === '' ? '无记录' : applyFor.last_apply_time), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: title.four
      }, applyFor.last_apply_time_distance === '' ? '无记录' : applyFor.last_apply_time_distance)));
    }
  }]);

  return FourItemapplys;
}(React.Component);

var Fiveitem$1 = /*#__PURE__*/function (_Component) {
  _inherits(Fiveitem, _Component);

  var _super = _createSuper(Fiveitem);

  function Fiveitem() {
    _classCallCheck(this, Fiveitem);

    return _super.apply(this, arguments);
  }

  _createClass(Fiveitem, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          title = _this$props.title,
          style = _this$props.style,
          text = _this$props.text;
      var _this$props$dataValid = this.props.dataValidation;
          _this$props$dataValid.liability;
          var applyFor = _this$props$dataValid.applyFor;
      return /*#__PURE__*/React__default["default"].createElement("div", {
        className: "fiveWrap121",
        style: _objectSpread2({}, style)
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions, {
        column: 5,
        layout: "vertical",
        bordered: true
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: title.first
      }, /*#__PURE__*/React__default["default"].createElement("span", {
        style: {
          fontWeight: 'bold'
        }
      }, text)), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: title.second
      }, applyFor.apply_time1 === '' ? 0 : applyFor.apply_time1), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: title.three
      }, applyFor.apply_time3 === '' ? 0 : applyFor.apply_time3), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: title.four
      }, applyFor.apply_time6 === '' ? 0 : applyFor.apply_time6), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: title.five
      }, applyFor.apply_time12 === '' ? 0 : applyFor.apply_time12)));
    }
  }]);

  return Fiveitem;
}(React.Component);

var Fiveitem = /*#__PURE__*/function (_Component) {
  _inherits(Fiveitem, _Component);

  var _super = _createSuper(Fiveitem);

  function Fiveitem() {
    _classCallCheck(this, Fiveitem);

    return _super.apply(this, arguments);
  }

  _createClass(Fiveitem, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          title = _this$props.title,
          style = _this$props.style,
          text = _this$props.text;
      var makeLoans = this.props.dataValidation.makeLoans;
      return /*#__PURE__*/React__default["default"].createElement("div", {
        className: "fiveWrap121",
        style: _objectSpread2({}, style)
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions, {
        column: 5,
        layout: "vertical",
        bordered: true
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: title.first
      }, /*#__PURE__*/React__default["default"].createElement("span", {
        style: {
          fontWeight: 'bold'
        }
      }, text)), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: title.second
      }, makeLoans.lend_number1 === '' ? 0 : makeLoans.lend_number1), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: title.three
      }, makeLoans.lend_number3 === '' ? 0 : makeLoans.lend_number3), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: title.four
      }, makeLoans.lend_number6 === '' ? 0 : makeLoans.lend_number6)));
    }
  }]);

  return Fiveitem;
}(React.Component);

var Threeitem = /*#__PURE__*/function (_Component) {
  _inherits(Threeitem, _Component);

  var _super = _createSuper(Threeitem);

  function Threeitem() {
    _classCallCheck(this, Threeitem);

    return _super.apply(this, arguments);
  }

  _createClass(Threeitem, [{
    key: "render",
    value:
    /* render() {
      const { style, title } = this.props;
      const { dataValidation: { makeLoans } } = this.props;
      return (
        <div className="threeWrap" style={{ ...style }}>
          <Descriptions column={5} layout="vertical" bordered>
            <Descriptions.Item label=""><span style={{ fontWeight: 'bold' }}>{title.observeNum}</span></Descriptions.Item>
            <Descriptions.Item label={title.first}>{makeLoans.repay_succ1 === '无记录' ? 0 : makeLoans.repay_succ1}</Descriptions.Item>
            <Descriptions.Item label={title.second}>{makeLoans.repay_succ12 === '无记录' ? 0 : makeLoans.repay_succ12}</Descriptions.Item>
          </Descriptions>
          <div className="threeAbnormal">
            <span style={{ fontWeight: 'bold' }}>{title.unobserveNum}</span>
            <span>{makeLoans.repay_fail1 === '无记录' ? 0 : makeLoans.repay_fail1}</span>
            <span>{makeLoans.repay_fail12 === '无记录' ? 0 : makeLoans.repay_fail12}</span>
          </div>
        </div>
      )
    } */
    function render() {
      var _this$props = this.props,
          style = _this$props.style,
          title = _this$props.title;
      var _this$props$dataValid = this.props.dataValidation;
          _this$props$dataValid.makeLoans;
          var conciseResult = _this$props$dataValid.conciseResult;
      var personal_loan_h = conciseResult.personal_loan_h;
      var columns = [{
        title: '',
        dataIndex: 'name'
      }, {
        title: '近1个月',
        dataIndex: 'm1'
      }, {
        title: '近3个月',
        dataIndex: 'm3'
      }, {
        title: '近6个月',
        dataIndex: 'm6'
      }, {
        title: '近12个月',
        dataIndex: 'm12'
      }, {
        title: '近24个月',
        dataIndex: 'm24'
      }];
      var customData = [{
        name: title.observeNum,
        m1: personal_loan_h.repay_succ1 || 0,
        m3: personal_loan_h.repay_succ3 || 0,
        m6: personal_loan_h.repay_succ6 || 0,
        m12: personal_loan_h.repay_succ12 || 0,
        m24: personal_loan_h.repay_succ24 || 0
      }, {
        name: title.unobserveNum,
        m1: personal_loan_h.repay_fail1 || 0,
        m3: personal_loan_h.repay_fail3 || 0,
        m6: personal_loan_h.repay_fail6 || 0,
        m12: personal_loan_h.repay_fail12 || 0,
        m24: personal_loan_h.repay_fail24 || 0
      }, {
        name: title.threeNum,
        m1: personal_loan_h.repay_money1 || 0,
        m3: personal_loan_h.repay_money3 || 0,
        m6: personal_loan_h.repay_money6 || 0,
        m12: personal_loan_h.repay_money12 || 0,
        m24: personal_loan_h.repay_money24 || 0
      }];
      return /*#__PURE__*/React__default["default"].createElement("div", {
        className: "threeWrap",
        style: _objectSpread2({}, style)
      }, /*#__PURE__*/React__default["default"].createElement("div", {
        className: "tableHeader"
      }, /*#__PURE__*/React__default["default"].createElement(antd.Table, {
        locale: {
          emptyText: '暂无记录'
        },
        bordered: true,
        columns: columns,
        dataSource: customData,
        pagination: false
      })), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions, {
        column: 6,
        layout: "vertical",
        bordered: true
      }));
    }
  }]);

  return Threeitem;
}(React.Component);

var Historyborrowing = /*#__PURE__*/function (_Component) {
  _inherits(Historyborrowing, _Component);

  var _super = _createSuper(Historyborrowing);

  function Historyborrowing() {
    _classCallCheck(this, Historyborrowing);

    return _super.apply(this, arguments);
  }

  _createClass(Historyborrowing, [{
    key: "render",
    value: function render() {
      var _this$props$dataValid = this.props.dataValidation,
          applyFor = _this$props$dataValid.applyFor,
          makeLoans = _this$props$dataValid.makeLoans;
      console.log("风控：", this.props);
      var titleName = {
        first: '消费分期类申请机构数 (个) ',
        second: '网络贷款类申请机构数 (个) ',
        three: '最近一次申请日期',
        four: '距离最近一次申请日期已有 (天) '
      };
      var fiveTitle = {
        first: '',
        second: '近1个月',
        three: '近3个月',
        four: '近6个月',
        five: '近12个月'
      };
      var fiveTitlerepay = {
        first: '',
        second: '近1个月',
        three: '近3个月',
        four: '近6个月'
      };
      var nameObject = {
        first: '消费分期类放款机构数 (个) ',
        second: '网络贷款类放款机构数 (个) ',
        three: '最近一次放款日期',
        four: '距离最近一次放款日期已有 (天) '
      };
      var threeName = {
        first: '近1个月',
        second: '近3个月',
        three: '近6个月',
        four: '近12个月',
        five: '近24个月',
        observeNum: '履约次数(次)',
        unobserveNum: '还款异常次数(次)',
        threeNum: '履约金额(元)'
      };
      return /*#__PURE__*/React__default["default"].createElement("div", {
        className: "historyWrap"
      }, /*#__PURE__*/React__default["default"].createElement("div", {
        className: "total"
      }, /*#__PURE__*/React__default["default"].createElement("span", {
        style: {
          fontWeight: 'bold'
        }
      }, "\u8FD112\u4E2A\u6708\u7533\u8BF7\u673A\u6784\u603B\u6570\u2003(\u4E2A)\uFF1A"), /*#__PURE__*/React__default["default"].createElement("span", null, applyFor.apply_mechanism_number === '无记录' ? 0 : applyFor.apply_mechanism_number)), /*#__PURE__*/React__default["default"].createElement(FourItemapplys, {
        dataValidation: this.props.dataValidation,
        style: {
          marginTop: 14
        },
        title: titleName
      }), /*#__PURE__*/React__default["default"].createElement(Fiveitem$1, {
        dataValidation: this.props.dataValidation,
        style: {
          marginTop: 22
        },
        title: fiveTitle,
        text: "\u7533\u8BF7\u6B21\u6570(\u6B21)"
      }), /*#__PURE__*/React__default["default"].createElement("div", {
        className: "total"
      }, /*#__PURE__*/React__default["default"].createElement("span", {
        style: {
          fontWeight: 'bold'
        }
      }, "\u8FD112\u4E2A\u6708\u653E\u6B3E\u673A\u6784\u603B\u6570\u2003(\u4E2A)\uFF1A"), /*#__PURE__*/React__default["default"].createElement("span", null, makeLoans.lenders === '' ? 0 : makeLoans.lenders)), /*#__PURE__*/React__default["default"].createElement(FourItem, {
        style: {
          marginTop: 14
        },
        title: nameObject,
        makeLoans: makeLoans
      }), /*#__PURE__*/React__default["default"].createElement(Fiveitem, {
        dataValidation: this.props.dataValidation,
        style: {
          marginTop: 22
        },
        title: fiveTitlerepay,
        text: "\u653E\u6B3E\u6B21\u6570(\u6B21)"
      }), /*#__PURE__*/React__default["default"].createElement(Threeitem, {
        dataValidation: this.props.dataValidation,
        style: {
          marginTop: 22
        },
        title: threeName
      }));
    }
  }]);

  return Historyborrowing;
}(React.Component);

var Historyoverdue = /*#__PURE__*/function (_Component) {
  _inherits(Historyoverdue, _Component);

  var _super = _createSuper(Historyoverdue);

  function Historyoverdue() {
    _classCallCheck(this, Historyoverdue);

    return _super.apply(this, arguments);
  }

  _createClass(Historyoverdue, [{
    key: "render",
    value: function render() {
      var _this$props$dataValid = this.props.dataValidation,
          overdueRecord = _this$props$dataValid.overdueRecord,
          overdueRecordDataList = _this$props$dataValid.overdueRecordDataList;
      var columns = [{
        title: '序号',
        dataIndex: 'key'
      }, {
        title: '逾期金额区间(元)',
        dataIndex: 'overdue_money'
      }, {
        title: '逾期时间',
        dataIndex: 'overdue_time'
      }, {
        title: '逾期时长',
        dataIndex: 'overdue_day'
      }, {
        title: '是否结清',
        dataIndex: 'settlement',
        render: function render(settlement) {
          return /*#__PURE__*/React__default["default"].createElement(React__default["default"].Fragment, null, settlement === 'N' ? '否' : null, settlement === 'Y' ? '是' : null);
        }
      }];
      var customData = overdueRecordDataList.map(function (item, sign) {
        var newsItem = _objectSpread2({}, item);

        var keys = sign + 1;
        newsItem.key = keys;
        return newsItem;
      });
      return /*#__PURE__*/React__default["default"].createElement("div", {
        className: "overdueWrap"
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions, {
        column: 2
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8FD16\u4E2A\u6708\u903E\u671F\u673A\u6784\u6B21\u6570"
      }, overdueRecord.overdue_mechanism_number === '' ? 0 : overdueRecord.overdue_mechanism_number), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8FD16\u4E2A\u6708\u903E\u671F\u603B\u6B21\u6570"
      }, overdueRecord.counts === '' ? 0 : overdueRecord.counts), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8FD16\u4E2A\u6708\u672A\u7ED3\u6E05\u903E\u671F\u6B21\u6570"
      }, overdueRecord.uncleared_counts === '' ? 0 : overdueRecord.uncleared_counts), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8FD16\u4E2A\u6708\u903E\u671F\u603B\u91D1\u989D(\u5143)"
      }, overdueRecord.overdue_money === '' ? 0 : overdueRecord.overdue_money)), /*#__PURE__*/React__default["default"].createElement("div", {
        className: "tableHeader"
      }, /*#__PURE__*/React__default["default"].createElement(antd.Table, {
        locale: {
          emptyText: '暂无记录'
        },
        bordered: true,
        columns: columns,
        dataSource: customData,
        pagination: false
      })), /*#__PURE__*/React__default["default"].createElement("div", {
        className: "text"
      }, "\u8BF4\u660E\uFF1AS\u4EE3\u8868\u671F\u6570\uFF0C1\u671F=7\u5929\uFF0Cs0\u8868\u793A\u4E0D\u52307\u5929\u3001s1\u4EE3\u88687-14\u5929\uFF0C\u4EE5\u6B64\u7C7B\u63A8\uFF1BM\u4EE3\u8868\u671F\u6570\uFF0C1\u671F=30\u5929\uFF0Cmo\u8868\u793A\u4E0D\u523030\u5929\uFF0Cml\u4EE3\u886830-60\u5929\uFF0C\u4EE5\u6B64\u7C7B\u63A8\u3002"));
    }
  }]);

  return Historyoverdue;
}(React.Component);

var Leaseinfo = /*#__PURE__*/function (_Component) {
  _inherits(Leaseinfo, _Component);

  var _super = _createSuper(Leaseinfo);

  function Leaseinfo() {
    _classCallCheck(this, Leaseinfo);

    return _super.apply(this, arguments);
  }

  _createClass(Leaseinfo, [{
    key: "render",
    value: function render() {
      var style = this.props.style;
      var rentalInformation = this.props.dataValidation.rentalInformation;
      return /*#__PURE__*/React__default["default"].createElement("div", {
        className: "fiveWrap",
        style: _objectSpread2({}, style)
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions, {
        column: 6,
        layout: "vertical",
        bordered: true
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: ""
      }, /*#__PURE__*/React__default["default"].createElement("span", {
        style: {
          fontWeight: 'bold'
        }
      }, "\u7533\u8BF7\u79DF\u8D41(\u6B21\u6570/\u673A\u6784\u6570)")), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8FD17\u5929"
      }, rentalInformation.d7_apply_time, "/", rentalInformation.d7_apply_agency_time), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8FD11\u4E2A\u6708"
      }, rentalInformation.m1_apply_time, "/", rentalInformation.m1_apply_agency_time), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8FD13\u4E2A\u6708"
      }, rentalInformation.m3_apply_time, "/", rentalInformation.m3_apply_agency_time), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8FD16\u4E2A\u6708"
      }, rentalInformation.m6_apply_time, "/", rentalInformation.m6_apply_agency_time), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8FD112\u4E2A\u6708"
      }, rentalInformation.m12_apply_time, "/", rentalInformation.m12_apply_agency_time)));
    }
  }]);

  return Leaseinfo;
}(React.Component);

var Courtrisklist = /*#__PURE__*/function (_Component) {
  _inherits(Courtrisklist, _Component);

  var _super = _createSuper(Courtrisklist);

  function Courtrisklist() {
    _classCallCheck(this, Courtrisklist);

    return _super.apply(this, arguments);
  }

  _createClass(Courtrisklist, [{
    key: "render",
    value: function render() {
      var riskInformation = this.props.dataValidation.riskInformation;
      var data_typeType = {
        "cpws": "裁判文书",
        "ktgg": "开庭公告",
        "ajlc": "案件流程信息",
        "fygg": "法院公告",
        "shixin": "失信公告",
        "zxgg": "执行公告",
        "bgt": "曝光台"
      };
      var columns = [{
        title: '序号',
        dataIndex: 'key'
      }, {
        title: '审结日期',
        dataIndex: 'sort_time_string'
      }, {
        title: '类型',
        dataIndex: 'data_type',
        render: function render(data_type, row, index) {
          return data_typeType[data_type];
        }
      }, {
        title: '摘要说明',
        dataIndex: 'summary'
      }, {
        title: '匹配度',
        dataIndex: 'compatibility'
      }];
      var customData = riskInformation.map(function (item, sign) {
        var newsItem = _objectSpread2({}, item);

        var keys = sign + 1;
        newsItem.key = keys;
        return newsItem;
      });
      return /*#__PURE__*/React__default["default"].createElement("div", {
        className: "tableHeader1212"
      }, /*#__PURE__*/React__default["default"].createElement(antd.Table, {
        locale: {
          emptyText: '暂无记录'
        },
        bordered: true,
        columns: columns,
        dataSource: customData,
        pagination: false
      }));
    }
  }]);

  return Courtrisklist;
}(React.Component);

var apply = [{
  name: '一般消费分期平台'
}, {
  name: '信用卡'
}, {
  name: '大型消费金融公司'
}, {
  name: '其它类型公司'
}, {
  name: 'P2P平台'
}, {
  name: '小额贷款公司'
}, {
  name: '申请查询总数'
}];

var Applyquery = /*#__PURE__*/function (_Component) {
  _inherits(Applyquery, _Component);

  var _super = _createSuper(Applyquery);

  function Applyquery() {
    _classCallCheck(this, Applyquery);

    return _super.apply(this, arguments);
  }

  _createClass(Applyquery, [{
    key: "render",
    value: function render() {
      var applyQuery = this.props.dataValidation.applyQuery;
      var applyQuerySeven = {
        d7_apply_setup_time_ybxffq: applyQuery.d7_apply_setup_time_ybxffq,
        d7_apply_setup_time_xyk: applyQuery.d7_apply_setup_time_xyk,
        d7_apply_setup_time_dxxfjr: applyQuery.d7_apply_setup_time_dxxfjr,
        d7_apply_setup_time_other: applyQuery.d7_apply_setup_time_other,
        d7_apply_setup_time_p2pwd: applyQuery.d7_apply_setup_time_p2pwd,
        d7_apply_setup_time_xedkgs: applyQuery.d7_apply_setup_time_xedkgs,
        d7_apply_setup_time: applyQuery.d7_apply_setup_time
      };
      var applyQuerythrity = {
        m1_apply_setup_time_ybxffq: applyQuery.m1_apply_setup_time_ybxffq,
        m1_apply_setup_time_xyk: applyQuery.m1_apply_setup_time_xyk,
        m1_apply_setup_time_dxxfjr: applyQuery.m1_apply_setup_time_dxxfjr,
        m1_apply_setup_time_other: applyQuery.m1_apply_setup_time_other,
        m1_apply_setup_time_p2pwd: applyQuery.m1_apply_setup_time_p2pwd,
        m1_apply_setup_time_xedkgs: applyQuery.m1_apply_setup_time_xedkgs,
        m1_apply_setup_time: applyQuery.m1_apply_setup_time
      };
      var applyQueryninety = {
        m3_apply_setup_time_ybxffq: applyQuery.m3_apply_setup_time_ybxffq,
        m3_apply_setup_time_xyk: applyQuery.m3_apply_setup_time_xyk,
        m3_apply_setup_time_dxxfjr: applyQuery.m3_apply_setup_time_dxxfjr,
        m3_apply_setup_time_other: applyQuery.m3_apply_setup_time_other,
        m3_apply_setup_time_p2pwd: applyQuery.m3_apply_setup_time_p2pwd,
        m3_apply_setup_time_xedkgs: applyQuery.m3_apply_setup_time_xedkgs,
        m3_apply_setup_time: applyQuery.m3_apply_setup_time
      };
      var applyQuerySevendata = [];
      var applyQuerythritydata = [];
      var applyQueryninetydata = [];

      for (var key in applyQuerySeven) {
        var seven = {};
        seven.day = applyQuerySeven[key];
        applyQuerySevendata.push(seven);
      }

      for (var _key in applyQuerythrity) {
        var _seven = {};
        _seven.day = applyQuerythrity[_key];
        applyQuerythritydata.push(_seven);
      }

      for (var _key2 in applyQueryninety) {
        var _seven2 = {};
        _seven2.day = applyQueryninety[_key2];
        applyQueryninetydata.push(_seven2);
      }

      var sevenDays = [{
        title: '近7天',
        dataIndex: 'day'
      }];
      var monthDays = [{
        title: '近30天',
        dataIndex: 'day'
      }];
      var ninetyDays = [{
        title: '近90天',
        dataIndex: 'day'
      }];
      var applycolumns = [{
        title: '',
        dataIndex: 'name'
      }];
      var applyData = apply.map(function (item, sign) {
        var newsItem = _objectSpread2({}, item);

        var keys = sign + 1;
        newsItem.key = keys;
        return newsItem;
      });
      var sevenData = applyQuerySevendata.map(function (item, sign) {
        var newsItem = _objectSpread2({}, item);

        var keys = sign + 1;
        newsItem.key = keys;
        return newsItem;
      });
      var monthData = applyQuerythritydata.map(function (item, sign) {
        var newsItem = _objectSpread2({}, item);

        var keys = sign + 1;
        newsItem.key = keys;
        return newsItem;
      });
      var ninetyData = applyQueryninetydata.map(function (item, sign) {
        var newsItem = _objectSpread2({}, item);

        var keys = sign + 1;
        newsItem.key = keys;
        return newsItem;
      });
      return /*#__PURE__*/React__default["default"].createElement("div", {
        className: "tableHeader1210"
      }, /*#__PURE__*/React__default["default"].createElement(antd.Table, {
        bordered: true,
        columns: applycolumns,
        dataSource: applyData,
        pagination: false
      }), /*#__PURE__*/React__default["default"].createElement(antd.Table, {
        locale: {
          emptyText: '暂无记录'
        },
        bordered: true,
        columns: sevenDays,
        dataSource: sevenData,
        pagination: false
      }), /*#__PURE__*/React__default["default"].createElement(antd.Table, {
        locale: {
          emptyText: '暂无记录'
        },
        bordered: true,
        columns: monthDays,
        dataSource: monthData,
        pagination: false
      }), /*#__PURE__*/React__default["default"].createElement(antd.Table, {
        locale: {
          emptyText: '暂无记录'
        },
        bordered: true,
        columns: ninetyDays,
        dataSource: ninetyData,
        pagination: false
      }));
    }
  }]);

  return Applyquery;
}(React.Component);

var defaultApiResult$1 = {
  base_info: {},
  court_risk_info_list: [],
  hit_risk_tagging: [],
  order_num: "",
  personal_loan_demand: {},
  personal_loan_f: {},
  personal_loan_s: {},
  personal_overdue_history: {},
  relevance_risk_check: {},
  rent_history: {},
  risk_list_check: {},
  score_norm_explain: "",
  verify_recomment: ""
};

var TLXProRiskReport = /*#__PURE__*/function (_Component) {
  _inherits(TLXProRiskReport, _Component);

  var _super = _createSuper(TLXProRiskReport);

  function TLXProRiskReport() {
    var _this;

    _classCallCheck(this, TLXProRiskReport);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "dataFactory", function () {
      var obj = _this.props.riskReport || defaultApiResult$1;
      var obj1 = _this.props.riskReport1 || defaultApiResult$1;
      return {
        orderNumber: obj.order_num,
        e: obj1,
        conciseResult: obj,
        synthesizeGrade: obj.score_norm_explain,
        riskLabeling: obj.hit_risk_tagging,
        basicInformation: obj.base_info,
        riskMonitoring: obj.risk_list_check,
        rentalInformation: obj.rent_history,
        overdueRecordDataList: obj.personal_overdue_history.datalist,
        overdueRecord: obj.personal_overdue_history,
        riskDetection: obj.relevance_risk_check,
        applyFor: obj.personal_loan_s,
        makeLoans: obj.personal_loan_f,
        riskInformation: obj.court_risk_info_list,
        applyQuery: obj.personal_loan_demand
      };
    });

    return _this;
  }

  _createClass(TLXProRiskReport, [{
    key: "render",
    value: function render() {
      var uiObj = this.dataFactory();
      var riskDetection = uiObj.riskDetection,
          rentalInformation = uiObj.rentalInformation,
          e = uiObj.e;
      console.log("obj111111", e);
      var columns7 = [{
        title: '',
        dataIndex: 'name'
      }, {
        title: '七天',
        dataIndex: 'd7'
      }, {
        title: '1个月',
        dataIndex: 'm1'
      }, {
        title: '3个月',
        dataIndex: 'm3'
      }, {
        title: '6个月',
        dataIndex: 'm6'
      }];
      var kv = {
        debt_org_d7_num: [0, 1, 9],
        debt_org_total_num: [0, 9, 14],
        debt_shopping_total_num: [0, 1, 2, 4],
        debt_www_total_num: [0, 1, 5, 11],
        debt_settled_num: [0, 5, 9, 14, 17],
        debt_first_distance_now: [0, 220, 450, 520],
        debt_org_m1_num: [0, 1, 4, 11],
        debt_org_m3_num: [0, 5, 10, 17],
        debt_org_m6_num: [0, 4, 7, 13],
        repay_succ_d7_num: [0, 3, 13],
        repay_succ_m1_num: [0, 7, 34],
        repay_succ_m3_num: [0, 2, 31],
        repay_succ_m6_num: [0, 3, 27, 50],
        repay_succ_d7_money: [0, 2000, 17000],
        repay_succ_m1_money: [0, 10000, 36000],
        repay_succ_m3_money: [0, 13000, 49000],
        repay_succ_m6_money: [0, 2000, 58000],
        repay_succ_lately_num: [0, 50, 160],
        repay_fail_d7_num: [0, 3, 5, 7],
        repay_fail_m1_num: [0, 3, 5, 34],
        repay_fail_m3_num: [0, 6, 22, 56],
        repay_fail_m6_num: [0, 3, 25, 30, 70],
        repay_fail_d7_money: [0, 2000, 10000, 17000, 26000],
        repay_fail_m1_money: [0, 6000, 10000, 36000],
        repay_fail_m3_money: [0, 10000, 50000, 80000],
        repay_fail_m6_money: [0, 2000, 30000, 60000, 90000],
        ovdure_org_num: [0, 1, 2, 4],
        ovdure_org_money: [0, 1000, 2000, 3000, 5000, 7000, 10000]
      };

      var handje = function handje(key) {
        if (kv[key]) {
          //console.log("出现了", kv[key], kv[key].length)
          //console.log("出现了", kv[key], e[key])
          if (e[key] < 1) return 0; //如果传进来是0，那么就返回0

          if (e[key] == kv[key].length) return kv[key][kv[key].length - 1] + "+"; //如果传过来最大的，那么就最大的+

          if (e[key] > kv[key].length) return "无"; //如果传过来的值没有，就返回无

          return kv[key][e[key] - 1] + "-" + kv[key][e[key]];
        }

        return e[key];
      };

      var ninetyData1 = [{
        name: "还款失败金额",
        d7: handje("repay_fail_d7_money"),
        m1: handje("repay_fail_m1_money"),
        m3: handje("repay_fail_m3_money"),
        m6: handje("repay_fail_m6_money")
      }, {
        name: "还款失败次数",
        d7: handje("repay_fail_d7_num"),
        m1: handje("repay_fail_m1_num"),
        m3: handje("repay_fail_m3_num"),
        m6: handje("repay_fail_m6_num")
      }, {
        name: "还款成功金额",
        d7: handje("repay_succ_d7_money"),
        m1: handje("repay_succ_m1_money"),
        m3: handje("repay_succ_m3_money"),
        m6: handje("repay_succ_m6_money")
      }, {
        name: "还款成功次数",
        d7: handje("repay_succ_d7_num"),
        m1: handje("repay_succ_m1_num"),
        m3: handje("repay_succ_m3_num"),
        m6: handje("repay_succ_m6_num")
      }, {
        name: "负债机构数",
        d7: handje("debt_org_d7_num"),
        m1: handje("debt_org_m1_num"),
        m3: handje("debt_org_m3_num"),
        m6: handje("debt_org_m6_num")
      }, {
        name: "身份证申请次数",
        d7: handje("apply_by_id_d7_num"),
        m1: handje("apply_by_id_m1_num"),
        m3: handje("apply_by_id_m3_num"),
        m6: handje("apply_by_id_m6_num")
      }, {
        name: "手机号申请次数",
        d7: handje("apply_by_phone_d7_num"),
        m1: handje("apply_by_phone_m1_num"),
        m3: handje("apply_by_phone_m3_num"),
        m6: handje("apply_by_phone_m6_num")
      }, {
        name: "身份证号申请平台数",
        d7: handje("apply_org_by_id_d7_num"),
        m1: handje("apply_org_by_id_m1_num"),
        m3: handje("apply_org_by_id_m3_num"),
        m6: handje("apply_org_by_id_m6_num")
      }, {
        name: "手机号申请平台数",
        d7: handje("apply_org_by_phone_d7_num"),
        m1: handje("apply_org_by_phone_m1_num"),
        m3: handje("apply_org_by_phone_m3_num"),
        m6: handje("apply_org_by_phone_m6_num")
      }];
      return /*#__PURE__*/React__default["default"].createElement("div", {
        className: "booleandataTotalContainer"
      }, /*#__PURE__*/React__default["default"].createElement("div", {
        className: "booleanDataWrap"
      }, /*#__PURE__*/React__default["default"].createElement("div", null, /*#__PURE__*/React__default["default"].createElement(HeaderTable, {
        dataValidation: uiObj
      }), /*#__PURE__*/React__default["default"].createElement(Divider$1, {
        name: "\u57FA\u672C\u4FE1\u606F"
      }), /*#__PURE__*/React__default["default"].createElement(BasicInformation, {
        dataValidation: uiObj
      }), /*#__PURE__*/React__default["default"].createElement(Divider$1, {
        name: "\u98CE\u9669\u540D\u5355\u76D1\u6D4B"
      }), /*#__PURE__*/React__default["default"].createElement(RiskList, {
        dataValidation: uiObj
      }), /*#__PURE__*/React__default["default"].createElement(Divider$1, {
        name: "\u673A\u6784\u67E5\u8BE2\u8BB0\u5F55"
      }), /*#__PURE__*/React__default["default"].createElement(Applyquery, {
        dataValidation: uiObj
      }), /*#__PURE__*/React__default["default"].createElement(Divider$1, {
        name: "\u5386\u53F2\u501F\u8D37\u884C\u4E3A"
      }), /*#__PURE__*/React__default["default"].createElement(Historyborrowing, {
        dataValidation: uiObj
      }), /*#__PURE__*/React__default["default"].createElement(Divider$1, {
        name: "\u5386\u53F2\u903E\u671F\u8BB0\u5F55"
      }), /*#__PURE__*/React__default["default"].createElement(Historyoverdue, {
        dataValidation: uiObj
      }), /*#__PURE__*/React__default["default"].createElement(Divider$1, {
        name: "\u4E8C\u4EE3\u98CE\u63A7\u4FE1\u606F"
      }), /*#__PURE__*/React__default["default"].createElement("div", {
        className: "booleandataTotalContainer"
      }, /*#__PURE__*/React__default["default"].createElement("div", null, /*#__PURE__*/React__default["default"].createElement("div", null, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions, {
        title: ""
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u9996\u6B21\u8D1F\u503A\u8DDD\u4ECA\u65F6\u957F"
      }, handje("debt_first_distance_now")), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8D1F\u503A\u673A\u6784\u603B\u6570"
      }, handje("debt_org_total_num")), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u5DF2\u7ED3\u6E05\u8D1F\u503A\u673A\u6784\u6570"
      }, handje("debt_settled_num")), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u6D88\u8D39\u91D1\u878D\u7C7B\u673A\u6784\u6570"
      }, handje("debt_shopping_total_num")), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u7F51\u7EDC\u8D37\u6B3E\u7C7B\u673A\u6784\u6570"
      }, handje("debt_www_total_num")), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u662F\u5426\u5B58\u5728\u903E\u671F\u672A\u7ED3\u6E05"
      }, handje("ovdure_flag")), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u5F53\u524D\u903E\u671F\u91D1\u989D"
      }, handje("ovdure_org_money")), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u5F53\u524D\u903E\u671F\u673A\u6784\u6570"
      }, handje("ovdure_org_num")), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u6700\u8FD1\u4E00\u6B21\u6210\u529F\u8FD8\u6B3E\u8DDD\u4ECA\u5929\u6570"
      }, handje("repay_succ_lately_num")))), /*#__PURE__*/React__default["default"].createElement("div", null, /*#__PURE__*/React__default["default"].createElement(antd.Table, {
        locale: {
          emptyText: '暂无记录'
        },
        bordered: true,
        columns: columns7,
        dataSource: ninetyData1,
        pagination: false
      })))), /*#__PURE__*/React__default["default"].createElement(Divider$1, {
        name: "\u79DF\u8D41\u4FE1\u606F\u8BE6\u60C5"
      }), /*#__PURE__*/React__default["default"].createElement(Leaseinfo, {
        dataValidation: uiObj
      }), /*#__PURE__*/React__default["default"].createElement("div", {
        className: "highestGrade"
      }, /*#__PURE__*/React__default["default"].createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "\u662F\u5426\u547D\u4E2D\u79DF\u8D41\u9ED1\u540D\u5355\uFF1A"), /*#__PURE__*/React__default["default"].createElement("span", null, rentalInformation.rent_history_black === 0 ? "未命中" : "命中")), /*#__PURE__*/React__default["default"].createElement(Divider$1, {
        name: "\u5173\u8054\u98CE\u9669\u68C0\u6D4B"
      }), /*#__PURE__*/React__default["default"].createElement("div", {
        className: "riskDetection"
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions, {
        column: 1
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "3\u4E2A\u6708\u8EAB\u4EFD\u8BC1\u5173\u8054\u624B\u673A\u53F7\u6570(\u4E2A)"
      }, riskDetection.m3_idcard_to_phone_time), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "3\u4E2A\u6708\u624B\u673A\u53F7\u5173\u8054\u8EAB\u4EFD\u8BC1\u6570(\u4E2A)"
      }, riskDetection.m3_phone_to_idcard_time))), /*#__PURE__*/React__default["default"].createElement(Divider$1, {
        name: "\u6CD5\u9662\u98CE\u9669\u4FE1\u606F"
      }), /*#__PURE__*/React__default["default"].createElement(Courtrisklist, {
        dataValidation: uiObj
      }), /*#__PURE__*/React__default["default"].createElement("div", {
        className: "footer"
      }, /*#__PURE__*/React__default["default"].createElement("p", null, "\u62A5\u544A\u4F7F\u7528\u8BF4\u660E:"), /*#__PURE__*/React__default["default"].createElement("div", {
        className: "text"
      }, "1\u3001\u672C\u62A5\u544A\u8457\u4F5C\u6743\u5C5E\u4E8E\u5E03\u5C14\uFF0C\u672A\u7ECF\u6211\u53F8\u6B63\u5F0F\u6587\u4EF6\u8BB8\u53EF\uFF0C\u4E0D\u5F97\u590D\u5236\u3001\u6458\u5F55\u548C\u8F6C\u8F7D\u3002"), /*#__PURE__*/React__default["default"].createElement("div", {
        className: "text"
      }, "2\u3001\u672C\u62A5\u544A\u4EC5\u4F9B\u4F7F\u7528\u8005\u53C2\u8003\uFF0C\u5E03\u5C14\u4E0D\u627F\u62C5\u636E\u6B64\u62A5\u544A\u4EA7\u751F\u7684\u4EFB\u4F55\u6CD5\u5F8B\u8D23\u4EFB\u3002")))));
    }
  }]);

  return TLXProRiskReport;
}(React.Component);

var Divider = (function (_ref) {
  var name = _ref.name,
      style = _ref.style,
      children = _ref.children;
  return /*#__PURE__*/React__default["default"].createElement("div", {
    style: _objectSpread2({}, style)
  }, /*#__PURE__*/React__default["default"].createElement(antd.Divider, {
    style: {
      fontSize: '20px',
      marginTop: 50
    }
  }, name), children);
});

var defaultApiResult = {
  ApplyLoanStr: {},
  //贷前意愿
  conciseResult: {},
  ApplyLoanUsury: {},
  DebtRepayStress: {},
  ExecutionLimited: {},
  ExecutionPro: {},
  Flag: {},
  FraudRelation_g: {},
  RiskStrategy: {},
  Rule: {},
  Score: {},
  SpecialList_c: {}
};

var BaironProRiskReport = /*#__PURE__*/function (_Component) {
  _inherits(BaironProRiskReport, _Component);

  var _super = _createSuper(BaironProRiskReport);

  function BaironProRiskReport() {
    var _this;

    _classCallCheck(this, BaironProRiskReport);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "dataFactory", function () {
      var obj = _this.props.riskReport || defaultApiResult;
      console.log("obj...", obj); //下面为要显示的数据

      return {
        ApplyLoanStr: obj.ApplyLoanStr,
        //贷前意愿
        conciseResult: obj || {},
        ApplyLoanUsury: obj.ApplyLoanUsury,
        DebtRepayStress: obj.DebtRepayStress || {},
        ExecutionLimited: obj.ExecutionLimited || {},
        ExecutionPro: obj.ExecutionPro || {},
        Flag: obj.Flag || {},
        FraudRelation_g: obj.FraudRelation_g || {},
        RiskStrategy: obj.RiskStrategy || {},
        Rule: obj.Rule || {},
        Score: obj.Score || {},
        SpecialList_c: obj.SpecialList_c || {}
      };
    });

    return _this;
  }

  _createClass(BaironProRiskReport, [{
    key: "render",
    value: function render() {
      var uiObj = this.dataFactory();
      var ApplyLoanStr = uiObj.ApplyLoanStr,
          ApplyLoanUsury = uiObj.ApplyLoanUsury,
          DebtRepayStress = uiObj.DebtRepayStress,
          ExecutionLimited = uiObj.ExecutionLimited,
          ExecutionPro = uiObj.ExecutionPro,
          FraudRelation_g = uiObj.FraudRelation_g,
          SpecialList_c = uiObj.SpecialList_c,
          RiskStrategy = uiObj.RiskStrategy;
      var id, cell;

      if (JSON.stringify(SpecialList_c) != "{}") {
        SpecialList_c.id.map(function (value, index) {
          id += value;
        });
        SpecialList_c.cell.map(function (value, index) {
          cell += value;
        });
      }

      var exe;
      console.log(ExecutionLimited, JSON.stringify(ExecutionLimited) != "{}");

      if (JSON.stringify(ExecutionLimited) != "{}" && ExecutionLimited.length != 0) {
        ExecutionLimited.map(function (value, index) {
          value.map(function (va, ind) {
            exe += /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
              label: "\u6CD5\u9662"
            }, va.iname, "-", va.sexname, "-", va.age, "-", va.courtname, "-", va.areaname, va.publishdate, "-", va.datatype, " ");
          });
        });
      }

      var pro;
      console.log(ExecutionPro, JSON.stringify(ExecutionPro) != "{}");

      if (JSON.stringify(ExecutionPro) != "{}" && ExecutionPro.length != 0) {
        ExecutionPro.map(function (value, index) {
          value.map(function (va, ind) {
            pro += /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
              label: "\u6CD5\u9662"
            }, va.casenum, "-", va.concretesituation, "-", va.time, " ");
          });
        });
      }

      return /*#__PURE__*/React__default["default"].createElement("div", {
        className: "booleandataTotalContainer"
      }, /*#__PURE__*/React__default["default"].createElement("div", {
        className: "booleanDataWrap"
      }, /*#__PURE__*/React__default["default"].createElement("div", null, /*#__PURE__*/React__default["default"].createElement(Divider, {
        name: "\u53CD\u6B3A\u8BC8"
      }), /*#__PURE__*/React__default["default"].createElement("div", {
        className: "riskDetection"
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions, {
        column: 3
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u53CD\u6B3A\u8BC8\u8BC4\u5206"
      }, RiskStrategy.ScoreAf.scoreafconsoff, "\u5206(\u5EFA\u8BAE", RiskStrategy.ScoreAf.scoreafconsoff >= 80 ? '拒绝' : '通过', ")"), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u4FE1\u7528\u8BC4\u5206"
      }, RiskStrategy.Score.scoreconson, "\u5206(\u5EFA\u8BAE", RiskStrategy.Score.scoreconson > 575 ? '通过' : RiskStrategy.Score.scoreconson < 370 ? '拒绝' : '复议', ")"), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u6574\u4F53\u5EFA\u8BAE"
      }, RiskStrategy.final_decision == 'Accept' ? '通过' : RiskStrategy.final_decision == 'Reject' ? '拒绝' : '复议'))), /*#__PURE__*/React__default["default"].createElement(Divider, {
        name: "\u507F\u503A\u538B\u529B\u6307\u6570"
      }), /*#__PURE__*/React__default["default"].createElement("div", {
        className: "riskDetection"
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions, {
        column: 1
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u507F\u503A\u538B\u529B\u6307\u6570"
      }, DebtRepayStress.nodebtscore ? DebtRepayStress.nodebtscore : '无', "  \u6570\u503C\u8D8A\u5927,\u538B\u529B\u8D8A\u5927\u3002"))), /*#__PURE__*/React__default["default"].createElement(Divider, {
        name: "\u8D37\u524D\u610F\u613F"
      }), /*#__PURE__*/React__default["default"].createElement("div", {
        className: "basicWrap0423"
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions, {
        column: 2
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u624B\u673A\u53F7\u8DDD\u6700\u65E9\u5728\u94F6\u884C\u673A\u6784\u7533\u8BF7\u95F4\u9694"
      }, ApplyLoanStr.fst.cell.bank.inteday ? ApplyLoanStr.fst.cell.bank.inteday + '天' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u624B\u673A\u53F7\u8DDD\u6700\u65E9\u5728\u975E\u94F6\u884C\u673A\u6784\u7533\u8BF7\u95F4\u9694"
      }, ApplyLoanStr.fst.cell.nbank.inteday ? ApplyLoanStr.fst.cell.nbank.inteday + '天' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8EAB\u4EFD\u8BC1\u8DDD\u6700\u65E9\u5728\u94F6\u884C\u673A\u6784\u7533\u8BF7\u95F4\u9694"
      }, ApplyLoanStr.fst.id.bank.inteday ? ApplyLoanStr.fst.id.bank.inteday + '天' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8EAB\u4EFD\u8BC1\u8DDD\u6700\u65E9\u5728\u975E\u94F6\u884C\u673A\u6784\u7533\u8BF7\u95F4\u9694"
      }, ApplyLoanStr.fst.id.nbank.inteday ? ApplyLoanStr.fst.id.nbank.inteday + '天' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u624B\u673A\u53F7\u8DDD\u6700\u8FD1\u5728\u94F6\u884C\u673A\u6784\u7533\u8BF7\u95F4\u9694"
      }, ApplyLoanStr.lst.cell.bank.inteday ? ApplyLoanStr.fst.cell.bank.inteday + '天' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u624B\u673A\u53F7\u8DDD\u6700\u8FD1\u5728\u975E\u94F6\u884C\u673A\u6784\u7533\u8BF7\u95F4\u9694"
      }, ApplyLoanStr.lst.cell.nbank.inteday ? ApplyLoanStr.fst.cell.nbank.inteday + '天' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8EAB\u4EFD\u8BC1\u8DDD\u6700\u8FD1\u5728\u94F6\u884C\u673A\u6784\u7533\u8BF7\u95F4\u9694"
      }, ApplyLoanStr.lst.id.bank.inteday ? ApplyLoanStr.fst.id.bank.inteday + '天' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8EAB\u4EFD\u8BC1\u8DDD\u6700\u8FD1\u5728\u975E\u94F6\u884C\u673A\u6784\u7533\u8BF7\u95F4\u9694"
      }, ApplyLoanStr.lst.id.nbank.inteday ? ApplyLoanStr.fst.id.nbank.inteday + '天' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u624B\u673A\u53F7\u8FD1\u4E03\u5929\u5728\u94F6\u884C\u673A\u6784\u7533\u8BF7\u95F4\u9694"
      }, ApplyLoanStr.d7.cell ? ApplyLoanStr.d7.cell.bank.inteday + '天' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u624B\u673A\u53F7\u8FD1\u4E03\u5929\u5728\u975E\u94F6\u884C\u673A\u6784\u7533\u8BF7\u95F4\u9694"
      }, ApplyLoanStr.d7.cell ? ApplyLoanStr.d7.cell.nbank.inteday + '天' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8EAB\u4EFD\u8BC1\u8FD1\u4E03\u5929\u5728\u94F6\u884C\u673A\u6784\u7533\u8BF7\u95F4\u9694"
      }, ApplyLoanStr.d7.id ? ApplyLoanStr.d7.id.bank.inteday + '天' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8EAB\u4EFD\u8BC1\u8FD1\u4E03\u5929\u5728\u975E\u94F6\u884C\u673A\u6784\u7533\u8BF7\u95F4\u9694"
      }, ApplyLoanStr.d7.id ? ApplyLoanStr.d7.id.nbank.inteday + '天' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u624B\u673A\u53F7\u8FD115\u5929\u5728\u94F6\u884C\u673A\u6784\u7533\u8BF7\u95F4\u9694"
      }, ApplyLoanStr.d15.cell ? ApplyLoanStr.d15.cell.bank.inteday + '天' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u624B\u673A\u53F7\u8FD115\u5929\u5728\u975E\u94F6\u884C\u673A\u6784\u7533\u8BF7\u95F4\u9694"
      }, ApplyLoanStr.d15.cell ? ApplyLoanStr.d15.cell.nbank.inteday + '天' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8EAB\u4EFD\u8BC1\u8FD115\u5929\u5728\u94F6\u884C\u673A\u6784\u7533\u8BF7\u95F4\u9694"
      }, ApplyLoanStr.d15.id ? ApplyLoanStr.d15.id.bank.inteday + '天' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8EAB\u4EFD\u8BC1\u8FD115\u5929\u5728\u975E\u94F6\u884C\u673A\u6784\u7533\u8BF7\u95F4\u9694"
      }, ApplyLoanStr.d15.id ? ApplyLoanStr.d15.id.nbank.inteday + '天' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u624B\u673A\u53F7\u8FD11\u4E2A\u6708\u5728\u94F6\u884C\u673A\u6784\u7533\u8BF7\u95F4\u9694"
      }, ApplyLoanStr.m1.cell ? ApplyLoanStr.m1.cell.bank.inteday + '天' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u624B\u673A\u53F7\u8FD11\u4E2A\u6708\u5728\u975E\u94F6\u884C\u673A\u6784\u7533\u8BF7\u95F4\u9694"
      }, ApplyLoanStr.m1.cell ? ApplyLoanStr.m1.cell.nbank.inteday + '天' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8EAB\u4EFD\u8BC1\u8FD11\u4E2A\u6708\u5728\u94F6\u884C\u673A\u6784\u7533\u8BF7\u95F4\u9694"
      }, ApplyLoanStr.m1.id ? ApplyLoanStr.m1.id.bank.inteday + '天' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8EAB\u4EFD\u8BC1\u8FD11\u4E2A\u6708\u5728\u975E\u94F6\u884C\u673A\u6784\u7533\u8BF7\u95F4\u9694"
      }, ApplyLoanStr.m1.id ? ApplyLoanStr.m1.id.nbank.inteday + '天' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u624B\u673A\u53F7\u8FD13\u4E2A\u6708\u5728\u94F6\u884C\u673A\u6784\u7533\u8BF7\u6B21\u6570"
      }, ApplyLoanStr.m3.cell ? ApplyLoanStr.m3.cell.bank.allnum + '次' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u624B\u673A\u53F7\u8FD13\u4E2A\u6708\u5728\u5176\u5B83\u673A\u6784\u7533\u8BF7\u6B21\u6570"
      }, ApplyLoanStr.m3.cell ? ApplyLoanStr.m3.cell.oth.allnum + '次' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8EAB\u4EFD\u8BC1\u8FD13\u4E2A\u6708\u5728\u94F6\u884C\u673A\u6784\u7533\u8BF7\u6B21\u6570"
      }, ApplyLoanStr.m3.id ? ApplyLoanStr.m3.id.bank.allnum + '次' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8EAB\u4EFD\u8BC1\u8FD13\u4E2A\u6708\u5728\u5176\u5B83\u673A\u6784\u7533\u8BF7\u6B21\u6570"
      }, ApplyLoanStr.m3.id ? ApplyLoanStr.m3.id.oth.allnum + '次' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u624B\u673A\u53F7\u8FD16\u4E2A\u6708\u5728\u94F6\u884C\u673A\u6784\u7533\u8BF7\u6B21\u6570"
      }, ApplyLoanStr.m6.cell ? ApplyLoanStr.m6.cell.bank.allnum + '次' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u624B\u673A\u53F7\u8FD16\u4E2A\u6708\u5728\u5176\u5B83\u673A\u6784\u7533\u8BF7\u6B21\u6570"
      }, ApplyLoanStr.m6.cell ? ApplyLoanStr.m6.cell.oth.allnum + '次' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8EAB\u4EFD\u8BC1\u8FD16\u4E2A\u6708\u5728\u94F6\u884C\u673A\u6784\u7533\u8BF7\u6B21\u6570"
      }, ApplyLoanStr.m6.id ? ApplyLoanStr.m6.id.bank.allnum + '次' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8EAB\u4EFD\u8BC1\u8FD16\u4E2A\u6708\u5728\u5176\u5B83\u673A\u6784\u7533\u8BF7\u6B21\u6570"
      }, ApplyLoanStr.m6.id ? ApplyLoanStr.m6.id.oth.allnum + '次' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u624B\u673A\u53F7\u8FD112\u4E2A\u6708\u5728\u94F6\u884C\u673A\u6784\u7533\u8BF7\u6B21\u6570"
      }, ApplyLoanStr.m12.cell ? ApplyLoanStr.m12.cell.bank.allnum + '次' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u624B\u673A\u53F7\u8FD112\u4E2A\u6708\u5728\u5176\u5B83\u673A\u6784\u7533\u8BF7\u6B21\u6570"
      }, ApplyLoanStr.m12.cell ? ApplyLoanStr.m12.cell.oth.allnum + '次' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8EAB\u4EFD\u8BC1\u8FD112\u4E2A\u6708\u5728\u94F6\u884C\u673A\u6784\u7533\u8BF7\u6B21\u6570"
      }, ApplyLoanStr.m12.id ? ApplyLoanStr.m12.id.bank.allnum + '次' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8EAB\u4EFD\u8BC1\u8FD112\u4E2A\u6708\u5728\u5176\u5B83\u673A\u6784\u7533\u8BF7\u6B21\u6570"
      }, ApplyLoanStr.m12.id ? ApplyLoanStr.m12.id.oth.allnum + '次' : '无'))), /*#__PURE__*/React__default["default"].createElement(Divider, {
        name: "\u501F\u8D37\u98CE\u9669\u52D8\u6D4B"
      }), /*#__PURE__*/React__default["default"].createElement("div", {
        className: "riskDetection"
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions, {
        column: 2
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8FD17\u5929\u8EAB\u4EFD\u8BC1\u53F7\u67E5\u8BE2\u7684\u7533\u8BF7\u6B21\u6570"
      }, ApplyLoanUsury.d7 ? ApplyLoanUsury.d7.id.allnum + '次' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8FD17\u5929\u624B\u673A\u53F7\u67E5\u8BE2\u7684\u7533\u8BF7\u6B21\u6570"
      }, ApplyLoanUsury.d7 ? ApplyLoanUsury.d7.cell.allnum + '次' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8FD115\u5929\u8EAB\u4EFD\u8BC1\u53F7\u67E5\u8BE2\u7684\u7533\u8BF7\u6B21\u6570"
      }, ApplyLoanUsury.d15 ? ApplyLoanUsury.d15.id.allnum + '次' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8FD115\u5929\u624B\u673A\u53F7\u67E5\u8BE2\u7684\u7533\u8BF7\u6B21\u6570"
      }, ApplyLoanUsury.d15 ? ApplyLoanUsury.d15.cell.allnum + '次' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8FD11\u4E2A\u6708\u8EAB\u4EFD\u8BC1\u53F7\u67E5\u8BE2\u7684\u7533\u8BF7\u6B21\u6570"
      }, ApplyLoanUsury.m1 ? ApplyLoanUsury.m1.id.allnum + '次' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8FD11\u4E2A\u6708\u624B\u673A\u53F7\u67E5\u8BE2\u7684\u7533\u8BF7\u6B21\u6570"
      }, ApplyLoanUsury.m1 ? ApplyLoanUsury.m1.cell.allnum + '次' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8FD13\u4E2A\u6708\u8EAB\u4EFD\u8BC1\u53F7\u67E5\u8BE2\u7684\u7533\u8BF7\u6B21\u6570"
      }, ApplyLoanUsury.m3 ? ApplyLoanUsury.m3.id.allnum + '次' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8FD13\u4E2A\u6708\u624B\u673A\u53F7\u67E5\u8BE2\u7684\u7533\u8BF7\u6B21\u6570"
      }, ApplyLoanUsury.m3 ? ApplyLoanUsury.m3.cell.allnum + '次' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8FD16\u4E2A\u6708\u8EAB\u4EFD\u8BC1\u53F7\u67E5\u8BE2\u7684\u7533\u8BF7\u6B21\u6570"
      }, ApplyLoanUsury.m6 ? ApplyLoanUsury.m6.id.allnum + '次' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8FD16\u4E2A\u6708\u624B\u673A\u53F7\u67E5\u8BE2\u7684\u7533\u8BF7\u6B21\u6570"
      }, ApplyLoanUsury.m6 ? ApplyLoanUsury.m6.cell.allnum + '次' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8FD112\u4E2A\u6708\u8EAB\u4EFD\u8BC1\u53F7\u67E5\u8BE2\u7684\u7533\u8BF7\u6B21\u6570"
      }, ApplyLoanUsury.m12 ? ApplyLoanUsury.m12.id.allnum + '次' : '无'), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8FD112\u4E2A\u6708\u624B\u673A\u53F7\u67E5\u8BE2\u7684\u7533\u8BF7\u6B21\u6570"
      }, ApplyLoanUsury.m12 ? ApplyLoanUsury.m12.cell.allnum + '次' : '无'))), /*#__PURE__*/React__default["default"].createElement(Divider, {
        name: "\u6CD5\u9662\u88AB\u6267\u884C\u4EBA"
      }), /*#__PURE__*/React__default["default"].createElement("div", {
        className: "riskDetection"
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions, {
        column: 1
      }, exe)), /*#__PURE__*/React__default["default"].createElement(Divider, {
        name: "\u6CD5\u9662\u88AB\u6267\u884C\u4EBA"
      }), /*#__PURE__*/React__default["default"].createElement("div", {
        className: "riskDetection"
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions, {
        column: 1
      }, pro)), /*#__PURE__*/React__default["default"].createElement(Divider, {
        name: "\u56E2\u4F19\u6B3A\u8BC8\u6392\u67E5"
      }), /*#__PURE__*/React__default["default"].createElement("div", {
        className: "riskDetection"
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions, {
        column: 2
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u67E5\u8BE2\u4EBA\u6B3A\u8BC8\u56E2\u4F19\u7B49\u7EA7\u53D6\u503C:3-10"
      }, FraudRelation_g.frg_list_level), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u67E5\u8BE2\u4EBA\u6B3A\u8BC8\u56E2\u4F19\u7FA4\u7EC4\u89C4\u6A21"
      }, FraudRelation_g.frg_group_num, "  \u8F93\u51FA\u6B3A\u8BC8\u56E2\u4F19\u7FA4\u7EC4\u89C4\u6A21,\u53D6\u503C:a\u3001b\u3001c\u3001d, a=[1,50\uFF09\u3001b=[50,100\uFF09\u3001c=[100,500\uFF09\u3001d=[500,+\uFF09"))), /*#__PURE__*/React__default["default"].createElement(Divider, {
        name: "\u7279\u6B8A\u540D\u5355\u9A8C\u8BC1"
      }), /*#__PURE__*/React__default["default"].createElement("div", {
        className: "riskDetection"
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions, {
        column: 2
      }, /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u624B\u673A\u53F7\u547D\u4E2D\u98CE\u9669\u6B21\u6570"
      }, id ? id : 0), /*#__PURE__*/React__default["default"].createElement(antd.Descriptions.Item, {
        label: "\u8EAB\u4EFD\u8BC1\u53F7\u547D\u4E2D\u98CE\u9669\u6B21\u6570"
      }, cell ? cell : 0))))));
    }
  }]);

  return BaironProRiskReport;
}(React.Component);

var payMethodCnMap = {
  ZFB: {
    label: "周期扣款",
    help: "\u7528\u6237\u4F7F\u7528\u652F\u4ED8\u5B9D\u652F\u4ED8\uFF0C\u9700\u8981\u5B8C\u6210\"\u5468\u671F\u6263\u6B3E\"\u7B7E\u7EA6",
    help2: "用户使用支付宝支付"
  },
  TTF: {
    label: "银行卡代扣",
    help: "\u7528\u6237\u4F7F\u7528\u94F6\u884C\u5361\u652F\u4ED8\uFF0C\u9700\u8981\u5B8C\u6210\"\u94F6\u884C\u5361\u4EE3\u6263\"\u7B7E\u7EA6",
    help2: "用户使用银行卡支付"
  },
  WX: {
    label: "代扣",
    help: "\u7528\u6237\u4F7F\u7528\u5FAE\u4FE1\u652F\u4ED8\uFF0C\u9700\u8981\u5B8C\u6210\"\u94F6\u884C\u5361\u4EE3\u6263\"\u7B7E\u7EA6\u6216\"\u5468\u671F\u6263\u6B3E\"\u7B7E\u7EA6",
    help2: "用户使用微信支付"
  }
}; // 周期扣款具体文案

var zhouqikoukuanReturnText = function zhouqikoukuanReturnText(resObj, label2) {
  var uoi = resObj.userOrderInfoDto || {};
  var bankPaySigned = uoi.bankPaySigned,
      cyclePaySigned = uoi.cyclePaySigned; // 分别表示银行卡是否签约代扣、支付宝是否签约代扣

  var greenStr = function greenStr(str) {
    return /*#__PURE__*/React__default["default"].createElement("span", {
      style: {
        color: "rgba(82, 196, 26, 1)"
      }
    }, str);
  };

  var redStr = function redStr(str) {
    return /*#__PURE__*/React__default["default"].createElement("span", {
      style: {
        color: "rgba(245, 34, 45, 1)"
      }
    }, str);
  };

  if (label2 === "周期扣款") {
    return cyclePaySigned ? greenStr("已签约") : redStr("未签约");
  } else if (label2 === "银行卡代扣") {
    return bankPaySigned ? greenStr("已签约") : redStr("未签约");
  }
}; // 周期扣款的label标签

var zhouqikoukuanReturnLabel = function zhouqikoukuanReturnLabel(resObj, label2) {
  var paymentMethod = resObj.paymentMethod; // 支付方式

  var obj = payMethodCnMap[paymentMethod] || {};
  obj.label;
      var help2 = obj.help2;
  return /*#__PURE__*/React__default["default"].createElement(React__default["default"].Fragment, null, /*#__PURE__*/React__default["default"].createElement("span", {
    style: {
      marginRight: 2
    }
  }, label2), /*#__PURE__*/React__default["default"].createElement(antd.Tooltip, {
    title: help2
  }, /*#__PURE__*/React__default["default"].createElement(antd.Icon, {
    type: "question-circle"
  })));
};

function getToken() {
  return localStorage.getItem("token");
}

function request$1(url) {
  var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var method = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'post';
  var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  var token = localStorage$1.getToken();

  if (method === 'post') {
    return baseRequest__default["default"](url, {
      method: 'post',
      data: value,
      headers: {
        token: token
      }
    });
  } else if (method === 'get') {
    return baseRequest__default["default"](url, {
      params: value,
      headers: {
        token: token
      }
    }, options);
  } else if (method === 'formdata') {
    return baseRequest__default["default"]({
      method: 'post',
      url: url,
      data: value,
      transformRequest: [function (data) {
        var ret = '';

        for (var it in data) {
          ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&';
        }

        ret = ret.substring(0, ret.length - 1);
        return ret;
      }],
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        token: token
      }
    });
  }
}

var DaiKeZhiFuBtn = /*#__PURE__*/function (_React$Component) {
  _inherits(DaiKeZhiFuBtn, _React$Component);

  var _super = _createSuper(DaiKeZhiFuBtn);

  function DaiKeZhiFuBtn() {
    var _this;

    _classCallCheck(this, DaiKeZhiFuBtn);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "state", {
      showModal: false,
      imgList: [],
      uploadImgUrl: ""
    });

    _defineProperty(_assertThisInitialized(_this), "checkCanClick", function () {
      var _this$props$orderInfo;

      var hitOrderStatus = ['01','11', '13', '04', '05', '06', '07', '08'];
      var orderStatus = (_this$props$orderInfo = _this.props.orderInfo) === null || _this$props$orderInfo === void 0 ? void 0 : _this$props$orderInfo.status;

      if (!hitOrderStatus.includes(orderStatus)) {
        // 只有待审批、待终审、待发货、代确认收货、租用中、待结算、结算代支付才有可能被点击
        return false;
      }

      var _ref = _this.props.periodItem || {},
          status = _ref.status; // 这是账单状态


      var hits = ["1", // 待支付
      "4" // 逾期待支付
      ];
      return hits.includes(status);
    });

    _defineProperty(_assertThisInitialized(_this), "showModal", function () {
      _this.setState({
        showModal: true
      });
    });

    _defineProperty(_assertThisInitialized(_this), "hideModal", function () {
      _this.setState({
        showModal: false
      });
    });

    _defineProperty(_assertThisInitialized(_this), "getImgUrl", function (fullUrl) {
      if (!fullUrl) {
        return "";
      }

      return fullUrl;
    });

    _defineProperty(_assertThisInitialized(_this), "submitDaiKePayHandler", function () {
      var _this$props$periodIte, _this$props$periodIte2;

      var imgUrl = _this.state.uploadImgUrl;

      if (!imgUrl) {
        antd.message.warning("不存在图片链接");
        return;
      }

      console.log(imgUrl);
      var postObj = {
        url: _this.getImgUrl(imgUrl),
        orderId: (_this$props$periodIte = _this.props.periodItem) === null || _this$props$periodIte === void 0 ? void 0 : _this$props$periodIte.orderId,
        period: (_this$props$periodIte2 = _this.props.periodItem) === null || _this$props$periodIte2 === void 0 ? void 0 : _this$props$periodIte2.currentPeriods
      };
      request$1("/hzsx/ope/order/stagesValetPay", postObj, "get").then(function (res) {
        var refetchListDataCb = _this.props.refetchCb;
        refetchListDataCb && refetchListDataCb(); // 重新拉取账期列表数据

        antd.message.success("处理成功");

        _this.setState({
          imgList: [],
          // 初始化图片列表
          showModal: false // 隐藏模态框

        });
      });
    });

    _defineProperty(_assertThisInitialized(_this), "beforeUpload", function (file) {
      var isJPG = file.type === "image/jpeg" || file.type === "image/png" || file.type === "image/gif";

      if (!isJPG) {
        antd.message.error("图片格式不正确");
      }

      var isLt2M = file.size / 1024 / 1024 < 2;

      if (!isLt2M) {
        antd.message.error("图片大于2MB");
      }

      return isJPG && isLt2M;
    });

    _defineProperty(_assertThisInitialized(_this), "handleUploadImage", function (_ref2) {
      var file = _ref2.file,
          fileList = _ref2.fileList;

      if (file.status === "done") {
        var images = fileList.map(function (v, index) {
          if (v.response) {
            var src = v.response.data;
            return {
              uid: index,
              src: src,
              url: src,
              isMain: v.isMain || null
            };
          }

          return v;
        });
        var imgSrc = images && images[0] && images[0].url;

        _this.setState({
          uploadImgUrl: imgSrc
        });
      }

      _this.setState({
        imgList: fileList
      });
    });

    _defineProperty(_assertThisInitialized(_this), "checkCanShowDaiKeZhiFuBtn", function () {
      var obj = _this.props.periodItem || {};
      return obj.paymentMethod === "VALET_PAYMENT";
    });

    return _this;
  }

  _createClass(DaiKeZhiFuBtn, [{
    key: "render",
    value: function render() {
      var _this$props$periodIte3;

      if (this.checkCanShowDaiKeZhiFuBtn()) {
        // 此时是显示待客支付按钮
        return null;
      }

      console.log('img:', this.state.imgList);
      return /*#__PURE__*/React__default["default"].createElement(React__default["default"].Fragment, null, /*#__PURE__*/React__default["default"].createElement(antd.Button, {
        className: "daikezhifubtnclsasa1210",
        onClick: this.showModal,
        disabled: !this.checkCanClick(),
        type: "link"
      }, "\u4EE3\u5BA2\u652F\u4ED8"), /*#__PURE__*/React__default["default"].createElement(antd.Modal, {
        title: "\u4EE3\u5BA2\u652F\u4ED8",
        visible: this.state.showModal,
        onOk: this.submitDaiKePayHandler,
        onCancel: this.hideModal
      }, /*#__PURE__*/React__default["default"].createElement("div", {
        className: "rowsdsjk12"
      }, /*#__PURE__*/React__default["default"].createElement("span", {
        className: "labelsds121s"
      }, "\u652F\u4ED8\u91D1\u989D\uFF1A"), /*#__PURE__*/React__default["default"].createElement("span", {
        className: "valuesdshj121"
      }, (_this$props$periodIte3 = this.props.periodItem) === null || _this$props$periodIte3 === void 0 ? void 0 : _this$props$periodIte3.currentPeriodsRent, "\u5143")), /*#__PURE__*/React__default["default"].createElement("div", {
        className: "rowsdsjk12"
      }, /*#__PURE__*/React__default["default"].createElement("span", {
        className: "labelsds121s"
      }, "\u51ED\u8BC1\uFF1A"), /*#__PURE__*/React__default["default"].createElement(antd.Upload, {
        accept: "image/*",
        action: "/zyj-backstage-web/hzsx/busShop/doUpLoadwebp",
        listType: "picture-card",
        headers: {
          token: getToken()
        },
        fileList: this.state.imgList,
        beforeUpload: this.beforeUpload,
        onChange: this.handleUploadImage
      }, !this.state.imgList || !this.state.imgList.length && /*#__PURE__*/React__default["default"].createElement("div", null, /*#__PURE__*/React__default["default"].createElement(antd.Icon, {
        type: "upload"
      }), /*#__PURE__*/React__default["default"].createElement("div", {
        className: "ant-upload-text"
      }, "\u4E0A\u4F20\u7167\u7247"))))));
    }
  }]);

  return DaiKeZhiFuBtn;
}(React__default["default"].Component);
DaiKeZhiFuBtn.propTypes = {
  periodItem: PropTypes__default["default"].object,
  // 账期对象
  refetchCb: PropTypes__default["default"].func,
  // 刷新账期列表数据
  orderInfo: PropTypes__default["default"].object // 订单对象

}; // export default DaiKeZhiFuBtn

function request(url) {
  var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var method = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'post';
  var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  var token = localStorage$1.getToken();

  if (method === 'post') {
    return baseRequest__default["default"](url, {
      method: 'post',
      data: value,
      headers: {
        token: token
      }
    });
  } else if (method === 'get') {
    return baseRequest__default["default"](url, {
      params: value,
      headers: {
        token: token
      }
    }, options);
  } else if (method === 'formdata') {
    return baseRequest__default["default"]({
      method: 'post',
      url: url,
      data: value,
      transformRequest: [function (data) {
        var ret = '';

        for (var it in data) {
          ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&';
        }

        ret = ret.substring(0, ret.length - 1);
        return ret;
      }],
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        token: token
      }
    });
  }
}

var payMethods = [{
  cn: "预授权扣款",
  val: "AUTH"
} // { cn: "周期扣款", val: "CYCLE" },
// { cn: "银行卡扣款", val: "BANK" },
];
var Daikou = /*#__PURE__*/function (_React$PureComponent) {
  _inherits(Daikou, _React$PureComponent);

  var _super = _createSuper(Daikou);

  function Daikou() {
    var _this;

    _classCallCheck(this, Daikou);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "state", {
      fetching: false,
      // 表示代扣处理中
      show: false,
      // 是否显示模态框
      deductionMethodType: null // 扣款方式

    });

    _defineProperty(_assertThisInitialized(_this), "daikouHandler", function () {
      var _this$props$periodIte, _this$props$periodIte2;

      if (!_this.daikouClickable()) {
        antd.message.warning("当前状态不可进行代扣");
        return;
      }

      var deductionMethodType = _this.state.deductionMethodType;

      if (!deductionMethodType) {
        antd.message.warning("请选中扣款方式");
        return;
      }

      var currentPeriods = (_this$props$periodIte = _this.props.periodItem) === null || _this$props$periodIte === void 0 ? void 0 : _this$props$periodIte.currentPeriods;

      if (!currentPeriods) {
        antd.message.warning("缺少期数，请重试");
        return;
      }

      var orderId = (_this$props$periodIte2 = _this.props.periodItem) === null || _this$props$periodIte2 === void 0 ? void 0 : _this$props$periodIte2.orderId;

      if (!orderId) {
        antd.message.warning("缺少订单ID，请重试");
        return;
      }

      var reqParams = {
        currentPeriods: currentPeriods,
        deductionMethodType: deductionMethodType,
        orderId: orderId
      }; // 请求参数

      var url = "/hzsx/ope/order/stageOrderWithhold";
      var method = "post";

      _this.setState({
        fetching: true
      });

      request(url, reqParams, method).then(function () {
        antd.message.info("处理中，请再稍等大概5秒", 5);
      }).then(_this.shitSleep5S).then(function () {
        antd.message.info("处理完成");
        var cb = _this.props.refetchCb;
        cb && cb();

        _this.reset();
      })["finally"](function () {
        _this.setState({
          fetching: false
        });
      });
    });

    _defineProperty(_assertThisInitialized(_this), "shitSleep5S", function () {
      return new Promise(function (r) {
        setTimeout(r, 5000);
      });
    });

    _defineProperty(_assertThisInitialized(_this), "daikouClickable", function () {
      var _this$props$orderInfo, _this$props$periodIte3;

      // 这是关于订单状态的判断
      var orderStatus = (_this$props$orderInfo = _this.props.orderInfoObj) === null || _this$props$orderInfo === void 0 ? void 0 : _this$props$orderInfo.status;
      var hits = ["01", // 待支付
      "11", // 待初审
      "13", // 待终审
      "10" // 交易关闭
      ];

      if (hits.includes(orderStatus)) {
        return false;
      } // const now = Date.now()
      // const daoqiTime = new Date(this.props.periodItem?.statementDate) // 账单到期时间
      // const isValidTime = daoqiTime.toString() !== "Invalid Date"
      // const dtv = daoqiTime.valueOf()
      // 这是关于期数还款状态的判断


      var status = (_this$props$periodIte3 = _this.props.periodItem) === null || _this$props$periodIte3 === void 0 ? void 0 : _this$props$periodIte3.status; // 账单支付状态

      return !_this.state.fetching && (status == "1" || status == "4") // 待支付相关状态
      ;
    });

    _defineProperty(_assertThisInitialized(_this), "showModal", function () {
      if (!_this.daikouClickable()) return; // 不可点击

      var methodlist = _this.returnPayMethods();

      if (!methodlist || !methodlist.length) {
        antd.message.warning("暂无可发起代扣的签约方式");
        return;
      }

      _this.setState({
        show: true
      });
    });

    _defineProperty(_assertThisInitialized(_this), "reset", function () {
      _this.setState({
        show: false
      }); // this.setState({ show: false, deductionMethodType: null })

    });

    _defineProperty(_assertThisInitialized(_this), "setKoukuangMethodHandler", function (e) {
      var v = e.target.value;

      _this.setState({
        deductionMethodType: v
      });
    });

    _defineProperty(_assertThisInitialized(_this), "returnPayMethods", function () {
      return payMethods; // const methodListTemp = this.props.apiRes?.deductionMethodTypes || [] // 用户支付方式
      // const result = payMethods.filter(obj => {
      //   const t = obj.val
      //   return methodListTemp.includes(t)
      // })
      // return result
    });

    return _this;
  }

  _createClass(Daikou, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      // 设置默认的扣款方式
      var methodList = this.returnPayMethods();

      if (methodList && methodList.length) {
        var m0 = methodList[0];
        m0 && m0.val && this.setState({
          deductionMethodType: m0.val
        });
      }
    }
    /**
     * 代扣的处理方法
     */

  }, {
    key: "render",
    value: function render() {
      var methodArr = this.returnPayMethods(); // 支付方式

      return /*#__PURE__*/React__default["default"].createElement(React__default["default"].Fragment, null, /*#__PURE__*/React__default["default"].createElement(antd.Button, {
        className: "daikouBtnsds1100293",
        type: "link",
        onClick: this.showModal,
        disabled: !this.daikouClickable()
      }, "\u53D1\u8D77\u4EE3\u6263"), /*#__PURE__*/React__default["default"].createElement(antd.Modal, {
        title: "\u8BF7\u9009\u62E9\u6263\u6B3E\u65B9\u5F0F",
        visible: this.state.show,
        onOk: this.daikouHandler,
        onCancel: this.reset,
        okButtonProps: {
          loading: this.state.fetching,
          disabled: this.state.fetching
        }
      }, /*#__PURE__*/React__default["default"].createElement(antd.Radio.Group, {
        onChange: this.setKoukuangMethodHandler,
        value: this.state.deductionMethodType
      }, methodArr.map(function (obj, idx) {
        return /*#__PURE__*/React__default["default"].createElement(antd.Radio, {
          key: idx,
          value: obj.val
        }, obj.cn);
      }))));
    }
  }]);

  return Daikou;
}(React__default["default"].PureComponent);

exports.BaironProRiskReport = BaironProRiskReport;
exports.DaiKeZhiFuBtn = DaiKeZhiFuBtn;
exports.Daikou = Daikou;
exports.RiskReportHisttoryRent = RiskReportHisttoryRent;
exports.TLXProRiskReport = TLXProRiskReport;
exports.UserRating = UserRating;
exports.YouhuiMoneyWithTooltip = YouhuiMoneyWithTooltip;
exports.zhouqikoukuanReturnLabel = zhouqikoukuanReturnLabel;
exports.zhouqikoukuanReturnText = zhouqikoukuanReturnText;
