/**
 * 天狼星风控报告
 */
import React from "react"
import FourItemapply from './components//PublicCard/FourItemapply'
import FiveItem from './components/PublicCard/FiveItem'
import FourItem from './components/PublicCard/FourItem'
import FiveItemrepay from './components/PublicCard/FiveItemrepay'
import ThreeItem from './components/PublicCard/ThreeItem'
import Historyoverdue from './components/Historyoverdue'
import CourtriskList from './components/CourtriskList'
import styles from "./index.scss"


export class TLXRiskReport extends React.Component {
  render() {
    const siriusResp = this.props.reportData // 风控报告接口所返回的数据
    // TODO: 应该不存在这个siriusRespData
    
    let redata = (siriusResp && siriusResp.resp_data) || {};
    let personal_loan_demand = redata.personal_loan_demand || {};
    let { wx_datad7, wx_datam1, wx_datam3 } = personal_loan_demand;

    let applyQuerySeven = wx_datad7 || [];
    let applyQuerythrity = wx_datam1 || [];
    let applyQueryninety = wx_datam3 || [];
    const applyQuerySevendata = [];
    const applyQuerythritydata = [];
    const applyQueryninetydata = [];
    for (let key in applyQuerySeven) {
      const seven = {};
      seven.day = applyQuerySeven[key];
      applyQuerySevendata.push(seven);
    }
    for (let key in applyQuerythrity) {
      const seven = {};
      seven.day = applyQuerythrity[key];
      applyQuerythritydata.push(seven);
    }
    for (let key in applyQueryninety) {
      const seven = {};
      seven.day = applyQueryninety[key];
      applyQueryninetydata.push(seven);
    }
    const sevenDays = [{ title: '近7天', dataIndex: 'day' }];
    const monthDays = [{ title: '近30天', dataIndex: 'day' }];
    const ninetyDays = [{ title: '近90天', dataIndex: 'day' }];
    const applycolumns = [{ title: '', dataIndex: 'name' }];
    const applyData = apply.map((item, sign) => {
      const newsItem = { ...item };
      const keys = sign + 1;
      newsItem.key = keys;
      return newsItem;
    });
    const sevenData = applyQuerySevendata.map((item, sign) => {
      const newsItem = { ...item };
      const keys = sign + 1;
      newsItem.key = keys;
      return newsItem;
    });
    const monthData = applyQuerythritydata.map((item, sign) => {
      const newsItem = { ...item };
      const keys = sign + 1;
      newsItem.key = keys;
      return newsItem;
    });
    const ninetyData = applyQueryninetydata.map((item, sign) => {
      const newsItem = { ...item };
      const keys = sign + 1;
      newsItem.key = keys;
      return newsItem;
    });

    let signLess ={
      display: "flex",
      flexDirection: "column",
      textAlign: "left",
      fontSize: 14,
      color: '#333'
    }
    const riskSign = (
      <div className={signLess}>
        {redata &&
          redata.hit_risk_tagging &&
          redata.hit_risk_tagging.map((item, sign) => {
            return (
              <span key={sign}>
                {sign + 1}、{item}
              </span>
            );
          })}
      </div>
    );
    const scoreStandard = (
      <div className={signLess}>
        <span>分值在(0-100)之间，得分越高，风险越大:</span>
        <span>(0-30)，建议通过；</span>
        <span>[30-80)，建议审核；</span>
        <span>[80-100)，建议拒绝；</span>
      </div>
    );
    const titleName = {
      first: '消费分期类申请机构数 (个) ',
      second: '网络贷款类申请机构数 (个) ',
      three: '最近一次申请日期',
      four: '距离最近一次申请日期已有 (天) ',
    };
    const fiveTitle = {
      first: '',
      second: '近1个月',
      three: '近3个月',
      four: '近6个月',
      five: '近12个月',
    };
    const fiveTitlerepay = {
      first: '',
      second: '近1个月',
      three: '近3个月',
      four: '近6个月',
    };
    const nameObject = {
      first: '消费分期类放款机构数 (个) ',
      second: '网络贷款类放款机构数 (个) ',
      three: '最近一次放款日期',
      four: '距离最近一次放款日期已有 (天) ',
    };
    const threeName = {
      first: '近1个月',
      second: '近12个月',
      observeNum: '履约次数(次)',
      unobserveNum: '还款异常次数(次)',
    };

    return (
      <div>
        {/* {siriusRespData.length === 2 ? (
            <div
              style={{
                textAlign: 'right',
                marginBottom: '30px',
              }}
            >
              <Radio.Group defaultValue={0} buttonStyle="solid" onChange={this.onRButton}>
                {siriusRespData.map((res, val) => {
                  return <Radio.Button value={val}>{res.name}风控报告</Radio.Button>;
                })}
              </Radio.Group>
            </div>
          ) : null} */}
          {/** FIXME: 下面这个length是一定存在的 */}
          {siriusRespData.length ? (
            <div>
              <Descriptions column={4} layout="vertical" bordered>
                <Descriptions.Item label="综合评分">
                  <span style={{ fontSize: 20, color: '#333', fontWeight: 'bold' }}>
                    {redata.multiple_score}
                  </span>
                </Descriptions.Item>
                <Descriptions.Item label="审核建议">
                  {redata.multiple_score < 30 ? (
                    <span style={{ color: '#07880E', fontSize: 16 }}>建议通过</span>
                  ) : null}
                  {redata.multiple_score >= 30 && redata.multiple_score < 80 ? (
                    <span style={{ color: '#07880E', fontSize: 16 }}>建议审核</span>
                  ) : null}
                  {redata.multiple_score >= 80 ? (
                    <span style={{ color: '#FF3C2D', fontSize: 16 }}>建议拒绝</span>
                  ) : null}
                </Descriptions.Item>
                <Descriptions.Item label="命中风险标注">{riskSign}</Descriptions.Item>
                <Descriptions.Item label="分值标注说明">{scoreStandard}</Descriptions.Item>
              </Descriptions>
              <Divider style={{ fontSize: '20px', marginTop: 50 }}>基本信息</Divider>
              <Descriptions column={2}>
                <Descriptions.Item label="姓名">
                  {redata.base_info && redata.base_info.name}
                </Descriptions.Item>
                <Descriptions.Item label="身份证号">
                  {redata.base_info && redata.base_info.ident_number}
                </Descriptions.Item>
                <Descriptions.Item label="手机号">
                  {redata.base_info && redata.base_info.phone}
                </Descriptions.Item>
                <Descriptions.Item label="年龄">
                  {redata.base_info && redata.base_info.age}
                </Descriptions.Item>
                <Descriptions.Item label="户籍">
                  {redata.base_info && redata.base_info.id_card_city}
                </Descriptions.Item>
                <Descriptions.Item label="号码归属地">
                  {redata.base_info && redata.base_info.mobile_address_city}
                </Descriptions.Item>
              </Descriptions>
              <Divider style={{ fontSize: '20px', marginTop: 50 }}>风险名单监测</Divider>
              <Descriptions column={2}>
                <Descriptions.Item label="特殊关注名单">
                  {redata.risk_list_check &&
                  redata.risk_list_check.personal_fraud_blacklist === '命中' ? (
                    <img
                      src="https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg"
                      alt="error"
                    />
                  ) : (
                    '未命中'
                  )}
                </Descriptions.Item>
                <Descriptions.Item label="归属地位于高风险集中地区">
                  {redata.risk_list_check &&
                  redata.risk_list_check.census_register_hign_risk_area === '命中' ? (
                    <img
                      src="https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg"
                      alt="error"
                    />
                  ) : (
                    '未命中'
                  )}
                </Descriptions.Item>
                <Descriptions.Item label="法院失信名单">
                  {redata.risk_list_check &&
                  redata.risk_list_check.court_break_faith_list === '命中' ? (
                    <img
                      src="https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg"
                      alt="error"
                    />
                  ) : (
                    '未命中'
                  )}
                </Descriptions.Item>
                <Descriptions.Item label="犯罪通缉名单">
                  {redata.risk_list_check &&
                  redata.risk_list_check.crime_manhunt_list === '命中' ? (
                    <img
                      src="https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg"
                      alt="error"
                    />
                  ) : (
                    '未命中'
                  )}
                </Descriptions.Item>
                <Descriptions.Item label="法院执行名单">
                  {redata.risk_list_check &&
                  redata.risk_list_check.court_execute_list === '命中' ? (
                    <img
                      src="https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg"
                      alt="error"
                    />
                  ) : (
                    '未命中'
                  )}
                </Descriptions.Item>
                <Descriptions.Item label="助学贷款欠费历史">
                  {redata.risk_list_check &&
                  redata.risk_list_check.student_loan_arrearage_list === '命中' ? (
                    <img
                      src="https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg"
                      alt="error"
                    />
                  ) : (
                    '未命中'
                  )}
                </Descriptions.Item>
                <Descriptions.Item label="信贷逾期名单">
                  {redata.risk_list_check &&
                  redata.risk_list_check.credit_overdue_list === '命中' ? (
                    <img
                      src="https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg"
                      alt="error"
                    />
                  ) : (
                    '未命中'
                  )}
                </Descriptions.Item>
                <Descriptions.Item label="高风险关注名单">
                  {redata.risk_list_check &&
                  redata.risk_list_check.hign_risk_focus_list === '命中' ? (
                    <img
                      src="https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg"
                      alt="error"
                    />
                  ) : (
                    '未命中'
                  )}
                </Descriptions.Item>
                <Descriptions.Item label="车辆租赁违约名单">
                  {redata.risk_list_check &&
                  redata.risk_list_check.car_rental_break_contract_list === '命中' ? (
                    <img
                      src="https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg"
                      alt="error"
                    />
                  ) : (
                    '未命中'
                  )}
                </Descriptions.Item>
                <Descriptions.Item label="法院结案名单">
                  {redata.risk_list_check && redata.risk_list_check.court_case_list === '命中' ? (
                    <img
                      src="https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg"
                      alt="error"
                    />
                  ) : (
                    '未命中'
                  )}
                </Descriptions.Item>
                <Descriptions.Item label="故意违章乘车名单">
                  {redata.risk_list_check &&
                  redata.risk_list_check.riding_break_contract_list === '命中' ? (
                    <img
                      src="https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg"
                      alt="error"
                    />
                  ) : (
                    '未命中'
                  )}
                </Descriptions.Item>
                <Descriptions.Item label="欠税名单">
                  {redata.risk_list_check && redata.risk_list_check.owing_taxes_list === '命中' ? (
                    <img
                      src="https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg"
                      alt="error"
                    />
                  ) : (
                    '未命中'
                  )}
                </Descriptions.Item>
                <Descriptions.Item label="欠税公司法人代表名单">
                  {redata.risk_list_check &&
                  redata.risk_list_check.owing_taxes_legal_person_list === '命中' ? (
                    <img
                      src="https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg"
                      alt="error"
                    />
                  ) : (
                    '未命中'
                  )}
                </Descriptions.Item>
                <Descriptions.Item label="虚拟号码库">
                  {redata.risk_list_check &&
                  redata.risk_list_check.virtual_number_base === '命中' ? (
                    <img
                      src="https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg"
                      alt="error"
                    />
                  ) : (
                    '未命中'
                  )}
                </Descriptions.Item>
                <Descriptions.Item label="通信小号库">
                  {redata.risk_list_check && redata.risk_list_check.small_number_base === '命中' ? (
                    <img
                      src="https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg"
                      alt="error"
                    />
                  ) : (
                    '未命中'
                  )}
                </Descriptions.Item>
              </Descriptions>
              <Divider style={{ fontSize: '20px', marginTop: 50 }}>机构查询记录</Divider>
              <div className={styles.tableHeader}>
                <Table
                  bordered={true}
                  columns={applycolumns}
                  dataSource={applyData}
                  pagination={false}
                />
                <Table
                  locale={{ emptyText: '暂无记录' }}
                  bordered={true}
                  columns={sevenDays}
                  dataSource={sevenData}
                  pagination={false}
                />
                <Table
                  locale={{ emptyText: '暂无记录' }}
                  bordered={true}
                  columns={monthDays}
                  dataSource={monthData}
                  pagination={false}
                />
                <Table
                  locale={{ emptyText: '暂无记录' }}
                  bordered={true}
                  columns={ninetyDays}
                  dataSource={ninetyData}
                  pagination={false}
                />
              </div>

              <Divider style={{ fontSize: '20px', marginTop: 50 }}>历史借贷行为</Divider>
              <div className={styles.historyWrap}>
                <div className={styles.total}>
                  <span style={{ fontWeight: 'bold' }}>近12个月申请机构总数3&emsp;(个)：</span>
                  <span>
                    {redata &&
                    redata.personal_loan_s &&
                    redata.personal_loan_s.apply_mechanism_number === '无记录'
                      ? 0
                      : redata &&
                        redata.personal_loan_s &&
                        redata.personal_loan_s.apply_mechanism_number}
                  </span>
                </div>
                <FourItemapply
                  style={{ marginTop: 14 }}
                  title={titleName}
                  data={redata && redata.personal_loan_s}
                />
                <FiveItem
                  style={{ marginTop: 22 }}
                  title={fiveTitle}
                  text="申请次数(次)"
                  data={redata && redata.personal_loan_s}
                />
                <div className={styles.total}>
                  <span style={{ fontWeight: 'bold' }}>近12个月放款机构总数&emsp;(个)：</span>
                  <span>
                    {redata && redata.personal_loan_f && redata.personal_loan_f.lenders === '无记录'
                      ? 0
                      : redata && redata.personal_loan_f && redata.personal_loan_f.lenders}
                  </span>
                </div>
                <FourItem
                  style={{ marginTop: 14 }}
                  title={nameObject}
                  data={redata && redata.personal_loan_f}
                />
                <FiveItemrepay
                  style={{ marginTop: 22 }}
                  title={fiveTitlerepay}
                  text="放款次数(次)"
                  data={redata && redata.personal_loan_f}
                />
                <ThreeItem
                  style={{ marginTop: 22 }}
                  title={threeName}
                  data={redata && redata.personal_loan_f}
                />
              </div>
              <Divider style={{ fontSize: '20px', marginTop: 50 }}>历史逾期记录</Divider>
              <Historyoverdue data={redata && redata.personal_overdue_history} />
              <Divider style={{ fontSize: '20px', marginTop: 50 }}>关联风险检测</Divider>

              <Descriptions column={1}>
                <Descriptions.Item label="3个月身份证关联手机号数(个)">
                  {redata &&
                  redata.relevance_risk_check &&
                  redata.relevance_risk_check.ident_to_phone_counts === '无记录'
                    ? 0
                    : redata &&
                      redata.relevance_risk_check &&
                      redata.relevance_risk_check.ident_to_phone_counts}
                </Descriptions.Item>
                <Descriptions.Item label="3个月手机号关联身份证数(个)">
                  {redata &&
                  redata.relevance_risk_check &&
                  redata.relevance_risk_check.phone_to_ident_counts === '无记录'
                    ? 0
                    : redata &&
                      redata.relevance_risk_check &&
                      redata.relevance_risk_check.phone_to_ident_counts}
                </Descriptions.Item>
              </Descriptions>
              <Divider style={{ fontSize: '20px', marginTop: 50 }}>法院风险信息</Divider>
              <CourtriskList data={redata && redata.court_risk_info_list} />
            </div>
          ) : (
            <span style={{ fontWeight: 700 }}>
              黑名单{' '}
              <img
                src="https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg"
                alt="error"
              />
            </span>
          )}
      </div>
    )
  }
}
