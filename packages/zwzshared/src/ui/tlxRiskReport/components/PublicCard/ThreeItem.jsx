import React, { Component } from 'react';
import styles from './index.less';
import { Descriptions } from 'antd';

class Threeitem extends Component {
  render() {
    const { style, title,data={} } = this.props;

    return (
      <div className={styles.threeWrap} style={{ ...style }}>
        <Descriptions column={5} layout="vertical" bordered>
          <Descriptions.Item label=""><span style={{ fontWeight: 'bold' }}>{title.observeNum}</span></Descriptions.Item>
          <Descriptions.Item label={title.first}>{data.repay_succ1 === '无记录' ? 0 : data.repay_succ1}</Descriptions.Item>
          <Descriptions.Item label={title.second}>{data.repay_succ12 === '无记录' ? 0 : data.repay_succ12}</Descriptions.Item>
        </Descriptions>
        <div className={styles.threeAbnormal}>
          <span style={{ fontWeight: 'bold' }}>{title.unobserveNum}</span>
          <span>{data.repay_fail1 === '无记录' ? 0 : data.repay_fail1}</span>
          <span>{data.repay_fail12 === '无记录' ? 0 : data.repay_fail12}</span>
        </div>
      </div>
    )
  }
}
export default Threeitem;