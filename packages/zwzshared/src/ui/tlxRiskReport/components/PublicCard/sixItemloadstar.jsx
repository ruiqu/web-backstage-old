import React, { Component } from 'react';
import styles from './index.less';
import { Descriptions } from 'antd';
import { connect } from 'dva';
@connect(({ dataValidation, apply_details }) => ({ dataValidation, apply_details }))
class Fiveitem extends Component {
  render() {
    const { title, style, text } = this.props;
    const { dataValidation: { apply_details } } = this.props;
    return (
      <div className={styles.fiveWrap} style={{ ...style }}>
        <Descriptions column={6} layout="vertical" bordered>
          <Descriptions.Item label={title.first}><span style={{ fontWeight: 'bold' }}>{text}</span></Descriptions.Item>
          <Descriptions.Item label={title.second}>{apply_details.d7_apply_times === '无记录' || apply_details.d7_apply_times === '' ? 0 : apply_details.d7_apply_times}</Descriptions.Item>
          <Descriptions.Item label={title.three}>{apply_details.m1_apply_times === '无记录' || apply_details.m1_apply_times === '' ? 0 : apply_details.m1_apply_times}</Descriptions.Item>
          <Descriptions.Item label={title.four}>{apply_details.m3_apply_times === '无记录' || apply_details.m3_apply_times === '' ? 0 : apply_details.m3_apply_times}</Descriptions.Item>
          <Descriptions.Item label={title.five}>{apply_details.m6_apply_times === '无记录' || apply_details.m6_apply_times === '' ? 0 : apply_details.m6_apply_times}</Descriptions.Item>
          {/* <Descriptions.Item label={title.six}>{apply_details.m12_apply_times === '无记录' || apply_details.m12_apply_times === '' ? 0 : apply_details.m12_apply_times}</Descriptions.Item> */}
        </Descriptions>
      </div>
    )
  }
}
export default Fiveitem;