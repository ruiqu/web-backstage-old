import React, { Component } from 'react';
import styles from './index.less';
import { Descriptions } from 'antd';

class Fiveitem extends Component {
  render() {
    const { title, style, text,data={} } = this.props;
  
    return (
      <div className={styles.fiveWrap} style={{ ...style }}>
        <Descriptions column={5} layout="vertical" bordered>
          <Descriptions.Item label={title.first}><span style={{ fontWeight: 'bold' }}>{text}</span></Descriptions.Item>
          <Descriptions.Item label={title.second}>{data.apply_time1 === '无记录' ? 0 : data.apply_time1}</Descriptions.Item>
          <Descriptions.Item label={title.three}>{data.apply_time3 === '无记录' ? 0 : data.apply_time3}</Descriptions.Item>
          <Descriptions.Item label={title.four}>{data.apply_time6 === '无记录' ? 0 : data.apply_time6}</Descriptions.Item>
          <Descriptions.Item label={title.five}>{data.apply_time12 === '无记录' ? 0 : data.apply_time12}</Descriptions.Item>
        </Descriptions>
      </div>
    )
  }
}
export default Fiveitem;