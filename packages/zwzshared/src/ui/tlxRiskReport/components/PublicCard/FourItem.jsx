import React, { Component } from 'react';
import styles from './index.less';
import { Descriptions } from 'antd';


class FourItem extends Component {
  render() {
    const { style, title,data={} } = this.props;
    
    return (
      <div className={styles.fourWrap} style={{ ...style }}>
        <Descriptions column={4} layout="vertical" bordered>
          <Descriptions.Item label={title.first}>{data.cflenders === '无记录' ? '----' : data.cflenders}</Descriptions.Item>
          <Descriptions.Item label={title.second}>{data.nllenders === '无记录' ? '----' : data.nllenders}</Descriptions.Item>
          <Descriptions.Item label={title.three}>{data.lend_time === '无记录' ? '----' : data.lend_time}</Descriptions.Item>
          <Descriptions.Item label={title.four}>{data.distance_now_days === '无记录' ? '----' : data.distance_now_days}</Descriptions.Item>
        </Descriptions>
      </div>
    )
  }
}
export default FourItem;