import React, { Component } from 'react';
import styles from './index.less';
import { Descriptions } from 'antd';
import { connect } from 'dva';
@connect(({ dataValidation, loan_details }) => ({ dataValidation, loan_details }))
class Fiveitem extends Component {
  render() {
    const { title, style, text } = this.props;
    const { dataValidation: { loan_details } } = this.props;
    return (
      <div className={styles.fiveWrap} style={{ ...style }}>
        <Descriptions column={5} layout="vertical" bordered>
          <Descriptions.Item label={title.first}><span style={{ fontWeight: 'bold' }}>{text}</span></Descriptions.Item>
          <Descriptions.Item label={title.second}>{loan_details.d7_lend_number === '无记录' || loan_details.d7_lend_number === '' ? 0 : loan_details.d7_lend_number}</Descriptions.Item>
          <Descriptions.Item label={title.three}>{loan_details.m1_lend_number === '无记录' || loan_details.m1_lend_number === '' ? 0 : loan_details.m1_lend_number}</Descriptions.Item>
          <Descriptions.Item label={title.four}>{loan_details.m3_lend_number === '无记录' || loan_details.m3_lend_number === '' ? 0 : loan_details.m3_lend_number}</Descriptions.Item>
          <Descriptions.Item label={title.five}>{loan_details.m6_lend_number === '无记录' || loan_details.m6_lend_number === '' ? 0 : loan_details.m6_lend_number}</Descriptions.Item>
        </Descriptions>
      </div>
    )
  }
}
export default Fiveitem;