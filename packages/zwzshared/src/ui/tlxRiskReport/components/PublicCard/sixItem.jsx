import React, { Component } from 'react';
import styles from './index.less';
import { Descriptions } from 'antd';
import { connect } from 'dva';
@connect(({ dataValidation, apply_details }) => ({ dataValidation, apply_details }))
class Fiveitem extends Component {
  render() {
    const { title, style, text } = this.props;
    const { dataValidation: { apply_details } } = this.props;
    console.log(apply_details)
    return (
      <div className={styles.fiveWrap} style={{ ...style }}>
        <Descriptions column={6} layout="vertical" bordered>
          <Descriptions.Item label={title.first}><span style={{ fontWeight: 'bold' }}>{text}</span></Descriptions.Item>
          <Descriptions.Item label={title.second}>{apply_details.apply_timed7 === '无记录' || apply_details.apply_timed7 === '' ? 0 : apply_details.apply_timed7}</Descriptions.Item>
          <Descriptions.Item label={title.three}>{apply_details.apply_timem1 === '无记录' || apply_details.apply_timem1 === '' ? 0 : apply_details.apply_timem1}</Descriptions.Item>
          <Descriptions.Item label={title.four}>{apply_details.apply_timem3 === '无记录' || apply_details.apply_timem3 === '' ? 0 : apply_details.apply_timem3}</Descriptions.Item>
          <Descriptions.Item label={title.five}>{apply_details.apply_timem6 === '无记录' || apply_details.apply_timem6 === '' ? 0 : apply_details.apply_timem6}</Descriptions.Item>
          {/* <Descriptions.Item label={title.six}>{apply_details.apply_timem12 === '无记录' || apply_details.apply_timem12 === '' ? 0 : apply_details.apply_timem12}</Descriptions.Item> */}
        </Descriptions>
      </div>
    )
  }
}
export default Fiveitem;