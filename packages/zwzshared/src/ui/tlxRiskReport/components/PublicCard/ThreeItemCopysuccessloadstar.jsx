import React, { Component } from 'react';
import styles from './index.less';
import { Descriptions } from 'antd';
import { connect } from 'dva';
@connect(({ dataValidation, d7_repayment, m1_repayment, m3_repayment, m6_repayment }) => ({ dataValidation, d7_repayment, m1_repayment, m3_repayment, m6_repayment }))
class Threeitem extends Component {
  render() {
    const { style, title } = this.props;
    const { dataValidation: { d7_repayment, m1_repayment, m3_repayment, m6_repayment } } = this.props;
    return (
      <div className={styles.threeWrap} style={{ ...style }}>
        <Descriptions column={5} layout="vertical" bordered>
          <Descriptions.Item label=""><span style={{ fontWeight: 'bold' }}>{title.observeNum}</span></Descriptions.Item>
          <Descriptions.Item label={title.first}>{d7_repayment.d7_repay_succ_cnt === '无记录' || d7_repayment.d7_repay_succ_cnt === '' ? 0 : d7_repayment.d7_repay_succ_cnt}</Descriptions.Item>
          <Descriptions.Item label={title.second}>{m1_repayment.m1_repay_succ_cnt === '无记录' || m1_repayment.m1_repay_succ_cnt === '' ? 0 : m1_repayment.m1_repay_succ_cnt}</Descriptions.Item>
          <Descriptions.Item label={title.three}>{m3_repayment.m3_repay_succ_cnt === '无记录' || m3_repayment.m3_repay_succ_cnt === '' ? 0 : m3_repayment.m3_repay_succ_cnt}</Descriptions.Item>
          <Descriptions.Item label={title.four}>{m6_repayment.m6_repay_succ_cnt === '无记录' || m6_repayment.m6_repay_succ_cnt === '' ? 0 : m6_repayment.m6_repay_succ_cnt}</Descriptions.Item>
        </Descriptions>
        <div className={styles.abnormal}>
          <span style={{ fontWeight: 'bold' }}>{title.unobserveNum}</span>
          <span>{d7_repayment.d7_repay_succ_money === '无记录' || d7_repayment.d7_repay_succ_money === '' ? 0 : d7_repayment.d7_repay_succ_money}</span>
          <span>{m1_repayment.m1_repay_succ_money === '无记录' || m1_repayment.m1_repay_succ_money === '' ? 0 : m1_repayment.m1_repay_succ_money}</span>

          <span>{m3_repayment.m3_repay_succ_money === '无记录' || m3_repayment.m3_repay_succ_money === '' ? 0 : m3_repayment.m3_repay_succ_money}</span>

          <span>{m6_repayment.m6_repay_succ_money === '无记录' || m6_repayment.m6_repay_succ_money === '' ? 0 : m6_repayment.m6_repay_succ_money}</span>
        </div>
      </div>
    )
  }
}
export default Threeitem;