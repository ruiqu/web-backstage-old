import React, { Component } from 'react';
import styles from './index.less';
import { Descriptions } from 'antd';
import { connect } from 'dva';
@connect(({ dataValidation, rentalInformation }) => ({ dataValidation, rentalInformation }))
class Leaseinformation extends Component {
  render() {
    const { title, style, text } = this.props;
    const { dataValidation: { rentalInformation } } = this.props;
    return (
      <div className={styles.fiveWrap} style={{ ...style }}>
        <Descriptions column={5} layout="vertical" bordered>
          <Descriptions.Item label={title.first}><span style={{ fontWeight: 'bold' }}>{text}</span></Descriptions.Item>
          <Descriptions.Item label={title.second}>{rentalInformation.times_d7 === '无记录' ? 0 : rentalInformation.times_d7}</Descriptions.Item>
          <Descriptions.Item label={title.three}>{rentalInformation.times_m1 === '无记录' ? 0 : rentalInformation.times_m1}</Descriptions.Item>
          <Descriptions.Item label={title.four}>{rentalInformation.times_m3 === '无记录' ? 0 : rentalInformation.times_m3}</Descriptions.Item>
          <Descriptions.Item label={title.five}>{rentalInformation.times_m6 === '无记录' ? 0 : rentalInformation.times_m6}</Descriptions.Item>
        </Descriptions>
      </div>
    )
  }
}
export default Leaseinformation;