import React, { Component } from 'react';
import styles from './index.less';
import { Descriptions } from 'antd';
import { connect } from 'dva';
@connect(({ dataValidation, repay_succ_money, repay_succ_times }) => ({ dataValidation, repay_succ_money, repay_succ_times }))
class Threeitem extends Component {
  render() {
    const { style, title } = this.props;
    const { dataValidation: { repay_succ_money, repay_succ_times } } = this.props;
    return (
      <div className={styles.threeWrap} style={{ ...style }}>
        <Descriptions column={5} layout="vertical" bordered>
          <Descriptions.Item label=""><span style={{ fontWeight: 'bold' }}>{title.observeNum}</span></Descriptions.Item>
          <Descriptions.Item label={title.first}>{repay_succ_times.d7 === '无记录' || repay_succ_times.d7 === '' ? 0 : repay_succ_times.d7}</Descriptions.Item>
          <Descriptions.Item label={title.second}>{repay_succ_times.m1 === '无记录' || repay_succ_times.m1 === '' ? 0 : repay_succ_times.m1}</Descriptions.Item>
          <Descriptions.Item label={title.three}>{repay_succ_times.m3 === '无记录' || repay_succ_times.m3 === '' ? 0 : repay_succ_times.m3}</Descriptions.Item>
          <Descriptions.Item label={title.four}>{repay_succ_times.m6 === '无记录' || repay_succ_times.m6 === '' ? 0 : repay_succ_times.m6}</Descriptions.Item>
        </Descriptions>
        <div className={styles.abnormal}>
          <span style={{ fontWeight: 'bold' }}>{title.unobserveNum}</span>
          <span>{repay_succ_money.d7 === '无记录' || repay_succ_money.d7 === '' ? 0 : repay_succ_money.d7}</span>
          <span>{repay_succ_money.m1 === '无记录' || repay_succ_money.m1 === '' ? 0 : repay_succ_money.m1}</span>
          <span>{repay_succ_money.m3 === '无记录' || repay_succ_money.m3 === '' ? 0 : repay_succ_money.m3}</span>
          <span>{repay_succ_money.m6 === '无记录' || repay_succ_money.m6 === '' ? 0 : repay_succ_money.m6}</span>
        </div>
      </div>
    )
  }
}
export default Threeitem;