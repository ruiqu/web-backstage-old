import React, { Component } from 'react';
import styles from './index.less';
import { Descriptions } from 'antd';

class Fiveitem extends Component {
  render() {
    const { title, style, text,data={} } = this.props;

    return (
      <div className={styles.fiveWrap} style={{ ...style }}>
        <Descriptions column={5} layout="vertical" bordered>
          <Descriptions.Item label={title.first}><span style={{ fontWeight: 'bold' }}>{text}</span></Descriptions.Item>
          <Descriptions.Item label={title.second}>{data.lend_number1 === '无记录' ? 0 : data.lend_number1}</Descriptions.Item>
          <Descriptions.Item label={title.three}>{data.lend_number3 === '无记录' ? 0 : data.lend_number3}</Descriptions.Item>
          <Descriptions.Item label={title.four}>{data.lend_number6 === '无记录' ? 0 : data.lend_number6}</Descriptions.Item>
        </Descriptions>
      </div>
    )
  }
}
export default Fiveitem;