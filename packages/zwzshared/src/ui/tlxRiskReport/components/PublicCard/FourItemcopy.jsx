import React, { Component } from 'react';
import styles from './index.less';
import { Descriptions } from 'antd';
import { connect } from 'dva';
@connect(({ dataValidation, loan_details }) => ({ dataValidation, loan_details }))
class FourItem extends Component {
  render() {
    const { style, title } = this.props;
    const { dataValidation: { loan_details } } = this.props;
    return (
      <div className={styles.fourWrap} style={{ ...style }}>
        <Descriptions column={4} layout="vertical" bordered>
          <Descriptions.Item label={title.first}>{loan_details.cflenders === '无记录' || loan_details.cflenders === '' ? '----' : loan_details.cflenders}</Descriptions.Item>
          <Descriptions.Item label={title.second}>{loan_details.nllenders === '无记录' || loan_details.nllenders === '' ? '----' : loan_details.nllenders}</Descriptions.Item>
          <Descriptions.Item label={title.three}>{loan_details.settlement_number === '无记录' || loan_details.settlement_number === '' ? '----' : loan_details.settlement_number}</Descriptions.Item>
          <Descriptions.Item label={title.four}>{loan_details.loanday === '无记录' || loan_details.loanday === '' ? '----' : loan_details.loanday}</Descriptions.Item>
        </Descriptions>
      </div>
    )
  }
}
export default FourItem;