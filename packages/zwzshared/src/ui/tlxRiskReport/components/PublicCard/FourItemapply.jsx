import React, { Component } from 'react';
import styles from './index.less';
import { Descriptions } from 'antd';

class FourItem extends Component {
  render() {
    const { style, title,data={} } = this.props;

    return (
      <div className={styles.fourWrap} style={{ ...style }}>
        <Descriptions column={4} layout="vertical" bordered>
          <Descriptions.Item label={title.first}>{data.consumer_apply_mechanism_number === '无记录' ? '----' : data.consumer_apply_mechanism_number}</Descriptions.Item>
          <Descriptions.Item label={title.second}>{data.network_loan_apply_mechanis_mnumber === '无记录' ? '----' : data.network_loan_apply_mechanis_mnumber}</Descriptions.Item>
          <Descriptions.Item label={title.three}>{data.last_apply_time === '无记录' ? '----' : data.last_apply_time}</Descriptions.Item>
          <Descriptions.Item label={title.four}>{data.distance_now_days === '无记录' ? '----' : data.distance_now_days}</Descriptions.Item>
        </Descriptions>
      </div>
    )
  }
}
export default FourItem;