import React, { Component } from 'react';
import styles from './index.less';
import { Descriptions } from 'antd';
import { connect } from 'dva';
@connect(({ dataValidation, liability }) => ({ dataValidation, liability }))
class Fiveitem extends Component {
  render() {
    const { title, style, text } = this.props;
    const { dataValidation: { liability } } = this.props;
    return (
      <div className={styles.fiveWrap} style={{ ...style }}>
        <Descriptions column={5} layout="vertical" bordered>
          <Descriptions.Item label={title.first}><span style={{ fontWeight: 'bold' }}>{text}</span></Descriptions.Item>
          <Descriptions.Item label={title.second}>{liability.d7 === '无记录' || liability.d7 === '' ? 0 : liability.d7}</Descriptions.Item>
          <Descriptions.Item label={title.three}>{liability.m1 === '无记录' || liability.m1 === '' ? 0 : liability.m1}</Descriptions.Item>
          <Descriptions.Item label={title.four}>{liability.m3 === '无记录' || liability.m3 === '' ? 0 : liability.m3}</Descriptions.Item>
          <Descriptions.Item label={title.five}>{liability.m6 === '无记录' || liability.m6 === '' ? 0 : liability.m6}</Descriptions.Item>
        </Descriptions>
      </div>
    )
  }
}
export default Fiveitem;