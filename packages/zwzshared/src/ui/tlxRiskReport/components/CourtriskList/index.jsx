import React, { Component } from 'react';
import styles from './index.less';
import { Descriptions, Table } from 'antd';

class Courtrisklist extends Component {
  render() {
    const {data=[]} = this.props;
    const columns = [
      { title: '序号', dataIndex: 'key' },
      { title: '审结日期', dataIndex: 'sort_time_string' },
      { title: '类型', dataIndex: 'data_type' },
      { title: '摘要说明', dataIndex: 'summary' },
      { title: '匹配度', dataIndex: 'compatibility' }
    ];
    const customData = data.map((item, sign) => {
      const newsItem = { ...item };
      const keys = sign + 1;
      newsItem.key = keys;
      return newsItem;
    });
    return (
      <div className={styles.tableHeader}>
        <Table
          locale={{ emptyText: '暂无记录' }}
          bordered={true}
          columns={columns}
          dataSource={customData}
          pagination={false}
        />
      </div>
    )
  }
}

export default Courtrisklist;