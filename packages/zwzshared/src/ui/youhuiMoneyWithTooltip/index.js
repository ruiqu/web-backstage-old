import React from "react"
import { Icon, Tooltip } from "antd"

export class YouhuiMoneyWithTooltip extends React.PureComponent {
  render() {
    const { money, youhuiList } = this.props
    if (Object.prototype.toString.call(youhuiList) !== "[object Array]") return money
    return (
      <div>
        {money}&nbsp;
        <Tooltip title={() => {
          return (
            <div>
              {
                youhuiList && youhuiList.length ?
                  youhuiList.map((obj, idx) => (
                    <div key={idx}>{obj.couponName}: {obj.discountAmount}元</div>
                  ))
                  :
                  "未使用优惠券"
              }
            </div>
          )
        }}>
          <Icon type="question-circle" />
        </Tooltip>
      </div>
    )
  }
}
