import React from "react"
import { Button, Modal, Upload, message, Icon } from "antd"
import PropTypes from "prop-types"
import { getToken } from "../../../../ope/src/utils/localStorage"
import request from "../../../../ope/src/services/baseService"
import "./index.scss"

export class DaiKeZhiFuBtn extends React.Component {
  state = {
    showModal: false,
    imgList: [],
    uploadImgUrl: "",
  }

  /**
   * 判断按钮是否可以点击，只有待支付和逾期待支付是可点击的
   */
  checkCanClick = () => {
    const hitOrderStatus = [
      '11',
      '13',
      '04',
      '05',
      '06',
      '07',
      '08',
    ]
    const orderStatus = this.props.orderInfo?.status
    if (!hitOrderStatus.includes(orderStatus)) { // 只有待审批、待终审、待发货、代确认收货、租用中、待结算、结算代支付才有可能被点击
      return false
    }
    const { status } = this.props.periodItem || {} // 这是账单状态
    const hits = [
      "1", // 待支付
      "4", // 逾期待支付
    ]
    return hits.includes(status)
  }

  // 点击显示支付弹窗
  showModal = () => {
    this.setState({ showModal: true })
  }

  // 隐藏模态框
  hideModal = () => {
    this.setState({ showModal: false })
  }

  // 获取到符合传参要求的图片链接
  getImgUrl = fullUrl => {
    if (!fullUrl) {
      return ""
      
    }
    return fullUrl
    const arr = fullUrl.split("https://rich-rent.oss-cn-hangzhou.aliyuncs.com/")
    return arr[1]
  }

  // 提交待客支付请求
  submitDaiKePayHandler = () => {
    const imgUrl = this.state.uploadImgUrl
    if (!imgUrl) {
      message.warning("不存在图片链接")
      return
    }
    console.log(imgUrl)
    const postObj = {
      url: this.getImgUrl(imgUrl),
      orderId: this.props.periodItem?.orderId,
      period: this.props.periodItem?.currentPeriods,
    }
    request("/hzsx/ope/order/stagesValetPay", postObj, "get").then(res => {
      const refetchListDataCb = this.props.refetchCb
      refetchListDataCb && refetchListDataCb() // 重新拉取账期列表数据
      message.success("处理成功")
      this.setState({
        imgList: [], // 初始化图片列表
        showModal: false, // 隐藏模态框
      })
    })
  }

  // 在上传之前进行处理
  beforeUpload = file => {
    const isJPG = file.type === "image/jpeg" || file.type === "image/png" || file.type === "image/gif"
    if (!isJPG) {
      message.error("图片格式不正确")
    }
    const isLt2M = file.size / 1024 / 1024 < 2
    if (!isLt2M) {
      message.error("图片大于2MB")
    }
    return isJPG && isLt2M
  }

  // 上传完之后的处理方法
  handleUploadImage = ({ file, fileList }) => {
    if (file.status === "done") {
      const images = fileList.map((v, index) => {
        if (v.response) {
          const src = v.response.data
          return { uid: index, src: src, url: src, isMain: v.isMain || null }
        }
        return v
      })
      const imgSrc = images && images[0] && images[0].url
      this.setState({ uploadImgUrl: imgSrc })
    }
    this.setState({ imgList: fileList })
  }

  // 判断是否显示待客支付按钮
  checkCanShowDaiKeZhiFuBtn = () => {
    const obj = this.props.periodItem || {}
    return obj.paymentMethod === "VALET_PAYMENT"
  }

  render() {
    if (this.checkCanShowDaiKeZhiFuBtn()) { // 此时是显示待客支付按钮
      return null
    }
console.log('img:',this.state.imgList);
    return (
      <>
        <Button
          className="daikezhifubtnclsasa1210"
          onClick={this.showModal}
          disabled={!this.checkCanClick()}
          type="link"
        >
          代客支付
        </Button>

        <Modal
          title="代客支付"
          visible={this.state.showModal}
          onOk={this.submitDaiKePayHandler}
          onCancel={this.hideModal}
        >
          <div className="rowsdsjk12">
            <span className="labelsds121s">支付金额：</span>
            <span className="valuesdshj121">{ this.props.periodItem?.currentPeriodsRent }元</span>
          </div>

          <div className="rowsdsjk12">
            <span className="labelsds121s">凭证：</span>
            <Upload
              accept="image/*"
              action="/hzsx/busShop/doUpLoadwebp"
              listType="picture-card"
              headers={{
                token: getToken(),
              }}
              fileList={this.state.imgList}
              beforeUpload={this.beforeUpload}
              onChange={this.handleUploadImage}
            >
              {
                !this.state.imgList || !this.state.imgList.length && (
                  <div>
                    <Icon type="upload" />
                    <div className="ant-upload-text">上传照片</div>
                  </div>
                )
              }
            </Upload>
          </div>
        </Modal>
      </>
    )
  }
}

DaiKeZhiFuBtn.propTypes = {
  periodItem: PropTypes.object, // 账期对象
  refetchCb: PropTypes.func, // 刷新账期列表数据
  orderInfo: PropTypes.object, // 订单对象
}

// export default DaiKeZhiFuBtn