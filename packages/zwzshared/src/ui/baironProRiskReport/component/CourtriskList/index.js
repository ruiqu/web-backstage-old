import React, { Component } from 'react';
import './index.scss';
import { Table } from 'antd';

class Courtrisklist extends Component {
  render() {
    const { dataValidation: { riskInformation } } = this.props;
    const data_typeType = {
      "cpws": "裁判文书",
      "ktgg": "开庭公告",
      "ajlc": "案件流程信息",
      "fygg": "法院公告",
      "shixin": "失信公告",
      "zxgg": "执行公告",
      "bgt": "曝光台",
      };
    const columns = [
      { title: '序号', dataIndex: 'key' },
      { title: '审结日期', dataIndex: 'sort_time_string' },
      { title: '类型', dataIndex: 'data_type',
        render: (data_type, row, index) => {
          return data_typeType[data_type]
        }
      },
      { title: '摘要说明', dataIndex: 'summary' },
      { title: '匹配度', dataIndex: 'compatibility' }
    ];
    const customData = riskInformation.map((item, sign) => {
      const newsItem = { ...item };
      const keys = sign + 1;
      newsItem.key = keys;
      return newsItem;
    });
    return (
      <div className="tableHeader1212">
        <Table
          locale={{ emptyText: '暂无记录' }}
          bordered={true}
          columns={columns}
          dataSource={customData}
          pagination={false}
        />
      </div>
    )
  }
}

export default Courtrisklist;