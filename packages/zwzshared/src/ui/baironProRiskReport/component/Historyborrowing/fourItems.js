import React, { Component } from 'react';
import './index2.scss';
import { Descriptions } from 'antd';

class FourItem extends Component {
  render() {
    const { style, title, makeLoans } = this.props;
    return (
      <div className="fourWrap0425" style={{ ...style }}>
        <Descriptions column={4} layout="vertical" bordered>
          <Descriptions.Item label={title.first}>{makeLoans.cflenders === '无记录' ? '----' : makeLoans.cflenders}</Descriptions.Item>
          <Descriptions.Item label={title.second}>{makeLoans.nllenders === '无记录' ? '----' : makeLoans.nllenders}</Descriptions.Item>
          <Descriptions.Item label={title.three}>{makeLoans.lend_time === '无记录' ? '----' : makeLoans.lend_time}</Descriptions.Item>
          <Descriptions.Item label={title.four}>{makeLoans.distance_now_days === '无记录' ? '----' : makeLoans.distance_now_days}</Descriptions.Item>
        </Descriptions>
      </div>
    )
  }
}

export default FourItem;