import React, { Component } from 'react';
import './index2.scss';
import { Descriptions } from 'antd';

class FourItemapplys extends Component {
  render() {
    const { style, title } = this.props;
    const { dataValidation: { applyFor } } = this.props;
    return (
      <div className="fourWrap0425" style={{ ...style }}>
        <Descriptions column={4} layout="vertical" bordered>
          <Descriptions.Item label={title.first}>{applyFor.consumer_apply_mechanism_number === '无记录' ? '----' : applyFor.consumer_apply_mechanism_number}</Descriptions.Item>
          <Descriptions.Item label={title.second}>{applyFor.network_loan_apply_mechanis_mnumber === '无记录' ? '----' : applyFor.network_loan_apply_mechanis_mnumber}</Descriptions.Item>
          <Descriptions.Item label={title.three}>{applyFor.last_apply_time === '无记录' ? '----' : applyFor.last_apply_time}</Descriptions.Item>
          <Descriptions.Item label={title.four}>{applyFor.last_apply_time_distance === '无记录' ? '----' : applyFor.last_apply_time_distance}</Descriptions.Item>
        </Descriptions>
      </div>
    )
  }
}
export default FourItemapplys;