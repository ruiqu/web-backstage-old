import React, { Component } from 'react';
import './index2.scss';
import { Descriptions } from 'antd';
class Threeitem extends Component {
  render() {
    const { style, title } = this.props;
    const { dataValidation: { makeLoans } } = this.props;
    return (
      <div className="threeWrap" style={{ ...style }}>
        <Descriptions column={5} layout="vertical" bordered>
          <Descriptions.Item label=""><span style={{ fontWeight: 'bold' }}>{title.observeNum}</span></Descriptions.Item>
          <Descriptions.Item label={title.first}>{makeLoans.repay_succ1 === '无记录' ? 0 : makeLoans.repay_succ1}</Descriptions.Item>
          <Descriptions.Item label={title.second}>{makeLoans.repay_succ12 === '无记录' ? 0 : makeLoans.repay_succ12}</Descriptions.Item>
        </Descriptions>
        <div className="threeAbnormal">
          <span style={{ fontWeight: 'bold' }}>{title.unobserveNum}</span>
          <span>{makeLoans.repay_fail1 === '无记录' ? 0 : makeLoans.repay_fail1}</span>
          <span>{makeLoans.repay_fail12 === '无记录' ? 0 : makeLoans.repay_fail12}</span>
        </div>
      </div>
    )
  }
}
export default Threeitem;