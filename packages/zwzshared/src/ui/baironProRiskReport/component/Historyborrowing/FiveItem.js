import React, { Component } from 'react';
import './index2.scss';
import { Descriptions } from 'antd';
class Fiveitem extends Component {
  render() {
    const { title, style, text } = this.props;
    const { dataValidation: { liability, applyFor } } = this.props;
    return (
      <div className="fiveWrap121" style={{ ...style }}>
        <Descriptions column={5} layout="vertical" bordered>
          <Descriptions.Item label={title.first}><span style={{ fontWeight: 'bold' }}>{text}</span></Descriptions.Item>
          <Descriptions.Item label={title.second}>{applyFor.apply_time1 === '无记录' ? 0 : applyFor.apply_time1}</Descriptions.Item>
          <Descriptions.Item label={title.three}>{applyFor.apply_time3 === '无记录' ? 0 : applyFor.apply_time3}</Descriptions.Item>
          <Descriptions.Item label={title.four}>{applyFor.apply_time6 === '无记录' ? 0 : applyFor.apply_time6}</Descriptions.Item>
          <Descriptions.Item label={title.five}>{applyFor.apply_time12 === '无记录' ? 0 : applyFor.apply_time12}</Descriptions.Item>
        </Descriptions>
      </div>
    )
  }
}
export default Fiveitem;