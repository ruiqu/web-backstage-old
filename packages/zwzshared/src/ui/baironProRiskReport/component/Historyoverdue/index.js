import React, { Component } from 'react';
import './index.scss';
import { Descriptions, Table } from 'antd';

class Historyoverdue extends Component {
  render() {
    const { dataValidation: { overdueRecord, overdueRecordDataList } } = this.props;
    const columns = [
      { title: '序号', dataIndex: 'key' },
      { title: '逾期金额区间(元)', dataIndex: 'overdue_money' },
      { title: '逾期时间', dataIndex: 'overdue_time' },
      { title: '逾期时长', dataIndex: 'overdue_day' },
      {
        title: '是否结清', dataIndex: 'settlement',
        render: (settlement) => {
          return (
            <>
              {
                settlement === 'N' ? '否' : null
              }
              {
                settlement === 'Y' ? '是' : null
              }
            </>
          )
        }
      }
    ];
    const customData = overdueRecordDataList.map((item, sign) => {
      const newsItem = { ...item };
      const keys = sign + 1;
      newsItem.key = keys;
      return newsItem;
    });
    return (
      <div className="overdueWrap">
        <Descriptions column={2}>
          <Descriptions.Item label="近6个月逾期机构次数">{overdueRecord.overdue_mechanism_number === '' ? 0 : overdueRecord.overdue_mechanism_number}</Descriptions.Item>
          <Descriptions.Item label="近6个月逾期总次数">{overdueRecord.counts === '' ? 0 : overdueRecord.counts}</Descriptions.Item>
          <Descriptions.Item label="近6个月未结清逾期次数">{overdueRecord.uncleared_counts === '' ? 0 : overdueRecord.uncleared_counts}</Descriptions.Item>
          <Descriptions.Item label="近6个月逾期总金额(元)">{overdueRecord.overdue_money === '' ? 0 : overdueRecord.overdue_money}</Descriptions.Item>
        </Descriptions>
        <div className="tableHeader">
          <Table
            locale={{ emptyText: '暂' }}
            bordered={true}
            columns={columns}
            dataSource={customData}
            pagination={false}
          />
        </div>
        <div className="text">说明：S代表期数，1期=7天，s0表示不到7天、s1代表7-14天，以此类推；M代表期数，1期=30天，mo表示不到30天，ml代表30-60天，以此类推。</div>
      </div>
    )
  }
}
export default Historyoverdue;