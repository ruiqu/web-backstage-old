import React, { Component } from 'react';
import './index.scss';
import { Table } from 'antd';
import { apply } from './data';

class ApplyLoan extends Component {
  render() {
    const { dataValidation: { ApplyLoanStr } } = this.props;

    return (
      <div className="basicWrap0423">
        <Descriptions column={2}>
          <Descriptions.Item label='最近七天'>{basicInformation.d7}</Descriptions.Item>
          <Descriptions.Item label='最近15天'>{basicInformation.d15}</Descriptions.Item>
          <Descriptions.Item label='最近一个月'>{basicInformation.m1}</Descriptions.Item>
          <Descriptions.Item label='最近三个月'>{basicInformation.m3}</Descriptions.Item>
          <Descriptions.Item label='最近六个月'>{basicInformation.m6}</Descriptions.Item>
          <Descriptions.Item label='最近十二个月'>{basicInformation.m12}</Descriptions.Item>
        </Descriptions>
      </div>
    )
  }
}

export default ApplyLoan;