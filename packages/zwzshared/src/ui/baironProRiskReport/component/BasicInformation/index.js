import React, { Component } from 'react';
import './index.scss';
import { Descriptions } from 'antd';

class BasicInformation extends Component {
  render() {
    const { dataValidation: { basicInformation } } = this.props;

    return (
      <div className="basicWrap0423">
        <Descriptions column={2}>
          <Descriptions.Item label='姓名'>{basicInformation.name}</Descriptions.Item>
          <Descriptions.Item label='身份证号'>{basicInformation.ident_number}</Descriptions.Item>
          <Descriptions.Item label='手机号'>{basicInformation.phone}</Descriptions.Item>
          <Descriptions.Item label='年龄'>{basicInformation.age}</Descriptions.Item>
          <Descriptions.Item label='户籍'>{basicInformation.ident_number_address}</Descriptions.Item>
          <Descriptions.Item label='号码归属地'>{basicInformation.phone_address}</Descriptions.Item>
        </Descriptions>
      </div>
    )
  }
}

export default BasicInformation;