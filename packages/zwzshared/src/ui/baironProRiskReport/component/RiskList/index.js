import React, { Component } from 'react';
import './index.scss';
import { Descriptions } from 'antd';

class RiskList extends Component {
  render() {
    const { dataValidation: { riskMonitoring } } = this.props;
    return (
      <div className="descriptionsWrap0424">
        <Descriptions column={2}>
          <Descriptions.Item label="特殊关注名单">
            {
              riskMonitoring.result_xd === 1 ? <img src='https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg' alt='error' /> : '未命中'
            }
          </Descriptions.Item>
          <Descriptions.Item label="归属地位于高风险集中地区">
            {
              riskMonitoring.census_register_high_risk_area === 1 ? <img src='https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg' alt='error' /> : '未命中'
            }
          </Descriptions.Item>
          <Descriptions.Item label="法院失信名单">
            {
              riskMonitoring.idcard_hit_fysx === 1 ? <img src='https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg' alt='error' /> : '未命中'
            }
          </Descriptions.Item>
          <Descriptions.Item label="犯罪通缉名单">
            {
              riskMonitoring.idcard_hit_fztj === 1 ? <img src='https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg' alt='error' /> : '未命中'
            }
          </Descriptions.Item>
          <Descriptions.Item label="法院执行名单">
            {
              riskMonitoring.idcard_hit_fyzx === 1 ? <img src='https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg' alt='error' /> : '未命中'
            }
          </Descriptions.Item>
          <Descriptions.Item label="助学贷款欠费历史">
            {
              riskMonitoring.idcard_hit_zxdkqf === 1 ? <img src='https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg' alt='error' /> : '未命中'
            }
          </Descriptions.Item>
          <Descriptions.Item label="信贷逾期名单">
            {
              riskMonitoring.idcard_hit_xdyq === 1 ? <img src='https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg' alt='error' /> : '未命中'
            }
          </Descriptions.Item>
          <Descriptions.Item label="高风险关注名单">
            {
              riskMonitoring.idcard_hit_gfxgz === 1 ? <img src='https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg' alt='error' /> : '未命中'
            }
          </Descriptions.Item>
          <Descriptions.Item label="车辆租赁违约名单">
            {
              riskMonitoring.idcard_hit_clzlwy === 1 ? <img src='https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg' alt='error' /> : '未命中'
            }
          </Descriptions.Item>
          <Descriptions.Item label="法院结案名单">
            {
              riskMonitoring.idcard_hit_fyja === 1 ? <img src='https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg' alt='error' /> : '未命中'
            }
          </Descriptions.Item>
          <Descriptions.Item label="故意违章乘车名单">
            {
              riskMonitoring.idcard_hit_gywzcc === 1 ? <img src='https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg' alt='error' /> : '未命中'
            }
          </Descriptions.Item>
          <Descriptions.Item label="欠税名单">
            {
              riskMonitoring.idcard_hit_qs === 1 ? <img src='https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg' alt='error' /> : '未命中'
            }
          </Descriptions.Item>
          <Descriptions.Item label="欠税公司法人代表名单">
            {
              riskMonitoring.idcard_hit_qsgsfrdb === 1 ? <img src='https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg' alt='error' /> : '未命中'
            }
          </Descriptions.Item>
          <Descriptions.Item label="虚拟号码库">
            {
              riskMonitoring.phone_hit_xjhm === 1 ? <img src='https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg' alt='error' /> : '未命中'
            }
          </Descriptions.Item>
          <Descriptions.Item label="通信小号库">
            {
              riskMonitoring.phone_hit_txxh === 1 ? <img src='https://booleandata-open.oss-cn-shanghai.aliyuncs.com/dataValidation%20/shoot.svg' alt='error' /> : '未命中'
            }
          </Descriptions.Item>
        </Descriptions>
      </div>
    )
  }
}
export default RiskList;