import React, { Component } from "react"
import "./index.scss"
import { Descriptions } from "antd"
import Divider from "./component/Divider/index"
import { defaultApiResult } from "./data"

export class BaironProRiskReport1 extends Component {
  // 对接口所返回的数据进行处理,接口返回数据的siriusRiskReport作为riskReport这个props
  dataFactory = () => {
    const obj = this.props.riskReport || defaultApiResult
    console.log("obj...",obj)
    //下面为要显示的数据
    return {
      TelCheck_s: obj.TelCheck_s ,  //贷前意愿
    }
  }

  render() {

    

    const uiObj = this.dataFactory()
    const { TelCheck_s,TelPeriod } = uiObj
    return (
      <div className="booleandataTotalContainer">
        <div className="booleanDataWrap">
          <div>
          <Divider name="手机三要素" />
            <div className="riskDetection">
              <Descriptions column={4}>
                <Descriptions.Item label="运营商">
                {TelCheck_s.operation =="1" ? '电信' :TelCheck_s.operation =="2" ?'联通':TelCheck_s.operation =="3" ?'移动':'其它'}
                </Descriptions.Item>
                
                <Descriptions.Item label="一致性">
                {TelCheck_s.result =="1" ? '一致' :TelCheck_s.result =="2" ?'不一致':'查无此号'}
                </Descriptions.Item>

                <Descriptions.Item label="在网时长">
                {TelPeriod.data.value=="1"?'0-6':TelPeriod.data.value=="2"?'6-12':TelPeriod.data.value=="3"?'12-24':'24+'}
                </Descriptions.Item>
                <Descriptions.Item label="在网状态">
                {TelStatus.data.value=="1"?'正常':TelPeriod.data.value=="2"?'停机':TelPeriod.data.value=="3"?'销号':'异常'}
                </Descriptions.Item>
              </Descriptions>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default BaironProRiskReport1
