import React, { Component } from "react"
import "./index.scss"
import { Descriptions } from "antd"
import Divider from "./component/Divider/index"
import { defaultApiResult } from "./data"

export class BaironProRiskReport extends Component {
  // 对接口所返回的数据进行处理,接口返回数据的siriusRiskReport作为riskReport这个props
  dataFactory = () => {
    const obj = this.props.riskReport || defaultApiResult
    console.log("obj...",obj)
    //下面为要显示的数据
    return {
      ApplyLoanStr: obj.ApplyLoanStr ,  //贷前意愿
      conciseResult: obj || {},
      ApplyLoanUsury: obj.ApplyLoanUsury ,
      DebtRepayStress: obj.DebtRepayStress || {},
      ExecutionLimited: obj.ExecutionLimited || {},
      ExecutionPro: obj.ExecutionPro || {},
      Flag: obj.Flag || {},
      FraudRelation_g: obj.FraudRelation_g || {},
      RiskStrategy: obj.RiskStrategy || {},
      Rule: obj.Rule || {},
      Score: obj.Score || {},
      SpecialList_c: obj.SpecialList_c || {},
    }
  }

  render() {
    const uiObj = this.dataFactory()
    const { ApplyLoanStr,ApplyLoanUsury,DebtRepayStress,ExecutionLimited,ExecutionPro,FraudRelation_g,SpecialList_c,RiskStrategy } = uiObj

    let id,cell;
    if(JSON.stringify(SpecialList_c) != "{}"){
      SpecialList_c.id.map((value, index) => {
        id+=value
      })
      SpecialList_c.cell.map((value, index) => {
        cell+=value
      })
    }

    let exe;
    console.log(ExecutionLimited,JSON.stringify(ExecutionLimited) != "{}")
    if(JSON.stringify(ExecutionLimited) != "{}"&&ExecutionLimited.length!=0){
      ExecutionLimited.map((value, index) => {
        value.map((va,ind)=>{
          exe+= <Descriptions.Item label="法院">{va.iname}-{va.sexname}-{va.age}-{va.courtname}-{va.areaname}{va.publishdate}-{va.datatype} </Descriptions.Item>
        })
      })
    }

    let pro;
    console.log(ExecutionPro,JSON.stringify(ExecutionPro) != "{}")
    if(JSON.stringify(ExecutionPro) != "{}"&&ExecutionPro.length!=0){
      ExecutionPro.map((value, index) => {
        value.map((va,ind)=>{
          pro+= <Descriptions.Item label="法院">{va.casenum}-{va.concretesituation}-{va.time} </Descriptions.Item>
        })
      })
    }
    return (
      <div className="booleandataTotalContainer">
        <div className="booleanDataWrap">
          <div>
          <Divider name="反欺诈" />
            <div className="riskDetection">
              <Descriptions column={3}>
                <Descriptions.Item label="反欺诈评分">
                {RiskStrategy.ScoreAf.scoreafconsoff }分(建议
                {RiskStrategy.ScoreAf.scoreafconsoff >=80 ? '拒绝' :'通过'})
                </Descriptions.Item>
                <Descriptions.Item label="信用评分">
                {RiskStrategy.Score.scoreconson }分(建议
                  {RiskStrategy.Score.scoreconson >575 ? '通过' :RiskStrategy.Score.scoreconson <370  ? '拒绝':'复议'})
                </Descriptions.Item>
                <Descriptions.Item label="整体建议">
                {RiskStrategy.final_decision =='Accept' ? '通过' :RiskStrategy.final_decision =='Reject' ? '拒绝':'复议'}
                </Descriptions.Item>
              </Descriptions>
            </div>


          <Divider name="偿债压力指数" />
            <div className="riskDetection">
              <Descriptions column={1}>
                <Descriptions.Item label="偿债压力指数">
                {DebtRepayStress.nodebtscore ? DebtRepayStress.nodebtscore :'无'}  数值越大,压力越大。
                </Descriptions.Item>
              </Descriptions>
            </div>
            
            <Divider name="贷前意愿" />
            <div className="basicWrap0423">
              <Descriptions column={2}>
                <Descriptions.Item label='手机号距最早在银行机构申请间隔'>
                  {ApplyLoanStr.fst.cell.bank.inteday? ApplyLoanStr.fst.cell.bank.inteday+'天' :'无'}
                  </Descriptions.Item>
                <Descriptions.Item label='手机号距最早在非银行机构申请间隔'>
                  {ApplyLoanStr.fst.cell.nbank.inteday? ApplyLoanStr.fst.cell.nbank.inteday+'天' :'无'}
                  </Descriptions.Item>
                <Descriptions.Item label='身份证距最早在银行机构申请间隔'>
                  {ApplyLoanStr.fst.id.bank.inteday? ApplyLoanStr.fst.id.bank.inteday+'天' :'无'}
                  </Descriptions.Item>
                <Descriptions.Item label='身份证距最早在非银行机构申请间隔'>
                  {ApplyLoanStr.fst.id.nbank.inteday? ApplyLoanStr.fst.id.nbank.inteday+'天' :'无'}
                  </Descriptions.Item>
                <Descriptions.Item label='手机号距最近在银行机构申请间隔'>
                  {ApplyLoanStr.lst.cell.bank.inteday? ApplyLoanStr.fst.cell.bank.inteday+'天' :'无'}
                  </Descriptions.Item>
                <Descriptions.Item label='手机号距最近在非银行机构申请间隔'>
                  {ApplyLoanStr.lst.cell.nbank.inteday? ApplyLoanStr.fst.cell.nbank.inteday+'天' :'无'}
                  </Descriptions.Item>
                <Descriptions.Item label='身份证距最近在银行机构申请间隔'>
                  {ApplyLoanStr.lst.id.bank.inteday? ApplyLoanStr.fst.id.bank.inteday+'天' :'无'}
                  </Descriptions.Item>
                <Descriptions.Item label='身份证距最近在非银行机构申请间隔'>
                  {ApplyLoanStr.lst.id.nbank.inteday? ApplyLoanStr.fst.id.nbank.inteday+'天' :'无'}
                  </Descriptions.Item>

                <Descriptions.Item label='手机号近七天在银行机构申请间隔'>
                  {ApplyLoanStr.d7.cell? ApplyLoanStr.d7.cell.bank.inteday+'天' : '无'}
                  </Descriptions.Item>
                <Descriptions.Item label='手机号近七天在非银行机构申请间隔'>
                  {ApplyLoanStr.d7.cell? ApplyLoanStr.d7.cell.nbank.inteday+'天' :'无'}
                  </Descriptions.Item>
                <Descriptions.Item label='身份证近七天在银行机构申请间隔'>
                  {ApplyLoanStr.d7.id? ApplyLoanStr.d7.id.bank.inteday+'天' :'无'}
                  </Descriptions.Item>
                <Descriptions.Item label='身份证近七天在非银行机构申请间隔'>
                  {ApplyLoanStr.d7.id? ApplyLoanStr.d7.id.nbank.inteday+'天' :'无'}
                  </Descriptions.Item>

                <Descriptions.Item label='手机号近15天在银行机构申请间隔'>
                  {ApplyLoanStr.d15.cell? ApplyLoanStr.d15.cell.bank.inteday+'天' : '无'}
                  </Descriptions.Item>
                <Descriptions.Item label='手机号近15天在非银行机构申请间隔'>
                  {ApplyLoanStr.d15.cell? ApplyLoanStr.d15.cell.nbank.inteday+'天' :'无'}
                  </Descriptions.Item>
                <Descriptions.Item label='身份证近15天在银行机构申请间隔'>
                  {ApplyLoanStr.d15.id? ApplyLoanStr.d15.id.bank.inteday+'天' :'无'}
                  </Descriptions.Item>
                <Descriptions.Item label='身份证近15天在非银行机构申请间隔'>
                  {ApplyLoanStr.d15.id? ApplyLoanStr.d15.id.nbank.inteday+'天' :'无'}
                  </Descriptions.Item>

                <Descriptions.Item label='手机号近1个月在银行机构申请间隔'>
                  {ApplyLoanStr.m1.cell? ApplyLoanStr.m1.cell.bank.inteday+'天' : '无'}
                  </Descriptions.Item>
                <Descriptions.Item label='手机号近1个月在非银行机构申请间隔'>
                  {ApplyLoanStr.m1.cell? ApplyLoanStr.m1.cell.nbank.inteday+'天' :'无'}
                  </Descriptions.Item>
                <Descriptions.Item label='身份证近1个月在银行机构申请间隔'>
                  {ApplyLoanStr.m1.id? ApplyLoanStr.m1.id.bank.inteday+'天' :'无'}
                  </Descriptions.Item>
                <Descriptions.Item label='身份证近1个月在非银行机构申请间隔'>
                  {ApplyLoanStr.m1.id? ApplyLoanStr.m1.id.nbank.inteday+'天' :'无'}
                  </Descriptions.Item>

                <Descriptions.Item label='手机号近3个月在银行机构申请次数'>
                  {ApplyLoanStr.m3.cell? ApplyLoanStr.m3.cell.bank.allnum+'次' : '无'}
                  </Descriptions.Item>
                <Descriptions.Item label='手机号近3个月在其它机构申请次数'>
                  {ApplyLoanStr.m3.cell? ApplyLoanStr.m3.cell.oth.allnum+'次' : '无'}
                  </Descriptions.Item>
                <Descriptions.Item label='身份证近3个月在银行机构申请次数'>
                  {ApplyLoanStr.m3.id? ApplyLoanStr.m3.id.bank.allnum+'次' : '无'}
                  </Descriptions.Item>
                <Descriptions.Item label='身份证近3个月在其它机构申请次数'>
                  {ApplyLoanStr.m3.id? ApplyLoanStr.m3.id.oth.allnum+'次' : '无'}
                  </Descriptions.Item>

                <Descriptions.Item label='手机号近6个月在银行机构申请次数'>
                  {ApplyLoanStr.m6.cell? ApplyLoanStr.m6.cell.bank.allnum+'次' : '无'}
                  </Descriptions.Item>
                <Descriptions.Item label='手机号近6个月在其它机构申请次数'>
                  {ApplyLoanStr.m6.cell? ApplyLoanStr.m6.cell.oth.allnum+'次' : '无'}
                  </Descriptions.Item>
                <Descriptions.Item label='身份证近6个月在银行机构申请次数'>
                  {ApplyLoanStr.m6.id? ApplyLoanStr.m6.id.bank.allnum+'次' : '无'}
                  </Descriptions.Item>
                <Descriptions.Item label='身份证近6个月在其它机构申请次数'>
                  {ApplyLoanStr.m6.id? ApplyLoanStr.m6.id.oth.allnum+'次' : '无'}
                  </Descriptions.Item>

                <Descriptions.Item label='手机号近12个月在银行机构申请次数'>
                  {ApplyLoanStr.m12.cell? ApplyLoanStr.m12.cell.bank.allnum+'次' : '无'}
                  </Descriptions.Item>
                <Descriptions.Item label='手机号近12个月在其它机构申请次数'>
                  {ApplyLoanStr.m12.cell? ApplyLoanStr.m12.cell.oth.allnum+'次' : '无'}
                  </Descriptions.Item>
                <Descriptions.Item label='身份证近12个月在银行机构申请次数'>
                  {ApplyLoanStr.m12.id? ApplyLoanStr.m12.id.bank.allnum+'次' : '无'}
                  </Descriptions.Item>
                <Descriptions.Item label='身份证近12个月在其它机构申请次数'>
                  {ApplyLoanStr.m12.id? ApplyLoanStr.m12.id.oth.allnum+'次' : '无'}
                  </Descriptions.Item>

              
              </Descriptions>
            </div>
            <Divider name="借贷风险勘测" />
            <div className="riskDetection">
              <Descriptions column={2}>

                <Descriptions.Item label="近7天身份证号查询的申请次数">
                {ApplyLoanUsury.d7? ApplyLoanUsury.d7.id.allnum+'次' :'无'}
                </Descriptions.Item>
                <Descriptions.Item label="近7天手机号查询的申请次数">
                {ApplyLoanUsury.d7? ApplyLoanUsury.d7.cell.allnum+'次' :'无'}
                </Descriptions.Item>
                
                <Descriptions.Item label="近15天身份证号查询的申请次数">
                {ApplyLoanUsury.d15? ApplyLoanUsury.d15.id.allnum+'次' :'无'}
                </Descriptions.Item>
                <Descriptions.Item label="近15天手机号查询的申请次数">
                {ApplyLoanUsury.d15? ApplyLoanUsury.d15.cell.allnum+'次' :'无'}
                </Descriptions.Item>

                <Descriptions.Item label="近1个月身份证号查询的申请次数">
                {ApplyLoanUsury.m1? ApplyLoanUsury.m1.id.allnum+'次' :'无'}
                </Descriptions.Item>
                <Descriptions.Item label="近1个月手机号查询的申请次数">
                {ApplyLoanUsury.m1? ApplyLoanUsury.m1.cell.allnum+'次' :'无'}
                </Descriptions.Item>

                <Descriptions.Item label="近3个月身份证号查询的申请次数">
                {ApplyLoanUsury.m3? ApplyLoanUsury.m3.id.allnum+'次' :'无'}
                </Descriptions.Item>
                <Descriptions.Item label="近3个月手机号查询的申请次数">
                {ApplyLoanUsury.m3? ApplyLoanUsury.m3.cell.allnum+'次' :'无'}
                </Descriptions.Item>

                <Descriptions.Item label="近6个月身份证号查询的申请次数">
                {ApplyLoanUsury.m6? ApplyLoanUsury.m6.id.allnum+'次' :'无'}
                </Descriptions.Item>
                <Descriptions.Item label="近6个月手机号查询的申请次数">
                {ApplyLoanUsury.m6? ApplyLoanUsury.m6.cell.allnum+'次' :'无'}
                </Descriptions.Item>

                <Descriptions.Item label="近12个月身份证号查询的申请次数">
                {ApplyLoanUsury.m12? ApplyLoanUsury.m12.id.allnum+'次' :'无'}
                </Descriptions.Item>
                <Descriptions.Item label="近12个月手机号查询的申请次数">
                {ApplyLoanUsury.m12? ApplyLoanUsury.m12.cell.allnum+'次' :'无'}
                </Descriptions.Item>
              </Descriptions>
            </div>

            <Divider name="法院被执行人" />
            <div className="riskDetection">
              <Descriptions column={1}>
                {exe}
              </Descriptions>
            </div>

            <Divider name="法院被执行人" />
            <div className="riskDetection">
              <Descriptions column={1}>
                {pro}
              </Descriptions>
            </div>

            <Divider name="团伙欺诈排查" />
            <div className="riskDetection">
              <Descriptions column={2}>
              <Descriptions.Item label="查询人欺诈团伙等级取值:3-10">
                { FraudRelation_g.frg_list_level }  
                </Descriptions.Item>
              <Descriptions.Item label="查询人欺诈团伙群组规模">
                { FraudRelation_g.frg_group_num }  输出欺诈团伙群组规模,取值:a、b、c、d, a=[1,50）、b=[50,100）、c=[100,500）、d=[500,+）
                </Descriptions.Item>
              </Descriptions>
            </div>
            
            <Divider name="特殊名单验证" />
            <div className="riskDetection">
              <Descriptions column={2}>
              <Descriptions.Item label="手机号命中风险次数">
                { id?id:0 }  
                </Descriptions.Item>
              <Descriptions.Item label="身份证号命中风险次数">
                { cell?cell:0 } 
                </Descriptions.Item>
              </Descriptions>
            </div>

          </div>
        </div>
      </div>
    )
  }
}
export default BaironProRiskReport
