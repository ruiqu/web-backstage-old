/**
 * 风控报告关于最近租赁次数的显示
 * 需求链接：https://rdc.aliyun.com/req/3525167
 */
import React from "react"
import { Table } from "antd"
import "./index.scss"

export class RiskReportHisttoryRent extends React.PureComponent {
  tableColumns = [
    {
      title: "",
      dataIndex: "key",
      key: "key",
      align: "center",
    },
    {
      title: "近7天",
      dataIndex: "rent_history_times_d7_mode_rent_history_jc",
    },
    {
      title: "近1个月",
      dataIndex: "rent_history_times_m1_mode_rent_history_jc",
    },
    {
      title: "近3个月",
      dataIndex: "rent_history_times_m3_mode_rent_history_jc",
    },
    {
      title: "近个6月",
      dataIndex: "rent_history_times_m6_mode_rent_history_jc",
    }
  ]

  // 数字的协助处理方法
  numHelper = num => {
    if (num == -1) return 0 // -1的话作为0处理
    return num || 0 // 取不到或者类null的情况也作为0处理
  }

  render() {
    const obj = this.props.historyRentObj || {} // 接口返回的mode_rent_history对象

    const dataSource = [
      {
        key: "申请租赁次数",
        ...obj
      }
    ]

    return (
      <div className="riskReportHistoryRentContainer-jsjsdscsc121">
        <Table
          bordered
          columns={this.tableColumns}
          dataSource={dataSource}
          pagination={false}
        />
        <div className="riskReportHistoryRow-cls">
          <div>
            <span>历史申请最高风险分数：</span>{this.numHelper(obj.rent_history_max_grade_mode_rent_history_jc)}
          </div>
          <div>
            <span>是否命中租赁黑名单：</span>{obj.rent_history_black_mode_rent_history_jc == 1 ? "命中" : "未命中"}
          </div>
        </div>
      </div>
    )
  }
}