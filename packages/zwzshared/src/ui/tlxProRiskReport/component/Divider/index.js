import React, { Component } from 'react';
import { Divider } from 'antd';
export default ({ name, style, children }) => {
  return (
    <div style={{ ...style }}>
      <Divider style={{ fontSize: '20px', marginTop:50 }}>{name}</Divider>
      {children}
    </div>
  )
}