import React, { Component } from 'react';
import './index2.scss';
import { Descriptions, Table } from 'antd';
class Threeitem extends Component {
  
  /* render() {
    const { style, title } = this.props;
    const { dataValidation: { makeLoans } } = this.props;
    return (
      <div className="threeWrap" style={{ ...style }}>
        <Descriptions column={5} layout="vertical" bordered>
          <Descriptions.Item label=""><span style={{ fontWeight: 'bold' }}>{title.observeNum}</span></Descriptions.Item>
          <Descriptions.Item label={title.first}>{makeLoans.repay_succ1 === '无记录' ? 0 : makeLoans.repay_succ1}</Descriptions.Item>
          <Descriptions.Item label={title.second}>{makeLoans.repay_succ12 === '无记录' ? 0 : makeLoans.repay_succ12}</Descriptions.Item>
        </Descriptions>
        <div className="threeAbnormal">
          <span style={{ fontWeight: 'bold' }}>{title.unobserveNum}</span>
          <span>{makeLoans.repay_fail1 === '无记录' ? 0 : makeLoans.repay_fail1}</span>
          <span>{makeLoans.repay_fail12 === '无记录' ? 0 : makeLoans.repay_fail12}</span>
        </div>
      </div>
    )
  } */

  render() {
    const { style, title } = this.props;
    const { dataValidation: { makeLoans,conciseResult } } = this.props;
    const { personal_loan_h } = conciseResult;
    const columns = [
      { title: '', dataIndex: 'name' },
      { title: '近1个月', dataIndex: 'm1' },
      { title: '近3个月', dataIndex: 'm3' },
      { title: '近6个月', dataIndex: 'm6' },
      { title: '近12个月', dataIndex: 'm12' },
      { title: '近24个月', dataIndex: 'm24' },
    ];
    const customData=[
      {name:title.observeNum,
        m1:personal_loan_h.repay_succ1||0 ,
        m3:personal_loan_h.repay_succ3||0,
        m6:personal_loan_h.repay_succ6||0,
        m12:personal_loan_h.repay_succ12||0,
        m24:personal_loan_h.repay_succ24||0,
      },
      {name:title.unobserveNum,
        m1:personal_loan_h.repay_fail1 ||0,
        m3:personal_loan_h.repay_fail3||0,
        m6:personal_loan_h.repay_fail6||0,
        m12:personal_loan_h.repay_fail12||0,
        m24:personal_loan_h.repay_fail24||0,
      },
      {name:title.threeNum,
        m1:personal_loan_h.repay_money1 ||0,
        m3:personal_loan_h.repay_money3||0,
        m6:personal_loan_h.repay_money6||0,
        m12:personal_loan_h.repay_money12||0,
        m24:personal_loan_h.repay_money24||0,
      },
    ]
    return (
      <div className="threeWrap" style={{ ...style }}>
        <div className="tableHeader">
          <Table
            locale={{ emptyText: '暂无记录' }}
            bordered={true}
            columns={columns}
            dataSource={customData}
            pagination={false}
          />
        </div>
        <Descriptions column={6} layout="vertical" bordered>
          {/* <Descriptions.Item label=""><span style={{ fontWeight: 'bold' }}>{title.observeNum}</span></Descriptions.Item>
          <Descriptions.Item label={title.first}>{personal_loan_h.repay_succ1 === '' ? 0 : personal_loan_h.repay_succ1}</Descriptions.Item>
          <Descriptions.Item label={title.second}>{personal_loan_h.repay_succ3 === '' ? 0 : personal_loan_h.repay_succ3}</Descriptions.Item>
          <Descriptions.Item label={title.three}>{personal_loan_h.repay_succ6 === '' ? 0 : personal_loan_h.repay_succ6}</Descriptions.Item>
          <Descriptions.Item label={title.four}>{personal_loan_h.repay_succ12 === '' ? 0 : personal_loan_h.repay_succ12}</Descriptions.Item>
          <Descriptions.Item label={title.five}>{personal_loan_h.repay_succ24 === '' ? 0 : personal_loan_h.repay_succ24}</Descriptions.Item> */}
          {/* <Descriptions.Item ><span style={{ fontWeight: 'bold' }}>{title.observeNum}</span></Descriptions.Item>
          <Descriptions.Item >{personal_loan_h.repay_succ1 === '' ? 0 : personal_loan_h.repay_succ1}</Descriptions.Item>
          <Descriptions.Item >{personal_loan_h.repay_succ3 === '' ? 0 : personal_loan_h.repay_succ3}</Descriptions.Item>
          <Descriptions.Item>{personal_loan_h.repay_succ6 === '' ? 0 : personal_loan_h.repay_succ6}</Descriptions.Item>
          <Descriptions.Item>{personal_loan_h.repay_succ12 === '' ? 0 : personal_loan_h.repay_succ12}</Descriptions.Item>
          <Descriptions.Item>{personal_loan_h.repay_succ24 === '' ? 0 : personal_loan_h.repay_succ24}</Descriptions.Item> */}
        </Descriptions>
        {/* <div className="myAbnormal">
          <span style={{ fontWeight: 'bold' }}>{title.unobserveNum}</span>
          <span>{personal_loan_h.repay_fail1 === '' ? 0 : personal_loan_h.repay_fail1}</span>
          <span>{personal_loan_h.repay_fail3 === '' ? 0 : personal_loan_h.repay_fail3}</span>
          <span>{personal_loan_h.repay_fail6 === '' ? 0 : personal_loan_h.repay_fail6}</span>
          <span>{personal_loan_h.repay_fail12 === '' ? 0 : personal_loan_h.repay_fail12}</span>
          <span>{personal_loan_h.repay_fail24 === '' ? 0 : personal_loan_h.repay_fail24}</span>
        </div>
        <div className="myAbnormal">
          <span style={{ fontWeight: 'bold' }}>{title.threeNum}</span>
          <span>{personal_loan_h.repay_money1 === '' ? 0 : personal_loan_h.repay_money1}</span>
          <span>{personal_loan_h.repay_money3 === '' ? 0 : personal_loan_h.repay_money3}</span>
          <span>{personal_loan_h.repay_money6 === '' ? 0 : personal_loan_h.repay_money6}</span>
          <span>{personal_loan_h.repay_money12 === '' ? 0 : personal_loan_h.repay_money12}</span>
          <span>{personal_loan_h.repay_money24 === '' ? 0 : personal_loan_h.repay_money24}</span>
        </div> */}
      </div>
    )
  }
}
export default Threeitem;