import React, { Component } from 'react';
import './index2.scss';
import { Descriptions } from 'antd';

class FourItem extends Component {
  render() {
    const { style, title, makeLoans } = this.props;
    return (
      <div className="fourWrap0425" style={{ ...style }}>
        <Descriptions column={4} layout="vertical" bordered>
          <Descriptions.Item label={title.first}>{makeLoans.cflenders === '' ? '----' : makeLoans.cflenders}</Descriptions.Item>
          <Descriptions.Item label={title.second}>{makeLoans.nllenders === '' ? '----' : makeLoans.nllenders}</Descriptions.Item>
          <Descriptions.Item label={title.three}>{makeLoans.lend_time === '' ? '----' : makeLoans.lend_time}</Descriptions.Item>
          <Descriptions.Item label={title.four}>{makeLoans.lend_time_distance === '' ? '----' : makeLoans.lend_time_distance}</Descriptions.Item>
        </Descriptions>
      </div>
    )
  }
}

export default FourItem;