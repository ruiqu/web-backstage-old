import React, { Component } from 'react';
import './index.scss';
import FourItem from './fourItems';
import FourItemapply from './fouritemapplys';
import FiveItem from './FiveItem';
import FiveItemrepay from './FiveItemrepay';
import ThreeItem from './ThreeItem';

class Historyborrowing extends Component {
  render() {
    const { dataValidation: { applyFor, makeLoans } } = this.props;
    console.log("风控：",this.props)
    const titleName = {
      first: '消费分期类申请机构数 (个) ',
      second: '网络贷款类申请机构数 (个) ',
      three: '最近一次申请日期',
      four: '距离最近一次申请日期已有 (天) '
    }
    const fiveTitle = {
      first: '',
      second: '近1个月',
      three: '近3个月',
      four: '近6个月',
      five: '近12个月'
    }
    const fiveTitlerepay = {
      first: '',
      second: '近1个月',
      three: '近3个月',
      four: '近6个月'
    }
    const nameObject = {
      first: '消费分期类放款机构数 (个) ',
      second: '网络贷款类放款机构数 (个) ',
      three: '最近一次放款日期',
      four: '距离最近一次放款日期已有 (天) '
    }
    const threeName = {
      first: '近1个月',
      second: '近3个月',
      three: '近6个月',
      four: '近12个月',
      five: '近24个月',
      observeNum: '履约次数(次)',
      unobserveNum: '还款异常次数(次)',
      threeNum: '履约金额(元)'
    }
    return (
      <div className="historyWrap">
        <div className="total">
          <span style={{ fontWeight: 'bold' }}>近12个月申请机构总数&emsp;(个)：</span>
          <span>{applyFor.apply_mechanism_number === '无记录' ? 0 : applyFor.apply_mechanism_number}</span>
        </div>
        <FourItemapply dataValidation={this.props.dataValidation} style={{ marginTop: 14 }} title={titleName} />
        <FiveItem dataValidation={this.props.dataValidation} style={{ marginTop: 22 }} title={fiveTitle} text='申请次数(次)' />
        <div className="total">
          <span style={{ fontWeight: 'bold' }}>近12个月放款机构总数&emsp;(个)：</span>
          <span>{makeLoans.lenders === '' ? 0 : makeLoans.lenders}</span>
        </div>
        <FourItem style={{ marginTop: 14 }} title={nameObject} makeLoans={makeLoans} />
        <FiveItemrepay dataValidation={this.props.dataValidation} style={{ marginTop: 22 }} title={fiveTitlerepay} text='放款次数(次)' />
        <ThreeItem dataValidation={this.props.dataValidation} style={{ marginTop: 22 }} title={threeName} />

      </div>
    )
  }
}
export default Historyborrowing;