import React, { Component } from 'react';
import './index2.scss';
import { Descriptions } from 'antd';
class Fiveitem extends Component {
  render() {
    const { title, style, text } = this.props;
    const { dataValidation: { makeLoans } } = this.props;
    return (
      <div className="fiveWrap121" style={{ ...style }}>
        <Descriptions column={5} layout="vertical" bordered>
          <Descriptions.Item label={title.first}><span style={{ fontWeight: 'bold' }}>{text}</span></Descriptions.Item>
          <Descriptions.Item label={title.second}>{makeLoans.lend_number1 === '' ? 0 : makeLoans.lend_number1}</Descriptions.Item>
          <Descriptions.Item label={title.three}>{makeLoans.lend_number3 === '' ? 0 : makeLoans.lend_number3}</Descriptions.Item>
          <Descriptions.Item label={title.four}>{makeLoans.lend_number6 === '' ? 0 : makeLoans.lend_number6}</Descriptions.Item>
        </Descriptions>
      </div>
    )
  }
}
export default Fiveitem;