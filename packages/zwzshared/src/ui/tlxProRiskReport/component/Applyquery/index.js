import React, { Component } from 'react';
import './index.scss';
import { Table } from 'antd';
import { apply } from './data';

class Applyquery extends Component {
  render() {
    const { dataValidation: { applyQuery } } = this.props;

    const applyQuerySeven = {
      d7_apply_setup_time_ybxffq:applyQuery.d7_apply_setup_time_ybxffq,
      d7_apply_setup_time_xyk:applyQuery.d7_apply_setup_time_xyk,
      d7_apply_setup_time_dxxfjr:applyQuery.d7_apply_setup_time_dxxfjr,
      d7_apply_setup_time_other:applyQuery.d7_apply_setup_time_other,
      d7_apply_setup_time_p2pwd:applyQuery.d7_apply_setup_time_p2pwd,
      d7_apply_setup_time_xedkgs:applyQuery.d7_apply_setup_time_xedkgs,
      d7_apply_setup_time:applyQuery.d7_apply_setup_time,
    };
    const applyQuerythrity = {
      m1_apply_setup_time_ybxffq:applyQuery.m1_apply_setup_time_ybxffq,
      m1_apply_setup_time_xyk:applyQuery.m1_apply_setup_time_xyk,
      m1_apply_setup_time_dxxfjr:applyQuery.m1_apply_setup_time_dxxfjr,
      m1_apply_setup_time_other:applyQuery.m1_apply_setup_time_other,
      m1_apply_setup_time_p2pwd:applyQuery.m1_apply_setup_time_p2pwd,
      m1_apply_setup_time_xedkgs:applyQuery.m1_apply_setup_time_xedkgs,
      m1_apply_setup_time:applyQuery.m1_apply_setup_time,
    };
    const applyQueryninety = {
      m3_apply_setup_time_ybxffq:applyQuery.m3_apply_setup_time_ybxffq,
      m3_apply_setup_time_xyk:applyQuery.m3_apply_setup_time_xyk,
      m3_apply_setup_time_dxxfjr:applyQuery.m3_apply_setup_time_dxxfjr,
      m3_apply_setup_time_other:applyQuery.m3_apply_setup_time_other,
      m3_apply_setup_time_p2pwd:applyQuery.m3_apply_setup_time_p2pwd,
      m3_apply_setup_time_xedkgs:applyQuery.m3_apply_setup_time_xedkgs,
      m3_apply_setup_time:applyQuery.m3_apply_setup_time,
    };
    const applyQuerySevendata = [];
    const applyQuerythritydata = [];
    const applyQueryninetydata = [];
    for (let key in applyQuerySeven) {
      const seven = {};
      seven.day = applyQuerySeven[key];
      applyQuerySevendata.push(seven);
    }
    for (let key in applyQuerythrity) {
      const seven = {};
      seven.day = applyQuerythrity[key];
      applyQuerythritydata.push(seven);
    }
    for (let key in applyQueryninety) {
      const seven = {};
      seven.day = applyQueryninety[key];
      applyQueryninetydata.push(seven);
    }
    const sevenDays = [
      { title: '近7天', dataIndex: 'day' }
    ];
    const monthDays = [
      { title: '近30天', dataIndex: 'day' }
    ];
    const ninetyDays = [
      { title: '近90天', dataIndex: 'day' }
    ];
    const applycolumns = [
      { title: '', dataIndex: 'name' }
    ];
    const applyData = apply.map((item, sign) => {
      const newsItem = { ...item };
      const keys = sign + 1;
      newsItem.key = keys;
      return newsItem;
    });
    const sevenData = applyQuerySevendata.map((item, sign) => {
      const newsItem = { ...item };
      const keys = sign + 1;
      newsItem.key = keys;
      return newsItem;
    });
    const monthData = applyQuerythritydata.map((item, sign) => {
      const newsItem = { ...item };
      const keys = sign + 1;
      newsItem.key = keys;
      return newsItem;
    });
    const ninetyData = applyQueryninetydata.map((item, sign) => {
      const newsItem = { ...item };
      const keys = sign + 1;
      newsItem.key = keys;
      return newsItem;
    });
    return (
      <div className="tableHeader1210">
        <Table
          bordered={true}
          columns={applycolumns}
          dataSource={applyData}
          pagination={false}
        />
        <Table
          locale={{ emptyText: '暂无记录' }}
          bordered={true}
          columns={sevenDays}
          dataSource={sevenData}
          pagination={false}
        />
        <Table
          locale={{ emptyText: '暂无记录' }}
          bordered={true}
          columns={monthDays}
          dataSource={monthData}
          pagination={false}
        />
        <Table
          locale={{ emptyText: '暂无记录' }}
          bordered={true}
          columns={ninetyDays}
          dataSource={ninetyData}
          pagination={false}
        />
      </div>
    )
  }
}

export default Applyquery;