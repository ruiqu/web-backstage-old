import React, { Component } from 'react';
import './index.scss';
import { Descriptions } from 'antd';

class Leaseinfo extends Component {
  render() {
    const { style } = this.props;
    const { dataValidation: { rentalInformation } } = this.props;
    return (
      <div className="fiveWrap" style={{ ...style }}>
        <Descriptions column={6} layout="vertical" bordered>
          <Descriptions.Item label=""><span style={{ fontWeight: 'bold' }}>申请租赁(次数/机构数)</span></Descriptions.Item>
          <Descriptions.Item label="近7天">{rentalInformation.d7_apply_time}/{rentalInformation.d7_apply_agency_time}</Descriptions.Item>
          <Descriptions.Item label="近1个月">{rentalInformation.m1_apply_time}/{rentalInformation.m1_apply_agency_time}</Descriptions.Item>
          <Descriptions.Item label="近3个月">{rentalInformation.m3_apply_time}/{rentalInformation.m3_apply_agency_time}</Descriptions.Item>
          <Descriptions.Item label="近6个月">{rentalInformation.m6_apply_time}/{rentalInformation.m6_apply_agency_time}</Descriptions.Item>
          <Descriptions.Item label="近12个月">{rentalInformation.m12_apply_time}/{rentalInformation.m12_apply_agency_time}</Descriptions.Item>
        </Descriptions>
      </div>
    )
  }
}
export default Leaseinfo;