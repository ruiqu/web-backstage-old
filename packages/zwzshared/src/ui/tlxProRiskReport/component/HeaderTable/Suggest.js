import React, { Component } from 'react';
export default ({ name, style, children }) => {
  return (
    <div style={{ ...style }}>
      <span>{name}</span>
      {children}
    </div>
  )
}