import React, { Component } from 'react';
import './index.scss';
import Suggest from './Suggest';
import { Descriptions } from 'antd';

class HeaderTable extends Component {
  render() {
    const { dataValidation: { synthesizeGrade, riskLabeling } } = this.props;
    
    const riskSign = (
      <div className="signLess">
        {
          riskLabeling.map((item, sign) => {
            return (
              <span key={sign}>{sign + 1}、{item}</span>
            )
          })
        }
      </div>
    )
    const scoreStandard = (
      <div className="signLess">
        <span>分值在200至700之间，得分越低，风险越高:</span>
        <span>640分以上，建议通过；</span>
        <span>640至560分，建议审核；</span>
        <span>560分以下，建议拒绝。</span>
      </div>
    )
    return (
      <div className="tableHeader1008">
        <Descriptions column={4} layout="vertical" bordered>
          <Descriptions.Item label='综合评分'><span style={{ fontSize: 20, color: '#333', fontWeight: 'bold' }}>{synthesizeGrade}</span></Descriptions.Item>
          <Descriptions.Item label='审核建议'>
            {
              synthesizeGrade > 640 ? <Suggest name='建议通过' style={{ color: '#07880E', fontSize: 16 }}></Suggest> : null
            }
            {
              synthesizeGrade >= 560 && synthesizeGrade <= 640 ? <Suggest name='建议审核' style={{ color: '#07880E', fontSize: 16 }}></Suggest> : null
            }
            {
              synthesizeGrade < 560 ? <Suggest name='建议拒绝' style={{ color: '#FF3C2D', fontSize: 16 }}></Suggest> : null
            }
          </Descriptions.Item>
          <Descriptions.Item label='命中风险标注'>{riskSign}</Descriptions.Item>
          <Descriptions.Item label='分值标注说明'>{scoreStandard}</Descriptions.Item>
        </Descriptions>
      </div>
    )
  }
}
export default HeaderTable;