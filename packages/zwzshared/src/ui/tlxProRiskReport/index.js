import React, { Component } from "react"
import "./index.scss"
import { Descriptions,Table } from "antd"
import HeaderTable from "./component/HeaderTable/index"
import Divider from "./component/Divider/index"
import BasicInformation from "./component/BasicInformation/index"
import RiskList from "./component/RiskList/index"
import Historyborrowing from "./component/Historyborrowing/index"
import Historyoverdue from "./component/Historyoverdue"
import Leaseinfo from "./component/Leaseinfo"
import Courtrisklist from "./component/CourtriskList"
import Applyquery from "./component/Applyquery/index"
import { defaultApiResult } from "./data"

export class TLXProRiskReport extends Component {
  // 对接口所返回的数据进行处理，接口返回数据的siriusRiskReport作为riskReport这个props
  dataFactory = () => {
    const obj = this.props.riskReport || defaultApiResult
    const obj1 = this.props.riskReport1 || defaultApiResult
    return {
      orderNumber: obj.order_num,
      e: obj1,
      conciseResult: obj,
      synthesizeGrade: obj.score_norm_explain,
      riskLabeling: obj.hit_risk_tagging,
      basicInformation: obj.base_info,
      riskMonitoring: obj.risk_list_check,
      rentalInformation: obj.rent_history,
      overdueRecordDataList: obj.personal_overdue_history.datalist,
      overdueRecord: obj.personal_overdue_history,
      riskDetection: obj.relevance_risk_check,
      applyFor: obj.personal_loan_s,
      makeLoans: obj.personal_loan_f,
      riskInformation: obj.court_risk_info_list,
      applyQuery: obj.personal_loan_demand
    }
  }

  render() {
    const uiObj = this.dataFactory()
    const { riskDetection, rentalInformation,e  } = uiObj
    console.log("obj111111",e)
    let columns7 = [
      {
        title: '',
        dataIndex: 'name',
      },
      {
        title: '七天',
        dataIndex: 'd7',
      },
      {
        title: '1个月',
        dataIndex: 'm1',
      },
      {
        title: '3个月',
        dataIndex: 'm3',
      },
      {
        title: '6个月',
        dataIndex: 'm6',
      },

    ]
    let kv = {
      debt_org_d7_num: [0, 1, 9],
      debt_org_total_num: [0, 9, 14],
      debt_shopping_total_num: [0, 1, 2, 4],
      debt_www_total_num: [0, 1, 5, 11],
      debt_settled_num: [0, 5, 9, 14, 17],
      debt_first_distance_now: [0, 220, 450, 520],
      debt_org_m1_num: [0, 1, 4, 11],
      debt_org_m3_num: [0, 5, 10, 17],
      debt_org_m6_num: [0, 4, 7, 13],
      repay_succ_d7_num: [0, 3, 13],
      repay_succ_m1_num: [0, 7, 34],
      repay_succ_m3_num: [0, 2, 31],
      repay_succ_m6_num: [0, 3, 27, 50],
      repay_succ_d7_money: [0, 2000, 17000],
      repay_succ_m1_money: [0, 10000, 36000],
      repay_succ_m3_money: [0, 13000, 49000],
      repay_succ_m6_money: [0, 2000, 58000],
      repay_succ_lately_num: [0, 50, 160],
      repay_fail_d7_num: [0, 3, 5, 7],
      repay_fail_m1_num: [0, 3, 5, 34],
      repay_fail_m3_num: [0, 6, 22, 56],
      repay_fail_m6_num: [0, 3, 25, 30, 70],
      repay_fail_d7_money: [0, 2000, 10000, 17000, 26000],
      repay_fail_m1_money: [0, 6000, 10000, 36000],
      repay_fail_m3_money: [0, 10000, 50000, 80000],
      repay_fail_m6_money: [0, 2000, 30000, 60000, 90000],
      ovdure_org_num: [0, 1, 2, 4],
      ovdure_org_money: [0, 1000, 2000, 3000, 5000, 7000, 10000],
    }
    let handje = (key) => {
      if (kv[key]) {
        //console.log("出现了", kv[key], kv[key].length)
        //console.log("出现了", kv[key], e[key])
        if (e[key] < 1) return 0  //如果传进来是0，那么就返回0
        if (e[key] == kv[key].length) return kv[key][(kv[key].length - 1)] + "+"  //如果传过来最大的，那么就最大的+
        if (e[key] > kv[key].length) return "无"  //如果传过来的值没有，就返回无
        return kv[key][(e[key] - 1)] + "-" + kv[key][e[key]]
      }
      return e[key]
    }


    let ninetyData1 = [
      { name: "还款失败金额", d7: handje("repay_fail_d7_money"), m1: handje("repay_fail_m1_money"), m3: handje("repay_fail_m3_money"), m6: handje("repay_fail_m6_money") },
      { name: "还款失败次数", d7: handje("repay_fail_d7_num"), m1: handje("repay_fail_m1_num"), m3: handje("repay_fail_m3_num"), m6: handje("repay_fail_m6_num"), },

      { name: "还款成功金额", d7: handje("repay_succ_d7_money"), m1: handje("repay_succ_m1_money"), m3: handje("repay_succ_m3_money"), m6: handje("repay_succ_m6_money") },
      { name: "还款成功次数", d7: handje("repay_succ_d7_num"), m1: handje("repay_succ_m1_num"), m3: handje("repay_succ_m3_num"), m6: handje("repay_succ_m6_num") },

      { name: "负债机构数", d7: handje("debt_org_d7_num"), m1: handje("debt_org_m1_num"), m3: handje("debt_org_m3_num"), m6: handje("debt_org_m6_num") },

      { name: "身份证申请次数", d7: handje("apply_by_id_d7_num"), m1: handje("apply_by_id_m1_num"), m3: handje("apply_by_id_m3_num"), m6: handje("apply_by_id_m6_num") },
      { name: "手机号申请次数", d7: handje("apply_by_phone_d7_num"), m1: handje("apply_by_phone_m1_num"), m3: handje("apply_by_phone_m3_num"), m6: handje("apply_by_phone_m6_num") },

      { name: "身份证号申请平台数", d7: handje("apply_org_by_id_d7_num"), m1: handje("apply_org_by_id_m1_num"), m3: handje("apply_org_by_id_m3_num"), m6: handje("apply_org_by_id_m6_num") },
      { name: "手机号申请平台数", d7: handje("apply_org_by_phone_d7_num"), m1: handje("apply_org_by_phone_m1_num"), m3: handje("apply_org_by_phone_m3_num"), m6: handje("apply_org_by_phone_m6_num") },
    ]

    return (
      <div className="booleandataTotalContainer">
        <div className="booleanDataWrap">
          <div>
            <HeaderTable dataValidation={uiObj} />
            <Divider name="基本信息" />
            <BasicInformation dataValidation={uiObj} />
            <Divider name="风险名单监测" />
            <RiskList dataValidation={uiObj} />
            <Divider name="机构查询记录" />
            <Applyquery dataValidation={uiObj} />
            <Divider name="历史借贷行为" />
            <Historyborrowing dataValidation={uiObj} />
            <Divider name="历史逾期记录" />
            <Historyoverdue dataValidation={uiObj} />
            
            <Divider name="二代风控信息" />


            <div className="booleandataTotalContainer">
            <div >
              <div>
                <Descriptions title="">
                  <Descriptions.Item label="首次负债距今时长">
                    {handje("debt_first_distance_now")}
                  </Descriptions.Item>
                  <Descriptions.Item label="负债机构总数">
                    {handje("debt_org_total_num")}
                  </Descriptions.Item>
                  <Descriptions.Item label="已结清负债机构数">
                    {handje("debt_settled_num")}
                  </Descriptions.Item>
                  <Descriptions.Item label="消费金融类机构数">
                    {handje("debt_shopping_total_num")}
                  </Descriptions.Item>
                  <Descriptions.Item label="网络贷款类机构数">
                    {handje("debt_www_total_num")}
                  </Descriptions.Item>
                  <Descriptions.Item label="是否存在逾期未结清">
                    {handje("ovdure_flag")}
                  </Descriptions.Item>
                  <Descriptions.Item label="当前逾期金额">
                    {handje("ovdure_org_money")}
                  </Descriptions.Item>
                  <Descriptions.Item label="当前逾期机构数">
                    {handje("ovdure_org_num")}
                  </Descriptions.Item>
                  <Descriptions.Item label="最近一次成功还款距今天数">
                    {handje("repay_succ_lately_num")}
                  </Descriptions.Item>
   
                </Descriptions>
              </div>
              <div >
                <Table
                  locale={{ emptyText: '暂无记录' }}
                  bordered={true}
                  columns={columns7}
                  dataSource={ninetyData1}
                  pagination={false}
                />
              </div>
            </div>

          </div>


            <Divider name="租赁信息详情" />
            <Leaseinfo dataValidation={uiObj} />
            {/* <div className="highestGrade">
              <span style={{ fontWeight: "bold" }}>历史申请最高风险分数：</span>
              <span>
                {rentalInformation.rent_history_max_grade}
              </span>
            </div> */}
            <div className="highestGrade">
              <span style={{ fontWeight: "bold" }}>是否命中租赁黑名单：</span>
              <span>
                {rentalInformation.rent_history_black === 0
                  ? "未命中"
                  : "命中"}
              </span>
            </div>
            <Divider name="关联风险检测" />
            <div className="riskDetection">
              <Descriptions column={1}>
                <Descriptions.Item label="3个月身份证关联手机号数(个)">
                  {riskDetection.m3_idcard_to_phone_time}
                </Descriptions.Item>
                <Descriptions.Item label="3个月手机号关联身份证数(个)">
                  {riskDetection.m3_phone_to_idcard_time}
                </Descriptions.Item>
              </Descriptions>
            </div>

            <Divider name="法院风险信息" />
            <Courtrisklist dataValidation={uiObj} />
            <div className="footer">
              <p>报告使用说明:</p>
              <div className="text">
                1、本报告著作权属于布尔，未经我司正式文件许可，不得复制、摘录和转载。
              </div>
              <div className="text">
                2、本报告仅供使用者参考，布尔不承担据此报告产生的任何法律责任。
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default TLXProRiskReport
