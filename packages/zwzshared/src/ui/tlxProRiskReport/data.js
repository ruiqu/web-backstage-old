export const booleanData = [
  {
    grade:23,
    suggest:'建议通过',
    key:1
  }
]
export const sevenDataes = [
  {
    name:'2'
  },
  {
    name:'3'
  },
  {
    name:'2'
  },
  {
    name:'0'
  },
  {
    name:'0'
  },
  {
    name:'0'
  },
  {
    name:'0'
  },
]

export const defaultApiResult = {
  base_info: {},
  court_risk_info_list: [],
  hit_risk_tagging: [],
  order_num: "",
  personal_loan_demand: {},
  personal_loan_f: {},
  personal_loan_s: {},
  personal_overdue_history: {},
  relevance_risk_check: {},
  rent_history: {},
  risk_list_check: {},
  score_norm_explain: "",
  verify_recomment: "",
}