/**
 * 发起代扣的处理方法
 * props定义: periodItem是每期对象
 */
import React from "react"
import { Modal, Radio, message, Button } from "antd"
import request from "../../../../business/src/services/baseService"
import "./index.scss"

const payMethods = [
  { cn: "预授权扣款", val: "AUTH" },
  // { cn: "周期扣款", val: "CYCLE" },
  // { cn: "银行卡扣款", val: "BANK" },
]

export class Daikou extends React.PureComponent {
  state = {
    fetching: false, // 表示代扣处理中
    show: false, // 是否显示模态框
    deductionMethodType: null, // 扣款方式
  }

  componentDidMount() {
    // 设置默认的扣款方式
    const methodList = this.returnPayMethods()
    if (methodList && methodList.length) {
      const m0 = methodList[0]
      m0 && m0.val && this.setState({ deductionMethodType: m0.val })
    }
  }

  /**
   * 代扣的处理方法
   */
  daikouHandler = () => {
    if (!this.daikouClickable()) {
      message.warning("当前状态不可进行代扣")
      return
    }

    const deductionMethodType = this.state.deductionMethodType
    if (!deductionMethodType) {
      message.warning("请选中扣款方式")
      return
    }

    const currentPeriods = this.props.periodItem?.currentPeriods
    if (!currentPeriods) {
      message.warning("缺少期数，请重试")
      return
    }

    const orderId = this.props.periodItem?.orderId
    if (!orderId) {
      message.warning("缺少订单ID，请重试")
      return
    }
  
    const reqParams = {
      currentPeriods,
      deductionMethodType,
      orderId,
    } // 请求参数
    const url = "/hzsx/ope/order/stageOrderWithhold"
    const method = "post"
  
    this.setState({ fetching: true })
    request(url, reqParams, method).then(() => {
      message.info("处理中，请再稍等大概5秒", 5)
    }).then(this.shitSleep5S).then(() => {
      message.info("处理完成")
      const cb = this.props.refetchCb
      cb && cb()
      this.reset()
    }).finally(() => {
      this.setState({ fetching: false })
    })
  }

  shitSleep5S = () => new Promise(r => {
    setTimeout(r, 5000)
  })

  // 判断代扣可点击
  // LOG: 最新的去掉了关于未到期的判断，哪怕未到期也支持发起代扣
  daikouClickable = () => {
    // 这是关于订单状态的判断
    const orderStatus = this.props.orderInfoObj?.status

    const hits = [
      "01", // 待支付
      "11", // 待初审
      "13", // 待终审
      "10", // 交易关闭
    ]
    if (hits.includes(orderStatus)) {
      return false
    }

    // const now = Date.now()
    // const daoqiTime = new Date(this.props.periodItem?.statementDate) // 账单到期时间
    // const isValidTime = daoqiTime.toString() !== "Invalid Date"
    // const dtv = daoqiTime.valueOf()
    // 这是关于期数还款状态的判断
    const status = this.props.periodItem?.status // 账单支付状态

    return (
      !this.state.fetching && // 不处于处理中状态
      // (isValidTime && now > dtv) && // 已到期
      (status == "1" || status == "4") // 待支付相关状态
    )
  }

  // 显示模态框
  showModal = () => {
    if (!this.daikouClickable()) return // 不可点击

    const methodlist = this.returnPayMethods()
    if (!methodlist || !methodlist.length) {
      message.warning("暂无可发起代扣的签约方式")
      return
    }

    this.setState({ show: true })
  }

  // 隐藏模态的处理方法
  reset = () => {
    this.setState({ show: false })
    // this.setState({ show: false, deductionMethodType: null })
  }

  // 设置扣款方式
  setKoukuangMethodHandler = e => {
    const v = e.target.value
    this.setState({ deductionMethodType: v })
  }

  // 返回支付方式
  returnPayMethods = () => {
    return payMethods
    // const methodListTemp = this.props.apiRes?.deductionMethodTypes || [] // 用户支付方式
    // const result = payMethods.filter(obj => {
    //   const t = obj.val
    //   return methodListTemp.includes(t)
    // })
    // return result
  }

  render() {
    const methodArr = this.returnPayMethods() // 支付方式

    return (
      <>
        <Button
          className="daikouBtnsds1100293"
          type="link"
          onClick={this.showModal}
          disabled={!this.daikouClickable()}
        >
          发起代扣
        </Button>

        <Modal
          title="请选择扣款方式"
          visible={this.state.show}
          onOk={this.daikouHandler}
          onCancel={this.reset}
          okButtonProps={{ loading: this.state.fetching, disabled: this.state.fetching }}
        >
          <Radio.Group onChange={this.setKoukuangMethodHandler} value={this.state.deductionMethodType}>
            {
              methodArr.map((obj, idx) => (
                <Radio key={idx} value={obj.val}>{ obj.cn }</Radio>
              ))
            }
          </Radio.Group>
        </Modal>
      </>
    )
  }
}
