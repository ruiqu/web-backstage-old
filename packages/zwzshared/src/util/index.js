import React from "react"
import { Tooltip, Icon } from "antd"

const payMethodCnMap = {
  ZFB: {
    label: "周期扣款",
    help: `用户使用支付宝支付，需要完成"周期扣款"签约`,
    help2: "用户使用支付宝支付",
  },
  TTF: {
    label: "银行卡代扣",
    help: `用户使用银行卡支付，需要完成"银行卡代扣"签约`,
    help2: "用户使用银行卡支付",
  },
  WX: {
    label: "代扣",
    help: `用户使用微信支付，需要完成"银行卡代扣"签约或"周期扣款"签约`,
    help2: "用户使用微信支付",
  }
}

// 周期扣款具体文案
export const zhouqikoukuanReturnText = (resObj, label2) => {
  const uoi = resObj.userOrderInfoDto || {}
  const { bankPaySigned, cyclePaySigned } = uoi // 分别表示银行卡是否签约代扣、支付宝是否签约代扣

  const greenStr = str => <span style={{ color: "rgba(82, 196, 26, 1)" }}>{str}</span>
  const redStr = str => <span style={{ color: "rgba(245, 34, 45, 1)" }}>{str}</span>

  if (label2 === "周期扣款") {
    return cyclePaySigned ? greenStr("已签约") : redStr("未签约")
  } else if (label2 === "银行卡代扣") {
    return bankPaySigned ? greenStr("已签约") : redStr("未签约")
  }
}

// 周期扣款的label标签
export const zhouqikoukuanReturnLabel = (resObj, label2) => {
  const { paymentMethod } = resObj // 支付方式
  const obj = payMethodCnMap[paymentMethod] || {}
  const { label, help2 } = obj

  return (
    <>
      <span style={{ marginRight: 2 }}>{label2}</span>
      <Tooltip title={help2}>
        <Icon type="question-circle" />
      </Tooltip>
    </>
  )
}