import { DefaultFooter, getMenuData, getPageTitle } from '@ant-design/pro-layout';
import { Helmet } from 'react-helmet';
import { Link } from 'umi';
import React, { useEffect } from 'react';
import { connect } from 'dva';
import { formatMessage } from 'umi-plugin-react/locale';
import SelectLang from '@/components/SelectLang';
import logo from '../assets/logo.svg';
import styles from './UserLayout.less';
import logoBg from '@/assets/image/logo-bg.png';
import autofit from 'autofit.js';
import { platformHook } from '@/utils/platformConfig';

const UserLayout = props => {
  useEffect(() => {
    autofit.init(
      {
        designHeight: 1080,
        designWidth: 1920,
        renderDom: '#root',
        resize: true,
      },
      false,
    );
    return () => {
      console.log('卸载时触发effect');
      autofit.off();
    };
  });
  const {
    route = {
      routes: [],
    },
  } = props;
  const { routes = [] } = route;

  const {
    children,
    location = {
      pathname: '',
    },
  } = props;
  const { breadcrumb } = getMenuData(routes);
  const title = getPageTitle({
    pathname: location.pathname,
    breadcrumb,
    formatMessage,
    ...props,
  });
  console.log(123, window.location.hostname);
  return (
    <>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={title} />
      </Helmet>

      <div
        className={styles.container}
        style={{
          position: 'relative',
        }}
      >
        <div
          style={{
            position: 'absolute',
            left: 56,
            top: 46,
            color: '#1442E3',
            fontSize: '32px',
            fontWeight: 800,
          }}
        >
          {platformHook().platformName()}
        </div>
        <div
          style={{
            position: 'absolute',
            top: 14,
            right: 253,
          }}
        >
          <img width={586} height={254} src={logoBg} alt="" srcset="" />
        </div>
        {children}
      </div>
    </>
  );
};
export default connect(({ settings }) => ({ ...settings }))(UserLayout);
