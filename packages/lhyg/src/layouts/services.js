import request from '@/services/baseService';

export default {
  // 公告查询接口
  selectNotice: data => {
    return request(`/hzsx/notice/selectNotice`, data, 'get');
  },
 
};
