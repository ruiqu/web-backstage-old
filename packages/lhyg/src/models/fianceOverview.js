export const defaultAppVersion = "ZWZ"

export const defaultPageSize = 10

export const tab1 = "listRent"

export const tab2 = "listBuyOut"

export const tab3 = "listPurchase"

export const defaultQuery = {
  appVersion: defaultAppVersion,
  orderId: "",
  times: [],
  pageNumber: 1,
  pageSize: defaultPageSize,
}

export default {
  namespace: "financeOverview",

  state: {
    listApiParams: {
      [tab1]: defaultQuery,
      [tab2]: defaultQuery,
      [tab3]: defaultQuery,
    }, // 分页接口的传参数据

    activeTab: tab1, // 处于焦点的tab
  },

  reducers: {
    /**
     * 修改传参数据
     * @param {*} state 
     * @param {*} param1 
     * @returns 
     */
    mutListApiParams(state, { payload: { val, key } }) {
      return {
        ...state,
        listApiParams: {
          ...state.listApiParams,
          [key]: val,
        }
      }
    },

    /**
     * 修改部分query数据的时候触发
     * @param {*} state 
     * @param {*} param1 
     */
    muPartOfListApiParams(state, { payload: { tab, key, val }}) {
      return {
        ...state,
        listApiParams: {
          ...state.listApiParams,
          [tab]: {
            ...state.listApiParams[tab],
            [key]: val,
          }
        }
      }
    },

    /**
     * 修改tab
     * @param {*} state 
     * @param {*} param1 
     */
    mutActiveTab(state, { payload: activeTab }) {
      return {
        ...state,
        activeTab,
      }
    }
  }
}