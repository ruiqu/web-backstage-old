export default {
  namespace: "problemListModel",

  state: {
    activeTab: "", // 处于焦点的tab
  },

  reducers: {
    /**
     * 修改tab
     * @param {*} state 
     * @param {*} param1 
     */
    mutActiveTab(state, { payload: activeTab }) {
      return {
        ...state,
        activeTab,
      }
    }
  }
}