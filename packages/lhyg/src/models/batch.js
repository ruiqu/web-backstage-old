import { router } from 'umi';
import * as shopApi from '@/services/batch';
import { message } from 'antd';

export default {
  namespace: 'batch',
  state: {
    List: [],
    Total: 1,
    EditData: {},
  },
  effects: {
    *queryTribeCommentPage({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.queryTribeCommentPage, payload);
      yield put({
        type: 'listData',
        payload: response,
      });
    },
    *queryProductEvaluationPage({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.queryProductEvaluationPage, payload);
      yield put({
        type: 'listData',
        payload: response,
      });
    },
    *selectTribeCommentEdit({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.selectTribeCommentEdit, payload);
      yield put({
        type: 'Edit',
        payload: response,
      });
    },
    *selectExamineProductEdit({ payload, callback }, { call, put }) {
      const response = yield call(shopApi.selectExamineProductEdit, payload);
      yield put({
        type: 'Edit',
        payload: response,
      });
    },
  },
  reducers: {
    listData(state, { payload }) {
      return {
        ...state,
        List: payload.data.records,
        Total: payload.data.total,
      };
    },
    Edit(state, { payload }) {
      return {
        ...state,
        EditData: payload.data,
      };
    },
    // shopInfoData(state, { payload }) {
    //   console.log('payload:', payload);
    //   return {
    //     ...state,
    //     InfoData: payload.data.shop,
    //     shopEnterpriseInfos: payload.data.enterpriseInfoDto.shopEnterpriseInfos,
    //     shopEnterpriseCertificates: payload.data.enterpriseInfoDto.shopEnterpriseCertificates,
    //   };
    // },
  },
};
