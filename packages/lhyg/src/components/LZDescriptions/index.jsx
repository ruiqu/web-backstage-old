import React from 'react';
import { Descriptions } from 'antd';

export const renderDescriptionsItems = (fields, title) => {
  return (
    <Descriptions title={title}>
      {fields &&
        fields.map(field => {
          return (
            <Descriptions.Item key={field.label} label={field.label} className={field.className}>
              {field.render ? field.render : field.value}
            </Descriptions.Item>
          );
        })}
    </Descriptions>
  );
};
