import {LZFormItem} from "@/components/LZForm";
import {Input, Select, DatePicker} from "antd";
import moment from "moment";
import React from "react";
import {withShopStatus, orderStatusMap, orderCloseStatusMap} from "@/utils/enum";

const { RangePicker } = DatePicker;
const { Option } = Select;

// 买断账单状态列表
const buyOutStatusData = {
  '01': '取消',
  // '02': '关闭',
  '03': '完成',
  '04': '待支付',
  // '05': '支付中',
};

const arrStatus =  [
  // 用户取消订单
  'WAITING_BUSINESS_DELIVERY/待商家发货',
  'WAITING_GIVE_BACK/租用中',
  'WAITING_SETTLEMENT/待结算',
  'BUSINESS_CLOSE_ORDER/商家关闭订单',
  'PLATFORM_CLOSE_ORDER/平台关闭订单',
  'SHOP_RISK_REVIEW_CLOSE_ORDER/商家风控关单',
  'USER_APPLICATION/用户申请关单',
  'WAITING_USER_RECEIVE_CONFIRM/待用户确认收货',
  'RC_REJECT/风控关闭订单',
  'USER_CANCELED_CLOSED/用户取消订单',
  'ALREADY_FREEZE/已冻结',
  'WAITING_PAYMENT/待支付',
  'APPLY_CLOSE_ORDER/订单关闭',
  'USER_OVERTIME_PAYMENT_CLOSED/用户超时支付关闭订单',
  'USER_DELETE_ORDER/用户删除订单',
  'BUSINESS_OVERTIME_CLOSE_ORDER/商家超时发货关闭订单',
  'WAITING_BUSINESS_CONFIRM_RETURN_REFUND/待商家确认退货退款',
  'BUSINESS_REFUSED_RETURN_REFUND/商家拒绝退货退款',
  'WAITING_USER_RETURN/待退货',
  'WAITING_REFUND/待退款',
  'WAITING_BUSINESS_RECEIVE_CONFIRM/待商家确认收货',
  'TO_BE_RETURNED/待归还',
  'WAITING_CONFIRM_SETTLEMENT/待确认结算端(C端确认)',
  'WAITING_SETTLEMENT_PAYMENT/出结算单后的待支付',
  'SETTLEMENT_WITHOUT_PAYMENT_OVERTIME_AUTOCONFIRM/自动确认结算',
  'SETTLEMENT_WITH_PAYMENT_OVERTIME/结算支付超时',
  'GIVING_BACK/归还中',
  'ORDER_FINISH/订单完成',
  'ORDER_VERDUE/逾期',
  'SETTLEMENT_RETURN_CONFIRM_PAY/结算单违约金支付逾期',
  'USER_PAY_FAIL_CLOSE/用户支付失败关闭订单',
  'WAITING_EVALUATION/待评价',
  'ALREADY_EVALUATION/已评价',
  'PENALTY_WAITING_SETTLEMENT/违约金待结算中',
  'JH_CREATED/已生成借还订单，其角色类似“待支付”',
  'WAITING_BUCKLE_SETTLEMENT/代扣结算中',
  'AITING_USER_RECEIVE_CONFIRM/已冻结',
]

const renderFormItemInput = ({field, getFieldDecorator, placeholder, label}) => {
  return (
    <LZFormItem label={label} field={field} key={field} getFieldDecorator={getFieldDecorator}>
      <Input placeholder={placeholder} style={{ width: 200 }} allowClear />
    </LZFormItem>
  )
}


export default {
  shopName: (getFieldDecorator)=>{
    return renderFormItemInput({getFieldDecorator, field: 'shopName', placeholder: '请输入店铺名称'})
  },
  type: (getFieldDecorator)=>{
    return (
      <LZFormItem field="type" key="type" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="分佣类型" style={{ width: 200 }} allowClear>
          <Option value="1">有效</Option>
          <Option value="0">失效</Option>
        </Select>
      </LZFormItem>
    )
  },
  status: (getFieldDecorator)=>{
    return (
      <LZFormItem field="status" key="status" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="分佣状态" style={{ width: 200 }} allowClear>
          <Option value="0">审核通过</Option>
          <Option value="1">审核拒绝</Option>
          <Option value="2">待审核</Option>
        </Select>
      </LZFormItem>
    )
  },
  isDeleted: (getFieldDecorator)=>{
    return (
      <LZFormItem field="isDelete" label="是否删除" key="isDeleted" getFieldDecorator={getFieldDecorator}
                  initialValue={false}>
        <Select placeholder="是否删除" style={{ width: 200 }} allowClear>
          <Option value={true}>是</Option>
          <Option value={false}>否</Option>
        </Select>
      </LZFormItem>
    )
  },
  applicant: (getFieldDecorator)=>{
    return renderFormItemInput({getFieldDecorator, field: 'applicant',
      placeholder: '请输入申请人'})
  },
  productName: (getFieldDecorator)=>{
    return renderFormItemInput({getFieldDecorator, field: 'productName',
      placeholder: '请输入商品名称', label: '商品名称'})
  },
  businessName: (getFieldDecorator)=>{
    return renderFormItemInput({getFieldDecorator, field: 'businessName',
      placeholder: '请输入商家名称', label: '商家名称'})
  },
  productId: (getFieldDecorator)=>{
    return renderFormItemInput({getFieldDecorator, field: 'productId',
      placeholder: '请输入商品编号', label: '商品编号'})
  },
  shopProductName: (getFieldDecorator)=>{
    return renderFormItemInput({getFieldDecorator, field: 'productName',
      placeholder: '请输入商品名称', label: '商品名称'})
  },
  categoryIds: (getFieldDecorator, options)=>{
    return (
      <LZFormItem field="categoryIds" key="categoryIds" label="商品类目" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="商品类目" style={{ width: 200 }} allowClear>
          {
            options.map(option=>{
              return <Option value={option.value} key={option.label}>{option.label}</Option>
            })
          }
        </Select>
      </LZFormItem>
    )
  },
  shopSaleStatus: (getFieldDecorator)=>{
    return (
      <LZFormItem field="type" key="type" label="上架状态" getFieldDecorator={getFieldDecorator}
      initialValue={"1"}>
        <Select placeholder="上架状态" style={{ width: 200 }} allowClear>
          {/*<Option value="0">回收站中的商品</Option>*/}
          <Option value="1">已上架</Option>
          <Option value="2">已下架</Option>
        </Select>
      </LZFormItem>
    )
  },
  auditStatus: (getFieldDecorator)=>{
    return (
      <LZFormItem field="auditState" key="auditState" label="审核状态" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="审核状态" style={{ width: 200 }} allowClear>
          <Option value={0}>审核中</Option>
          <Option value={1}>审核拒绝</Option>
          <Option value={2}>审核通过</Option>
        </Select>
      </LZFormItem>
    )
  },
  withBusinessSettleStatus: (getFieldDecorator)=>{
    return (
      <LZFormItem field="status" label={'结算状态'} key="status" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="结算状态" style={{ width: 200 }} allowClear>
          {/* {
            Object.keys(withShopStatus).map(v=>{
              return (
                <Option value={v} key={v}>{withShopStatus[v]}</Option>
              )
            })
          } */}
           <Option value={1} key={1}>待结算</Option>
           <Option value={2} key={2}>待审核</Option>
           <Option value={3} key={3}>待支付</Option>
           <Option value={4} key={4}>已支付</Option>
        </Select>
      </LZFormItem>
    )
  },
  orderer: (getFieldDecorator)=>{
    return renderFormItemInput({getFieldDecorator, field: 'userName',
      placeholder: '请输入下单人姓名', label: '下单人姓名'})
  },
  ordererPhone: (getFieldDecorator)=>{
    return renderFormItemInput({getFieldDecorator, field: 'telephone',
      placeholder: '请输入下单人手机号', label: '下单人手机号'})
  },
  orderId: (getFieldDecorator)=>{
    return renderFormItemInput({getFieldDecorator, field: 'orderId',
      placeholder: '请输入订单编号', label: '订单编号'})
  },
  
  // 部落
  tribeTitle: (getFieldDecorator)=>{
    return renderFormItemInput({getFieldDecorator, field: 'title', placeholder: '请输入部落标题'})
  },
  tribeStatus: (getFieldDecorator)=>{
    return (
      <LZFormItem field="status" key="status" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="状态" style={{ width: 200 }} allowClear>
          <Option value="0">无效</Option>
          <Option value="1">有效</Option>
        </Select>
      </LZFormItem>
    )
  },
  
  
  createTimeStart: (getFieldDecorator)=>{
    return (
      <LZFormItem field="createDate" label="账单生成时间" key="createDate" getFieldDecorator={getFieldDecorator}>
        <RangePicker
          showTime
          format="YYYY/MM/DD"
          placeholder={['开始时间', '结束时间']}
          allowClear
        />
      </LZFormItem>
    )
  },
  orderStatus: (getFieldDecorator)=>{
    return (
      <LZFormItem field="orderStatus" label="订单状态" key="status" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="订单状态" style={{ width: 200 }} allowClear>
          <Option value="" key="all">全部</Option>
          {
            ['01','11','04','05','06','07','08','09','10','12'].map(value => {
              return <Option value={value} key={value}>{orderStatusMap[value]}</Option>
            })
          }
        </Select>
      </LZFormItem>
    )
  },
  buyOutOrderStatus: (getFieldDecorator)=>{
    return (
      <LZFormItem field="orderStatus" label="订单状态" key="status" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="订单状态" style={{ width: 200 }} allowClear>
          {/* <Option value="" key="all">全部</Option> */}
          {
            Object.keys(buyOutStatusData).sort().map(value => {
              return <Option value={value} key={value}>{buyOutStatusData[value]}</Option>
            })
          }
        </Select>
      </LZFormItem>
    )
  },
  purchaseOrderStatus: (getFieldDecorator)=>{
    return (
      <LZFormItem field="orderStatus" label="订单状态" key="status" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="订单状态" style={{ width: 200 }} allowClear>
          {/* <Option value="" key="all">全部</Option> */}
          <Option value="WAITING_FOR_PAY">待支付</Option>
          <Option value="CANCEL">已取消</Option>
          <Option value="WAITING_FOR_DELIVERY">待发货</Option>
          <Option value="WAITING_RECEIVE">待收货</Option>
          <Option value="FINISH">已完成</Option>
        </Select>
      </LZFormItem>
    )
  },
  orderCloseStatus: (getFieldDecorator)=>{
    return (
      <LZFormItem field="closeType" key="closeType" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="关单类型" style={{ width: 200 }} allowClear>
          <Option value="" key="all">全部</Option>
          {
            Object.keys(orderCloseStatusMap).map(value => {
              return <Option value={value} key={value}>{orderCloseStatusMap[value]}</Option>
            })
          }
        </Select>
      </LZFormItem>
    )
  },
  memberName: (getFieldDecorator)=>{
    return renderFormItemInput({getFieldDecorator, field: 'memberName',
      placeholder: '请输入姓名', label: '姓名'})
  },
  departments: (getFieldDecorator, options)=>{
    return (
      <LZFormItem field="departments" label="所属部门" key="departments" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="所属部门" style={{ width: 200 }} allowClear>
          {
            options.map(option=>{
              return <Option value={option.value} key={option.label}>{option.label}</Option>
            })
          }
        </Select>
      </LZFormItem>
    )
  },
  packageName: (getFieldDecorator)=>{
    return renderFormItemInput({getFieldDecorator, field: 'name', placeholder: '请输入大礼包名称'})
  },
  packageStatus: (getFieldDecorator)=>{
    return (
      <LZFormItem field="status" key="status" getFieldDecorator={getFieldDecorator}>
        <Select placeholder="大礼包状态" style={{ width: 200 }} allowClear>
          <Option value="0">无效</Option>
          <Option value="1">有效</Option>
        </Select>
      </LZFormItem>
    )
  },
  
  
}

