import React, {Component, Fragment} from 'react';
import moment from 'moment';
import { Form, DatePicker, Input, Select, Button } from 'antd';
import SearchItemEnum from "@/components/Search/SearchItemEnum";

const FormItem = Form.Item;
const { RangePicker } = DatePicker;
const { Option } = Select;
@Form.create()

export default class Search extends Component {
  //查询
  handleFilter = () => {
    this.props.form.validateFields((err, value) => {
      this.props.handleFilter(value);
    });
  };
  // 重置
  handleReset = async () => {
    this.props.form.resetFields();
    this.props.form.setFieldsValue({
      status: undefined,
      type: undefined,
      isDelete: undefined
    });
    this.props.handleFilter({ queryType: '重置' });
  };
  // 导出
  exportData = () => {
    this.props.form.validateFields((err, value) => {
      this.props.exportData(value);
    });
  }
  
  renderLZSearch = (searchConfigs)=>{
    const {getFieldDecorator,} = this.props.form
    let allOptions = this.props.options || []
  
    return (
      <Fragment>
        {
          searchConfigs.map(searchConfig=>{
            let _options = []
            if (allOptions) {
              const item = allOptions.find(f=> f.field === searchConfig) || []
              if (item) {
                _options = item.options
              }
            }
            return SearchItemEnum[searchConfig](getFieldDecorator, _options)
          })
        }
      </Fragment>
    )
  }
  
  selectForms = (source)=>{
    let searchConfigs = []
    switch (source) {
      case "佣金设置":
        searchConfigs = ['shopName','applicant', 'type', 'status']
        break;
      case "佣金审批":
        searchConfigs = ['shopName', 'type', 'status']
        break;
      case "常规订单":
        searchConfigs = ['withBusinessSettleStatus', 'orderer', 'ordererPhone','orderId',
          'createTimeStart']
        break;
      case "买断订单":
        searchConfigs = ['withBusinessSettleStatus', 'orderer', 'ordererPhone','orderId',
          'createTimeStart']
        break;
      case "购买订单":
        searchConfigs = ['withBusinessSettleStatus', 'orderer', 'ordererPhone','orderId',
          'createTimeStart']
        break;
      case "部落配置":
        searchConfigs = ['tribeTitle', 'tribeStatus']
        break;
      case "成员管理":
        searchConfigs = ['memberName', 'departments']
        break;
      case "商品列表":
        searchConfigs = ['shopProductName', 'productId', 'categoryIds', 'shopSaleStatus', 'auditStatus',
        'isDeleted']
        break;
      case "大礼包":
        searchConfigs = ['packageName', 'packageStatus']
        break;
      default:
        break;
    }
    return this.renderLZSearch(searchConfigs);
  }
  
  renderFooter = () => {
    const { needExport, needReset = true } = this.props
    return (
      <div>
        <FormItem>
          <Button type="primary" onClick={this.handleFilter}>
            查询
          </Button>
          {
            needReset?(
              <Button style={{ marginLeft: 8 }} onClick={this.handleReset}>
                重置
              </Button>
            ): null
          }
          {
            needExport ? (
              <Button style={{ marginLeft: 8 }} onClick={this.exportData} >
                导出
              </Button>
            ) : null
          }
        </FormItem>
      </div>
    )
  }
  
  render() {
    const {source} = this.props
    const { getFieldDecorator } = this.props.form;
    return (
      <Form layout="inline">
        {this.selectForms(source)}
        {
          this.props.channelList ? (
            <FormItem>
              {getFieldDecorator('channelId', {
                initialValue: '',
              })(
                <Select style={{ width: 180 }}>
                  <Option value="">全部渠道来源</Option>
                  {this.props.channelList.map(item => {
                    return (
                      <Option key={item.channelId} value={item.channelId}>
                        {item.channelName}
                      </Option>
                    );
                  })}
                </Select>
              )}
            </FormItem>
          ) : null
        }
        {this.renderFooter()}
      </Form>
    );
  }
}
