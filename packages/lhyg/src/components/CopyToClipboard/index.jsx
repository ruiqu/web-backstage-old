import React, { Component } from 'react';
import CopyToClipboard from 'react-copy-to-clipboard';
import { message } from 'antd';
export default class ToClipboard extends Component {
  onCopy = e => {
    message.destroy();
    message.success(`复制内容：${e}`);
  };
  render() {
    const { text } = this.props;
    return (
      <CopyToClipboard text={text} onCopy={this.onCopy}>
        <a>{text}</a>
      </CopyToClipboard>
    );
  }
}
