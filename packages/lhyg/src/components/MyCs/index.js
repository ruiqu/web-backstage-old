import React, { Component, Fragment } from 'react';
import { Form, DatePicker, Input, Select, Button, Table,Drawer } from 'antd';
import request from '@/services/baseService';
import { onTableData, getParam, makeSub, renderOrderStatus } from '@/utils/utils.js';
import AntTable from '@/components/AntTable';
@Form.create()

export default class MyCs extends Component {
  //查询
  state = {
    isshow: false,
    db: {},
    attachment: {},
    sfz: {},
  }


  render() {
    const { orderByStagesDtoList } = this.props

    const columnsByStagesStute = {
      '1': '待支付',
      '2': '已支付',
      '3': '逾期已支付',
      '4': '逾期待支付',
      '5': '已取消',
      '6': '已结算',
      '7': '已退款,可用',
      '8': '部分还款',
    };
    let collstus = {
      '01': '承诺还款',
      '02': '申请延期还款',
      '03': '拒绝还款',
      '04': '电话无人接听',
      '05': '电话拒接',
      '06': '电话关机',
      '07': '电话停机',
      '08': '客户失联',
    };
    const BusinessCollection = [
      {
        title: '记录人',
        dataIndex: 'userName',
      },
      {
        title: '记录时间',
        dataIndex: 'createTime',
      },
      {
        title: '结果',
        dataIndex: 'result',
        render: result => collstus[result],
      },
      {
        title: '小记',
        dataIndex: 'notes',
      },
    ];
    



    return (
      <>
       <AntTable
            isLimitHeightTable={true}
            columns={BusinessCollection}
            dataSource={onTableData(orderByStagesDtoList)}
            paginationProps={{
              current: 1,
              pageSize: 1000,
              total: 1,
            }}
          />

      </>
    );
  }
}
