import React, { Component } from 'react';
import styles from './index.less';
import { Table, Badge, message } from 'antd';
import { tableDataes } from './data';
import { router } from 'umi';
import { connect } from 'dva';
@connect(() => ({}))
export default class AntTable extends Component {
  render() {
    const tableClsName = this.props.isLimitHeightTable ? styles.limitHeightTable : "" // 如果需要限高的话，那么返回其样式名
    return (
      <div className={tableClsName} style={{ marginBottom: '28px' }}>
        <Table
          columns={this.props.columns}
          dataSource={this.props.dataSource}
          bordered={this.props.bordered}
          pagination={false}
          size="middle"
          tableLayout="fixed"
          scroll={{ y: 430, x: this.props.scrollx }}
        />
      </div>
    );
  }
}
