import React, { Component } from 'react';
import styles from './index.less';
import { Table } from 'antd';
export default class MyTable extends Component {
  //获取子组件页数
  onChange = e => {
    e && this.props.onPage(e);
  };
  render() {
    let { columns, dataSource, paginationProps, scroll,rowSelection } = this.props;
    if(!paginationProps)paginationProps={}
    if(!rowSelection)rowSelection =false
    paginationProps.pageSizeOptions= ["10", "20", "30", "50"]
    paginationProps.showSizeChanger= true
    return (
      <div>
        <Table
        // bordered
          scroll={{x: scroll}}
          columns={columns}
          dataSource={dataSource}
          pagination={paginationProps}
          onChange={this.onChange}
          rowSelection={rowSelection}
        />
      </div>
    );
  }
}
