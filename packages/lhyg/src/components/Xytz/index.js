import React, { Component, Fragment } from 'react';
import { Form, DatePicker, Input, Select, Button, Table } from 'antd';
import request from '@/services/baseService';
import { onTableData, getParam, makeSub, renderOrderStatus } from '@/utils/utils.js';
@Form.create()
//探针
export default class Xytz extends Component {
  //查询
  state = {
    riskType: "11",
    isshow: false,
    isload: false,
    v历史: null,
    v选择的日期: null,
    db: {},
    data: {},
    sfz: {},
  }

  viewCredit() {
    const { sfz,serialNum } = this.props
    let req={
      "uid": sfz.uid,
      "orderId": serialNum,
      "serialNum":serialNum,
      "phone": sfz.telephone,
      "riskType": this.state.riskType,
      "idCardNo": sfz.idCard,
      "userName": sfz.realName
  }

    request(`/hzsx/business/order/queryXyTzReportByUid`,req).then(res=>{
      let db=JSON.parse(res.siriusRiskReport)
      console.log("ressss,",db)
      this.setState({ db, isshow: true })
    })

  }
  f获取历史 = () => {
    const { sfz } = this.props
    if (this.state.isload) return
    this.setState({ isload: true });
    let db = {}
    db.uid = sfz.uid
    db.phone = sfz.telephone
    db.idCardNo = sfz.idCard
    db.riskType = this.state.riskType
    db.userName = sfz.realName
    //发起请求，直接请求获取列表
    request("/hzsx/business/order/queryLsReportByUid", db).then(res => {
      console.log("历史的数据是", res)
      //this.f布尔历史数据处理(res)
      let v历史 = {}
      for (let i = 0; i < res.length; i++) {
        const ele = res[i];
        let k = ele.createTime.substring(0, 10);
        if (!v历史[k]) v历史[k] = {}
        const str2 = ele.reportResult.replaceAll('\"', '"')
        v历史[k] = JSON.parse(str2)
        //判断数据有没有，有的话就添加，没有就新增 
      }
      Object.keys(v历史).map((item, i) => {
        console.log("历史是", item, i)
      })
      console.log("的数据是db", v历史)
      let arr = Object.keys(v历史);
      if (arr.length == 1) {
        message.success("暂时没有历史数据")
        this.setState({ isload: false });
      } else {
        this.setState({ isload: false, v选择的日期: -1, v历史 });
      }
      //判断一下，把第一个给去掉，然后显示另外 的俩个
    })
  }

  xran = () => {
    let { v历史, v选择的日期 } = this.state
    console.log("v历史", v历史)
    //如果没有，那么就显示
    if (!v选择的日期) {
      return <><Button onClick={() => this.f获取历史()} type="primary">查询历史</Button></>
    } else {
      return (<>
        {Object.keys(v历史).map(option => {
          return (
            <Button disabled={option==v选择的日期} onClick={() => {
              this.setState({ v选择的日期: option })
            }} type="primary">{option}</Button>
          );
        })}
      </>
      )
    }
  }
  render() {
    const { db, isshow, sfz, data, v选择的日期, v历史 } = this.state
    console.log("OBJ是", db)
    let v数据 = db
    console.log("判断是否选择了日期", v数据, v历史)
    if(v历史)console.log("taya ,",Object.keys(v历史).length > 0,Object.keys(v历史).length )
    if (v选择的日期 && v历史 && Object.keys(v历史).length > 0) {//如果选择了日期，那么重置数据
      v数据 = v历史[v选择的日期]
      console.log("选择了数据", v数据, v选择的日期)
    }
    let v报告详情1 = [
      { title: '最大逾期金额', dataIndex: 'max_overdue_amt', width: 150 },
      { title: '最大逾期天数', dataIndex: 'max_overdue_days', width: 150 },
      { title: '最近逾期时间', dataIndex: 'latest_overdue_time', width: 150 },
      { title: '最大履约金额', dataIndex: 'max_performance_amt', width: 150 },
      { title: '最近履约时间', dataIndex: 'latest_performance_time', width: 150 },
    ]
    let v报告详情2 = [
      { title: '履约笔数', dataIndex: 'count_performance', width: 150 },
      { title: '当前逾期机构数', dataIndex: 'currently_overdue', width: 150 },
      { title: '当前履约机构数', dataIndex: 'currently_performance', width: 150 },
      { title: '异常还款机构数', dataIndex: 'acc_exc', width: 150 },
      { title: '睡眠机构数', dataIndex: 'acc_sleep', width: 150 },
    ]
   
    const f获取参数 = (db) => {
      let detail = []
      if (db) detail.push(db)
      return detail
    }
    return (
      <>
        {
          !isshow && (
            <Button onClick={() => this.viewCredit()} type="primary">点击查询</Button>
          )
        }

        {
          isshow && (
            <>

              <p style={{ fontSize: 20 }}>
                探针报告详情:
              </p>
              <Table
                bordered
                columns={v报告详情1}
                dataSource={f获取参数(v数据.data)}
                pagination={false}
              />
              <Table
                bordered
                columns={v报告详情2}
                dataSource={f获取参数(v数据.data)}
                pagination={false}
              />
             
            </>
          )
        }

      </>
    );
  }
}
