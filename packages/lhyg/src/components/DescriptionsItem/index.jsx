import React, { Component } from 'react';
import styles from './index.less';
import { Row, Col } from 'antd';

export default class DescriptionsItem extends Component {
  render() {
    let { DesItem, colg } = this.props;
    let { xxl, xl, lg, md, sm, xs } = colg;
    return (
      <div className={styles.descriptionsItem}>
        <Row>
          {DesItem.map(el => {
            return (
              <Col key={el.key} xxl={xxl} xl={xl} lg={lg} md={md} sm={sm} xs={xs}>
                <div className={styles.title}>
                  {typeof el.title === 'function' && el.title.nodeType !== 'number'
                    ? el.title()
                    : el.title}
                </div>
                <div className={styles.value}>
                  {typeof el.value === 'function' && el.title.nodeType !== 'number'
                    ? el.value()
                    : el.value}
                </div>
              </Col>
            );
          })}
        </Row>
      </div>
    );
  }
}
