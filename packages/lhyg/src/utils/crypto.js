import CryptoJS from 'crypto-js';
import { platformHook } from '@/utils/platformConfig';

/**  解密 */
export const decrypt = (str, key = 'Card@Decode12345') => {
  var key = CryptoJS.enc.Utf8.parse(key);
  var decrypt = CryptoJS.AES.decrypt(str, key, {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7,
  });
  return CryptoJS.enc.Utf8.stringify(decrypt).toString();
};

/**  加密 */
export const encrypt = (str, key = 'Card@Decode12345') => {
  var sKey = CryptoJS.enc.Utf8.parse(key);
  var sContent = CryptoJS.enc.Utf8.parse(str);
  var encrypted = CryptoJS.AES.encrypt(sContent, sKey, {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7,
  });
  return encrypted.toString();
};

//接口解密字段列表
const decryptFieldList = ['phone','realName','cardNo'];

// 检测数据类型的功能函数
const checkedType = target =>
  Object.prototype.toString
    .call(target)
    .replace(/\[object (\w+)\]/, '$1')
    .toLowerCase();

/**  接口递归加密 */
export const encryptClone = (target, hash = new WeakMap()) => {
  let result;
  let type = checkedType(target);
  if (type === 'object') result = {};
  else if (type === 'array') result = [];
  else return target;
  if (hash.get(target)) return target;

  let copyObj = new target.constructor();
  hash.set(target, copyObj);
  for (let key in target) {
    if (checkedType(target[key]) === 'object' || checkedType(target[key]) === 'array') {
      result[key] = decodeClone(target[key], hash);
    } else {
      let keyValue = target[key];
      if (decryptFieldList.includes(key)) {
        keyValue = encrypt(target[key], platformHook().platformEncryption());
      }
      result[key] = keyValue;
    }
  }
  return result;
};
