const storageStr = 'bussiness-web-x-';

export function getToken() {
  return localStorage.getItem(`token`);
}

export function setToken(str) {
  if (str) {
    localStorage.setItem(`${storageStr}token`, str);
  }
}

export function getShopId() {
  return localStorage.getItem(`${storageStr}shopId`);
}

export function setShopId(str) {
  if (str) {
    localStorage.setItem(`${storageStr}shopId`, str);
  }
}

export function getShopAdminId() {
  return localStorage.getItem(`${storageStr}shopAdminId`);
}

export function setShopAdminId(str) {
  if (str) {
    localStorage.setItem(`${storageStr}shopAdminId`, str);
  }
}

export function setCurrentUser(obj) {
  if (obj) {
    localStorage.setItem(`${storageStr}currentUser`, JSON.stringify(obj));
  }
}

export function getCurrentUser() {
  const currentUserString = localStorage.getItem(`${storageStr}currentUser`);
  if (currentUserString) {
    return JSON.parse(currentUserString);
  }
  return null;
}
