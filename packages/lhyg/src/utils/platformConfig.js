// eslint-disable-next-line @typescript-eslint/no-unused-vars
const { NODE_ENV } = process.env;
export const isAiQian = false;

export const aiQianDown = () => {
  let url = '/hzsx/business/order/downOrderContract';
  if (isAiQian) {
    url = '/hzsx/business/order/downAsignOrderContract';
  }
  return url;
};

// http://192.168.110.152:9002 章超
// http://192.168.110.198:9002 罗鑫
// http://192.168.110.165:9002 沈林江
// http://192.168.110.207:9002 詹黎锋
// https://wxsc.lzyigou.com
// https://ddyg.yuexiaozu.com
// https://kksc.richtech123.com
// https://yxyg.manchuiwl.com 龙鱼（共享）
export const apiUrl = NODE_ENV === 'development' ? 'http://192.168.110.165:9002' : '';

export const platformHook = () => {
  const platform = [
    {
      url: 'http://192.168.110.102:8001',
      title: '本地商城后台管理系统',
      isCard: true,
      newControl: true,
      sign: true,
      isInterior: true,
      encryption: '',
    },
    {
      url: 'localhost:8001',
      title: '本地商城后台管理系统',
      isCard: true,
      newControl: true,
      sign: true,
      isInterior: true,
      // KF2XSJaN7xbPRMoB
      encryption: '',
    },
    {
      url: 'http://192.168.110.102:8003',
      title: '本地商城后台管理系统',
      isCard: true,
      newControl: true,
      sign: true,
      isInterior: true,
      encryption: 'KF2XSJaN7xbPRMoB',
    },
    {
      url: 'kksc.richtech123.com',
      title: '可可商家后台管理系统',
      isCard: true,
      newControl: false,
      sign: true,
      isInterior: true,
      encryption: '',
    },
    {
      url: 'ddyg.yuexiaozu.com',
      title: '百合商家后台管理系统',
      isCard: true,
      newControl: true,
      sign: true,
      isInterior: true,
      encryption: '',
    },
    {
      url: 'wxsc.lzyigou.com',
      title: '大福商家后台管理系统',
      isCard: true,
      newControl: true,
      sign: true,
      isInterior: true,
      encryption: 'KF2XSJaN7xbPRMoB',
    },
    {
      url: 'xksc.lzyigou.com',
      title: '星空商家后台管理系统',
      isCard: true,
      newControl: true,
      sign: true,
      isInterior: true,
      encryption: '',
    },
    {
      url: 'xksc.yuezukeji.com',
      title: '时光商家后台管理系统',
      isCard: true,
      newControl: true,
      sign: true,
      isInterior: true,
      encryption: 'KF2XSJaN7xbPRMoB',
    },
    {
      url: 'ys.yuexiaozu.com',
      title: '来这易购商家后台管理系统',
      isCard: true,
      newControl: false,
      sign: false,
      isInterior: true,
      encryption: '',
    },
    {
      url: 'yxyg.manchuiwl.com',
      title: '龙鱼商家后台管理系统',
      isCard: false,
      newControl: true,
      sign: true,
      isInterior: false,
      encryption: '',
    },
    {
      url: 'htyg.manchuiwl.com',
      title: '海豚商家后台管理系统',
      isCard: false,
      newControl: true,
      sign: true,
      isInterior: false,
      encryption: '',
    },
    {
      url: 'dfsc.yzgouwu.com',
      title: '大发商家后台管理系统',
      isCard: true,
      newControl: true,
      sign: false,
      isInterior: false,
      encryption: 'KF2XSJaN7xbPRMoB',
    },
    {
      url: 'jysc.benxicy.com.cn',
      title: '金翼商家后台管理系统',
      isCard: false,
      newControl: true,
      sign: true,
      isInterior: false,
      encryption: 'KF2XSJaN7xbPRMoB',
    },
    {
      url: 'jd.xinywh.com',
      title: '大吉商家后台管理系统',
      isCard: true,
      newControl: true,
      sign: false,
      isInterior: false,
      encryption: '',
    },
    {
      url: 'hwg.rhdzsw.cn',
      title: '三只鸭商家后台管理系统',
      isCard: false,
      newControl: true,
      sign: false,
      isInterior: false,
      encryption: 'KF2XSJaN7xbPRMoB',
    },
    {
      url: 'llyg.lingdunzuji.cn',
      title: '玲珑易购商家后台管理系统',
      isCard: false,
      newControl: true,
      sign: true,
      isInterior: false,
      encryption: 'KF2XSJaN7xbPRMoB',
    },
    {
      url: 'ttg.mxnb.top',
      title: '天天购商家后台管理系统',
      isCard: false,
      newControl: true,
      sign: false,
      isInterior: false,
      encryption: 'KF2XSJaN7xbPRMoB',
    },
  ];

  /** 当前商城的名称 */
  const platformName = () => {
    let title = '';
    platform.forEach(el => {
      if (el.url.includes(window.location.host)) {
        title = el.title;
      }
    });
    return title;
  };

  /** 当前商城是否买卡 */
  const platformCard = () => {
    let bol = false;
    platform.forEach(el => {
      if (el.url.includes(window.location.host)) {
        bol = el.isCard;
      }
    });
    return bol;
  };

  /** 当前商城是否用新风控 */
  const platformNewControl = () => {
    let bol = false;
    platform.forEach(el => {
      if (el.url.includes(window.location.host)) {
        bol = el.newControl;
      }
    });
    return bol;
  };

  /** 当前上传是否需要签约存在  蚂蚁相关 */
  const platformSign = () => {
    let bol = false;
    platform.forEach(el => {
      if (el.url.includes(window.location.host)) {
        bol = el.sign;
      }
    });
    return bol;
  };

  /**  是否内部系统 */
  const platformIsInterior = () => {
    if (window.location.hostname == 'ddyg.yuexiaozu.com') {
      return true;
    }

    if (window.location.hostname == 'wxsc.lzyigou.com') {
      return true;
    }
    if (window.location.hostname == 'xksc.lzyigou.com') {
      return true;
    }
    if (window.location.hostname == 'xksc.yuezukeji.com') {
      return true;
    }
    if (window.location.hostname == 'jysc.benxicy.com.cn') {
      return true;
    }
    if (window.location.hostname == 'localhost') {
      return true;
    }
    return false;
    // let bol = false;
    // platform.forEach(el => {
    //   if (el.url.includes(window.location.host)) {
    //     bol = el.isInterior;
    //   }
    // });
    // return bol;
  };

  /** 是否接口加密 */
  const platformEncryption = () => {
    let str = '';
    platform.forEach(el => {
      if (el.url.includes(window.location.host)) {
        str = el.encryption;
      }
    });
    return str;
  };

  return {
    platformName,
    platformCard,
    platformNewControl,
    platformSign,
    platformIsInterior,
    platformEncryption,
  };
};
