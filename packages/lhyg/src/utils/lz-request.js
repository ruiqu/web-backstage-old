// /**
//  * request 网络请求工具
//  * 更详细的 api 文档: https://github.com/umijs/umi-request
//  */
// import { extend } from 'umi-request';
import axios from 'axios';
import { notification } from 'antd';
import { message } from 'antd';
import { getPageQuery } from '@/utils/utils';
import { router } from 'umi';
import { stringify } from 'querystring';
import { apiUrl, platformHook } from '@/utils/platformConfig';
import { encrypt, decrypt, encryptClone } from './crypto';

//
const codeMessage = {
  200: '服务器成功返回请求的数据。',
  201: '新建或修改数据成功。',
  202: '一个请求已经进入后台排队（异步任务）。',
  204: '删除数据成功。',
  400: '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
  401: '用户没有权限（令牌、用户名、密码错误）。',
  403: '用户得到授权，但是访问是被禁止的。',
  404: '发出的请求针对的是不存在的记录，服务器没有进行操作。',
  406: '请求的格式不可得。',
  410: '请求的资源被永久删除，且不会再得到的。',
  422: '当创建一个对象时，发生一个验证错误。',
  500: '服务器发生错误，请检查服务器。',
  502: '网关错误。',
  503: '服务不可用，服务器暂时过载或维护。',
  504: '网关超时。',
};
/**
 * 异常处理程序
 */

const errorHandler = error => {
  const { response } = error;

  if (response && response.status) {
    const errorText = codeMessage[response.status] || response.statusText;
    const { status, url } = response;
    notification.error({
      message: `请求错误 ${status}: ${url}`,
      description: errorText,
    });
  } else if (!response) {
    notification.error({
      description: '您的网络发生异常，无法连接服务器',
      message: '网络异常',
    });
  }

  return response;
};
// /**
//  * 配置request请求时的默认参数
//  */
//
// const request = extend({
//   errorHandler,
//   // 默认错误处理
//   credentials: 'include', // 默认请求是否带上cookie
//
// });
//
// request.interceptors.response.use(response => {
//   return new Promise(((resolve, reject) => {
//     response.json().then(res=>{
//       if (!res.errorCode) {
//         resolve(res.data)
//         return res.data
//       } else {
//         const msg = res.errorMessage? res.errorMessage : res.errorMsg
//         message.error('操作失败：'+ msg)
//         reject(res)
//       }
//     }).catch(err=>{
//       reject(err)
//     })
//     return response;
//   }))
// });
// export default request;
const { NODE_ENV } = process.env;
const urls = () => {
  const { host } = location;
  if (host.includes('zyjtest')) {
    // 测试环境
    return 'http://zyjtest.zwzrent.net/hzsx';
  } else if (host.includes('pre')) {
    // 预发环境
    return 'https://pre.zwzrent.cn/hzsx';
  } else if (host.includes('127.0.0.1') || host.includes('localhost')) {
    // 本地开发
    return 'http://zyjtest.zwzrent.net/hzsx';
  } else {
    // 正式环境
    return 'https://www.zwzrent.cn/hzsx';
  }
  // if (location && location.protocol.split(':')[0] === 'http') {
  //   return 'http://zyjtest.zwzrent.net/hzsx';
  // } else {
  //   return 'https://www.zwzrent.cn/hzsx';
  // }
};
const lzRequest = axios.create({
  //这个测试域名后期正式编译的时候注释掉
  baseURL: `${apiUrl}/zyj-backstage-web`,

  withCredentials: true,
  timeout: 60 * 1000,
  headers: {
    'Content-Type': 'application/json;text/plain;charset=UTF-8',
  },
});

const decryptApiList = [
  'hzsx/userAudit/getUserAuditList',
  'hzsx/ope/order/pageAuthRecord',
  'hzsx/ope/order/queryOverOrderByStagesNew',
  'hzsx/business/order/queryOverDueOrdersByConditionNew',
  'hzsx/business/order/queryOrderByRentNew',
  'hzsx/userWhite/selectPage',
  'hzsx/userBlack/selectPage',
];
lzRequest.interceptors.request.use(config => {

  if (config.method == 'post' && platformHook().platformEncryption()) {
    decryptApiList.forEach(el => {
      if (el && config.url.includes(el)) {
        config.data = encrypt(JSON.stringify(config.data), platformHook().platformEncryption());
        console.log('这1',config.data, decrypt(config.data, platformHook().platformEncryption()));
      }
    });
  }

  console.log('调用接口');
  if (config.method == 'get' && platformHook().platformEncryption()) {
    decryptApiList.forEach(el => {
      if (el && config.url.includes(el)) {
        config.params = encryptClone(config.params);
      }
    });

    // console.log(config)
  }

  return config;
});
//接口解密接口列表
const encryptApiList = [
  'hzsx/userAudit/queryBusinessUserRiskDetail',
  'hzsx/userAudit/getUserAuditList',
  'hzsx/ope/order/pageAuthRecord',
  'hzsx/ope/order/queryOverOrderByStagesNew',
  'hzsx/business/order/queryOverDueOrdersByConditionNew',
  'hzsx/business/order/queryOrderByRentNew',
  'hzsx/userWhite/selectPage',
  'hzsx/userBlack/selectPage',
];
// response interceptor
lzRequest.interceptors.response.use(
  response => {
    let res = response.data;
    if (platformHook().platformEncryption())
      encryptApiList.forEach(el => {
        if (el && response.config.url.includes(el)) {
          res = eval('(' + decrypt(res.encrypt, platformHook().platformEncryption()) + ')');
        }
      });
    console.log('调用接口', res);
    if (res.appid) localStorage.setItem('appid', res.appid);
    if (res.code === 'LOGIN_INVALID') {
      const { redirect } = getPageQuery(); // Note: There may be security issues, please note
      if (window.location.pathname !== '/user/login' && !redirect) {
        message.error('登陆失败！请重新登陆～');
        router.replace({
          pathname: '/user/login',
          search: stringify({
            redirect: window.location.href,
          }),
        });
      }
    }
    if (res.code === '444444') {
      message.error(res.errorMsg);
      localStorage.removeItem('token');
      router.replace({
        pathname: '/user/login',
        search: stringify({
          redirect: window.location.href,
        }),
      });
    }
    if (res.errorCode || res.error || res.errorMsg) {
      const msg = res.errorMessage || res.errorMsg || res.message;
      if (msg === 'AUTH_FAILED') {
        message.error('权限不足！');
      } else {
        message.error('操作失败：' + msg);
      }
      return Promise.reject(res);
    } else {
      // 兼容一些非正常结构
      const data = res.data;
      return data;
    }
  },
  // errorHandler
);

export default lzRequest;
