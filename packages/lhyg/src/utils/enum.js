export const orderStatusMap = {
  '01': '待支付',
  // '02': '支付中',
  // '03': '已支付申请关单',
  '16': '机审拒绝',
  '17': '待审批',
  '11': '待人审',
  '04': '待发货',
  '05': '待确认收货',
  '06': '试用中',
  '07': '待结算',
  '08': '结算待支付',
  '09': '订单完成',
  '10': '交易关闭',
  '12': '待归还',
  '19': '黑名单用户',
  '20': '黑名单渠道',
  '21': '等待机审',
};

//审核状态
export const verfiyStatusMap = ['待审', '通过', '拒绝'];

export const orderCloseStatusMap = [
  {
    value: '01',
    content: '未支付用户主动申请',
  },
  {
    value: '02',
    content: '支付失败',
  },
  {
    value: '03',
    content: '超时支付',
  },
  {
    value: '04',
    content: '已支付用户主动申请',
  },
  {
    value: '05',
    content: '风控拒绝',
  },
  {
    value: '06',
    content: '商家关闭(客户要求)',
  },
  {
    value: '07',
    content: '商家风控关闭订单',
  },
  {
    value: '08',
    content: '商家超时发货',
  },
  {
    value: '09',
    content: '平台关闭订单',
  },
  {
    value: '10',
    content: '用户主动申请',
  },
  {
    value: '11',
    content: '平台风控关闭订单',
  },
];

export const payStatus = {
  '1': '待支付',
  '2': '已支付',
  '3': '逾期已支付',
  '4': '逾期待支付',
  '5': '已取消',
  '6': '已结算',
  '7': '已退款,可用',
};

export const withShopStatus = {
  UNPAID: '未支付',
  // 'DEALING': '支付中',
  PAID: '已支付',
  FAIL: '支付失败',
};

export const AuditStatus = {
  '00': '待审批',
  // 'DEALING': '支付中',
  '01': '审批通过',
  '02': '审批拒绝',
};

export const AuditReason = {
  '01': '法院涉案',
  '02': '纯白户',
  '03': '客户失联',
  '04': '不提供资料',
  '05': '多余订单',
  '06': '重新下单',
  '07': '客户不同意方案',
  '08': '终审拒绝',
  '09': '高炮过多',
  '10': '删除账单',
  '11': '在逾',
  '12': '黑名单',
  '13': '毙掉',
  '14': '年纪太小',
  '15': '年纪太大',
  '16': '还款能力弱',
  '17': '还款异常多',
};

export const PayMent = {
  ZFB: '支付宝',
  VALET_PAYMENT: '代客支付',
  MYL: '蚂蚁链扣款',
  TTF: '统统付',
  WX: '微信',
  FYPAY: '富友支付',
};

export const confirmSettlementType = {
  '01': '完好',
  '02': '损坏',
  '03': '丢失',
  '04': '违约',
};
export const confirmSettlementStatus = {
  '01': '待支付',
  '02': '待支付',
  '03': '已支付',
  '04': '待支付',
};

export const BuyOutEnum = ['不支持买断', '支持提前买断', '仅到期可买断'];

export const defaultPlaceHolder = '-'; // 统一的默认占位符，便于修改

//渠道统计
export const qdDayEnum = [
  {
    key: '01',
    value: '昨天',
  },
  {
    key: '02',
    value: '今天',
  },
  {
    key: '03',
    value: '前天',
  },
  {
    key: '04',
    value: '7天',
  },
  {
    key: '05',
    value: '15天',
  },
  {
    key: '06',
    value: '30天',
  },
];

export const aiQianStatus = {
  '0': '未签约',
  '1': '签约中',
  '2': '签约完成',
  '3': '过期',
  '4': '拒签',
  '6': '作废',
  '-2': '异常',
};

export const clientRegisterStatus = {
  '1': '注册通过',
  '2': 'OCR通过',
  '3': '活体通过',
  '4': '联系人通过',
  '5': '风控未通过',
  '6': '待人审',
  '7': '审核通过',
  '8': '审核未通过',
  '9': '待风控',
  '10': '审核关闭',
  '18': '等待风控结果',
  '44': '风控异常',
};
export const clientRegisterStatusMap = [
  {
    id: '1',
    title: '注册通过',
  },
  {
    id: '2',
    title: 'OCR通过',
  },
  {
    id: '3',
    title: '活体通过',
  },
  {
    id: '4',
    title: '联系人通过',
  },
  {
    id: '5',
    title: '风控未通过',
  },
  {
    id: '6',
    title: '待人审',
  },
  {
    id: '7',
    title: '审核通过',
  },
  {
    id: '8',
    title: '审核未通过',
  },
  {
    id: '9',
    title: '待风控',
  },
  {
    id: '10',
    title: '审核关闭',
  },
  {
    id: '18',
    title: '等待风控结果',
  },
  {
    id: '44',
    title: '风控异常',
  },
];

export const antdigitalStatus = {
  0: '未代扣',
  1: '代扣成功',
  2: '代扣失败',
  3: '取消代扣',
  4: '退款',
};

export const FYpayStatus = {
  '00': '未支付',
  '01': '支付中',
  '02': '支付成功',
  '03': '支付失败',
  '04': '取消支付',
  '05': '全额退款',
};
