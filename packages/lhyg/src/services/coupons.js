import request from '@/utils/request';
function getToken() {
  const token = localStorage.getItem('token');
  return token;
}
export const getCouponListByPage = body =>
  request('/hzsx/business/shop/getCouponListByPage', {
    method: 'GET',
    body,
  });

export const saveCouponByShopId = body =>
  request('/hzsx/business/shop/saveCouponByShopId', {
    method: 'POST',
    body,
  });

export const updateShopCoupon = body =>
  request('/hzsx/business/shop/updateShopCoupon', {
    method: 'POST',
    body,
  });

export const delCouponById = body =>
  request('/hzsx/business/shop/delCouponById', {
    method: 'GET',
    body,
  });
export const setBusinessPrdouct = body =>
  request('/hzsx/business/product/selectBusinessPrdouct', {
    method: 'POST',
    body,
  });
export const getUpload = body =>
  request('/hzsx/business/coupon/readPhoneFromExcel', {
    method: 'POST',
    body,
  });
// export const couponAdd = body =>
//   request('ope/coupon/add', {
//     method: 'POST',
//     body,
//   });

export async function couponAdd(params) {
  return request('/hzsx/couponTemplate/add', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
export async function modify(params) {
  return request('/hzsx/couponTemplate/modify', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}

export async function couponPackagemodify(params) {
  return request('/hzsx/couponPackage/modify', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}

// export async function list(params) {
//   return request('/hzsx/couponPackage/queryPage', {
//     method: 'POST',
//     data: params,
//     headers: { token: getToken() },
//   });
// }
// export const deleteList = body =>
//   request(`ope/coupon/delete?id=${body.id}`, {
//     method: 'GET',
//   });
export async function deleteList(params) {
  return request(`/hzsx/couponTemplate/delete?id=${params.id}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}

export async function couponPackageDelete(params) {
  return request(`/hzsx/couponPackage/delete?id=${params.id}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}
// export const getSecondaryCategory = body =>
//   request(`ope/category/selectReceptionCategoryList?categoryId=${body.id}`,{
//       method: 'GET'
//   })

// export const detailBindList = body =>
//   request('ope/coupon/detailBindList', {
//     method: 'POST',
//     body,
//   });
export const upDate = body =>
  request('/hzsx/ope/coupon/update', {
    method: 'POST',
    body,
  });
export const couponPackage = body =>
  request('/hzsx/ope/couponPackage/add', {
    method: 'POST',
    body,
  });
// export const couponPackageList = body =>
//   request('ope/couponPackage/list', {
//     method: 'POST',
//     body,
//   });
export async function queryPage(params) {
  return request('/hzsx/couponPackage/queryPage', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}

export async function couponTemplatequeryPage(params) {
  return request('/hzsx/couponTemplate/queryPage', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}

export async function getAssignAbleTemplate(params) {
  return request('/hzsx/couponTemplate/getAssignAbleTemplate', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}

// export const couponPackageDelete = body =>
//   request(`ope/couponPackage/delete?id=${body.id}`, {
//     method: 'get',
//   });
export const getPageData = body =>
  request(`/hzsx/ope/couponPackage/getUpdatePageData?id=${body.id}`, {
    method: 'get',
  });

export async function getUpdatePageDatas(params) {
  return request(`/hzsx/couponPackage/getUpdatePageData?id=${params.id}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}
export const getupdate = body =>
  request('/hzsx/ope/couponPackage/update', {
    method: 'POST',
    body,
  });

export async function add(params) {
  return request('/hzsx/couponPackage/add', {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
// export const detailDataList = body => {
//   return request('ope/coupon/detail', {
//     method: 'GET',
//     body,
//   });
// };

export const getGoods = body => {
  return request('/hzsx/ope/product/selectExaminePoroductList', {
    method: 'POST',
    body,
  });
};

// export const getSelectAlls = body =>
//   request(`ope/category/selectAll`, {
//     method: 'GET',
//   });

export async function getSelectAlls(params) {
  return request(`/hzsx/category/selectParentCategoryList`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}

// export async function couponTemplatequeryPage(params) {
//   return request('/hzsx/couponTemplate/queryPage', {
//     method: 'POST',
//     data: params,
//     headers: { token: getToken() },
//   });
// }
export async function detailBindList(params) {
  return request(`/hzsx/userCoupon/queryPage`, {
    method: 'post',
    data: params,
    headers: { token: getToken() },
  });
}
// export const getUpdatePageData = body =>
//   request(`ope/coupon/getUpdatePageData?id=${body.id}`, {
//     method: 'get',
//   });
export async function getUpdatePageData(params) {
  return request(`/hzsx/couponTemplate/getUpdatePageData?id=${params.id}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}
