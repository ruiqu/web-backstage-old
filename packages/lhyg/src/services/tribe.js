import request from "@/services/baseService";

export default {
  queryTribePage:(data)=>{
    return request(`/hzsx/tribe/queryTribePage`, data)
  },
  deleteTribe:(data)=>{
    return request(`/hzsx/tribe/deleteTribe`, data)
  },
  addTribe:(data)=>{
    return request(`/hzsx/tribe/addTribe`, data)
  },
  queryTribeDetail:(data)=>{
    return request(`/hzsx/tribe/queryTribeDetail`, data)
  },
  modifyTribe:(data)=>{
    return request(`/hzsx/tribe/modifyTribe`, data)
  },
}
