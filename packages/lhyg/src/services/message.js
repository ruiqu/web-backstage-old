import request from '@/utils/request';
function getToken() {
  const token = localStorage.getItem('token');
  return token;
}
//运营模板消息分页
export async function queryOpeOrderByCondition(params) {
  return request(`/hzsx/messageTem/getMessageTemplateByPage`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}
export async function saveMessageTemplate(params) {
  return request(`/hzsx/messageTem/saveMessageTemplate`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}

export async function updateMessageTemplate(params) {
  return request(`/hzsx/messageTem/updateMessageTemplate`, {
    method: 'POST',
    data: params,
    headers: { token: getToken() },
  });
}

export async function delMessageTemplate(params) {
  return request(`/hzsx/messageTem/delMessageTemplate?id=${params.id}`, {
    method: 'get',
    data: params,
    headers: { token: getToken() },
  });
}
