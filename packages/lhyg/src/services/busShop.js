import request from "@/services/baseService";

export default {
  selectBusShopInfo:(data)=>{
    return request(`/hzsx/busShop/selectBusShopInfo`, data, 'get')
  },
  getShopContractData:(data)=>{
    return request(`/hzsx/busShop/getShopContractData`, data, 'get')
  },
  // busShop/signShopContract
  signShopContract:(data)=>{
    return request(`/hzsx/busShop/signShopContract`, data, 'get')
  },
  updateShopAndEnterpriseInfo:(data)=>{
    return request(`/hzsx/busShop/updateShopAndEnterpriseInfo`, data)
  },
  selectShopRuleAndGiveBackByShopId:(data)=>{
    return request(`/hzsx/busShop/selectShopRuleAndGiveBackByShopId`, data, 'get')
  },
  delShopGiveBackAddressById:(data)=>{
    return request(`/hzsx/shopAddress/delShopGiveBackAddressById`, data, 'get')
  },
  getByCode:(data)=>{
    return request(`/hzsx/config/getByCode?code=esign:fee`, data, 'get')
  },
  saveShopGiveBackAddress:(data)=>{
    return request(`/hzsx/shopAddress/saveShopGiveBackAddress`, data)
  },
  updateShopGiveBackAddressById:(data)=>{
    return request(`/hzsx/shopAddress/updateShopGiveBackAddressById`, data)
  },
  generateOrderContract:(data)=>{
    return request(`/hzsx/business/order/generateOrderContract`, data, 'get')
  },
  // /hzsx/business/order/NewgenerateOrderContract  三要素爱签签约
  // business/order/generateOrderContract
}
