import request from "@/services/baseService";

export default {
  queryBackstageUserPage:(data)=>{
    return request(`/hzsx/user/queryBackstageUserPage`, data)
  },
  updateAuth:(data)=>{
    return request(`/hzsx/user/updateAuth`, data)
  },
  queryBackstageUserDetail:(data)=>{
    return request(`/hzsx/user/queryBackstageUserDetail`, data)
  },
  updateByUser:(data)=>{
    return request(`/hzsx/user/updateByUser`, data)
  },
  modifyBackstageUser:(data)=>{
    return request(`/hzsx/user/modifyBackstageUser`, data)
  },
  isEnabledUser:(data)=>{
    return request(`/hzsx/user/isEnabled`, data, 'post')
  },
  delete:(data)=>{
    return request(`/hzsx/user/delete`, data, 'get')
  },
  authPage:(data)=>{
    return request(`/hzsx/user/modifyBackstageUser`, data, 'get')
  },
  addBackstageUser:(data)=>{
    return request(`/hzsx/user/addBackstageUser`, data)
  }
  
}
