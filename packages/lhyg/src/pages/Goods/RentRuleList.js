import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Button, Table, Icon, Divider, Popconfirm, Spin } from 'antd';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import busShopService from "@/services/busShop";

@connect(({ rentRule, loading }) => ({
  ...rentRule,
  loading: loading.models.rentRule,
}))
class RentRuleList extends PureComponent {
  
  state = {
    tableData: []
  }
  
  columns = [
    {
      title: '名称',
      dataIndex: 'name',
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
    },
    {
      title: '操作',
      dataIndex: 'action',
      render: (_, record) => (
        <div>
          <a onClick={() => this.gotoDetail(record.id)}>
            <Icon type="edit" />
          </a>
          <Divider type="vertical" />
          <Popconfirm title="是否删除该规则？" onConfirm={() => this.handleDelete(record.id)}>
            <a>
              <Icon type="delete" />
            </a>
          </Popconfirm>
        </div>
      ),
    },
  ];
  
  componentWillMount() {
    this.getList()
  }

  handleDelete = id => {
  
  };
  
  getList = () => {
    this.setState({
      loading: true,
    });
    busShopService.selectShopRuleAndGiveBackByShopId().then(res => {
      this.setState({
        tableData: res.records || [],
      });
    }).finally(() => {
      this.setState({
        loading: false,
      });
    })
  }

  handleAdd = () => {
    const { dispatch } = this.props;
    dispatch(routerRedux.push('/goods/rentRuleList/rentRuleDetail/add'));
  };

  gotoDetail = id => {
    const { dispatch } = this.props;
    dispatch(routerRedux.push(`/goods/rentRuleList/rentRuleDetail/${id}`));
  };

  render() {
    const { tableData, loading } = this.state;
    return (
      <PageHeaderWrapper title={false}>
        <Spin spinning={loading}>
          <Card bordered={false} style={{ marginTop: 20 }}>
            <Button onClick={this.handleAdd} type="primary" shape="round" style={{ marginBottom: '30px' }}>
              新增租赁规则模板
            </Button>
            <Table
              rowKey={record => record.id}
              columns={columns}
              dataSource={tableData}
              size="middle"
              pagination={false}
              bordered />
          </Card>
        </Spin>
      </PageHeaderWrapper>
    );
  }
}

export default RentRuleList;
