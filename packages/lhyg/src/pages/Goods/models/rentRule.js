import { message } from 'antd';
import { routerRedux } from 'dva/router';
import * as servicesApi from '../services';

export default {
  spacename: 'rentRule',
  state: {
    list: [],
    currentId: null,
  },
  effects: {
    *getShopRentRulesByShopId(_, { call, put }) {
      const res = yield call(servicesApi.getShopRentRulesByShopId);
      if (res) {
        yield put({
          type: 'saveList',
          payload: res.data,
        });
      }
    },

    *saveShopRent({ payload }, { call, put }) {
      let work = servicesApi.saveShopRentRule;
      if (payload.id) {
        work = servicesApi.updateShopRentRulesById;
      }
      const res = yield call(work, payload);
      if (res) {
        message.success('保存成功');
        yield put(routerRedux.push('/goods/rentRuleList'));
      }
    },

    *delShopRentRulesByshopId({ payload }, { call, put }) {
      const res = yield call(servicesApi.delShopRentRulesByshopId, payload);
      if (res) {
        yield put({
          type: 'getShopRentRulesByShopId',
        });
        message.success('删除成功');
      }
    },
  },
  reducers: {
    saveList(state, { payload }) {
      return { ...state, list: payload };
    },
    setCurrentId(state, { payload }) {
      return { ...state, currentId: payload };
    },
  },
};
