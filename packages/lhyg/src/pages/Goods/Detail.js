import 'braft-editor/dist/index.css';
import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Card, message, Spin } from 'antd';
import { cloneDeep, isEqual } from 'lodash';

import { PageHeaderWrapper } from '@ant-design/pro-layout';
import goodsService from '@/services/goods';
import MainForm from '@/pages/Goods/goodsForm';
import { router } from 'umi';
import shopAdditionalService from '@/services/shopAdditional';
import busShopService from '@/services/busShop';
import RouterWillLeave from '@/components/RouterWillLeave';

// 新增和修改商品的页面

@connect(({ goodsDetail, loading }) => ({
  goodsDetail,
  loading: loading.models.goodsDetail,
}))
class GoodsDetail extends PureComponent {
  state = {
    id: '',
    buyOutValue: 0,
    loading: false,
    goodsDetail: {},
    categories: [],
    addServiceTableData: [],
    districts: [],
    giveBackList: [],
  };

  componentWillMount() {
    const {
      match: {
        params: { id },
      },
    } = this.props;

    this.findCategories();
    this.getShopAdditionalServiceList();
    this.getGiveBackList();
    this.selectDistrict();
    if (id && id !== 'add') {
      this.getData(id);
    } else {
      // 给一个商品的初始化的数据
      this.setState({
        goodsDetail: {
          name: '',
          oldNewDegree: 1,
          labels: [],
          additionals: null,
          shopAdditionals: [],
          buyOutSupport: 1,
          returnRule: 1, // 默认是支持提前归还的
          productSkuses: [],
          minRentCycle: '',
          maxRentCycle: '',
          type: 1,
          detail: '',
          province: '',
          city: '',
          freightType: 'PAY',
          returnfreightType: 'PAY', // 归还快递在新增时的默认值
          addIds: [],
          images: [],
          src: null,
          productId: '',
          salesVolume: null,
          lowestSalePrice: null,
          parameters: [],
          productCouponList: null,
          collected: null,
          specs: [
            { name: '颜色', opeSpecId: 1, values: [] },
            { name: '规格', opeSpecId: 2, values: [] },
          ],
          serviceTel: null,
          //商家平台 1 h5 2 支付宝 3 app 4微信小程序
          sjPlats: '',
        },
        id,
      });
    }
  }

  getData = (id = this.state.id) => {
    this.setState({
      loading: true,
    });
    goodsService
      .selectProductEdit({
        id,
      })
      .then(res => {
        if (res.sjPlats) {
          res.sjPlats = res.sjPlats.split(',');
        }
        this.setState({
          buyOutValue: 0,
          goodsDetail: res,
          id,
        });
      })
      .finally(() => {
        this.setState({
          loading: false,
        });
      });
  };

  findCategories = () => {
    goodsService.findCategories().then(res => {
      let data = res.httpStatus ? [] : res;
      this.setState({
        categories: data || [],
      });
    });
  };

  getShopAdditionalServiceList = () => {
    shopAdditionalService
      .selectShopAdditionalServicesList({
        pageSize: 9999,
        pageNumber: 1,
        name: '',
      })
      .then(res => {
        // message.success('获取数据成功')
        this.setState({
          addServiceTableData: res.records || [],
        });
      });
  };

  selectDistrict = () => {
    goodsService.selectDistrict().then(res => {
      let data = res.httpStatus ? [] : res;
      this.setState({
        districts: data || [],
      });
    });
  };

  getGiveBackList = () => {
    busShopService.selectShopRuleAndGiveBackByShopId().then(res => {
      let data = res.httpStatus ? [] : res;
      this.setState({
        giveBackList: data || [],
      });
    });
  };

  setInitList = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'goodsDetail/selectShopRuleAndGiveBackByShopId',
    });
  };

  // 改变规格，改变之后重新处理sku的渲染
  changeSpecs = (specs, opeSpecId) => {
    const { goodsDetail } = this.state;
    const _goodsDetail = cloneDeep(goodsDetail);
    _goodsDetail.specs = _goodsDetail.specs.map(f => {
      if (f.opeSpecId === opeSpecId) {
        return specs;
      }
      return f;
    });
    this.setState(
      {
        goodsDetail: _goodsDetail,
      },
      () => {
        this.handleSku();
      },
    );
    // const old_specs = _goodsDetail.specs.find(f=> f.opeSpecId !== _type).values
  };

  // 处理SKu渲染的逻辑
  handleSku = () => {
    const { goodsDetail } = this.state;
    const _goodsDetail = cloneDeep(goodsDetail);
    let _specs = {},
      colorSpecs = {};
    _goodsDetail.specs.forEach(v => {
      if (v.opeSpecId === 1) {
        colorSpecs = v;
      } else {
        _specs = v;
      }
    });
    const _productSkuses = _goodsDetail.productSkuses;
    const colorSpecsLength = colorSpecs.values.length;
    const specsLength = _specs.values.length;
    let _arr = [];
    for (let i = 0; i < colorSpecsLength; i++) {
      for (let j = 0; j < specsLength; j++) {
        const index_p = _productSkuses.findIndex(_p => {
          return (
            _p.specAll[0].platformSpecValue === colorSpecs.values[i].name &&
            _p.specAll[1].platformSpecValue === _specs.values[j].name
          );
        });
        if (index_p > -1) {
          _arr.push(_productSkuses[index_p]);
        } else {
          _arr.push({
            cycs: [{ days: 1, price: '', salePrice: '' }],
            depositPrice: null,
            inventory: '',
            marketPrice: '',
            platformSpecValue: null,
            specAll: [
              {
                opeSpecId: 1,
                platformSpecName: '颜色',
                platformSpecValue: colorSpecs.values[i].name,
              },
              { opeSpecId: 2, platformSpecName: '规格', platformSpecValue: _specs.values[j].name },
            ],
          });
        }
      }
    }
    _goodsDetail.productSkuses = _arr;
    this.setState({
      goodsDetail: _goodsDetail,
    });
  };

  changeSku = (sku, index) => {
    const { goodsDetail } = this.state;
    const _goodsDetail = cloneDeep(goodsDetail);
    _goodsDetail.productSkuses[index] = sku;
    this.setState({
      goodsDetail: _goodsDetail,
    });
  };

  deleteSku = index => {
    const { goodsDetail } = this.state;
    const _goodsDetail = cloneDeep(goodsDetail);
    _goodsDetail.productSkuses.splice(index, 1);
    this.setState({
      goodsDetail: _goodsDetail,
    });
  };

  deleteSpecs = (specs, index) => {
    const { goodsDetail } = this.state;
    const _goodsDetail = cloneDeep(goodsDetail);
    _goodsDetail.specs = specs;
    _goodsDetail.productSkuses.splice(index, 1);
    this.setState({
      goodsDetail: _goodsDetail,
    });
  };

  // 新增或者修改商品的时候触发
  handleSubmit = data => {
    const { goodsDetail, id } = this.state;
    const { categoryIds = [] } = data;
    const isAdd = id && id !== 'add';
    const service = isAdd ? goodsService.updateBusProduct : goodsService.busInsertProduct;
    const postData = {
      ...goodsDetail,
      ...data,
      specs: goodsDetail.specs,
      categoryId: categoryIds[categoryIds.length - 1],
      id: id === 'add' ? null : id,
    };
    postData.salesVolume *= 1;
    postData.isOne = 0;
    postData.buyOutSupport = 0;
    postData.returnRule = 1; //支持提前归还
    postData.isZuwan = 1;
    postData.sjPlats = postData.sjPlats.join(',');
    postData.specs.forEach(el => {
      if (el.opeSpecId == 1) {
        el.values[0].cardType = postData.cardTypeValue;
      }
    });
    postData.productSkuses.forEach(el => {
      el.specAll.forEach(el2 => {
        if (el2.opeSpecId == 1) {
          el2.cardType = postData.cardTypeValue
        }
      });
    });
    // postData.specAll.forEach(el =>{
    //   if(el.opeSpecId == 1){
    //     el.cardType = postData.cardTypeValue
    //   }
    // })
    console.log(postData, 'postData');
    if (this.checkPriceIsInValid(postData)) {
      return Promise.reject('[Error]:参数校验失败');
    }
    postData.addIds = [209];
    console.log(123456789789, postData);
    return service(postData).then(res => {
      const msg = isAdd ? '新增成功' : '更新成功';
      message.success(msg);
      this.cancel();
    });
  };

  checkPriceIsInValid = obj => {
    const skuList = (obj && obj.productSkuses) || [];
    for (const skuObj of skuList) {
      const cycsList = skuObj.cycs;
      for (const cycsObj of cycsList) {
        const { price, salePrice } = cycsObj || {};
        // if (price == undefined) {
        //   message.warn("请补全租赁平均价")
        //   return true
        // }
        // if (salePrice == undefined) {
        //   message.warn("请补全销售价格")
        //   return true
        // }
        // if (sesameDeposit == undefined) {
        //   message.warn("请补全押金")
        //   return true
        // }
      }
    }
    return false;
  };

  onProductBuyout = value => {
    this.setState({
      buyOutValue: value,
    });
  };

  cancel = (isPrompt, isSubmit) => {
    this.setState(
      {
        isPrompt,
        isSubmit,
      },
      () => {
        router.push('/goods/list');
      },
    );
  };

  renderRouterWillLeave() {
    const { isPrompt, isSubmit } = this.state;

    return <RouterWillLeave isPrompt={isPrompt} isSubmit={isSubmit} />;
  }

  render() {
    const {
      loading,
      buyOutValue,
      id,
      goodsDetail,
      categories,
      addServiceTableData,
      districts,
      giveBackList,
    } = this.state;

    const title = id === 'add' ? '新增商品' : '修改商品';

    return (
      <PageHeaderWrapper title={title}>
        <Spin spinning={loading}>
          <Card>
            {/*{*/}
            {/*  this.renderRouterWillLeave()*/}
            {/*}*/}
            {goodsDetail && (
              <MainForm
                goodsDetail={goodsDetail}
                categories={categories}
                addServiceTableData={addServiceTableData}
                changeSpecs={this.changeSpecs}
                deleteSpecs={this.deleteSpecs}
                changeSku={this.changeSku}
                deleteSku={this.deleteSku}
                districts={districts}
                giveBackList={giveBackList}
                handleSubmit={this.handleSubmit}
                setInitList={this.setInitList}
                editType={id}
                cancel={this.cancel}
                buyOutValue={buyOutValue}
                onProductBuyout={value => this.onProductBuyout(value)}
                getGiveBackList={this.getGiveBackList}
              />
            )}
          </Card>
        </Spin>
      </PageHeaderWrapper>
    );
  }
}

export default GoodsDetail;
