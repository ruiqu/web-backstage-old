import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Button, Table, Icon, Divider, Popconfirm, Spin } from 'antd';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

@connect(({ compensate, loading }) => ({
  ...compensate,
  loading: loading.models.compensate,
}))
class CompensateList extends PureComponent {
  componentWillMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'compensate/getShopCompensateRulesByShopId',
    });
  }

  handleDelete = id => {
    const { dispatch } = this.props;
    dispatch({
      type: 'compensate/delShopCompensateRentRulesById',
      payload: { id },
    });
  };

  handleAdd = () => {
    const { dispatch } = this.props;
    dispatch(routerRedux.push('/goods/compensateList/compensateDetail/add'));
  };

  gotoDetail = id => {
    const { dispatch } = this.props;
    dispatch(routerRedux.push(`/goods/compensateList/compensateDetail/${id}`));
  };

  render() {
    const columns = [
      {
        title: '名称',
        dataIndex: 'name',
      },
      {
        title: '创建时间',
        dataIndex: 'createTime',
      },
      {
        title: '操作',
        dataIndex: 'action',
        render: (_, record) => (
          <div>
            <a onClick={() => this.gotoDetail(record.id)}>
              <Icon type="edit" />
            </a>
            <Divider type="vertical" />
            <Popconfirm title="是否删除该规则？" onConfirm={() => this.handleDelete(record.id)}>
              <a>
                <Icon type="delete" />
              </a>
            </Popconfirm>
          </div>
        ),
      },
    ];
    const { list, loading } = this.props;
    return (
      <PageHeaderWrapper title={false}>
        <Spin spinning={loading}>
          <Button
            onClick={this.handleAdd}
            type="primary"
            shape="round"
            style={{ marginBottom: '30px' }}
          >
            新增赔偿规则模板
          </Button>
          <Table rowKey={record => record.id} columns={columns} dataSource={list} size="middle" pagination={false} bordered />
        </Spin>
      </PageHeaderWrapper>
    );
  }
}

export default CompensateList;
