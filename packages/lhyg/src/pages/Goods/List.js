import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Button,
  Card,
  Tabs,
  Table,
  Divider,
  Popconfirm,
  Spin, Badge, Modal, message
} from 'antd';
import { routerRedux } from 'dva/router';
import Search from '../../components/Search';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import goodsService from "@/services/goods";
import CopyToClipboard from '@/components/CopyToClipboard';
import moment from "moment";
const QRCode = require('qrcode.react');
const auditState = ['审核中', '审核拒绝', '审核通过'];
const tabList = [
  {

    key: '0',
    tab: '全部商品',
    prop: 'allCounts',
    filterKey: '',
    filterStatus: ''
  },
  {
    key: '1',
    tab: '审核通过',
    prop: 'putOnCounts',
    filterKey: 'type',
    filterStatus: '1'
  },
  {
    key: '2',
    tab: '审核拒绝',
    prop: 'notPutOnCounts',
    filterKey: 'type',
    filterStatus: '2'
  },
  {
    auditState: 3,
    key: '3',
    prop: 'pendingCounts',
    tab: '待审核',
    filterKey: 'auditState',
    filterStatus: '0'
  },
  {
    auditState: 4,
    prop: 'notPassCounts',
    key: '4',
    tab: '未通过',
    filterKey: 'auditState',
    filterStatus: '1'
  },
  // {
  //   key: '0',
  //   tab: '回收站',
  // },
];


@connect(({ goodsList, loading }) => ({
  ...goodsList,
  loading: loading.models.goodsList,
}))
class List extends PureComponent {
  state = {
    tableData: [],
    AuditDetail: [],
    loading: true,
    tabType: '0',
    ewmurl: "",
    showwem: false,
    auditVisible: false,
    current: 1,
    pageNumber: 1,
    pageSize: 10,
    total: 0,
    counts: {},
    categories: [],
    modalTableCurrent: 1,
    modalPageSize: 10,
    modalTotal: 0,
    filter: {},

  };

  columns = [
    {
      title: '商品编号',
      dataIndex: 'productId',
      width: 120,
      align: 'center',
      render: text => <>
        {text.indexOf("TF") !== -1 && <div>转单</div>}
        <CopyToClipboard text={text} />
      </>,
      // render: text => <CopyToClipboard text={text} />,
    },
    {
      title: '商品图片',
      dataIndex: 'src',
      render: text => <img src={text} alt="" style={{ width: 60, height: 60 }} />,
    },
    {
      title: '商品名称',
      dataIndex: 'name',
    },
    {
      title: '商品类目',
      dataIndex: 'opeCategoryStr',
      width: 160,
    },
    {
      title: '状态',
      dataIndex: 'status',
      align: 'center',
      width: 90,
      key: 'status',
      render: text => {
        const success = text === 1
        const status = success ? 'success' : 'default'
        const badgeText = success ? '已上架' : '已下架'
        return (<Badge status={status} text={badgeText} />)
      },
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      width: 120
    },
    {
      title: '审核状态',
      dataIndex: 'auditState',
      key: 'auditState',
      width: 120,
      render: (text, record) => {
        const _text = auditState[text]
        return (
          <div>
            {_text}
          </div>
        );
      },
    },
    {
      title: '操作',
      dataIndex: 'action',
      width: 160,
      render: (_, record) => (
        <div>
          <a onClick={() => this.gotoDetail(record.id)}>
            修改
          </a>
          <Divider type="vertical" />
          <a onClick={() => this.handleCopy(record)}>复制地址</a>
          <Divider type="vertical" />
          <a onClick={() => this.createewm(record)}>二维码</a>
          <Divider type="vertical" />
          <Divider type="vertical" />
          <a onClick={() => this.handleCopy(record,'h5')}>h5地址</a>
          <Divider type="vertical" />
          <a onClick={() => this.createewm(record,'h5')}>h5二维码</a>
          <Divider type="vertical" />

          <Popconfirm title="是否删除该商品？" onConfirm={() => this.handleDelete(record)}>
            <a>删除</a>
          </Popconfirm>
        </div>
      ),
    },
  ];

  auditColumns = [
    {
      title: '审核时间',
      dataIndex: 'createTime',
      key: 'createTime',
      align: 'center'
    },
    {
      title: '审核人员',
      dataIndex: 'operator',
      width: 120,
    },
    {
      title: '审核结果',
      dataIndex: 'auditStatus',
      width: 120,
      render: text => {
        const _text = auditState[text]
        if (text === 1) {
          return (
            <span className="red">
              <Badge status="error" />
              未通过
            </span>
          )
        }
        return _text
      },
    },
    {
      title: '反馈详情',
      dataIndex: 'feedBack',
    },
  ]

  // search = {}

  componentDidMount() {
    console.log(this.search.props.handleFilter());
    this.getTabCount()
    this.findCategories()
  }


  findCategories = () => {
    goodsService.findCategories().then(res => {
      this.setState({
        categories: res || []
      });
    })
  }

  handleDelete = record => {
    goodsService.busUpdateProductByRecycleDel({
      id: record.id
    }).then(res => {
      message.success('删除成功')
      this.handleFilter()
    })
  };

  createewm = (record, qd = "alipay") => {
    let url = this.urlcreate(record.productId,qd)

    this.setState({
      ewmurl: url,
      showwem: true
    });
  }

  handleCopy = (record, qd = "alipay") => {
    let url = this.urlcreate(record.productId,qd)
    let transfer = document.createElement('input');
    document.body.appendChild(transfer);
    transfer.value = url;  // 这里表示想要复制的内容
    transfer.focus();
    transfer.select();
    if (document.execCommand('copy')) { //判断是否支持document.execCommand('copy')       document.execCommand('copy');
    }
    transfer.blur();
    message.success('复制成功');
    document.body.removeChild(transfer);
    console.log("复制的参数", url)
  };

  urlcreate = (id, qd = "alipay") => {
    let t = new Date().getTime() + 3600 * 1 * 1000
    if (qd == "h5") {
      var host = window.location.host;
      url = "https://" + host + "/#/src/pages/product/product?productId=" + id
      url+="&t="+ t
      return url
    }
    let appid = localStorage.getItem("appid")
    if (!appid) appid = "2021004108695551"
    //alipays://platformapi/startapp?appId=1231111&page=pages/iii&query=pid%3D1
    let url = "alipays://platformapi/startapp?appId=" + appid + "&page=src%2Fpages%2Fproduct%2Fproduct%3FproductId%3D"
    url += id
    //url +=  "%26c%3Dam"
    url += "%26t%3D"
    url += t
    //当时时间戳+2小时
    return "https://ds.alipay.com/?scheme=" + encodeURIComponent(url)
  };

  gotoDetail = id => {
    const { dispatch } = this.props;
    dispatch(
      routerRedux.push({
        pathname: `/goods/list/detail/${id}`,
      })
    );
  };

  handleUpDown = record => {
    const { dispatch, queryInfo } = this.props;
    dispatch({
      type: 'goodsList/busUpdateProductByUpOrDown',
      payload: record,
      callback: () => {
        this.setDispatch(queryInfo);
      },
    });
  };

  tabChange = (type) => {
    console.log(type);
    this.setState({
      tabType: type
    }, () => {
      this.handleFilter()
    });
  }

  handleAuditModal = (record) => {
    this.selectExamineProductAuditLog(record)
    this.setState({
      auditVisible: true
    });
  }

  close = () => {
    this.setState({
      auditVisible: false,
      showwem: false
    });
  }

  selectExamineProductAuditLog = (record) => {
    const { modalTableCurrent, modalPageSize } = this.state
    goodsService.selectExamineProductAuditLog({
      id: record.id,
      itemId: record.productId,
      pageNumber: modalTableCurrent,
      pageSize: modalPageSize
    }).then(res => {
      this.setState({
        AuditDetail: res.records || [],
        modalTotal: res.total,
        modalTableCurrent: res.current,
      });
    })
  }

  getTabCount = () => {
    goodsService.selectBusinessPrdouctCounts().then(res => {
      this.setState({
        counts: res
      });
    })
  }

  initData = () => {
    const { pageNumber, pageSize, filter, tabType } = this.state
    const item = tabList[tabType]
    if (item.filterKey) {
      filter[item.filterKey] = item.filterStatus
    }
    let payload = {
      pageNumber,
      pageSize,
      type: '1', // 默认已上架
      isDelete: false,
      ...filter,
    };
    this.setState(
      {
        loading: true,
      },
      () => {
        goodsService.selectBusinessPrdouct({
          ...payload
        }).then(res => {
          this.setState({
            tableData: res.records,
            total: res.total,
            loading: false,
            current: res.current,
          });
        })
      },
    );
  };

  handleFilter = (data = {}) => {
    const { type } = data
    if (data.queryType && data.queryType === '分页') {
      this.initData();
    } else {
      this.setState(
        {
          pageNumber: 1,
          pageSize: 10,
          filter: data
        },
        () => {
          this.initData();
        },
      );
    }
  };

  //分页，下一页
  onChange = pageNumber => {
    this.setState(
      {
        pageNumber: pageNumber,
      },
      () => {
        this.handleFilter({ queryType: '分页' });
      },
    );
  };

  showTotal = () => {
    return `共有${this.state.total}条`;
  };

  //切换每页数量
  onShowSizeChange = (current, pageSize) => {
    this.setState({ pageSize: pageSize }, () => {
      this.handleFilter({ queryType: '分页' });
    });
  };

  handleExportData = (filter) => {
    if (filter.createDate && filter.createDate.length > 1) {
      filter.createTimeStart = moment(filter.createDate[0]).format('YYYY-MM-DD HH:mm:ss');
      filter.createTimeEnd = moment(filter.createDate[1]).format('YYYY-MM-DD HH:mm:ss');
    }
    goodsService.exportBusinessPrdouct(filter).then(res => {
      // downloadFile(res)
      location.href = res
    })
  }

  render() {
    const {
      loading, current, total, categories = [], tabType, auditVisible, AuditDetail, tableData, counts,
      modalPageSize, modalTotal, modalTableCurrent, showwem, ewmurl
    } = this.state;
    const options = [{ field: 'categoryIds', options: categories }]


    return (
      <PageHeaderWrapper title={false} className="nav-tab">
        {/*<Tabs activeKey={tabType} onChange={this.tabChange}>*/}
        {/*   {tabList.map(tab => (*/}
        {/*     <TabPane tab={`${tab.tab}(${counts[tab.prop]})`} key={tab.key} />*/}
        {/*   ))}*/}
        {/* </Tabs>*/}
        <Spin spinning={loading}>
          <Card bordered={false}>
            <Search needExport needReset source={'商品列表'} exportData={this.handleExportData}
              options={options}
              ref={ref => this.search = ref}
              handleFilter={this.handleFilter} />
          </Card>
          <Card bordered={false}>
            <div className="ds-inline-block" onClick={() => this.gotoDetail('add')}>
              <Button type="primary">新增商品</Button>
            </div>
            <Table
              rowKey="id"
              columns={this.columns}
              dataSource={tableData}
              pagination={{
                current: current,
                total: total,
                onChange: this.onChange,
                showTotal: this.showTotal,
                showQuickJumper: true,
                pageSizeOptions: ['5', '10', '20'],
                showSizeChanger: true,
                onShowSizeChange: this.onShowSizeChange,
              }}
            />
          </Card>
        </Spin>
        <Modal
          title="审核详情"
          visible={auditVisible}
          footer={null}
          onCancel={this.close}
          width={900}
        >
          <Card bordered={false}>
            <Table
              rowKey="id"
              columns={this.auditColumns}
              dataSource={AuditDetail}
              pagination={{
                current: modalTableCurrent,
                total: modalTotal,
                onChange: this.onChange,
                showTotal: this.showTotal,
                showQuickJumper: true,
                pageSizeOptions: ['5', '10', '20'],
                showSizeChanger: true,
                onShowSizeChange: this.onShowSizeChange,
              }}
            />
          </Card>
        </Modal>
        <Modal
          title="商品二维码"
          visible={showwem}
          footer={null}
          onCancel={this.close}
          width={300}
        >
          <QRCode value={ewmurl} size={256} id={'qrCode'} />
        </Modal>
      </PageHeaderWrapper>
    );
  }
}

export default List;
