import React, { PureComponent } from 'react';
import {Input, Tooltip, Tag, Icon, message, Button} from 'antd';

class OtherSpec extends PureComponent {
  state = {
    inputVisible: false,
    inputValue: '',
    param: ''
  };

  render() {
    const { inputVisible, inputValue } = this.state;
    const { value, onChange, parameters = [] } = this.props;

    const saveInputRef = input => {
      this.input = input;
    };

    const showInput = () => {
      this.setState({ inputVisible: true }, () => this.input.focus());
    };

    const handleInputChange = e => {
      this.setState({ inputValue: e.target.value });
    };

    const handleInputAdd = () => {
      if (!inputValue) {
        this.setState({ inputVisible: false, inputValue: '' });
        return;
      }
      const index = value.indexOf(inputValue);
      if (index === -1) {
        const newValue = [...value];
        newValue.push(inputValue);
        onChange(newValue);
        this.setState({ inputVisible: false, inputValue: '' });
      } else {
        message.info('已存在相同的规格');
      }
    };
  
    const handlePropsInputChange = (evt) => {
      this.setState({
        param: evt.target.value
      });
    }
  
    const handleChangeParamItemValue = (evt, index) => {
      const value = evt.target.value
      const newParam = [...parameters];
      newParam[index].value = value
      onChange(newParam);
    }

    const closeParam = (evt, index) => {
      const newParam = [...parameters];
      newParam.splice(index, 1);
      onChange(newParam);
    };
  
    const submitProps = (val) => {
      const { param } = this.state
      const newParam = [...parameters];
      newParam.push({name: param, value: ''});
      onChange(newParam)
      cancelProps()
    }
    const cancelProps = () => {
      this.setState({
        param: ''
      });
    }

    return (
      <div>
        <div className="param-header df">
          <Input placeholder="请输入商品参数" value={this.state.param}
                 className="w294" onChange={handlePropsInputChange}/>
          <a onClick={cancelProps} className="grey-65 ml-20">取消</a>
  
          <a onClick={submitProps} className="primary-color ml-20">确定</a>
        </div>
        <div className="param-content">
          {
            parameters.map((parameter,index)=>{
              return (
                <div className="param-content-item">
                  {parameter.name}:
                  <div className="param-content-item-action">
                    <Input placeholder="请输入参数"
                           onChange={evt=>handleChangeParamItemValue(evt, index)}
                           className="w-112 mr-5" value={parameter.value}/>
                    <Icon type="close-circle"
                          onClick={(evt)=> closeParam(evt, index)} />
                  </div>
                </div>
              )
            })
          }
        </div>
        {/*{value.map((tag, index) => {*/}
        {/*  const isLongTag = tag.length > 20;*/}
        {/*  const tagElem = (*/}
        {/*    <Tag color="magenta" key={tag} closable onClose={() => handleTagClose(index)}>*/}
        {/*      {isLongTag ? `${tag.slice(0, 20)}...` : tag}*/}
        {/*    </Tag>*/}
        {/*  );*/}
        {/*  return isLongTag ? (*/}
        {/*    <Tooltip title={tag} key={tag.id}>*/}
        {/*      {tagElem}*/}
        {/*    </Tooltip>*/}
        {/*  ) : (*/}
        {/*    tagElem*/}
        {/*  );*/}
        {/*})}*/}
        {/*{inputVisible && (*/}
        {/*  <Input*/}
        {/*    ref={saveInputRef}*/}
        {/*    type="text"*/}
        {/*    size="small"*/}
        {/*    style={{ width: 78 }}*/}
        {/*    value={inputValue}*/}
        {/*    onChange={handleInputChange}*/}
        {/*    onBlur={handleInputAdd}*/}
        {/*    onPressEnter={handleInputAdd}*/}
        {/*  />*/}
        {/*)}*/}
        {/*{!inputVisible && (*/}
        {/*  <Tag*/}
        {/*    color="volcano"*/}
        {/*    onClick={() => showInput()}*/}
        {/*    style={{ background: '#fff', borderStyle: 'dashed' }}*/}
        {/*  >*/}
        {/*    <Icon type="plus" /> 新增*/}
        {/*  </Tag>*/}
        {/*)}*/}
      </div>
    );
  }
}

export default OtherSpec;
