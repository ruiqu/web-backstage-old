import React, { PureComponent } from 'react';
import {
  Table,
  Select,
  InputNumber,
  Icon,
  Upload,
  Button,
  message,
  Form,
  Input,
  Tooltip,
  Radio,
  Card,
} from 'antd';

import { LZFormItem } from '@/components/LZForm';
function messageAlert(params) {
  message.destroy();
  message.warning(params);
}

const largeFormItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 4 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 20 },
  },
};

@Form.create()
class PurchaseNewProductSku extends PureComponent {
  state = {
    uploading: false,
    marketPrice: 0,
    freezePrice: 0,
    wkPrice: 0,  //尾款
    xjPrice: 0,  //现价
    salePrice: '',
  };

  componentDidMount() {
    const { productSkuses } = this.props;
    const { marketPrice,freezePrice,wkPrice,xjPrice } = productSkuses;
    this.setState({
      marketPrice,freezePrice,wkPrice,xjPrice
    });
  }

  changeSkuField = (evt, field) => {
    const value = evt.target.value;
    const { productSkuses, changeSku, index } = this.props;
    let _productSkuses = { ...productSkuses };

    this.setState({
      marketPrice: value,
    });

    _productSkuses[field] = value;
    changeSku(_productSkuses, index);
  };

  render() {
    const {
      index,
      form: { getFieldDecorator },
      productSkuses = {},
      specs = [],
      deleteSku,
    } = this.props;
    const { inventory, marketPrice, specAll, salePrice, isHandlingFee,freezePrice ,wkPrice,xjPrice} = productSkuses;
    const close = (colorTitle, specTitle) => {
      deleteSku(index, colorTitle, specTitle);
    };

    const label = (
      <label className="">
        分期手续费
        <Tooltip
          title={(
            <table>
              <tr>
                <th>商户承担</th>
                <th></th>
              </tr>
              <tr>
                <th>期数</th>
                <th>费率</th>
              </tr>
              <tr>
                <td>3</td>
                <td>1.8%</td>
              </tr>
              <tr>
                <td>6</td>
                <td>4.5%</td>
              </tr>
              <tr>
                <td>12</td>
                <td>7.5%</td>
              </tr>
              <tr>
                <th>用户承担</th>
                <th></th>
              </tr>
              <tr>
                <th>期数</th>
                <th>费率</th>
              </tr>
              <tr>
                <td>3</td>
                <td>2.3%</td>
              </tr>
              <tr>
                <td>6</td>
                <td>4.5%</td>
              </tr>
              <tr>
                <td>12</td>
                <td>7.5%</td>
              </tr>
            </table>
          )}
        >
          <Icon type="info-circle" />
        </Tooltip>
      </label>
    );

    let title = '';
    let colorTitle = '',
      specTitle = '';
    if (specs.values.length) {
      colorTitle = specAll[0].platformSpecValue || '';
      specTitle = specAll[1].platformSpecValue || '';
    }
    title = colorTitle ? colorTitle + '/' + specTitle : specTitle;
    const { isBuyout } = this.props;
    return (
      <Card
        title={title}
        style={{ marginBottom: '10px' }}
        extra={<Icon type="close" onClick={evt => close(colorTitle, specTitle)} />}
      >
        <Form>
          <LZFormItem
            formItemLayout={largeFormItemLayout}
            label="官方售价-原价"
            field="marketPrice"
            requiredMessage="请输入官方售价"
            getFieldDecorator={getFieldDecorator}
            onChange={evt => this.changeSkuField(evt, 'marketPrice')}
            initialValue={marketPrice}
          >
            <Input placeholder="元" />
          </LZFormItem>
          <LZFormItem
            formItemLayout={largeFormItemLayout}
            label="预付款"
            field="freezePrice"
            getFieldDecorator={getFieldDecorator}
            onChange={evt => this.changeSkuField(evt, 'freezePrice')}
            initialValue={freezePrice}
          >
            <Input placeholder="元" />
          </LZFormItem>

          <LZFormItem
            formItemLayout={largeFormItemLayout}
            label="尾款"
            field="wkPrice"
            getFieldDecorator={getFieldDecorator}
            onChange={evt => this.changeSkuField(evt, 'wkPrice')}
            initialValue={wkPrice}
          >
            <Input placeholder="元" />
          </LZFormItem>
          <LZFormItem
            formItemLayout={largeFormItemLayout}
            label="现价"
            field="xjPrice"
            getFieldDecorator={getFieldDecorator}
            onChange={evt => this.changeSkuField(evt, 'xjPrice')}
            initialValue={xjPrice}
          >
            <Input placeholder="元" />
          </LZFormItem>
          <LZFormItem
            formItemLayout={largeFormItemLayout}
            label="购买售价"
            field="salePrice"
            requiredMessage="请输入购买售价"
            getFieldDecorator={getFieldDecorator}
            onChange={evt => this.changeSkuField(evt, 'salePrice')}
            initialValue={salePrice}
          >
            <Input placeholder="元" />
          </LZFormItem>
          <LZFormItem
            formItemLayout={largeFormItemLayout}
            label="库存数量"
            field="inventory"
            requiredMessage="请输入库存数量"
            getFieldDecorator={getFieldDecorator}
            onChange={evt => this.changeSkuField(evt, 'inventory')}
            initialValue={inventory}
          >
            <Input placeholder="数量" />
          </LZFormItem>
          {isBuyout === 0 ? null : (
            <LZFormItem
              formItemLayout={largeFormItemLayout}
              label={label}
              field="isHandlingFee"
              getFieldDecorator={getFieldDecorator}
              initialValue={isHandlingFee || 0}
              // rules={[{ validator: checkMaxRent }]}
            >
              <Radio.Group onChange={evt => this.changeSkuField(evt, 'isHandlingFee')}>
                <Radio value={0}>用户承担</Radio>
                <Radio value={1}>商户承担</Radio>
              </Radio.Group>
            </LZFormItem>
          )}
        </Form>
      </Card>
    );
  }
}
export default PurchaseNewProductSku;
