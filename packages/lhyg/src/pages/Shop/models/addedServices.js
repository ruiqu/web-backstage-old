import { message } from 'antd';
import * as servicesApi from '../services';

export default {
  namespace: 'addedServices',
  state: {
    queryInfo: {
      pageNumber: 1,
      pageSize: 10,
      approvalStatus: null,
      name: null,
    },
    total: 0,
    list: [{}],
    current: null,
    products: {
      queryInfo: {
        pageNumber: 1,
        pageSize: 10,
      },
      total: 0,
      list: [],
    },
    goodsList: {
      queryInfo: {
        id: null,
        productId: null,
        pageNumber: 1,
        pageSize: 10,
      },
      total: 0,
      list: [],
    },
  },

  effects: {
    // 获取增值服务列表
    *selectShopAdditionalServicesList({ payload }, { call, put }) {
      const res = yield call(servicesApi.selectShopAdditionalServicesList, payload);
      if (res) {
        yield put({
          type: 'saveList',
          payload: { data: res.data, queryInfo: payload },
        });
      }
    },

    // 新增修改增值服务
    *insertShopAdditionServices({ payload, callback }, { call }) {
      const res = yield call(servicesApi.insertShopAdditionServices, payload);
      if (res) {
        message.success('保存成功');
        if (callback) {
          callback(res.data);
        }
      }
    },

    // 删除增值服务
    *deletShopAdditionService({ payload, callback }, { call }) {
      const res = yield call(servicesApi.deletShopAdditionService, payload);
      if (res) {
        message.success('删除成功');
        if (callback) {
          callback();
        }
      }
    },

    // 查询增值服务商品
    *selectShopAdditionalServicesProudctList({ payload }, { call, put }) {
      const res = yield call(servicesApi.selectShopAdditionalServicesProudctList, payload);
      if (res) {
        yield put({
          type: 'saveProductList',
          payload: { data: res.data, queryInfo: payload },
        });
      }
    },

    // 新增增值服务商品
    *insertShopAdditionServicesProduct({ payload, callback }, { call }) {
      const res = yield call(servicesApi.insertShopAdditionServicesProduct, payload);
      if (res) {
        message.success('新增成功');
        if (callback) {
          callback();
        }
      }
    },

    *deletShopAdditionServicesProduct({ payload, callback }, { call }) {
      const res = yield call(servicesApi.deletShopAdditionServicesProduct, payload);
      if (res) {
        message.success('删除成功');
        if (callback) {
          callback();
        }
      }
    },

    *checkedAdditionalServicesProductList({ payload }, { call, put }) {
      const res = yield call(servicesApi.checkedAdditionalServicesProductList, payload);
      if (res) {
        yield put({
          type: 'saveGoodsList',
          payload: { data: res.data, queryInfo: payload },
        });
      }
    },
  },

  reducers: {
    saveList(state, { payload }) {
      return {
        ...state,
        list: payload.data.records,
        total: payload.data.total,
        queryInfo: {
          ...payload.queryInfo,
        },
      };
    },

    saveProductList(state, { payload }) {
      return {
        ...state,
        products: {
          ...state.products,
          list: payload.data.records,
          total: payload.data.total,
          queryInfo: {
            ...payload.queryInfo,
          },
        },
      };
    },

    saveGoodsList(state, { payload }) {
      return {
        ...state,
        goodsList: {
          ...state.goodsList,
          list: payload.data.records,
          total: payload.data.total,
          queryInfo: {
            ...payload.queryInfo,
          },
        },
      };
    },

    setCurrent(state, { payload }) {
      return { ...state, current: payload };
    },

    clearProductList(state) {
      return { ...state, products: { ...state.products, list: [] } };
    },
  },
};
