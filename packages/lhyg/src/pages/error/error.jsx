import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import MyPageTable from '@/components/MyPageTable';
import {
  Card,
  Form,
  DatePicker,
  Input,
  Button,
  Row,
  Col,
  Select,
  message,
  Tooltip,
  Switch,
} from 'antd';
import request from '@/services/baseService';
import moment from 'moment';
import styles from './index.less';

const { Option } = Select;
const { RangePicker } = DatePicker;
@Form.create()
class errorList extends Component {
  state = {
    search: {
      pageNumber: 1,
      pageSize: 10,
      errStkClass: '',
      errStkMethod: '',
      errClass: '',
      createTimeStart: '',
      createTimeEnd: '',
      updateTimeStart: '',
      updateTimeEnd: '',
      status: '',
    },
    tableList: [],
    current: 1,
    total: 0,
    pages: 1,
    modal: {
      type: 'add',
      title: '',
    },
    queryLoad: false,
    modalLoad: false,
  };
  componentDidMount() {
    let { createTimeEnd } = this.getDay(0);
    let { createTimeStart } = this.getDay(-1);

    this.state.search.createTimeStart = createTimeStart;
    this.state.search.createTimeEnd = createTimeEnd;
    this.getList();
  }

  getDay = key => {
    let db = {};
    var now = new Date();
    var date = now.getDate();
    //now.setDate(date + 1);  //获取后一天
    now.setDate(date + key); //获取前一天
    var y = now.getFullYear();
    var m = (now.getMonth() + 1).toString().padStart(2, '0');
    var d = now
      .getDate()
      .toString()
      .padStart(2, '0');
    var ymd = y + '-' + m + '-' + d;
    let createTimeEnd = ymd + ' 23:59:59';
    let createTimeStart = ymd + ' 00:00:00';
    return {
      createTimeEnd,
      createTimeStart,
    };
  };

  getList = () => {
    const { search } = this.state;

    let obj = Object.fromEntries(
      Object.entries(search).filter(([key, value]) => {
        // 过滤空值
        return value !== '' && value !== null && value !== undefined;
      }),
    );

    request(`/hzsx/errorStatics/selectByErrInfo`, obj, 'post').then(res => {
      console.log(123546, res);
      this.setState({
        tableList: res.records,
        queryLoad: false,
        pages: res.pages,
        total: res.total,
        current: res.current,
      });
    });
  };

  handleStatus = record => {
    request(
      `/hzsx/errorStatics/markDealed`,
      {
        errStk: record.errStk,
        period: record.period,
      },
      'post',
    ).then(res => {
      message.success('修改成功');
    });
  };

  columns = [
    {
      title: 'No.',
      dataIndex: 'id',
      rowScope: 'row',
      width: 60,
      render: (record, index, indent, expanded) => {
        return indent + 1;
      },
    },
    {
      title: '错误栈顶详情',
      dataIndex: 'errStk',
      width: 160,
    },
    {
      title: '错误栈顶类',
      dataIndex: 'errStkClass',
      width: 160,
    },
    {
      title: '错误栈顶方法',
      dataIndex: 'errStkMethod',
      width: 160,
    },
    {
      title: '错误类型',
      dataIndex: 'errClass',
      width: 200,
    },
    {
      title: '归属地',
      dataIndex: 'ipAttribution',
      width: 160,
    },
    {
      title: '时间周期',
      dataIndex: 'period',
      width: 160,
    },
    {
      title: '技术',
      dataIndex: 'count',
      width: 80,
    },
    {
      title: '错误描述',
      dataIndex: 'message',
      width: 160,
      onCell: () => {
        return {
          style: {
            maxWidth: 100,
            overflow: 'hidden',
            whiteSpace: 'nowrap',
            textOverflow: 'ellipsis',
            cursor: 'pointer',
          },
        };
      },
      render: (e, record) => {
        return <Tooltip placement="topLeft" title={record.message} />;
      },
    },

    {
      title: '状态',
      dataIndex: 'status',
      width: 160,
      render: (e, record) => {
        return (
          <Switch
            checkedChildren="已处理"
            unCheckedChildren="未处理"
            onClick={() => this.handleStatus(record)}
            defaultChecked={record.status !== 1}
            disabled={record.status !== 1}
          />
        );
        // if (record.status == 1) {
        //   return '未处理';
        // }
        // return '已处理';
      },
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      width: 160,
    },
    {
      title: '更新时间',
      dataIndex: 'updateTime',
      width: 160,
    },
    // {
    //   title: 'ip地址',
    //   dataIndex: 'ip',
    //   width: 160,
    // },
  ];

  handSearch = () => {
    this.props.form.validateFields(
      ['errStkClass', 'errStkMethod', 'errClass', 'status', 'createTime', 'updateTime'],
      (err, values) => {
        if (!err) {
          let createTimeStart = '';
          let createTimeEnd = '';
          if (values.createTime) {
            createTimeStart = moment(values.createTime[0]).format('YYYY-MM-DD HH:mm:ss');
            createTimeEnd = moment(values.createTime[1]).format('YYYY-MM-DD HH:mm:ss');
          }

          let updateTimeStart = '';
          let updateTimeEnd = '';

          if (values.updateTime) {
            updateTimeStart = moment(values.updateTime[0]).format('YYYY-MM-DD HH:mm:ss');
            updateTimeEnd = moment(values.updateTime[1]).format('YYYY-MM-DD HH:mm:ss');
          }

          this.setState(
            {
              queryLoad: true,
              search: {
                pageNumber: 1,
                pageSize: 10,
                errStkClass: !!values.errStkClass ? values.errStkClass.trim() : '',
                errStkMethod: !!values.errStkMethod ? values.errStkMethod.trim() : '',
                errClass: !!values.errClass ? values.errClass.trim() : '',
                status: values.status,
                createTimeStart,
                createTimeEnd,
                updateTimeStart,
                updateTimeEnd,
              },
            },
            () => this.getList(),
          );
        }
      },
    );
  };
  onPage = e => {
    // pageNumber: 1,
    // pageSize: 3,
    let data = Object.assign(this.state.search, { pageNumber: e.current, pageSize: e.pageSize });

    this.setState(
      {
        search: data,
      },
      () => this.getList(),
    );
  };

  render() {
    const { current, pages, total, search, queryLoad } = this.state;
    const { getFieldDecorator } = this.props.form;
    console.log('条数', total);
    const paginationProps = {
      current: current,
      pageSize: search.pageSize,
      total: total,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / search.pageSize)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };

    return (
      <PageHeaderWrapper title={false} className="nav-tab">
        <Card bordered={false} className="mt-20">
          <Form layout="inline" className="mb-20">
            <Row>
              <Col span={8}>
                <Form.Item label={<span className={styles.formLabel}>错误栈顶类</span>}>
                  {getFieldDecorator('errStkClass', {
                    rules: [],
                  })(<Input placeholder="请输入错误栈顶类" />)}
                </Form.Item>
              </Col>

              <Col span={8}>
                <Form.Item label={<span className={styles.formLabel}>错误栈顶方法</span>}>
                  {getFieldDecorator('errStkMethod', {
                    rules: [],
                  })(<Input placeholder="请输入错误栈顶方法" />)}
                </Form.Item>
              </Col>

              <Col span={8}>
                <Form.Item label={<span className={styles.formLabel}>状态</span>}>
                  {getFieldDecorator(
                    'status',
                    {},
                  )(
                    <Select placeholder="请选择" allowClear style={{ width: 202 }}>
                      <Option value={0}>已处理</Option>
                      <Option value={1}>未处理</Option>
                    </Select>,
                  )}
                </Form.Item>
              </Col>

              <Col span={8}>
                <Form.Item label={<span className={styles.formLabel}>错误类型</span>}>
                  {getFieldDecorator('errClass', {
                    rules: [],
                  })(<Input placeholder="请输入错误类型" />)}
                </Form.Item>
              </Col>

              <Col span={8}>
                <Form.Item label={<span className={styles.formLabel}>创建时间</span>}>
                  {getFieldDecorator('createTime', {
                    initialValue: [
                      moment(this.state.search.createTimeStart, 'YYYY-MM-DD HH:mm:ss'),
                      moment(this.state.search.createTimeEnd, 'YYYY-MM-DD HH:mm:ss'),
                    ],
                  })(
                    <RangePicker
                      showTime={{
                        defaultValue: [
                          moment('00:00:00', 'HH:mm:ss'),
                          moment('23:59:59', 'HH:mm:ss'),
                        ],
                      }}
                      format="YYYY-MM-DD HH:mm:ss"
                    />,
                  )}
                </Form.Item>
              </Col>

              <Col span={8}>
                <Form.Item label={<span className={styles.formLabel}>更新时间</span>}>
                  {getFieldDecorator(
                    'updateTime',
                    {},
                  )(
                    <RangePicker
                      showTime={{
                        defaultValue: [
                          moment('00:00:00', 'HH:mm:ss'),
                          moment('23:59:59', 'HH:mm:ss'),
                        ],
                      }}
                      format="YYYY-MM-DD HH:mm:ss"
                    />,
                  )}
                </Form.Item>
              </Col>
            </Row>

            <Form.Item>
              <Button loading={queryLoad} onClick={() => this.handSearch()} type="primary">
                查询
              </Button>
            </Form.Item>
          </Form>

          {/* </Card>
        <Card bordered={false}> */}
          <MyPageTable
            scroll={true}
            onPage={this.onPage}
            style={{ marginTop: '20px' }}
            paginationProps={paginationProps}
            dataSource={this.state.tableList}
            columns={this.columns}
            rowKey="id"
          />
        </Card>
      </PageHeaderWrapper>
    );
  }
}

export default errorList;
