import React from "react"
import { connect } from "dva"
import { PageHeaderWrapper } from "@ant-design/pro-layout"
import { Card, Tabs, Table } from "antd"
import request from "@/services/baseService"
import router from "umi/router"
import { onTableData } from "@/utils/utils"

const { TabPane } = Tabs
@connect(({ problemListModel }) => ({
  ...problemListModel
}))
class ProblemList extends React.PureComponent {
  state = {
    categoryList: [], // 分类列表数据
    pageLoading: false, // 页面加载中
  }

  componentDidMount() {
    this.fetchDataHandler()
  }

  // 加载分类以及常见问题列表数据
  fetchDataHandler = () => {
    this.setState({ pageLoading: true })
    request("/hzsx/noticeCenter/queryOpeNoticeTabList").then(res => {
      const firstId = res && res[0]?.id
      if (!this.props.activeTab) {
        this.props.dispatch({ type: "problemListModel/mutActiveTab", payload: firstId })
      }
      this.setState({ categoryList: res })
    }).finally(() => {
      this.setState({ pageLoading: false })
    })
  }

  changeTabHandler = v => {
    this.props.dispatch({ type: "problemListModel/mutActiveTab", payload: v })
  }

  // 找出问题集合
  returnQuestionList = () => {
    const arr = this.state.categoryList
    if (Object.prototype.toString.call(arr) !== "[object Array]") return []
    const matchObj = arr.find(obj => obj.id == this.props.activeTab) || {}
    return matchObj.itemDtos || []
  }

  // 查看文章详情
  seeDetail = obj => {
    const { id } = obj
    const url = `/serviceCenter/problemDetail/${id}`
    router.push(url)
  }

  columns = [
    { title: "ID", dataIndex: "id" },
    { title: "标题", dataIndex: "name" },
    { title: "创建时间", dataIndex: "createTime" },
    {
      title: "操作",
      render: (_, row) => {
        return <a onClick={() => this.seeDetail(row)}>查看详情</a>
      }
    }
  ]

  render() {
    const questionList = onTableData(this.returnQuestionList())

    return (
      <PageHeaderWrapper  title={false}>
        <Card loading={this.state.pageLoading} bordered={false}>
          <Tabs onChange={this.changeTabHandler} activeKey={`${this.props.activeTab}`}>
            {
              this.state.categoryList.map(obj => (
                <TabPane tab={obj.name} key={obj.id}>
                  <Table
                    columns={this.columns}
                    dataSource={questionList}
                    pagination={false}
                  />
                </TabPane>
              ))
            }
          </Tabs>
        </Card>
      </PageHeaderWrapper>
    )
  }
}

export default ProblemList