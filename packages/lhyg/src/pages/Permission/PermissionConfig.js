import React, { PureComponent } from 'react'
import {
  Card, Icon,
  Button,
  Radio,
  Form, Checkbox, message, Spin, Modal
} from 'antd'
import { connect } from 'dva'
import {PageHeaderWrapper} from "@ant-design/pro-layout";
import departmentService from "@/services/department";
import {router} from "umi";
import {cloneDeep, isEqual} from 'lodash'
import RouterWillLeave from "@/components/RouterWillLeave";


const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 2 },
  },
  wrapperCol: {
    xs: { span: 22 },
    sm: { span: 22 },
  },
}

@connect()
@Form.create()
class PermissionConfig extends PureComponent {
  state = {
    id: '',
    authList: [],
    info: {},
    type: 'all',
    cs:0,
    secondList: [],
    functionList: [],
    firstIndex: 0,
    secondIndex: 0,
    userType: 'user',
    originValues: [],
    isSubmit: false
  }
  
  componentDidMount () {
    const {match: {params: { id }}} = this.props;
    const data = id.split('-')
    const userType = data[0]
    const _id = data[1]
    this.getDetail(userType, _id)
    this.queryOne(userType, _id)
    this.setState({
      userType
    });
  }
  
  getDetail = (type, id) => {
    const isDepartment = type === 'department'
    const service = isDepartment ? departmentService.authPage : departmentService.userAuthPage
    let payload = {}
    if (isDepartment) {
      payload = {departmentId: id}
    } else {
      payload = {id}
    }
    service(payload).then(res => {
      const secondList = res && res[0] && res[0].child || []
      const functionList = secondList && secondList[0] && secondList[0].child || []
      this.setState({
        authList: res,
        originValues: res,
        secondList,
        functionList,
        id
      }, ()=>{
        this.handleIsAllSelect(res)
      });
    }).finally(() => {
      this.setState({
        loading: false
      });
    })
  }
  
  queryOne = (userType, id) => {
    const isDepartment = userType === 'department'
    const service = isDepartment ? departmentService.queryOne : departmentService.queryBackstageUserDetail
    service({
      id
    }).then(res => {
      this.setState({
        info: res
      });
    })
  }
  
  onChange = (evt) => {
    this.setState({
      type: evt.target.value
    });
  }
  onChange = (evt) => {
    this.setState({
      type: evt.target.value
    });
  }
  onChange1 = (evt) => {
     console.log(evt.target.value,"1111")
    this.setState({
      cs: evt.target.value
    });
  }
  
  handleIsAllSelect = (authList) => {
    authList.forEach(value => {
      if (!value.shosen) {
        this.setState({
          type: 'part'
        });
      }
      if (value.child) {
        this.handleIsAllSelect(value.child)
      }
    })
  }
  
  handleAllSelect = (authList, isSelect = true) => {
    if (!authList) {
      return
    }
    let _authList = authList.map(value => {
      value.chosen = isSelect
      if (value.child) {
        this.handleAllSelect(value.child, isSelect)
      }
      return value
    })
    return _authList
  }
  
  handleSubmit = () => {
    let { authList, id, type, userType,cs } = this.state
    this.setState({
      loading:true
    })
    cs*=1
    if (type === 'all') {
      authList = this.handleAllSelect(authList)
    }
    const isDepartment = userType === 'department'
    const service = isDepartment ? departmentService.updateAuth : departmentService.userUpdateAuth
    service({
      backstageFunctionChooseDtoList: authList,
      cs,
      departmentId: id,
      backstageUserId: id
    }).then(res => {
      message.success('提交成功')
      this.setState({
        isSubmit: true,
      });
      this.handleCancel()
    }).finally(el => {
      this.setState({
        loading:false
      })
    })
  }
  
  handleCancel = () => {
    const { userType } = this.state
    const isDepartment = userType === 'department'
    const path = isDepartment ? '/permission/index' : '/permission/member'
    router.push(path)
  }
  
  handleCheckBoxChange = (evt, auth, mapIndex, isLabel=false) => {
    let { authList, secondList, functionList } = this.state
    let checked = ''
    if (isLabel) {
      checked = evt.currentTarget 
                  && evt.currentTarget.parentNode.previousSibling.getElementsByTagName("input")[0].checked
    } else {
      checked = evt.target && evt.target.checked
    }
    let _authList = cloneDeep(authList)
    let _secondList = cloneDeep(secondList)
    let _functionList = cloneDeep(functionList)
    const map = [_authList, _secondList, _functionList]
    let currentList = cloneDeep(map[mapIndex])
    const index = currentList.findIndex(f=> f.id === auth.id)
    if (index > -1) {
      if (mapIndex === 0) {
        if (!isLabel) {
          _authList[index].chosen = checked
        }
        if (checked || isLabel) {
          _secondList = _authList && _authList[index] && _authList[index].child || secondList;
          _functionList = _secondList && _secondList[0] && _secondList[0].child || functionList;
          if (!isLabel) {
            this.handleAllSelect(_secondList, checked)
          }
        }
        if (!checked) {
          _secondList = _secondList.map(v=> {
            v.chosen = false
            return v
          })
          _functionList = _functionList.map(v=> {
            v.chosen = false
            return v
          })
          if (!isLabel) {
            this.handleAllSelect(_secondList, checked)
            this.handleAllSelect(_functionList, checked)
          }
        }
        
      } else if (mapIndex === 1) {
        if (!isLabel) {
          _secondList[index].chosen = checked
        }
        const firstIndex = _authList.findIndex(f=> f.id === auth.parentId)
        _authList[firstIndex].child = _secondList
        if (!checked && _secondList) {
          _secondList[index].child = _secondList[index].child.map(v=> {
            v.chosen = false
            return v
          })
        }
        _functionList = _secondList && _secondList[index].child || functionList;
        if (!isLabel) {
          this.handleAllSelect(_functionList, checked)
        }
      } else {
        if (!isLabel) {
          _functionList[index].chosen = checked
        }
        const secondIndex = _secondList.findIndex(f=> f.id === auth.parentId)
        const second = _secondList[secondIndex]
        const firstIndex = _authList.findIndex(f=> f.id === second.parentId)
        _authList[firstIndex].child[secondIndex].child = _functionList
      }
    }
    this.setState({
      secondList: _secondList,
      authList: _authList,
      functionList: _functionList,
    });
  }
  
  handleCheckBoxLabelClick = (evt, auth, mapIndex) => {
    evt.persist();
    evt.preventDefault();
    this.handleCheckBoxChange(evt, auth, mapIndex, true)
  }
  
  
  renderAuthListCards = () => {
    const { authList = [], secondList = [], functionList = [], type } = this.state
    if (type !== 'part') {
      return  null
    }
    return (
      <div className="df auth-cards">
        <Card
          className="mt-16 w-214"
          type="inner"
          title="一级类目"
        >
          {
            authList.map(auth=>{
              return (
                <Checkbox className="auth-cards-checkbox" key={auth.id} checked={auth.chosen}
                          onChange={evt=>this.handleCheckBoxChange(evt, auth, 0)}>
                  <div className="df flex-1 space-between-center"
                       onClick={evt=>this.handleCheckBoxLabelClick(evt, auth, 0)}>
                    <span>{auth.name}</span>
                    {
                      auth.child && auth.child.length> 0? <Icon type="right" />: null
                    }
                  </div>
                </Checkbox>
              )
            })
          }
        </Card>
        <Card
          className="mt-16 w-256"
          type="inner"
          title="下属二级类目"
        >
          {
            secondList.map(auth=>{
              return (
                <Checkbox className="auth-cards-checkbox" key={auth.id} checked={auth.chosen}
                          onChange={evt=>this.handleCheckBoxChange(evt, auth, 1)}>
                  <div className="df flex-1 space-between-center"
                       onClick={evt=>this.handleCheckBoxLabelClick(evt, auth, 1)}>
                    <span>{auth.name}</span>
                    {
                      auth.child && auth.child.length> 0? <Icon type="right" />: null
                    }
                  </div>
                </Checkbox>
              )
            })
          }
        </Card>
        <Card
          className="mt-16 w-256"
          type="inner"
          title="功能"
        >
          {
            functionList.map(auth=>{
              return (
                <Checkbox className="auth-cards-checkbox" key={auth.id} checked={auth.chosen}
                          onChange={evt=>this.handleCheckBoxChange(evt, auth, 2)}>
                  <div className="df flex-1 space-between-center">
                    <span>{auth.name}</span>
                  </div>
                </Checkbox>
              )
            })
          }
        </Card>
      </div>
    )
  }
  
  renderRouterWillLeave=()=>{
    const { originValues, isSubmit, authList } = this.state
    let isPrompt = !isEqual(authList, originValues)
    return (
      <RouterWillLeave isPrompt={isPrompt} isSubmit={isSubmit}  />
    )
  }
  
  render() {
    const {
      loading,info, type, userType,cs
    } = this.state;
    const { name, } = info;
    const radioStyle = {
      display: 'block',
      height: '30px',
      lineHeight: '30px',
    };
    
    const title = userType === 'user'? '成员名称': '部门名称'
    
    return (
      <PageHeaderWrapper  title={false}>
        {
          this.renderRouterWillLeave()
        }
        <Spin spinning={loading}>
          <Card bordered={false} style={{ marginTop: 20 }}>
            <Form {...formItemLayout}>
              <Form.Item label={title}>{name}</Form.Item>
              <Form.Item label="是否催收" wrapperCol={{span: 17}}>
                <div>
                  <div>
                    <Radio.Group onChange={this.onChange1} value={cs}>
                      <Radio style={radioStyle} value='1'>是</Radio>
                      <Radio style={radioStyle} value='0'>否</Radio>
                    </Radio.Group>
                  </div>
                </div>
              </Form.Item>
              <Form.Item label="权限设置" wrapperCol={{span: 17}}>
                <div>
                  <div>
                    <Radio.Group onChange={this.onChange} value={type}>
                      <Radio style={radioStyle} value="all">全部</Radio>
                      <Radio style={radioStyle} value="part">部分</Radio>
                    </Radio.Group>
                  </div>
                  {
                    this.renderAuthListCards()
                  }
                </div>
      
              </Form.Item>

              <Form.Item wrapperCol={{offset: 2}}>
                <Button style={{ marginLeft: 8 }} onClick={this.handleCancel}>
                  取消
                </Button>
                <Button type="primary" htmlType="submit" onClick={this.handleSubmit}>
                  确定
                </Button>
              </Form.Item>
            </Form>
  
          </Card>
        </Spin>
      </PageHeaderWrapper>
    )
  }
}

export default PermissionConfig
