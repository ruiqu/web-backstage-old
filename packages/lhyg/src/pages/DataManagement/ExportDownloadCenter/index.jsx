import React from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Card, Spin, Badge, Button, message, BackTop } from 'antd';
import { fetchAllDownloadUrls } from '@/services/order';
import MyPageTable from '@/components/MyPageTable';
import styles from './index.less';

class ExportDownloadCenter extends React.Component {
  state = {
    loading: false, // 是否正在加载列表数据中
    results: [], // 结果数据
  }

  componentDidMount() {
    this.fetchDatas();
  }

  /**
   * 渲染表格中的状态栏
   * @param {*} val 
   * @param {*} record 
   * @param {*} idx 
   */
  renderStatus = val => {
    if (val === 'SUCCESS') return <Badge status="success" text="成功" />
    if (val === 'FAIL') return <Badge status="default" text="失败" />
    const map = { PROCESSING: '处理中', EMPTY: '空记录' };
    return map[val];
  }

  /**
   * 渲染表格中的操作栏
   */
  renderOperation = (val, record) => {
    const downloadFile = () => {
      const url = record && record.url;
      if (!url) {
        message.warning('该文件不存在下载链接');
        return;
      }
      window.location.href = url;
    };
    return <Button onClick={downloadFile} type='link' disabled={record && record.status !== 'SUCCESS'}>下载</Button>
  }

  columns = [
    { title: '文件名称', dataIndex: 'fileName' },
    { title: '导出时间', dataIndex: 'exportTime' },
    { title: '状态', dataIndex: 'status', render: this.renderStatus },
    { title: '操作', dataIndex: 'url', render: this.renderOperation },
  ]

  /**
   * 加载历史导出订单数据
   * @returns 
   */
  fetchDatas = () => {
    if (this.state.loading) return;
    this.setState({ loading: true });
    let list = []; // 订单列表数据
    fetchAllDownloadUrls().then(res => {
      list = res && res.data;
    }).catch(() => {
      console.error('/hzsx/export/exportHistory接口报错');
    }).finally(() => {
      this.setState({ loading: false, results: list });
    });
  }

  render() {
    return (
      <PageHeaderWrapper title={false}>
        <Card bordered={false}>
          <div className={styles.buttonContainer}>
            <span className={styles.flexg} />
            <Button disabled={this.state.loading} onClick={this.fetchDatas} type='primary'>刷新</Button>
          </div>
          <Spin spinning={this.state.loading}>
            <MyPageTable
              paginationProps={false}
              dataSource={this.state.results}
              columns={this.columns}
            />
          </Spin>
        </Card>
        <BackTop />
      </PageHeaderWrapper>
    );
  }
}

export default ExportDownloadCenter;