import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import {
  Button,
  Card,
  Form,
  Spin,
  Table,
  Tooltip,
  Modal,
  message,
  Row,
  Col,
  Select,
  Popconfirm,
} from 'antd';
import { connect } from 'dva';
import { getToken, getCurrentUser } from '@/utils/localStorage';
import { routerRedux } from 'dva/router';
import orderService from '@/services/order';
import request from '@/services/baseService';
import { getTimeDistance, renderOrderStatus } from '@/utils/utils';
import { orderCloseStatusMap, orderStatusMap } from '@/utils/enum';

@Form.create()
export default class ReceiveOrder extends Component {
  state = {
    loading: false,
    userLoading: false,
    orderList: [],
    userList: [],
    gBtusua: '',
    userVisible: false,
    // 选择的用户
    selectUserItem: {},
    // 选择订单
    selectOrderItem: [],
    paginationProps: {
      pageSizeOptions: ['10', '20', '30', '50'],
      showSizeChanger: true,
      defaultCurrent: 1,
      defaultPageSize: 10,
      total: 0,
    },
  };

  // 页面装载前 在render 之前调用
  componentWillMount() {
    this.setState({
      loading: true,
    });
  }

  // 页面装载后
  componentDidMount() {
    this.initData();
  }

  onChangePage = e => {
    console.log(999, e);
    this.setState(
      {
        loading: true,
        paginationProps: {
          ...e,
          defaultCurrent: e.current,
          defaultPageSize: e.pageSize,
        },
      },
      () => {
        this.initData();
      },
    );
  };

  //未认领的订单数据
  initData = () => {
    console.log(123456, this.state);
    this.setState.loading = true;
    let da = {
      status: this.state.gBtusua,
      pageNumber: this.state.paginationProps.defaultCurrent,
      pageSize: this.state.paginationProps.defaultPageSize,
    };

    orderService.listQueryOpeOrderByCondition(da).then(res => {
      if (res) {
        console.log('orderList', res.total);
        let pageNav = this.state.paginationProps;
        pageNav.total = res.total;
        this.setState({
          orderList: res.records,
          loading: false,
          paginationProps: pageNav,
        });
      } else {
        this.setState({
          loading: false,
        });
      }
    });
  };

  selectUser = () => {
    if (this.state.selectOrderItem && this.state.selectOrderItem.length == 0) {
      message.warning('请先选择订单');
    } else {
      this.setState(
        {
          userLoading: true,
        },
        () => {
          // 用户
          orderService.listBackstageUser().then(res => {
            if (res) {
              console.log('userList', res);
              this.setState({
                userList: res,
                userLoading: false,
                userVisible: true,
              });
            } else {
              message.warning('无有效用户');
            }
          });
        },
      );
    }
  };

  onChanges = e => {
    this.setState({
      gBtusua: e,
    });
  };

  render() {
    const { form } = this.props;

    const user = getCurrentUser();
    const { getFieldDecorator } = form;
    const { loading, orderList, userLoading, userList, userVisible, paginationProps } = this.state;

    const columns = [
      {
        title: '订单编号',
        dataIndex: 'orderId',
        width: 160,
      },
      {
        title: '渠道',
        dataIndex: 'channelName',
        width: 80,
      },
      {
        title: '商品名称',
        dataIndex: 'productName',
        width: 160,
      },
      {
        title: '规格',
        dataIndex: 'productInfo',
        width: 120,
        render: e => {
          return <>{e.spec}</>;
        },
      },
      {
        title: '已付期数',
        width: 110,
        render: e => {
          return (
            <>
              <Tooltip placement="top" title={e.currentPeriods}>
                <div
                  style={{
                    width: 60,
                  }}
                >
                  {e.payedPeriods}/{e.totalPeriods}
                </div>
              </Tooltip>
            </>
          );
        },
      },
      {
        title: '总租金',
        width: 90,
        render: e => {
          return <>{e.totalRentAmount}</>;
        },
      },
      {
        title: '已付租金',
        width: 90,
        render: e => {
          return <>{e.payedRentAmount}</>;
        },
      },
      {
        title: '订单状态',
        dataIndex: 'status',
        width: 120,
        render: (_, record) => renderOrderStatus(record),
      },
      {
        title: '下单人姓名',
        width: 120,
        render: e => {
          return <>{e.realName}</>;
        },
      },
      {
        title: '下单人手机号',
        width: 120,
        render: e => {
          return <>{e.telephone}</>;
        },
      },
      {
        title: '下单时间',
        dataIndex: 'placeOrderTime',
        width: 120,
      },
      {
        title: '收货人手机号',
        width: '120px',
        render: e => {
          return <>{e.addressUserPhone}</>;
        },
      },
      {
        title: '操作',
        width: '120px',
        render: (text, record) => {
          return user.job && user.job === 1 ? (
            ''
          ) : (
            <Popconfirm title="确定认领此订单吗？" onConfirm={() => renling(record)}>
              <a>认领订单</a>
            </Popconfirm>
          );
        },
      },
    ];

    const columnsUser = [
      {
        title: '成员账号',
        dataIndex: 'mobile',
      },
      {
        title: '姓名',
        dataIndex: 'name',
      },
      {
        title: '邮箱地址',
        dataIndex: 'email',
      },
      {
        title: '是否启用',
        dataIndex: 'status',
        render: text => (text === 'VALID' ? '启用' : '未启用'),
      },
    ];

    const renling = async render => {
      orderService
        .claimOrders({
          orderIdNew: render.orderId,
          uid: render.uid,
        })
        .then(res => {
          this.initData();
        });
    };

    const handleOk = () => {
      const { selectOrderItem, selectUserItem } = this.state;

      this.setState(
        {
          userVisible: true,
        },
        () => {
          orderService
            .allocationClaim({
              orderId: selectOrderItem,
              buckstageUserId: selectUserItem.buckstageUserId,
              name: selectUserItem.name,
            })
            .then(res => {
              // console.log("认领", res)
              message.success('认领成功');
              this.initData();
              this.setState({
                userVisible: false,
              });
            });
        },
      );
    };

    const handleCancel = () => {
      this.setState({
        userVisible: false,
      });
    };

    return (
      <PageHeaderWrapper title={false}>
        <Card>
          <Form layout="inline" onSubmit={this.initData}>
            <Row>
              {/* <Col span={8}>
                            <Button type="primary" onClick={() => this.selectUser()}>关联用户</Button>
                            </Col> */}
              <Col span={8}>
                <Form.Item label="">
                  {getFieldDecorator('status', {
                    // initialValue: '',
                  })(
                    <Select
                      placeholder="订单状态"
                      allowClear
                      style={{ width: 180 }}
                      onChange={this.onChanges}
                    >
                      {['01', '11'].map(value => {
                        return (
                          <Option value={value.toString()} key={value.toString()}>
                            {orderStatusMap[value.toString()]}
                          </Option>
                        );
                      })}
                    </Select>,
                  )}
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item>
                  <Button type="primary" htmlType="submit">
                    查询
                  </Button>
                  {user.job && user.job === 1 ? (
                    <Button type="primary" onClick={() => this.selectUser()}>
                      关联用户
                    </Button>
                  ) : (
                    ''
                  )}
                </Form.Item>
              </Col>
              <Col span={8} style={{ position: 'absolute', right: '0px' }}>
                <text style={{ position: 'absolute', right: '0px' }}>
                  {orderList.length ? '合计' + orderList.length + '条' : ''}
                </text>
              </Col>
            </Row>
          </Form>
          <Spin spinning={loading}>
            <Table
              loading={loading}
              columns={columns}
              dataSource={orderList}
              pagination={paginationProps}
              onChange={this.onChangePage}
              // pagination={false}
              rowKey={record => record.orderId}
              rowSelection={{
                type: 'checkbox',
                onChange: (selectedRowKeys, selectedRows) => {
                  console.log(selectedRowKeys);
                  console.log(selectedRows);

                  this.setState({
                    selectOrderItem: selectedRowKeys,
                  });
                },
              }}
              scroll={{ x: scroll }}
            />
          </Spin>
          <Spin spinning={userLoading}>
            <Modal
              title="选择用户"
              visible={userVisible}
              onOk={handleOk}
              onCancel={handleCancel}
              width={1000}
            >
              <Table
                columns={columnsUser}
                dataSource={userList}
                pagination={false}
                rowKey={record => record.id}
                rowSelection={{
                  type: 'radio',
                  onChange: (selectedRowKeys, selectedRows) => {
                    console.log(selectedRowKeys);
                    console.log(selectedRows);
                    this.setState({
                      selectUserItem: {
                        buckstageUserId: selectedRows[0].id,
                        name: selectedRows[0].name,
                      },
                    });
                  },
                }}
                scroll={{ y: 400 }}
              />
            </Modal>
          </Spin>
        </Card>
      </PageHeaderWrapper>
    );
  }
}
