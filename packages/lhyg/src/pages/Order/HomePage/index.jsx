import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import {
  Card,
  Tooltip,
  Button,
  Form,
  Input,
  Select,
  DatePicker,
  Spin,
  Row,
  Col,
  Divider,
  Modal,
  message,
  Drawer,
  InputNumber,
  Table,
  Icon,
} from 'antd';
import MyPageTable from '@/components/MyPageTable';
import { onTableData, ddzq, getqd, zffs } from '@/utils/utils.js';
import CopyToClipboard from '@/components/CopyToClipboard';
import { getTimeDistance, renderOrderStatus } from '@/utils/utils';
import moment from 'moment';
const { Option } = Select;
const { RangePicker } = DatePicker;
import { routerRedux } from 'dva/router';
import { getCurrentUser } from '@/utils/localStorage';
import MyStage from '@/components/MyStage'; //探针
import AntTable from '@/components/AntTable';
import request from '../../../services/baseService';
import { orderCloseStatusMap, orderStatusMap, verfiyStatusMap, AuditReason } from '@/utils/enum';
import { exportCenterHandler } from '../util';
import { UserRating } from 'zwzshared';
import styles from './index.less';

@connect(({ order, loading }) => ({
  ...order,
  loading: loading.effects['order/queryOpeOrderByConditionList'],
}))
@Form.create()
export default class HomePage extends Component {
  state = {
    current: 1,
    pageSize: 30,
    orderVisible: '',
    orderId: null,
    datas: {},
    record: {},
    defaulttime: [],
    initialValue: [],
    stages: [],
    stagesShow: false,
    backuser: [],
    channelList: [],
    expressList: [],
    yunTime: getTimeDistance('month'),
    gBtusua: '04', // 默认状态: 待发货
    queryLoad: false,
    rowSelectionOrderId: [],
    initIdCard: '',
  };

  componentDidMount(e) {
    const { status, ti, form, idCard } = this.props.location.query;
    console.log(789789789, form, idCard);
    var date = new Date();
    let d1 = moment(date).format('YYYY-MM-DD') + ' 00:00:00';
    let d2 = moment(date).format('YYYY-MM-DD') + ' 23:59:59';
    if (ti) {
      console.log('d1,d2', d1, d2);
      this.setState({
        initialValue: [moment(d1), moment(d2)],
      });
      this.props.form.setFieldsValue({
        createTimeStart: d1,
        createTimeEnd: d2,
      });
      this.onList(1, 30, {
        createTimeStart: d1,
        createTimeEnd: d2,
      });
    } else if (status) {
      console.log('e 有状态传过来', status);
      let setFields = {};
      let datas = {};
      if (status == 88 || status == 99) {
        //如果是这两种状态都要时间为当天，今日成交，客户拒绝关单
        setFields.createTimeStart = d1;
        setFields.createTimeEnd = d2;
        datas.initialValue = [moment(d1), moment(d2)];
        datas.createTimeStart = d1;
        datas.createTimeEnd = d2;
      }
      //待审核,复审状态通过
      if (status == 88) {
        //今日成交
        datas.status = '11';
        setFields.preVerify = 1;
      } else if (status == 89) {
        //被拒绝
        setFields.closeMold = '07';
        datas.status = '10';
      } else if (status == 99) {
        //客户拒绝关单
        setFields.closeMold = '07';
        datas.status = '10';
      } else if (status == 87) {
        //通过未成交
        setFields.preVerify = 1;
        datas.status = '10';
      } else {
        datas.status = status;
      }
      setFields.status = datas.status;

      console.log(setFields, datas, 2222222);

      this.setState({ datas: setFields }, () => {
        this.props.form.setFieldsValue(setFields);
        this.onList(1, 30, setFields);
        // this.getExpressList();
      });
    } else if (form && idCard) {
      if (form === 'claimOrder') {
        this.setState(
          {
            initIdCard: idCard,
          },
          () => {
            this.handleSubmit();
          },
        );
      }
    } else {
      console.log('走这里');
      this.onList(1, 30);
      // this.getExpressList();
    }

    this.props.dispatch({
      type: 'order/PlateformChannel',
    });

    //zyj-backstage-web/hzsx/business/channel/getChannelList
    console.log('渠道获取列表');
    request('/hzsx/business/channel/getChannelList', {
      pageNumber: 1,
      islist: 1,
      pageSize: 1000,
    }).then(res => {
      console.log('返回的信息是', res);
      res.records.unshift({ id: '000', name: '默认' });
      this.setState({
        channelList: res.records,
      });
    });
    //获取分配的用户列表
    request('/hzsx/user/listBackstageUser', {}, 'get').then(res => {
      console.log('用户信息：', res);
      this.setState({
        backuser: res,
      });
    });
  }
  onList = (pageNumber, pageSize, data = {}) => {
    console.log('onlist的数据：', data);

    const { dispatch } = this.props;
    dispatch({
      type: 'order/queryOpeOrderByConditionList',
      payload: {
        pageSize,
        pageNumber,
        ...data,
      },
    }).then(() => {
      this.setState({
        queryLoad: false,
      });
    });
  };
  // 重置
  handleReset = e => {
    this.setState(
      {
        gBtusua: '',
      },
      () => {
        this.props.form.resetFields();
        this.props.form.setFieldsValue({
          status: undefined,
        });
        this.handleSubmit(e);
      },
    );
  };
  handleSubmit = e => {
    e && e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (values.createDate && values.createDate.length < 1) {
        values.createTimeStart = '';
        values.createTimeEnd = '';
        this.setState(
          {
            datas: { ...values },
            current: 1,
            queryLoad: true,
          },
          () => {
            this.onList(1, 30, { ...values });
          },
        );
      } else if (values.createDate) {
        values.createTimeStart = moment(values.createDate[0]).format('YYYY-MM-DD') + ' 00:00:00';
        values.createTimeEnd = moment(values.createDate[1]).format('YYYY-MM-DD') + ' 23:59:59';
        this.setState(
          {
            datas: { ...values },
            current: 1,
            queryLoad: true,
          },
          () => {
            this.onList(1, 30, { ...values });
          },
        );
      } else {
        this.setState(
          {
            datas: { ...values },
            current: 1,
            queryLoad: true,
          },
          () => {
            this.onList(1, 30, { ...values });
          },
        );
      }
    });
  };
  newexp = e => {
    console.log('aaa,提交请求', e);
    e && e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (values.createDate && values.createDate.length < 1) {
        values.createTimeStart = '';
        values.createTimeEnd = '';
        this.onexport1(values);
      } else if (values.createDate) {
        values.createTimeStart = moment(values.createDate[0]).format('YYYY-MM-DD') + ' 00:00:00';
        values.createTimeEnd = moment(values.createDate[1]).format('YYYY-MM-DD') + ' 23:59:59';
        this.onexport1(values);
      } else {
        this.onexport1(values);
      }
    });
  };

  ziti = e => {
    e && e.preventDefault();
    if (this.state.rowSelectionOrderId.length == 0) {
      message.error('请先选择订单');
      return;
    }
    request(`/hzsx/business/order/orderPickUpSelect`, {
      orderIds: this.state.rowSelectionOrderId,
    }).then(res => {
      message.success('操作完成', 5);
    });
  };

  onexport1 = values => {
    values.tp = 'list';
    if (values.status) values.status = [values.status];
    //exportCenterHandler(this, 'exportRentOrder', true, false);
    request(`/hzsx/export/rentOrder`, values).then(res => {
      console.log('res...', res);
      message.success('完成', 5);
    });
  };

  onexport = () => {
    exportCenterHandler(this, 'exportRentOrder', true, false);
    //request(`/hzsx/export/rentOrder`, data)
  };

  showOrderModal = (type, orderId) => {
    console.log('按钮点击', type, orderId);
    this.setState({
      orderVisible: type,
      orderId,
    });
  };

  renderEditModal() {
    const { orderVisible, orderId } = this.state;
    const { getFieldDecorator } = this.props.form;
    const handleOk = e => {
      e.preventDefault();
      this.props.form.validateFields(['channelId'], (err, values) => {
        if (!err) {
          values.orderId = orderId;
          console.log('orderid:', values);
          request(`/hzsx/buckstageOrderClaim/updateChannelId`, values).then(res => {
            console.log('rcs', res);
            this.setState({
              orderVisible: false,
            });
            message.success('添加完成', 5);
          });
        }
      });
    };

    return (
      <div>
        <Modal
          title="修改渠道"
          visible={orderVisible == 'editqd'}
          onOk={handleOk}
          onCancel={this.handleCancel}
        >
          <Form>
            <Form.Item label="">
              {getFieldDecorator(
                'channelId',
                {},
              )(
                <Select
                  placeholder="渠道来源"
                  allowClear
                  style={{ width: 202 }}
                  onChange={this.onChangesChannel}
                >
                  {this.state.channelList.map(value => {
                    return (
                      <Option value={value.id} key={value.id}>
                        {value.name}
                      </Option>
                    );
                  })}
                </Select>,
              )}
            </Form.Item>
          </Form>
        </Modal>
      </div>
    );
  }
  //编辑渠道金额
  renderAddQdjeModal() {
    const { orderVisible, orderId } = this.state;
    const { getFieldDecorator } = this.props.form;
    const handleOk = e => {
      e.preventDefault();
      this.props.form.validateFields(['qdPrice'], (err, values) => {
        if (!err) {
          values.orderId = orderId;
          console.log('orderid:', values);
          request(`/hzsx/buckstageOrderClaim/updateChannelPrice`, values).then(res => {
            console.log('rcs', res);
            this.setState({
              orderVisible: false,
            });
            message.success('添加完成', 5);
          });
        }
      });
    };

    return (
      <div>
        <Modal
          title="修改渠道金额"
          visible={orderVisible == 'qdprice'}
          onOk={handleOk}
          onCancel={this.handleCancel}
        >
          <Form>
            <Form.Item label="">
              {getFieldDecorator(
                'qdPrice',
                {},
              )(<InputNumber placeholder="金额" style={{ width: '90%' }} />)}
            </Form.Item>
          </Form>
        </Modal>
      </div>
    );
  }

  handleCancel = e => {
    this.setState({
      orderVisible: '',
    });
  };
  //table 页数
  onPage = e => {
    console.log('eeeeonPage', e, this.state.datas);
    this.setState(
      {
        current: e.current,
        pageSize: e.pageSize,
      },
      () => {
        const params = {
          ...this.state.datas,
        };
        this.onList(e.current, e.pageSize, params);
      },
    );
  };
  onChanges = e => {
    this.setState({
      gBtusua: e,
    });
  };

  // 认领订单
  lingqu = () => {
    // console.log("user",getCurrentUser());
    this.props.dispatch(
      routerRedux.push({
        pathname: `/Order/HomePage/ReceiveOrder`,
      }),
    );
  };

  //查看账单
  seeStage = orderId => {
    request(`/hzsx/business/order/queryOrderStagesDetail`, { orderId }).then(res => {
      console.log('sjrh：', res.orderByStagesDtoList);
      this.setState({
        stages: res.orderByStagesDtoList,
        stagesShow: true,
      });
    });
  };

  onClose = () => {
    console.log('取消：');
    this.state.stagesShow = false;
    this.setState({ stagesShow: false });
  };
  render() {
    const { allList, allTotal, loading, form } = this.props;
    let orderByStagesDtoList = this.state.stages;
    console.log(999, this.state.queryLoad);
    let initialValue = this.state.initialValue;
    const paginationProps = {
      current: this.state.current,
      pageSize: this.state.pageSize,
      total: allTotal,
      pageSizeOptions: ['10', '20', '30', '50'],
      showSizeChanger: true,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / 30)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };

    const columnsByStagesStute = {
      '1': '待支付',
      '2': '已支付',
      '3': '逾期已支付',
      '4': '逾期待支付',
      '5': '已取消',
      '6': '已结算',
      '7': '已退款,可用',
      '8': '部分还款',
    };

    const user = getCurrentUser();

    const { getFieldDecorator } = form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };

    const riskRemark = () => {
      return (
        <div>
          <Tooltip title="是否黑名单| 系统还款记录| 系统压力">
            <span>风控备注</span>
            <Icon style={{ color: '#1890ff' }} type="exclamation" />
          </Tooltip>
        </div>
      );
    };

    let columns = [
      {
        title: '订单编号',
        dataIndex: 'orderId',
        render: orderId => {
          let oid = orderId.slice(0, 8) + '...';
          return <CopyToClipboard text={orderId} />;
        },
        width: '120px',
      },
      {
        title: '渠道来源',
        dataIndex: 'channelName',
        width: 100,
        render: text => (text === '000' ? '默认' : text),
      },
      {
        title: '初始下单渠道',
        dataIndex: 'firstChannelName',
        width: 100,
        render: text => (text === '000' ? '默认' : text),
      },
      {
        title: '审核',
        dataIndex: 'rname',
        width: 100,
      },
      {
        title: '商品名称',
        dataIndex: 'productName',
        width: 120,
        // ellipsis: true,
      },

      // {
      //   title: '已付期数',
      //   width: 110,
      //   render: e => {
      //     return (
      //       <>
      //         <Tooltip placement="top" title={e.currentPeriods}>
      //           <div
      //             style={{
      //               width: 60,
      //             }}
      //           >
      //             <p>
      //               {e.payedPeriods}/{e.totalPeriods + 1}
      //             </p>
      //             <a onClick={() => {
      //               this.seeStage(e.orderId)
      //             }}>查看</a>
      //           </div>
      //         </Tooltip>
      //       </>
      //     );
      //   },
      // },
      /*  {
         title: '总租金',
         width: 90,
         render: e => {
           return <>{e.totalRentAmount}</>;
         },
       }, */
      {
        title: '已付金额',
        width: 90,
        render: e => {
          return (
            <>
              <div>
                <p style={{ display: 'flex' }}>{e.payedRentAmount}</p>
                <p style={{ display: 'flex' }}>{e.totalRentAmount}</p>
              </div>
            </>
          );
        },
      },
      {
        title: '姓名/手机号',
        width: 140,
        render: e => {
          return (
            <>
              <div>
                {e && e.isVip && e.isVip == 1 ? (
                  <p className="green-status">{e.realName}</p>
                ) : (
                  <p>{e.realName}</p>
                )}
                <p>{e.telephone}</p>
              </div>
            </>
          );
        },
      },
      {
        title: '风控评分',
        dataIndex: 'modelScore',
      },
      {
        title: '风控备注',
        width: 140,
        dataIndex: 'riskRemark',
      },
      {
        title: '风控信息',
        dataIndex: 'riskMsg',
        render: (e,record) => {
          return record.riskMsg === '黑名单' ? <span style={{color:'red'}}>{record.riskMsg}!!</span> : record.riskMsg
        }
      },
      {
        title: '下单时间',
        dataIndex: 'placeOrderTime',
        width: 120,
      },
      {
        title: '完结时间',
        dataIndex: 'finishTime',
        width: 120,
      },
      {
        title: '收货人手机号',
        width: '140px',
        render: e => {
          return <>{e.addressUserPhone}</>;
        },
      },

      {
        title: '订单状态',
        dataIndex: 'status',
        width: 130,
        render: (_, record) => {
          return renderOrderStatus(record);
        },
      },
      {
        title: '复审状态',
        dataIndex: 'preVerify',
        width: 120,
        render: (text, record, index) => {
          return (
            <>
              {verfiyStatusMap[text || 0]}-{record.orderRemark}
            </>
          );
        },
      },

      {
        title: '关单类型',
        dataIndex: 'closeMold',
        width: 120,
        render: (text, record, index) => {
          return <>{AuditReason[text]}</>;
        },
      },
      {
        title: '试用周期',
        dataIndex: 'isWeek',
        width: 120,
        render: (text, record, index) => {
          return <>{ddzq[text]}</>;
        },
      },
      // {
      //   title: '起租时间',
      //   width: '120px',
      //   render: e => {
      //     return <>{e.rentStart}</>;
      //   },
      // },
      // {
      //   title: '归还时间',
      //   width: '120px',
      //   render: e => {
      //     return <>{e.unrentTime}</>;
      //   },
      // },
      // {
      //   title: '渠道展示金额',
      //   dataIndex: 'qdPrice',
      //   width: 120,
      // },
      {
        title: '订单来源',
        width: 120,
        render: e => {
          return <>{getqd(e.qd)}</>;
        },
      },
      {
        title: '操作',
        width: 160,
        fixed: 'right',
        align: 'center',
        render: (e, record) => {
          return (
            <div>
              <a
                className="primary-color"
                // onClick={() => router.push(`/Order/Details?id=${e.orderId}`)}
                href={`#/Order/HomePage/Details?id=${e.orderId}`}
                target="_blank"
              >
                处理
              </a>
              <Divider type="vertical" />
              <a
                className="primary-color"
                onClick={() => {
                  this.showOrderModal('editqd', e.orderId);
                }}
              >
                修改渠道
              </a>
              {/* <Divider type="vertical" />
              <a
                className="primary-color"
                onClick={() => {
                  this.showOrderModal("qdprice", e.orderId)
                }}
              >
                修改渠道金额
              </a> */}
            </div>
          );
        },
      },
    ];

    const columnsByStages = [
      {
        title: '总期数',
        dataIndex: 'totalPeriods',
      },
      {
        title: '当前期数',
        dataIndex: 'currentPeriods',
      },
      {
        title: '租金',
        dataIndex: 'currentPeriodsRent',
      },
      {
        title: '状态',
        render: e => (
          <>
            <span>{columnsByStagesStute[e.status]}</span>
          </>
        ),
      },
      {
        title: '支付时间',
        dataIndex: 'repaymentDate',
      },
      {
        title: '账单到期时间',
        dataIndex: 'statementDate',
      },
    ];

    const rowSelection = {
      onChange: (selectedRowKeys, selectedRows) => {
        let array = [];
        selectedRows.forEach(el => {
          array.push(el.orderId);
        });
        this.setState({
          rowSelectionOrderId: array,
        });
      },
      getCheckboxProps: record => ({
        disabled: record.name === 'Disabled User', // Column configuration not to be checked
        name: record.name,
      }),
    };

    const notRepurchasedLabel = () => {
      return (
        <Tooltip title="已经下单并完结一次订单，但是未复购的用户">
          <Icon style={{ color: '#1890ff' }} type="exclamation" />
          <span>未下单用户</span>
        </Tooltip>
      );
    };

    return (
      <PageHeaderWrapper title={false}>
        <Drawer
          width={620}
          title="账单信息"
          placement="right"
          onClose={this.onClose}
          visible={this.state.stagesShow}
        >
          <MyStage orderByStagesDtoList={orderByStagesDtoList}></MyStage>
        </Drawer>
        <Card bordered={false}>
          <Form labelAlign="right" layout="inline" onSubmit={this.handleSubmit}>
            <Row>
              <Col span={8}>
                <Form.Item label={<span className={styles.formLabel}>商品名称</span>}>
                  {getFieldDecorator(
                    'productName',
                    {},
                  )(<Input allowClear placeholder="请输入商品名称" />)}
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label={<span className={styles.formLabel}>下单人姓名</span>}>
                  {getFieldDecorator('userName', {})(<Input allowClear placeholder="请输入下单人姓名" />)}
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label={<span className={styles.formLabel}>下单人手机号</span>}>
                  {getFieldDecorator(
                    'telephone',
                    {},
                  )(<Input allowClear placeholder="请输入下单人手机号" />)}
                </Form.Item>
              </Col>
              <Col span={8}>
                {' '}
                <Form.Item label={<span className={styles.formLabel}>订单编号</span>}>
                  {getFieldDecorator(
                    'orderId',
                    {},
                  )(<Input allowClear placeholder="请输入订单编号" />)}
                </Form.Item>
              </Col>

              <Col span={8}>
                <Form.Item label={<span className={styles.formLabel}>用户身份证</span>}>
                  {getFieldDecorator(
                    'idCard',
                    {initialValue: this.state.initIdCard,},
                  )(<Input allowClear placeholder="请输入用户身份证" />)}
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label={<span className={styles.formLabel}>收货人手机号</span>}>
                  {getFieldDecorator(
                    'addressUserPhone',
                    {},
                  )(<Input allowClear placeholder="请输入收货人手机号" />)}
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label={<span className={styles.formLabel}>订单状态</span>}>
                  {getFieldDecorator(
                    'status',
                    {},
                  )(
                    <Select
                      placeholder="订单状态"
                      allowClear
                      style={{ width: 202 }}
                      onChange={this.onChanges}
                    >
                      {[
                        '01',
                        '16',
                        '17',
                        '11',
                        '04',
                        '05',
                        '06',
                        '07',
                        '08',
                        '09',
                        '10',
                        '19',
                        '20',
                        '21',
                      ].map(value => {
                        return (
                          <Option value={value.toString()} key={value.toString()}>
                            {orderStatusMap[value.toString()]}
                          </Option>
                        );
                      })}
                    </Select>,
                  )}
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label={<span className={styles.formLabel}>审核员姓名</span>}>
                  {getFieldDecorator(
                    'rid',
                    {},
                  )(
                    <Select
                      placeholder="审核员"
                      allowClear
                      style={{ width: 202 }}
                      onChange={this.onChangesChannel}
                    >
                      {this.state.backuser.map(value => {
                        return (
                          <Option value={value.id} key={value.id}>
                            {value.name}
                          </Option>
                        );
                      })}
                    </Select>,
                  )}
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label={<span className={styles.formLabel}>排序</span>}>
                  {getFieldDecorator(
                    'sort',
                    {},
                  )(
                    <Select placeholder="排序" allowClear style={{ width: 202 }}>
                      <Option value="id_desc">时间倒序</Option>
                    </Select>,
                  )}
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label={<span className={styles.formLabel}>渠道来源</span>}>
                  {getFieldDecorator(
                    'channelId',
                    {},
                  )(
                    <Select
                      placeholder="渠道来源"
                      allowClear
                      style={{ width: 202 }}
                      onChange={this.onChangesChannel}
                      showSearch
                      filterOption={(input, option) =>
                        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                      }
                    >
                      {this.state.channelList.map(value => {
                        return (
                          <Option value={value.id} key={value.id}>
                            {value.name}
                          </Option>
                        );
                      })}
                    </Select>,
                  )}
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label={<span className={styles.formLabel}>初始下单渠道</span>}>
                  {getFieldDecorator(
                    'firstChannelId',
                    {},
                  )(
                    <Select
                      placeholder="初始下单渠道"
                      allowClear
                      style={{ width: 202 }}
                      onChange={this.onChangesChannel}
                      showSearch
                      filterOption={(input, option) =>
                        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                      }
                    >
                      {this.state.channelList.map(value => {
                        return (
                          <Option value={value.id} key={value.id}>
                            {value.name}
                          </Option>
                        );
                      })}
                    </Select>,
                  )}
                </Form.Item>
              </Col>

              <Col span={8}>
                <Form.Item label={<span className={styles.formLabel}>关单类型</span>}>
                  {getFieldDecorator(
                    'closeType',
                    {},
                  )(
                    <Select placeholder="关单类型" allowClear style={{ width: 202 }}>
                      {orderCloseStatusMap.map(value => {
                        return (
                          <Option value={value.value} key={value.value}>
                            {value.content}
                          </Option>
                        );
                      })}
                    </Select>,
                  )}
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label={<span className={styles.formLabel}>复审状态</span>}>
                  {getFieldDecorator(
                    'preVerify',
                    {},
                  )(
                    <Select placeholder="复审状态" allowClear style={{ width: 202 }}>
                      {verfiyStatusMap.map((value, i) => {
                        return (
                          <Option value={i} key={i}>
                            {value}
                          </Option>
                        );
                      })}
                    </Select>,
                  )}
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label={<span className={styles.formLabel}>关闭原因</span>}>
                  {getFieldDecorator(
                    'closeMold',
                    {},
                  )(
                    <Select style={{ width: 202 }} allowClear placeholder="请选择">
                      {Object.keys(AuditReason).map(option => {
                        return (
                          <Option key={option} value={option}>
                            {AuditReason[option]}
                          </Option>
                        );
                      })}
                    </Select>,
                  )}
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label={<span className={styles.formLabel}>订单周期</span>}>
                  {getFieldDecorator(
                    'unit',
                    {},
                  )(
                    <Select style={{ width: 202 }} allowClear placeholder="请选择">
                      {ddzq.map((value, i) => {
                        return (
                          <Option value={i} key={i}>
                            {value}
                          </Option>
                        );
                      })}
                    </Select>,
                  )}
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label={<span className={styles.formLabel}>支付方式</span>}>
                  {getFieldDecorator(
                    'buyWay',
                    {},
                  )(
                    <Select style={{ width: 202 }} allowClear placeholder="请选择">
                      {zffs.map((value, i) => {
                        return (
                          <Option value={value.value} key={value.key}>
                            {value.label}
                          </Option>
                        );
                      })}
                    </Select>,
                  )}
                </Form.Item>
              </Col>
              {/* <Col span={8}>
                <Form.Item label={<span className={styles.formLabel}>是否风控</span>}>
                  {getFieldDecorator(
                    'isRisk',
                    {},
                  )(
                    <Select placeholder="请选择" allowClear style={{ width: 202 }}>
                      <Option value={1}>是</Option>
                      <Option value={0}>否</Option>
                    </Select>,
                  )}
                </Form.Item>
              </Col> */}
              <Col span={8}>
                <Form.Item
                  label={<span className={styles.formLabel}>{notRepurchasedLabel()}</span>}
                >
                  {getFieldDecorator(
                    'notRepurchased',
                    {},
                  )(
                    <Select placeholder="请选择" allowClear style={{ width: 202 }}>
                      <Option value={true}>是</Option>
                    </Select>,
                  )}
                </Form.Item>
              </Col>

              <Col span={16}>
                <Form.Item label={<span className={styles.formLabel}>创建时间</span>}>
                  {getFieldDecorator('createDate', {
                    initialValue: initialValue,
                  })(<RangePicker />)}
                </Form.Item>
              </Col>
            </Row>

            <div>
              <Form.Item>
                <Button loading={this.state.queryLoad} type="primary" htmlType="submit">
                  查询
                </Button>
              </Form.Item>
              <Form.Item>
                <Button htmlType="button" onClick={this.handleReset}>
                  重置
                </Button>
              </Form.Item>
              <Form.Item>
                <Button onClick={this.newexp}>导出</Button>
              </Form.Item>
              {/* <Form.Item>
                <Button onClick={this.ziti}>一键自提</Button>
              </Form.Item> */}
              {
                <Form.Item>
                  {user.job && user.job === 1 ? (
                    <Button
                      style={{ color: '#ffffff', backgroundColor: '#1890ff' }}
                      onClick={this.lingqu}
                    >
                      分配订单
                    </Button>
                  ) : (
                    <Button
                      style={{ color: '#ffffff', backgroundColor: '#1890ff' }}
                      onClick={this.lingqu}
                    >
                      认领订单
                    </Button>
                  )}
                </Form.Item>
              }
            </div>

            {/*  <UserRating form={this.props.form} /> */}
            <div></div>
          </Form>
          {this.renderEditModal()}
          {this.renderAddQdjeModal()}
          <Spin spinning={loading}>
            <MyPageTable
              scroll={true}
              onPage={this.onPage}
              paginationProps={paginationProps}
              dataSource={onTableData(allList)}
              columns={columns}
              rowSelection={rowSelection}
            />
          </Spin>
        </Card>
      </PageHeaderWrapper>
    );
  }
}
