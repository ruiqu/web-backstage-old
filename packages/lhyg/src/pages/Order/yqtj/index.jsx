import React, { Component } from 'react';
import {
  Card, Badge, Row, Col, Descriptions, Icon, Spin, Table, Form,Select ,
  Switch, NavBar, Checkbox, Radio, Input, Tabs, TabBar, DatePicker, Divider, Button
} from 'antd';
const FormItem = Form.Item;
import { Line, Area, Liquid } from '@ant-design/charts';
import { RingProgress, Column } from '@ant-design/plots';
import MyPageTable from '@/components/MyPageTable';
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
import request from '@/services/baseService';
import { getTimeDistance } from '@/utils/utils';
import { router } from 'umi';
import { connect } from 'dva';
import moment from 'moment';
import { getParam } from '@/utils/utils';
@Form.create()
@connect(({ order }) => ({ ...order }))
//逾期的统计数据
export default class Yqtj extends Component {
  state = {
    mydb: [],
    statis: {},
    rangeTimeData: getTimeDistance('week'),
    StatisticsData: getTimeDistance('week'),
    createTimeStart: null,
    createTimeEnd: null,
    day: 7,
    channel:[],
    channel1:[],
    channel2:[],
    
    v转化统计: [],
    v成交排行: [],
    v订单排行: [],
    dayNum: 7,
    day7db: {},
    dataDat: {},
    loading: false,
    StaData: [],
    moneyData: [],

  };
  componentDidMount() {
    var day1 = new Date();
    day1.setTime(day1.getTime() - 24 * 60 * 60 * 1000);
    var s1 = day1.getFullYear() + '-' + (day1.getMonth() + 1) + '-' + day1.getDate();
    console.log(s1, 's1s1s1s1');
    console.log(this.state.rangeTimeData, 'rangeTimeData');
    const { dispatch } = this.props;



    if (getParam('code')) {
      router.push(`/user`);
    } else {

    }
    this.reqdb(0)

  }



f获取几天前时间(day){
  var now = new Date();
  var date = now.getDate();
  now.setDate(date - day);  
  var y = now.getFullYear();
  var m = (now.getMonth() + 1).toString().padStart(2, "0");
  var d = now.getDate().toString().padStart(2, "0");
  var ymd = y + "-" + m + "-" + d;
  return ymd
}

  reqdb(dayNum = 1) {
    this.setState({
      loading: true,
      dayNum
    })


    let db = {}

    db.createTimeEnd = this.f获取几天前时间(1) + " 23:59:59"
    db.createTimeStart = this.f获取几天前时间(7) + " 00:00:00"
    console.log("aaa时间为:",db)
    this.newreqdb(db)

  }
  newreqdb( values) {
    this.setState({
      loading: true,
    })
    values.istime=1
    request(`/hzsx/business/channel/getChannelDb`, values).then(res => {
      console.log("rcs111", res)

    }).catch(e => {
      this.setState({ loading: false, });
      console.log("aq获取异常了", e)
    })
  }



  // 跳转
  iconRouter(url) {
    router.push(url);
  }

  router2(index, ti) {
    if (!index) return
    let query = {}
    //return console.log("aaa",index,ti)
    if (ti) {
      query.ti = 1
      if (ti == 2) {
        return router.push({
          pathname: '/zz/zyz',
          query,
        });
      }
    }
    else query.status = index
    console.log("aaaa", query)
    router.push({
      pathname: '/Order/HomePage',
      query,
    });
  }


  pickGreatest(arr, num) {
    const sorter = (a, b) => b.val - a.val;
    const descendingCopy = arr.slice().sort(sorter);
    return descendingCopy.splice(0, num);
  }
  handleSubmit = e => {
    e && e.preventDefault();
    this.props.form.validateFields((err, values) => {
      console.log("点击了提交", values)
      if (values.createDate) {
        values.createTimeStart = moment(values.createDate[0]).format('YYYY-MM-DD') + " 00:00:00";
        values.createTimeEnd = moment(values.createDate[1]).format('YYYY-MM-DD') + " 23:59:59";
        console.log("点击了提交", values)
      } 
      this.newreqdb(values)
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const { stateList, card2List, mydb, statis, StatisticsData,channel2,
      loading, dayNum, v转化统计, v成交排行, v订单排行 } = this.state;
    let ym = 'https://rich-rent.oss-cn-hangzhou.aliyuncs.com/icon/'
    let configs = {}
    let total = mydb.length;

    const data = [
      {
        name: 'London',
        月份: 'Jan.',
        月均降雨量: 18.9,
      },
      {
        name: 'London',
        月份: 'Feb.',
        月均降雨量: 28.8,
      },
      {
        name: 'London',
        月份: 'Mar.',
        月均降雨量: 39.3,
      },
      {
        name: 'London',
        月份: 'Apr.',
        月均降雨量: 81.4,
      },
      {
        name: 'London',
        月份: 'May',
        月均降雨量: 47,
      },
      {
        name: 'London',
        月份: 'Jun.',
        月均降雨量: 20.3,
      },
      {
        name: 'London',
        月份: 'Jul.',
        月均降雨量: 24,
      },
      {
        name: 'London',
        月份: 'Aug.',
        月均降雨量: 35.6,
      },
      {
        name: 'Berlin',
        月份: 'Jan.',
        月均降雨量: 12.4,
      },
      {
        name: 'Berlin',
        月份: 'Feb.',
        月均降雨量: 23.2,
      },
      {
        name: 'Berlin',
        月份: 'Mar.',
        月均降雨量: 34.5,
      },
      {
        name: 'Berlin',
        月份: 'Apr.',
        月均降雨量: 99.7,
      },
      {
        name: 'Berlin',
        月份: 'May',
        月均降雨量: 52.6,
      },
      {
        name: 'Berlin',
        月份: 'Jun.',
        月均降雨量: 35.5,
      },
      {
        name: 'Berlin',
        月份: 'Jul.',
        月均降雨量: 37.4,
      },
      {
        name: 'Berlin',
        月份: 'Aug.',
        月均降雨量: 42.4,
      },
    ];

    const v订单排行设置 = {
      data: v订单排行,
      isGroup: true,
      xField: 'day',
      yField: 'val',
      seriesField: 'name',

      /** 设置颜色 */
      //color: ['#1ca9e6', '#f88c24'],

      /** 设置间距 */
      // marginRatio: 0.1,
      label: {
        // 可手动配置 label 数据标签位置
        position: 'middle',
        // 'top', 'middle', 'bottom'
        // 可配置附加的布局方法
        layout: [
          // 柱形图数据标签位置自动调整
          {
            type: 'interval-adjust-position',
          }, // 数据标签防遮挡
          {
            type: 'interval-hide-overlap',
          }, // 数据标签文颜色自动调整
          {
            type: 'adjust-color',
          },
        ],
      },
    };
    const v成交排行设置 = {
      data: v成交排行,
      isGroup: true,
      xField: 'day',
      yField: 'val',
      seriesField: 'name',

      /** 设置颜色 */
      //color: ['#1ca9e6', '#f88c24'],

      /** 设置间距 */
      // marginRatio: 0.1,
      label: {
        // 可手动配置 label 数据标签位置
        position: 'middle',
        // 'top', 'middle', 'bottom'
        // 可配置附加的布局方法
        layout: [
          // 柱形图数据标签位置自动调整
          {
            type: 'interval-adjust-position',
          }, // 数据标签防遮挡
          {
            type: 'interval-hide-overlap',
          }, // 数据标签文颜色自动调整
          {
            type: 'adjust-color',
          },
        ],
      },
    };
    const config = {
      data: v转化统计,
      isGroup: true,
      xField: 'day',
      yField: 'val',
      seriesField: 'name',

      /** 设置颜色 */
      //color: ['#1ca9e6', '#f88c24'],

      /** 设置间距 */
      // marginRatio: 0.1,
      label: {
        // 可手动配置 label 数据标签位置
        position: 'middle',
        // 'top', 'middle', 'bottom'
        // 可配置附加的布局方法
        layout: [
          // 柱形图数据标签位置自动调整
          {
            type: 'interval-adjust-position',
          }, // 数据标签防遮挡
          {
            type: 'interval-hide-overlap',
          }, // 数据标签文颜色自动调整
          {
            type: 'adjust-color',
          },
        ],
      },
    };

    const columns = [

      {
        title: '渠道名称',
        dataIndex: 'name',
        width: 100,
      },
      {
        title: '订单数量',
        dataIndex: 'v订单量',
        width: 120,
        defaultSortOrder: 'descend',
        sorter: (a, b) => a.v订单量 - b.v订单量,
      },
      {
        title: '下单人数',
        dataIndex: 'v下单人数',
        width: 120,
        sorter: (a, b) => a.v下单人数 - b.v下单人数,
      },
      {
        title: '待支付',
        dataIndex: 'v待支付',
        width: 80,
      },
      {
        title: '待审批',
        dataIndex: 'v待审批',
        width: 80,
      },
      {
        title: '待成交',
        dataIndex: 'v待成交',
        width: 80,
      },
      {
        title: '客户拒绝关单',
        dataIndex: 'v客户拒绝关单',
        width: 140,
      },
      {
        title: '转化率',
        //dataIndex: 'v转化率',
        width: 100,
        render: e => {
          if(!e.v成交量)return 0
          return ((e.v成交量 / e.v订单量) * 100).toFixed(2) + "%"
        },
      },
      {
        title: '逾期量',
        dataIndex: 'v逾期量',
        width: 100,
      },
      {
        title: '逾期率',
        //dataIndex: 'v转化率',
        width: 100,
        render: e => {
          if(!e.v成交量)return 0
          return ((e.v逾期量 / e.v成交量) * 100).toFixed(2) + "%"
        },
      },
      {
        title: '成交单数',
        dataIndex: 'v成交量',
        width: 120,
        sorter: (a, b) => a.v成交量 - b.v成交量,
      },
      {
        title: '成交金额',
        dataIndex: 'v成交金额',
        width: 100,
      },


    ];

    return (
      <div className="index">
        <Spin spinning={loading} delay={500}>
          <Row gutter={16} style={{ 'height': "400px" }}>
            <Col span={24} style={{ marginTop: 10, padding: 10, fontSize: 16, color: '#000000', 'font-weight': 'bold' }}>
              渠道数据<img src={ym + '椭圆形 2.png'}></img>
            </Col>
            <Col span={24}>
              <Card bordered={false}>
                <Form layout="inline" onSubmit={this.handleSubmit}>
                  <Row>
                    <Col span={8}>
                      <Form.Item label="渠道来源">
                        {getFieldDecorator('cid', {
                          // initialValue: '',
                        })(
                          <Select
                            placeholder="渠道来源"
                            allowClear
                            style={{ width: 180 }}
                          >
                            {this.state.channel2.map(value => {
                              return (
                                <Option value={value.id+''} key={value.id}>
                                  {value.name}
                                </Option>
                              );
                            })}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col span={8}>
                      <Form.Item label="">
                        {getFieldDecorator('createDate', {})(<RangePicker />)}
                      </Form.Item>
                    </Col>
                    <Col span={24}>

                      <Button disabled={dayNum == 7} type="primary" onClick={() => this.reqdb(7)}>
                        7天
                      </Button>
                      <Button disabled={dayNum == 15} type="primary" onClick={() => this.reqdb(15)}>
                        15天
                      </Button>
                      <Button disabled={dayNum == 30} type="primary" onClick={() => this.reqdb(30)}>
                        30天
                      </Button>
                      <Button type="primary" htmlType="submit">
                        查询
                      </Button>
                    </Col>
                  </Row>
                </Form>
                <div>
                </div>

                {/* <MyPageTable
                  paginationProps={paginationProps}
                  dataSource={mydb}
                  scroll
                  bordered
                  columns={columns}
                  //onPage={this.onChange}
                /> */}
                <div>
                  <Table
                    scroll={{ x: true, y: 450 }}
                    columns={columns}
                    dataSource={mydb}
                    pagination={false}
                  //onChange={this.onChange}
                  />
                </div>
              </Card>

            </Col>

           
          </Row>

        </Spin>
      </div>
    );
  }
}
