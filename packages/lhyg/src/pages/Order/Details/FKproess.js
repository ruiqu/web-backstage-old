import React, { Component } from 'react';
import request from '@/services/baseService';
import { Divider, Tooltip, Row, Col } from 'antd';
import styles from './index.less';

class FKproess extends Component {
  state = {
    FKproess: {},
    isShow: false,
    userOrderInfoDto: {},
    xyReport: {},
    xyReportC: {},
  };
  // componentWillReceiveProps(nextProps) {
  //   if (nextProps.userOrderInfoDto !== this.props.userOrderInfoDto) {
  //     // sth值发生改变下一步工作
  //     request(
  //       '/zyj-api-web/hzsx/liteUserOrders/queryRiskReport',
  //       {
  //         orderId: nextProps.orderId,
  //         uid: nextProps.userOrderInfoDto.uid,
  //       },
  //       'post',
  //     ).then(res => {
  //       let data = eval(' (' + res + ')');
  //       if (!data) return;

  //       // data[1].court_risk_info_list.push({
  //       //   sort_time_string: '2019年3月31日',
  //       //   data_type: 'cpws',
  //       //   summary: '法院：上海市浦东新区人民法院 案号：（2017）沪0115民初1***9号',
  //       //   compatibility: '0.1',
  //       // });
  //       // data[1].court_risk_info_list.push({
  //       //   sort_time_string: '2019年3月31日',
  //       //   data_type: 'cpws',
  //       //   summary: '法院：上海市浦东新区人民法院 案号：（2017）沪0115民初1***9号',
  //       //   compatibility: '0.1',
  //       // });
  //       // data[1].court_risk_info_list.push({
  //       //   sort_time_string: '2019年3月31日',
  //       //   data_type: 'cpws',
  //       //   summary: '法院：上海市浦东新区人民法院 案号：（2017）沪0115民初1***9号',
  //       //   compatibility: '0.1',
  //       // });
  //       this.setState({
  //         FKproess: data,
  //         isShow: true,
  //       });
  //       // console.log('风控评分报告', data);
  //     });
  //   }
  // }

  componentDidMount() {
    const { orderId, userOrderInfoDto } = this.props;

    request(
      '/zyj-api-web/hzsx/liteUserOrders/queryRiskReport',
      {
        // uid: '479930f6ed3533ff3e682e0984adc5dabe8c0728lg4tnpzzvhdo',
        // orderId: '000OI20231219-8845728687937117777',
        orderId: orderId,
        uid: userOrderInfoDto.uid,
      },
      'post',
    ).then(res => {
      if (res.type && res.result) {
        if (res.type === '26') {
          console.log('风控报告', eval(' (' + res.result + ')'));
          let data = eval(' (' + res.result + ')');
          if (!data) return;
          this.setState({
            FKproess: data,
            FKType: res.type,
            isShow: true,
          });
        }
        if (res.type === '27') {
          let data = eval(' (' + res.result + ')');
          console.log('风控报告', data);
          if (!data) return;
          this.setState({
            FKproess: data,
            FKType: res.type,
            isShow: true,
            xyReport: data.raw_report_xy,
            xyReportC: data.raw_report_xy_c,
          });
        }
      }

      // data[1].court_risk_info_list.push({
      //   sort_time_string: '2019年3月31日',
      //   data_type: 'cpws',
      //   summary: '法院：上海市浦东新区人民法院 案号：（2017）沪0115民初1***9号',
      //   compatibility: '0.1',
      // });
      // data[1].court_risk_info_list.push({
      //   sort_time_string: '2019年3月31日',
      //   data_type: 'cpws',
      //   summary: '法院：上海市浦东新区人民法院 案号：（2017）沪0115民初1***9号',
      //   compatibility: '0.1',
      // });
      // data[1].court_risk_info_list.push({
      //   sort_time_string: '2019年3月31日',
      //   data_type: 'cpws',
      //   summary: '法院：上海市浦东新区人民法院 案号：（2017）沪0115民初1***9号',
      //   compatibility: '0.1',
      // });
    });
  }

  render() {
    /**身份信息核验--实名核验 */
    const identElementsRaw = {
      '01': '不一致',
      '02': '一致',
    };
    /**身份信息核验--运营商合演 */
    const networkElemetnRaw = {
      '01': '不一致',
      '02': '一致',
    };
    /**身份信息核验--运营商在网时长 */
    const timeOnlineRaw = {
      '1': '[0,3)',
      '2': '[3,6)',
      '3': '[6,12)',
      '4': '[12,24)',
      '5': '[24,+]',
      '0': '查无此号或已注销',
      '-1': '不支持该运营商',
      '-999': '手机状态异常（注:1、销号6月以上；2、携号转网；3、未放出去的号码）；非本网手机号码',
    };
    /**法院风险信息--类型 */
    const FYcourtDataType = {
      cpws: '裁判文书',
      ktgg: '开庭公告',
      ajlc: '案件流程信息',
      fygg: '法院公告',
      shixin: '失信公告',
      zxgg: '执行公告',
      bgt: '曝光台',
    };

    const newInfoRender = () => {
      return (
        <div>
          <div>
            <div className={styles.box}>
              <div className={styles.box5}>
                <div className={styles.firstBox}>
                  <div className={styles.firstBoxTitle}>申请准⼊分：</div>
                  <div className={styles.firstBoxNumber}>
                    {this.state.xyReport?.apply_report_detail?.A22160001}
                  </div>
                </div>
              </div>
              <div className={styles.box5}>
                <div className={styles.firstBox}>
                  <div className={styles.firstBoxTitle}>申请命中机构数：</div>
                  <div className={styles.firstBoxNumber}>
                    {this.state.xyReport?.apply_report_detail?.A22160003}
                  </div>
                </div>
              </div>
              <div className={styles.box5}>
                <div className={styles.firstBox}>
                  <div className={styles.firstBoxTitle}>申请准入置信度：</div>
                  <div className={styles.firstBoxNumber}>
                    {this.state.xyReport?.apply_report_detail?.A22160002}
                  </div>
                </div>
              </div>
              <div className={styles.box5}>
                <div className={styles.firstBox}>
                  <div className={styles.firstBoxTitle}>申请命中消⾦类机构数：</div>
                  <div className={styles.firstBoxNumber}>
                    {this.state.xyReport?.apply_report_detail?.A22160004}
                  </div>
                </div>
              </div>
              <div className={styles.box5}>
                <div className={styles.firstBox}>
                  <div className={styles.firstBoxTitle}>申请命中⽹络贷款类机构数：</div>
                  <div className={styles.firstBoxNumber}>
                    {this.state.xyReport?.apply_report_detail?.A22160005}
                  </div>
                </div>
              </div>
            </div>

            <div className={styles.box}>
              <div className={styles.box5}>
                <div className={styles.firstBox}>
                  <div className={styles.firstBoxTitle}>最近⼀次查询时间：</div>
                  <div className={styles.firstBoxNumber}>
                    {this.state.xyReport?.apply_report_detail?.A22160007}
                  </div>
                </div>
              </div>
              <div className={styles.box5}>
                <div className={styles.firstBox}>
                  <div className={styles.firstBoxTitle}>近1个⽉机构总查询笔数：</div>
                  <div className={styles.firstBoxNumber}>
                    {this.state.xyReport?.apply_report_detail?.A22160008}
                  </div>
                </div>
              </div>
              <div className={styles.box5}>
                <div className={styles.firstBox}>
                  <div className={styles.firstBoxTitle}>近3个⽉机构总查询笔数：</div>
                  <div className={styles.firstBoxNumber}>
                    {this.state.xyReport?.apply_report_detail?.A22160009}
                  </div>
                </div>
              </div>
              <div className={styles.box5}>
                <div className={styles.firstBox}>
                  <div className={styles.firstBoxTitle}>近6个⽉机构总查询笔数：</div>
                  <div className={styles.firstBoxNumber}>
                    {this.state.xyReport?.apply_report_detail?.A22160010}
                  </div>
                </div>
              </div>
              <div className={styles.box5}>
                <div className={styles.firstBox}>
                  <div className={styles.firstBoxTitle}>机构总查询次数：</div>
                  <div className={styles.firstBoxNumber}>
                    {this.state.xyReport?.apply_report_detail?.A22160006}
                  </div>
                </div>
              </div>
            </div>

            <div className={styles.box}>
              <div className={styles.box7}>
                <div className={styles.firstBox}>
                  <div className={styles.firstBoxTitle}>贷款行为分：</div>
                  <div className={styles.firstBoxNumber}>
                    {this.state.xyReport?.behavior_report_detail?.B22170001}
                  </div>
                </div>
              </div>
              <div className={styles.box7}>
                <div className={styles.firstBox}>
                  <div className={styles.firstBoxTitle}>信⽤贷款时⻓：</div>
                  <div className={styles.firstBoxNumber}>
                    {this.state.xyReport?.behavior_report_detail?.B22170053}
                  </div>
                </div>
              </div>
              <div className={styles.box7}>
                <div className={styles.firstBox}>
                  <div className={styles.firstBoxTitle}>最近⼀次贷款放款时间：</div>
                  <div className={styles.firstBoxNumber}>
                    {this.state.xyReport?.behavior_report_detail?.B22170054}
                  </div>
                </div>
              </div>
              <div className={styles.box7}>
                <div className={styles.firstBox}>
                  <div className={styles.firstBoxTitle}>最近⼀次履约距今天数：</div>
                  <div className={styles.firstBoxNumber}>
                    {this.state.xyReport?.behavior_report_detail?.B22170050}
                  </div>
                </div>
              </div>
              <div className={styles.box7}>
                <div className={styles.firstBox}>
                  <div className={styles.firstBoxTitle}>正常还款订单数占/贷款总订单数⽐例：</div>
                  <div className={styles.firstBoxNumber}>
                    {this.state.xyReport?.behavior_report_detail?.B22170034}
                  </div>
                </div>
              </div>
              <div className={styles.box7}>
                <div className={styles.firstBox}>
                  <div className={styles.firstBoxTitle}>贷款已结清订单数：</div>
                  <div className={styles.firstBoxNumber}>
                    {this.state.xyReport?.behavior_report_detail?.B22170052}
                  </div>
                </div>
              </div>
              <div className={styles.box7}>
                <div className={styles.firstBox}>
                  <div className={styles.firstBoxTitle}>贷款行为置信度：</div>
                  <div className={styles.firstBoxNumber}>
                    {this.state.xyReport?.behavior_report_detail?.B22170051}
                  </div>
                </div>
              </div>
            </div>

            <div className={styles.box}>
              <div className={styles.box4}>
                <div className={styles.firstBox}>
                  <div className={styles.firstBoxTitle}>近12个⽉贷款⾦额/在1k及以下的笔数：</div>
                  <div className={styles.firstBoxNumber}>
                    {this.state.xyReport?.behavior_report_detail?.B22170012}
                  </div>
                </div>
              </div>
              <div className={styles.box4}>
                <div className={styles.firstBox}>
                  <div className={styles.firstBoxTitle}>近12个⽉贷款⾦额/在1k-3k的笔数：</div>
                  <div className={styles.firstBoxNumber}>
                    {this.state.xyReport?.behavior_report_detail?.B22170013}
                  </div>
                </div>
              </div>
              <div className={styles.box4}>
                <div className={styles.firstBox}>
                  <div className={styles.firstBoxTitle}>近12个⽉贷款⾦额/在3k-10k的笔数：</div>
                  <div className={styles.firstBoxNumber}>
                    {this.state.xyReport?.behavior_report_detail?.B22170014}
                  </div>
                </div>
              </div>
              <div className={styles.box4}>
                <div className={styles.firstBox}>
                  <div className={styles.firstBoxTitle}>近12个⽉贷款⾦额/在1w以上的笔数：</div>
                  <div className={styles.firstBoxNumber}>
                    {this.state.xyReport?.behavior_report_detail?.B22170015}
                  </div>
                </div>
              </div>
            </div>

            <div className={styles.box}>
              <div className={styles.box4}>
                <div className={styles.firstBox}>
                  <div className={styles.firstBoxTitle}>近12个⽉消⾦类贷款机构数：</div>
                  <div className={styles.firstBoxNumber}>
                    {this.state.xyReport?.behavior_report_detail?.B22170021}
                  </div>
                </div>
              </div>
              <div className={styles.box4}>
                <div className={styles.firstBox}>
                  <div className={styles.firstBoxTitle}>近24个⽉消⾦类贷款机构数：</div>
                  <div className={styles.firstBoxNumber}>
                    {this.state.xyReport?.behavior_report_detail?.B22170022}
                  </div>
                </div>
              </div>
              <div className={styles.box4}>
                <div className={styles.firstBox}>
                  <div className={styles.firstBoxTitle}>近12个⽉⽹贷类贷款机构数：</div>
                  <div className={styles.firstBoxNumber}>
                    {this.state.xyReport?.behavior_report_detail?.B22170023}
                  </div>
                </div>
              </div>
              <div className={styles.box4}>
                <div className={styles.firstBox}>
                  <div className={styles.firstBoxTitle}>近24个⽉⽹贷类贷款机构数：</div>
                  <div className={styles.firstBoxNumber}>
                    {this.state.xyReport?.behavior_report_detail?.B22170024}
                  </div>
                </div>
              </div>
            </div>

            <div className={styles.second}>
              <div className={styles.secondTitle}>近期贷款行为</div>

              <div
                className={styles.box}
                style={{ background: '#3c7df7', borderRadius: '10px 10px 0 0 ' }}
              >
                <div className={styles.secondLabel}>行为时间</div>
                <div className={styles.secondLabel}>贷款笔数</div>
                <div className={styles.secondLabel}>贷款总金额</div>
                <div className={styles.secondLabel}>贷款机构</div>
                <div className={styles.secondLabel}>失败扣款笔数</div>
                <div className={styles.secondLabel}>履约贷款总金额</div>
                <div className={styles.secondLabel}>履约贷款次数</div>
              </div>
              <div className={styles.box} style={{ margin: 0 }}>
                <div className={styles.secondLabel2}>近1个月</div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170002}
                </div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170007}
                </div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170016}
                </div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170035}
                </div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170040}
                </div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170045}
                </div>
              </div>
              <div className={styles.box} style={{ margin: 0 }}>
                <div className={styles.secondLabel2}>近3个月</div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170003}
                </div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170008}
                </div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170017}
                </div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170036}
                </div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170041}
                </div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170046}
                </div>
              </div>
              <div className={styles.box} style={{ margin: 0 }}>
                <div className={styles.secondLabel2}>近6个月</div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170004}
                </div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170009}
                </div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170018}
                </div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170037}
                </div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170042}
                </div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170047}
                </div>
              </div>
              <div className={styles.box} style={{ margin: 0 }}>
                <div className={styles.secondLabel2}>近12个月</div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170005}
                </div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B221700010}
                </div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170019}
                </div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170038}
                </div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170043}
                </div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170048}
                </div>
              </div>
              <div className={styles.box} style={{ margin: 0 }}>
                <div className={styles.secondLabel2}>近24个月</div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170006}
                </div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B221700011}
                </div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170020}
                </div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170039}
                </div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170044}
                </div>
                <div className={styles.secondLabel2}>
                  {this.state.xyReport?.behavior_report_detail?.B22170049}
                </div>
              </div>
            </div>

            <div className={styles.thirdly}>
              <div className={styles.thirdlyTitle}>近期贷款行为</div>

              <div style={{ display: 'flex' }}>
                <div className={styles.thirdlyGrid}>
                  <div>
                    近6个⽉M0+逾期贷款笔数：
                    {this.state.xyReport?.behavior_report_detail?.B22170025}
                  </div>
                  <div>
                    近6个⽉M1+逾期贷款笔数：
                    {this.state.xyReport?.behavior_report_detail?.B22170028}
                  </div>
                  <div>
                    近6个⽉累积逾期贷款金额:{' '}
                    {this.state.xyReport?.behavior_report_detail?.B22170031}
                  </div>
                </div>

                <div className={styles.thirdlyGrid}>
                  <div>
                    近12个⽉M0+逾期贷款笔数：
                    {this.state.xyReport?.behavior_report_detail?.B22170026}
                  </div>
                  <div>
                    近12个⽉M1+逾期贷款笔数：
                    {this.state.xyReport?.behavior_report_detail?.B22170029}
                  </div>
                  <div>
                    近12个⽉累积逾期贷款金额:{' '}
                    {this.state.xyReport?.behavior_report_detail?.B22170032}
                  </div>
                </div>

                <div className={styles.thirdlyGrid}>
                  <div>
                    近24个⽉M0+逾期贷款笔数：
                    {this.state.xyReport?.behavior_report_detail?.B22170027}
                  </div>
                  <div>
                    近24个⽉M1+逾期贷款笔数：
                    {this.state.xyReport?.behavior_report_detail?.B22170030}
                  </div>
                  <div>
                    近24个⽉累积逾期贷款金额:{' '}
                    {this.state.xyReport?.behavior_report_detail?.B22170033}
                  </div>
                </div>
              </div>
            </div>

            <div className={styles.second}>
              <div className={styles.secondTitle}>信用详情</div>

              <div className={styles.box}>
                <div className={styles.box5}>
                  <div className={styles.firstBox2}>
                    <div className={styles.firstBoxTitle}>⽹贷授信额度</div>
                    <div className={styles.firstBoxNumber}>
                      {this.state.xyReport?.current_report_detail?.C22180001}
                    </div>
                  </div>
                </div>

                <div className={styles.box5}>
                  <div className={styles.firstBox2}>
                    <div className={styles.firstBoxTitle}>⽹贷额度置信度</div>
                    <div className={styles.firstBoxNumber}>
                      {this.state.xyReport?.current_report_detail?.C22180002}
                    </div>
                  </div>
                </div>
                <div className={styles.box5}>
                  <div className={styles.firstBox2}>
                    <div className={styles.firstBoxTitle}>⽹络贷款类机构数</div>
                    <div className={styles.firstBoxNumber}>
                      {this.state.xyReport?.current_report_detail?.C22180003}
                    </div>
                  </div>
                </div>
                <div className={styles.box5}>
                  <div className={styles.firstBox2}>
                    <div className={styles.firstBoxTitle}>⽹络贷款类产品数</div>
                    <div className={styles.firstBoxNumber}>
                      {this.state.xyReport?.current_report_detail?.C22180004}
                    </div>
                  </div>
                </div>
                <div className={styles.box5}>
                  <div className={styles.firstBox2}>
                    <div className={styles.firstBoxTitle}>⽹络贷款机构最⼤授信额度</div>
                    <div className={styles.firstBoxNumber}>
                      {this.state.xyReport?.current_report_detail?.C22180005}
                    </div>
                  </div>
                </div>
                <div className={styles.box5}>
                  <div className={styles.firstBox2}>
                    <div className={styles.firstBoxTitle}>⽹络贷款机构平均授信额度</div>
                    <div className={styles.firstBoxNumber}>
                      {this.state.xyReport?.current_report_detail?.C22180006}
                    </div>
                  </div>
                </div>
              </div>

              <div className={styles.box} style={{ marginTop: '10px' }}>
                <div className={styles.box5}>
                  <div className={styles.firstBox2}>
                    <div className={styles.firstBoxTitle}>消⾦贷款类机构数</div>
                    <div className={styles.firstBoxNumber}>
                      {this.state.xyReport?.current_report_detail?.C22180007}
                    </div>
                  </div>
                </div>

                <div className={styles.box5}>
                  <div className={styles.firstBox2}>
                    <div className={styles.firstBoxTitle}>消⾦贷款类产品数</div>
                    <div className={styles.firstBoxNumber}>
                      {this.state.xyReport?.current_report_detail?.C22180008}
                    </div>
                  </div>
                </div>
                <div className={styles.box5}>
                  <div className={styles.firstBox2}>
                    <div className={styles.firstBoxTitle}>消⾦贷款类机构最⼤授信额度</div>
                    <div className={styles.firstBoxNumber}>
                      {this.state.xyReport?.current_report_detail?.C22180009}
                    </div>
                  </div>
                </div>
                <div className={styles.box5}>
                  <div className={styles.firstBox2}>
                    <div className={styles.firstBoxTitle}>消⾦贷款类机构平均授信额度</div>
                    <div className={styles.firstBoxNumber}>
                      {this.state.xyReport?.current_report_detail?.C22180010}
                    </div>
                  </div>
                </div>
                <div className={styles.box5}>
                  <div className={styles.firstBox2}>
                    <div className={styles.firstBoxTitle}>消⾦建议授信额度</div>
                    <div className={styles.firstBoxNumber}>
                      {this.state.xyReport?.current_report_detail?.C22180011}
                    </div>
                  </div>
                </div>
                <div className={styles.box5}>
                  <div className={styles.firstBox2}>
                    <div className={styles.firstBoxTitle}>消⾦额度置信度</div>
                    <div className={styles.firstBoxNumber}>
                      {this.state.xyReport?.current_report_detail?.C22180012}
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className={styles.second}>
              <div className={styles.secondTitle}>近期逾期还款表现</div>

              <div className={styles.box}>
                <div className={styles.box5}>
                  <div className={styles.firstBox2}>
                    <div className={styles.firstBoxTitle}>最⼤逾期⾦额</div>
                    <div className={styles.firstBoxNumber}>
                      {this.state.xyReportC?.max_overdue_amt || '-'}
                    </div>
                  </div>
                </div>
                <div className={styles.box5}>
                  <div className={styles.firstBox2}>
                    <div className={styles.firstBoxTitle}>最⻓逾期天数</div>
                    <div className={styles.firstBoxNumber}>
                      {this.state.xyReportC?.max_overdue_days || '-'}
                    </div>
                  </div>
                </div>
                <div className={styles.box5}>
                  <div className={styles.firstBox2}>
                    <div className={styles.firstBoxTitle}>最近逾期时间</div>
                    <div className={styles.firstBoxNumber}>
                      {this.state.xyReportC?.latest_overdue_time || '-'}
                    </div>
                  </div>
                </div>
                <div className={styles.box5}>
                  <div className={styles.firstBox2}>
                    <div className={styles.firstBoxTitle}>最⼤履约⾦额</div>
                    <div className={styles.firstBoxNumber}>
                      {this.state.xyReportC?.max_performance_amt || '-'}
                    </div>
                  </div>
                </div>
                <div className={styles.box5}>
                  <div className={styles.firstBox2}>
                    <div className={styles.firstBoxTitle}>最近履约时间</div>
                    <div className={styles.firstBoxNumber}>
                      {this.state.xyReportC?.latest_performance_time || '-'}
                    </div>
                  </div>
                </div>
              </div>

              <div className={styles.box} style={{ marginTop: '10px' }}>
                <div className={styles.box5}>
                  <div className={styles.firstBox2}>
                    <div className={styles.firstBoxTitle}>履约笔数</div>
                    <div className={styles.firstBoxNumber}>
                      {this.state.xyReportC?.count_performance || '-'}
                    </div>
                  </div>
                </div>
                <div className={styles.box5}>
                  <div className={styles.firstBox2}>
                    <div className={styles.firstBoxTitle}>当前逾期机构数</div>
                    <div className={styles.firstBoxNumber}>
                      {this.state.xyReportC?.currently_overdue || '-'}
                    </div>
                  </div>
                </div>

                <div className={styles.box5}>
                  <div className={styles.firstBox2}>
                    <div className={styles.firstBoxTitle}>当前履约机构数</div>
                    <div className={styles.firstBoxNumber}>
                      {this.state.xyReportC?.currently_performance || '-'}
                    </div>
                  </div>
                </div>

                <div className={styles.box5}>
                  <div className={styles.firstBox2}>
                    <div className={styles.firstBoxTitle}>异常还款机构数</div>
                    <div className={styles.firstBoxNumber}>
                      {this.state.xyReportC?.acc_exc || '-'}
                    </div>
                  </div>
                </div>

                <div className={styles.box5}>
                  <div className={styles.firstBox2}>
                    <div className={styles.firstBoxTitle}>睡眠机构数</div>
                    <div className={styles.firstBoxNumber}>
                      {this.state.xyReportC?.acc_sleep || '-'}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    };
    const { FKproess, isShow, FKType } = this.state;
    return <div>{FKType == '26' ? '' : newInfoRender()}</div>;
  }
}

export default FKproess;
