/**
 * 加载风险反欺诈信息数据
 */
import request from "../../../../services/baseService"

/**
* 加载风险反欺诈信息数据
* @param {*} orderId : 订单ID
* @returns Object
*/
export const fetchRiskErrHandler = orderId => {
  if (!orderId) return Promise.reject({})
  return request(`/hzsx/antiFraud/queryAntiFraudByOrderId?orderId=${orderId}`, {}, "get").then(data => {
    return data || {}
  })
}