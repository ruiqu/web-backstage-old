import React, { PureComponent, Fragment } from 'react';
import './credit.less';
import styles from './creditDetailOld.less';
import { RiskReportHisttoryRent } from 'zwzshared'
import { Card, Icon, Table, Divider, Tag } from 'antd';

//信用分详情

class CreditDetailOld extends PureComponent {
  state = {
    creditInfo: this.props.creditInfo || {},
    userOrders: this.props.userOrders || {},
    scoreTableColumns1:
      this.props.creditInfo &&
      this.props.creditInfo.verifyResult &&
      this.props.creditInfo.verifyResult.refuse_details
        ? [
            {
              title: '综合评分',
              dataIndex: 'score',
              key: 'score',
              align: 'center',
            },
            {
              title: '拒绝原因',
              dataIndex: 'refuse_details',
              key: 'refuse_details',
              align: 'center',
            },
          ]
        : [
            {
              title: '综合评分',
              dataIndex: 'score',
              key: 'score',
              align: 'center',
            },
            {
              title: '审批建议',

              key: 'suggest',
              align: 'center',
              render: () => {
                return <div>{this.props.comprehensiveSuggest}</div>;
              },
            },
            // {
            //   title: '命中风险标注',
            //   dataIndex: 'refuse_details',
            //   key: 'refuse_details',
            // },
            {
              title: '分值标注说明',
              dataIndex: 'detail',
              key: 'detail',
              render: text => (
                <div>
                  <p>分值在（0-100）之间，得分越高，风险越大：</p>
                </div>
              ),
            },
          ],
    scoreTableData1: [],
    scoreTableColumns2: [
      {
        title: ' ',
        dataIndex: 'key',
        key: 'key',
      },
      {
        title: '近7天',
        dataIndex: 'day7',
        key: 'day7',
      },
      {
        title: '近30天',
        dataIndex: 'day30',
        key: 'day30',
      },
      {
        title: '近90天',
        dataIndex: 'day90',
        key: 'day90',
      },
    ],
    scoreTableData2: [],
    scoreTableColumns3: [
      {
        title: '消费分期类申请机构数(个)',
        dataIndex: 'num1',
        key: 'num1',
      },
      {
        title: '网络贷款类申请机构数(个)',
        dataIndex: 'num2',
        key: 'num2',
      },
      {
        title: '最近一次申请日期',
        dataIndex: 'last_apply_time',
        key: 'last_apply_time',
      },
      {
        title: '距离最近一次申请日期已有(天)',
        dataIndex: 'day',
        key: 'day',
      },
    ],
    scoreTableData3: [],
    scoreTableColumns4: [
      {
        title: ' ',
        dataIndex: 'key',
        key: 'key',
      },
      {
        title: '近1个月',
        dataIndex: 'month1',
        key: 'month1',
      },
      {
        title: '近3个月',
        dataIndex: 'month3',
        key: 'month3',
      },
      {
        title: '近6个月',
        dataIndex: 'month6',
        key: 'month6',
      },
      {
        title: '近12个月',
        dataIndex: 'month12',
        key: 'month12',
      },
    ],
    scoreTableData4: [],
    scoreTableColumns5: [
      {
        title: '消费分期类放款机构数(个)',
        dataIndex: 'num1',
        key: 'num1',
      },
      {
        title: '网络贷款类放款机构数(个)',
        dataIndex: 'num2',
        key: 'num2',
      },
      {
        title: '最近一次放款日期',
        dataIndex: 'last_apply_time',
        key: 'last_apply_time',
      },
      {
        title: '距离最近一次放款日期已有(天)',
        dataIndex: 'day',
        key: 'day',
      },
    ],
    scoreTableData5: [],
    scoreTableColumns6: [
      {
        title: ' ',
        dataIndex: 'key',
        key: 'key',
      },
      {
        title: '近1个月',
        dataIndex: 'month1',
        key: 'month1',
      },
      {
        title: '近3个月',
        dataIndex: 'month3',
        key: 'month3',
      },
      {
        title: '近6个月',
        dataIndex: 'month6',
        key: 'month6',
      },
      /* {
        title: '近12个月',
        dataIndex: 'month12',
        key: 'month12',
      }, */
    ],
    scoreTableData6: [],
    scoreTableColumns7: [
      {
        title: ' ',
        dataIndex: 'key',
        key: 'key',
      },
      {
        title: '近1个月',
        dataIndex: 'month1',
        key: 'month1',
      },
      {
        title: '近12个月',
        dataIndex: 'month12',
        key: 'month12',
      },
    ],
    scoreTableData7: [],
    scoreTableColumns8: [
      {
        title: '序号',
        dataIndex: 'index',
        key: 'index',
      },
      {
        title: '逾期金额区间(元)',
        dataIndex: 'overdue_money',
        key: 'overdue_money',
      },
      {
        title: '逾期时间',
        dataIndex: 'overdue_time',
        key: 'overdue_time',
      },
      {
        title: '逾期时长',
        dataIndex: 'overdue_day',
        key: 'overdue_day',
      },
      {
        title: '是否结清',
        dataIndex: 'settlement',
        key: 'settlement',
      },
    ],
    scoreTableData8: [],
    scoreTableColumns9: [
      {
        title: '序号',
        dataIndex: 'index',
        key: 'index',
      },
      {
        title: '审结日期',
        dataIndex: 'sort_time_string',
        key: 'sort_time_string',
      },
      {
        title: '类型',
        dataIndex: 'data_type',
        key: 'data_type',
      },
      {
        title: '摘要说明',
        dataIndex: 'summary',
        key: 'summary',
      },
      {
        title: '匹配度',
        dataIndex: 'compatibility',
        key: 'compatibility',
      },
    ],
    scoreTableData9: [],
  };
  componentDidMount() {
    const { creditInfo } = this.state;
    console.log(11, creditInfo);

    const scoreTableData8 = [];
    if (
      creditInfo.interfaceMultiplePlatformsV1 &&
      creditInfo.interfaceMultiplePlatformsV1.data_list
    ) {
      creditInfo.interfaceMultiplePlatformsV1.data_list.map((item, index) => {
        scoreTableData8.push({
          index: index + 1,
          overdue_money: item.overdue_money,
          overdue_time: item.overdue_time,
          overdue_day: item.overdue_day,
          settlement: item.settlement === 'Y' ? '是' : '否',
        });
      });
    }

    const scoreTableData9 = [];
    if (creditInfo.interfacePersonalLawList) {
      creditInfo.interfacePersonalLawList.map((item, index) => {
        scoreTableData9.push({
          index: index + 1,
          sort_time_string: item.sort_time_string,
          data_type: item.data_type,
          summary: item.summary,
          compatibility: item.compatibility,
        });
      });
    }

    this.setState({
      scoreTableData1: [
        {
          key: 1,
          score: creditInfo.verifyResult && creditInfo.verifyResult.score,
          suggest: creditInfo.verifyResult && creditInfo.verifyResult.suggest,
          refuse_details: creditInfo.verifyResult && creditInfo.verifyResult.refuse_details,
        },
      ],
      scoreTableData2: [
        {
          key: '一般消费分期平台',
          day7:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.common_amortize_platform_d7,
          day30:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.common_amortize_platform_d30,
          day90:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.common_amortize_platform_d90,
        },
        {
          key: '信用卡',
          day7:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.credit_card_d7,
          day30:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.credit_card_d30,
          day90:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.credit_card_d90,
        },
        {
          key: '大型消费金融公司',
          day7:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.large_finance_platform_d7,
          day30:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.large_finance_platform_d30,
          day90:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.large_finance_platform_d90,
        },
        {
          key: '其他类型公司',
          day7:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.other_platform_d7,
          day30:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.other_platform_d30,
          day90:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.other_platform_d90,
        },
        {
          key: 'P2P平台',
          day7:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.p2p_platform_d7,
          day30:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.p2p_platform_d30,
          day90:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.p2p_platform_d90,
        },
        {
          key: '小额贷款公司',
          day7:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.petty_loan_platform_d7,
          day30:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.petty_loan_platform_d30,
          day90:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.petty_loan_platform_d90,
        },
        {
          key: '申请查询总数',
          day7:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.platform_total_d7,
          day30:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.platform_total_d30,
          day90:
            creditInfo.interfaceBeforeAntifraud &&
            creditInfo.interfaceBeforeAntifraud.platform_total_d90,
        },
      ],
      scoreTableData3: [
        {
          key: 1,
          num1:
            (creditInfo.interfaceMultiplePlatformsV1 &&
              creditInfo.interfaceMultiplePlatformsV1.consumer_apply_mechanism_number !==
                undefined &&
              creditInfo.interfaceMultiplePlatformsV1.consumer_apply_mechanism_number + '') ||
            '----',
          num2:
            (creditInfo.interfaceMultiplePlatformsV1 &&
              creditInfo.interfaceMultiplePlatformsV1.network_loan_apply_number !== undefined &&
              creditInfo.interfaceMultiplePlatformsV1.network_loan_apply_number + '') ||
            '----',
          last_apply_time:
            (creditInfo.interfaceMultiplePlatformsV1 &&
              creditInfo.interfaceMultiplePlatformsV1.last_apply_time) ||
            '----',
          day:
            creditInfo.interfaceMultiplePlatformsV1 &&
            creditInfo.interfaceMultiplePlatformsV1.recent_apply_date_list &&
            creditInfo.interfaceMultiplePlatformsV1.recent_apply_date_list.length
              ? JSON.stringify(creditInfo.interfaceMultiplePlatformsV1.recent_apply_date_list)
              : '----',
        },
      ],
      scoreTableData4: [
        {
          key: '申请次数(次)',
          month1:
            creditInfo.interfaceMultiplePlatformsV1 &&
            creditInfo.interfaceMultiplePlatformsV1.apply_time1,
          month3:
            creditInfo.interfaceMultiplePlatformsV1 &&
            creditInfo.interfaceMultiplePlatformsV1.apply_time3,
          month6:
            creditInfo.interfaceMultiplePlatformsV1 &&
            creditInfo.interfaceMultiplePlatformsV1.apply_time6,
          month12:
            creditInfo.interfaceMultiplePlatformsV1 &&
            creditInfo.interfaceMultiplePlatformsV1.apply_time12,
        },
      ],
      scoreTableData5: [
        {
          key: 1,
          num1:
            (creditInfo.interfaceMultiplePlatformsV1 &&
              creditInfo.interfaceMultiplePlatformsV1.cflenders_12 !== undefined &&
              creditInfo.interfaceMultiplePlatformsV1.cflenders_12 + '') ||
            '----',
          num2:
            (creditInfo.interfaceMultiplePlatformsV1 &&
              creditInfo.interfaceMultiplePlatformsV1.nllenders_12 !== undefined &&
              creditInfo.interfaceMultiplePlatformsV1.nllenders_12 + '') ||
            '----',
          last_apply_time:
            (creditInfo.interfaceMultiplePlatformsV1 &&
              creditInfo.interfaceMultiplePlatformsV1.lend_time) ||
            '----',
          day:
            creditInfo.interfaceMultiplePlatformsV1 &&
            creditInfo.interfaceMultiplePlatformsV1.recent_lend_date_list &&
            creditInfo.interfaceMultiplePlatformsV1.recent_lend_date_list.length
              ? JSON.stringify(creditInfo.interfaceMultiplePlatformsV1.recent_lend_date_list)
              : '----',
        },
      ],
      scoreTableData6: [
        {
          key: '放款次数(次)',
          month1:
            creditInfo.interfaceMultiplePlatformsV1 &&
            creditInfo.interfaceMultiplePlatformsV1.lend_number1,
          month3:
            creditInfo.interfaceMultiplePlatformsV1 &&
            creditInfo.interfaceMultiplePlatformsV1.lend_number3,
          month6:
            creditInfo.interfaceMultiplePlatformsV1 &&
            creditInfo.interfaceMultiplePlatformsV1.lend_number6,
          // month12: creditInfo.interfaceMultiplePlatformsV1 && creditInfo.interfaceMultiplePlatformsV1.lend_number12,
        },
      ],
      scoreTableData7: [
        {
          key: '履约次数(次)',
          month1:
            creditInfo.interfaceMultiplePlatformsV1 &&
            creditInfo.interfaceMultiplePlatformsV1.repay_succ1,
          month12:
            creditInfo.interfaceMultiplePlatformsV1 &&
            creditInfo.interfaceMultiplePlatformsV1.repay_succ12,
        },
        {
          key: '还款异常次数(次)',
          month1:
            creditInfo.interfaceMultiplePlatformsV1 &&
            creditInfo.interfaceMultiplePlatformsV1.repay_fail1,
          month12:
            creditInfo.interfaceMultiplePlatformsV1 &&
            creditInfo.interfaceMultiplePlatformsV1.repay_fail12,
        },
      ],
      scoreTableData8,
      scoreTableData9,
    });
  }

  render() {
    const { creditInfo, userOrders } = this.state;
    const hitrotyRentObj = this.props.creditInfo?.mode_rent_history || {}
    return (
      <div className={'container ' + styles.creditDetailOldContainer}>
        {/* <h2 className="center">风险报告</h2> */}
  
        <div className={styles.containerDiv}>
          <div className={styles.titleHeader}>
            <span className={styles.titleHeaderSpan}>///</span> 风险分{' '}
            <span className={styles.titleHeaderSpan}>///</span>
          </div>
          <Table
            bordered
            rowKey={record => record.key}
            columns={this.state.scoreTableColumns1}
            dataSource={this.state.scoreTableData1}
            pagination={false}
          />
          {creditInfo.verifyResult && creditInfo.verifyResult.refuse_details ? (
            ''
          ) : (
            <Fragment>
              <Divider>基本信息</Divider>
              <div className={styles.floatHalf}>
                <span>
                  姓名：
                  <span>{userOrders.userName && userOrders.userName.substr(0, 1) + '**'}</span>
                </span>
                <span>
                  身份证号：
                  <span>
                    {userOrders.idCard &&
                      userOrders.idCard.substr(0, 3) +
                        '***********' +
                        userOrders.idCard.substr(14, 18)}
                  </span>
                </span>
                <span>
                  手机号：
                  <span>
                    {userOrders.telephone &&
                      userOrders.telephone.substr(0, 3) +
                        '****' +
                        userOrders.telephone.substr(7, 11)}
                  </span>
                </span>
                <span>
                  年龄：<span>{creditInfo.age}</span>
                </span>
                <span>
                  户籍：
                  <span>
                    {creditInfo.interfaceBeforeAntifraud &&
                      creditInfo.interfaceBeforeAntifraud.ident_number_address}
                  </span>
                </span>
                <span>
                  号码归属地：
                  <span>
                    {creditInfo.interfaceBeforeAntifraud &&
                      creditInfo.interfaceBeforeAntifraud.phone_address}
                  </span>
                </span>
              </div>
              <RiskReportHisttoryRent historyRentObj={hitrotyRentObj} />
              <Divider>风险名单监测</Divider>
              <div className={styles.floatHalf}>
                <span>
                  特殊关注名单：
                  {creditInfo.interfaceSpecialLevelList &&
                  creditInfo.interfaceSpecialLevelList.result_code_desc &&
                  !creditInfo.interfaceSpecialLevelList.result_code_desc.includes('未命中') ? (
                    <span className={styles.errorScore}>命中</span>
                  ) : (
                    <span>未命中</span>
                  )}
                </span>
                <span>
                  归属地位于高风险集中地区：
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.census_register_hign_risk_area
                  ) ? (
                    <span>未命中</span>
                  ) : (
                    <span className={styles.errorScore}>命中</span>
                  )}
                </span>
                <span>
                  法院失信名单：
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.court_break_faith_list
                  ) ? (
                    <span>未命中</span>
                  ) : (
                    <span className={styles.errorScore}>命中</span>
                  )}
                </span>
                <span>
                  犯罪通缉名单：
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.crime_manhunt_list
                  ) ? (
                    <span>未命中</span>
                  ) : (
                    <span className={styles.errorScore}>命中</span>
                  )}
                </span>
                <span>
                  法院执行名单：
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.court_execute_list
                  ) ? (
                    <span>未命中</span>
                  ) : (
                    <span className={styles.errorScore}>命中</span>
                  )}
                </span>
                <span>
                  助学贷款欠费名单：
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.student_loan_arrearage_list
                  ) ? (
                    <span>未命中</span>
                  ) : (
                    <span className={styles.errorScore}>命中</span>
                  )}
                </span>
                <span>
                  信贷逾期名单：
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.credit_overdue_list
                  ) ? (
                    <span>未命中</span>
                  ) : (
                    <span className={styles.errorScore}>命中</span>
                  )}
                </span>
                <span>
                  高风险关注名单：
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.hign_risk_focus_list
                  ) ? (
                    <span>未命中</span>
                  ) : (
                    <span className={styles.errorScore}>命中</span>
                  )}
                </span>
                <span>
                  车辆租赁违约名单：
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.car_rental_break_contract_list
                  ) ? (
                    <span>未命中</span>
                  ) : (
                    <span className={styles.errorScore}>命中</span>
                  )}
                </span>
                <span>
                  法院结案名单：
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.court_case_list
                  ) ? (
                    <span>未命中</span>
                  ) : (
                    <span className={styles.errorScore}>命中</span>
                  )}
                </span>
                <span>
                  故意违章乘车名单：
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.riding_break_contract_list
                  ) ? (
                    <span>未命中</span>
                  ) : (
                    <span className={styles.errorScore}>命中</span>
                  )}
                </span>
                <span>
                  欠税名单：
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.owing_taxes_list
                  ) ? (
                    <span>未命中</span>
                  ) : (
                    <span className={styles.errorScore}>命中</span>
                  )}
                </span>
                <span>
                  欠税公司法人代表名单：
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.owing_taxes_legal_person_list
                  ) ? (
                    <span>未命中</span>
                  ) : (
                    <span className={styles.errorScore}>命中</span>
                  )}
                </span>
                <span>
                  虚拟号码库：
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.virtual_number_base
                  ) ? (
                    <span>未命中</span>
                  ) : (
                    <span className={styles.errorScore}>命中</span>
                  )}
                </span>
                <span>
                  通讯小号库：
                  {!(
                    creditInfo.interfaceBeforeAntifraud &&
                    creditInfo.interfaceBeforeAntifraud.small_number_base
                  ) ? (
                    <span>未命中</span>
                  ) : (
                    <span className={styles.errorScore}>命中</span>
                  )}
                </span>
              </div>
              <Divider>机构查询记录</Divider>
              <Table
                bordered
                rowKey={record => record.key}
                columns={this.state.scoreTableColumns2}
                dataSource={this.state.scoreTableData2}
                pagination={false}
              />
              <Divider>历史借贷行为</Divider>
              <p>
                近12个月申请机构总数(个):
                {(creditInfo.interfaceMultiplePlatformsV1 &&
                  creditInfo.interfaceMultiplePlatformsV1.apply_platforms_m12) ||
                  0}
              </p>
              <Table
                bordered
                className={styles.marginBottom}
                rowKey={record => record.key}
                columns={this.state.scoreTableColumns3}
                dataSource={this.state.scoreTableData3}
                pagination={false}
              />
              <Table
                bordered
                className={styles.marginBottom}
                rowKey={record => record.key}
                columns={this.state.scoreTableColumns4}
                dataSource={this.state.scoreTableData4}
                pagination={false}
              />
              <p>
                近12个月放款机构总数(个):
                {(creditInfo.interfaceMultiplePlatformsV1 &&
                  creditInfo.interfaceMultiplePlatformsV1.lenders) ||
                  0}
              </p>
              <Table
                bordered
                className={styles.marginBottom}
                rowKey={record => record.key}
                columns={this.state.scoreTableColumns5}
                dataSource={this.state.scoreTableData5}
                pagination={false}
              />
              <Table
                bordered
                className={styles.marginBottom}
                rowKey={record => record.key}
                columns={this.state.scoreTableColumns6}
                dataSource={this.state.scoreTableData6}
                pagination={false}
              />
              <Table
                bordered
                className={styles.marginBottom}
                rowKey={record => record.key}
                columns={this.state.scoreTableColumns7}
                dataSource={this.state.scoreTableData7}
                pagination={false}
              />
              <Divider>历史逾期记录</Divider>
              <div className={styles.floatHalf}>
                <span>
                  近6个月逾期机构次数：
                  <span>
                    {creditInfo.interfaceMultiplePlatformsV1 &&
                      creditInfo.interfaceMultiplePlatformsV1.overdue_mechanism_number}
                  </span>
                </span>
                <span>
                  近6个月逾期总次数：
                  <span>
                    {creditInfo.interfaceMultiplePlatformsV1 &&
                      creditInfo.interfaceMultiplePlatformsV1.overdue_total_counts}
                  </span>
                </span>
                {/* <span>近6个月未结清逾期次数：<span>{creditInfo.interfaceMultiplePlatformsV1 && creditInfo.interfaceMultiplePlatformsV1.apply_platforms_m12}</span></span> */}
                <span>
                  近6个月逾期总金额(元)1：
                  <span>
                    {creditInfo.interfaceMultiplePlatformsV1 &&
                      creditInfo.interfaceMultiplePlatformsV1.overdue_total_money}
                  </span>
                </span>
              </div>
              <Table
                bordered
                rowKey={record => record.index}
                columns={this.state.scoreTableColumns8}
                dataSource={this.state.scoreTableData8}
                pagination={false}
              />
              <p className={styles.tips}>
                说明：S代表期数，1期=7天，s0表示不到7天，s1代表7-14天，以此类推；M代表期数，1期=30天，m0表示不到30天，m1表示30-60天，以此类推。
              </p>
              <Divider>关联风险检测</Divider>
              <div>
                <p>
                  3个月身份证关联手机号数(个)：
                  <span>
                    {creditInfo.interfaceBeforeAntifraud &&
                      creditInfo.interfaceBeforeAntifraud.ident_to_phone_counts}
                  </span>
                </p>
                <p>
                  3个月身份证关联身份证数(个)：
                  <span>
                    {creditInfo.interfaceBeforeAntifraud &&
                      creditInfo.interfaceBeforeAntifraud.phone_to_ident_counts}
                  </span>
                </p>
              </div>
              <Divider>法院风险信息</Divider>
              <Table
                bordered
                rowKey={record => record.index}
                columns={this.state.scoreTableColumns9}
                dataSource={this.state.scoreTableData9}
                pagination={false}
              />
            </Fragment>
          )}
        </div>
      </div>
    );
  }
}

export default CreditDetailOld;
