import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import {
  Card,
  Tooltip,
  Button,
  Form,
  Input,
  Select,
  DatePicker,
  Spin,
  Row,
  Col,
  Divider,
  Modal,
  Drawer,
  Timeline,
  InputNumber,
  Descriptions,
  Upload,
  Icon,
  message,
} from 'antd';
const { TextArea } = Input;
import { getToken } from '@/utils/localStorage';
import MyPageTable from '@/components/MyPageTable';
import { onTableData } from '@/utils/utils.js';
import CustomCard from '@/components/CustomCard';
import CopyToClipboard from '@/components/CopyToClipboard';
import { getTimeDistance, renderOrderStatus } from '@/utils/utils';
import moment from 'moment';
const { Option } = Select;

const { RangePicker } = DatePicker;
import { routerRedux } from 'dva/router';
import { getCurrentUser } from '@/utils/localStorage';
import request from '../../../services/baseService';
import { orderCloseStatusMap, orderStatusMap } from '@/utils/enum';
import { exportCenterHandler } from '../util';
import { UserRating } from 'zwzshared';
import { apiUrl } from '@/utils/platformConfig';
@connect(({ order, loading }) => ({
  ...order,
  loading: loading.effects['order/queryOpeOrderByConditionList'],
}))
@Form.create()
export default class HomePage extends Component {
  state = {
    current: 1,
    visible: '',
    uploadImgUrl: '',
    orderVisible: '',
    load: false,
    orderId: null,
    complainEventId: null,
    imgList: [],
    mydata: {},
    datas: {},
    record: {},
    mydb: [],
    expressList: [],
    yunTime: getTimeDistance('month'),
    gBtusua: '04', // 默认状态: 待发货
  };

  renderShowInfo() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const { orderVisible, orderId } = this.state;
    const { getFieldDecorator } = this.props.form;
    const handleOk = e => {
      e.preventDefault();
      this.props.form.validateFields(['cost_price'], (err, values) => {
        if (!err) {
          console.log('orderid:', orderId, values);
          values.orderId = orderId;
          values.costPrice = values.cost_price;
          request(`/hzsx/business/order/addcostPrice`, values).then(res => {
            console.log('rcs', res);
            this.setState({
              visible: false,
              orderVisible: false,
            });
            message.success('添加完成', 5);
          });
        }
      });
    };
    let wlList = [];
    return (
      <div>
        {/*  <Modal
          title="查看详情"
          visible={orderVisible === 'lookts'}
          onOk={handleOk}
          onCancel={this.handleCancel}
        >
          <Form>
            <Form.Item label="成本金额" {...formItemLayout}>
              {getFieldDecorator('cost_price', {
                rules: [{ required: true, message: '请输入成本金额' }],
              })(<InputNumber placeholder="请输入" />)}
            </Form.Item>
          </Form>
        </Modal> */}
        <Drawer
          width={420}
          title="查看详情"
          placement="right"
          onClose={this.handleCancel}
          visible={orderVisible === 'lookts'}
        >
          {orderVisible === 'lookts' ? (
            <>
              <Timeline>
                {wlList.map((item, idx) => {
                  let color = 'blue';
                  if (idx === 0) {
                    color = 'green';
                  }
                  return (
                    <Timeline.Item
                      style={{ color: idx !== 0 ? '#aaa' : '#333' }}
                      key={idx}
                      color={color}
                    >
                      <p>{item.context}</p>
                      <p>{item.ftime}</p>
                    </Timeline.Item>
                  );
                })}
              </Timeline>
            </>
          ) : null}
          <Card bordered={false}>
            <Form onSubmit={this.handleSubmit1}>
              <Form.Item label="处理情况" {...formItemLayout}>
                {getFieldDecorator('feedbackCode', {
                  rules: [{ required: true, message: '请选择结果' }],
                })(
                  <Select style={{ width: '100%' }} placeholder="请选择结果">
                    <Option value="00">使用体验保障金退款</Option>
                    <Option value="02">通过其他方式退款</Option>
                    <Option value="03">已发货</Option>
                    <Option value="04">已发货</Option>
                    <Option value="05">已发货</Option>
                    <Option value="06">非我方责任范围</Option>
                  </Select>,
                )}
              </Form.Item>
              <Form.Item label="反馈内容" {...formItemLayout}>
                {getFieldDecorator('feedbackContent', {
                  rules: [{ required: true, message: '请输入反馈内容' }],
                })(<TextArea placeholder="请输入" />)}
              </Form.Item>

              <div className="rowsdsjk12">
                <span className="labelsds121s">凭证：</span>
                <Upload
                  accept="image/*"
                  action= {`${apiUrl}/zyj-backstage-web/hzsx/busShop/doUpLoadwebp`}
                  listType="picture-card"
                  headers={{
                    Token: getToken(),
                  }}
                  fileList={this.state.imgList}
                  beforeUpload={this.beforeUpload}
                  onChange={this.handleUploadImage}
                >
                  {!this.state.imgList ||
                    (!this.state.imgList.length && (
                      <div>
                        <Icon type="upload" />
                        <div className="ant-upload-text">上传照片</div>
                      </div>
                    ))}
                </Upload>
              </div>
              <div className="rowsdsjk12">
                <Button htmlType="submit">确认</Button>
              </div>
            </Form>
          </Card>
        </Drawer>
      </div>
    );
  }

  // 上传完之后的处理方法
  handleUploadImage = ({ file, fileList }) => {
    if (file.status === 'done') {
      const images = fileList.map((v, index) => {
        if (v.response) {
          const src = v.response.data;
          return { uid: index, src: src, url: src, isMain: v.isMain || null };
        }
        return v;
      });
      const imgSrc = images && images[0] && images[0].url;
      this.setState({ uploadImgUrl: imgSrc });
    }
    this.setState({ imgList: fileList });
  };

  // 在上传之前进行处理
  beforeUpload = file => {
    const isJPG =
      file.type === 'image/jpeg' || file.type === 'image/png' || file.type === 'image/gif';
    if (!isJPG) {
      message.error('图片格式不正确');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('图片大于2MB');
    }
    return isJPG && isLt2M;
  };

  showOrderModal = (type, complainEventId) => {
    console.log('按钮点击', type, complainEventId);
    request('/hzsx/ts/findDetailed', { complainEventId }).then(res => {
      console.log('获取请求详情', res);

      //获取一下投诉的详情
      this.setState({
        orderVisible: type,
        complainEventId,
      });
    });
  };
  handleCancel = e => {
    this.setState({
      visible: false,
      orderVisible: false,
    });
  };

  componentDidMount() {
    const { status } = this.props.location.query;
    if (status) {
      this.setState(
        {
          gBtusua: status,
          load: true,
        },
        () => {
          this.props.form.setFieldsValue({
            status,
          });
          this.onList(1, 10, {
            status: this.state.gBtusua,
          });
          // this.getExpressList();
        },
      );
    } else {
      this.onList(1, 10);
      // this.getExpressList();
    }

    this.props.dispatch({
      type: 'order/PlateformChannel',
    });
  }
  onList = (pageNumber, pageSize, data = {}) => {
    request('/hzsx/ts/findList', {
      pageSize,
      pageNumber,
      ...data,
    }).then(res => {
      console.log('aaaa:', res);
      this.setState({
        mydata: res,
        load: false,
        //orderVisible:"lookts"
      });
    });
    const { dispatch } = this.props;
    /* dispatch({
      type: 'order/queryOpeOrderByConditionList',
      payload: {
        pageSize,
        pageNumber,
        iszy: 1,
        ...data,
      },
    }); */
  };
  // 重置
  handleReset = e => {
    this.setState(
      {
        gBtusua: '',
      },
      () => {
        this.props.form.resetFields();
        this.props.form.setFieldsValue({
          status: undefined,
        });
        this.handleSubmit(e);
      },
    );
  };
  handleSubmit1 = e => {
    e && e.preventDefault();
    this.props.form.validateFields((err, values) => {
      let img=this.state.imgList
      values.flieurl=img[0].response.data
      values.complainEventId=this.state.complainEventId
      request("/hzsx/ts/updateOrder",values).then(res=>{
        console.log("请求结果",res)
        message.success('处理成功');

      })
      console.log("values",values,img)
    });
  };

  handleSubmit = e => {
    e && e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (values.createDate && values.createDate.length < 1) {
        values.createTimeStart = '';
        values.createTimeEnd = '';
        this.setState(
          {
            datas: { ...values },
            current: 1,
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      } else if (values.createDate) {
        values.createTimeStart = moment(values.createDate[0]).format('YYYY-MM-DD HH:mm:ss');
        values.createTimeEnd = moment(values.createDate[1]).format('YYYY-MM-DD HH:mm:ss');
        this.setState(
          {
            datas: { ...values },
            current: 1,
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      } else {
        this.setState(
          {
            datas: { ...values },
            current: 1,
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      }
    });
  };

  onexport = () => {
    exportCenterHandler(this, 'exportRentOrder', true, false);
  };

  handleCancel = e => {
    this.setState({
      visible: false,
      orderVisible: false,
    });
  };
  //table 页数
  onPage = e => {
    this.setState(
      {
        current: e.current,
      },
      () => {
        const params = {
          ...this.state.datas,
        };
        this.onList(e.current, 10, params);
      },
    );
  };
  onChanges = e => {
    this.setState({
      gBtusua: e,
    });
  };

  // 认领订单
  lingqu = () => {
    // console.log("user",getCurrentUser());
    this.props.dispatch(
      routerRedux.push({
        pathname: `/Order/HomePage/ReceiveOrder`,
      }),
    );
  };

  render() {
    const { load, mydata } = this.state;
    const { loading, form, mydb } = this.props;
    const { cost, yj, payedRent, allRent, page } = mydata;
    console.log('page', page);
    let allTotal = mydata?.total;
    let allList = mydata?.records;

    console.log('mydata', mydata);
    const paginationProps = {
      current: this.state.current,
      pageSize: 10,
      total: allTotal,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / 10)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };

    const statusMap = {
      MERCHANT_PROCESSING: '商家处理中',
      MERCHANT_FEEDBACKED: '商家已反馈',
      FINISHED: '投诉已完结',
      CANCELLED: '投诉已撤销',
      PLATFORM_PROCESSING: '平台处理中',
      PLATFORM_FINISH: '平台处理完结',
      CLOSED: '系统关闭',
    };
    const user = getCurrentUser();
    const columns = [
      {
        title: '订单编号',
        dataIndex: 'orderId',
        width: 130,
        render: (text, record, index) => {
          return (
            <>
              <span style={{ color: 'red' }}>
                {record.children == null ? null : `(共${record.children.length + 1}单) `}
              </span>
              <span>{text}</span>
            </>
          );
        },
      },
      {
        title: '手机号',
        dataIndex: 'phoneNo',
        width: 120,
      },
      {
        title: '状态',
        width: 140,
        render: e => {
          return (
            <>
              <div>
                <p>{statusMap[e.status]}</p>
              </div>
            </>
          );
        },
      },

      {
        title: '商家订单号',
        dataIndex: 'merchantOrderNo',
        width: 120,
      },
      {
        title: '投诉时间',
        dataIndex: 'gmtModified',
        width: 120,
      },

      {
        title: '用户诉求',
        dataIndex: 'leafCategoryName',
        width: 100,
      },

      {
        title: '原因',
        dataIndex: 'complainReason',
        width: 120,
      },
      {
        title: '用户投诉内容',
        dataIndex: 'content',
        width: 120,
      },
      {
        title: '操作',
        width: 120,
        fixed: 'right',
        align: 'center',
        render: (e, record) => {
          return (
            <div>
              <a
                className="primary-color"
                // onClick={() => router.push(`/Order/Details?id=${e.orderId}`)}
                href={`#/Order/HomePage/Details?id=${e.orderId}`}
                target="_blank"
              >
                订单
              </a>
              <Divider type="vertical" />
              <a
                className="primary-color"
                onClick={() => {
                  this.showOrderModal('lookts', e.complainEventId);
                }}
                target="_blank"
              >
                查看
              </a>
            </div>
          );
        },
      },
    ];
    const { getFieldDecorator } = form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    return (
      <>
        <PageHeaderWrapper title={false}>
          {this.renderShowInfo()}
          <Card bordered={false}>
            <Spin spinning={load}>
              <MyPageTable
                scroll={true}
                onPage={this.onPage}
                paginationProps={paginationProps}
                dataSource={onTableData(allList)}
                columns={columns}
              />
            </Spin>
          </Card>
        </PageHeaderWrapper>
      </>
    );
  }
}
