import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import {
  Card,
  Tooltip,
  Button,
  Form,
  Input,
  Select,
  DatePicker,
  Spin,
  Row,
  Col,
  Divider,
  Modal,
  InputNumber,
  Descriptions,
  message,
  Drawer,
  Icon,
} from 'antd';

import MyStage from '@/components/MyStage'; //探针
import Tbxj from '@/components/Tbxj'; //探针
import MyPageTable from '@/components/MyPageTable';
import { onTableData } from '@/utils/utils.js';
import CustomCard from '@/components/CustomCard';
import CopyToClipboard from '@/components/CopyToClipboard';
import { getTimeDistance, renderOrderStatus, ddzq, dayMap } from '@/utils/utils';
import moment from 'moment';
const { Option } = Select;

const { RangePicker } = DatePicker;
import { routerRedux } from 'dva/router';
import { getCurrentUser } from '@/utils/localStorage';
import request from '../../../services/baseService';
import { orderCloseStatusMap, orderStatusMap } from '@/utils/enum';
import { exportCenterHandler } from '../util';
import { UserRating } from 'zwzshared';
import DescriptionsItem from '@/components/DescriptionsItem';

@connect(({ order, loading }) => ({
  ...order,
  loading: loading.effects['order/queryOpeOrderByConditionList'],
}))
@Form.create()
export default class HomePage extends Component {
  state = {
    current: 1,
    visible: '',
    orderVisible: '',
    load: false,
    stagesShow: false,
    orderId: null,
    channelList: [],
    stages: [],
    initialValue: [],
    backuser: [],
    mydata: {},
    datas: {},
    record: {},
    mydb: [],
    expressList: [],
    yunTime: getTimeDistance('month'),
    gBtusua: '04', // 默认状态: 待发货
    queryLoad: false,
    rowSelectionOrderId: [],
    queryOrderStatistics: {}, //统计
  };

  renderAddCostPriceModal() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const { orderVisible, orderId } = this.state;
    const { getFieldDecorator } = this.props.form;
    const handleOk = e => {
      e.preventDefault();
      this.props.form.validateFields(['cost_price'], (err, values) => {
        if (!err) {
          console.log('orderid:', orderId, values);
          values.orderId = orderId;
          values.costPrice = values.cost_price;
          request(`/hzsx/business/order/addcostPrice`, values).then(res => {
            console.log('rcs', res);
            this.setState({
              visible: false,
              orderVisible: false,
            });
            this.props.form.resetFields();
            //this.handleReset();
            message.success('添加完成', 5);
          });
        }
      });
    };

    return (
      <div>
        <Modal
          title="添加成本"
          visible={orderVisible === 'addcost'}
          onOk={handleOk}
          onCancel={this.handleCancel}
        >
          <Form>
            <Form.Item label="成本金额" {...formItemLayout}>
              {getFieldDecorator('cost_price', {
                rules: [{ required: true, message: '请输入成本金额' }],
              })(<InputNumber placeholder="请输入" />)}
            </Form.Item>
          </Form>
        </Modal>
      </div>
    );
  }
  showOrderModal = (type, orderId) => {
    console.log('按钮点击', type, orderId);
    this.setState({
      orderVisible: type,
      orderId,
    });
  };

  handleCancel = e => {
    this.setState({
      visible: false,
      orderVisible: false,
    });
  };

  renderAddYjPriceModal() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const { orderVisible, orderId } = this.state;
    const { getFieldDecorator } = this.props.form;
    const handleOk = e => {
      e.preventDefault();
      this.props.form.validateFields(['yj'], (err, values) => {
        if (!err) {
          console.log('orderid:', orderId, values);
          values.orderId = orderId;

          request(`/hzsx/business/order/addyj`, values).then(res => {
            console.log('rcs', res);
            this.setState({
              visible: false,
              orderVisible: false,
            });
            this.props.form.resetFields();
            //this.handleReset()
            message.success('添加完成', 5);
          });
        }
      });
    };

    return (
      <div>
        <Modal
          title="添加押金"
          visible={orderVisible === 'addyj'}
          onOk={handleOk}
          onCancel={this.handleCancel}
        >
          <Form>
            <Form.Item label="押金金额" {...formItemLayout}>
              {getFieldDecorator('yj', {
                rules: [{ required: true, message: '请输入成本金额' }],
              })(<InputNumber placeholder="请输入" />)}
            </Form.Item>
          </Form>
        </Modal>
      </div>
    );
  }

  componentDidMount() {
    request('/hzsx/user/listBackstageUser', {}, 'get').then(res => {
      console.log('用户信息：', res);
      this.setState({
        backuser: res,
      });
    });

    const { status, ti } = this.props.location.query;
    if (ti) {
      var date = new Date();
      let d1 = moment(date).format('YYYY-MM-DD') + ' 00:00:00';
      let d2 = moment(date).format('YYYY-MM-DD') + ' 23:59:59';
      console.log('d1,d2', d1, d2);
      this.setState({
        initialValue: [moment(d1), moment(d2)],
      });
      this.props.form.setFieldsValue({
        createTimeStart: d1,
        createTimeEnd: d2,
      });
      this.onList(1, 30, {
        createTimeStart: d1,
        createTimeEnd: d2,
      });
      return;
    }
    if (status) {
      this.setState(
        {
          gBtusua: status,
          load: true,
        },
        () => {
          this.props.form.setFieldsValue({
            status,
          });
          this.onList(1, 30, {
            status: this.state.gBtusua,
          });
          // this.getExpressList();
        },
      );
    } else {
      this.onList(1, 30);
      // this.getExpressList();
    }

    this.props.dispatch({
      type: 'order/PlateformChannel',
    });
    request('/hzsx/business/channel/getChannelList', {
      pageNumber: 1,
      islist: 1,
      pageSize: 1000,
    }).then(res => {
      console.log('返回的信息是', res);
      res.records.unshift({ id: '000', name: '默认' });
      this.setState({
        channelList: res.records,
      });
    });
  }

  ziti = e => {
    e && e.preventDefault();
    if (this.state.rowSelectionOrderId.length == 0) {
      message.error('请先选择订单');
      return;
    }
    request(`/hzsx/business/order/orderPickUpSelect`, {
      orderIds: this.state.rowSelectionOrderId,
    }).then(res => {
      message.success('操作完成', 5);
    });
  };

  //导出租用中的订单
  exporder = e => {
    e && e.preventDefault();
    this.props.form.validateFields((err, values) => {
      console.log(values, 'values');
      if (values.createDate && values.createDate.length < 1) {
        values.createTimeStart = '';
        values.createTimeEnd = '';
      } else if (values.createDate) {
        values.createTimeStart = moment(values.createDate[0]).format('YYYY-MM-DD HH:mm:ss');
        values.createTimeEnd = moment(values.createDate[1]).format('YYYY-MM-DD HH:mm:ss');
      }
      if (values.status) values.status = [values.status];
      else values.status = ['04', '05', '06', '07', '08', '09'];
      console.log('db:', values);
      request('/hzsx/export/rentOrder', values).then(res => {
        console.log('返回的信息是', res);
        message.success('导出成功');
      });
    });
  };
  onList = (pageNumber, pageSize, data = {}) => {
    this.setState({
      load: true,
    });
    //判断有没有时间
    console.log('data1111:', data);
    request('/hzsx/business/order/queryOrderByRentNew', {
      pageSize,
      pageNumber,
      iszy: 1,
      ...data,
      unit:0
    }).then(res => {
      console.log('aaaa:', res);
      this.setState({
        mydata: {
          page: {
            total: res.total,
            records: res.records,
          },
        },
        load: false,
        queryLoad: false,
      });
    });

    request('/hzsx/business/order/queryOrderByRentCount', {
      pageSize,
      pageNumber,
      iszy: 1,
      ...data,
      unit:0
    }).then(res => {
      this.setState({
        queryOrderStatistics: res,
      });
      console.log(123456, res);
    });
    const { dispatch } = this.props;
    /* dispatch({
      type: 'order/queryOpeOrderByConditionList',
      payload: {
        pageSize,
        pageNumber,
        iszy: 1,
        ...data,
      },
    }); */
  };
  //查看账单
  seeStage = orderId => {
    request(`/hzsx/business/order/queryOrderStagesDetail`, { orderId }).then(res => {
      console.log('sjrh：', res.orderByStagesDtoList);
      this.setState({
        stages: res.orderByStagesDtoList,
        stagesShow: true,
      });
    });
  };

  // 重置
  handleReset = e => {
    this.setState(
      {
        gBtusua: '',
      },
      () => {
        this.props.form.resetFields();
        this.props.form.setFieldsValue({
          status: undefined,
        });
        this.handleSubmit(e);
      },
    );
  };
  handleSubmit = e => {
    e && e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (values.createDate && values.createDate.length < 1) {
        values.createTimeStart = '';
        values.createTimeEnd = '';
        this.setState(
          {
            datas: { ...values },
            current: 1,
            queryLoad: true,
          },
          () => {
            this.onList(1, 30, { ...values });
          },
        );
      } else if (values.createDate) {
        values.createTimeStart = moment(values.createDate[0]).format('YYYY-MM-DD HH:mm:ss');
        values.createTimeEnd = moment(values.createDate[1]).format('YYYY-MM-DD HH:mm:ss');
        this.setState(
          {
            datas: { ...values },
            current: 1,
            queryLoad: true,
          },
          () => {
            this.onList(1, 30, { ...values });
          },
        );
      } else {
        this.setState(
          {
            datas: { ...values },
            current: 1,
            queryLoad: true,
          },
          () => {
            this.onList(1, 30, { ...values });
          },
        );
      }
    });
  };

  onexport = () => {
    exportCenterHandler(this, 'exportRentOrder', true, false);
  };

  handleCancel = e => {
    this.setState({
      visible: false,
      orderVisible: false,
    });
  };
  //table 页数
  onPage = e => {
    this.setState(
      {
        current: e.current,
      },
      () => {
        const params = {
          ...this.state.datas,
        };
        this.onList(e.current, 30, params);
      },
    );
  };
  onChanges = e => {
    this.setState({
      gBtusua: e,
    });
  };

  // 认领订单
  lingqu = () => {
    // console.log("user",getCurrentUser());
    this.props.dispatch(
      routerRedux.push({
        pathname: `/Order/HomePage/ReceiveOrder`,
      }),
    );
  };
  onClose = () => {
    console.log('取消：');
    this.state.stagesShow = false;
    this.setState({ stagesShow: false });
  };
  render() {
    const { load, mydata, initialValue, queryOrderStatistics } = this.state;
    let orderByStagesDtoList = this.state.stages;
    const { loading, form, mydb } = this.props;
    let allTotal = mydata?.page?.total;
    let allList = mydata?.page?.records;

    console.log('mydata', mydata);
    const paginationProps = {
      current: this.state.current,
      pageSize: 30,
      total: allTotal,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / 30)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };
    const gettext = (val, e) => {
      if (val == '000') val = '默认';
      //<p className="green-status">{e.realName}</p>
      if (e.isRed == 1 && e.status == '06') return <p className="red-status">{val}</p>;
      return <p>{val}</p>;
    };
    const user = getCurrentUser();
    const columns = [
      {
        title: '订单编号',
        dataIndex: 'orderId',
        render: orderId => {
          return (
            <>
              <CopyToClipboard text={orderId}></CopyToClipboard>
            </>
          );
        },
        width: '120px',
      },
      {
        title: '渠道来源',
        width: 120,
        render: e => gettext(e.channelName, e),
      },
      {
        title: '初始下单渠道',
        width: 120,
        dataIndex: 'firstChannelName',
        render:(e,records) =>{
          return <p>{records.firstChannelName}</p>
        }
      },
      {
        title: '姓名/手机号',
        width: 140,
        render: e => {
          return (
            <>
              <div>
                {e && e.isVip && e.isVip == 1 ? (
                  <p className="green-status">{e.realName}</p>
                ) : (
                  <p>{e.realName}</p>
                )}
                <p>{e.telephone}</p>
              </div>
            </>
          );
        },
      },
      {
        title: '商品名称',
        //dataIndex: 'productName',
        width: 160,
        render: e => gettext(e.productName + e.skuTitle, e),
      },
      {
        title: '试用周期',
        //dataIndex: 'isWeek',
        width: 120,
        render: e => gettext(ddzq[e.isWeek], e),
      },
      // {
      //   title: '已付期数',
      //   width: 110,
      //   render: e => {
      //     return (
      //       <>
      //         <Tooltip placement="top" title={e.currentPeriods}>
      //           <div
      //             style={{
      //               width: 60,
      //             }}
      //           >
      //             <p>
      //               {e.payedPeriods}/{e.totalPeriods + 1}
      //             </p>
      //             <a onClick={() => {
      //               this.seeStage(e.orderId)
      //             }}>查看</a>
      //           </div>
      //         </Tooltip>
      //       </>
      //     );
      //   },
      // },
      {
        title: '已付金额',
        width: 90,
        render: e => {
          return (
            <>
              <div>
                <p
                  style={{ display: 'flex', 'align-items': 'center', 'justify-content': 'center' }}
                >
                  {e.payedRentAmount}
                </p>
                <p
                  style={{ display: 'flex', 'align-items': 'center', 'justify-content': 'center' }}
                >
                  {e.totalRentAmount}
                </p>
              </div>
            </>
          );
        },
      },
      // {
      //   title: '已付押金',
      //   dataIndex: 'yj',
      //   width: 120,
      // },
      {
        title: '成本',
        dataIndex: 'costPrice',
        width: 120,
      },
      {
        title: '差值',
        dataIndex: 'cz',
        width: 120,
        render: (_, i) => {
          let cb = i.costPrice || 0; //成本
          let yj = i.yj || 0; //已付押金
          let zj = i.payedRentAmount || 0; //已付押金
          //差值cz(成本-已付押金-已付租金)
          let yfzj = i.payedRentAmount; //已付租金
          let cz = zj + yj - cb;
          //console.log("差值",i)
          return cz.toFixed(2);
        },
      },
      {
        title: '应收',
        dataIndex: 'lr',
        width: 120,
        render: (_, i) => {
          let cb = i.costPrice || 0; //成本
          let zzj = i.totalRentAmount; //总金额
          let yfzj = i.payedRentAmount; //已付租金
          //利润（总租金-成本）
          //console.log("利润（总租金-成本）", i)
          return (zzj - yfzj).toFixed(2);
        },
      },
      {
        title: '审核',
        dataIndex: 'rname',
        width: 100,
      },
      {
        title: '复购次数',
        dataIndex: 'repurchaseCurrent',
        width: 100,
        render: (_, record) => (!record.repurchaseCurrent ? 0 : record.repurchaseCurrent),
      },
      {
        title: '下单时间',
        dataIndex: 'placeOrderTime',
        width: 120,
      },
      {
        title: '完结时间',
        dataIndex: 'finishTime',
        width: 120,
      },
      {
        title: '订单状态',
        dataIndex: 'status',
        width: 120,
        render: (_, record) => renderOrderStatus(record),
      },

      {
        title: '操作',
        width: 240,
        fixed: 'right',
        align: 'center',
        render: (e, record) => {
          return (
            <div>
              <a
                className="primary-color"
                // onClick={() => router.push(`/Order/Details?id=${e.orderId}`)}
                href={`#/Order/HomePage/Details?id=${e.orderId}`}
                target="_blank"
              >
                处理
              </a>
              <Divider type="vertical" />
              <a
                className="primary-color"
                onClick={() => {
                  this.showOrderModal('addcost', e.orderId);
                }}
                target="_blank"
              >
                添加成本
              </a>
              {/* <Divider type="vertical" />
              <a
                className="primary-color"
                onClick={() => {
                  this.showOrderModal('addyj', e.orderId);
                }}
              >
                添加押金
              </a> */}
            </div>
          );
        },
      },
    ];

    const { getFieldDecorator } = form;

    const rowSelection = {
      onChange: (selectedRowKeys, selectedRows) => {
        let array = [];
        selectedRows.forEach(el => {
          array.push(el.orderId);
        });
        this.setState({
          rowSelectionOrderId: array,
        });
      },
      getCheckboxProps: record => ({
        disabled: record.name === 'Disabled User', // Column configuration not to be checked
        name: record.name,
      }),
    };

    const difference = () => {
      return (
        <Tooltip title="已付金额-总成本">
          <span>差额</span>
          <Icon style={{color:'#1890ff'}} type="exclamation" />
        </Tooltip>
      );
    };

    const accounts = () => {
      return (
        <Tooltip title="所有未到期金额（包含已经支付的）">
          <span>未到期金额</span>
          <Icon style={{ color: '#1890ff' }} type="exclamation" />
        </Tooltip>
      );
    };


    const delinquencyRate = () => {
      return (
        <Tooltip title="（逾期金额 / 到期金额 ） * 100">
          <span>逾期率</span>
          <Icon style={{color:'#1890ff'}} type="exclamation" />
        </Tooltip>
      );
    };

    const orderOverdueRate = () => {
      return (
        <Tooltip title="（逾期订单数 / 到期订单数 ） * 100">
          <span>订单逾期率</span>
          <Icon style={{color:'#1890ff'}}n type="exclamation" />
        </Tooltip>
      );
    };
    const overdueAmount = () => {
      return (
        <Tooltip title="到期未付款金额累加（支付晚于还款时间（已取消，已完结等账单状态都归为逾期金额），部分还款中的未还款金额也加入统计）">
          <span>逾期金额</span>
          <Icon style={{color:'#1890ff'}}n type="exclamation" />
        </Tooltip>
      );
    }

    const amountDue = () => {
      return (
        <Tooltip title="到期金额累加（还款日之后的金额进行统计）">
          <span>到期金额</span>
          <Icon style={{color:'#1890ff'}}n type="exclamation" />
        </Tooltip>
      );
    }

    
    const DesItem = [
      {
        key: '1',
        title: '订单总金额',
        value: queryOrderStatistics.total || 0,
      },
      {
        key: '2',
        title: '总成本',
        value: queryOrderStatistics.totalCost || 0,
      },
      {
        key: '3',
        title: '已付金额',
        value: queryOrderStatistics.amountPaid || 0,
      },
      {
        key: '4',
        title: () => {
          return difference();
        },
        value: queryOrderStatistics.difference || 0,
      },
      {
        key: '5',
        title: () => {
          return accounts();
        },
        value: queryOrderStatistics.accounts || 0,
      },
      {
        key: '6',
        title: () => {
          return amountDue();
        },
        value: queryOrderStatistics.amountDue || 0,
      },
      {
        key: '7',
        title: () => {
          return overdueAmount();
        },
        value: queryOrderStatistics.overdueAmount || 0,
      },
      {
        key: '8',
        title: () => {
          return delinquencyRate();
        },
        value: `${queryOrderStatistics.delinquencyRate}%`,
      },
      {
        key: '9',
        title: '到期订单数',
        value: queryOrderStatistics.dueOrderNumber || 0,
      },
      {
        key: '10',
        title: '逾期订单数',
        value: queryOrderStatistics.lateOrderNumber || 0,
      },
      {
        key: '11',
        title: () => {
          return orderOverdueRate()
        },
        value: queryOrderStatistics.orderOverdueRate,
      },
    ];

    return (
      <>
        <PageHeaderWrapper title={false}>
          <Spin spinning={load}>
            <Drawer
              width={620}
              title="账单信息"
              placement="right"
              onClose={this.onClose}
              visible={this.state.stagesShow}
            >
              <MyStage orderByStagesDtoList={orderByStagesDtoList}></MyStage>
            </Drawer>
            {this.renderAddCostPriceModal()}
            {this.renderAddYjPriceModal()}
            <Drawer
              width={820}
              title="账单信息"
              placement="right"
              onClose={this.onClose}
              visible={this.state.stagesShow}
            ></Drawer>
            <Card
              bordered={false}
              className="ant-card antd-pro-pages-finance-capital-account-index-mb20"
            >
              <div style={{ fontSize: 20, marginBottom: 20 }}>
                <strong>统计信息</strong>
                <img src="https://rich-rent.oss-cn-hangzhou.aliyuncs.com/icon/椭圆形 2.png"></img>
              </div>
              <DescriptionsItem
                DesItem={DesItem}
                colg={{
                  xxl: 4,
                  xl: 4,
                  lg: 4,
                  md: 6,
                  sm: 8,
                  xs: 12,
                }}
              />
            </Card>
            <Card bordered={false}>
              <Form layout="inline" onSubmit={this.handleSubmit}>
                <Row>
                  <Col span={8}>
                    <Form.Item label="商品名称">
                      {getFieldDecorator(
                        'productName',
                        {},
                      )(<Input allowClear placeholder="请输入商品名称" />)}
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item label="下单人姓名">
                      {getFieldDecorator(
                        'userName',
                        {},
                      )(<Input allowClear placeholder="请输入下单人姓名" />)}
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item label="下单人手机号">
                      {getFieldDecorator(
                        'telephone',
                        {},
                      )(<Input allowClear placeholder="请输入下单人手机号" />)}
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    {' '}
                    <Form.Item label="订单编号">
                      {getFieldDecorator(
                        'orderId',
                        {},
                      )(<Input allowClear placeholder="请输入订单编号" />)}
                    </Form.Item>
                  </Col>

                  <Col span={8}>
                    <Form.Item label="用户身份证">
                      {getFieldDecorator(
                        'idCard',
                        {},
                      )(<Input allowClear placeholder="请输入下单人身份证号" />)}
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item label="收货人手机号">
                      {getFieldDecorator(
                        'addressUserPhone',
                        {},
                      )(<Input allowClear placeholder="请输入收货人手机号" />)}
                    </Form.Item>
                  </Col>

                  <Col span={8}>
                    <Form.Item label="订单状态">
                      {getFieldDecorator('status', {
                        // initialValue: '',
                      })(
                        <Select
                          placeholder="订单状态"
                          allowClear
                          style={{ width: 180 }}
                          onChange={this.onChanges}
                        >
                          {['01', '11', '04', '05', '06', '07', '08', '09', '10'].map(value => {
                            return (
                              <Option value={value.toString()} key={value.toString()}>
                                {orderStatusMap[value.toString()]}
                              </Option>
                            );
                          })}
                        </Select>,
                      )}
                    </Form.Item>
                  </Col>
                  {/* <Col span={8}>
                    <Form.Item label="订单周期">
                      {getFieldDecorator(
                        'unit',
                        {},
                      )(
                        <Select style={{ width: 180 }} allowClear placeholder="请选择">
                          {ddzq.map((value, i) => {
                            return (
                              <Option value={i} key={i}>
                                {value}
                              </Option>
                            );
                          })}
                        </Select>,
                      )}
                    </Form.Item>
                  </Col> */}
                  <Col span={8}>
                    <Form.Item label="订单时限">
                      {getFieldDecorator(
                        'ddsx',
                        {},
                      )(
                        <Select style={{ width: 180 }} allowClear placeholder="请选择">
                          {dayMap.map((value, i) => {
                            return (
                              <Option value={value} key={i}>
                                {value}天
                              </Option>
                            );
                          })}
                        </Select>,
                      )}
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item label="渠道来源">
                      {getFieldDecorator('channelId', {
                        // initialValue: '',
                      })(
                        <Select
                          placeholder="渠道来源"
                          allowClear
                          style={{ width: 180 }}
                          onChange={this.onChangesChannel}
                        >
                          {this.state.channelList.map(value => {
                            return (
                              <Option value={value.id} key={value.id}>
                                {value.name}
                              </Option>
                            );
                          })}
                        </Select>,
                      )}
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item label="审核名字">
                      {getFieldDecorator(
                        'rid',
                        {},
                      )(
                        <Select
                          placeholder="审核员"
                          allowClear
                          style={{ width: 180 }}
                          onChange={this.onChangesChannel}
                        >
                          {this.state.backuser.map(value => {
                            return (
                              <Option value={value.id} key={value.id}>
                                {value.name}
                              </Option>
                            );
                          })}
                        </Select>,
                      )}
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item label="是否逾期">
                      {getFieldDecorator('isyuqi', {
                        // initialValue: '',
                      })(
                        <Select placeholder="是否逾期" allowClear style={{ width: 180 }}>
                          <Option value="1">逾期</Option>
                        </Select>,
                      )}
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item label="创建时间">
                      {getFieldDecorator('createDate', { initialValue })(<RangePicker />)}
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item label="复购次数">
                      {getFieldDecorator('repurchaseCurrent', {
                        // initialValue: '',
                      })(<InputNumber min={0} max={50} />)}
                    </Form.Item>
                  </Col>
                </Row>

                {/*  <UserRating form={this.props.form} /> */}
                <div>
                  <Form.Item>
                    <Button loading={this.state.queryLoad} type="primary" htmlType="submit">
                      查询
                    </Button>
                  </Form.Item>
                  <Form.Item>
                    <Button htmlType="button" onClick={this.handleReset}>
                      重置
                    </Button>
                  </Form.Item>
                  <Form.Item>
                    <Button htmlType="button" onClick={this.exporder}>
                      导出
                    </Button>
                  </Form.Item>
                  <Form.Item>
                    <Button onClick={this.ziti}>一键自提</Button>
                  </Form.Item>
                </div>
              </Form>

              <MyPageTable
                scroll={true}
                onPage={this.onPage}
                paginationProps={paginationProps}
                dataSource={onTableData(allList)}
                columns={columns}
                rowSelection={rowSelection}
              />
            </Card>
          </Spin>
        </PageHeaderWrapper>
      </>
    );
  }
}
