import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import { Card, Button, Form, Input, DatePicker, Spin } from 'antd';
import MyPageTable from '@/components/MyPageTable';
import { onTableData } from '@/utils/utils.js';
import CopyToClipboard from '@/components/CopyToClipboard';
import moment from 'moment';
const { RangePicker } = DatePicker;
import { getTimeDistance } from '@/utils/utils';
import { exportCenterHandler } from '../util';

@connect(({ order, loading }) => ({
  ...order,
  loading: loading.effects['order/queryWaitingGiveBackOrder'],
}))
@Form.create()
export default class OverdueNoRetrun extends Component {
  state = {
    current: 1,
    datas: {},
    yunTime: getTimeDistance('month'),
  };
  componentDidMount() {
    this.props.dispatch({
      type: 'order/PlateformChannel',
    });
    this.onList(1, 10);
  }
  onList = (pageNumber, pageSize, data = {}) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'order/queryWaitingGiveBackOrder',
      payload: {
        pageSize,
        pageNumber,
        ...data,
      },
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      console.log('Received values of form: ', values);
      if (values.createDate && values.createDate.length < 1) {
        values.createTimeStart = '';
        values.createTimeEnd = '';
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      } else if (values.createDate) {
        values.createTimeStart = moment(values.createDate[0]).format('YYYY-MM-DD HH:mm:ss');
        values.createTimeEnd = moment(values.createDate[1]).format('YYYY-MM-DD HH:mm:ss');
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      } else {
        this.setState(
          {
            datas: { ...values },
          },
          () => {
            this.onList(1, 10, { ...values });
          },
        );
      }
    });
  };
  //table 页数
  onPage = e => {
    this.setState(
      {
        current: e.current,
      },
      () => {
        this.onList(e.current, 10, this.state.datas);
      },
    );
  };
  // 重置
  handleReset = e => {
    this.props.form.resetFields();
    this.handleSubmit(e);
  };
  confirm = () => {};
  onexport = () => {
    exportCenterHandler(this, 'exportNotReturnOrder', false, true);
    // const { yunTime } = this.state;
    // if (this.props.form.getFieldValue('createDate')) {
    //   this.props.dispatch({
    //     type: 'order/exportWaitingGiveBack',
    //     payload: {
    //       createTimeEnd: moment(this.props.form.getFieldValue('createDate')[1]).format(
    //         'YYYY-MM-DD HH:mm:ss',
    //       ),
    //       createTimeStart: moment(this.props.form.getFieldValue('createDate')[0]).format(
    //         'YYYY-MM-DD HH:mm:ss',
    //       ),
    //     },
    //   });
    // } else {
    //   this.props.dispatch({
    //     type: 'order/exportWaitingGiveBack',
    //     payload: {
    //       createTimeEnd: moment(yunTime[1]).format('YYYY-MM-DD HH:mm:ss'),
    //       createTimeStart: moment(yunTime[0]).format('YYYY-MM-DD HH:mm:ss'),
    //     },
    //   });
    // }
  };
  render() {
    const { waitingGiveBackList, waitingGiveTotal, PlateformList, loading } = this.props;
    const paginationProps = {
      current: this.state.current,
      pageSize: 10,
      total: waitingGiveTotal,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / 10)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };
    const columns = [
      {
        title: '订单编号',
        dataIndex: 'orderId',
        render: orderId => <CopyToClipboard text={orderId} />,
        width: '120px',
      },
      {
        title: '渠道来源',
        dataIndex: 'channelName',
        width: '10%',
      },
      {
        title: '商品名称',
        dataIndex: 'productName',
        width: '200px',
      },
      {
        title: '当前租期',
        dataIndex: 'currentPeriods',
        width: '60px',
      },
      {
        title: '总租金',
        width: '100px',
        render: e => {
          return (
            <>
              {e.totalRentAmount}
            </>
          );
        },
      },
      {
        title: '已付租金',
        width: '100px',
        render: e => {
          return (
            <>
              {e.payedRentAmount}
            </>
          );
        },
      },
      {
        title: '下单人姓名',
        width: '100px',
        render: e => {
          return (
            <>
              {e.realName}
            </>
          );
        },
      },
      {
        title: '下单人手机号',
        width: '120px',
        render: e => {
          return (
            <>
              {e.telephone}
            </>
          );
        },
      },
      {
        title: '起租时间',
        width: '120px',
        render: e => {
          return (
            <>
              {e.rentStart}
            </>
          );
        },
      },
      {
        title: '归还时间',
        width: '120px',
        render: e => {
          return (
            <>
              {e.unrentTime}
            </>
          );
        },
      },
      {
        title: '下单时间',
        dataIndex: 'placeOrderTime',
        width: '120px',
      },
      {
        title: '操作',
        fixed: 'right',
        width: '70px',
        align: 'center',
        render: e => {
          return (
            <div style={{ textAlign: 'center' }}>
              <a
                className="primary-color"
                // onClick={() => router.push(`/Order/Details?id=${e.orderId}&settlement=settlement`)}
                href={`#/Order/HomePage/Details?id=${e.orderId}`}
                target="_blank"
              >
                详情
              </a>
            </div>
          );
        },
      },
    ];
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    return (
      <PageHeaderWrapper title={false}>
        <Card bordered={false}>
          <Form layout="inline" onSubmit={this.handleSubmit}>
            <Form.Item label="商品名称">
              {getFieldDecorator('productName', {})(<Input allowClear placeholder="请输入商品名称" />)}
            </Form.Item>
            <Form.Item label="下单人姓名">
              {getFieldDecorator('userName', {})(<Input allowClear placeholder="请输入下单人姓名" />)}
            </Form.Item>
            <Form.Item label="下单人手机号">
              {getFieldDecorator('telephone', {})(<Input allowClear placeholder="请输入下单人手机号" />)}
            </Form.Item>
            <Form.Item label="订单编号">
              {getFieldDecorator('orderId', {})(<Input allowClear placeholder="请输入订单编号" />)}
            </Form.Item>
            <Form.Item label="创建时间">{getFieldDecorator('createDate', {})(<RangePicker allowClear />)}</Form.Item>
            <div>
              <Form.Item>
                <Button type="primary" htmlType="submit">
                  查询
                </Button>
              </Form.Item>
              <Form.Item>
                <Button htmlType="button" onClick={this.handleReset}>
                  重置
                </Button>
              </Form.Item>
              <Form.Item>
                <Button onClick={this.onexport}>导出</Button>
              </Form.Item>
            </div>
          </Form>
          <Spin spinning={loading}>
            <MyPageTable
              scroll={true}
              onPage={this.onPage}
              paginationProps={paginationProps}
              dataSource={onTableData(waitingGiveBackList)}
              columns={columns}
            />
          </Spin>
        </Card>
      </PageHeaderWrapper>
    );
  }
}
