import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import MyPageTable from '@/components/MyPageTable';
import {
  Card,
  Table,
  Divider,
  Form,
  DatePicker,
  Input,
  Button,
  Modal,
  message,
  Popconfirm,
} from 'antd';
import request from '@/services/baseService';
import moment from 'moment';

const { RangePicker } = DatePicker;
@Form.create()
class whiteList extends Component {
  state = {
    search: {
      pageNumber: 1,
      pageSize: 10,
      mobile: '',
    },
    form: {
      mobile: '',
    },
    tableList: [],
    current: 1,
    total: 0,
    pages: 1,
    modal: {
      type: 'add',
      title: '',
    },
    queryLoad: false,
    modalLoad: false,
  };
  componentDidMount() {
    this.getList();
  }

  getList = () => {
    const { search,form } = this.state;

    let obj = Object.fromEntries(
      Object.entries(search).filter(([key, value]) => {
        // 过滤空值
        return value !== '' && value !== null && value !== undefined;
      }),
    );
    // if(form.mobile){
    //   obj.mobile = form.mobile
    // }

    request(`/hzsx/user/getLoginLog`, obj, 'get').then(res => {
      console.log(123546, res);
      this.setState({
        tableList: res.records,
        queryLoad: false,
        pages: res.pages,
        total: res.total,
        current: res.current,
      });
    });
  };

  columns = [
    {
      title: 'No.',
      dataIndex: 'id',
      rowScope: 'row',
      width: 60,
      render: (record, index, indent, expanded) => {
        return indent + 1;
      },
    },
    {
      title: '账号',
      dataIndex: 'mobile',
      width: 160,
    },
    {
      title: '登录时间',
      dataIndex: 'createTime',
      width: 160,
    },
    // {
    //   title: 'ip地址',
    //   dataIndex: 'ip',
    //   width: 160,
    // },
    {
      title: '归属地',
      dataIndex: 'ipAttribution',
      width: 160,
    },
  ];

  handSearch = () => {
    this.props.form.validateFields(
      [ 'mobile'],
      (err, values) => {
        if (!err) {


          this.setState(
            {
              queryLoad: true,
              search: {
                pageNumber: 1,
                pageSize: 10,
                mobile: values.mobile,
              },
            },
            () => this.getList(),
          );
        }
      },
    );
  };
  onPage = e => {
    // pageNumber: 1,
    // pageSize: 3,
    let data = Object.assign(this.state.search, { pageNumber: e.current, pageSize: e.pageSize });

    this.setState(
      {
        search: data,
      },
      () => this.getList(),
    );
  };

  render() {
    const { current, pages, total, search, queryLoad } = this.state;
    const { getFieldDecorator } = this.props.form;
    console.log('条数', total);
    const paginationProps = {
      current: current,
      pageSize: search.pageSize,
      total: total,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / search.pageSize)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };

    return (
      <PageHeaderWrapper title={false} className="nav-tab">
        <Card bordered={false} className="mt-20">
          <Form layout="inline" className='mb-20'>
            <Form.Item label="账号：">
              {getFieldDecorator('mobile', {
                rules: [],
              })(<Input placeholder="请输入账号" />)}
            </Form.Item>
            <Form.Item>
              <Button
                loading={queryLoad}
                onClick={() => this.handSearch()}
                type="primary"
                
              >
                查询
              </Button>
            </Form.Item>
          </Form>

          {/* </Card>
        <Card bordered={false}> */}
          <MyPageTable
            //   scroll={true}
            onPage={this.onPage}
            style={{ marginTop: '20px' }}
            paginationProps={paginationProps}
            dataSource={this.state.tableList}
            columns={this.columns}
            rowKey="id"
          />
        </Card>
      </PageHeaderWrapper>
    );
  }
}

export default whiteList;
