import React from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Card, Button, Form, Table, Input, Icon, Popconfirm, message } from 'antd';
import { onTableData } from '@/utils/utils.js';
import MyPageTable from '@/components/MyPageTable';
import request from '@/services/baseService';
import moment from 'moment';

@Form.create()
export default class BanPhonePage extends React.Component {
  state = {
    list: [],
    //手机号查询
    type: 1,
  };

  componentDidMount() {
    this.getList();
  }

  getList = (keys = '') => {
    request(
      '/hzsx/userBlack/getRedisDisabledList',
      {
        keys,
        type: this.state.type,
      },
      'get',
    ).then(res => {
      this.setState({
        list: res,
      });
    });
  };

  handleSubmit = () => {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.getList(values.keys);
        console.log('Received values of form: ', values);
      }
    });
  };
  confirm = rend => {
    request(
      '/hzsx/userBlack/deleteByDisabled',
      {
        key: rend.phone,
        type: this.state.type,
      },
      'post',
    ).then(res => {
      message.success('删除成功');
      this.getList();
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { list } = this.state;

    const cancel = e => {};

    const columns = [
      {
        title: '用户令牌',
        dataIndex: 'token',
        key: 'token',
      },
      {
        title: '手机号',
        dataIndex: 'phone',
        key: 'phone',
      },
      {
        title: '访问次数',
        dataIndex: 'count',
        key: 'count',
        width: 100,
      },
      {
        title: '封禁理由',
        dataIndex: 'text',
        key: 'text',
        width: 400,
      },
      {
        title: '请求地址',
        dataIndex: 'url',
        key: 'url',
        width: 300,
      },
      {
        title: '创建时间',
        dataIndex: 'createTime',
        key: 'createTime',
        width: 150,
      },
      {
        title: '操作',
        key: 'x',
        width: 100,
        render: (text, rend) => {
          return (
            <Popconfirm
              title="确定删除吗"
              onConfirm={() => this.confirm(rend)}
              onCancel={cancel}
              okText="Yes"
              cancelText="No"
            >
              <a>删除</a>
            </Popconfirm>
          );
        },
      },
    ];

    return (
      <>
        <PageHeaderWrapper title={false}>
          <Card bordered={false}>
            <Form layout="inline" onSubmit={this.handleSubmit} style={{ marginBottom: '20px' }}>
              <Form.Item>
                {getFieldDecorator(
                  'keys',
                  {},
                )(
                  <Input
                    prefix={<Icon type="mobile" style={{ color: 'rgba(0,0,0,.25)' }} />}
                    placeholder="请输入手机号"
                  />,
                )}
              </Form.Item>
              <Form.Item>
                <Button type="primary" htmlType="submit">
                  查询
                </Button>
              </Form.Item>
            </Form>
            <Table dataSource={list} columns={columns} />;
          </Card>
        </PageHeaderWrapper>
      </>
    );
  }
}
