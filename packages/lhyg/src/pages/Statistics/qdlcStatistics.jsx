import React from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import {
  Card,
  Button,
  Form,
  DatePicker,
  Descriptions,
  message,
  Select,
  Table,
  InputNumber,
} from 'antd';
import { onTableData } from '@/utils/utils.js';
import MyPageTable from '@/components/MyPageTable';
import request from '@/services/baseService';
import moment from 'moment';
import DescriptionsItem from '@/components/DescriptionsItem';

const { MonthPicker, RangePicker } = DatePicker;

@Form.create()
export default class StatisticsPage extends React.Component {
  state = {
    current: 1,

    //----到期统计
    expireCollect: {},
    expireData: [],
    expireCurrent: 1,
    expireSize: 10,
    expireTotal: 0,
    expireLoad: false,
    expireQdList: [],
    //----到期统计
  };

  componentDidMount() {
    this.channelStagesStat();
    this.expireStatistics();
    this.expireTable({ size: this.state.expireSize, current: this.state.expireCurrent });
  }

  componentWillUnmount() {
    clearInterval(this.dailyInterval);
  }

  //---------------到期统计

  channelStagesStat = () => {
    request('/hzsx/business/channel/getChannelList', {
      // pageNumber: 1,
      // islist: 1,
      size: 1000,
    }).then(res => {
      this.setState({
        expireQdList: res.records,
      });
    });
  };

  expireStatistics = (param = {}) => {
    request('hzsx/statistics/orderStagesTotalStat ', param, 'post').then(res => {
      this.setState({
        expireCollect: res,
      });
    });
  };

  expireTable = param => {
    request('hzsx/statistics/channelAndRepurchaseTotalStat', param, 'post').then(res => {
      this.setState({
        expireData: res.records,
        expireTotal: res?.total,
        expireCurrent: res.current,
      });
      setTimeout(() => {
        this.setState({
          expireLoad: false,
        });
      }, 500);
    });
  };
  expireHandleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState({
          expireLoad: true,
        });
        let param = {
          current: this.state.dailyCurrent,
          size: this.state.dailySize,
          name: values.expireQdName,
          repurchaseCurrent: values.repurchaseCurrent,
        };
        if (values.expireRangePicker && values.expireRangePicker.length > 0) {
          param.dateStart = `${moment(values.expireRangePicker[0]).format('YYYY-MM-DD')} 00:00:00`;
          param.dateEnd = `${moment(values.expireRangePicker[1]).format('YYYY-MM-DD')} 23:59:59`;
        }
        this.expireTable(param);
        this.expireStatistics({
          name: values.expireQdName,
          dateStart: param.dateStart,
          dateEnd: param.dateEnd,
          repurchaseCurrent: values.repurchaseCurrent,
        });
      }
    });
  };
  expireOnPage = e => {
    let { rangePicker, expireQdName } = this.props.form.getFieldsValue();
    this.setState(
      {
        expireCurrent: e.current,
        expireSize: e.pageSize,
      },
      () => {
        let param = {
          current: e.current,
          size: e.pageSize,
          name: values.expireQdName,
        };
        if (expireQdName) {
          param.name = expireQdName;
        }
        if (rangePicker) {
          param.dateStart = `${moment(rangePicker[0]).format('YYYY-MM-DD')} 00:00:00`;
          param.dateEnd = `${moment(rangePicker[1]).format('YYYY-MM-DD')} 23:59:59`;
        }
        this.expireTable(param);
      },
    );
  };
  //---------------到期统计

  render() {
    //---------------到期统计

    const { getFieldDecorator } = this.props.form;
    const {
      expireCollect,
      expireData,
      expireCurrent,
      expireSize,
      expireTotal,
      expireLoad,
      expireQdList,
    } = this.state;

    const expireColumns = [
      {
        title: '渠道',
        dataIndex: 'name',
      },
      {
        title: '到期订单数',
        dataIndex: 'orderDue',
      },
      {
        title: ' 新用户订单数',
        dataIndex: 'orderDueNew',
      },
      {
        title: ' 老用户订单数',
        dataIndex: 'orderDueOld',
      },
      {
        title: '到期金额数',
        dataIndex: 'amountDue',
      },

      {
        title: '新用户到期金额数',
        dataIndex: 'amountDueNew',
      },
      {
        title: '老用户到期金额数',
        dataIndex: 'amountDueOld',
      },
      {
        title: '金额回款率 ',
        dataIndex: 'amountOverdueRate',
        render: (e, text) => {
          return `${(text.amountOverdueRate * 100).toFixed(2)}%`;
        },
      },
      {
        title: '新用户金额回款率 ',
        dataIndex: 'amountOverdueNewRate',
        render: (e, text) => {
          return `${(text.amountOverdueNewRate * 100).toFixed(2)}%`;
        },
      },
      {
        title: '老用户金额回款率 ',
        dataIndex: 'amountOverdueOldRate',
        render: (e, text) => {
          return `${(text.amountOverdueOldRate * 100).toFixed(2)}%`;
        },
      },
    ];
    const expirePaginationProps = {
      current: expireCurrent,
      pageSize: expireSize,
      total: expireTotal,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(expireTotal / expireSize)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };

    const DesItem = [
      {
        key: '1',
        title: '到期订单数',
        value: expireCollect.orderDue || 0,
      },
      {
        key: '2',
        title: '新用户订单数',
        value: expireCollect.orderDueNew || 0,
      },
      {
        key: '3',
        title: '老用户订单数',
        value: expireCollect.orderDueOld || 0,
      },
      {
        key: '4',
        title: '到期金额数',
        value: expireCollect.amountDue || 0,
      },
      {
        key: '5',
        title: '新用户到期金额数',
        value: expireCollect.amountDueNew || 0,
      },
      {
        key: '6',
        title: '老用户到期金额数',
        value: expireCollect.amountDueOld || 0,
      },
      {
        key: '7',
        title: '金额回款率 ',
        value: `${(expireCollect.amountOverdueRate * 100).toFixed(2)}%`,
      },
      {
        key: '8',
        title: '新用户金额回款率 ',
        value: `${(expireCollect.amountOverdueNewRate * 100).toFixed(2)}%`,
      },
      {
        key: '9',
        title: '老用户金额回款率 ',
        value: `${(expireCollect.amountOverdueOldRate * 100).toFixed(2)}%`,
      },
    ];

    //---------------到期统计
    return (
      <>
        <PageHeaderWrapper title={false}>
          <Card
            bordered={false}
            style={{
              marginTop: 24,
            }}
            //   className="ant-card antd-pro-pages-finance-capital-account-index-mb20"
          >
            <div style={{ fontSize: 20, marginBottom: 10 }}>
              <strong>渠道轮次统计</strong>
              <img src="https://rich-rent.oss-cn-hangzhou.aliyuncs.com/icon/椭圆形 2.png"></img>
            </div>
            <DescriptionsItem
              DesItem={DesItem}
              colg={{
                xxl: 4,
                xl: 4,
                lg: 4,
                md: 6,
                sm: 8,
                xs: 12,
              }}
            />
            <Descriptions column={4}>
              {/* <Descriptions.Item label="账单日">{expireCollect.statementDate}</Descriptions.Item> */}
            </Descriptions>
          </Card>
          <Card bordered={false}>
            <Form
              layout="inline"
              onSubmit={this.expireHandleSubmit}
              style={{ marginBottom: '20px' }}
            >
              <Form.Item label="日期筛选">
                {getFieldDecorator('expireRangePicker')(<RangePicker format="YYYY-MM-DD" />)}
              </Form.Item>
              <Form.Item label="渠道筛选">
                {getFieldDecorator(
                  'expireQdName',
                  {},
                )(
                  <Select
                    allowClear
                    showSearch
                    placeholder="请选择"
                    filterOption={(input, option) =>
                      option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                    }
                    style={{ width: '180px' }}
                  >
                    {this.state.expireQdList.map(res => {
                      return (
                        <Option value={res.name} key={res.id}>
                          {res.name}
                        </Option>
                      );
                    })}
                  </Select>,
                )}
              </Form.Item>
              <Form.Item label="复购轮次">
                {getFieldDecorator('repurchaseCurrent')(
                  <InputNumber min={0} placeholder="请输入" />,
                )}
              </Form.Item>
              <Form.Item>
                <Button type="primary" htmlType="submit" loading={expireLoad}>
                  查询
                </Button>
              </Form.Item>
            </Form>
            {/* <Table columns={expireColumns} dataSource={expireData} pagination={false} /> */}
            <MyPageTable
              scroll={true}
              onPage={this.expireOnPage}
              paginationProps={expirePaginationProps}
              dataSource={onTableData(expireData)}
              columns={expireColumns}
              rowSelection={false}
            />
          </Card>
        </PageHeaderWrapper>
      </>
    );
  }
}
