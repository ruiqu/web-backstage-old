import React from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Card, Button, Form, DatePicker, Descriptions, message, Spin } from 'antd';
import { onTableData, objEmpty } from '@/utils/utils.js';
import MyPageTable from '@/components/MyPageTable';
import DescriptionsItem from '@/components/DescriptionsItem';
import request from '@/services/baseService';
import moment from 'moment';

const { MonthPicker, RangePicker } = DatePicker;

@Form.create()
export default class StatisticsPage extends React.Component {
  state = {
    current: 1,
    //----每日统计
    dailyCollect: {},
    dailyData: [],
    dailyCurrent: 1,
    dailySize: 10,
    dailyTotal: 0,
    dailyLoad: false,
    dailyRefreshLoad: false,
    dailyRefreshLoadTitle: '刷新',
    dailyCollectSpain: true,
    dailyTableSpain: true,
    //----每日统计
  };

  componentDidMount() {
    this.dailyDataRefresh();
    // this.dailyStatistics();
    // this.dailyTable({ size: this.state.dailySize });
  }

  componentWillUnmount() {
    clearInterval(this.dailyInterval);
  }

  //---------------每日统计

  //每日统计
  dailyStatistics = (param = {}) => {
    this.setState({
      dailyCollectSpain: true,
    });
    let obj = objEmpty(param);
    request('hzsx/statistics/orderTotalStat', obj, 'post').then(res => {
      this.setState({
        dailyCollect: res,
        dailyCollectSpain: false,
      });
    });
  };

  //每日统计表格
  dailyTable = param => {
    this.setState({
      dailyTableSpain: true,
    });
    let obj = objEmpty(param);
    request('hzsx/statistics/orderPageStat', obj, 'post').then(res => {
      this.setState({
        dailyData: res.records,
        dailyTotal: res?.total,
        dailyCurrent: res.current,
        dailyTableSpain: false,
      });
      setTimeout(() => {
        this.setState({
          dailyLoad: false,
        });
      }, 500);
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState({
          dailyLoad: true,
        });
        let param = {
          current: this.state.dailyCurrent,
          size: this.state.dailySize,
        };
        if (values.rangePicker && values.rangePicker.length > 0) {
          param.dateStart = `${moment(values.rangePicker[0]).format('YYYY-MM-DD')} 00:00:00`;
          param.dateEnd = `${moment(values.rangePicker[1]).format('YYYY-MM-DD')} 23:59:59`;
        } else {
          param.dateStart = '';
          param.dateEnd = '';
        }
        this.dailyTable(param);
        this.dailyStatistics({
          dateStart: param.dateStart,
          dateEnd: param.dateEnd,
        });
      }
    });
  };
  //table 页数
  onPage = e => {
    this.setState(
      {
        dailyCurrent: e.current,
        dailySize: e.pageSize,
      },
      () => {
        let { rangePicker } = this.props.form.getFieldsValue();
        let param = {
          current: e.current,
          size: e.pageSize,
        };
        if (rangePicker && rangePicker.length > 0) {
          param.dateStart = `${moment(rangePicker[0]).format('YYYY-MM-DD')} 00:00:00`;
          param.dateEnd = `${moment(rangePicker[1]).format('YYYY-MM-DD')} 23:59:59`;
        }
        this.dailyTable(param);
      },
    );
  };
  //---------------每日统计

  dailyDataRefresh = () => {
    this.setState(
      {
        dailyRefreshLoad: true,
      },
      () => {
        request('hzsx/statistics/refreshStat', {}, 'post').then(res => {
          this.props.form.resetFields();
          let dailyRefreshLoadTitle = 5;
          this.dailyInterval = setInterval(() => {
            dailyRefreshLoadTitle -= 1;
            this.setState({ dailyRefreshLoadTitle: ` ${dailyRefreshLoadTitle}秒` });
            if (dailyRefreshLoadTitle === 0) {
              clearInterval(this.dailyInterval);
              this.setState({
                dailyRefreshLoadTitle: '刷新',
                dailyRefreshLoad: false,
              });
            }
          }, 1000);
          message.info('刷新成功');
          this.dailyStatistics();
          this.dailyTable({
            size: this.state.dailySize,
          });

          // this.expireStatistics();
          // this.expireTable({
          //   size: this.state.expireSize,
          // });
        });
      },
    );
  };

  render() {
    //---------------每日统计
    const {
      dailyCollect,
      dailyCurrent,
      dailyTotal,
      dailyData,
      dailySize,
      dailyLoad,
      dailyRefreshLoad,
      dailyRefreshLoadTitle,
      dailyCollectSpain,
      dailyTableSpain,
    } = this.state;
    const { getFieldDecorator } = this.props.form;

    const dailyColumns = [
      {
        title: '日期',
        dataIndex: 'statTime',
        width: 120,
      },
      {
        title: '注册数',
        dataIndex: 'registerNum',
        width: 100,
      },
      {
        title: '实名数',
        dataIndex: 'realNameNum',
        width: 100,
      },
      {
        title: '下单人数',
        dataIndex: 'orderUserNum',
        width: 100,
      },
      {
        title: '下单数量',
        dataIndex: 'orderNum',
        width: 100,
      },
      {
        title: '新用户机审通过数',
        dataIndex: 'newRiskPassNum',
        width: 150,
      },
      {
        title: '机审通过数',
        dataIndex: 'riskPassNum',
        width: 120,
      },
      {
        title: '总成交数',
        dataIndex: 'dealNum',
        width: 120,
      },
      {
        title: '初购成交数',
        dataIndex: 'newDealNum',
        width: 120,
      },
      {
        title: '复购成交数',
        dataIndex: 'oldDealNum',
        width: 120,
      },
      {
        title: '合同金额',
        dataIndex: 'rent',
        width: 120,
      },
      {
        title: '成本',
        dataIndex: 'costPrice',
        width: 120,
      },
      {
        title: '下单转化率',
        dataIndex: 'placeOrderRate',
        width: 120,
        render: (e, text) => {
          return `${(text.placeOrderRate * 100).toFixed(2)}%`;
        },
      },
      {
        title: '成交转化率',
        dataIndex: 'dealRate',
        width: 120,
        render: (e, text) => {
          return `${(text.dealRate * 100).toFixed(2)}%`;
        },
      },
    ];
    const paginationProps = {
      current: dailyCurrent,
      pageSize: dailySize,
      total: dailyTotal,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(dailyTotal / dailySize)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };
    const dailyDesItem = [
      {
        key: '1',
        title: '注册数',
        value: dailyCollect.registerNum || 0,
      },
      {
        key: '2',
        title: '实名数',
        value: dailyCollect.realNameNum || 0,
      },
      {
        key: '3',
        title: '下单人数',
        value: dailyCollect.orderUserNum || 0,
      },
      {
        key: '4',
        title: '下单数量',
        value: dailyCollect.orderNum || 0,
      },
      {
        key: '50',
        title: '新用户机审通过数',
        value: dailyCollect.newRiskPassNum || 0,
      },
      {
        key: '5',
        title: '机审通过数',
        value: dailyCollect.riskPassNum || 0,
      },
      {
        key: '6',
        title: '总成交数',
        value: dailyCollect.dealNum || 0,
      },
      {
        key: '7',
        title: '初购成交数',
        value: dailyCollect.newDealNum || 0,
      },
      {
        key: '8',
        title: '复购成交数',
        value: dailyCollect.oldDealNum || 0,
      },
      {
        key: '9',
        title: '合同金额',
        value: dailyCollect.rent || 0,
      },
      {
        key: '10',
        title: '成本',
        value: dailyCollect.costPrice || 0,
      },
      {
        key: '11',
        title: '下单转化率',
        value: `${(dailyCollect.placeOrderRate * 100).toFixed(2)}%`,
      },
      {
        key: '12',
        title: '成交转换率',
        value: `${(dailyCollect.dealRate * 100).toFixed(2)}%`,
      },
    ];
    //---------------每日统计

    return (
      <>
        <PageHeaderWrapper title={false}>
          <Card
            bordered={false}
            //   className="ant-card antd-pro-pages-finance-capital-account-index-mb20"
          >
            <div style={{ fontSize: 20, marginBottom: 10 }}>
              <strong>每日统计</strong>
              <img src="https://rich-rent.oss-cn-hangzhou.aliyuncs.com/icon/椭圆形 2.png"></img>
            </div>
            <Spin spinning={dailyCollectSpain}>
              <DescriptionsItem
                DesItem={dailyDesItem}
                colg={{
                  xxl: 2,
                  xl: 4,
                  lg: 4,
                  md: 6,
                  sm: 8,
                  xs: 12,
                }}
              />
            </Spin>
          </Card>
          <Card bordered={false}>
            <Form layout="inline" onSubmit={this.handleSubmit} style={{ marginBottom: '20px' }}>
              <Form.Item label="日期筛选">
                {getFieldDecorator('rangePicker')(<RangePicker format="YYYY-MM-DD" />)}
              </Form.Item>
              <Form.Item>
                <Button type="primary" htmlType="submit" loading={dailyLoad}>
                  查询
                </Button>
              </Form.Item>
              <Form.Item>
                <Button
                  type="primary"
                  onClick={() => this.dailyDataRefresh()}
                  loading={dailyRefreshLoad}
                >
                  {dailyRefreshLoadTitle}
                </Button>
              </Form.Item>
            </Form>

            <Spin spinning={dailyTableSpain}>
              <MyPageTable
                scroll={true}
                onPage={this.onPage}
                paginationProps={paginationProps}
                dataSource={onTableData(dailyData)}
                columns={dailyColumns}
              />
            </Spin>
          </Card>
        </PageHeaderWrapper>
      </>
    );
  }
}
