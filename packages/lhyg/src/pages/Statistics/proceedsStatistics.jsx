import moment from 'moment';
import { useEffect, useState } from 'react';
import request from '@/services/baseService';
import CopyToClipboard from '@/components/CopyToClipboard';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import DescriptionsItem from '@/components/DescriptionsItem';
import { Card, Spin, Form, DatePicker, Button, Select, message } from 'antd';

const { RangePicker } = DatePicker;
const { Option } = Select;

const ProcesssStatistics = props => {
  const { form } = props;
  console.log(123, form);

  const { getFieldDecorator } = form;

  useEffect(() => {
    getStageBillReceipts();
  }, []);

  /** 顶部统计 */
  const [desItem, setDesItem] = useState([]);
  const [descSpain, setSescSpain] = useState(false);

  const getStageBillReceipts = async (obj = {}) => {
    setSescSpain(true);
    let res = await request('hzsx/statistics/stageBillReceipts ', obj, 'post');
    setDesItem([
      {
        key: '1',
        title: '实收款合计',
        value: res.actualReceipts || 0,
      },
      {
        key: '2',
        title: '收款合计',
        value: res.totalReceipts || 0,
      },
      {
        key: '3',
        title: '退款合计',
        value: res.totalRefund || 0,
      },
      {
        key: '4',
        title: '到期收款',
        value: res.expireReceipts || 0,
      },
      {
        key: '5',
        title: '逾期收款',
        value: res.overdueReceipts || 0,
      },
      {
        key: '6',
        title: '提前处理收款',
        value: res.advanceReceipts || 0,
      },
    ]);
    setSescSpain(false);
  };

  const handleSubmit = () => {
    form.validateFields((err, values) => {
      if (values.repayDateTime) {
        values.repayDateTimeStart =
          moment(values.repayDateTime[0]).format('YYYY-MM-DD') + ' 00:00:00';
        values.repayDateTimeEnd =
          moment(values.repayDateTime[1]).format('YYYY-MM-DD') + ' 23:59:59';
        delete values.repayDateTime;
      }
      getStageBillReceipts(values);
      console.log(9999, values);
    });
  };

  /** 款项渠道 */
  const [payTypeChannel, setPayTypeChannel] = useState([]);
  const handleSelectChange = value => {
    if (value === '01') {
      setPayTypeChannel([
        {
          label: 'APP支付',
          value: '04',
        },
        {
          label: '微信小程序支付',
          value: '12',
        },
        {
          label: '微信公众号支付',
          value: '31',
        },
        {
          label: '微信支付（线下）',
          value: '29',
        },
        {
          label: '支付宝h5支付',
          value: '14',
        },
        {
          label: '支付宝支付（线下）',
          value: '30',
        },
        {
          label: '富有扫码',
          value: '21',
        },
        {
          label: '蚂蚁链代扣',
          value: '22',
        },
        {
          label: '宝付支付',
          value: '24',
        },
      ]);
    } else {
      setPayTypeChannel([
        {
          label: '支付宝退款',
          value: '25',
        },
        {
          label: '微信退款',
          value: '13',
        },
        {
          label: '银行卡退款',
          value: '26',
        },
        {
          label: '蚂蚁链退款',
          value: '27',
        },
        {
          label: '富有退款',
          value: '28',
        },
      ]);
    }
  };

  /** 导出状态 */
  const [exportLoading, setExportLoading] = useState(false);
  const handleExport = () => {
    form.validateFields(async (err, values) => {
      if (values.repayDateTime) {
        values.createTimeStart = moment(values.repayDateTime[0]).format('YYYY-MM-DD') + ' 00:00:00';
        values.createTimeEnd = moment(values.repayDateTime[1]).format('YYYY-MM-DD') + ' 23:59:59';
        delete values.repayDateTime;
      }
      values.tp = 'bill';
      setExportLoading(true);
      await request('hzsx/export/rentOrder', obj, 'post');
      message.success('导出成功');
      setExportLoading(false);
    });
  };

  /** 表格操作 */

  let columns = [
    {
      title: '订单编号',
      dataIndex: 'orderId',
      render: orderId => {
        return <CopyToClipboard text={orderId} />;
      },
      width: '120px',
    },
    {
      title: '姓名',
      dataIndex: 'userName',

    },
    {
      title: '手机号',
      dataIndex: 'telephone',
    },
    {
      title: '账单状态',
      dataIndex: 'status',
    },
    {
      title: '收款状态',
      dataIndex: 'incomeType',
    },
    {
      title: '收款方式',
      dataIndex: 'payType',
    },
    {
      title: '收款金额',
      dataIndex: 'beforePrice',
    },
    {
      title: '收款时间',
      dataIndex: 'createTime',
    },
  ];

  const onPage = e => {
    console.log('eeeeonPage', e, this.state.datas);
    this.setState(
      {
        current: e.current,
        pageSize: e.pageSize,
      },
      () => {
        const params = {
          ...this.state.datas,
        };
        this.onList(e.current, e.pageSize, params);
      },
    );
  };
  const paginationProps = {
    current: this.state.current,
    pageSize: this.state.pageSize,
    total: allTotal,
    pageSizeOptions: ['10', '20', '30', '50'],
    showSizeChanger: true,
    showTotal: total => (
      <span style={{ fontSize: '14px' }}>
        <span>共{Math.ceil(total / 30)}页</span>&emsp;
        <span>共{total}条</span>
      </span>
    ),
  };

  return (
    <PageHeaderWrapper title={false}>
      <Card
        bordered={false}
        style={{
          marginTop: 24,
        }}
      >
        <div style={{ fontSize: 20, marginBottom: 10 }}>
          <strong>款项统计</strong>
          <img src="https://rich-rent.oss-cn-hangzhou.aliyuncs.com/icon/椭圆形 2.png"></img>
        </div>
        <Spin spinning={descSpain}>
          <DescriptionsItem
            DesItem={desItem}
            colg={{
              xxl: 4,
              xl: 4,
              lg: 4,
              md: 6,
              sm: 8,
              xs: 12,
            }}
          />
        </Spin>
      </Card>
      <Card bordered={false}>
        <Form layout="inline" onSubmit={handleSubmit} style={{ marginBottom: '20px' }}>
          <Form.Item label="款项类型">
            {getFieldDecorator('incomeType')(
              <Select
                style={{ width: 120 }}
                onChange={handleSelectChange}
                placeholder="请选择款项类型"
                allowClear
              >
                <Option value="01">收款</Option>
                <Option value="02">退款</Option>
              </Select>,
            )}
          </Form.Item>
          {payTypeChannel.length > 0 ? (
            <Form.Item label="款项渠道">
              {getFieldDecorator('payType')(
                <Select style={{ width: 200 }} placeholder="请选择款项渠道" allowClear>
                  {payTypeChannel.map(item => {
                    return (
                      <Option value={item.value} key={item.value}>
                        {item.label}
                      </Option>
                    );
                  })}
                </Select>,
              )}
            </Form.Item>
          ) : null}

          <Form.Item label="日期筛选">
            {getFieldDecorator('repayDateTime')(<RangePicker format="YYYY-MM-DD" />)}
          </Form.Item>
          <Form.Item>
            {/* <Button type="primary" htmlType="submit" loading={descSpain}> */}
            <Button type="primary" htmlType="submit">
              查询
            </Button>
          </Form.Item>
          <Form.Item>
            <Button type="primary" onClick={() => handleExport()} loading={exportLoading}>
              导出
            </Button>
          </Form.Item>
        </Form>
        <Spin spinning={expireTableSpain}>
          <MyPageTable
            scroll={true}
            onPage={this.expireOnPage}
            paginationProps={paginationProps}
            dataSource={onTableData(expireData)}
            columns={columns}
            rowSelection={false}
          />
        </Spin>
      </Card>
    </PageHeaderWrapper>
  );
};

export default Form.create()(ProcesssStatistics);
