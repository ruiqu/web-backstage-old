import React from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Card, Button, Form, DatePicker, Descriptions, message, Spin } from 'antd';
import { onTableData, objEmpty } from '@/utils/utils.js';
import MyPageTable from '@/components/MyPageTable';
import DescriptionsItem from '@/components/DescriptionsItem';
import request from '@/services/baseService';
import moment from 'moment';

const { MonthPicker, RangePicker } = DatePicker;

@Form.create()
export default class StatisticsPage extends React.Component {
  state = {
    current: 1,

    //----到期统计
    expireCollect: {},
    expireData: [],
    expireCurrent: 1,
    expireSize: 10,
    expireTotal: 0,
    expireLoad: false,
    expireCollectSpain: true,
    expireTableSpain: true,

    dailyRefreshLoad: false,
    dailyRefreshLoadTitle: '刷新',
    //----到期统计
  };

  componentDidMount() {
    this.dailyDataRefresh();
    // this.expireStatistics();
    // this.expireTable({ size: this.state.expireSize });
  }

  componentWillUnmount() {
    clearInterval(this.dailyInterval);
  }

  //---------------到期统计
  expireStatistics = (param = {}) => {
    this.setState({
      expireCollectSpain: true,
    });
    let obj = objEmpty(param);
    request('hzsx/statistics/orderStagesTotalStat ', obj, 'post').then(res => {
      this.setState({
        expireCollect: res,
        expireCollectSpain: false,
      });
      console.log(123, res);
    });
  };

  expireTable = param => {
    this.setState({
      expireTableSpain: true,
    });
    let obj = objEmpty(param);
    request('hzsx/statistics/orderStagesPageStat', obj, 'post').then(res => {
      this.setState({
        expireData: res.records,
        expireTotal: res?.total,
        expireCurrent: res.current,
        expireTableSpain: false,
      });
      setTimeout(() => {
        this.setState({
          expireLoad: false,
        });
      }, 500);
    });
  };
  expireHandleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState({
          expireLoad: true,
        });
        let param = {
          current: this.state.dailyCurrent,
          size: this.state.dailySize,
        };

        if (values.expireRangePicker && values.expireRangePicker.length > 0) {
          param.dateStart = `${moment(values.expireRangePicker[0]).format('YYYY-MM-DD')} 00:00:00`;
          param.dateEnd = `${moment(values.expireRangePicker[1]).format('YYYY-MM-DD')} 23:59:59`;
        } else {
          param.dateStart = '';
          param.dateEnd = '';
        }
        this.expireTable(param);
        this.expireStatistics({
          dateStart: param.dateStart,
          dateEnd: param.dateEnd,
        });
      }
    });
  };
  expireOnPage = e => {
    let { expireRangePicker } = this.props.form.getFieldsValue();
    this.setState(
      {
        expireCurrent: e.current,
        expireSize: e.pageSize,
      },
      () => {
        let param = {
          current: e.current,
          size: e.pageSize,
        };
        console.log(expireRangePicker, 123);
        if (expireRangePicker && expireRangePicker.length > 0) {
          param.dateStart = `${moment(expireRangePicker[0]).format('YYYY-MM-DD')} 00:00:00`;
          param.dateEnd = `${moment(expireRangePicker[1]).format('YYYY-MM-DD')} 23:59:59`;
        } else {
          param.dateStart = '';
          param.dateEnd = '';
        }
        this.expireTable(param);
      },
    );
  };
  //---------------到期统计

  dailyDataRefresh = () => {
    this.setState(
      {
        dailyRefreshLoad: true,
      },
      () => {
        request('hzsx/statistics/refreshStat', {}, 'post').then(res => {
          this.props.form.resetFields();
          let dailyRefreshLoadTitle = 5;
          this.dailyInterval = setInterval(() => {
            dailyRefreshLoadTitle -= 1;
            this.setState({ dailyRefreshLoadTitle: ` ${dailyRefreshLoadTitle}秒` });
            if (dailyRefreshLoadTitle === 0) {
              clearInterval(this.dailyInterval);
              this.setState({
                dailyRefreshLoadTitle: '刷新',
                dailyRefreshLoad: false,
              });
            }
          }, 1000);
          message.info('刷新成功');
          // this.dailyStatistics();
          // this.dailyTable({
          //   size: this.state.dailySize,
          // });

          this.expireStatistics();
          this.expireTable({
            size: this.state.expireSize,
          });
        });
      },
    );
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    //---------------到期统计
    const {
      expireCollect,
      expireData,
      expireCurrent,
      expireSize,
      expireTotal,
      expireLoad,
      dailyRefreshLoad,
      dailyRefreshLoadTitle,
      expireCollectSpain,
      expireTableSpain,
    } = this.state;

    const expireColumns = [
      {
        title: '日期',
        dataIndex: 'statementDate',
      },
      {
        title: '到期订单数',
        dataIndex: 'orderDue',
      },
      {
        title: ' 新用户订单数',
        dataIndex: 'orderDueNew',
      },
      {
        title: ' 老用户订单数',
        dataIndex: 'orderDueOld',
      },
      {
        title: '到期金额数',
        dataIndex: 'amountDue',
      },
      {
        title: '到期成本',
        dataIndex: 'costPriceTotal',
      },
      {
        title: '新用户到期金额数',
        dataIndex: 'amountDueNew',
      },
      {
        title: '老用户到期金额数',
        dataIndex: 'amountDueOld',
      },
      {
        title: '复购数',
        dataIndex: 'againRepurchaseNum',
      },
      {
        title: '金额回款率 ',
        dataIndex: 'amountOverdueRate',
        render: (e, text) => {
          return `${(text.amountOverdueRate * 100).toFixed(2)}%`;
        },
      },
      {
        title: '新用户金额回款率 ',
        dataIndex: 'amountOverdueNewRate',
        render: (e, text) => {
          return `${(text.amountOverdueNewRate * 100).toFixed(2)}%`;
        },
      },
      {
        title: '老用户金额回款率 ',
        dataIndex: 'amountOverdueOldRate',
        render: (e, text) => {
          return `${(text.amountOverdueOldRate * 100).toFixed(2)}%`;
        },
      },
      {
        title: '复购率 ',
        dataIndex: 'againRepurchaseRate',
        render: (e, text) => {
          return `${(text.againRepurchaseRate * 100).toFixed(2)}%`;
        },
      },
    ];

    const expirePaginationProps = {
      current: expireCurrent,
      pageSize: expireSize,
      total: expireTotal,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(expireTotal / expireSize)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };

    const expireDesItem = [
      {
        key: '1',
        title: '到期订单数',
        value: expireCollect.orderDue || 0,
      },
      {
        key: '2',
        title: '新用户订单数',
        value: expireCollect.orderDueNew || 0,
      },
      {
        key: '3',
        title: '老用户订单数',
        value: expireCollect.orderDueOld || 0,
      },
      {
        key: '4',
        title: '到期金额数',
        value: expireCollect.amountDue || 0,
      },
      {
        key: '12',
        title: '到期成本',
        value: expireCollect.costPriceTotal || 0,
      },
      {
        key: '5',
        title: '新用户到期金额数',
        value: expireCollect.amountDueNew || 0,
      },
      {
        key: '6',
        title: '老用户到期金额数',
        value: expireCollect.amountDueOld || 0,
      },
      {
        key: '10',
        title: '复购数',
        value: expireCollect.againRepurchaseNum || 0,
      },
      {
        key: '7',
        title: '金额回款率 ',
        value: `${(expireCollect.amountOverdueRate * 100).toFixed(2)}%`,
      },
      {
        key: '8',
        title: '新用户金额回款率 ',
        value: `${(expireCollect.amountOverdueNewRate * 100).toFixed(2)}%`,
      },
      {
        key: '9',
        title: '老用户金额回款率 ',
        value: `${(expireCollect.amountOverdueOldRate * 100).toFixed(2)}%`,
      },
      {
        key: '11',
        title: ' 复购率',
        value: `${(expireCollect.againRepurchaseRate * 100).toFixed(2)}%`,
      },
    ];

    //---------------到期统计
    return (
      <>
        <PageHeaderWrapper title={false}>
          <Card
            bordered={false}
            style={{
              marginTop: 24,
            }}
            //   className="ant-card antd-pro-pages-finance-capital-account-index-mb20"
          >
            <div style={{ fontSize: 20, marginBottom: 10 }}>
              <strong>到期统计</strong>
              <img src="https://rich-rent.oss-cn-hangzhou.aliyuncs.com/icon/椭圆形 2.png"></img>
            </div>
            <Spin spinning={expireCollectSpain}>
              <DescriptionsItem
                DesItem={expireDesItem}
                colg={{
                  xxl: 4,
                  xl: 4,
                  lg: 4,
                  md: 6,
                  sm: 8,
                  xs: 12,
                }}
              />
            </Spin>
          </Card>
          <Card bordered={false}>
            <Form
              layout="inline"
              onSubmit={this.expireHandleSubmit}
              style={{ marginBottom: '20px' }}
            >
              <Form.Item label="日期筛选">
                {getFieldDecorator('expireRangePicker')(<RangePicker format="YYYY-MM-DD" />)}
              </Form.Item>
              <Form.Item>
                <Button type="primary" htmlType="submit" loading={expireLoad}>
                  查询
                </Button>
              </Form.Item>
              <Form.Item>
                <Button
                  type="primary"
                  onClick={() => this.dailyDataRefresh()}
                  loading={dailyRefreshLoad}
                >
                  {dailyRefreshLoadTitle}
                </Button>
              </Form.Item>
            </Form>
            <Spin spinning={expireTableSpain}>
              <MyPageTable
                scroll={true}
                onPage={this.expireOnPage}
                paginationProps={expirePaginationProps}
                dataSource={onTableData(expireData)}
                columns={expireColumns}
                rowSelection={false}
              />
            </Spin>
          </Card>
        </PageHeaderWrapper>
      </>
    );
  }
}
