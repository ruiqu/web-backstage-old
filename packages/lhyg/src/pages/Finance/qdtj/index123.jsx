import React, { Component } from 'react';
import { Form, Spin, Row, Col, Card, Select, DatePicker, Button, Table } from 'antd';
import { connect } from 'dva';
import request from '@/services/baseService';
import { qdDayEnum } from '@/utils/enum';
import moment from 'moment';
import { RingProgress, Column } from '@ant-design/plots';

const { MonthPicker, RangePicker, WeekPicker } = DatePicker;

@Form.create()
@connect(({ order }) => ({ ...order }))
export default class Home extends Component {
  state = {
    loading: false,
    qdDayEnumKey: '02',
    //渠道数据列表
    dataList: [],
    createTimeStart: null,
    createTimeEnd: null,
  };

  componentDidMount() {
    this.newreqdb();
  }

  reqdb(value) {
    this.setState(
      {
        qdDayEnumKey: value,
      },
      () => {
        this.newreqdb();
      },
    );
  }

  newreqdb(obj = {}) {
    obj.qdDayEnum = this.state.qdDayEnumKey;
    this.setState({
      //数据加载
      loading: true,
    });

    // values.istime = 1;
    request(`/hzsx/business/channel/getChannelDbNew`, obj).then(res => {
      this.setState({
        loading: false,
        dataList: res,
      });
      this.handdb(res);
    });
  }

  //处理成表格数据
  handdb(mydb) {
    //先获取最近七天
    let day7cj = {}; //最近七天成交
    let day7 = {}; //成近七天总订单
    let v总图表 = [];
    let v七天成交排行 = [];
    let v日期数据 = [];
    let v每日成交 = {};
    let v每日下单 = {};
    for (let i = 0; i < mydb.length; i++) {
      const ele = mydb[i];
      mydb[i].v转化率 = ele.v成交单量 ? ((ele.v成交单量 / ele.v订单量) * 100).toFixed(2) : 0;
      for (let key in ele.v渠道成交订单) {
        if (!v每日成交[key]) v每日成交[key] = [];
        v每日成交[key].push({
          name: key,
          day: ele.name,
          val: ele.v渠道成交订单[key],
        });
        if (!day7cj[key]) day7cj[key] = ele.v渠道成交订单[key];
        else day7cj[key] += ele.v渠道成交订单[key];
      }
      for (let key in ele.v渠道订单) {
        if (!v每日下单[key]) v每日下单[key] = [];
        v每日下单[key].push({
          name: key,
          day: ele.name,
          val: ele.v渠道订单[key],
        });
        //先判断日期有没有，如果没 ，那么添加进去
        if (!day7[key]) day7[key] = ele.v渠道订单[key];
        else day7[key] += ele.v渠道订单[key];
      }
    }
    //获取当天日期排行最前的几个是属于哪个
    for (let key in day7) {
      let dbb = {
        name: '订单量',
        day: key,
        val: day7[key],
      };
      v总图表.push(dbb);
    }
    for (let key in day7cj) {
      let dbb = {
        name: '成交量',
        day: key,
        val: day7cj[key],
      };
      v总图表.push(dbb);
    }
    console.log('ele.v渠道订单', v总图表);
    //console.log("ele.每日的成交与下单", this.pickGreatest(v每日成交数据,5),this.pickGreatest(v每日下单数据,5))
    let 每日下单表格数据 = [];
    let 每日成交表格数据 = [];
    for (let key in v每日成交) {
      let db = this.pickGreatest(v每日成交[key], 5);
      for (let key1 in db) {
        每日成交表格数据.push({
          name: db[key1].day,
          day: key,
          val: db[key1].val,
        });
      }
    }
    for (let key in v每日下单) {
      //v每日下单[key] = this.pickGreatest(v每日下单[key], 5)
      let db = this.pickGreatest(v每日下单[key], 5);
      for (let key1 in db) {
        每日下单表格数据.push({
          name: db[key1].day,
          day: key,
          val: db[key1].val,
        });
      }
    }
    //再来两个循环，分别把数据加进对象

    console.log('ele.每日的成交与下单', 每日下单表格数据, 每日成交表格数据);
    this.setState({
      mydb,
      v成交排行: 每日成交表格数据,
      v订单排行: 每日下单表格数据,
      v转化统计: v总图表,
      loading: false,
    });
  }

  handleSubmit = e => {
    e && e.preventDefault();
    this.props.form.validateFields((err, values) => {
      this.setState({
        qdDayEnumKey: '07',
      });
      if (values.createDate) {
        values.createTimeStart = moment(values.createDate[0]).format('YYYY-MM-DD') + ' 00:00:00';
        values.createTimeEnd = moment(values.createDate[1]).format('YYYY-MM-DD') + ' 23:59:59';
      }
      this.newreqdb(values);
    });
  };

  render() {
    const { loading, dataList, qdDayEnumKey, v转化统计 } = this.state;
    const { getFieldDecorator } = this.props.form;
    const ym = 'https://rich-rent.oss-cn-hangzhou.aliyuncs.com/icon/';
    const columns = [
      {
        title: '渠道名称',
        dataIndex: 'name',
        key: 'name',
        width: 100,
      },
      {
        title: 'uv',
        dataIndex: 'uv',
        key: 'uv',
        width: 100,
      },
      {
        title: '注册数量',
        dataIndex: 'registerNum',
        key: 'registerNum',
      },
      {
        title: '实名数量',
        dataIndex: 'realNameNum',
        key: 'realNameNum',
      },
      {
        title: '下单人数',
        dataIndex: 'orderUserNum',
        key: 'orderUserNum',
      },
      {
        title: '机审通过数',
        dataIndex: 'RiskPassNum',
        key: 'RiskPassNum',
      },
      {
        title: '成交订单数',
        dataIndex: 'dealNum',
        key: 'dealNum',
      },
      {
        title: '成交人数',
        dataIndex: '',
      },
      {
        title: '待转化',
        dataIndex: 'daizhuanhua',
        key: 'daizhuanhua',
        render: (e, text) => {
          return text.daizhuanhua ? text.daizhuanhua : '';
        },
      },
      {
        title: '注册率',
        dataIndex: 'registerRate',
        key: 'registerRate',
        render: (e, text) => {
          return text.registerRate ? text.registerRate : '';
        },
      },
      {
        title: '下单率',
        dataIndex: 'orderRate',
        key: 'orderRate',
        render: (e, text) => {
          return text.orderRate ? text.orderRate : '';
        },
      },
      {
        title: '机审通过率',
        dataIndex: 'RiskPassRate',
        key: 'RiskPassRate',
        render: (e, text) => {
          return text.RiskPassRate ? text.RiskPassRate : '';
        },
      },
      {
        title: '成交率',
        dataIndex: 'dealRate',
        key: 'dealRate',
        render: (e, text) => {
          return text.dealRate ? text.dealRate : '';
        },
      },
    ];

    const config = {
      data: v转化统计,
      isGroup: true,
      xField: 'day',
      yField: 'val',
      seriesField: 'name',

      /** 设置颜色 */
      //color: ['#1ca9e6', '#f88c24'],

      /** 设置间距 */
      // marginRatio: 0.1,
      label: {
        // 可手动配置 label 数据标签位置
        position: 'middle',
        // 'top', 'middle', 'bottom'
        // 可配置附加的布局方法
        layout: [
          // 柱形图数据标签位置自动调整
          {
            type: 'interval-adjust-position',
          }, // 数据标签防遮挡
          {
            type: 'interval-hide-overlap',
          }, // 数据标签文颜色自动调整
          {
            type: 'adjust-color',
          },
        ],
      },
    };

    return (
      <div className="index">
        <Spin spinning={loading} delay={500}>
          <Row gutter={16} style={{ height: '400px' }}>
            <Col
              span={24}
              style={{
                marginTop: 10,
                padding: 10,
                fontSize: 16,
                color: '#000000',
                'font-weight': 'bold',
              }}
            >
              渠道数据<img src={ym + '椭圆形 2.png'}></img>
            </Col>
            <Col span={24}>
              <Card bordered={false}>
                <Form layout="inline" onSubmit={this.handleSubmit}>
                  <Row>
                    <Col span={8}>
                      <Form.Item label="渠道来源">
                        {getFieldDecorator('cid', {
                          // initialValue: '',
                        })(
                          <Select placeholder="渠道来源" allowClear style={{ width: 180 }}>
                            {dataList.map(value => {
                              return (
                                <Option value={value.id + ''} key={value.id}>
                                  {value.name}
                                </Option>
                              );
                            })}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col span={8}>
                      <Form.Item label="">
                        {getFieldDecorator('createDate', {})(<RangePicker />)}
                      </Form.Item>
                    </Col>
                    <Col span={24}>
                      {qdDayEnum.map(el => {
                        return (
                          <Button
                            disabled={el.key == qdDayEnumKey}
                            type="primary"
                            onClick={() => this.reqdb(el.key)}
                          >
                            {el.value}
                          </Button>
                        );
                      })}
                      <Button htmlType="submit" type="primary">
                        查询
                      </Button>
                    </Col>
                  </Row>
                </Form>
                <div></div>

                <div>
                  <Table
                    scroll={{ x: true, y: 450 }}
                    columns={columns}
                    dataSource={dataList}
                    pagination={false}
                    //onChange={this.onChange}
                  />
                </div>
              </Card>
            </Col>

            <Col
              span={24}
              style={{
                marginTop: 10,
                padding: 10,
                fontSize: 16,
                color: '#000000',
                'font-weight': 'bold',
              }}
            >
              大盘数据<img src={ym + '椭圆形 2.png'}></img>
            </Col>

            <Col span={24}>
              <Card bordered={false} style={{ 'border-radius': '29px' }}>
                <Column {...config} />
              </Card>
            </Col>
          </Row>
        </Spin>
      </div>
    );
  }
}
