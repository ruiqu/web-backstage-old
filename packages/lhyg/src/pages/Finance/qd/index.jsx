import React, { PureComponent } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import MyPageTable from '@/components/MyPageTable';
import request from '@/services/baseService';
import {
  Tabs,
  Button,
  Select,
  Table,
  Card,
  message,
  Icon,
  Modal,
  Form,
  Input,
  DatePicker,
  Switch,
  Spin,
  Row,
  Col,
  Upload,
} from 'antd';
import { uploadUrl } from '@/config';
import { getToken } from '@/utils/localStorage';
const { TabPane } = Tabs;
import moment from 'moment';
const { RangePicker } = DatePicker;
const { Option } = Select;
import { onTableData } from '@/utils/utils.js';
const QRCode = require('qrcode.react');
@connect(({ messageIndex }) => ({
  ...messageIndex,
}))
@Form.create()
class MessageSetting extends PureComponent {
  state = {
    loading: false,
    onChannel: '1', //默认
    newChannel: null,
    pageNumber: 1,
    pageSize: 10,
    total: 10,
    current: 1,
    selectedRowKeys: [],
    addvisibe: false, //控制添加modal的
    mobile: 'Banner添加', //添加modal的标题
    imgSrc: '', //上传成功后的url
    qname: '', //渠道名称
    istime: false,
    lookewm: false, //控制添加modal的
    ewmurl: '', //控制添加modal的
    createTimeStart: '', //查询开始时间
    createTimeEnd: '', //查询结束时间
    nameTitle: 'Banner名称',
    tabActiveKey: '1', //tab组件默认激活的
    editVisible: false,
    modalScore: 'add',
    address: 'banner', //编辑modal，目标位置的默认值
    name: null,
    url: null,
    sort: null,
    status: null,
    img: null,
    id: null,
    channelData: [],
    // 增加、编辑相关
    level: null,
    mobile: null,
    channelOpenFlag: '0',
    otherRisk:'1',
    type: 1,
    tempKeywords: [],
    remark: null,
    channelScore: '',
    tempTime: null,
    tempChannel: null,
    tid: null,
    fileList: [],
    isDeleteFile: false,
    setPreviewImage: '',
    tableSpinning: false,
  };
  level = {
    1: '授权登录后但是未成功下单',
    2: '点击立即租赁但未成功下单',
    3: '收藏成功后未成功下单',
  };

  disabledClick = checked => {
    request(
      'hzsx/business/channel/saveOrUpdate',
      {
        id: checked.id,
      },
      'post',
    ).then(res => {
      this.getList();
    });
  };
  // List表头
  columns = [
    {
      title: 'id',
      dataIndex: 'id',
      align: 'center',
      width: '10%',
    },
    {
      title: '渠道名称',
      dataIndex: 'name',
      align: 'center',
      width: 200,
    },
    {
      title: '手机号',
      width: 150,
      dataIndex: 'mobile',
      align: 'center',
    }, 
    {
      title: '风控级别',
      width: 150,
      dataIndex: 'otherRisk',
      align: 'center',
      render:(text, record) => {
        if(record.otherRisk == '0'){
          return '紧'
        }
        if(record.otherRisk == '1'){
          return '松'
        }
        if(record.otherRisk == '2'){
          return '白'
        }
      }
    },
    {
      title: '渠道状态',
      dataIndex: 'channelOpenFlag',
      width: 150,
      align: 'center',
      render: (text, record) => {
        if (record.channelOpenFlag == '0') {
          return '无';
        }
        if (record.channelOpenFlag == '1') {
          return '黑名单';
        }
        if (record.channelOpenFlag == '2') {
          return '白名单';
        }
      },
    },
    {
      title: '渠道禁用',
      dataIndex: 'isDisabled',
      align: 'center',
      width: 150,
      render: (text, record) => {
        return (
          <Switch
            checkedChildren="启用"
            unCheckedChildren="禁用"
            checked={record.isDisabled == '0' ? true : false}
            onChange={() => this.disabledClick(record)}
          />
        );
      },
    },
    {
      title: 'uv',
      dataIndex: 'uv',
      key: 'uv',
      width: 100,
    },
    {
      title: '注册数量',
      dataIndex: 'registerNum',
      key: 'registerNum',
      width: 100,
    },
    {
      title: '实名数量',
      dataIndex: 'realNameNum',
      key: 'realNameNum',
      width: 100,
    },
    {
      title: '下单人数',
      dataIndex: 'orderUserNum',
      key: 'orderUserNum',
      width: 100,
    },
    {
      title: '下单数量',
      dataIndex: 'orderNum',
      key: 'orderNum',
      width: 100,
    },
    {
      title: '机审通过数',
      dataIndex: 'riskPassNum',
      key: 'riskPassNum',
      width: 120,
    },
    {
      title: '成交订单数',
      dataIndex: 'dealNum',
      key: 'dealNum',
      width: 120,
    },
    // {
    //   title: '成交人数',
    //   dataIndex: '',
    //   width: 100,
    // },
    {
      title: '待转化',
      dataIndex: 'daizhuanhua',
      key: 'daizhuanhua',
      width: 100,
      render: (e, text) => {
        return text.daizhuanhua ? text.daizhuanhua : '';
      },
    },
    {
      title: '操作',
      align: 'center',
      width: 100,
      fixed: 'right',
      render: (text, record) => {
        return (
          <div>
            {/* <span
              style={{ cursor: 'pointer', wordBreak: 'keep-all', marginLeft: 15, color: '#5476F0' }}
              onClick={() => this.copy(record)}
            >
              复制
            </span>
            <span
              style={{ cursor: 'pointer', wordBreak: 'keep-all', marginLeft: 15, color: '#5476F0' }}
              onClick={() => this.copy(record, true)}
            >
              网址
            </span>
            <span
              style={{ cursor: 'pointer', wordBreak: 'keep-all', marginLeft: 15, color: '#5476F0' }}
              onClick={() => this.ewm(record, true)}
            >
              二维码
            </span>
            <br></br> */}

            <span
              style={{ cursor: 'pointer', wordBreak: 'keep-all', marginLeft: 15, color: '#5476F0' }}
              onClick={() => this.copyapp(record)}
            >
              app
            </span>
            <span
              style={{ cursor: 'pointer', wordBreak: 'keep-all', marginLeft: 15, color: '#5476F0' }}
              onClick={() => this.copyapp(record, true)}
            >
              app二维码
            </span>
            <span
              style={{ cursor: 'pointer', wordBreak: 'keep-all', marginLeft: 15, color: '#5476F0' }}
              onClick={() => this.copyLoginApp(record, true)}
            >
              落地页网址
            </span>
            <span
              style={{ cursor: 'pointer', wordBreak: 'keep-all', marginLeft: 15, color: '#5476F0' }}
              onClick={() => this.edit(record)}
            >
              修改
            </span>
            <span
              style={{ cursor: 'pointer', wordBreak: 'keep-all', marginLeft: 15, color: '#5476F0' }}
              onClick={() => this.del(record.id)}
            >
              删除
            </span>
          </div>
        );
      },
    },
  ];

  componentDidMount() {
    this.getList();
    //this.getChannel();
  }
  //获取渠道信息
  getChannel = () => {
    this.props.dispatch({
      type: 'order/PlateformChannel',
      payload: {},
      callback: res => {
        this.setState({
          channelData: res.data,
        });
      },
    });
  };
  //获取List的数据
  getList = () => {
    this.setState(
      {
        loading: true,
        tableSpinning: true,
      },
      () => {
        console.log('获取渠道首页');
        let url = `/hzsx/business/channel/getChannelListNew`;
        /* if (this.state.istime) {
          url = `/hzsx/business/channel/getChannelListByDate`
        } */
        let param = {
          current: this.state.pageNumber,
          size: this.state.pageSize,
          createTimeStart: this.state.createTimeStart,
          createTimeEnd: this.state.createTimeEnd,
          name: this.state.qname,
        };
        request(url, param, 'post').then(res => {
          console.log(res, '返回的列表数据');
          message.success('获取信息成功');
          this.setState({
            total: res.total,
            channelData: res.records,
            current: res.current,
            loading: false,
            tableSpinning: false,
          });
        });
        return;
        this.props.dispatch({
          type: 'messageIndex/getList',
          payload: {
            pageNumber: this.state.pageNumber,
            pageSize: this.state.pageSize,
          },
          callback: res => {
            console.log(res);
            if (res.responseType === 'SUCCESS') {
              message.success('获取信息成功');
              this.setState({
                total: res.data.total,
                current: res.data.current,
                loading: false,
              });
            } else {
              message.error(res.msg);
            }
          },
        });
      },
    );
  };
  //删除渠道
  del = id => {
    //return console.log("开始删除")
    request(`/hzsx/business/channel/delChannel?channelId=${id}`, {}, 'get').then(res => {
      message.success('删除成功');
      this.getList();
    });
  };

  edit = record => {
    if (record.back_img) {
      this.setState(
        {
          fileList: [
            { uid: record.back_img, url: record.back_img, src: record.back_img, isMain: null },
          ],
        },
        () => {
          this.props.form.setFieldsValue({
            backImg: record.back_img,
          });
        },
      );
    }
    this.setState(
      {
        addvisibe: true,
        modalScore: 'edit',
        tid: record.id,
        tempKeywords: JSON.parse(record.keywords || '[]'),
      },
      () => {
        console.log(789, record);
        this.props.form.setFieldsValue({
          tempChannel: record.channelId,
          remark: record.mark,
          channelScore: record.channelScore,
          mobile: record.mobile,
          type: record.status,
          tempTime: record.triggerInterval,
          level: record.level,
          name: record.name,
          id: record.id,
          channelOpenFlag: record.channelOpenFlag,
          otherRisk:record.otherRisk,
          backImg: record.back_img,
        });
      },
    );
  };
  createqr = record => {
    //console.log(record)
    return this.urlcreate(record.id, record.appid, true);
  };
  ewm = record => {
    let url = this.urlcreate(record.id, record.appid, true);

    console.log('erm:', url);
    this.setState({
      lookewm: true,
      ewmurl: url,
    });
  };

  copyLoginApp = record => {
    let url =
      'https://' + window.location.host + '/#/src/pages/login/index?cid=' + record.id + '&type=qd';
    this.copy11(url);
  };

  copy = (record, isurl = false) => {
    this.copy11(this.urlcreate(record.id, record.appid, isurl));
  };

  copyapp = (record, ewm = false) => {
    let url = 'https://' + window.location.host + '/#/?cid=' + record.id;
    if (ewm) {
      this.setState({
        lookewm: true,
        ewmurl: url,
      });
    } else {
      this.copy11(url);
    }
  };

  copy11 = url => {
    let transfer = document.createElement('input');
    document.body.appendChild(transfer);
    transfer.value = url; // 这里表示想要复制的内容
    transfer.focus();
    transfer.select();
    if (document.execCommand('copy')) {
      //判断是否支持document.execCommand('copy')       document.execCommand('copy');
    }
    transfer.blur();
    message.success('复制成功');
    document.body.removeChild(transfer);
  };

  urlcreate = (id, appid, isurl = false) => {
    //alipays://platformapi/startapp?appId=1231111&page=pages/iii&query=pid%3D1
    let url = 'alipays://platformapi/startapp?appId=' + appid + '&page=src/pages/index/index';
    url += '&query=cid%3D' + id;
    if (isurl) url = 'https://ds.alipay.com/?scheme=' + encodeURIComponent(url);
    return url;
  };

  handleFilter = () => {
    this.getList();
  };

  addTempKeyword() {
    const tempKeywords = [...this.state.tempKeywords];
    tempKeywords.push({
      keyword: '',
      value: '',
    });
    //console.log(tempKeywords)
    this.setState({
      tempKeywords,
    });
  }
  deleteTempKeyword(index) {
    const tempKeywords = [...this.state.tempKeywords];
    tempKeywords.splice(index, 1);
    //console.log(tempKeywords)
    this.setState({
      tempKeywords,
    });
  }
  save(e, index, type) {
    const tempKeywords = [...this.state.tempKeywords];
    tempKeywords[index][type] = e.currentTarget.value;
    //console.log(tempKeywords)
    this.setState(
      {
        tempKeywords,
      },
      () => {
        this.props.form.validateFields(['tempKeywords'], { force: true });
      },
    );
  }

  //分页，下一页
  onChange = pageNumber => {
    // alert(pageNumber)
    this.setState(
      {
        pageNumber: pageNumber.current,
      },
      () => {
        this.handleFilter();
      },
    );
  };
  showTotal = () => {
    return `共有${this.state.total}条`;
  };
  //切换每页数量
  onShowSizeChange = pageSize => {
    //console.log(pageSize, 'pageSize');
    this.setState(
      {
        pageSize: pageSize.current,
        pageNumber: 1,
      },
      () => {
        this.handleFilter();
      },
    );
  };
  //表格多选框的回调
  onSelectChange = (selectedRowKeys, selectedRows) => {
    this.setState({ selectedRowKeys });
  };

  // 新增
  add = () => {
    this.setState(
      {
        addvisibe: true,
        modalScore: 'add',
        tid: '',
        tempKeywords: [],
      },
      () => {
        this.props.form.setFieldsValue({
          tempChannel: null,
          remark: null,
          channelScore: '',
          tempKeywords: [],
          mobile: null,
          type: 1,
          tempTime: null,
          level: null,
          name: null,
          channelOpenFlag: '0',
          otherRisk:'1'
        });
      },
    );
  };
  // modal的确认，确认添加和确认更新的
  addOk = () => {
    this.setState({ loading: true });
    this.props.form.validateFields({ force: true }, (err, value) => {
      if (!err) {
        let param = {
          channelId: value.tempChannel,
          mark: value.remark || '',
          mobile: value.mobile,
          level: value.level,
          password: value.password,
          name: value.name,
          channelOpenFlag: value.channelOpenFlag,
          otherRisk:value.otherRisk,
          backImg: this.state.fileList?.[0]?.response?.data,
        };
        if (value.channelScore) {
          param.channelScore = value.channelScore;
        }
        let url = `/hzsx/business/channel/insertChannel`;

        if (this.state.modalScore != 'add') {
          //如果是编辑
          url = `/hzsx/business/channel/uptChannel`;
          param.id = this.state.tid;
        }
        //return console.log(param, url)
        request(url, param, 'post')
          .then(resData => {})
          .finally(() => {
            this.setState({ loading: false, addvisibe: false });
            this.getList();
          });
      }
    });
  };
  tempKeywordsValidator = (rule, val, callback) => {
    val = this.state.tempKeywords;
    if (!(val && val.length)) {
      callback('模板关键词不能为空');
    }
    let validateResult = true;
    val.forEach(item => {
      if (!(item.keyword && item.value)) {
        validateResult = false;
      }
    });
    if (!validateResult) {
      callback('模板关键词不能有空格');
    }
    callback();
  };
  tempTimeValidator = (rule, val, callback) => {
    if (/^[0-6]$/.test(val)) {
      callback();
    } else {
      callback('只能输入0-6的数字');
    }
  };
  //查询
  handleSubmit1 = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (values.createDate && values.createDate.length > 1) {
        values.createTimeStart =  `${moment(values.createDate[0]).format('YYYY-MM-DD')} 00:00:00`;
        values.createTimeEnd  =`${moment(values.createDate[1]).format('YYYY-MM-DD')} 23:59:59`;
        values.istime = true;
      }else{
        values.createTimeStart = ''
        values.createTimeEnd = ''
      }
      this.setState(
        {
          loading: true,
          ...values,
        },
        () => {
          this.getList();
          console.log('values', values);
        },
      );
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      this.setState(
        {
          loading: true,
        },
        () => {
          console.log('values', values);
          this.props.dispatch({
            type: 'messageIndex/getList',
            payload: {
              pageNumber: 1,
              pageSize: 10,
              mobile: values.title1,
              channelId: values.tempChannel1,
            },
            callback: res => {
              console.log(res);
              if (res.responseType === 'SUCCESS') {
                message.success('获取信息成功');
                this.setState({
                  total: res.data.total,
                  current: res.data.current,
                  loading: false,
                });
              } else {
                message.error(res.msg);
              }
            },
          });
        },
      );
    });
  };

  handleReset = e => {
    this.props.form.resetFields();
    this.props.form.setFieldsValue({
      status: undefined,
      type: undefined,
    });
    this.handleSubmit(e);
  };

  render() {
    const {
      total,
      current,
      addvisibe,
      modalScore,
      channelData,
      level,
      name,
      mobile,
      type,
      tempKeywords,
      remark,
      channelScore,
      ewmurl,
      lookewm,
      tempChannel,
      channelOpenFlag,
      fileList,
      setPreviewImage,
      isDeleteFile,
      tableSpinning,
    } = this.state;
    const paginationProps = {
      current: current,
      pageSize: 10,
      total: total,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / 10)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };

    const handlePreview = ({ thumbUrl, url }) => {
      const imgUrl = thumbUrl ? thumbUrl : url;

      this.setState({
        setPreviewImage: imgUrl,
      });

      // setPreviewImage(imgUrl);
    };

    // 限制用户上传大小与格式
    const beforeUpload = file => {
      const isJPG =
        file.type === 'image/jpeg' || file.type === 'image/png' || file.type === 'image/gif';
      if (!isJPG) {
        message.error('图片格式不正确');
      }
      const isLt2M = file.size / 1024 / 1024 < 2;
      if (!isLt2M) {
        message.error('图片大于2MB');
      }
      return isJPG && isLt2M;
    };

    const handOnRemove = file => {
      const { form } = this.props;
      console.log(123, form);
      form.setFieldsValue({
        backImg: '',
      });

      this.setState(
        {
          fileList: [],
          isDeleteFile: true,
        },
        () => {
          handleUploadImage({ file: {}, fileList: [] });
        },
      );
    };

    const handleUploadImage = ({ file, fileList }) => {
      if (isDeleteFile) {
        this.setState({
          isDeleteFile: false,
        });
        return;
      }
      console.log('onChange', file, fileList);
      if (file && file.status === 'done') {
        console.log(123, file, fileList);
        let arrays = [file];
        console.log(999, arrays);
        const images = arrays.map((v, index) => {
          if (v.response) {
            const src = v.response.data;
            return { uid: index, src: src, url: src, isMain: v.isMain || null };
          }
          return v;
        });
        this.setState({
          fileList: images,
        });
        // setFileList(images);
      }
      if (JSON.stringify(file) !== '{}')
        this.setState({
          fileList: [file],
        });
      // setFileList(fileList);
    };

    const checkMainImages = (_, value, callback) => {
      if (!value && fileList.length == 0) {
        callback('请上传主图');
        return;
      }
      callback();
    };

    const { list } = this.props;
    const editColumns = [
      {
        mobile: '关键词',
        dataIndex: 'keyword',
        editable: true,
        width: '40%',
        render: (text, record, index) => (
          <Input
            defaultValue={text}
            onPressEnter={e => this.save(e, index, 'keyword')}
            onBlur={e => this.save(e, index, 'keyword')}
          />
        ),
      },
      {
        mobile: '内容',
        dataIndex: 'value',
        editable: true,
        width: '40%',
        render: (text, record, index) => (
          <Input
            defaultValue={text}
            onPressEnter={e => this.save(e, index, 'value')}
            onBlur={e => this.save(e, index, 'value')}
          />
        ),
      },
      {
        mobile: (
          <Icon
            onClick={() => this.addTempKeyword()}
            style={{ cursor: 'pointer' }}
            type="plus-circle"
          />
        ),
        dataIndex: 'operation',
        align: 'center',
        width: '20%',
        render: (text, record, index) => {
          return (
            <Icon
              onClick={() => this.deleteTempKeyword(index)}
              style={{ cursor: 'pointer' }}
              type="minus-circle"
            />
          );
        },
      },
    ];

    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };

    return (
      <PageHeaderWrapper title={false}>
        <Card bordered={false}>
          <Form layout="inline" onSubmit={this.handleSubmit1}>
            <Row>
              <Col span={4}>
                <Form.Item label="">
                  {getFieldDecorator(
                    'qname',
                    {},
                  )(<Input allowClear placeholder="请输入渠道名称" />)}
                </Form.Item>
              </Col>

              <Col span={8}>
                <Form.Item label="">
                  {getFieldDecorator('createDate', {})(<RangePicker />)}
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item>
                  <Button type="primary" htmlType="submit">
                    查询
                  </Button>
                </Form.Item>
                <Form.Item>
                  <Button type="primary" onClick={this.add}>
                    新增
                  </Button>
                </Form.Item>
              </Col>
            </Row>
            <div></div>
          </Form>
          <Spin spinning={tableSpinning}>
            <MyPageTable
              paginationProps={paginationProps}
              dataSource={channelData}
              scroll
              bordered={false}
              columns={this.columns}
              onPage={this.onChange}
            />
          </Spin>
        </Card>
        <Modal
          mobile={modalScore == 'add' ? '新增' : '修改'}
          visible={addvisibe}
          onCancel={() => {
            this.setState({
              addvisibe: false,
              fileList: [],
              isDeleteFile: true,
            });
            this.props.form.resetFields();
          }}
          onOk={this.addOk}
        >
          <Form {...formItemLayout}>
            <Form.Item label="渠道名称">
              {getFieldDecorator('name', {
                rules: [
                  {
                    required: true,
                    message: '渠道名称不能为空',
                  },
                ],
                initialValue: name ? name : null,
              })(<Input />)}
            </Form.Item>
            <Form.Item label="手机号">
              {getFieldDecorator('mobile', {
                rules: [
                  {
                    required: true,
                    message: '手机号不能为空',
                  },
                ],
                initialValue: mobile ? mobile : null,
              })(<Input />)}
            </Form.Item>
            <Form.Item label="密码">
              {getFieldDecorator('password', {})(<Input type="password" />)}
            </Form.Item>
            <Form.Item label="风控级别">
              {getFieldDecorator('otherRisk', {
                rules: [{ required: true, message: '请选择风控级别!' }],
              })(
                <Select placeholder="请选择风控级别">
                  <Option value="0">紧</Option>
                  <Option value="1">松</Option>
                  <Option value="2">白</Option>
                </Select>,
              )}
            </Form.Item>

            <Form.Item label="渠道开关">
              {getFieldDecorator('channelOpenFlag', {
                rules: [{ required: true, message: '请选择渠道开关!' }],
              })(
                <Select placeholder="请选择渠道开关">
                  <Option value="0">无</Option>
                  <Option value="1">黑名单</Option>
                  <Option value="2">白名单</Option>
                </Select>,
              )}
            </Form.Item>
            <Form.Item label="备注">
              {getFieldDecorator('remark', {
                initialValue: remark ? remark : null,
              })(<Input />)}
            </Form.Item>
            <Form.Item label="风控通过分" help="默认请填写分数0.3">
              {getFieldDecorator('channelScore', {
                initialValue: channelScore ? channelScore : '',
                rules: [{ required: true, message: '请填写风控分数' }],
              })(<Input />)}
            </Form.Item>

            <Form.Item label="背景图片">
              {getFieldDecorator('backImg', {
                // initialValue: goodsDetail.images,
                // rules: [{ validator: checkMainImages }],
              })(
                <div className="upload-desc">
                  <Upload
                    accept="image/*"
                    action={uploadUrl}
                    listType="picture-card"
                    headers={{
                      token: getToken(),
                    }}
                    fileList={fileList}
                    onPreview={handlePreview} //预览
                    beforeUpload={beforeUpload}
                    onChange={handleUploadImage}
                    onRemove={handOnRemove}
                  >
                    <div>
                      <Icon type="upload" />
                      <div className="ant-upload-text">上传照片</div>
                    </div>
                  </Upload>
                  <div className="size-desc w294">
                    {/* <span>● 尺寸为700x700px及以上长方形</span>
                    <br /> */}
                    <span>● 不能出现商家logo、水印、电话、微信等联系方式</span>
                    <br />
                  </div>
                </div>,
              )}
            </Form.Item>
          </Form>
        </Modal>
        <Modal
          mobile="渠道二维码"
          visible={lookewm}
          onCancel={() => {
            this.setState({
              lookewm: false,
            });
          }}
        >
          <QRCode value={ewmurl} size={256} id={'qrCode'} />
        </Modal>
        <Modal
          visible={!!setPreviewImage}
          footer={null}
          onCancel={() =>
            this.setState({
              setPreviewImage: '',
            })
          }
          width={600}
        >
          <img src={setPreviewImage} alt="" style={{ width: '100%', paddingTop: 15 }} />
        </Modal>
      </PageHeaderWrapper>
    );
  }
}

export default MessageSetting;
