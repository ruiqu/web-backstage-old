import { PageHeaderWrapper } from '@ant-design/pro-layout';
import React, { Component } from 'react';
import { Card, Table } from 'antd';
import moment from 'moment';
import settlementService from './services/settlement';
import statusMap from '@/config/status.config';

export default class settlement extends Component {
  state = {
    tableData: [],
    loading: true,
    total: 1,
    pageNumber: 1,
    pageSize: 10,
  };
  componentDidMount() {
    this.onList();
  }
  handleFilter = (data = {}) => {
    data.settleDate =
      moment(data.settleDate).format('YYYY-MM-DD') === 'Invalid date'
        ? undefined
        : moment(data.settleDate).format('YYYY-MM-DD') + ' 00:00:00';
    this.onList(data);
  };
  onList = (data = {}) => {
    const { pageNumber, pageSize } = this.state;
    this.setState(
      {
        loading: true,
      },
      () => {
        settlementService.queryAccountPeriodPage({ pageNumber, pageSize, ...data }).then(res => {
          this.setState({
            total: res.total,
            tableData: res.records,
            loading: false,
          });
        });
      },
    );
  };
  onChange = e => {
    this.setState(
      {
        pageNumber: e,
      },
      () => {
        this.onList();
      },
    );
  };
  showTotal = () => {};
  onShowSizeChange = () => {};

  columns = [
    // {
    //   title: '合作方式',
    //   dataIndex: 'appVersion',
    //   render: val => {
    //     const vMap = {
    //       ZWZ: '淘淘享租',
    //       LITE: '简版小程序',
    //       TOUTIAO: '抖音小程序',
    //     }
    //     const cn = vMap[val] || '-'
    //     return cn
    //   }
    // },
    {
      title: '结算日期',
      dataIndex: 'settleDate',
      align: 'center',
      render:e=>moment(e).format('YYYY-MM-DD')
    },
    {
      title: '结算金额（元）',
      dataIndex: 'totalSettleAmount',
      align: 'center',
    },
    {
      title: '佣金（元）',
      dataIndex: 'totalBrokerage',
      align: 'center',
    },
    {
      title: '结算状态',
      dataIndex: 'status',
      align: 'center',
      render: e => statusMap[e],
    },
    {
      title: '操作',
      align: 'center',

      render: (text, record) => {
        return (
          <div className="table-action">
            <a href={`#/finance/settlement/detail?id=${record.id}`} target="_blank">查看</a>
          </div>
        );
      },
    },
  ];
  render() {
    const { tableData, loading, total, pageNumber } = this.state;

    return (
      <PageHeaderWrapper  title={false}>
        <Card bordered={false} style={{ marginTop: 20 }}>
          <Table
            columns={this.columns}
            loading={loading}
            dataSource={tableData}
            rowKey={record => record.id}
            pagination={{
              current: pageNumber,
              total: total,
              pageSize: 10,
              onChange: this.onChange,
              showTotal: total => (
                <span style={{ fontSize: '14px' }}>
                  <span>共{Math.ceil(total / 10)}页</span>&emsp;
                  <span>共{total}条</span>
                </span>
              ),
            }}
          />
        </Card>
      </PageHeaderWrapper>
    );
  }
}
