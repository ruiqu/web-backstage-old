import { Component } from 'react';
import Statics from './components/statics';
import DataPoll from './components/dataPoll';
import Expire from './components/thirdLine/expire';
import Board from './components/thirdLine/board';
import MarketData from './components/marketData';
import styles from './index.less';
import request from '@/services/baseService';

export default class Home extends Component {
  state = {
    /** 顶部数据 */
    headerData: {
      /** 注册数 */
      registerNum: 0,
      /** 订单数 */
      orderNum: 0,
      /** 成交数 */
      dealNum: 0,
      /** 成交金额 */
      rent: 0,
      /** 转化率 */
      placeOrderRate: '0%',
      /** 机审通过数 */
      riskPassNum: 0,
      /** 到期金额数 */
      amountDue: 0,
      /** 支付金额数 */
    },
  };

  componentDidMount() {
    // this.getStatisticsData();
    // this.getExpireData();
  }

  /**
   * 获取每日统计
   */
  getStatisticsData() {
    request('hzsx/statistics/orderPageStat', { size: 10 }, 'post').then(res => {
      // this.setState({
      //   dayStatistics: res.records[0],
      //   yesStatistics: res.records[1],
      // });
    });
  }

  /**
   * 获取到期统计
   */
  getExpireData() {
    request('hzsx/statistics/orderStagesPageStat', { size: 10 }, 'post').then(res => {
      this.setState({
        dayExpire: res.records[0],
        yesExpire: res.records[1],
      });
    });
  }

  render() {
    return (
      <div>
        <Statics />
        <DataPoll />
        <div className={styles.thirdLine}>
          <Expire />
          <Board />
        </div>
        <MarketData />
      </div>
    );
  }
}
