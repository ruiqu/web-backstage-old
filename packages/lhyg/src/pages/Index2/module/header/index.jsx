import { Component } from 'react';
import styles from './index.less';

export default class headerModule extends Component {
  render() {
    const { name, rightText } = this.props;
    return (
      <div className={styles.mains}>
        <div className={styles.title}>{name}</div>
        {rightText ? <div className={styles.rightText}>{rightText}</div> : ''}
      </div>
    );
  }
}
