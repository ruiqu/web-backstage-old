import { Component } from 'react';
import HeaderModule from '../../module/header';
import styles from './index.less';
import ReactEcharts from 'echarts-for-react';
import * as echarts from 'echarts';

export default class MarketData extends Component {
  statisticalData = () => {
    let option = {
      grid: {
        left: '90',
        right: '40',
        top: '40',
        bottom: '40',
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross',
          label: {
            backgroundColor: '#6a7985',
          },
        },
      },
      xAxis: {
        type: 'category',
        boundaryGap: false,
        data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
        textStyle: {
          color: '#f44336',
        },
        axisLabel: {
          textStyle: {
            color: 'rgba(153, 153, 153, 1)',
          },
        },
        axisLine: {
          lineStyle: {
            color: '#fff', //x轴轴线颜色
          },
        },
      },
      yAxis: {
        type: 'value',
        offset: '30',
        axisLabel: {
          textStyle: {
            color: 'rgba(153, 153, 153, 1)',
          },
        },
        splitLine: {
          lineStyle: {
            type: 'dashed',
            color: 'rgba(0, 0, 0, 0.10)',
          },
        },
      },
      series: [
        {
          smooth: true,
          data: [820, 932, 901, 634, 1290, 630, 1320],
          type: 'line',

          label: {
            show: true,
            color: '#1774F9',
          },
          lineStyle: {
            color: '#1774F9',
          },
          areaStyle: {
            normal: {
              color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                {
                  offset: 1,
                  color: 'rgba(31, 121, 249, 0)', // 0% 处的颜色
                },
                {
                  offset: 0,
                  color: 'rgba(31, 121, 249, 0.60)', // 100% 处的颜色
                },
              ]), //背景渐变色
            },
          },
        },
      ],
    };
    return option;
  };

  render() {
    return (
      <div className={styles.main}>
        <HeaderModule name="大盘数据" />
        <div className={styles.content}>
          <ReactEcharts
            style={{ height: '250px', width: '100%' }}
            option={this.statisticalData()}
          />
        </div>
      </div>
    );
  }
}
