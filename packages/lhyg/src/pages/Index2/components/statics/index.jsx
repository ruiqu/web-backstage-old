import { Component } from 'react';
import HeaderModule from '../../module/header';
import styles from './index.less';

export default class statics extends Component {
  state = {
    dayStatistics: {},
    yesStatistics: {},
    dayExpire: {},
    yesExpire: {},
  };

  /**
   * 获取试用中数据
   */
  getTrialData() {
    // request('hzsx/business/order/queryOrderByRentCount', { createTimeEnd: createTimeEnd,createTimeStart:createTimeStart }, 'post')
  }

  render() {
    const { dayStatistics } = this.state;

    return (
      <div className={styles.main}>
        <div className={styles.box}>
          <HeaderModule name="注册数：" rightText="今日" />
          <div>{dayStatistics.registerNum}</div>
        </div>
        <div className={styles.box}>
          <HeaderModule name="订单数：" rightText="今日" />
          <div>{dayStatistics.orderNum}</div>
        </div>
        <div className={styles.box}>
          <HeaderModule name="成交数：" rightText="今日" />
          <div>{dayStatistics.dealNum}</div>
        </div>
        <div className={styles.box}>
          <HeaderModule name="成交金额：" rightText="今日" />
          <div>{dayStatistics.rent}</div>
        </div>
        <div className={styles.box}>
          <HeaderModule name="转化率：" rightText="今日" />
          <div>{dayStatistics.placeOrderRate}</div>
        </div>
        <div className={styles.box}>
          <HeaderModule name="机审通过率：" rightText="今日" />
          <div>{dayStatistics?.riskPassNum}</div>
        </div>
        <div className={styles.box}>
          <HeaderModule name="到期金额数：" rightText="今日" />
          {/* <div>{dayExpire?.amountDue}</div> */}
        </div>
        <div className={styles.box}>
          <HeaderModule name="支付金额数：" rightText="今日" />
          {/* <div>{dayExpire.}</div> */}
        </div>
        <div className={styles.box}>
          <HeaderModule name="到期数：" rightText="今日" />
        </div>
        <div className={styles.box}>
          <HeaderModule name="已付数：" rightText="今日" />
        </div>
      </div>
    );
  }
}
