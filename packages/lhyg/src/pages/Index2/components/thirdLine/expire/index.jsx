import { Component } from 'react';
import HeaderModule from '../../../module/header';
import styles from './index.less';
import ReactEcharts from 'echarts-for-react';

export default class Expire extends Component {
  componentDidMount() {
    // this.getOverdueRate();
  }

  overdueRate = () => {
    let option = {
      tooltip: {
        trigger: 'item',
      },
      series: [
        {
          name: '订单逾期率',
          type: 'pie',
          radius: ['40%', '70%'],
          avoidLabelOverlap: false,
          padAngle: 5,
          label: {
            formatter: '{d}%',
          },
          labelLine: {
            length: 5,
            length2: 10,
          },
          data: [
            {
              value: 1048,
              name: '到期金额',
              itemStyle: {
                color: '#05D9CD',
              },
              label: {
                color: '#05D9CD',
                fontSize: 12,
              },
              formatter: '',
            },
            {
              value: 735,
              name: '支付金额',
              itemStyle: {
                color: '#1774F9',
              },
              label: {
                color: '#1774F9',
                fontSize: 12,
              },
            },
          ],
          emphasis: {
            itemStyle: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: 'rgba(0, 0, 0, 0.5)',
            },
          },
        },
      ],
    };

    return option;
  };

  expireRate = () => {
    let option = {
      tooltip: {
        trigger: 'item',
      },
      series: [
        {
          name: '订单逾期率',
          type: 'pie',
          radius: ['40%', '70%'],
          avoidLabelOverlap: false,
          padAngle: 5,
          label: {
            formatter: '{d}%',
          },
          labelLine: {
            length: 5,
            length2: 10,
          },
          data: [
            {
              value: 1048,
              name: '到期数量',
              itemStyle: {
                color: '#05D9CD',
              },
              label: {
                color: '#05D9CD',
                fontSize: 12,
              },
              formatter: '',
            },
            {
              value: 735,
              name: '已付数量',
              itemStyle: {
                color: '#1774F9',
              },
              label: {
                color: '#1774F9',
                fontSize: 12,
              },
            },
          ],
          emphasis: {
            itemStyle: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: 'rgba(0, 0, 0, 0.5)',
            },
          },
        },
      ],
    };

    return option;
  };

  render() {
    return (
      <div className={styles.mains}>
        <HeaderModule name="到期数据" />
        <div className={styles.content}>
          <div className={styles.pic}>
            <div className={styles.overduePic}>
              <ReactEcharts
                style={{ height: '130px', width: '100%' }}
                option={this.overdueRate()}
              />
              <div className={styles.overdueTitle}>订单逾期率</div>
            </div>
            <div className={styles.overduePic}>
              <ReactEcharts style={{ height: '130px', width: '100%' }} option={this.expireRate()} />
              <div className={styles.overdueTitle}>金额逾期率</div>
            </div>
          </div>
          <div className={styles.progress}>
            <div className={styles.progressTitle}>
              <div className={styles.progressTitleName} style={{ flex: 1 }}>
                类型
              </div>
              <div className={styles.progressTitleName} style={{ flex: 3 }}>
                数量
              </div>
              <div className={styles.progressTitleName} style={{ flex: 1 }}>
                单位
              </div>
            </div>

            <div className={styles.progressMain}>
              <div className={styles.progressMainBox}>
                <div className={styles.progressMainBoxTitle}>
                  <div className={styles.progressMainBoxMoney}>到期金额：</div>
                  <div className={styles.progressMainBoxNumber}>489</div>
                </div>
                <div className={styles.progressMainBoxProgress}></div>
                <div className={styles.progressMainBoxUnit}>(万元)</div>
              </div>
              <div className={styles.progressMainBox}>
                <div className={styles.progressMainBoxTitle}>
                  <div className={styles.progressMainBoxMoney}>到期金额：</div>
                  <div className={styles.progressMainBoxNumber}>489</div>
                </div>
                <div className={styles.progressMainBoxProgress}></div>
                <div className={styles.progressMainBoxUnit}>(万元)</div>
              </div>
              <div className={styles.progressMainBox}>
                <div className={styles.progressMainBoxTitle}>
                  <div className={styles.progressMainBoxMoney}>到期金额：</div>
                  <div className={styles.progressMainBoxNumber}>489</div>
                </div>
                <div className={styles.progressMainBoxProgress}></div>
                <div className={styles.progressMainBoxUnit}>(万元)</div>
              </div>
              <div className={styles.progressMainBox}>
                <div className={styles.progressMainBoxTitle}>
                  <div className={styles.progressMainBoxMoney}>到期金额：</div>
                  <div className={styles.progressMainBoxNumber}>489</div>
                </div>
                <div className={styles.progressMainBoxProgress}></div>
                <div className={styles.progressMainBoxUnit}>(万元)</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
