import { Component } from 'react';
import HeaderModule from '../../module/header';
import styles from './index.less';

export default class DataPoll extends Component {
  render() {
    return (
      <div className={styles.mains}>
        <HeaderModule name="数据汇总" />
        <div className={styles.contentBox}>
          <div className={styles.content}>
            <div className={styles.contentKey}>注册数</div>
            <div className={styles.contentValue}>123456</div>
          </div>
          <div className={styles.content}>
            <div className={styles.contentKey}>注册数</div>
            <div className={styles.contentValue}>123456</div>
          </div>
          <div className={styles.content}>
            <div className={styles.contentKey}>注册数</div>
            <div className={styles.contentValue}>123456</div>
          </div>
          <div className={styles.content}>
            <div className={styles.contentKey}>注册数</div>
            <div className={styles.contentValue}>123456</div>
          </div>
          <div className={styles.content}>
            <div className={styles.contentKey}>注册数</div>
            <div className={styles.contentValue}>123456</div>
          </div>
          <div className={styles.content}>
            <div className={styles.contentKey}>注册数</div>
            <div className={styles.contentValue}>123456</div>
          </div>
        </div>
      </div>
    );
  }
}
