import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import MyPageTable from '@/components/MyPageTable';
import {
  Card,
  Spin,
  Form,
  DatePicker,
  Input,
  Button,
  Select,
  message,
  Row,
  Col,
  TreeSelect,
  InputNumber,
} from 'antd';
import request from '@/services/baseService';
import moment from 'moment';
import index from '@/components/Frame';
import { objEmpty } from '@/utils/utils.js';
import { getCurrentUser } from '@/utils/localStorage';
import { saveAs } from 'file-saver';
import axios from 'axios';
import { getToken } from '@/utils/localStorage';
import { clientRegisterStatus, clientRegisterStatusMap, orderCloseStatusMap } from '@/utils/enum';
import styles from './index.less';
import CopyToClipboard from '@/components/CopyToClipboard';
import { orderStatusMap } from '@/utils/enum';
import { renderOrderStatus } from '@/utils/utils';

const { RangePicker } = DatePicker;
const { SHOW_PARENT } = TreeSelect;

@Form.create()
class clientList extends Component {
  state = {
    search: {
      current: 1,
      size: 30,
      mobile: '',
    },
    loading: false,
    form: {
      mobile: '',
    },
    tableList: [],
    total: 0,
    pages: 1,
    modal: {
      type: 'add',
      title: '',
    },
    queryLoad: false,
    modalLoad: false,
    channelList: [],
    userList: [],
    registerStatusValue: [],
    maxQuota: 10,
    minQuota: 0,
  };
  componentDidMount() {
    this.getList();
    this.getChannelList();
    this.getBackstageUserList();
  }

  /**
   * 渠道获取列表
   */
  getChannelList = () => {
    request('/hzsx/business/channel/getChannelList', {
      islist: 1,
      pageNumber: 1,
      pageSize: 1000,
    }).then(res => {
      this.setState({
        channelList: res.records,
      });
    });
  };

  /**
   * 获取审核人列表
   */
  getBackstageUserList = () => {
    request('/hzsx/user/listBackstageUser', {}, 'get').then(res => {
      this.setState({
        userList: res,
      });
    });
  };

  getList = () => {
    this.setState({
      loading: true,
    });
    const { search, form } = this.state;

    let obj = Object.fromEntries(
      Object.entries(search).filter(([key, value]) => {
        // 过滤空值
        return value !== '' && value !== null && value !== undefined;
      }),
    );

    request(`/hzsx/userAudit/getUserAuditList`, obj, 'post').then(res => {
      this.setState({
        tableList: res.records,
        queryLoad: false,
        pages: res.pages,
        total: res.total,
        loading: false,
      });
    });
  };

  cancelTheClaim = e => {
    request(`/hzsx/userAudit/closeClaim`, { serialNum: e.serialNum }, 'get')
      .then(res => {
        message.success('取消成功');
        this.getList();
      })
      .catch(el => {});
  };

  dealWith = e => {
    console.log(e);
  };

  columns = [
    {
      title: 'No.',
      dataIndex: 'id',
      rowScope: 'row',
      width: 60,
      render: (record, index, indent, expanded) => {
        return indent + 1;
      },
    },
    {
      title: '渠道名称',
      dataIndex: 'channelName',
    },
    {
      title: '审核人',
      dataIndex: 'rname',
    },
    {
      title: '姓名',
      dataIndex: 'userName',
    },
    {
      title: '身份证',
      dataIndex: 'idCard',
    },
    {
      title: '手机号',
      dataIndex: 'telephone',
    },
    {
      title: '进件时间',
      dataIndex: 'createTime',
    },
    {
      title: '注册时间',
      dataIndex: 'registerTime',
    },  
    {
      title: '复购次数',
      dataIndex: 'repurchaseNum',
    },
    {
      title: '订单状态',
      dataIndex: 'newOrderStatus',
      render: (record, index) => {
        return index?.newOrderStatus && renderOrderStatus({ status: index.newOrderStatus });
      },
    },
    {
      title: '认证状态',
      dataIndex: 'registerStatus',
      render: (record, index) => {
        return clientRegisterStatus[index.registerStatus];
      },
    },
    {
      title: '历史额度',
      dataIndex: 'beforeAuditLimit',
    },
    {
      title: '总额度',
      dataIndex: 'totalLimit',
    },
    {
      title: '剩余额度',
      dataIndex: 'surplusLimit',
    },
    // {
    //   title: '建议审核额度',
    //   dataIndex: 'proposalAuditLimit',
    // },
    {
      title: '模型分',
      dataIndex: 'modelScore',
    },
    {
      title: '风控备注',
      dataIndex: 'riskRemark',
    },
    {
      title: '风控信息',
      dataIndex: 'riskMsg',
    },
    {
      title: '用户备注',
      dataIndex: 'closeNotes',
    },
    {
      title: '拒绝备注',
      dataIndex: 'closeType',
      render: text => {
        if (text == '06') {
          return '客户要求关单';
        }
        if (text == '07') {
          return '商家风控关单';
        }
        return '';
      },
    },
    {
      title: '上次订单完结时间',
      dataIndex: 'orderFinishTime',
    },
    {
      title: '风控流水号',
      dataIndex: 'serialNum',
      render: serialNum => {
        return <CopyToClipboard text={serialNum} />;
      },
    },
    {
      title: '操作',
      fixed: 'right',
      render: e => {
        return (
          <div style={{ display: 'flex' }}>
            <Button
              type="link"
              style={{ padding: '0 10px' }}
              target="_blank"
              href={`#/Client/details?serialNum=${e.serialNum}&uids=${e.uid}`}
            >
              处理
            </Button>
            <Button
              style={{ marginLeft: '0', padding: '0 10px' }}
              type="link"
              href={`#/Order/HomePage?form=claimOrder&idCard=${e.idCard}`}
            >
              查看订单
            </Button>
            {e.rid == getCurrentUser().id && [6, 7].includes(e.registerStatus) ? (
              <div>
                <Button
                  type="link"
                  style={{ padding: '0 10px' }}
                  onClick={() => this.cancelTheClaim(e)}
                >
                  取消认领
                </Button>
              </div>
            ) : (
              ''
            )}
          </div>
        );
      },
    },
  ];

  handEport = () => {
    this.props.form.validateFields(
      ['userName', 'telephone', 'isCertified', 'channelId', 'rangePicker'],
      (err, values) => {
        if (!err) {
          let createTimeStart = '';
          let createTimeEnd = '';

          if (values.rangePicker && values.rangePicker.length > 0) {
            createTimeStart = moment(values.rangePicker[0]).format('YYYY-MM-DD HH:mm:ss');
            createTimeEnd = moment(values.rangePicker[1]).format('YYYY-MM-DD HH:mm:ss');
          }
          let obj = {
            telephone: values.telephone,
            userName: values.userName,
            isCertified: values.isCertified,
            channelId: values.channelId,
            createTimeStart,
            createTimeEnd,
          };
          let objs = objEmpty(obj);
          this.setState({
            loading: true,
          });

          axios({
            method: 'GET',
            url: '/zyj-backstage-web/hzsx/user/userListExport',
            params: objs,
            headers: {
              token: getToken(),
            },
            responseType: 'blob',
          }).then(res => {
            if (res.data.type == 'application/json') {
              message.error('导出失败，或者导出数据为空');
            } else {
              let url = window.URL.createObjectURL(
                new Blob([res.data], {
                  type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                }),
              );

              saveAs(url, '客户导出.xlsx');
            }

            this.setState({
              loading: false,
            });
          });
        }
      },
    );
  };

  handSearch = () => {
    this.props.form.validateFields(
      [
        'userName',
        'telephone',
        'idCard',
        'isAuth',
        'rid',
        'channelId',
        'registerStatus',
        'isRisk',
        'serialNum',
        'closeType',
        'closeMold',
        'rangePicker',
        'surplusLimitStart',
        'surplusLimitEnd',
        'closeNotes',
        'newOrderStatus',
        'registerNotName',
        'nameNotCreate',
        'registerTime',
      ],
      (err, values) => {
        if (!err) {
          let registerStatusListArray = [];
          let registerStatusList = '';
          let stageStatusListArray = [];
          let stageStatusList = '';

          // console.log(123456,values.registerStatus)
          if (values.registerStatus) {
            // console.log(values.registerStatus);
            values.registerStatus.forEach(element => {
              if (element.includes('-')) {
                let split = element.split('-');
                stageStatusListArray.push(Number(split[1]));
              } else {
                registerStatusListArray.push(Number(element));
              }
            });
            // console.log('小类', stageStatusListArray);
            // console.log('大类', registerStatusListArray);
            // stageStatusList = stageStatusListArray.join();
            // registerStatusList = registerStatusListArray.join();
          }

          let createTimeStart = '';
          let createTimeEnd = '';

          if (values.rangePicker && values.rangePicker.length > 0) {
            createTimeStart = `${moment(values.rangePicker[0]).format('YYYY-MM-DD')} 00:00:00`;
            createTimeEnd = `${moment(values.rangePicker[1]).format('YYYY-MM-DD')} 23:59:59`;
          }

          let registerTimeStart = '';
          let registerTimeEnd = '';

          if (values.registerTime && values.registerTime.length > 0) {
            registerTimeStart = `${moment(values.registerTime[0]).format('YYYY-MM-DD')} 00:00:00`;
            registerTimeEnd = `${moment(values.registerTime[1]).format('YYYY-MM-DD')} 23:59:59`;
          }

          this.setState(
            {
              queryLoad: true,
              search: {
                current: 1,
                size: 30,
                userName: values.userName,
                telephone: values.telephone,
                idCard: values.idCard,
                isAuth: values.isAuth,
                rid: values.rid,
                channelId: values.channelId,
                // registerStatus: values.registerStatus,
                isRisk: values.isRisk,
                serialNum: values.serialNum,
                closeType: values.closeType,
                closeMold: values.closeMold,
                rangePicker: values.rangePicker,
                stageStatusList: registerStatusListArray,
                registerStatusList: stageStatusListArray,
                surplusLimitStart: values.surplusLimitStart,
                surplusLimitEnd: values.surplusLimitEnd,
                closeNotes: values.closeNotes,
                newOrderStatus: values.newOrderStatus,
                registerNotName: values.registerNotName,
                nameNotCreate: values.nameNotCreate,
                registerTimeStart,
                registerTimeEnd,
                createTimeStart,
                createTimeEnd,
              },
            },
            () => this.getList(),
          );
        }
      },
    );
  };
  onPage = e => {
    let data = Object.assign(this.state.search, { current: e.current, size: e.pageSize });
    this.setState(
      {
        search: data,
      },
      () => this.getList(),
    );
  };

  onChange = value => {
    this.setState({ value });
  };

  render() {
    const { pages, total, search, queryLoad } = this.state;
    const { getFieldDecorator } = this.props.form;
    const paginationProps = {
      current: search.current,
      pageSize: search.size,
      total: total,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / search.size)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };

    const treeData = [
      {
        title: '申请前',
        value: '1',
        key: '1',
        field: 'stageStatusList',
        children: [
          {
            title: '注册通过',
            value: '1-1',
            key: '1-1',
            field: 'registerStatusList',
          },
          {
            title: 'OCR通过',
            value: '1-2',
            key: '1-2',
            field: 'registerStatusList',
          },
          {
            title: '活体通过',
            value: '1-3',
            key: '1-3',
            field: 'registerStatusList',
          },
          {
            title: '联系人通过',
            value: '1-4',
            key: '1-4',
            field: 'registerStatusList',
          },
        ],
      },
      {
        title: '等待申请结果',
        value: '2',
        key: '2',
        field: 'stageStatusList',
        children: [
          {
            title: '待人审',
            value: '2-6',
            key: '2-6',
            field: 'registerStatusList',
          },
        ],
      },
      {
        title: '申请通过',
        value: '3',
        key: '3',
        field: 'stageStatusList',
        children: [
          {
            title: '审核通过',
            value: '3-7',
            key: '3-7',
            field: 'registerStatusList',
          },
        ],
      },
      {
        title: '申请未通过',
        value: '4',
        key: '4',
        field: 'stageStatusList',
        children: [
          {
            title: '风控未通过',
            value: '4-5',
            key: '4-5',
            field: 'registerStatusList',
          },
          {
            title: '审核未通过',
            value: '4-8',
            key: '4-8',
            field: 'registerStatusList',
          },
        ],
      },
    ];

    const tProps = {
      treeData,
      allowClear: true,
      // value: this.state.registerStatusValue,
      // onChange: this.onChange,
      treeCheckable: true,
      showCheckedStrategy: SHOW_PARENT,
      searchPlaceholder: '请选择认证状态',
      style: {
        width: '185px',
      },
    };

    const maxOnChange = value => {
      this.setState({
        maxQuota: value,
      });
    };

    const mixOnChange = value => {
      this.setState({
        minQuota: value,
      });
    };

    return (
      <PageHeaderWrapper title={false} className="nav-tab">
        <Spin spinning={this.state.loading}>
          <Card bordered={false}>
            <Form layout="inline" className="mb-20">
              <Row>
                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>姓名</span>}>
                    {getFieldDecorator('userName', {
                      rules: [],
                    })(<Input className={styles.formInput} placeholder="请输入姓名" />)}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>手机号</span>}>
                    {getFieldDecorator('telephone', {
                      rules: [],
                    })(<Input className={styles.formInput} placeholder="请输入手机号" />)}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>身份证</span>}>
                    {getFieldDecorator('idCard', {
                      rules: [],
                    })(<Input className={styles.formInput} placeholder="请输入身份证" />)}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>审核人</span>}>
                    {getFieldDecorator(
                      'rid',
                      {},
                    )(
                      <Select
                        placeholder="请选择审核人"
                        allowClear
                        showSearch
                        className={styles.formInput}
                        filterOption={(input, option) =>
                          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {this.state.userList.map(value => {
                          return (
                            <Select.Option value={value.id} key={value.id}>
                              {value.name}
                            </Select.Option>
                          );
                        })}
                      </Select>,
                    )}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>渠道来源</span>}>
                    {getFieldDecorator(
                      'channelId',
                      {},
                    )(
                      <Select
                        placeholder="请选择渠道来源"
                        allowClear
                        showSearch
                        className={styles.formInput}
                        filterOption={(input, option) =>
                          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {this.state.channelList.map(value => {
                          return (
                            <Select.Option value={value.id} key={value.id}>
                              {value.name}
                            </Select.Option>
                          );
                        })}
                      </Select>,
                    )}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>是否风控</span>}>
                    {getFieldDecorator('isRisk', {
                      rules: [],
                    })(
                      <Select className={styles.formInput} placeholder="请选择是否风控" allowClear>
                        <Select.Option value={1}>是</Select.Option>
                        <Select.Option value={0}>否</Select.Option>
                      </Select>,
                    )}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>风控流水号</span>}>
                    {getFieldDecorator('serialNum', {
                      rules: [],
                    })(<Input className={styles.formInput} placeholder="请输入风控流水号" />)}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>剩余额度</span>}>
                    {getFieldDecorator('surplusLimitStart', {
                      rules: [],
                    })(
                      <InputNumber
                        style={{ width: '150px' }}
                        placeholder="请输入最小余额"
                        min={1}
                        max={this.state.maxQuota}
                        onChange={mixOnChange}
                      />,
                    )}
                    <span style={{ width: '10px', display: 'inline-block' }}></span>~
                    <span style={{ width: '10px', display: 'inline-block' }}></span>
                    {getFieldDecorator('surplusLimitEnd', {
                      rules: [],
                    })(
                      <InputNumber
                        style={{ width: '150px' }}
                        placeholder="请输入最大余额"
                        min={this.state.minQuota}
                        max={100000}
                        onChange={maxOnChange}
                      />,
                    )}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>进件时间</span>}>
                    {getFieldDecorator('rangePicker')(<RangePicker format="YYYY-MM-DD" />)}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>认证状态</span>}>
                    {getFieldDecorator(
                      'registerStatus',
                      {},
                    )(
                      <TreeSelect className={styles.formInput} {...tProps} />,
                      // <Select
                      //   placeholder="请选择认证状态"
                      //   allowClear
                      //   showSearch
                      //   style={{ width: 180 }}
                      //   filterOption={(input, option) =>
                      //     option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                      //   }
                      // >
                      //   {clientRegisterStatusMap.map(value => {
                      //     return (
                      //       <Select.Option value={value.id} key={value.id}>
                      //         {value.title}
                      //       </Select.Option>
                      //     );
                      //   })}
                      // </Select>,
                    )}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>订单状态</span>}>
                    {getFieldDecorator(
                      'newOrderStatus',
                      {},
                    )(
                      <Select placeholder="订单状态" allowClear style={{ width: 202 }}>
                        {[
                          '01',
                          '16',
                          '17',
                          '11',
                          '04',
                          '05',
                          '06',
                          '07',
                          '08',
                          '09',
                          '10',
                          '19',
                          '20',
                          '21',
                        ].map(value => {
                          return (
                            <Option value={value.toString()} key={value.toString()}>
                              {orderStatusMap[value.toString()]}
                            </Option>
                          );
                        })}
                      </Select>,
                    )}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>用户备注</span>}>
                    {getFieldDecorator('closeNotes', {
                      rules: [],
                    })(<Input className={styles.formInput} placeholder="请输入用户备注" />)}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>注册未实名</span>}>
                    {getFieldDecorator(
                      'registerNotName',
                      {},
                    )(
                      <Select placeholder="请选择注册未实名" allowClear style={{ width: 202 }}>
                        <Option value="是" key={1}>
                          是
                        </Option>
                      </Select>,
                    )}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>实名未下单</span>}>
                    {getFieldDecorator(
                      'nameNotCreate',
                      {},
                    )(
                      <Select placeholder="请选择实名未下单" allowClear style={{ width: 202 }}>
                        <Option value="是" key={1}>
                          是
                        </Option>
                      </Select>,
                    )}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>注册时间</span>}>
                    {getFieldDecorator('registerTime')(<RangePicker format="YYYY-MM-DD" />)}
                  </Form.Item>
                </Col>
              </Row>
              <div>
                <Form.Item>
                  <Button loading={queryLoad} onClick={() => this.handSearch()} type="primary">
                    查询
                  </Button>
                </Form.Item>
                {/* <Form.Item>
                  <Button loading={queryLoad} onClick={() => this.handSearch()} type="primary">
                    认领订单
                  </Button>
                </Form.Item> */}
                {/* <Form.Item>
                  <Button loading={queryLoad} onClick={() => this.handEport()} type="primary">
                    导出
                  </Button>
                </Form.Item> */}
              </div>
            </Form>
            <MyPageTable
              scroll="max-content"
              onPage={this.onPage}
              style={{ marginTop: '20px' }}
              paginationProps={paginationProps}
              dataSource={this.state.tableList}
              columns={this.columns}
              rowKey="id"
            />
          </Card>
        </Spin>
      </PageHeaderWrapper>
    );
  }
}

export default clientList;
