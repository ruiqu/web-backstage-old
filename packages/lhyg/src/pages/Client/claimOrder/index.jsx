import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import MyPageTable from '@/components/MyPageTable';
import {
  Card,
  Spin,
  Form,
  DatePicker,
  Input,
  Button,
  Select,
  message,
  Row,
  Col,
  Popconfirm,
} from 'antd';
import request from '@/services/baseService';
import moment from 'moment';
import index from '@/components/Frame';
import { objEmpty } from '@/utils/utils.js';
import { saveAs } from 'file-saver';
import axios from 'axios';
import { getToken } from '@/utils/localStorage';
import { clientRegisterStatus, clientRegisterStatusMap, orderCloseStatusMap } from '@/utils/enum';
import styles from './index.less';
import CopyToClipboard from '@/components/CopyToClipboard';

const { RangePicker } = DatePicker;

const { Option } = Select;

@Form.create()
class claimOrder extends Component {
  state = {
    search: {
      current: 1,
      size: 30,
      mobile: '',
      pageClass: '2',
    },
    loading: false,
    form: {
      mobile: '',
    },
    tableList: [],
    total: 0,
    pages: 1,
    modal: {
      type: 'add',
      title: '',
    },
    queryLoad: false,
    modalLoad: false,
    channelList: [],
    userList: [],
    lingLoad: false,
  };
  componentDidMount() {
    this.getList();
    this.getChannelList();
    this.getBackstageUserList();
  }

  /**
   * 渠道获取列表
   */
  getChannelList = () => {
    request('/hzsx/business/channel/getChannelList', {
      islist: 1,
      pageNumber: 1,
      pageSize: 1000,
    }).then(res => {
      this.setState({
        channelList: res.records,
      });
    });
  };

  /**
   * 获取审核人列表
   */
  getBackstageUserList = () => {
    request('/hzsx/user/listBackstageUser', {}, 'get').then(res => {
      this.setState({
        userList: res,
      });
    });
  };

  getList = () => {
    this.setState({
      loading: true,
    });
    const { search, form } = this.state;

    let obj = Object.fromEntries(
      Object.entries(search).filter(([key, value]) => {
        // 过滤空值
        return value !== '' && value !== null && value !== undefined;
      }),
    );
    console.log(1233, obj);

    request(`/hzsx/userAudit/getUserAuditList`, obj, 'post').then(res => {
      this.setState({
        tableList: res.records,
        queryLoad: false,
        pages: res.pages,
        total: res.total,
        loading: false,
      });
    });
  };

  onConfirm = e => {
    console.log(e);
    let obj = {
      serialNum: e.serialNum,
      uid: e.uid,
    };
    this.setState({
      lingLoad: true,
    });

    request(`/hzsx/userAudit/claimUsers`, obj, 'post')
      .then(res => {
        message.success('认领成功');
        this.setState({
          lingLoad: false,
        });
        this.getList();
      })
  };

  columns = [
    {
      title: 'No.',
      dataIndex: 'id',
      rowScope: 'row',
      width: 60,
      render: (record, index, indent, expanded) => {
        return indent + 1;
      },
    },
    {
      title: '渠道名称',
      dataIndex: 'channelName',
    },
    {
      title: '审核人',
      dataIndex: 'rname',
    },
    {
      title: '姓名',
      dataIndex: 'userName',
    },
    {
      title: '身份证',
      dataIndex: 'idCard',
    },
    {
      title: '手机号',
      dataIndex: 'telephone',
    },
    {
      title: '进件时间',
      dataIndex: 'createTime',
    },
    {
      title: '上次订单完结时间',
      dataIndex: 'orderFinishTime',
    },
    {
      title: '认证状态',
      dataIndex: 'registerStatus',
      render: (record, index) => {
        return clientRegisterStatus[index.registerStatus];
      },
    },
    {
      title: '历史额度',
      dataIndex: 'beforeAuditLimit',
    },
    {
      title: '总额度',
      dataIndex: 'totalLimit',
    },
    // {
    //   title: '建议审核额度',
    //   dataIndex: 'proposalAuditLimit',
    // },
    {
      title: '模型分',
      dataIndex: 'modelScore',
    },
    {
      title: '风控备注',
      dataIndex: 'riskRemark',
    },
    {
      title: '风控流水号',
      dataIndex: 'serialNum',
      render: serialNum => {
        return <CopyToClipboard text={serialNum} />;
      },
    },
    {
      title: '操作',
      fixed: 'right',
      width: 100,
      render: e => {
        return (
          <Popconfirm
            title="确定认领此订单吗？"
            onConfirm={() => this.onConfirm(e)}
            okText="是"
            cancelText="否"
          >
            <Button type="primary" loading={this.state.lingLoad}>
              认领
            </Button>
          </Popconfirm>
        );
      },
    },
  ];

  handSearch = () => {
    this.props.form.validateFields(
      [
        'userName',
        'telephone',
        'idCard',
        'isAuth',
        'rid',
        'channelId',
        'isRisk',
        'serialNum',
        'closeType',
        'closeMold',
        'rangePicker',
      ],
      (err, values) => {
        if (!err) {
          let createTimeStart = '';
          let createTimeEnd = '';

          if (values.rangePicker && values.rangePicker.length > 0) {
            createTimeStart = `${moment(values.rangePicker[0]).format('YYYY-MM-DD')} 00:00:00`;
            createTimeEnd = `${moment(values.rangePicker[1]).format(
              'YYYY-MM-DD HH:mm:ss',
            )} 23:59:59`;
          }
          this.setState(
            {
              queryLoad: true,
              search: {
                current: 1,
                size: 30,
                userName: values.userName,
                telephone: values.telephone,
                idCard: values.idCard,
                isAuth: values.isAuth,
                rid: values.rid,
                channelId: values.channelId,
                isRisk: values.isRisk,
                serialNum: values.serialNum,
                closeType: values.closeType,
                closeMold: values.closeMold,
                rangePicker: values.rangePicker,
                pageClass: '2',
                createTimeStart,
                createTimeEnd,
              },
            },
            () => this.getList(),
          );
        }
      },
    );
  };
  onPage = e => {
    let data = Object.assign(this.state.search, { current: e.current, size: e.pageSize });
    this.setState(
      {
        search: data,
      },
      () => this.getList(),
    );
  };

  render() {
    const { pages, total, search, queryLoad } = this.state;
    const { getFieldDecorator } = this.props.form;
    const paginationProps = {
      current: search.current,
      pageSize: search.size,
      total: total,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / search.size)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };

    return (
      <PageHeaderWrapper title={false} className="nav-tab">
        <Spin spinning={this.state.loading}>
          <Card bordered={false}>
            <Form layout="inline" className="mb-20">
              <Row>
                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>姓名</span>}>
                    {getFieldDecorator('userName', {
                      rules: [],
                    })(<Input placeholder="请输入姓名" />)}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>手机号</span>}>
                    {getFieldDecorator('telephone', {
                      rules: [],
                    })(<Input placeholder="请输入手机号" />)}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>身份证</span>}>
                    {getFieldDecorator('idCard', {
                      rules: [],
                    })(<Input placeholder="请输入身份证" />)}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>审核人</span>}>
                    {getFieldDecorator(
                      'rid',
                      {},
                    )(
                      <Select
                        placeholder="请选择审核人"
                        allowClear
                        showSearch
                        style={{ width: 180 }}
                        filterOption={(input, option) =>
                          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {this.state.userList.map(value => {
                          return (
                            <Select.Option value={value.id} key={value.id}>
                              {value.name}
                            </Select.Option>
                          );
                        })}
                      </Select>,
                    )}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>渠道来源</span>}>
                    {getFieldDecorator(
                      'channelId',
                      {},
                    )(
                      <Select
                        placeholder="请选择渠道来源"
                        allowClear
                        showSearch
                        style={{ width: 180 }}
                        filterOption={(input, option) =>
                          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {this.state.channelList.map(value => {
                          return (
                            <Select.Option value={value.id} key={value.id}>
                              {value.name}
                            </Select.Option>
                          );
                        })}
                      </Select>,
                    )}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>是否风控</span>}>
                    {getFieldDecorator('isRisk', {
                      rules: [],
                    })(
                      <Select style={{ width: '180px' }} placeholder="请选择是否认证">
                        <Select.Option value={1}>是</Select.Option>
                        <Select.Option value={0}>否</Select.Option>
                      </Select>,
                    )}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>风控流水号</span>}>
                    {getFieldDecorator('serialNum', {
                      rules: [],
                    })(<Input placeholder="请输入风控流水号" />)}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>进件时间</span>}>
                    {getFieldDecorator('rangePicker')(<RangePicker format="YYYY-MM-DD" />)}
                  </Form.Item>
                </Col>
              </Row>
              <div>
                <Form.Item>
                  <Button loading={queryLoad} onClick={() => this.handSearch()} type="primary">
                    查询
                  </Button>
                </Form.Item>
              </div>
            </Form>

            <MyPageTable
              scroll="max-content"
              onPage={this.onPage}
              style={{ marginTop: '20px' }}
              paginationProps={paginationProps}
              dataSource={this.state.tableList}
              columns={this.columns}
            />
          </Card>
        </Spin>
      </PageHeaderWrapper>
    );
  }
}

export default claimOrder;
