import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import {
  Card,
  Spin,
  Form,
  DatePicker,
  Input,
  Button,
  Row,
  Col,
} from 'antd';
import moment from 'moment';
import request from '@/services/baseService';
import MyPageTable from '@/components/MyPageTable';
import styles from './index.less';

const { RangePicker } = DatePicker;

@Form.create()
class clientRecord extends Component {
  state = {
    loading: false,
    search: {
      current: 1,
      size: 30,
      mobile: '',
    },
    total: 0,
    pages: 1,
    tableList: [],
  };
  componentDidMount() {
    this.getList();
  }

  getList() {
    this.setState({
      loading: true,
    });
    const { search, form } = this.state;

    let obj = Object.fromEntries(
      Object.entries(search).filter(([key, value]) => {
        // 过滤空值
        return value !== '' && value !== null && value !== undefined;
      }),
    );

    request('/hzsx/ope/order/pageAuthRecord', obj).then(res => {
      this.setState({
        tableList: res.records,
        pages: res.pages,
        total: res.total,
        loading: false,
      });
    });
  }

  handSearch = () => {
    this.props.form.validateFields(['userName', 'telephone', 'createTime'], (err, values) => {
      if (!err) {
        let createTimeBegin = '';
        let createTimeEnd = '';

        if (values.createTime && values.createTime.length > 0) {
          createTimeBegin = `${moment(values.createTime[0]).format('YYYY-MM-DD')} 00:00:00`;
          createTimeEnd = `${moment(values.createTime[1]).format('YYYY-MM-DD')} 23:59:59`;
        }

        this.setState(
          {
            queryLoad: true,
            search: {
              current: 1,
              size: 30,
              userName: values.userName,
              telephone: values.telephone,
              createTimeBegin,
              createTimeEnd,
            },
          },
          () => this.getList(),
        );
      }
    });
  };

  onPage = e => {
    let data = Object.assign(this.state.search, { current: e.current, size: e.pageSize });
    this.setState(
      {
        search: data,
      },
      () => this.getList(),
    );
  };

  render() {
    const columns = [
      {
        title: 'No.',
        dataIndex: 'id',
        rowScope: 'row',
        width: 60,
        render: (record, index, indent, expanded) => {
          return indent + 1;
        },
      },
      {
        title: '姓名',
        dataIndex: 'userName',
      },
      {
        title: '手机号',
        dataIndex: 'telephone',
      },
      {
        title: '操作类型',
        render: (record, index) => {
          if (index.operationType == '01') {
            return 'OCR认证';
          }
          if (index.operationType == '02') {
            return '人脸认证';
          }
          if (index.operationType == '03') {
            return '三要素';
          }
        },
      },
      {
        title: '认证时间',
        dataIndex: 'createTime',
      },
    ];

    const { pages, total, search, queryLoad } = this.state;
    const { getFieldDecorator } = this.props.form;

    const paginationProps = {
      current: search.current,
      pageSize: search.size,
      total: total,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / search.size)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };

    return (
      <PageHeaderWrapper title={false} className="nav-tab">
        <Spin spinning={this.state.loading}>
          <Card bordered={false}>
            <Form layout="inline" className="mb-20">
              <Row>
                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>姓名</span>}>
                    {getFieldDecorator('userName', {
                      rules: [],
                    })(<Input className={styles.formInput} placeholder="请输入姓名" />)}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>手机号</span>}>
                    {getFieldDecorator('telephone', {
                      rules: [],
                    })(<Input className={styles.formInput} placeholder="请输入手机号" />)}
                  </Form.Item>
                </Col>

                <Col span={8}>
                  <Form.Item label={<span className={styles.formLabel}>注册时间</span>}>
                    {getFieldDecorator('createTime')(<RangePicker format="YYYY-MM-DD" />)}
                  </Form.Item>
                </Col>
              </Row>
              <div>
                <Form.Item>
                  <Button loading={this.state.loading} onClick={() => this.handSearch()} type="primary">
                    查询
                  </Button>
                </Form.Item>
              </div>
            </Form>

            <MyPageTable
              scroll="max-content"
              onPage={this.onPage}
              style={{ marginTop: '20px' }}
              paginationProps={paginationProps}
              dataSource={this.state.tableList}
              columns={columns}
              rowKey="id"
            />
          </Card>
        </Spin>
      </PageHeaderWrapper>
    );
  }
}

export default clientRecord;
