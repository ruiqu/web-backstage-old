import { Button, Popconfirm, message } from 'antd';
import request from '@/services/baseService';

const addAWhiteList = props => {
  const { uid, getDetail } = props;
  const confirm = async () => {
    await request(
      'hzsx/userBlack/oneKeyBlacklist',
      {
        id: uid,
      },
      'post',
    );
    message.success('操作成功');
    getDetail();
  };
  return (
    <Popconfirm title="确定拉入黑名单吗?" onConfirm={() => confirm()} okText="是" cancelText="否">
      <Button type="primary" style={{ margin: '0 0 0 10px' }}>
        一键拉黑
      </Button>
    </Popconfirm>
  );
};

export default addAWhiteList;
