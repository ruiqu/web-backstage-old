import React, { useState, useEffect } from 'react';
import { Button, Modal, Form, Input, Row, Col, Select, message } from 'antd';
import { useDetailsHook } from '../hook';
import request from '@/services/baseService';

const { TextArea } = Input;
const { Option } = Select;

const examinationPassed = props => {
  const {
    beforeAuditLimit,
    proposalAuditLimit,
    totalLimit,
    surplusLimit,
    form,
    getDetail,
  } = props;
  const { getFieldDecorator } = form;

  const detailsHook = useDetailsHook();
  const { serialNum } = detailsHook;

  const [modalVisible, setModalVisible] = useState(false);

  /**打开弹窗 */
  const openModal = () => {
    setModalVisible(true);
  };
  /**点击确定 */
  const handleOk = async () => {
    props.form.validateFields(['auditLimit', 'registerStatus'], async (err, values) => {
      if (!err) {
        await request(
          '/hzsx/userAudit/changeLimit   ',
          {
            serialNum,
            auditLimit: values.auditLimit,
            registerStatus: values.registerStatus,
          },
          'post',
        );
        setModalVisible(false);
        message.success('操作成功');
        getDetail();
      }
    });
  };

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 18 },
    },
  };

  return (
    <>
      <Modal
        title="修改额度"
        visible={modalVisible}
        onCancel={() => setModalVisible(false)}
        onOk={() => handleOk()}
      >
        <Row>
          <Col
            style={{
              color: 'rgba(0, 0, 0, 0.85)',
              fontSize: '14px',
              textAlign: 'right',
              marginBottom: '20px',
            }}
            span={6}
          >
            总额度：
          </Col>
          <Col style={{ marginBottom: '25px' }} span={4}>
            {totalLimit}
          </Col>
          <Col
            style={{
              color: 'rgba(0, 0, 0, 0.85)',
              fontSize: '14px',
              textAlign: 'right',
              marginBottom: '20px',
            }}
            span={6}
          >
            剩余额度：
          </Col>
          <Col style={{ marginBottom: '25px' }} span={4}>
            {surplusLimit}
          </Col>
        </Row>

        <Row>
          <Col
            style={{
              color: 'rgba(0, 0, 0, 0.85)',
              fontSize: '14px',
              textAlign: 'right',
              marginBottom: '20px',
            }}
            span={6}
          >
            历史额度：
          </Col>
          <Col style={{ marginBottom: '25px' }} span={4}>
            {beforeAuditLimit}
          </Col>
          <Col
            style={{
              color: 'rgba(0, 0, 0, 0.85)',
              fontSize: '14px',
              textAlign: 'right',
              marginBottom: '20px',
            }}
            span={6}
          >
            建议额度：
          </Col>
          <Col style={{ marginBottom: '25px' }} span={4}>
            {proposalAuditLimit}
          </Col>
        </Row>

        <Form {...formItemLayout} className="login-form">
          <Form.Item label="修改额度">
            {getFieldDecorator('auditLimit', {
              rules: [{ required: true, message: '请填写需要修改成的额度' }],
            })(<Input style={{ width: '180px' }} placeholder="请填写需要修改成的额度" />)}
          </Form.Item>
          <Form.Item label="认证状态">
            {getFieldDecorator('registerStatus', {
              initialValue: '7',
              rules: [{ required: true, message: '请选择认证状态' }],
            })(
              <Select style={{ width: 120 }}>
                <Option value="7">通过</Option>
                <Option value="6">待人审</Option>
              </Select>,
            )}
          </Form.Item>
        </Form>
      </Modal>
      <Button type="primary" onClick={() => openModal()}>
        修改额度
      </Button>
    </>
  );
};

export default Form.create()(examinationPassed);
