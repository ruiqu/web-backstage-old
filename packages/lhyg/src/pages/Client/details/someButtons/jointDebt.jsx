import React, { useState, useEffect } from 'react';
import { Button, Modal, Table, Input, Icon } from 'antd';
import request from '@/services/baseService';
import { orderStatusMap } from '@/utils/enum';
import Highlighter from 'react-highlight-words';

const JointDebt = props => {
  const { windControl } = props;

  const [data, setData] = useState({});

  const [visible, setVisible] = useState(false);

  const [searchedColumn, setSearchedColumn] = useState('');

  const [searchText, setSearchText] = useState('');

  const [searchInput, setSearchInput] = useState(null);

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = clearFilters => {
    clearFilters();
    setSearchText('');
  };

  const getColumnSearchProps = (dataIndex, placeholderName) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        {setSelectedKeys}
        <Input
          ref={node => {
            setSearchInput(node);
          }}
          placeholder={`请输入${placeholderName}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          type="primary"
          onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          搜索
        </Button>
        <Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
          重置
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.select());
      }
    },
    render: text =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      ) : (
        text
      ),
  });

  const handOtherRisk = () => {
    request('/hzsx/business/order/otherRisk', { idCard: windControl.idCardNo }, 'post').then(
      res => {
        console.log(123, res);
        setData({
          otherRiskList: res?.OtherRiskResponse,
          otherRiskFinish: res?.finish,
          otherRiskNoFinish: res?.notFinish,
          passPlat: res?.passPlat,
          totalPlat: res?.totalPlat,
        });
        setVisible(true);
      },
    );
  };

  const btnConfirmHandler = () => {
    setData({});
    setVisible(false);
  };
  const statusFilterRow = (function() {
    let array = [];
    for (let item in orderStatusMap) {
      array.push({
        text: orderStatusMap[item],
        value: item,
      });
    }

    return array;
  })();

  const columns = [
    {
      title: '姓名',
      dataIndex: 'user_name',
      key: 'user_name',
    },
    {
      title: '电话',
      dataIndex: 'telephone',
      key: 'telephone',
    },
    {
      title: '身份证',
      dataIndex: 'id_card',
      key: 'id_card',
    },
    {
      title: '状态',
      dataIndex: 'status',
      key: 'status',
      filters: statusFilterRow,
      onFilter: (value, record) => record.status == value,
      render: (text, record, index) => orderStatusMap[text],
    },
    {
      title: '当期应付租额',
      dataIndex: 'current_periods_rent',
      key: 'current_periods_rent',
    },
    {
      title: '平台',
      dataIndex: 'shop_name',
      key: 'shop_name',
      ...getColumnSearchProps('shop_name', '平台名称'),
    },
    {
      title: '逾期天数',
      dataIndex: 'overdue_days',
      key: 'overdue_days',
    },
    {
      title: '账单日',
      dataIndex: 'statement_date',
      key: 'statement_date',
      sorter: (a, b) => {
        let aTime = new Date(a.statement_date).getTime();
        let bTime = new Date(b.statement_date).getTime();
        return aTime - bTime;
      },
    },
    {
      title: '还款日',
      dataIndex: 'repayment_date',
      key: 'repayment_date',
      sorter: (a, b) => {
        let aTime = new Date(a.repayment_date).getTime();
        let bTime = new Date(b.repayment_date).getTime();
        return aTime - bTime;
      },
    },
    {
      title: '创建时间',
      dataIndex: 'create_time',
      key: 'create_time',
      sorter: (a, b) => {
        let aTime = new Date(a.create_time).getTime();
        let bTime = new Date(b.create_time).getTime();
        return aTime - bTime;
      },
    },
  ];

  return (
    <>
      <Button type="primary" onClick={() => handOtherRisk()}>
        测试按钮
      </Button>
      <Modal
        width="80%"
        title="测试的详情"
        visible={visible}
        onOk={() => btnConfirmHandler()}
        onCancel={() => btnConfirmHandler()}
        destroyOnClose
      >
        <div style={{ marginBottom: '20px' }}>
          <span>已还债：{data.otherRiskFinish}</span>
          <span style={{ marginLeft: '30px' }}>在途中：{data.otherRiskNoFinish}</span>
          <span style={{ marginLeft: '30px' }}>通过平台数：{data.passPlat}</span>
          <span style={{ marginLeft: '30px' }}>申请平台数：{data.totalPlat}</span>
          <span style={{ marginLeft: '30px' }}>首逾订单数：{data.overdueOrder}</span>
          <span style={{ marginLeft: '30px' }}>首逾金额数：{data.overdueAmount}</span>

        </div>
        <Table
          scroll={{ y: 400 }}
          dataSource={data.otherRiskList}
          columns={columns}
          pagination={false}
        />
      </Modal>
    </>
  );
};

export default JointDebt;
