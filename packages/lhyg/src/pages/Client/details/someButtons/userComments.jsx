import React, { useState, useEffect } from 'react';
import { Form, Button, Modal, Input, Select, message } from 'antd';
import { useDetailsHook } from '../hook';
import request from '@/services/baseService';

const { TextArea } = Input;
const { Option } = Select;

const userComments = props => {
  const { getDetail, form } = props;
  const { getFieldDecorator } = form;

  const detailsHook = useDetailsHook();
  const { serialNum } = detailsHook;

  const [modalVisible, setModalVisible] = useState(false);

  const [customize, setCustomize] = useState(false);

  /**打开弹窗 */
  const openModal = () => {
    setModalVisible(true);
  };
  /**点击确定 */
  const handleOk = () => {
    props.form.validateFields(['closeNotes', 'customizeCloseNotes'], async (err, values) => {
      if (!err) {
        let closeNotes = values.closeNotes;

        if (closeNotes === 'customize') {
          closeNotes = values.customizeCloseNotes;
        }

        // console.log(closeNotes);
        // return;
        await request('/hzsx/userAudit/businessCloseNotes', { serialNum, closeNotes }, 'post');
        message.success('操作成功');
        setModalVisible(false);
        getDetail();
      }
    });
  };

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 18 },
    },
  };
  const onChange = value => {
    if (value === 'customize') {
      setCustomize(true);
      return;
    }
    setCustomize(false);
  };

  return (
    <>
      <Modal
        title="用户备注"
        visible={modalVisible}
        onCancel={() => setModalVisible(false)}
        onOk={() => handleOk()}
      >
        <Form {...formItemLayout} className="login-form">
          <Form.Item label="用户备注">
            {getFieldDecorator('closeNotes', {
              rules: [{ required: true, message: '请选择用户备注' }],
            })(
              <Select showSearch onChange={onChange} style={{ width: '100%' }}>
                <Option value="额度低">额度低</Option>
                <Option value="时间短">时间短</Option>
                <Option value="使用麻烦">使用麻烦</Option>
                <Option value="暂时不需要">暂时不需要</Option>
                <Option value="审核麻烦">审核麻烦</Option>
                <Option value="customize">自定义</Option>
              </Select>,
            )}
          </Form.Item>

          {customize ? (
            <Form.Item label="自定义备注">
              {getFieldDecorator('customizeCloseNotes', {
                rules: [{ required: true, message: '请填写自定义备注' }],
              })(<TextArea rows={4} placeholder="请填写自定义备注" />)}
            </Form.Item>
          ) : (
            ''
          )}
        </Form>
      </Modal>
      <Button type="primary" onClick={() => openModal()}>
        用户备注
      </Button>
    </>
  );
};

export default Form.create()(userComments);
