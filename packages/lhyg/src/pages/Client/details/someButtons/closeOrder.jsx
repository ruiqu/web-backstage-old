import React, { useState, useEffect } from 'react';
import { Button, Modal, Form, Input, Row, Col, Select, message } from 'antd';
import { useDetailsHook } from '../hook';
import { orderCloseStatusMap } from '@/utils/enum';
import request from '@/services/baseService';

const { TextArea } = Input;
const { Option } = Select;

const CloseOrder = props => {
  const { getDetail, form } = props;
  const { getFieldDecorator } = form;

  const detailsHook = useDetailsHook();
  const { serialNum } = detailsHook;

  const [modalVisible, setModalVisible] = useState(false);

  /**打开弹窗 */
  const openModal = () => {
    setModalVisible(true);
  };
  /**点击确定 */
  const handleOk = () => {
    props.form.validateFields(['closeType', 'closeReason'], async (err, values) => {
      if (!err) {
        await request(
          '/hzsx/userAudit/businessClosePayedUser',
          {
            serialNum,
            closeReason: values.closeReason,
            closeType: values.closeType,
          },
          'post',
        );
        setModalVisible(false);
        message.success('操作成功');
        getDetail();
      }
    });
  };

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 18 },
    },
  };

  return (
    <>
      <Modal
        title="关闭订单"
        visible={modalVisible}
        onCancel={() => setModalVisible(false)}
        onOk={() => handleOk()}
      >
        <Form {...formItemLayout} className="login-form">
          <Form.Item label="关单类型">
            {getFieldDecorator('closeType', {
              rules: [{ required: true, message: '请填选择关单类型' }],
            })(
              <Select style={{ width: '180px' }} placeholder="请选择关单类型">
                <Option value={'06'} key={'06'}>
                  客户要求关单
                </Option>
                <Option value={'07'} key={'07'}>
                  商家风控关单
                </Option>
              </Select>,
            )}
          </Form.Item>
          <Form.Item label="关单原因">
            {getFieldDecorator('closeReason', {
              rules: [{ required: true, message: '请填写关单原因' }],
            })(<TextArea rows={4} placeholder="关单原因" />)}
          </Form.Item>
        </Form>
      </Modal>
      <Button type="primary" onClick={() => openModal()}>
        关闭订单
      </Button>
    </>
  );
};

export default Form.create()(CloseOrder);
