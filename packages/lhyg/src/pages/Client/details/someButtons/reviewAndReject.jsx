import React, { useState, useEffect } from 'react';
import { Button, Modal, Form, Input, Row, Col, Select, message } from 'antd';
import { useDetailsHook } from '../hook';
import { orderCloseStatusMap } from '@/utils/enum';
import request from '@/services/baseService';

const { TextArea } = Input;
const { Option } = Select;

const reviewAndReject = props => {
  const { getDetail, form } = props;
  const { getFieldDecorator } = form;

  const detailsHook = useDetailsHook();
  const { serialNum } = detailsHook;

  const [modalVisible, setModalVisible] = useState(false);

  /**打开弹窗 */
  const openModal = () => {
    setModalVisible(true);
  };
  /**点击确定 */
  const handleOk = () => {
    props.form.validateFields(['closeReason'], async (err, values) => {
      if (!err) {
        await request(
          '/hzsx/userAudit/telephoneAuditUser',
          {
            orderAuditStatus: '02',
            serialNum,
            closeReason: values.closeReason,
          },
          'post',
        );
        setModalVisible(false);
        message.success('操作成功');
        getDetail();
      }
    });
  };

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 18 },
    },
  };

  return (
    <>
      <Modal
        title="审核拒绝"
        visible={modalVisible}
        onCancel={() => setModalVisible(false)}
        onOk={() => handleOk()}
      >
        <Form {...formItemLayout} className="login-form">
          {/* <Form.Item label="关单类型">
            {getFieldDecorator('closeMold', {
              rules: [{ required: true, message: '请填选择关单类型' }],
            })(
              <Select style={{ width: '180px' }} placeholder="请选择关单类型">
                {orderCloseStatusMap.map(value => {
                  return (
                    <Option value={value.value} key={value.value}>
                      {value.content}
                    </Option>
                  );
                })}
              </Select>,
            )}
          </Form.Item> */}
          <Form.Item label="拒绝原因">
            {getFieldDecorator('closeReason', {
              rules: [{ required: true, message: '请填写拒绝原因' }],
            })(<TextArea rows={4} placeholder="拒绝原因" />)}
          </Form.Item>
        </Form>
      </Modal>
      <Button type="primary" onClick={() => openModal()}>
        审核拒绝
      </Button>
    </>
  );
};

export default Form.create()(reviewAndReject);
