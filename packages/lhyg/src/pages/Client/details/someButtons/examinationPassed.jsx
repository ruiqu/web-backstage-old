import React, { useState, useEffect } from 'react';
import { Button, Modal, Form, Input, Row, Col, message } from 'antd';
import { useDetailsHook } from '../hook';
import request from '@/services/baseService';

const { TextArea } = Input;

const examinationPassed = props => {
  const { beforeAuditLimit, proposalAuditLimit, form, getDetail } = props;
  const { getFieldDecorator } = form;

  const detailsHook = useDetailsHook();
  const { serialNum } = detailsHook;

  const [modalVisible, setModalVisible] = useState(false);

  /**打开弹窗 */
  const openModal = () => {
    setModalVisible(true);
  };
  /**点击确定 */
  const handleOk = async () => {
    props.form.validateFields(['newLimit', 'closeReason'], async (err, values) => {
      if (!err) {
        await request(
          '/hzsx/userAudit/telephoneAuditUser',
          {
            orderAuditStatus: '01',
            serialNum,
            closeReason: values.closeReason,
            auditLimit: values.newLimit,
          },
          'post',
        );
        setModalVisible(false);
        message.success('操作成功');
        getDetail();
      }
    });
  };

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 18 },
    },
  };

  return (
    <>
      <Modal
        title="审核通过"
        visible={modalVisible}
        onCancel={() => setModalVisible(false)}
        onOk={() => handleOk()}
      >
        <Row>
          <Col
            style={{
              color: 'rgba(0, 0, 0, 0.85)',
              fontSize: '14px',
              textAlign: 'right',
              marginBottom: '20px',
            }}
            span={6}
          >
            历史额度：
          </Col>
          <Col style={{ marginBottom: '25px' }} span={18}>
            {beforeAuditLimit}
          </Col>
        </Row>

        <Row>
          <Col
            style={{
              color: 'rgba(0, 0, 0, 0.85)',
              fontSize: '14px',
              textAlign: 'right',
              marginBottom: '20px',
            }}
            span={6}
          >
            建议额度：
          </Col>
          <Col style={{ marginBottom: '25px' }} span={18}>
            {proposalAuditLimit}
          </Col>
        </Row>

        <Form {...formItemLayout} className="login-form">
          <Form.Item label="当前额度">
            {getFieldDecorator('newLimit', {
              rules: [{ required: true, message: '请填写当前额度' }],
            })(<Input style={{ width: '180px' }} placeholder="审核额度" />)}
          </Form.Item>
          <Form.Item label="小记">
            {getFieldDecorator('closeReason', {
              rules: [{ required: true, message: '请填写小记' }],
            })(<TextArea rows={4} placeholder="小记" />)}
          </Form.Item>
        </Form>
      </Modal>
      <Button type="primary" onClick={() => openModal()}>
        审核通过
      </Button>
    </>
  );
};

export default Form.create()(examinationPassed);
