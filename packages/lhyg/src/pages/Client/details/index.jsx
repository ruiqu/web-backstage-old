import React, { useState, useEffect } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Button, Card, Modal, Tag, Drawer, Table } from 'antd';
import PersonalDetails from './personalDetails';
import RiskReport from './riskReport';
import {
  ExaminationPassed,
  ReviewAndReject,
  AddAWhiteList,
  AddBlacklist,
  CloseOrder,
  ModifyTheQuota,
  UserComments,
  JointDebt,
} from './someButtons';
import { useDetailsHook } from './hook';
import request from '@/services/baseService';
import { getCurrentUser } from '@/utils/localStorage';
import { clientRegisterStatus, orderStatusMap } from '@/utils/enum';
import { platformHook } from '@/utils/platformConfig';

const clientDeatil = () => {
  const detailsHook = useDetailsHook();
  const { serialNum, uids } = detailsHook;

  useEffect(() => {
    getDetail();
  }, [serialNum]);

  //右侧历史订单
  const [drawerVisible, setDrawerVisible] = useState(false);
  const [drawerList, setDrawerList] = useState([]);
  const drawerColumns = [
    {
      title: '订单编号',
      dataIndex: 'orderId',
      width: 130,
      render: (text, record, index) => {
        return (
          <>
            <span style={{ color: 'red' }}>
              {record.children == null ? null : `(共${record.children.length + 1}单) `}
            </span>
            <span>{text}</span>
          </>
        );
      },
    },

    {
      title: '创建时间',
      dataIndex: 'createTime',
      width: 120,
    },

    {
      title: '审核人',
      dataIndex: 'rname',
      width: 80,
    },
    {
      title: '订单状态',
      dataIndex: 'status',
      width: 120,
      render: (text, record, index) => orderStatusMap[text],
    },
    {
      title: '订单备注',
      dataIndex: 'orderRemark',
      width: 120,
    },
    {
      title: '操作',
      width: 80,
      fixed: 'right',
      align: 'center',
      render: (e, record) => {
        return (
          <div>
            <a
              className="primary-color"
              href={`#/Order/HomePage/Details?id=${e.orderId}`}
              target="_blank"
            >
              处理
            </a>
          </div>
        );
      },
    },
  ];

  //图片状态
  const [visiblesimg, setVisiblesimg] = useState(false);
  const [imgUrl, setImgUrl] = useState('');
  const [imgRotate, setImgRotate] = useState('');

  //客户UID
  const [uid, setUid] = useState([]);

  //客户信息
  const [personalDetailsData, setPersonalDetailsData] = useState([]);

  //联系人
  const [contactPerson, setContactPerson] = useState([]);

  //所有数据
  const [allData, setAllData] = useState({});

  //风控报告
  const [windControl, setWindControl] = useState({});

  //审批结论
  const [approvalConclusion, setApprovalConclusion] = useState({});

  const onKaiImg = url => {
    setImgUrl(url);
    setVisiblesimg(true);
  };

  const imageUrl = res => {
    console.log(res);
    // res.userInfoDto.pdfUrl =
    //   'https://rich-rent.oss-cn-hangzhou.aliyuncs.com/964f007540fd45d895c619f8e89450d9.jpg';
    if (res) {
      return (
        <img
          src={res}
          style={{ width: 146, height: 77, marginRight: 20 }}
          onClick={() => onKaiImg(res)}
        />
      );
    }
  };

  //获取客户信息
  const getDetail = async () => {
    let res = await request(
      '/hzsx/userAudit/queryBusinessUserRiskDetail',
      {
        uid: uids,
        serialNum,
        shopId: localStorage.getItem(`shopId`),
      },
      'post',
    );

    setUid(res?.userCertification?.uid);

    setPersonalDetailsData([
      {
        key: 'userName',
        label: '姓名',
        value: res?.userCertification?.userName,
      },
      {
        key: 'telephone',
        label: '手机号',
        value: res?.userCertification?.telephone,
      },
      {
        key: 'idCard',
        label: '身份证号',
        value: res?.userCertification?.idCard,
      },
      {
        key: 'age',
        label: '年龄',
        value: res?.userInfoDto?.age,
      },
      {
        key: 'gender',
        label: '性别',
        value: res?.userInfoDto?.gender,
      },
      {
        key: 'limitDate',
        label: '身份证到期日',
        value: res?.userCertification?.limitDate,
      },
      {
        key: 'faceRecognitionPhoto',
        label: '人脸识别照片',
        value: (function() {
          return imageUrl(res?.userInfoDto?.pdfUrl);
        })(),
      },
      {
        key: 'idPhoto',
        label: '身份证照片',
        value: (function() {
          return (
            <div>
              {imageUrl(res?.userCertification?.idCardBackUrl)}
              {imageUrl(res?.userCertification?.idCardFrontUrl)}
            </div>
          );
        })(),
      },
      {
        key: 'empty',
        label: '',
        value: '',
      },
      {
        key: 'allOrders',
        label: '全部订单数',
        value: (function() {
          return (
            <div>
              <Tag color="blue">{res?.backstageUserRiskOrdersDto?.ordersList?.length}</Tag>
              <Button
                size="small"
                type="primary"
                onClick={() => {
                  setDrawerVisible(true);
                  setDrawerList(res?.backstageUserRiskOrdersDto?.ordersList);
                }}
              >
                查看
              </Button>
            </div>
          );
        })(),
      },
      {
        key: 'endOrderNumber',
        label: '在途/完结订单数',
        value: (function() {
          return (
            <Tag color="blue">
              {res?.backstageUserRiskOrdersDto?.ztOrders}/
              {res?.backstageUserRiskOrdersDto?.wjOrders}
            </Tag>
          );
        })(),
      },
      {
        key: 'numberOfRepurchase',
        label: '复购次数',
        value: res?.userCertification?.repurchaseNum,
      },
      {
        key: 'certificationStatus',
        label: '认证状态',
        value: clientRegisterStatus[res?.userRiskRecord?.registerStatus],
      },
      {
        key: 'certificationTime',
        label: '进件时间',
        value: res?.userRiskRecord?.createTime,
      },
      {
        key: 'modelDivision',
        label: '模型分',
        value: res?.userRiskRecord?.modelScore,
      },
      {
        key: 'auditQuantity',
        label: '历史额度',
        value: res?.userRiskLimit?.beforeAuditLimit,
      },
      // {
      //   key: 'auditRecommendationQuota',
      //   label: '审核建议额度',
      //   value: '',
      // },
      {
        key: 'afterReview',
        label: '总额度',
        value: res?.userRiskLimit?.totalLimit,
      },
      {
        key: 'surplusLimit',
        label: '剩余额度',
        value: res?.userRiskLimit?.surplusLimit,
      },
      {
        key: 'riskControlRemarks',
        label: '风控备注',
        value: res?.userRiskLimit?.riskRemark,
      },
    ]);

    setContactPerson([
      {
        key: 'emergencyContactName_1',
        label: '紧急联系人姓名',
        value: res?.userCertification?.firstContactName,
      },
      {
        key: 'relationship_1',
        label: '关系',
        value: res?.userCertification?.firstContactRelation,
      },
      {
        key: 'contactInformation_1',
        label: '联系方式',
        value: res?.userCertification?.firstContactPhone,
      },
      {
        key: 'emergencyContactName_2',
        label: '紧急联系人姓名',
        value: res?.userCertification?.twoContactName,
      },
      {
        key: 'relationship_2',
        label: '关系',
        value: res?.userCertification?.twoContactRelation,
      },
      {
        key: 'contactInformation_2',
        label: '联系方式',
        value: res?.userCertification?.twoContactPhone,
      },
    ]);

    setWindControl({
      uid: res?.userCertification?.uid,
      serialNum: res?.userRiskLimit?.serialNum,
      orderId: res?.userRiskLimit?.serialNum,
      phone: res?.userCertification?.telephone,
      idCardNo: res?.userCertification?.idCard,
      userName: res?.userCertification?.userName,
    });

    let registerStatus = '未审批';
    if (res?.userRiskLimit?.registerStatus == 7) {
      registerStatus = '审批通过';
    }
    if (res?.userRiskLimit?.registerStatus == 8) {
      registerStatus = '审批未通过';
    }

    setApprovalConclusion({
      //审批时间
      auditTime: res?.userRiskLimit?.auditTime,
      //审批人
      rname: res?.userRiskLimit?.rname,
      //审批结果
      registerStatus,
      //小记
      auditRemark: res?.userRiskLimit?.auditRemark,
    });

    setAllData(res);
  };

  return (
    <PageHeaderWrapper title={false}>
      <Card>
        <Drawer
          width={720}
          title="历史订单"
          placement="right"
          onClose={() => {
            setDrawerVisible(false);
          }}
          visible={drawerVisible}
        >
          <Table footer={null} dataSource={drawerList} columns={drawerColumns} />
        </Drawer>

        <Modal
          title="身份证图片"
          visible={visiblesimg}
          onCancel={() => {
            setVisiblesimg(false);
            setImgRotate(0);
          }}
          destroyOnClose
          footer={null}
        >
          <img
            src={imgUrl}
            alt="alt"
            style={{ width: '100%', transform: `rotate(${imgRotate}deg)` }}
          />
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
              boxSizing: 'border-box',
              padding: '50px 150px 0 150px',
            }}
          >
            <img
              src={require('../../../../public/rotate-icon.png')}
              alt="alt"
              onClick={() => setImgRotate((imgRotate || 0) - 90)}
              style={{ transform: 'rotateY(180deg)', cursor: 'pointer' }}
            />
            <img
              src={require('../../../../public/rotate-icon.png')}
              alt="alt"
              onClick={() => setImgRotate((imgRotate || 0) + 90)}
              style={{ cursor: 'pointer' }}
            />
          </div>
        </Modal>

        <PersonalDetails data={personalDetailsData} />
        <div>
          {getCurrentUser().id == allData?.userRiskLimit?.rid ? (
            <>
              {/* 审批通过 */}
              <ExaminationPassed
                proposalAuditLimit={allData?.userRiskLimit?.proposalAuditLimit}
                beforeAuditLimit={allData?.userRiskLimit?.beforeAuditLimit}
                getDetail={() => getDetail()}
              />
              {/* 审批拒绝 */}
              <ReviewAndReject getDetail={() => getDetail()} />
              {/* 一键拉白 */}
              <AddAWhiteList uid={uid} getDetail={() => getDetail()} />
              {/* 一键拉黑 */}
              <AddBlacklist uid={uid} getDetail={() => getDetail()} />
              {/* 关闭订单 */}
              {/* <CloseOrder getDetail={() => getDetail()} /> */}
            </>
          ) : (
            ''
          )}
          {platformHook().platformIsInterior() ? <JointDebt windControl={windControl} /> : null}

          {/* 修改额度按钮 */}
          <ModifyTheQuota
            proposalAuditLimit={allData?.userRiskLimit?.proposalAuditLimit}
            beforeAuditLimit={allData?.userRiskLimit?.beforeAuditLimit}
            totalLimit={allData?.userRiskLimit?.totalLimit}
            surplusLimit={allData?.userRiskLimit?.surplusLimit}
            getDetail={() => getDetail()}
          />
          <UserComments getDetail={() => getDetail()} />
        </div>
      </Card>

      <Card style={{ marginTop: '20px' }}>
        <RiskReport
          contactPersonData={contactPerson}
          windControl={windControl}
          approvalConclusion={approvalConclusion}
          uid={uid}
        />
      </Card>
    </PageHeaderWrapper>
  );
};

export default clientDeatil;
