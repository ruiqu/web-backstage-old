import { useState } from 'react';
import { useLocation } from 'react-router-dom';

export const useDetailsHook = () => {
  const location = useLocation();
  const [serialNum, setSerialNum] = useState(location.query.serialNum);
  const [uids, setUids] = useState(location.query.uids);
  return {
    serialNum,
    uids,
  };
};
