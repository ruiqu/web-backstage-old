import React from 'react';
import { Descriptions } from 'antd';
import CustomCard from '@/components/CustomCard';

const { Item } = Descriptions;

const personalDetails = props => {
  const { data } = props;

  const item = () => {
    let listItem = [];
    data.map(({ key, label, value }) => {
      let list = (
        <Item key={key} label={label}>
          {value}
        </Item>
      );
      listItem.push(list);
    });

    return listItem;
  };

  return (
    <>
      <Descriptions title={<CustomCard title="客户信息" />}>{item()}</Descriptions>
    </>
  );
};

export default personalDetails;
