import React, { useState } from 'react';
import { Tabs, Descriptions, Button } from 'antd';
import CustomCard from '@/components/CustomCard';
import { RadarReport, Probe, RiskControlReport } from './components';
import FKproessRend from './components/FKproess';
import { useDetailsHook } from '../hook';
import request from '@/services/baseService';
import { platformHook } from '@/utils/platformConfig';

const { TabPane } = Tabs;
const { Item } = Descriptions;

const riskReport = props => {
  const { contactPersonData, windControl, approvalConclusion, uid } = props;

  const detailsHook = useDetailsHook();
  const { serialNum } = detailsHook;

  const infoItem = () => {
    let listItem = [];
    contactPersonData.map(({ key, label, value }) => {
      let list = (
        <Item key={key} label={label}>
          {value}
        </Item>
      );
      listItem.push(list);
    });

    return listItem;
  };

  return (
    <Tabs defaultActiveKey="1" animated={false}>
      <TabPane tab="客户信息" key="1">
        <Descriptions style={{ marginTop: '15px' }} title={<CustomCard title="联系人信息" />}>
          {infoItem()}
        </Descriptions>
      </TabPane>
      {/* <TabPane tab="风控报告" key="2">
        <RiskControlReport windControl={windControl} />
        <Button type="primary" onClick={() => riskControlReport()}>
          点击查询
        </Button>
      </TabPane> */}
      {platformHook().platformIsInterior() ? (
        <TabPane tab="雷达" key="3">
          <RadarReport windControl={windControl} />
        </TabPane>
      ) : null}
      {platformHook().platformIsInterior() ? (
        <TabPane tab="探针" key="4">
          <Probe windControl={windControl} />
        </TabPane>
      ) : null}

      {platformHook().platformNewControl() ? (
        <TabPane tab="风控报告" key="6">
          <FKproessRend orderId={serialNum} userOrderInfoDto={{ uid }} />
        </TabPane>
      ) : (
        ''
      )}

      <TabPane tab="审批结论" key="5">
        <div style={{ marginTop: '10px' }}>审批结果：{approvalConclusion.registerStatus}</div>
        <div style={{ marginTop: '10px' }}>审批时间：{approvalConclusion.auditTime}</div>
        <div style={{ marginTop: '10px' }}>审批人：{approvalConclusion.rname}</div>
        <div style={{ marginTop: '10px' }}>小记：{approvalConclusion.auditRemark}</div>
      </TabPane>
    </Tabs>
  );
};

export default riskReport;
