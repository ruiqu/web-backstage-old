import { useState } from 'react';
import { Table, Button } from 'antd';
import request from '@/services/baseService';

// {
//     "uid": "1136c233c4b889dd3d6401fd009ba9fbf24fd8076tlru9drze7c",
//     "orderId": "000OI20240317-8323808797612113823",
//     "phone": "18287189998",
//     "riskType": "11",
//     "idCardNo": "530103198301081823",
//     "userName": "朱妍"
// }

const riskControlReport = props => {
  const { windControl } = props;

  const [isshow, setIsshow] = useState(false);
  const [report, setReport] = useState();

  const radarQuery = async () => {
    let obj = {
      ...windControl,
      ...{
        riskType: '01',
      },
    };
    let res = await request('hzsx/business/order/querySiriusReportByUid', obj, 'post');

    // let res = {
    //   siriusRiskReport:
    //     '{"requestTime":"2024-03-23 14:41:51","code":"success","data":{"max_performance_amt":"10000-20000","count_performance":"128","currently_overdue":"0","acc_sleep":"38","currently_performance":"44","result_code":"2","latest_performance_time":"2024-03","acc_exc":"0"},"requestId":"17111761107670","message":"成功"}',
    //   createTime: null,
    //   reportType: '11',
    // };
    console.log('风控报告', res);
    // let db = JSON.parse(res.siriusRiskReport);
    // setReport([{ ...db.data, ...{ key: 'siriusRiskReport' } }]);
    // setIsshow(true);
  };

  let v报告详情1 = [
    { title: '最大逾期金额', dataIndex: 'max_overdue_amt', width: 150 },
    { title: '最大逾期天数', dataIndex: 'max_overdue_days', width: 150 },
    { title: '最近逾期时间', dataIndex: 'latest_overdue_time', width: 150 },
    { title: '最大履约金额', dataIndex: 'max_performance_amt', width: 150 },
    { title: '最近履约时间', dataIndex: 'latest_performance_time', width: 150 },
  ];
  let v报告详情2 = [
    { title: '履约笔数', dataIndex: 'count_performance', width: 150 },
    { title: '当前逾期机构数', dataIndex: 'currently_overdue', width: 150 },
    { title: '当前履约机构数', dataIndex: 'currently_performance', width: 150 },
    { title: '异常还款机构数', dataIndex: 'acc_exc', width: 150 },
    { title: '睡眠机构数', dataIndex: 'acc_sleep', width: 150 },
  ];

  return (
    <>
      <Button type="primary" onClick={() => radarQuery()}>
        点击查询
      </Button>
      {isshow && (
        <>
          <p style={{ fontSize: 20 }}>探针报告详情:</p>
          <Table bordered columns={v报告详情1} dataSource={report} pagination={false} />
          <Table bordered columns={v报告详情2} dataSource={report} pagination={false} />
        </>
      )}
    </>
  );
};

export default riskControlReport;
