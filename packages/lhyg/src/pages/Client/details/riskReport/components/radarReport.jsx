import { useState } from 'react';
import { Button, Table } from 'antd';
import request from '@/services/baseService';

const radarReport = props => {
  const { windControl } = props;
  /**查看雷达 */
  //   {
  //     "uid": "1136c233c4b889dd3d6401fd009ba9fbf24fd8076tlru9drze7c",
  //     "orderId": "000OI20240317-8323808797612113823",
  //     "phone": "18287189998",
  //     "riskType": "12",
  //     "idCardNo": "530103198301081823",
  //     "userName": "朱妍"
  // }
  const [isshow, setIsshow] = useState(false);
  const [applyReportDetail, setApplyReportDetail] = useState([]);
  const [behaviorReportDetail, setBehaviorReportDetail] = useState([]);
  const [currentReportDetail, setCurrentReportDetail] = useState([]);
  const radarReport = async () => {
    let obj = {
      ...windControl,
      ...{
        riskType: '12',
      },
    };

    let res = await request('hzsx/business/order/queryXyTzReportByUid', obj, 'post');
    // let res = {
    //   siriusRiskReport:
    //     '{"requestTime":"2024-03-23 13:58:17","code":"success","data":{"current_report_detail":{"C22180006":"1300","C22180007":"9","C22180008":"15","C22180009":"16800","C22180002":"85","C22180003":"1","C22180004":"2","C22180005":"1300","C22180010":"6700","C22180011":"8000","C22180012":"84","C22180001":"1300"},"behavior_report_detail":{"B22170019":"6","B22170050":"(0,7]","B22170053":"613","B22170010":"[50000,+)","B22170054":"2024-03","B22170051":"84","B22170052":"22","B22170013":"1","B22170014":"14","B22170011":"[50000,+)","B22170012":"1","B22170017":"3","B22170018":"4","B22170015":"1","B22170016":"2","B22170020":"10","B22170021":"5","B22170024":"1","B22170025":"0","B22170022":"9","B22170023":"1","B22170028":"0","B22170029":"0","B22170026":"0","B22170027":"0","B22170031":"0","B22170032":"0","B22170030":"0","B22170035":"2","B22170036":"4","B22170033":"0","B22170034":"100%","B22170039":"5","B22170037":"5","B22170038":"5","B22170008":"[20000,30000)","B22170009":"[30000,50000)","B22170042":"[20000,30000)","B22170043":"[30000,50000)","B22170040":"[3000,5000)","B22170041":"[10000,20000)","B22170002":"2","B22170046":"14","B22170003":"6","B22170047":"25","B22170044":"[30000,50000)","B22170001":"616","B22170045":"5","B22170006":"23","B22170007":"[5000,10000)","B22170004":"10","B22170048":"47","B22170005":"17","B22170049":"64"},"apply_report_detail":{"A22160009":"60","A22160008":"32","A22160007":"2024-03","A22160006":"192","A22160005":"6","A22160004":"35","A22160003":"58","A22160002":"75","A22160001":"639","A22160010":"84"}},"requestId":"17111734970981","message":"成功"}',
    //   createTime: null,
    //   reportType: '12',
    // };
    let db = JSON.parse(res.siriusRiskReport);

    setApplyReportDetail([{ ...db.data.apply_report_detail, ...{ key: 'apply_report_detail' } }]);
    setBehaviorReportDetail([
      { ...db.data.behavior_report_detail, ...{ key: 'behavior_report_detail' } },
    ]);
    setCurrentReportDetail([
      { ...db.data.current_report_detail, ...{ key: 'current_report_detail' } },
    ]);
    setIsshow(true);
  };

  let v报告详情1 = [
    { title: '申请准入分', dataIndex: 'A22160001', width: 150 },
    { title: '申请准入置信度', dataIndex: 'A22160002', width: 150 },
    { title: '申请命中机构数', dataIndex: 'A22160003', width: 150 },
    { title: '申请命中消金类机构数', dataIndex: 'A22160004', width: 150 },
    { title: '申请命中网络贷款类机构数', dataIndex: 'A22160005', width: 150 },
  ];
  let v报告详情2 = [
    { title: '机构总查询次数', dataIndex: 'A22160006', width: 150 },
    { title: '最近一次查询时间', dataIndex: 'A22160007', width: 150 },
    { title: '近1个月机构总查询笔数', dataIndex: 'A22160008', width: 150 },
    { title: '近3个月机构总查询笔数', dataIndex: 'A22160009', width: 150 },
    { title: '近6个月机构总查询笔数', dataIndex: 'A22160010', width: 150 },
  ];
  let v行为雷达报告 = [
    { title: '贷款行为分', dataIndex: 'B22170001', width: 150 },
    { title: '近1个月贷款笔数', dataIndex: 'B22170002', width: 150 },
    { title: '近3个月贷款笔数', dataIndex: 'B22170003', width: 150 },
    { title: '近6个月贷款笔数', dataIndex: 'B22170004', width: 150 },
    { title: '近12个月贷款笔数', dataIndex: 'B22170005', width: 150 },
    { title: '近24个月贷款笔数', dataIndex: 'B22170006', width: 150 },
  ];
  let v行为雷达报告1 = [
    { title: '近1个月贷款总金额', dataIndex: 'B22170007', width: 150 },
    { title: '近3个月贷款总金额', dataIndex: 'B22170008', width: 150 },
    { title: '近6个月贷款总金额', dataIndex: 'B22170009', width: 150 },
    { title: '近12个月贷款总金额', dataIndex: 'B22170010', width: 150 },
    { title: '近24个月贷款总金额', dataIndex: 'B22170011', width: 150 },
    { title: '近12个月贷款金额在1k及以下的笔数', dataIndex: 'B22170012', width: 150 },
  ];
  let v行为雷达报告2 = [
    { title: '近12个月贷款金额在1k-3k的笔数', dataIndex: 'B22170013', width: 150 },
    { title: '近12个月贷款金额在3k-10k的笔数', dataIndex: 'B22170014', width: 150 },
    { title: '近12个月贷款金额在1w以上的笔数', dataIndex: 'B22170015', width: 150 },
    { title: '近1个月贷款机构数', dataIndex: 'B22170016', width: 150 },
    { title: '近3个月贷款机构数', dataIndex: 'B22170017', width: 150 },
    { title: '近6个月贷款机构数', dataIndex: 'B22170018', width: 150 },
  ];
  let v行为雷达报告3 = [
    { title: '近12个月贷款机构数', dataIndex: 'B22170019', width: 150 },
    { title: '近24个月贷款机构数', dataIndex: 'B22170020', width: 150 },
    { title: '近12个月消金类贷款机构数', dataIndex: 'B22170021', width: 150 },
    { title: '近24个月消金类贷款机构数', dataIndex: 'B22170022', width: 150 },
    { title: '近12个月网贷类贷款机构数', dataIndex: 'B22170023', width: 150 },
    { title: '近24个月网贷类贷款机构数', dataIndex: 'B22170024', width: 150 },
  ];
  let v行为雷达报告4 = [
    { title: '近6个月M0+逾期贷款笔数', dataIndex: 'B22170025', width: 150 },
    { title: '近12个月M0+逾期贷款笔数', dataIndex: 'B22170026', width: 150 },
    { title: '近24个月M0+逾期贷款笔数', dataIndex: 'B22170027', width: 150 },
    { title: '近6个月M1+逾期贷款笔数', dataIndex: 'B22170028', width: 150 },
    { title: '近12个月M1+逾期贷款笔数', dataIndex: 'B22170029', width: 150 },
    { title: '近24个月M1+逾期贷款笔数', dataIndex: 'B22170030', width: 150 },
  ];
  let v行为雷达报告5 = [
    { title: '近6个月累计逾期金额', dataIndex: 'B22170031', width: 150 },
    { title: '近12个月累计逾期金额', dataIndex: 'B22170032', width: 150 },
    { title: '近24个月累计逾期金额', dataIndex: 'B22170033', width: 150 },
    { title: '正常还款订单数占贷款总订单数比例', dataIndex: 'B22170034', width: 150 },
    { title: '近1个月失败扣款笔数', dataIndex: 'B22170025', width: 150 },
    { title: '近3个月失败扣款笔数', dataIndex: 'B22170036', width: 150 },
  ];
  let v行为雷达报告6 = [
    { title: '近6个月失败扣款笔数', dataIndex: 'B22170037', width: 150 },
    { title: '近12个月失败扣款笔数', dataIndex: 'B22170038', width: 150 },
    { title: '近24个月失败扣款笔数', dataIndex: 'B22170039', width: 150 },
    { title: '近1个月履约贷款总金额', dataIndex: 'B22170040', width: 150 },
    { title: '近3个月履约贷款总金额', dataIndex: 'B22170041', width: 150 },
    { title: '近6个月履约贷款总金额', dataIndex: 'B22170042', width: 150 },
  ];
  let v行为雷达报告7 = [
    { title: '近12个月履约贷款总金额', dataIndex: 'B22170043', width: 150 },
    { title: '近24个月履约贷款总金额', dataIndex: 'B22170044', width: 150 },
    { title: '近1个月履约贷款次数', dataIndex: 'B22170045', width: 150 },
    { title: '近3个月履约贷款次数', dataIndex: 'B22170046', width: 150 },
    { title: '近6个月履约贷款次数', dataIndex: 'B22170047', width: 150 },
    { title: '近12个月履约贷款次数', dataIndex: 'B22170048', width: 150 },
  ];
  let v行为雷达报告8 = [
    { title: '近24个月履约贷款次数', dataIndex: 'B22170049', width: 150 },
    { title: '最近一次履约距今天数', dataIndex: 'B22170050', width: 150 },
    { title: '贷款行为置信度', dataIndex: 'B22170051', width: 150 },
    { title: '贷款已结清订单数', dataIndex: 'B22170052', width: 150 },
    { title: '信用贷款时长', dataIndex: 'B22170053', width: 150 },
    { title: '最近一次贷款放款时间', dataIndex: 'B22170054', width: 150 },
  ];

  let v信用详情 = [
    { title: '网贷授信额度', dataIndex: 'C22180001', width: 150 },
    { title: '网贷额度置信度', dataIndex: 'C22180002', width: 150 },
    { title: '网络贷款类机构数', dataIndex: 'C22180003', width: 150 },
    { title: '网络贷款类产品数', dataIndex: 'C22180004', width: 150 },
    { title: '网络贷款机构最大授信额度', dataIndex: 'C22180005', width: 150 },
    { title: '网络贷款机构平均授信额度', dataIndex: 'C22180006', width: 150 },
  ];
  let v信用详情1 = [
    { title: '消金贷款类机构数', dataIndex: 'C22180007', width: 150 },
    { title: '消金贷款类产品数', dataIndex: 'C22180008', width: 150 },
    { title: '消金贷款类机构最大授信额度', dataIndex: 'C22180009', width: 150 },
    { title: '消金贷款类机构平均授信额度', dataIndex: 'C22180010', width: 150 },
    { title: '消金建议授信额度', dataIndex: 'C22180011', width: 150 },
    { title: '消金额度置信度', dataIndex: 'C22180012', width: 150 },
  ];

  return (
    <>
      <Button type="primary" onClick={() => radarReport()}>
        点击查询
      </Button>

      {isshow && (
        <>
          <p style={{ fontSize: 20 }}>申请雷达报告详情:</p>
          <Table bordered columns={v报告详情1} dataSource={applyReportDetail} pagination={false} />
          <Table bordered columns={v报告详情2} dataSource={applyReportDetail} pagination={false} />
          <p></p>
          <p style={{ fontSize: 20 }}>行为雷达报告详情:</p>
          <Table
            bordered
            columns={v行为雷达报告}
            dataSource={behaviorReportDetail}
            pagination={false}
          />
          <Table
            bordered
            columns={v行为雷达报告1}
            dataSource={behaviorReportDetail}
            pagination={false}
          />
          <Table
            bordered
            columns={v行为雷达报告2}
            dataSource={behaviorReportDetail}
            pagination={false}
          />
          <Table
            bordered
            columns={v行为雷达报告1}
            dataSource={behaviorReportDetail}
            pagination={false}
          />
          <Table
            bordered
            columns={v行为雷达报告3}
            dataSource={behaviorReportDetail}
            pagination={false}
          />
          <Table
            bordered
            columns={v行为雷达报告4}
            dataSource={behaviorReportDetail}
            pagination={false}
          />
          <Table
            bordered
            columns={v行为雷达报告5}
            dataSource={behaviorReportDetail}
            pagination={false}
          />
          <Table
            bordered
            columns={v行为雷达报告6}
            dataSource={behaviorReportDetail}
            pagination={false}
          />
          <Table
            bordered
            columns={v行为雷达报告7}
            dataSource={behaviorReportDetail}
            pagination={false}
          />
          <Table
            bordered
            columns={v行为雷达报告8}
            dataSource={behaviorReportDetail}
            pagination={false}
          />

          <p style={{ fontSize: 20 }}>信用详情:</p>
          <Table bordered columns={v信用详情} dataSource={currentReportDetail} pagination={false} />
          <Table
            bordered
            columns={v信用详情1}
            dataSource={currentReportDetail}
            pagination={false}
          />
        </>
      )}
    </>
  );
};

export default radarReport;
