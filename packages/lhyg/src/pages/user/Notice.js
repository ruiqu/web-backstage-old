import React, { Component } from 'react';
import { getParam } from '@/utils/utils';
import { connect } from 'dva';
@connect(({}) => ({}))
export default class Notice extends Component {
  state = {
    data: '授权成功',
  };
  // /user/notice
  componentDidMount() {
    console.log(getParam('code'), 'getParam');
    console.log(getParam('state').split('#')[0], 'state');
    // /hzsx/weixin/saveWeixinUserInfo

    this.props.dispatch({
      type: 'order/saveWeixinUserInfo',
      payload: {
        code: getParam('code'),
        state: getParam('state').split('#')[0],
      },
      callback: res => {
        console.log(res, 'asdsad');
        this.setState({
          data: res.data,
        });
      },
    });
  }
  render() {
    return (
      <div
        style={{
          textAlign: 'center',
          lineHeight: '100vh',
        }}
      >
        {this.state.data}
      </div>
    );
  }
}
