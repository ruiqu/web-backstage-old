import React, { Component } from 'react';
import { router } from 'umi';
import styles from './index.less';
import Frame from '@/components/Frame';
import ResetForm from './components/ResetForm';
import logoActive from '@/assets/image/logo-active.png';
import logoBot from '@/assets/image/logo-bot.png';


export default class ChangePassword extends Component {
  render() {
    return (
      <div className={styles.layouts}>
        <div className={styles.frameright}>
          <div>
            <img src={logoActive} alt="" srcset="" />
          </div>
          <ResetForm />
          <div style={{ position: 'absolute', bottom: -38, right: 50 }}>
            <img height={81} width={60} src={logoBot} alt="" srcset="" />
          </div>
        </div>
      </div>
    );
  }
}
