import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import MyPageTable from '@/components/MyPageTable';
import {
  Card,
  Table,
  Divider,
  Form,
  DatePicker,
  Input,
  Button,
  Modal,
  message,
  Popconfirm,
  Switch,
  Upload,
  Icon,
} from 'antd';
import request from '@/services/baseService';
import moment from 'moment';
import { apiUrl } from '@/utils/platformConfig';
const { RangePicker } = DatePicker;
const { confirm } = Modal;
@Form.create()
class CardTicketList extends Component {
  state = {
    search: {
      pageNum: 1,
      pageSize: 10,
      supplierName: '',
    },
    form: {
      supplierName: '',
      cardNo: '',
      supplierCode: '',
      remarks: '',
    },
    tableList: [],
    current: 1,
    total: 0,
    pages: 1,
    modal: {
      type: 'add',
      title: '',
    },
    queryLoad: false,
    modalLoad: false,
    renderModalVisible: false,
    renderViewModalVisible: false,
    renderViewModalList: [],

    expImportVisible: false,
    fileList: [],
  };
  componentDidMount() {
    this.getList();
  }

  getList = () => {
    const { search } = this.state;
    let obj = Object.fromEntries(
      Object.entries(search).filter(([key, value]) => {
        // 过滤空值
        return value !== '' && value !== null && value !== undefined;
      }),
    );

    request(`/hzsx/business/cardTicket/supplierList`, obj, 'post').then(res => {
      this.setState({
        tableList: res.records,
        queryLoad: false,
        pages: res.pages,
        total: res.total,
        current: res.current,
      });
    });
  };

  switchOnChange = rec => {
    request(
      `/hzsx/business/cardTicket/enableSupplier`,
      {
        id: rec.id,
      },
      'put',
    ).then(res => {
      this.getList();
    });
  };

  columns = [
    {
      title: 'No.',
      dataIndex: 'id',
      rowScope: 'row',
      width: 60,
    },
    {
      title: '供应商名称',
      dataIndex: 'supplierName',
    },
    {
      title: '供应商编码',
      dataIndex: 'supplierCode',
    },
    {
      title: '是否启用',
      dataIndex: 'isEnable',
      render: (text, rec) => {
        return <Switch checked={!!rec.isEnable} onClick={() => this.switchOnChange(rec)} />;
      },
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
    },
    {
      title: '操作',
      //   width: ,
      //   fixed: 'right',
      align: 'center',
      render: (e, record) => {
        return (
          <div>
            <a onClick={() => this.handleView(record)}>查看库存</a>
            <Divider type="vertical" />
            <a onClick={() => this.handleEdit(record)}>修改</a>
            <Divider type="vertical" />
            <Popconfirm
              title="确定删除嘛"
              onConfirm={() => this.handleDelete(record)}
              okText="是"
              cancelText="否"
            >
              <a>删除</a>
            </Popconfirm>
          </div>
        );
      },
    },
  ];

  expImport = () => {
    this.setState({
      expImportVisible: true,
    });
  };

  renderExpImportModal = () => {
    const { expImportVisible } = this.state;
    const that = this;

    const uploadWarningContent = (repeatCardList, repeatCardNum, totalCard) => {
      return (
        <div>
          <div>导入成功，本次总共导入<span style={{margin:'0 2px'}}>{totalCard}</span>条数据，重复<span style={{color:'#ff4d4f',margin:'0 2px'}}>{repeatCardNum}</span>条数据</div>
          <div style={{marginTop:'10px',marginLeft:'38px'}}>重复卡号：<span style={{color:'#ff4d4f'}}>{repeatCardList.toString()}</span></div>
          
        </div>
      );
    };

    const props = {
      name: 'file',
      action:
        `${apiUrl}/zyj-backstage-web/hzsx/business/order/cardTicketImportByMY`,
      headers: { token: localStorage.getItem('token') },
      multiple: true,
      onChange(info) {
        if (info.file.status === 'done') {
          if (info.file.response.errorCode || info.file.response.errorMessage) {
            message.error(`${info.file.response.errorMessage}`);
            info.fileList[0].status = 'error';
          } else {
            console.log(123, info?.file?.response?.data?.repeatCardNum);
            if (info?.file?.response?.data?.repeatCardNum > 0) {
              let { repeatCardList, repeatCardNum, totalCard } = info.file.response.data;
              Modal.warning({
                content: uploadWarningContent(repeatCardList, repeatCardNum, totalCard),
              });
            } else {
              message.success(`导入成功`);
            }

            info.fileList[0].status = 'success';
          }
        } else if (info.file.status === 'error') {
          message.error(`上传失败`);
        }
        let fileList = [...info.fileList];

        fileList = fileList.slice(-1);

        that.setState({ fileList });
      },
    };

    const downloadFile = () => {
      window.location.href = '/business/卡券导入.xlsx';
    };

    return (
      <Modal
        title="卡券导入"
        width={650}
        visible={expImportVisible}
        onCancel={() => {
          this.setState({
            expImportVisible: false,
            fileList: [],
          });
        }}
        footer={null}
      >
        <div>
          重要提示：导入后，卡券将自动存入对应供应商的库存中
          <br />
          导入前，务必使用规范的模板！！
        </div>

        <Upload {...props} fileList={this.state.fileList}>
          <Button type="primary" style={{ marginTop: 20 }}>
            <Icon type="upload" /> 上传
          </Button>

          <Button type="primary" style={{ marginTop: 20 }} onClick={() => downloadFile()}>
            导入模板下载
          </Button>
        </Upload>
      </Modal>
    );
  };

  handleView = record => {
    request(
      `/hzsx/business/cardTicket/cardTicketQuantity`,
      {
        supplierCode: record.supplierCode,
      },
      'get',
    ).then(res => {
      this.setState({
        renderViewModalList: res,
        renderViewModalVisible: true,
      });
    });
  };

  handSearch = () => {
    this.props.form.validateFields(
      ['searchSupplierName', 'searchCardNo', 'searchSupplierCode'],
      (err, values) => {
        if (!err) {
          let startDate = '';
          let endDate = '';

          this.setState(
            {
              queryLoad: true,
              search: {
                pageNum: 1,
                pageSize: 10,
                supplierName: values.searchSupplierName,
                supplierCode: values.searchSupplierCode,
                cardNo: values.searchCardNo,
              },
            },
            () => this.getList(),
          );
        }
      },
    );
  };

  renderViewModal() {
    const { renderViewModalVisible, renderViewModalList } = this.state;
    const columns = [
      {
        title: '虚拟卡产品名称',
        dataIndex: 'productName',
        key: 'productName',
      },
      {
        title: '产品编码',
        dataIndex: 'productCode',
        key: 'productCode',
      },
      {
        title: '产品价格',
        dataIndex: 'productPrices',
        key: 'productPrices',
      },
      {
        title: '库存数量',
        dataIndex: 'totalStock',
        key: 'totalStock',
      },
    ];
    return (
      <Modal
        title="查看库存"
        visible={renderViewModalVisible}
        width={700}
        onCancel={() =>
          this.setState({
            renderViewModalVisible: false,
          })
        }
        footer={null}
      >
        <Table dataSource={renderViewModalList} pagination={false} columns={columns} />
      </Modal>
    );
  }

  renderModal() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const { renderModalVisible, modalLoad, form, modal } = this.state;
    const { getFieldDecorator } = this.props.form;
    const handleOk = e => {
      e.preventDefault();
      this.props.form.validateFields(
        ['supplierName', 'supplierCode', 'isEnable'],
        (err, values) => {
          if (!err) {
            this.setState({
              modalLoad: true,
            });
            if (modal.type === 'add') {
              values.isEnable = values.isEnable ? 1 : 0;
              request(`/hzsx/business/cardTicket/insertSupplier`, values, 'post').then(res => {
                this.setState({
                  renderModalVisible: false,
                  modalLoad: false,
                });
                this.props.form.resetFields();
                message.success('添加完成', 5);
                this.getList();
              });
            }

            if (modal.type === 'edit') {
              values.isEnable = values.isEnable ? 1 : 0;
              request(
                `/hzsx/business/cardTicket/updateSupplier`,
                { id: form.id, ...values },
                'put',
              ).then(res => {
                console.log('rcs', res);
                this.setState({
                  renderModalVisible: false,
                  modalLoad: false,
                });
                this.props.form.resetFields();
                message.success('修改完成', 5);
                this.getList();
              });
            }
          }
        },
      );
    };

    const handleCancel = e => {
      this.props.form.resetFields();
      this.setState({
        renderModalVisible: false,
        form: {
          id: '',
          supplierName: '',
          cardNo: '',
          supplierCode: '',
          remarks: '',
        },
      });
    };

    return (
      <Modal
        title={modal.title}
        visible={renderModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        confirmLoading={modalLoad}
      >
        <Form {...formItemLayout}>
          <Form.Item label="供应商名称：">
            {getFieldDecorator('supplierName', {
              initialValue: form.supplierName,
              rules: [
                {
                  required: true,
                  message: '请输入手机号',
                },
              ],
            })(<Input placeholder="请输入手机号" />)}
          </Form.Item>
          <Form.Item label="供应商编码：">
            {getFieldDecorator('supplierCode', {
              initialValue: form.supplierCode,
              rules: [
                {
                  required: true,
                  message: '请输入手机号',
                },
              ],
            })(<Input placeholder="请输入姓名" />)}
          </Form.Item>
          <Form.Item label="是否启用：">
            {getFieldDecorator('isEnable', {
              valuePropName: 'checked',
              initialValue: !!form.isEnable ? true : false,
            })(<Switch />)}
          </Form.Item>
        </Form>
      </Modal>
    );
  }

  handleDelete = record => {
    request(`/hzsx/business/cardTicket/deleteSupplier?id=${record.id}`, {}, 'delete').then(res => {
      message.success('删除完成', 5);
      this.getList();
    });
  };

  handleEdit = record => {
    this.setState({
      renderModalVisible: true,
      modal: {
        type: 'edit',
        title: '修改供应商',
      },
      form: {
        id: record.id,
        supplierName: record.supplierName,
        supplierCode: record.supplierCode,
        isEnable: record.isEnable,
      },
    });
  };

  onPage = e => {
    // pageNum: 1,
    // pageSize: 3,
    let data = Object.assign(this.state.search, { pageNum: e.current, pageSize: e.pageSize });

    this.setState(
      {
        search: data,
      },
      () => this.getList(),
    );
  };

  handleAdd = () => {
    this.setState({
      renderModalVisible: true,
      modal: {
        type: 'add',
        title: '新增供应商',
      },
    });
  };

  render() {
    const { current, pages, total, search, queryLoad } = this.state;
    const { getFieldDecorator } = this.props.form;
    console.log('条数', total);
    const paginationProps = {
      current: current,
      pageSize: search.pageSize,
      total: total,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / search.pageSize)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };

    return (
      <PageHeaderWrapper title={false} className="nav-tab">
        {this.renderViewModal()}
        {this.renderModal()}
        {this.renderExpImportModal()}
        <Card bordered={false} className="mt-20">
          <Form layout="inline">
            <Form.Item label="供应商名称：">
              {getFieldDecorator('searchSupplierName', {
                rules: [],
              })(<Input placeholder="请输入供应商名称" allowClear />)}
            </Form.Item>
            <Form.Item label="供应商编码：">
              {getFieldDecorator('searchSupplierCode', {
                rules: [],
              })(<Input placeholder="请输入供应商编码" allowClear />)}
            </Form.Item>
          </Form>
          <Button
            loading={queryLoad}
            onClick={() => this.handSearch()}
            style={{ margin: '20px 0' }}
            type="primary"
            h
          >
            查询
          </Button>
          <Button type="primary" onClick={() => this.handleAdd()}>
            新增
          </Button>
          <Button type="primary" onClick={() => this.expImport()}>
            卡券导入
          </Button>
          {/* </Card>
        <Card bordered={false}> */}
          <MyPageTable
            //   scroll={true}
            onPage={this.onPage}
            style={{ marginTop: '20px' }}
            paginationProps={paginationProps}
            dataSource={this.state.tableList}
            columns={this.columns}
            rowKey="id"
          />
        </Card>
      </PageHeaderWrapper>
    );
  }
}

export default CardTicketList;
