import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import MyPageTable from '@/components/MyPageTable';
import { Card, Spin, Form, DatePicker, Input, Button, Select } from 'antd';
import request from '@/services/baseService';
import moment from 'moment';
import axios from 'axios';
import { getToken } from '@/utils/localStorage';
import { saveAs } from 'file-saver';

const { RangePicker } = DatePicker;
const { Option } = Select;

@Form.create()
class CardTicketList extends Component {
  state = {
    search: {
      current: 1,
      size: 10,
      userName: '',
      orderId: '',
      telephone: '',
      sendTimeStart: '',
      sendTimeEnd: '',
      useTimeStart: '',
      useTimeEnd: '',
      channelType: 'CJT',
    },
    tableList: [],
    current: 1,
    total: 0,
    pages: 1,
    modal: {
      type: 'add',
      title: '',
    },
    spinning: false,
    cardType: [],
  };
  componentDidMount() {
    this.getList();
    this.getCardType();
  }

  getCardType = () => {
    request(`/hzsx/business/cardTicket/supplierList`, { pageNum: 1, pageSize: 100 }, 'post').then(
      res => {
        this.setState({
          cardType: res.records,
        });
      },
    );
  };

  getList = () => {
    const { search } = this.state;
    let obj = Object.fromEntries(
      Object.entries(search).filter(([key, value]) => {
        // 过滤空值
        return value !== '' && value !== null && value !== undefined;
      }),
    );
    this.setState({
      spinning: true,
    });

    request(`/hzsx/business/order/queryCardByPage`, obj, 'post').then(res => {
      this.setState({
        tableList: res.records,
        pages: res.pages,
        total: res.total,
        current: res.current,
        spinning: false,
      });
    });
  };

  columns = [
    {
      title: 'No.',
      dataIndex: 'id',
      rowScope: 'row',
      width: 100,
    },
    {
      title: '订单ID',
      dataIndex: 'orderId',
    },
    {
      title: '姓名',
      dataIndex: 'userName',
    },
    {
      title: '手机号',
      dataIndex: 'telephone',
    },
    {
      title: '供应商名称',
      dataIndex: 'channelType',
      render: (e, text) => {
        let title = ''
        this.state.cardType.forEach(el => {
          if(el.supplierCode === e){
            title = el.supplierName
          }
        })
        return title
      },
    },
    {
      title: '卡名称',
      dataIndex: 'cardName',
    },
    {
      title: '卡号',
      dataIndex: 'cardCode',
    },
    {
      title: '发送时间',
      dataIndex: 'sendTime',
    },
    {
      title: '核销时间',
      dataIndex: 'useTime',
    },
  ];

  handExoirt = () => {
    this.props.form.validateFields(
      ['channelType', 'userName', 'orderId', 'telephone', 'sendTime', 'useTime'],
      (err, values) => {
        if (!err) {
          let sendTimeStart = '';
          let sendTimeEnd = '';
          let useTimeStart = '';
          let useTimeEnd = '';
          if (values.sendTime && values.sendTime.length > 0) {
            sendTimeStart = `${moment(values.sendTime[0]).format('YYYY-MM-DD')} 00:00:00`;
            sendTimeEnd = `${moment(values.sendTime[1]).format('YYYY-MM-DD')} 23:59:59`;
          }
          if (values.useTime && values.useTime.length > 0) {
            useTimeStart = `${moment(values.useTime[0]).format('YYYY-MM-DD')} 00:00:00`;
            useTimeEnd = `${moment(values.useTime[1]).format('YYYY-MM-DD')} 23:59:59`;
          }

          this.setState(
            {
              search: {
                userName: values.userName,
                orderId: values.orderId,
                telephone: values.telephone,
                channelType: values.channelType,
                sendTimeStart: sendTimeStart,
                sendTimeEnd: sendTimeEnd,
                useTimeStart: useTimeStart,
                useTimeEnd: useTimeEnd,
              },
            },
            () => {
              let obj = Object.fromEntries(
                Object.entries(this.state.search).filter(([key, value]) => {
                  // 过滤空值
                  return value !== '' && value !== null && value !== undefined;
                }),
              );
              axios({
                method: 'POST',
                url: '/zyj-backstage-web/hzsx/business/order/cardQueryExport',
                data: obj,
                headers: {
                  token: getToken(),
                },
                responseType: 'blob',
              }).then(res => {
                let url = window.URL.createObjectURL(
                  new Blob([res.data], {
                    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                  }),
                );

                saveAs(url, '卡券导出.xlsx');
              });
            },
          );
        }
      },
    );
  };

  handSearch = () => {
    this.props.form.validateFields(
      ['channelType', 'userName', 'orderId', 'telephone', 'sendTime', 'useTime'],
      (err, values) => {
        if (!err) {
          let sendTimeStart = '';
          let sendTimeEnd = '';
          let useTimeStart = '';
          let useTimeEnd = '';
          if (values.sendTime && values.sendTime.length > 0) {
            sendTimeStart = `${moment(values.sendTime[0]).format('YYYY-MM-DD')} 00:00:00`;
            sendTimeEnd = `${moment(values.sendTime[1]).format('YYYY-MM-DD')} 23:59:59`;
          }
          if (values.useTime && values.useTime.length > 0) {
            useTimeStart = `${moment(values.useTime[0]).format('YYYY-MM-DD')} 00:00:00`;
            useTimeEnd = `${moment(values.useTime[1]).format('YYYY-MM-DD')} 23:59:59`;
          }

          this.setState(
            {
              search: {
                current: 1,
                size: 10,
                userName: values.userName,
                orderId: values.orderId,
                telephone: values.telephone,
                channelType: values.channelType,
                sendTimeStart: sendTimeStart,
                sendTimeEnd: sendTimeEnd,
                useTimeStart: useTimeStart,
                useTimeEnd: useTimeEnd,
              },
            },
            () => this.getList(),
          );
        }
      },
    );
  };

  onPage = e => {
    // current: 1,
    // size: 3,
    let data = Object.assign(this.state.search, { current: e.current, size: e.size });

    this.setState(
      {
        search: data,
      },
      () => this.getList(),
    );
  };

  render() {
    const { current, pages, total, search, cardType } = this.state;
    const { getFieldDecorator } = this.props.form;
    const paginationProps = {
      current: current,
      size: search.size,
      total: total,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / search.size)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };

    return (
      <PageHeaderWrapper title={false} className="nav-tab">
        <Card bordered={false} className="mt-20">
          <Form layout="inline">
            <Form.Item label="供应商">
              {getFieldDecorator('channelType', {
                rules: [],
                initialValue: 'CJT',
              })(
                <Select placeholder="请选择供应商" style={{ width: '150px' }} allowClear>
                  {cardType.map(option => {
                    return (
                      <Option value={option.supplierCode} key={option.supplierCode}>
                        {option.supplierName}
                      </Option>
                    );
                  })}
                </Select>,
              )}
            </Form.Item>
            <Form.Item label="用户名称：">
              {getFieldDecorator('userName', {
                rules: [],
              })(<Input placeholder="请输入用户名称" allowClear />)}
            </Form.Item>
            <Form.Item label="订单号：">
              {getFieldDecorator('orderId', {
                rules: [],
              })(<Input placeholder="请输入订单号" allowClear />)}
            </Form.Item>
            <Form.Item label="手机号：">
              {getFieldDecorator('telephone', {
                rules: [],
              })(<Input placeholder="请输入手机号" allowClear />)}
            </Form.Item>
            <Form.Item label="发卡日期筛选：">
              {getFieldDecorator('sendTime')(<RangePicker format="YYYY-MM-DD" />)}
            </Form.Item>
            <Form.Item label="核销日期筛选：">
              {getFieldDecorator('useTime')(<RangePicker format="YYYY-MM-DD" />)}
            </Form.Item>
          </Form>
          <Button
            loading={this.state.spinning}
            onClick={() => this.handSearch()}
            style={{ margin: '20px 0' }}
            type="primary"
          >
            查询
          </Button>
          <Button onClick={() => this.handExoirt()} style={{ margin: '20px' }} type="primary">
            导出
          </Button>
          {/* </Card>
        <Card bordered={false}> */}
          <Spin spinning={this.state.spinning}>
            <MyPageTable
              //   scroll={true}
              onPage={this.onPage}
              style={{ marginTop: '20px' }}
              paginationProps={paginationProps}
              dataSource={this.state.tableList}
              columns={this.columns}
              rowKey="id"
            />
          </Spin>
        </Card>
      </PageHeaderWrapper>
    );
  }
}

export default CardTicketList;
