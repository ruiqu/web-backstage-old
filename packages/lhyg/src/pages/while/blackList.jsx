import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import MyPageTable from '@/components/MyPageTable';
import {
  Card,
  Table,
  Divider,
  Form,
  DatePicker,
  Input,
  Button,
  Modal,
  message,
  Popconfirm,
} from 'antd';
import request from '@/services/baseService';
import moment from 'moment';

const { RangePicker } = DatePicker;
@Form.create()
class whiteList extends Component {
  state = {
    search: {
      pageNumber: 1,
      pageSize: 10,
      phone: '',
      realName: '',
      cardNo: '',
      startDate: '',
      endDate: '',
    },
    form: {
      phone: '',
      cardNo: '',
      realName: '',
      remarks: '',
    },
    tableList: [],
    current: 1,
    total: 0,
    pages: 1,
    modal: {
      type: 'add',
      title: '',
    },
    queryLoad: false,
    modalLoad: false,
    renderModalVisible: false,
  };
  componentDidMount() {
    this.getList();
  }

  getList = () => {
    const { search } = this.state;

    // let obj = search.keys(el =>{
    //   console.log(el)
    // });
    let obj = Object.fromEntries(
      Object.entries(search).filter(([key, value]) => {
        // 过滤空值
        return value !== '' && value !== null && value !== undefined;
      }),
    );

    request(`/hzsx/userBlack/selectPage`, obj, 'get').then(res => {
      this.setState({
        tableList: res.page.records,
        queryLoad: false,
        pages: res.page.pages,
        total: res.page.total,
        current: res.page.current,
      });
    });
  };

  columns = [
    {
      title: 'No.',
      dataIndex: 'id',
      rowScope: 'row',
      width: 60,
    },
    {
      title: '身份证',
      dataIndex: 'cardNo',
      width: 160,
    },
    {
      title: '电话',
      dataIndex: 'phone',
      width: 160,
    },
    {
      title: '姓名',
      dataIndex: 'realName',
      width: 160,
    },
    {
      title: '备注',
      dataIndex: 'remarks',
      width: 160,
    },
    {
      title: '创建时间',
      dataIndex: 'createDate',
      width: 160,
    },
    // {
    //   title: '失效时间',
    //   dataIndex: 'invalidDate',
    //   width: 160,
    // },
    {
      title: '操作',
      width: 140,
      //   fixed: 'right',
      align: 'center',
      render: (e, record) => {
        return (
          <div>
            <a onClick={() => this.handleEdit(record)}>修改</a>
            <Divider type="vertical" />
            <Popconfirm
              title="确定删除嘛"
              onConfirm={() => this.handleDelete(record)}
              okText="是"
              cancelText="否"
            >
              <a>删除</a>
            </Popconfirm>
          </div>
        );
      },
    },
  ];

  handSearch = () => {
    this.props.form.validateFields(
      ['searchPhone', 'searchRealName', 'searchCardNo', 'searchRangePicker'],
      (err, values) => {
        if (!err) {
          let startDate = '';
          let endDate = '';
          if (values.searchRangePicker) {
            startDate = moment(values.searchRangePicker[0]).format('YYYY-MM-DD');
            endDate = moment(values.searchRangePicker[1]).format('YYYY-MM-DD');
          }

          this.setState(
            {
              queryLoad: true,
              search: {
                pageNumber: 1,
                pageSize: 10,
                phone: values.searchPhone,
                realName: values.searchRealName,
                cardNo: values.searchCardNo,
                startDate: startDate,
                endDate: endDate,
              },
            },
            () => this.getList(),
          );
        }
      },
    );
  };

  renderModal() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const { renderModalVisible, modalLoad, form, modal } = this.state;
    const { getFieldDecorator } = this.props.form;
    const handleOk = e => {
      e.preventDefault();
      this.props.form.validateFields(['phone', 'realName', 'cardNo', 'remarks'], (err, values) => {
        if (!err) {
          this.setState({
            modalLoad: true,
          });
          if (modal.type === 'add')
            request(`/hzsx/userBlack/addUserBlack`, values, 'post').then(res => {
              console.log('rcs', res);
              this.setState({
                renderModalVisible: false,
                modalLoad: false,
              });
              this.props.form.resetFields();
              message.success('添加完成', 5);
              this.getList();
            });

          if (modal.type === 'edit')
            request(`/hzsx/userBlack/updateUserBlack`, { id: form.id, ...values }, 'post').then(
              res => {
                console.log('rcs', res);
                this.setState({
                  renderModalVisible: false,
                  modalLoad: false,
                });
                this.props.form.resetFields();
                message.success('修改完成', 5);
                this.getList();
              },
            );
        }
      });
    };

    const handleCancel = e => {
      this.props.form.resetFields();
      this.setState({
        renderModalVisible: false,
        form: {
          id: '',
          phone: '',
          cardNo: '',
          realName: '',
          remarks: '',
        },
      });
    };

    return (
      <Modal
        title={modal.title}
        visible={renderModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        confirmLoading={modalLoad}
      >
        <Form {...formItemLayout}>
          <Form.Item label="手机号：">
            {getFieldDecorator('phone', {
              initialValue: form.phone,
              rules: [
                {
                  required: true,
                  message: '请输入手机号',
                },
              ],
            })(<Input placeholder="请输入手机号" />)}
          </Form.Item>
          <Form.Item label="姓名：">
            {getFieldDecorator('realName', {
              initialValue: form.realName,
              rules: [
                {
                  required: true,
                  message: '请输入手机号',
                },
              ],
            })(<Input placeholder="请输入姓名" />)}
          </Form.Item>
          <Form.Item label="身份证：">
            {getFieldDecorator('cardNo', {
              initialValue: form.cardNo,
              rules: [
                {
                  required: true,
                  message: '请输入身份证',
                },
              ],
            })(<Input placeholder="请输入身份证" />)}
          </Form.Item>
          <Form.Item label="备注：">
            {getFieldDecorator('remarks', {
              initialValue: form.remarks,
            })(<Input placeholder="请输入备注" />)}
          </Form.Item>
          {/* <Form.Item label="失效时间：">
            {getFieldDecorator('rangePicker', {
              rules: [{ type: 'array', required: true, message: '请输入失效时间' }],
            })(<RangePicker />)}
          </Form.Item> */}
        </Form>
      </Modal>
    );
  }

  handleDelete = record => {
    request(`/hzsx/userBlack/deleteById`, { id: record.id }, 'post').then(res => {
      message.success('删除完成', 5);
      this.getList();
    });
  };

  handleEdit = record => {
    this.setState({
      renderModalVisible: true,
      modal: {
        type: 'edit',
        title: '修改黑名单',
      },
      form: {
        id: record.id,
        phone: record.phone,
        cardNo: record.cardNo,
        realName: record.realName,
        remarks: record.remarks,
      },
    });
  };

  onPage = e => {
    // pageNumber: 1,
    // pageSize: 3,
    let data = Object.assign(this.state.search, { pageNumber: e.current, pageSize: e.pageSize });

    this.setState(
      {
        search: data,
      },
      () => this.getList(),
    );
  };

  handleAdd = () => {
    this.setState({
      renderModalVisible: true,
      modal: {
        type: 'add',
        title: '新增黑名单',
      },
    });
  };

  render() {
    const { current, pages, total, search, queryLoad } = this.state;
    const { getFieldDecorator } = this.props.form;
    console.log('条数', total);
    const paginationProps = {
      current: current,
      pageSize: search.pageSize,
      total: total,
      showTotal: total => (
        <span style={{ fontSize: '14px' }}>
          <span>共{Math.ceil(total / search.pageSize)}页</span>&emsp;
          <span>共{total}条</span>
        </span>
      ),
    };

    return (
      <PageHeaderWrapper title={false} className="nav-tab">
        {this.renderModal()}
        <Card bordered={false} className="mt-20">
          <Form layout="inline">
            <Form.Item label="手机号：">
              {getFieldDecorator('searchPhone', {
                rules: [],
              })(<Input placeholder="请输入手机号" />)}
            </Form.Item>
            <Form.Item label="姓名：">
              {getFieldDecorator('searchRealName', {
                rules: [],
              })(<Input placeholder="请输入姓名" />)}
            </Form.Item>
            <Form.Item label="身份证：">
              {getFieldDecorator('searchCardNo', {
                rules: [],
              })(<Input placeholder="请输入身份证" />)}
            </Form.Item>
            {/* <Form.Item label="失效时间：">
              {getFieldDecorator('searchRangePicker', {})(<RangePicker />)}
            </Form.Item> */}
            {/* {this.renderFooter()} */}
          </Form>
          <Button
            loading={queryLoad}
            onClick={() => this.handSearch()}
            style={{ marginTop: '20px' }}
            type="primary"
            h
          >
            查询
          </Button>
          <Button type="primary" onClick={() => this.handleAdd()}>
            新增
          </Button>
          {/* </Card>
        <Card bordered={false}> */}
          <MyPageTable
            //   scroll={true}
            onPage={this.onPage}
            style={{ marginTop: '20px' }}
            paginationProps={paginationProps}
            dataSource={this.state.tableList}
            columns={this.columns}
            rowKey="id"
          />
        </Card>
      </PageHeaderWrapper>
    );
  }
}

export default whiteList;
