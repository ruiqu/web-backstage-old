// 本地如果用域名接口报错，请用ip，发布的时候请用域名
// export const baseUrl = 'http://139.224.164.75:9000/zwzrent-ope/';//广鼎环境

//export const baseUrl = 'http://139.196.149.180:9000/zwzrent-ope/';//自己搭建的测试环境
import { apiUrl } from '@/utils/platformConfig';
const { NODE_ENV } = process.env;

export const baseUrl = 'https://www.zwzrent.com/zwzrent-ope/';//广鼎线上环境

// export const uploadUrl = NODE_ENV === 'development' ? 'http://192.168.110.152:9002/zyj-backstage-web/hzsx/busShop/doUpLoadwebp' : 'https://yg.yuexiaozu.com/zyj-backstage-web/hzsx/busShop/doUpLoadwebp';
//152章超   198罗
export const uploadUrl =  `${apiUrl}/zyj-backstage-web/hzsx/busShop/doUpLoadwebp`;