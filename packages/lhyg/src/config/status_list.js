export default {
  // 用户取消订单
  USER_CANCELED_CLOSED: '用户取消订单',
  // 已冻结
  ALREADY_FREEZE: '已冻结',
  // 待支付
  WAITING_PAYMENT: '待支付',
  // 用户超时支付关闭订单
  USER_OVERTIME_PAYMENT_CLOSED: '用户超时支付关闭订单',
  // 用户删除订单
  USER_DELETE_ORDER: '用户删除订单',
  // 平台关闭订单
  PLATFORM_CLOSE_ORDER: '平台关闭订单',
  // 商家超时发货关闭订单
  BUSINESS_OVERTIME_CLOSE_ORDER: '商家超时发货关闭订单',
  // 商家关闭订单
  BUSINESS_CLOSE_ORDER: '商家关闭订单',
  // 待商家发货
  WAITING_BUSINESS_DELIVERY: '待商家发货',
  // 待用户确认收货
  WAITING_USER_RECEIVE_CONFIRM: '待用户确认收货',
  // 待商家确认退货退款
  WAITING_BUSINESS_CONFIRM_RETURN_REFUND: '待商家确认退货退款',
  // 待商家确认收货
  WAITING_BUSINESS_RECEIVE_CONFIRM: '待商家确认收货',
  // 租用中
  WAITING_GIVE_BACK: '租用中',
  TO_BE_RETURNED: '待归还',
  // 待结算
  WAITING_SETTLEMENT: '待结算',
  // 待确认结算端(C端确认)
  WAITING_CONFIRM_SETTLEMENT: '待确认结算端(C端确认)',
  // 出结算单后的待支付
  WAITING_SETTLEMENT_PAYMENT: '出结算单后的待支付',
  // 自动确认结算
  SETTLEMENT_WITHOUT_PAYMENT_OVERTIME_AUTOCONFIRM: '自动确认结算',
  // // 结算支付超时
  SETTLEMENT_WITH_PAYMENT_OVERTIME: '结算支付超时',
  // 归还中 只在核销时起作用
  GIVING_BACK: '归还中',
  // 订单完成
  ORDER_FINISH: '订单完成',
  // 逾期
  ORDER_VERDUE: '逾期',
  // 结算单违约金支付逾期
  SETTLEMENT_RETURN_CONFIRM_PAY: '结算单违约金支付逾期',
  // 用户支付失败关闭订单
  USER_PAY_FAIL_CLOSE: '用户支付失败关闭订单',

  // 风控关单
  RC_REJECT: '风控关单',
  APPLY_CLOSE_ORDER: '订单关闭',

  WAITING_BUCKLE_SETTLEMENT: '代扣结算中',
  SHOP_RISK_REVIEW_CLOSE_ORDER: '商家风控关单',
};
