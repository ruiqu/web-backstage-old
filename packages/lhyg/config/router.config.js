export default [
  {
    path: '/user',
    component: '../layouts/UserLayout',
    routes: [
      {
        name: 'login',
        path: '/user/login',
        component: './user/login',
      },
      {
        name: 'ChangePassword',
        path: '/user/ChangePassword',
        component: './user/ChangePassword',
      },
      {
        name: 'PasswordStatuspage',
        path: '/user/PasswordStatuspage',
        component: './user/PasswordStatuspage',
      },
      {
        name: 'notice',
        path: '/user',
        component: './user/Notice',
      },
      {
        component: './404',
      },
    ],
  },
  {
    path: '/',
    component: '../layouts/SecurityLayout',
    routes: [
      {
        path: '/',
        component: '../layouts/BasicLayout',
        // authority: ['admin', 'user'],
        routes: [
          // dashboard
          { path: '/', redirect: '/Index' },
          // 首页
          {
            path: '/Index',
            name: 'Index',
            icon: 'bank',
            component: './Index',
          },
          // 首页
          // {
          //   path: '/Index2',
          //   name: 'Index',
          //   icon: 'bank',
          //   component: './Index2',
          // },
          //数据统计
          {
            path: '/statistics',
            name: 'statistics',
            icon: 'bar-chart',
            authority: ['数据统计'],
            routes: [
              {
                authority: ['数据统计列表'],
                name: 'statistics2',
                path: '/statistics/statistics',
                component: './Statistics/statistics',
              },
              {
                authority: ['到期统计'],
                name: 'expireStatistics',
                path: '/statistics/expireStatistics',
                component: './Statistics/expireStatistics',
              },
              // {
              //   authority: ['到期统计'],
              //   name: 'proceedsStatistics',
              //   path: '/statistics/proceedsStatistics',
              //   component: './Statistics/proceedsStatistics',
              // },
              {
                authority: ['渠道到期统计'],
                name: 'qdStatistics',
                path: '/statistics/qdStatistics',
                component: './Statistics/qdStatistics',
              },
              // {
              //   authority: ['渠道轮次统计'],
              //   name: 'qdlcStatistics',
              //   path: '/statistics/qdlcStatistics',
              //   component: './Statistics/qdlcStatistics',
              // },
            ],
          },
          // 店铺管理
          {
            path: '/shop',
            name: 'shop',
            icon: 'shop',
            authority: ['店铺管理'],
            routes: [
              {
                path: '/shop/info',
                name: 'info',
                authority: ['店铺信息'],
                component: './Shop/Info',
              },
              {
                path: '/shop/info/addInfo',
                name: 'addInfo',
                component: './Shop/AddInfo',
                hideInMenu: true,
              },
              {
                path: '/shop/info/Agreement',
                name: 'Agreement',
                component: './Shop/Agreement',
                hideInMenu: true,
              },
            ],
          },
          //卡券供应商管理
          {
            path: '/cardTicket',
            name: 'cardTicket',
            icon: 'credit-card',
            authority: ['卡券供应商管理'],
            routes: [
              {
                path: '/cardTicket/list',
                name: 'cardTicketList',
                authority: ['供应商管理'],
                component: './CardTicket/List',
              },
              {
                path: '/cardTicket/export',
                name: 'cardExport',
                authority: ['卡券导出'],
                component: './CardTicket/Export',
              },
            ],
          },

          // 商品管理
          {
            path: '/goods',
            name: 'goods',
            icon: 'shopping',
            authority: ['商品管理'],
            routes: [
              // 所有商品
              {
                path: '/goods/list',
                name: 'list',
                component: './Goods/List',
                authority: ['商品列表'],
              },
              // {
              //   path: '/goods/Purchase',
              //   name: 'Purchase',
              //   component: './Goods/Purchase',
              //   authority: ['商品列表'],
              // },
              {
                path: '/goods/list/detail/:id?',
                name: 'detail',
                component: './Goods/Detail',
                hideInMenu: true,
              },
              {
                path: '/goods/Purchase/PurchaseDetail/:id?',
                name: 'PurchaseDetail',
                component: './Goods/PurchaseDetail',
                hideInMenu: true,
              },

              // 租赁规则模板
              // {
              //   path: '/goods/rentRuleList',
              //   name: 'rentRuleList',
              //   component: './Goods/RentRuleList',
              // },
              // // 租赁规则模板详情
              // {
              //   path: '/goods/rentRuleList/rentRuleDetail/:id?',
              //   name: 'rentRuleDetail',
              //   component: './Goods/RentRuleDetail',
              //   hideInMenu: true,
              // },
              // // 赔偿规则模板
              // {
              //   path: '/goods/compensateList',
              //   name: 'compensateList',
              //   component: './Goods/CompensateList',
              // },
              // {
              //   path: '/goods/compensateList/compensateDetail/:id?',
              //   name: 'compensateDetail',
              //   component: './Goods/CompensateDetail',
              //   hideInMenu: true,
              // },
              // 归还地址
              // {
              //   path: '/goods/givebackList',
              //   name: 'givebackList',
              //   component: './Goods/GivebackList',
              //   authority: ['归还地址'],
              // },
              // 增值服务
              // {
              //   path: '/goods/addedServices',
              //   name: 'addedServices',
              //   component: './Shop/AddedServices',
              //   authority: ['增值服务'],
              // },
              {
                path: '/goods/addedServices/detail/:id?',
                name: 'serviceDetail',
                component: './Shop/ServiceDetail',
                hideInMenu: true,
              },
            ],
          },
          //客户管理
          {
            path: '/Client',
            name: 'Client',
            icon: 'user',
            hideInMenu: false,
            authority: ['客户管理'],
            routes: [
              {
                path: '/Client/client',
                name: 'client',
                // icon: 'user',
                authority: ['客户列表'],
                component: './Client/client/index',
              },
              {
                path: '/Client/Details',
                hideInMenu: true,
                name: 'clientDetails',
                component: './Client/details/index',
              },
              {
                path: '/Client/claimOrder',
                name: 'claimOrder',
                component: './Client/claimOrder/index',
                authority: ['认领客户'],
                // hideInMenu: true
              },
              {
                path: '/Client/claimRecord',
                name: 'claimRecord',
                component: './Client/claimRecord/index',
                authority: ['客户认证记录'],
              },
            ],
          },
          // 订单
          {
            path: '/Order',
            name: 'Order',
            icon: 'audit',
            hideInMenu: false,
            authority: ['订单管理'],
            routes: [
              {
                path: '/Order/HomePage',
                component: './Order/HomePage',
                hideInMenu: false,
                name: 'HomePage',
                authority: ['订单列表'],
              },
              {
                path: '/Order/monthTj',
                component: './Order/monthTj',
                hideInMenu: true,
                name: 'monthTj',
                authority: ['订单统计'],
              },
              {
                path: '/Order/overOrder',
                component: './Order/overOrder',
                hideInMenu: false,
                name: 'overOrder',
                authority: ['到期订单'],
              },
              {
                path: '/Order/HomePage/Details',
                component: './Order/Details',
                hideInMenu: true,
                name: 'Details',
              },
              {
                path: '/Order/HomePage/ReceiveOrder',
                component: './Order/HomePage/ReceiveOrder',
                hideInMenu: true,
                name: 'ReceiveOrder',
              },
              // {
              //   path: '/Order/Close',
              //   component: './Order/Close',
              //   hideInMenu: false,
              //   name: 'Close',
              // },

              {
                path: '/Order/BeOverdue',
                component: './Order/BeOverdue',
                hideInMenu: false,
                name: 'BeOverdue',
                authority: ['逾期订单'],
              },
              {
                path: '/Order/yqtj',
                component: './Order/yqtj',
                hideInMenu: false,
                name: 'yqtj',
                authority: ['逾期统计'],
              },
              // {
              //   path: '/Order/OverdueNoRetrun',
              //   component: './Order/OverdueNoRetrun',
              //   hideInMenu: false,
              //   name: 'OverdueNoRetrun',
              //   authority: ['到期未归还订单'],
              // },
              {
                path: '/Order/BeOverdue/Details',
                component: './Order/Details',
                hideInMenu: true,
                name: 'Details',
              },
              {
                path: '/Order/OverdueNoRetrun/Details',
                component: './Order/Details',
                hideInMenu: true,
                name: 'Details',
              },
              // {
              //   path: '/Order/BuyOut',
              //   component: './Order/BuyOut',
              //   hideInMenu: false,
              //   name: 'BuyOut',
              //   authority: ['买断订单'],
              // },
              {
                path: '/Order/BuyOut/Details',
                component: './Order/Details',
                hideInMenu: true,
                name: 'Details',
              },

              // {
              //   path: '/Order/RentRenewal',
              //   component: './Order/RentRenewal',
              //   hideInMenu: false,
              //   name: 'RentRenewal',
              //   authority: ['续租订单'],
              // },
              {
                path: '/Order/RentRenewal/Details',
                component: './Order/Details',
                hideInMenu: true,
                name: 'Details',
              },
              // {
              //   path: '/Order/Purchase',
              //   component: './Order/Purchase',
              //   hideInMenu: false,
              //   name: 'Purchase',
              //   authority: ['续租订单'],
              // },
              // {
              //   path: '/Order/Purchase/Details',
              //   component: './Order/Details',
              //   hideInMenu: true,
              //   name: 'Details',
              // },
              {
                path: '/Order/Details',
                component: './Order/Details',
                hideInMenu: true,
                name: 'Details',
              },
              // {
              //   path: '/Order/Details/:id',
              //   component: './Order/Details',
              //   hideInMenu: true,
              //   name: 'Details',
              // },
              {
                path: '/Order/BuyOut/BuyOutDetails',
                component: './Order/BuyOutDetails',
                hideInMenu: true,
                name: 'BuyOutDetails',
              },
              {
                path: '/Order/Purchase/PurchaseDetails',
                component: './Order/PurchaseDetails',
                hideInMenu: true,
                name: 'Details',
              },
              {
                component: './404',
              },
            ],
          },

          {
            path: '/zz',
            name: 'zz',
            icon: 'GlobalOutlined',
            authority: ['试用中订单'],
            routes: [
              {
                path: '/zz/zyz',
                component: './Order/zyz',
                hideInMenu: false,
                name: 'zyz',
                authority: ['试用中'],
              },
              {
                path: '/zz/yuefu',
                component: './Order/yuefu',
                hideInMenu: false,
                name: 'yuefu',
                authority: ['试用（30天）订单'],
              },
              {
                path: '/zz/zhoufu',
                component: './Order/zhoufu',
                hideInMenu: false,
                name: 'zhoufu',
                authority: ['试用（7天）订单'],
              },
              // {
              //   path: '/zz/rifu',
              //   component: './Order/rifu',
              //   hideInMenu: false,
              //   name: 'rifu',
              //   authority: ['日付账单'],
              // },
              {
                path: '/zz/day10fu',
                component: './Order/day10fu',
                hideInMenu: false,
                name: 'day10fu',
                authority: ['试用（10天）订单'],
              },
              // {
              //   path: '/zz/day5fu',
              //   component: './Order/day5fu',
              //   hideInMenu: false,
              //   name: 'day5fu',
              //   authority: ['10天账单'],
              // },
            ],
          },
          {
            path: '/qd',
            name: 'qd',
            icon: 'ForkOutlined',
            authority: ['渠道管理'],
            routes: [
              {
                path: '/qd/qd',
                name: 'qd',
                component: './Finance/qd',
                authority: ['渠道'],
              },
              {
                path: '/qd/qdtj',
                name: 'qdtj',
                component: './Finance/qdtj',
                authority: ['渠道统计'],
              },
            ],
          },
          // 营销管理

          // 数据管理
          {
            path: '/dataManagement',
            name: 'dataManagement',
            icon: 'table',
            authority: ['数据管理'],
            routes: [
              {
                path: '/dataManagement/dataDownloadCenter',
                name: 'dataDownloadCenter',
                component: './DataManagement/ExportDownloadCenter',
                authority: ['导出数据下载'],
              },
            ],
          },
          {
            path: '/coupons',
            icon: 'money-collect',
            name: 'coupons',
            hideInMenu: true,
            authority: ['营销管理'],
            // authority: ['admin'],
            routes: [
              {
                path: '/coupons/list',
                name: 'list',
                component: './Coupons/List',
                authority: ['优惠券列表'],
              },
              {
                path: '/coupons/list/add',
                name: 'add',
                component: './Coupons/AddList',
                hideInMenu: true,
              },
              {
                path: '/coupons/Package',
                name: 'Package',
                component: './Coupons/Package',
                authority: ['大礼包'],
              },
              {
                hideInMenu: true,
                path: '/coupons/list/ToView',
                name: 'ToView',
                component: './Coupons/ToView',
              },
              {
                hideInMenu: true,
                path: '/coupons/TheEditorList',
                name: 'TheEditorList',
                component: './Coupons/TheEditorList',
              },
              {
                path: '/coupons/config',
                name: 'config',
                component: './Coupons/Config',
                authority: ['店铺营销图配置'],
              },
            ],
          },

          // 财务管理
          {
            path: '/finance',
            name: 'finance',
            icon: 'shopping',
            hideInMenu: true,
            authority: ['财务管理'],
            routes: [
              {
                path: '/finance/capitalAccount',
                name: 'money',
                component: './Finance/capitalAccount',
                authority: ['资金账户'],
              },

              {
                path: '/finance/settlement',
                name: 'settlement',
                component: './Finance/settlement',
                authority: ['财务结算'],
              },
              {
                path: '/finance/overview',
                name: 'overview',
                component: './Finance/overview',
                authority: ['结算明细查询'],
              },
              {
                path: '/finance/feeDetail',
                name: 'feeDetail',
                component: './Finance/feeDetail',
                authority: ['费用结算明细'],
              },
              {
                path: '/finance/settlement/detail',
                name: 'settlementdetail',
                component: './Finance/settlementDetail',
                hideInMenu: true,
              },
              {
                path: '/finance/normal',
                name: 'normal',
                component: './Finance/normal',

                hideInMenu: true,
              },
              {
                path: '/finance/normal/detail/:id',
                name: 'detail',
                component: './Finance/orderDetail',
                hideInMenu: true,
              },

              {
                path: '/finance/buyout',
                name: 'buyout',
                component: './Finance/buyout',

                hideInMenu: true,
              },
              {
                path: '/finance/buyout/detail/:id',
                name: 'detail',
                component: './Finance/buyoutOrderDetail',
                hideInMenu: true,
              },
              {
                path: '/finance/purchase',
                name: 'purchase',
                component: './Finance/purchase',

                hideInMenu: true,
              },
              {
                path: '/finance/purchase/purchaseDetail/:id',
                name: 'purchaseDetail',
                component: './Finance/purchaseDetail',
                hideInMenu: true,
              },
            ],
          },
          // 权限管理
          {
            path: '/permission',
            name: 'permission',
            icon: 'tool',
            authority: ['权限管理'],
            routes: [
              {
                path: '/permission/index',
                name: 'index',
                component: './Permission/index',
                authority: ['部门列表'],
              },
              {
                path: '/permission/index/config/:id',
                name: 'config',
                component: './Permission/PermissionConfig',
                hideInMenu: true,
              },
              {
                path: '/permission/member',
                name: 'member',
                component: './Member/index',
                authority: ['成员管理'],
              },
              {
                path: '/permission/member/config/:id',
                name: 'config',
                component: './Permission/PermissionConfig',
                hideInMenu: true,
              },
              // {
              //   path: '/permission/notice',
              //   name: 'notice',
              //   component: './Notice/index',
              //   authority: ['成员管理'],
              // },
            ],
          },

          // 服务中心
          {
            path: '/serviceCenter',
            name: 'serviceCenter',
            icon: 'question-circle',
            authority: ['权限管理'],
            // hideInMenu: true,
            routes: [
              {
                path: '/serviceCenter',
                name: 'serviceCenterProblem',
                component: './ServiceCenter/problemList',
                authority: ['常见问题'],
              },
              {
                path: '/serviceCenter/ts',
                component: './Order/ts',
                // hideInMenu: true,
                name: 'ts',
                authority: ['权限管理'],
              },
              {
                path: '/serviceCenter/problemDetail/:id',
                name: 'serviceCenterProblemDetail',
                component: './ServiceCenter/problemDetail',
                hideInMenu: true,
              },
            ],
          },
          // 白名单
          {
            path: '/while',
            name: 'while',
            icon: 'contacts',
            authority: ['名单管理'],
            routes: [
              {
                path: '/while/whiteList',
                name: 'whiteList',
                component: './while/whiteList',
                authority: ['白名单'],
              },
              {
                path: '/while/blackList',
                name: 'blackList',
                component: './while/blackList',
                authority: ['黑名单'],
              },
            ],
          },
          // 禁封功能
          {
            path: '/ban',
            name: 'ban',
            icon: 'stop',
            authority: ['违规禁封管理'],
            routes: [
              {
                path: '/ban/phone',
                name: 'banPhone',
                component: './ban/phone',
                authority: ['手机黑名单'],
              },
              {
                path: '/ban/ip',
                name: 'banIp',
                component: './ban/ip',
                authority: ['IP黑名单'],
              },
            ],
          },
          //客户列表
          // {
          //   path: '/client',
          //   name: 'client',
          //   icon: 'user',
          //   authority: ['客户列表'],
          //   path: '/Client/client',
          //   component: './Client/client',
          // },
          //登录记录
          {
            path: '/log',
            name: 'log',
            icon: 'clock-circle',
            authority: ['登录日志'],
            path: '/Log/log',
            component: './Log/log',
          },
          {
            path: '/error',
            name: 'error',
            icon: 'close-circle',
            authority: ['登录日志'],
            component: './error/error',
          },
          {
            component: './404',
          },
        ],
      },
      {
        component: './404',
      },
    ],
  },
  {
    component: './404',
  },
];
