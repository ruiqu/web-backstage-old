## 1、项目介绍

蓝海易购
运营平台+商家平台代码仓库。
     
packages/zwzshared包里面可以放两个项目都需要使用到的代码。

## 2、项目使用
      
+ cnpm install -g rollup: 全局安装rollup，用来编译共享包代码
+ cnpm install -g onchange: 全局安装onchange，用来监听zwzshared包中的文件变化。非必须
+ onchange -i "./src/**/*.js" -- rollup -c
+ npm run install: 给packages目录中的所有子项目（运营平台、商家平台、共享包）安装依赖
+ npm run start:ope: 启动运营平台
+ npm run start:lhyg: 启动商家平台
       
## 3、一些问题

### 3-1、云效流水线打包很慢的问题

目前在下载依赖时，相比较老方案，需要多下载如下这些包：

+ lerna（项目主包，必须下载）
+ packages/zwzshared里面package.json中的所有依赖
    
其中lerna这个package必须下载，但是对于zwzshared里面的依赖来说，不论是运行时依赖还是dev依赖，在流水线环境中是没有必要下载的。（开发自己开发时打包好了
     
已知：对于lerna项目而言，如果利用了lerna构建一个共享代码包的话，那么就必须使用lerna bootstrap命令来下载依赖。
     
问题是：这边没有查阅到lerna的相关资料可以忽略某个子package的dev依赖。所以目前对于zwzshared包的运行时依赖目前也还是下载了。
     
## 4、全局搜索***名字***改为公司名称